## Interface: 30300
## Title: Fatality
## Notes: Announces player deaths to chat. Configuration via Lua.
## SavedVariables: FatalityDB
## Author: Moop
## Version: 1.1.4

Fatality.lua