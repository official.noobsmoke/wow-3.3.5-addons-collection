## Interface: 30300
## Title: Ardent Defender
## Notes: Protection Paladin addon. Tracks and announces things automagicaly.
## Author: Blinddate
## Version: 1.95
## X-Category: Interface Enhancements 
## SavedVariables: ArdentDefenderDB
## X-Embeds: Ace3
## OptionalDeps: Ace3
## X-Curse-Packaged-Version: v1.95
## X-Curse-Project-Name: Ardent Defender
## X-Curse-Project-ID: ardent-defender
## X-Curse-Repository-ID: wow/ardent-defender/mainline

embeds.xml
Core.lua
