--[[-----------------------------------------------------------------------------
-- Ardent Defender is a standalone addon for Paladins that provides several features
handy for tanks. 

** Track and Announce Ardent Defender saving your life
** Alerts you when another person causes a parryhaste on your target
** Also alerts if you cause a parryhaste on a boss someone else is tanking.
** Uses libDataBroker to give you damage and ardent save stats for current session
** Announces bubblewall UP and DOWN status. (No more making macros!)
** Announce if your taunt fails and why (IMMUNE, Resist) and if your avenger shield misses
** Makes a reasonable assumption of the amount Ardent Defender proc will heal you for

--** Thanks to: 
	Ace3, for the framework.
	hypehuman from WoWHead comments for his formula for calculating AD Heals
	All my friends and guildmates on US-Thrall for helping me test this addon.
	Tankadin2 - Ardent Defender plugin for the idea and some of the original code (now deprecated and replaced)
	


--------------------------------------------------------------------------------]]

-- Starts us off. Thank you Ace3!

ArdentDefender = LibStub("AceAddon-3.0"):NewAddon("ArdentDefender", "AceConsole-3.0", "AceEvent-3.0")


-- Checks to see if we are on the PTR
local isPTR = FeedbackUI and true or false
local VERSION = "1.95"
local debug = false
local enabled = true
local AD_FLASHTEXTURE = "Interface\\Addons\\ArdentDefender\\Media\\flash.tga" -- Flashes screen blue
local AD_SOUND = "Sound\\Spells\\SimonGame_Visual_BadPress.wav" -- Ahh.. brings back memories of BC, doesn't it?

-- Flags
local SAY = 2
local PARTY = 4
local RAID = 8
local RAIDWARNING = 16
local WHISPER = 32
local LOCAL = 64


local function HasFlag(x,y)

	if (bit.band(x,y) > 0) then 
		return true 
	else 
		return false 
	end 
end

local function ToggleFlag(x,y)

	 return bit.bxor(x,y)
end



local ArdentSpells= {
	Other = {
		[6940] = "HANDSAC",
		[10278] = "HOP",
		[1038] = "SALV",
		[48788] = "LOH",
		[633] = "LOH",
		[2800] = "LOH",
		[10310] = "LOH",
		[27154] = "LOH",
		[19753] = "DI",
		[1044] = "FREEDOM",
	},
	Self = {
		[64205] = "DIVSAC",
		[66233] = "AVERT", -- Ardent Defender proc (de)buff
		[498] = "BUBBLEWALL", 
		[66235] = "ADHEAL",
		[31935] = "AVENGERMISS", -- all ranks of avenger shield
		[32699] = "AVENGERMISS",
		[32700] = "AVENGERMISS",
		[48826] = "AVENGERMISS",
		[48827] = "AVENGERMISS",
		[62124] = "TAUNTMISS", -- Hand of reckoning
		[31789] = "TAUNTMISS", -- Righteous Defense
		[25780] = "RFHACK", -- Little hack to keep an eye on RF
	},
	Taunts = {
	[355] = "WARRIORTAUNT", -- Taunt
	[694] = "WARRIORTAUNT", -- Mocking Blow
	[1161] = "WARRIORTAUNT", -- Challenging shout
	[49576] = "DKTAUNT", -- Death grip
	[56222] = "DKTAUNT", -- Dark Command
	[62124] = "PALLYTAUNT", -- Hand of reckoning
	[31789] = "PALLYTAUNT", -- Righteous Defense
	[6795] = "DRUIDTAUNT", -- Growl
	[5209] = "DRUIDTAUNT", -- Challenging Roar
	[20736] = "HUNTERTAUNT", -- Distracting shot
	},
}

-- I don't use most of these colors, but I might someday!
local WHT = "|cffffffff";
local BLK = "|cff000000";
local YEL = "|cffffff20";
local RED = "|cffff2020";
local GRN = "|cff20ff20";
local BLU = "|cff2020ff";
local HIL = "|cfffbd284";

--------------------------------------------------------------------------------
-- Sets up our basic options and defaults using AceConfig-3.0
--------------------------------------------------------------------------------

local options = {
	name = "Ardent Defender",
	handler = ArdentDefender,
	type = "group",
	get = function(info) return ArdentDefender.db.profile[info[#info]] end,
	set = function(info, value) ArdentDefender.db.profile[info[#info]] = value end,

	args = {
		general = {
			type = "group",
			name = "General Settings",
			cmdInline = true,
			order = 1,
			args = {
				info = {
					type = "description",
					order = 1,
					name = "Provides several useful tools to help weary Tankadin's in their travels.\nIf you are a Paladin, wear a shield, and have the Ardent Defender talent.. this addon is for you!\n\nEnjoy.\n-Blinddate, US-Thrall.\nVersion ".. VERSION,
				},
				generalheader = {
					order = 2,
					type = "header",
					name = "General Options",
				},
				nulloption = {
					order = 4,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				
				combatAnnounce = {
					type = "toggle", order = 5, 
					name = "End of Combat stats",
					desc = "Toggles displaying combat stats when exiting combat. Will only display if you took damage.",
				},
				--[[whisperOther = {
					type = "toggle", order = 9,
					name = "WhisperOther",
					desc = "Toggles whispering when you use HoP, HoSalv or LoH on other players."
				},
				
				sendAnnouncement = {
				type = "toggle", order = 5,
				name = "Death Averted",
				desc = "Toggles announcing Ardent Defender death aversions.",
				},
				trackAura = { 
					type = "toggle", order = 6,
					name = "Aura Tracking",
					desc = "Turn on/off announcement of Bubble Wall and Divine Sacrifice.",
				},
				announceChannel = {
					type= "select", order = 3,
					name = "Announcement Channel",
					desc = "Choose the channel Ardent Defender will announce to.  \r\nNote: If RW is selected, it will downgrade to Raid or Party depending on your circumstances.",
					values = { ["SAY"] = "SAY",["YELL"] = "YELL",["LOCAL"] = "LOCAL",["PARTY"] = "PARTY",["RAID"] = "RAID",["RAID_WARNING"] = "RAID_WARNING"}
				},--]]
				parryTracker = {
					type = "select", order = 6,
					name = "Parry Tracker",
					desc = "Choose setting for parry tracking. Whisper, Local only, or disabled.",
					values = { ["WHISPER"] = "WHISPER", ["LOCAL"] = "LOCAL", ["DISABLED"] = "DISABLED" },
				},
				tauntWatcher = {
					type = "toggle", order = 9,
					name = "Taunt failure tracking",
					desc = "Announces taunt failures due to miss, resist or immunities.\n",
				},
				specialFX = {
					type = "toggle", order = 7,
					name = "Special Effects",
					desc = "Toggles the use of effects such as screen flashing and sounds when AD procs.",
				},
				annoyRF = {
					type = "toggle",order = 8,
					name = "Righteous Fury",
					desc = "Flashes screen constantly if you are wearing Defense gear and do not have the RF buff.",
				},
				otherTaunt = {
					type = "toggle", order = 9,
					name = "Track Other Taunts",
					desc = "If enabled, announces to channel when someone taunts your target. If disabled, it announces it localy only.",
				},
				nulloption = {
					order = 9,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				tauntinfo = {
					order = 10.5,
					type = "description",
					name = "The channel(s) selected here will be used to announce when someone\n Taunts your current tank target, or when your Taunt misses, is resisted, or target is immune to taunt.",
				},
				tauntHeader = {
					order = 10,
					type = "header", 
					name = "Taunt Options",
				},
				tauntRaid = {
					type = "toggle", order = 11, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.taunt,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.taunt = ToggleFlag(ArdentDefender.db.profile.Flags.taunt,RAID) end,
				},
				tauntRW = {
					type = "toggle", order = 11, 
					name = "RWarning",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.taunt,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.taunt = ToggleFlag(ArdentDefender.db.profile.Flags.taunt,RAIDWARNING) end,
				},
				tauntParty = {
					type = "toggle", order = 11, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.taunt,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.taunt = ToggleFlag(ArdentDefender.db.profile.Flags.taunt,PARTY) end,
				},
				tauntLocal = {
					type = "toggle", order = 11, 
					name = "Local",
					desc = "Toggles announcing locally.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.taunt,LOCAL) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.taunt = ToggleFlag(ArdentDefender.db.profile.Flags.taunt,LOCAL) end,
				},
								
			},
		},
		messages = {
			name = "Custom Messages",
			type = "group",
			order = 2,
			args = {
				avertlheader = {
					order = 0,
					type = "header",
					name = "Avert Settings",
				},
				avertMessage = {
					type = "input", order = 1, width = "full",
					name = "Death avoided message.",
					desc = "The message to be displayed when Ardent Defender procs and your life.\n",
					usage = "Type your message. <Heal> in your message will be replaced with how much Ardent Defender healed you for, if it healed you.",
				},
				avertRaid = {
					type = "toggle", order = 1, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.avert,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.avert = ToggleFlag(ArdentDefender.db.profile.Flags.avert,RAID) end,
				},
				avertRW = {
					type = "toggle", order = 1, 
					name = "RWarning",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.avert,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.avert = ToggleFlag(ArdentDefender.db.profile.Flags.avert,RAIDWARNING) end,
				},
				avertParty = {
					type = "toggle", order = 1, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.avert,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.avert = ToggleFlag(ArdentDefender.db.profile.Flags.avert,PARTY) end,
				},
				avertLocal = {
					type = "toggle", order = 1, 
					name = "Local",
					desc = "Toggles announcing locally.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.avert,LOCAL) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.avert = ToggleFlag(ArdentDefender.db.profile.Flags.avert,LOCAL) end,
				},
				nulloption = {
					order = 1.4,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				
				Bubbleheader = {
					order = 1.5,
					type = "header",
					name = "Bubblewall settings",
				},
				bubbleMessageUp = {
					type = "input", width = "full", order = 2,
					name = "Bubblewall Active.",
					desc = "Message to be announced when your bubblewall is active.",
					usage ="Type your message.",
				},
				bubbleMessageDown = {
					type ="input", width = "full", order = 3,
					name = "Bubblewall Down",
					desc = "Message to be announced when your bubblewall is down.",
					usage = "Type your message.",
				},
				bubbleRaid = {
					type = "toggle", order = 3, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.bubble,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.bubble = ToggleFlag(ArdentDefender.db.profile.Flags.bubble,RAID) end,
				},
				bubbleRW= {
					type = "toggle", order = 3, 
					name = "RW",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.bubble,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.bubble = ToggleFlag(ArdentDefender.db.profile.Flags.bubble,RAIDWARNING) end,
				},
				bubbleParty= {
					type = "toggle", order = 3, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.bubble,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.bubble = ToggleFlag(ArdentDefender.db.profile.Flags.bubble,PARTY) end,
				},
				bubbleLocal = {
					type = "toggle", order = 3, 
					name = "Local",
					desc = "Toggles announcing locally.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.bubble,LOCAL) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.bubble = ToggleFlag(ArdentDefender.db.profile.Flags.bubble,LOCAL) end,
				},
				nulloption2 = {
					order = 3.4,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				
				DivSacheader = {
					order = 3.5,
					type = "header",
					name = "Divine Sacrifice Settings",
				},
				divsacMessageUp = {
					type ="input", width = "full",cmdHidden = true, order = 4,
					name = "Divine Sacrifice Active",
					desc = "Message to be announced when your Divine Sacrifice is used.",
					usage = "Type your message.",
				},
				divsacMessageDown = {
					type ="input", width = "full",cmdHidden = true, order = 5,
					name = "Divine Sacrifice down",
					desc = "Message to be announced when your Divine Sacrifice is down.",
					usage = "Type your message.",
				},
				divsacRaid = {
					type = "toggle", order = 5, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.divsac,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.divsac = ToggleFlag(ArdentDefender.db.profile.Flags.divsac,RAID) end,
				},
				divsacRW = {
					type = "toggle", order = 5, 
					name = "RW",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.divsac,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.divsac = ToggleFlag(ArdentDefender.db.profile.Flags.divsac,RAIDWARNING) end,
				},
				divsacParty = {
					type = "toggle", order = 5, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.divsac,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.divsac = ToggleFlag(ArdentDefender.db.profile.Flags.divsac,PARTY) end,
				},
				divsacLocal = {
					type = "toggle", order = 5, 
					name = "Local",
					desc = "Toggles announcing locally.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.divsac,LOCAL) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.divsac = ToggleFlag(ArdentDefender.db.profile.Flags.divsac,LOCAL) end,
				},
								
				avengerMessage = {
					type ="input", width = "full",order = 1,
					name = "Avenger Shield Missed",
					desc = "Message announced when your Avenger Shield misses one or more targets. ",
					usage = "Type your message.",
				},
				
			},
		},
		handmessages = {
			name = "Hand of ... messages",
			type = "group",
			order = 3,
			args = {
				handsacMessageUp = {
					type ="input", width = "full",cmdHidden = true, order = 1,
					name = "Hand of Sacrifice message active",
					desc = "Message announced when your Hand of Sacrifice is activated.",
					usage = "Type your message. <target> will be replaced with the name of the player you placed your hand on.",
				},
				handsacMessageDown = {
					type ="input", width = "full",cmdHidden = true, order = 2,
					name = "Hand of Sacrifice down",
					desc = "Message announced when your Hand of Sacrifice has ended.",
					usage = "Type your message. <you> is replaced by target name if announced to raid.",
				},
				handsacRaid = {
					type = "toggle", order = 2, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsac,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsac = ToggleFlag(ArdentDefender.db.profile.Flags.handsac,RAID) end,
				},
				handsacRW = {
					type = "toggle", order = 2, 
					name = "RW",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsac,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsac = ToggleFlag(ArdentDefender.db.profile.Flags.handsac,RAIDWARNING) end,
				},
				handsacParty = {
					type = "toggle", order = 2, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsac,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsac = ToggleFlag(ArdentDefender.db.profile.Flags.handsac,PARTY) end,
				},
				handsacLocal = {
					type = "toggle", order = 2, 
					name = "Local",
					desc = "Toggles announcing locally.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsac,LOCAL) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsac = ToggleFlag(ArdentDefender.db.profile.Flags.handsac,LOCAL) end,
				},
			--[[	handsacWhisper = {
					type = "toggle", order = 2, 
					name = "Whisper",
					desc = "Toggles announcing to Yell.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsac,WHISPER) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsac = ToggleFlag(ArdentDefender.db.profile.Flags.handsac,WHISPER) end,
				},--]]
				nulloption = {
					order = 2.4,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				
				handsalvMessage = {
					type ="input", width = "full",cmdHidden = true, order = 3,
					name = "Hand of Salvation active",
					desc = "Message WHISPERED to target when you use Hand of Salvation on them.",
					usage = "Type your message. <you> is replaced by target name if announced to raid.",
				},
				handsalvRaid = {
					type = "toggle", order = 3, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsalv,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsalv = ToggleFlag(ArdentDefender.db.profile.Flags.handsalv,RAID) end,
				},
				handsalvRW = {
					type = "toggle", order = 3, 
					name = "RW",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsalv,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsalv = ToggleFlag(ArdentDefender.db.profile.Flags.handsalv,RAIDWARNING) end,
				},
				handsalvParty = {
					type = "toggle", order = 3, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsalv,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsalv = ToggleFlag(ArdentDefender.db.profile.Flags.handsalv,PARTY) end,
				},
				handsalvWhisper= {
					type = "toggle", order = 3, 
					name = "Whisper",
					desc = "Toggles announcing to Whisper.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handsalv,WHISPER) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handsalv = ToggleFlag(ArdentDefender.db.profile.Flags.handsalv,WHISPER) end,
				},
				nulloption = {
					order = 3.4,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				handprotMessage = {
					type ="input", width = "full",cmdHidden = true, order = 4,
					name = "Hand of Protection active",
					desc = "Message WHISPERED to target when you use Hand of Protection on them.",
					usage = "Type your message. <you> is replaced by target name if announced to raid.",
				},
				handprotRaid = {
					type = "toggle", order = 4, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handprot,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handprot = ToggleFlag(ArdentDefender.db.profile.Flags.handprot,RAID) end,
				},
				handprotRW = {
					type = "toggle", order = 4, 
					name = "RW",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handprot,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handprot = ToggleFlag(ArdentDefender.db.profile.Flags.handprot,RAIDWARNING) end,
				},
				handprotParty = {
					type = "toggle", order = 4, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handprot,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handprot = ToggleFlag(ArdentDefender.db.profile.Flags.handprot,PARTY) end,
				},
				handprotWhisper = {
					type = "toggle", order = 4, 
					name = "Whisper",
					desc = "Toggles announcing to Whisper.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handprot,WHISPER) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handprot = ToggleFlag(ArdentDefender.db.profile.Flags.handprot,WHISPER) end,
				},
				nulloption = {
					order = 4.4,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				handfreeMessage = {
					type ="input", width = "full",cmdHidden = true, order = 5,
					name = "Hand of Freedom message",
					desc = "Message WHISPERED when you use Hand of Freedom on them.",
					usage = "Type your message. <you> is replaced by target name if announced to raid.",
				},
				handfreeRaid = {
					type = "toggle", order = 5, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handfree,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handfree = ToggleFlag(ArdentDefender.db.profile.Flags.handfree,RAID) end,
				},
				handfreeRW = {
					type = "toggle", order = 5, 
					name = "RW",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handfree,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handfree = ToggleFlag(ArdentDefender.db.profile.Flags.handfree,RAIDWARNING) end,
				},
				handfreeParty = {
					type = "toggle", order = 5, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handfree,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handfree = ToggleFlag(ArdentDefender.db.profile.Flags.handfree,PARTY) end,
				},
				handfreeWhisper = {
					type = "toggle", order = 5, 
					name = "Whisper",
					desc = "Toggles announcing to Whisper.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.handfree,WHISPER) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.handfree = ToggleFlag(ArdentDefender.db.profile.Flags.handfree,WHISPER) end,
				},
				nulloption = {
					order = 5.4,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				lohMessage = {
					type ="input", width = "full",cmdHidden = true, order = 6,
					name = "Lay On Hands",
					desc = "Message WHISPERED to target when you use your Lay on Hands on them.",
					usage = "Type your message. <you> is replaced by target name if announced to raid.",
				},
				lohRaid = {
					type = "toggle", order = 6, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.loh,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.loh = ToggleFlag(ArdentDefender.db.profile.Flags.loh,RAID) end,
				},
				lohRW = {
					type = "toggle", order = 6, 
					name = "RW",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.loh,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.loh = ToggleFlag(ArdentDefender.db.profile.Flags.loh,RAIDWARNING) end,
				},
				lohParty = {
					type = "toggle", order = 6, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.loh,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.loh = ToggleFlag(ArdentDefender.db.profile.Flags.loh,PARTY) end,
				},
				lohWhisper = {
					type = "toggle", order = 6, 
					name = "Whisper",
					desc = "Toggles announcing to Whisper.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.loh,WHISPER) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.loh = ToggleFlag(ArdentDefender.db.profile.Flags.loh,WHISPER) end,
				},
				nulloption = {
					order = 6.4,
					type = "description",
					name = " ",
					cmdHidden = true
				},
				diMessage = {
					type = "input", width  = "full", order = 7,
					name = "Divine Intervention",
					desc= "Message WHISPERED when you use Divine Intervention on someone.",
					usage ="Type your message",
				},diRaid = {
					type = "toggle", order = 7, 
					name = "Raid",
					desc = "Toggles announcing to raid.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.di,RAID) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.di = ToggleFlag(ArdentDefender.db.profile.Flags.di,RAID) end,
				},
				diRW = {
					type = "toggle", order = 7, 
					name = "RW",
					desc = "Toggles announcing to raidwarning.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.di,RAIDWARNING) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.di = ToggleFlag(ArdentDefender.db.profile.Flags.di,RAIDWARNING) end,
				},
				diParty = {
					type = "toggle", order = 7, 
					name = "Party",
					desc = "Toggles announcing to party.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.di,PARTY) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.di = ToggleFlag(ArdentDefender.db.profile.Flags.di,PARTY) end,
				},
				diWhisper = {
					type = "toggle", order = 7, 
					name = "Whisper",
					desc = "Toggles announcing to Whisper.",
					get = function(info) return HasFlag(ArdentDefender.db.profile.Flags.di,WHISPER) end,
					set = function(info,value) ArdentDefender.db.profile.Flags.di = ToggleFlag(ArdentDefender.db.profile.Flags.di,WHISPER) end,
				},
				
			}
		},
		
	}

}

--------------------------------------------------------------------------------
-- Standard defaults for config data.
--------------------------------------------------------------------------------
local defaults = {
    profile =  {
		avertMessage = "Death AVERTED. Ardent Defender is my hero and healed me for <heal>.", -- Basic message to be sent when we save the day
		bubbleMessageUp = "Bubblewall is ACTIVE!",
		bubbleMessageDown = "Bubblewall is down.",
		divsacMessageUp = "\124cff71d5ff\124Hspell:64205\124h[Divine Sacrifice]\124h\124r is ACTIVE!! Please Watch my health!",
		divsacMessageDown = "\124cff71d5ff\124Hspell:64205\124h[Divine Sacrifice]\124h\124r is down",
		handsacMessageUp = "\124cff71d5ff\124Hspell:6940\124h[Hand of Sacrifice]\124h\124r ACTIVE on <target>, please watch my health!!",
		handsacMessageDown = "\124cff71d5ff\124Hspell:6940\124h[Hand of Sacrifice]\124h\124r down.",
		handsalvMessage = "(Auto) I used my \124cff71d5ff\124Hspell:1038\124h[Hand of Salvation]\124h\124r on <you>.",
		handprotMessage = "(Auto) I used my \124cff71d5ff\124Hspell:10278\124h[Hand of Protection]\124h\124r on <you>!.",
		diMessage = "(Auto) I used my \124cff71d5ff\124Hspell:19753\124h[Divine Intervention]\124h\124r on <you>. Right click the buff to dispell it if you need to.",
		lohMessage = "(Auto) I used my \124cff71d5ff\124Hspell:48788\124h[Lay on Hands]\124h\124r on <you>!",
		handfreeMessage = "(Auto) I used my \124cff71d5ff\124Hspell:1044\124h[Hand of Freedom]\124h\124r on <you>!",
		avengerMessage = "Avenger shield missed!! Watch your threat!",
	
		
		combatAnnounce = false, -- Announce end of combat info in chatbox. Default off.
		tauntWatcher = true, -- Announces when your taunt target is IMMUNE, it resists, or it misses. Also watches Avenger Shield
		specialFX = true, -- Screen shaking and flashing
		annoyRF = false,
		otherTaunt = false,
		handAnnounce = false,
		parryTracker = "DISABLED",
		
		version = 1.92, -- Just incase I need to change crap in the future...
		Flags = {
			bubble = RAID,
			divsac = RAID,
			handsac = WHISPER,
			handsalv = WHISPER,
			handprot = WHISPER,
			di = WHISPER,
			loh = WHISPER,
			handfree = WHISPER,
			avenger = RAIDWARNING,
			avert = RAIDWARNING + RAID,
			taunt = RAID,
		},
	},

}


--------------------------------------------------------------------------------
-- Sets up our local variables, session and last combat data.
-- This data does not persist, resets every time you logout or reset the UI.
--------------------------------------------------------------------------------
local AD_Info = {
		adProc = 0,
		session = {
			DamageTaken = 0,
			DeathsAverted = 0,
			SpellDmgTaken = 0,
			DmgBlocked = 0,
			AmtHealed = 0,
			DmgAbsorbed = 0,
		},
		lastCombat = {
			DamageTaken = 0,
			DeathsAverted = 0,
			SpellDmgTaken = 0,
			DmgBlocked = 0,
			AmtHealed = 0,
			DmgAbsorbed = 0,
    	},
}

--------------------------------------------------------------------------------
-- OnInitialize()
-- Initialization function, sets up options table, database, slash commands, LDB Object,etc.
--------------------------------------------------------------------------------
function ArdentDefender:OnInitialize()

    -- Initialize the database including defaults.    
	self.db = LibStub("AceDB-3.0"):New("ArdentDefenderDB", defaults, "Default")

	-- Setup the options table.
    LibStub("AceConfig-3.0"):RegisterOptionsTable("ArdentDefender", options)
    self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("ArdentDefender", "ArdentDefender")

	-- Register our slash commands
    self:RegisterChatCommand("ad", "ChatCommand")
    self:RegisterChatCommand("ardentdefender", "ChatCommand")

    	-- LibDataBroker Object.Displays overall session info, ability to open config window, reset data and announce session data.
	self.ldb = LibStub:GetLibrary("LibDataBroker-1.1"):NewDataObject("Ardent Defender", {
		type = "launcher",
		label = "AD",
		OnClick = function(_, msg)
			if (msg == "LeftButton") then
    			if (IsShiftKeyDown()) then
                	local raidnum = GetNumRaidMembers()
					local partynum = GetNumPartyMembers()
					local channel = "LOCAL"

					if (raidnum > 0) then
						channel = "RAID"
					elseif (raidnum == 0 and partynum > 0) then
						channel = "PARTY"
					end
					
					if (channel == "LOCAL") then
						ArdentDefender:Message(ArdentDefender:FormatStats("SESSION",nil),channel)
					else
						ArdentDefender:FormatStats("SESSION",channel)
					end

				else
					InterfaceOptionsFrame_OpenToCategory(self.optionsFrame)
				end
			elseif (msg == "RightButton" and IsAltKeyDown()) then
				ArdentDefender:ClearData()
			end
		end,
			icon = "Interface\\Icons\\Spell_Holy_ArdentDefender",
		OnTooltipShow = function(tooltip)
			if not tooltip or not tooltip.AddLine then return end
			local adThreshold = math.floor(UnitHealthMax("player") * 0.35)
			
			local hasDS = select(5,GetTalentInfo(2,6))
			local hasAD = select(5,GetTalentInfo(2,18))
			local hasDG = select(5,GetTalentInfo(2,9))

			-- Thank you to hypehuman from the WoWHead comment section for this formula. :)
			local adHeal = math.floor(GetCombatRatingBonus(CR_DEFENSE_SKILL) * UnitHealthMax("player") * (0.3 / 140))

			tooltip:AddLine(format("%sArdent Defender|r",WHT))
			tooltip:AddLine(format("%s~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|r\n",BLU))
			
			if (hasAD == 3) then
				tooltip:AddLine(format("%sAD is active at:|r %dHP.  %sAD will heal: |r%d",WHT,adThreshold,WHT,adHeal))
			else
   			    tooltip:AddLine(format("%sArdent Defender is not fully talented (or not available at all.)|r",RED),1,0,0)
			end
			
			if ( hasDS == 1) then
				local amount = (GetNumPartyMembers() * (UnitHealthMax("player") *0.4))
				tooltip:AddLine(format("%sDivine Sacrifice absorbs:|r %d Health for each member of your party (%d total)",WHT, (UnitHealthMax("player")*0.4),amount))
				if (hasDG == 1) then
					tooltip:AddLine(format("%sDivine Sacrifice also reduces all raid member damage taken by 10%%|r",WHT))
				elseif (hasDG == 2) then
					tooltip:AddLine(format("%sDivine Sacrifice also reduces all raid member damage taken by 20%%|r",WHT))
				end
			else
			    tooltip:AddLine(format("%sDivine Sacrifice not available.",RED))
			end
			tooltip:AddLine(self:FormatStats("SESSION",nil))
			--tooltip:AddLine(format("Deaths averted: %d, AD Heals: %d", AD_Info.session.DeathsAverted, AD_Info.session.AmtHealed))
			--tooltip:AddLine(format("Dmg In: %d, Dmg Blocked: %d, Dmg Absorbed: %d",AD_Info.session.DamageTaken, AD_Info.session.DmgBlocked, AD_Info.session.DmgAbsorbed))
			
			tooltip:AddLine("|cffeda55fClick|r to show options menu.",0,1,0)
			tooltip:AddLine("|cffeda55fAlt-Right-Click|r to clear all data.",0,1,0)
			tooltip:AddLine("|cffeda55fShift-Click to report data to party/raid channel",0,1,0)
	end, })


end

--------------------------------------------------------------------------------
-- self:Debug(...)
-- Simple debug function. If Debug is enabled, prints message to player chatbox
--------------------------------------------------------------------------------
function ArdentDefender:Debug(...)
	if (debug) then
		self:Print(...)
	end
end

--------------------------------------------------------------------------------
-- OnEnable()
-- Called when Addon is loaded(?) Register our Events, etc.
--------------------------------------------------------------------------------
function ArdentDefender:OnEnable()

	enabled = true
	self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	self:RegisterEvent("PLAYER_REGEN_DISABLED") -- Entering combat
 	self:RegisterEvent("PLAYER_REGEN_ENABLED") -- Exiting combat
	self:RegisterEvent("PLAYER_ENTERING_WORLD") -- Game loaded
	if (isPTR) then -- Turns on debug by default if loaded on the PTR
		self:Print("Detected PTR. Debugging enabled")
		debug = true
	end
	
end
--------------------------------------------------------------------------------
-- OnDisable()
-- Called when the Addon is disabled, unregisters our events.
--------------------------------------------------------------------------------
function ArdentDefender:OnDisable()

	enabled = false
	self:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	self:UnregisterEvent("PLAYER_REGEN_DISABLED")
	self:UnregisterEvent("PLAYER_REGEN_ENABLED")
	self:UnregisterEvent("PLAYER_ENTERING_WORLD")
end

--------------------------------------------------------------------------------
-- Our command only serves to open the config menu at the moment. 
--------------------------------------------------------------------------------
function ArdentDefender:ChatCommand(input)
	if not input or input:trim() == "" then
		InterfaceOptionsFrame_OpenToCategory(self.optionsFrame)
	elseif (input == "standby") then
		if (enabled) then
			self:Print("Ardent Defender DISABLED. Type /ad standby to resume.")
			self:Disable()
		elseif (enabled == false) then
			self:Print("Ardent Defender ENABLED.")
			self:Enable()
		end
	else
		LibStub("AceConfigCmd-3.0").HandleCommand(ArdentDefender, "ad", "ArdentDefender", input)
	end
end

	
--------------------------------------------------------------------------------
-- self:ClearData()
-- Clears all saved data from session and combat Useful.. I guess?
--------------------------------------------------------------------------------
function ArdentDefender:ClearData()

	AD_Info.session.DamageTaken = 0
	AD_Info.session.DeathsAverted = 0
	AD_Info.session.AmtHealed = 0
	AD_Info.session.DmgBlocked = 0
	AD_Info.session.DmgAbsorbed = 0
	AD_Info.adProc = 0


	AD_Info.lastCombat.DamageTaken = 0
	AD_Info.lastCombat.DeathsAverted = 0
	AD_Info.lastCombat.AmtHealed = 0
	AD_Info.lastCombat.DmgBlocked = 0
	AD_Info.lastCombat.DmgAbsorbed = 0

	ArdentDefender:Print("All saved data cleared.")
 end


--------------------------------------------------------------------------------
-- self:COMBAT_LOG_EVENT_UNFILTERED(...)
-- Has a Godaweful amount of parameters, depending on which event is triggered. I hate this.
-- Combat log handler. Checks for Parrys, Bubblewall, and Ardent Defender Heals, Divine Sacrifice, etc etc..
--------------------------------------------------------------------------------
function ArdentDefender:COMBAT_LOG_EVENT_UNFILTERED(...)
	local eventType, sourceGUID, sourceName, sourceFlags,destGUID, destName, _, prefix1, prefix2, prefix3 = select(3,...)
	local PlayerGUID = UnitGUID("player")
	local apply = (eventType == "SPELL_AURA_APPLIED" and true or false)
		
	
	if ( eventType == "SPELL_AURA_APPLIED" or eventType == "SPELL_AURA_REMOVED") then
		if (sourceGUID == PlayerGUID and destGUID ~=PlayerGUID and ArdentSpells.Other[prefix1]) then
			self:Announce(ArdentSpells.Other[prefix1],apply,destName)
		elseif (sourceGUID == PlayerGUID and destGUID == PlayerGUID and ArdentSpells.Self[prefix1]) then
			self:Announce(ArdentSpells.Self[prefix1],apply,destName)
		--elseif (sourceGUID ~=PlayerGUID and destGUID == UnitGUID("target") and UnitGUID("targettarget") == PlayerGUID and ArdentSpells.Taunts[prefix1] and apply)  then
			--self:Announce("OTHERTAUNT",prefix2,sourceName)
		end
	elseif (eventType == "SWING_MISSED") then
		if(prefix1 == "ABSORB" and destGUID == PlayerGUID) then -- We ABSORBED an attack fully, let's record it.
			AD_Info.lastCombat.DmgAbsorbed = AD_Info.lastCombat.DmgAbsorbed + prefix2
			AD_Info.session.DmgAbsorbed = AD_Info.session.DmgAbsorbed + prefix2
			--self:Debug("DmgAbsorbed: ",prefix2)
		elseif (prefix1 == "BLOCK" and destGUID == PlayerGUID) then -- We BLOCKED an attack fully, let's record the amount.
			AD_Info.lastCombat.DmgBlocked = AD_Info.lastCombat.DmgBlocked + prefix2
			AD_Info.session.DmgBlocked = AD_Info.session.DmgBlocked + prefix2
			--self:Debug("Dmgblocked: ", prefix2)
		elseif (prefix1 == "PARRY" and ArdentDefender.db.profile.tauntWatcher) then
			if (UnitGUID("targettarget") == PlayerGUID and sourceGUID ~= PlayerGUID and destGUID == UnitGUID("target")) then -- When someone else causes the boss to parry while you are tanking
				--self:Debug("ParryOther: ",sourceName,sourceFlags,"/",bit.band(sourceFlags,COMBATLOG_OBJECT_TYPE_PLAYER))
				self:Announce("OTHERPARRY", (bit.band(sourceFlags, COMBATLOG_OBJECT_TYPE_PLAYER) ~=0),sourceName) -- the bit_band returns true or false (hopefuly) of whether it was a PLAYER or NPC who caused the parry.
			elseif (UnitGUID("targettarget") ~= PlayerGUID and sourceGUID == PlayerGUID) then -- This is when you are DPSing and cause a parry.
				self:Announce("PARRY",apply,destName)
			end
		end
	-- New: Check for Taunt and Avenger shield misses
	elseif ( eventType == "SPELL_MISSED" and sourceGUID == PlayerGUID and ArdentSpells.Self[prefix1]) then 
		local immunity = select(13,...)
		self:Announce(ArdentSpells.Self[prefix1],(immunity == "IMMUNE" and true or false),destName)
	elseif (eventType == "SPELL_CAST_SUCCESS" and sourceGUID ~=PlayerGUID and destGUID == UnitGUID("target") and UnitGUID("targettarget") == PlayerGUID and ArdentSpells.Taunts[prefix1]) then
		self:Announce("OTHERTAUNT",prefix2,sourceName)
	-- Check for Ardent Defender heal
	elseif (eventType == "SPELL_HEAL" and sourceGUID == PlayerGUID and destGUID == PlayerGUID and ArdentSpells.Self[prefix1] and ArdentSpells.Self[prefix1] == "ADHEAL") then -- We detected an Ardent Defender heal, lets record it.
		local amount = select(13,...)
		-- Seriously.. Why does LUA not have a simple increment function? Why can't this be just a ++ like in C?
		AD_Info.lastCombat.AmtHealed = AD_Info.lastCombat.AmtHealed + amount
		AD_Info.session.AmtHealed = AD_Info.session.AmtHealed + amount
		AD_Info.lastCombat.DeathsAverted = 1 + AD_Info.lastCombat.DeathsAverted
		AD_Info.session.DeathsAverted = 1 + AD_Info.session.DeathsAverted
		self:Debug("AD Heal: ", amount)
		--self:Announce("ADHEAL",UnitHealth("player"),amount) -- We check this elsewhere now.. ADHeal does not always proc if you received a large hit, instead you simply don't die and receive the debuff.
		AD_Info.adProc = amount
	
	elseif ((eventType == "SPELL_DAMAGE" or eventType == "SWING_DAMAGE" or eventType == "RANGE_DAMAGE") and destGUID == PlayerGUID) then -- Player receives spell dmg
		local dmg, overkill, school, resisted, blocked, absorbed = 0
		if (eventType == "SWING_DAMAGE" or eventType == "RANGE_DAMAGE") then
			dmg, overkill, school, resisted, blocked, absorbed = select(10,...)
		else
			dmg, overkill, school, resisted, blocked, absorbed = select(13,...)
		end

		AD_Info.lastCombat.DamageTaken = AD_Info.lastCombat.DamageTaken + dmg
		AD_Info.session.DamageTaken = AD_Info.session.DamageTaken + dmg
		--self:Debug("DMGEvent: ", eventType, "Dmg in: ", dmg)
		if (blocked and blocked > 0) then
		    AD_Info.lastCombat.DmgBlocked = AD_Info.lastCombat.DmgBlocked + blocked
		    AD_Info.session.DmgBlocked = AD_Info.session.DmgBlocked + blocked
		    --self:Debug("DMGEvent: Dmg: ",dmg, " Blocked: ", blocked)
		end
		if (absorbed and absorbed > 0) then
		    AD_Info.lastCombat.DmgAbsorbed = AD_Info.lastCombat.DmgAbsorbed + absorbed
		    AD_Info.session.DmgAbsorbed = AD_Info.session.DmgAbsorbed + absorbed
		    --self:Debug("DMGEvent: Dmg: ", dmg, " Absorbed: ", absorbed)
		end
	elseif (eventType == "SPELL_HEAL" and sourceGUID == PlayerGUID and destGUID ~= PlayerGUID and ArdentSpells.Other[prefix1] == "LOH") then
		self:Announce("LOH",true,destName)
	end
end

--------------------------------------------------------------------------------
-- self:PLAYER_REGEN_DISABLED()
-- Triggered when entering combat. Clears combat values, checks talents just incase.
--------------------------------------------------------------------------------
function ArdentDefender:PLAYER_REGEN_DISABLED()

	AD_Info.lastCombat.DamageTaken = 0
 	AD_Info.lastCombat.DeathsAverted = 0
 	AD_Info.lastCombat.AmtHealed = 0
 	AD_Info.lastCombat.DmgBlocked = 0
 	AD_Info.lastCombat.DmgAbsorbed = 0
	AD_Info.adProc = 0
end

--------------------------------------------------------------------------------
-- self:PLAYER_REGEN_ENABLED()
-- Triggered when player exits combat / loses aggro.
-- Let's generate stats screen, if enabled, and only if we took damage. (Spam otherwise)
--------------------------------------------------------------------------------
function ArdentDefender:PLAYER_REGEN_ENABLED()

	if ( AD_Info.lastCombat.DamageTaken ~= 0 and self.db.profile.combatAnnounce ) then
		self:Print(self:FormatStats("COMBAT",nil))
 	end
	
	
end

--------------------------------------------------------------------------------
-- self:PLAYER_ENTERING_WORLD()
-- Triggered when the player loads the game.
-- This just checks to make sure you are a Paladin, and disables itself if you aren't,
-- also refreshes talent check.. but do we need to?
--------------------------------------------------------------------------------
function ArdentDefender:PLAYER_ENTERING_WORLD()
	local _, class = UnitClass("player")
	
		if (class ~= "PALADIN") then
			DisableAddOn("ArdentDefender")
			self:Print("You're not a Paladin.. disabling..")
		end
		-- Version check. Resets the database back to defaults if youre version is different than the newest release. (Fixes some bugs)
	if ( not self.db.profile.version or (self.db.profile.version < defaults.profile.version)) then
		self.db:ResetDB()
		self:Print("Database outdated. Resetting defaults.")
		self:Debug("Profile reset")
	end
	if (not self.db.profile.parryTracker) then
		self.db.profile.parryTracker = "DISABLED"
	end
	
	if (self.db.profile.annoyRF and not HasRF()) then
		self:RFAnnoy()
	end
	
end

function ArdentDefender:NewMessage(msg,flags,target)

	local str = ">>> "..msg.." <<<"
	self:Debug("NewMessage: Flags: "..flags)
	
	local sentlocal = false
	local isRaid = (GetNumRaidMembers() >0)
	local isParty = ((GetNumPartyMembers() >0) and not isRaid)
	
	if (flags == -1) then
		self:Print(msg)
		return
	end
	
	if (HasFlag(flags,RAID)) then
		if (isRaid) then
			SendChatMessage(str,"RAID")
		else
			self:Print("You are not in a RAID. "..msg)
			sentlocal = true
		end
	end
	
	if (HasFlag(flags,LOCAL)) then
		self:Print(str)
		sentlocal = true
		--SendChatMessage(str,"LOCAL")
	end
	
	--if (HasFlag(flags,WHISPER) and target ~=nil) then
	--	SendChatMessage(str,"WHISPER", nil,target)
	--end
	
	if (HasFlag(flags,RAIDWARNING)) then
		if ((isRaid and IsRaidOfficer())) then
			SendChatMessage(str,"RAID_WARNING")
		else
			if ( not sentlocal ) then
				sentlocal = true
				self:Print("You are not in a RAID. "..msg)
			end
		end
	end
	
	if (HasFlag(flags,SAY)) then
		SendChatMessage(str, "SAY")
	end
	if (HasFlag(flags,PARTY)) then
		if (isParty) then
			SendChatMessage(str,"PARTY")
		else
			if (not sentlocal) then
				sentlocal = true
				self:Print("You are not in a PARTY. "..msg)
			end
		end
	end
end


--------------------------------------------------------------------------------
-- Simple function to add our AD Tag and send it to a channel
--------------------------------------------------------------------------------
function ArdentDefender:Message(msg,channel)

	if (channel == "LOCAL") then
		self:Print(msg)
	else 
		local str = ">>> "..msg.." <<<"
		SendChatMessage(str,channel)
		self:Debug("Message: Called with arguments: ",str, " ",channel)
	end
end
--------------------------------------------------------------------------------
-- Formats either Session or last Combat info into a standard string for showing
-- returns a string
--------------------------------------------------------------------------------
function ArdentDefender:FormatStats(type,channel)
	local str, i = nil
	local totalDmg = 0
    local prewrapper, endwrapper, Averted, Absorbed, Blocked, Healed
	if (type == "SESSION") then
		i = AD_Info.session
	else
		i = AD_Info.lastCombat
	end
	
	if (channel == nil) then  -- This is such an ugly kludge, but we can't sent color or multiline messages to chat channels.
		prewrapper = BLU.."\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|r\n"
		endwrapper = BLU.."\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|r"
		Averted, Absorbed, Blocked, Healed = RED.."Zero|r",RED.."Zero|r",RED.."Zero|r",RED.."Zero|r"
	else 
		Averted, Absorbed, Blocked, Healed = "Zero","Zero","Zero","Zero"
	end
	
	totalDmg = i.DmgAbsorbed + i.DmgBlocked
	if (i.DeathsAverted > 0) then
		Averted = i.DeathsAverted
	end
	if (i.DmgAbsorbed >0) then
		Absorbed = i.DmgAbsorbed
	end
	if (i.DmgBlocked > 0) then
		Blocked = i.DmgBlocked
	end
	if (i.AmtHealed > 0) then
		Healed = i.AmtHealed
	end
	if (channel == nil) then
		str = format ("%s%sDeaths Averted: |r%s\n%sBlocked:|r %s|r    %sAbsorbed: |r%s\n",prewrapper,GRN,Averted,GRN,Blocked,GRN,Absorbed )
		str = str..format("%sArdent Heals:|r %s\n%sDmg Mitigated:|r %d\n%sDmg Taken: |r%d%s",GRN,Healed,GRN,totalDmg,GRN,i.DamageTaken,endwrapper)
		return str
	else
		ArdentDefender:Message("Ardent Defender statistics",channel)
		ArdentDefender:Message(format("Deaths Averted: %s, Blocked: %s, Absorbed: %s",Averted,Blocked,Absorbed),channel)
		ArdentDefender:Message(format("Ardent Heals: %s, Dmg Mitigated: %d, Dmg Taken: %d", Healed,totalDmg,i.DamageTaken),channel)
		return
	end
end

--------------------------------------------------------------------------------
-- Decides on a channel depending on whether a player is in a Party or Raid
--------------------------------------------------------------------------------
function ArdentDefender:Channel()

	local channel = self.db.profile.announceChannel 
 	local isRaid = (GetNumRaidMembers() >0)
	local isParty = ((GetNumPartyMembers() >0) and not isRaid)
	
	self:Debug("Channel(): isRaid: ",isRaid," isParty: ", isParty)
	
	if (channel == "LOCAL" or (not isRaid and not isParty)) then -- Easy as pie! Lets skip all that raid crap if we're alone, or we choose to be alone.
		return "LOCAL"
	end
	
	if (channel == "PARTY") then --Send to PARTY channel unless we're actually in a raid, ifso, upgrade to raid.
	    if isRaid then
			return "RAID"
		else
			return "PARTY"
		end
		
	elseif (channel == "YELL" or channel == "SAY") then -- We don't need checking if we are sending to YELL or SAY
		return channel

	elseif (channel == "RAID") then -- Send to RAID Channel, but downgrades to Party if we're not in a raid.

		if (isRaid) then -- We're in a RAID.
			return "RAID"
		else  -- We're in a party, put it there instead of RAID.
			return "PARTY"
		end
	elseif (channel == "RAID_WARNING") then -- We want RAID_WARNING, only works if you are an assistant in a raid or in a normal party. Lets figure out which.
		if ((isRaid and IsRaidOfficer())) then
			return "RAID_WARNING"
		elseif (isRaid and not IsRaidOfficer()) then
			return "RAID"
		else
			return "PARTY"
		end
	else
		self:Debug("Channel(): Bad argument: ", channel)
		return nil
	end
end
--------------------------------------------------------------------------------
-- Basic announce function for all events we need to announce
--------------------------------------------------------------------------------
function ArdentDefender:Announce(type,event,special)

	local msg = nil
	local status = (event) and "UP!!!" or "DOWN!!!"
	local Flags = nil
	local pvp = false
	self:Debug("Announce() called with arguments: ", type or "nil", "/",event or "nil", "-",special or "nil", " -- ", channel)
	
	-- Quick and dirty hack to prevent announcements if we are in a BG.
	SetMapToCurrentZone()
	_, instance = IsInInstance()
	if (instance == "pvp" ) then
		pvp = true
	end
	
	if (type == "AVERT" and event) then
		local adheal = AD_Info.adProc or 0
		
		if (adheal> 0 ) then
			AD_Info.adProc = 0
		end
			
		msg = string.gsub(self.db.profile.avertMessage,"<heal>",adheal)
		Flags = self.db.profile.Flags.avert
		if ( self.db.profile.specialFX) then
			PlaySoundFile(AD_SOUND)
			self:Flash()
			self:Shake()
		end
	elseif (type == "AVENGERMISS" and not event) then
		msg = self.db.profile.avengerMessage
		Flags = self.db.profile.Flags.avert
	elseif (type == "BUBBLEWALL" ) then
		if (event) then
			msg = self.db.profile.bubbleMessageUp
		else
			msg = self.db.profile.bubbleMessageDown
		end
		Flags = self.db.profile.Flags.bubble
	elseif (type == "TAUNTMISS" and self.db.profile.tauntWatcher) then
		if (event) then
			msg = "Target is IMMUNE to taunt!"
		else
			msg = "Taunt resisted!"
		end
		Flags = self.db.profile.Flags.taunt
--	elseif (type == "PARRY" and self.db.profile.parryTracker ~="DISABLED") then -- Boss has parried OUR attack
--		msg = "Target has PARRIED your attack."
--		Flags = -1
	elseif (type == "OTHERPARRY" and event == true) then -- Boss parried someone else's attack.. why are they standing in front??
		if (self.db.profile.parryTracker == "WHISPER" and event == true) then  -- If enabled, lets whisper them
			msg = "(AutoMessage) You caused my target to PARRY, stand behind it!"
			SendChatMessage(msg, "WHISPER", nil, special)
			return
		elseif (self.db.profile.parryTracker == "LOCAL") then      -- Otherwise we print to local
			msg = special.." has caused your target to PARRY!!"
			Flags = -1
		end
	elseif (type == "DIVSAC" ) then -- Divine Sacrifice
	    if (event) then
			msg = self.db.profile.divsacMessageUp
		else
			msg = self.db.profile.divsacMessageDown
		end
		Flags = self.db.profile.Flags.divsac
	elseif (type == "HANDSAC" ) then
	    if (event) then
			msg = string.gsub(self.db.profile.handsacMessageUp,"<target>",special)
		else
			msg = self.db.profile.handsacMessageDown
		end
		Flags = self.db.profile.Flags.handsac
	elseif (type == "SALV" and event ) then
		if(HasFlag(self.db.profile.Flags.handsalv,WHISPER)) then
			SendChatMessage(self.db.profile.handsalvMessage,"WHISPER",nil,special)
		end
		msg = string.gsub(self.db.profile.handsalvMessage, "<you>", special)
		Flags = self.db.profile.Flags.handsalv
	elseif (type == "HOP" and event ) then
		if (HasFlag(self.db.profile.Flags.handprot,WHISPER)) then
			SendChatMessage(self.db.profile.handprotMessage, "WHISPER",nil,special)
		end
		msg = string.gsub(self.db.profile.handprotMessage,"<you>", special)
		Flags = self.db.profile.Flags.handprot
	elseif (type == "DI" and event) then
		if (HasFlag(self.db.profile.Flags.di,WHISPER)) then
			SendChatMessage(self.db.profile.diMessage,"WHISPER",nil,special)
		end
		msg = string.gsub(self.db.profile.diMessage,"<you>",special)
		Flags = self.db.profile.Flags.di
	elseif (type == "LOH" and event) then
		if (HasFlag(self.db.profile.Flags.loh,WHISPER)) then
			SendChatMessage(self.db.profile.lohMessage, "WHISPER",nil,special)
		end
		msg = string.gsub(self.db.profile.lohMessage, "<you>", special)
		Flags = self.db.profile.Flags.loh
	elseif (type == "FREEDOM" and event) then
		if (HasFlag(self.db.profile.Flags.handfree,WHISPER)) then
			SendChatMessage(self.db.profile.handfreeMessage, "WHISPER", nil, special)
		end
		msg = string.gsub(self.db.profile.handfreeMessage, "<you>", special)
		Flags = self.db.profile.Flags.handfree
	elseif (type == "RFHACK" and self.db.profile.annoyRF and not HasRF()) then
		self:RFAnnoy()
	elseif (type == "OTHERTAUNT" ) then
		if (self.db.profile.otherTaunt== false) then
			Flags = -1
			msg = special.." has TAUNTED your target with "..event
		else
			Flags = self.db.profile.Flags.taunt
			msg = special.." has TAUNTED my target with "..event
		end
		
	else
	    --self:Debug("Announce() invalid argument! "..type)
	    msg = nil
	end
	
	if (msg and not pvp) then
		self:NewMessage(msg,Flags,special)
	elseif (msg and pvp) then
		self:NewMessage(msg,-1,special)
	end
end

-- Creates a frame, attaches our texture and flashes it, this is adapted from the Omen addon
function ArdentDefender:Flash()
	if not self.FlashFrame then
		local flasher = CreateFrame("Frame", "ADFlashFrame")
		flasher:SetToplevel(true)
		flasher:SetFrameStrata("FULLSCREEN_DIALOG")
		flasher:SetAllPoints(UIParent)
		flasher:EnableMouse(false)
		flasher:Hide()
		flasher.texture = flasher:CreateTexture(nil, "BACKGROUND")
		flasher.texture:SetTexture(AD_FLASHTEXTURE)
		flasher.texture:SetAllPoints(UIParent)
		flasher.texture:SetBlendMode("ADD")
		flasher:SetScript("OnShow", function(self)
			self.elapsed = 0
			self:SetAlpha(0)
		end)
		flasher:SetScript("OnUpdate", function(self, elapsed)
			elapsed = self.elapsed + elapsed
			if elapsed < 2.6 then
				local alpha = elapsed % 1.3
				if alpha < 0.15 then
					self:SetAlpha(alpha / 0.15)
				elseif alpha < 0.9 then
					self:SetAlpha(1 - (alpha - 0.15) / 0.6)
				else
					self:SetAlpha(0)
				end
			else
				self:Hide()
			end
			self.elapsed = elapsed
		end)
		self.FlashFrame = flasher
	end

	self.FlashFrame:Show()
end

-- This was adapted from Omen3 (with permission), their original comments are below. --Blind
-- This function is adapted from Omen2 to be self-contained,
-- which was initially taken from BigWigs
function ArdentDefender:Shake()
	local shaker = self.ShakerFrame
	if not shaker then
		shaker = CreateFrame("Frame", "ADShaker", UIParent)
		shaker:Hide()
		shaker:SetScript("OnUpdate", function(self, elapsed)
			elapsed = self.elapsed + elapsed
			local x, y = 0, 0 -- Resets to original position if we're supposed to stop.
			if elapsed >= 0.8 then
				self:Hide()
			else
				x, y = random(-8, 8), random(-8, 8)
			end
			if (WorldFrame:IsProtected() and InCombatLockdown()) then
				if not shaker.fail then
					ArdentDefender:Message("Cannot shake world frame if you have had nameplates turned on at least once since logging in.","LOCAL")
					shaker.fail = true
				end
				self:Hide()
			else
				WorldFrame:ClearAllPoints()
				for i = 1, #self.originalPoints do
					local v = self.originalPoints[i]
					WorldFrame:SetPoint(v[1], v[2], v[3], v[4] + x, v[5] + y)
				end
			end
			self.elapsed = elapsed
		end)
		shaker:SetScript("OnShow", function(self)
			-- Store old worldframe positions, we need them all, people have frame modifiers for it
			if not self.originalPoints then
				self.originalPoints = {}
				for i = 1, WorldFrame:GetNumPoints() do
					tinsert(self.originalPoints, {WorldFrame:GetPoint(i)})
				end
			end
			self.elapsed = 0
		end)
		self.ShakerFrame = shaker
	end

	shaker:Show()
end
function HasRF()
	local n = UnitAura("player","Righteous Fury")
	local d = GetCombatRatingBonus(CR_DEFENSE_SKILL)
	
	if (d > 100 and not n) then
		return false
	else
		return true
	end
end
function ArdentDefender:RFAnnoy()
	if not self.RFFrame then
		local frame = CreateFrame("Frame", "ADAnnoyFrame")
		frame:SetToplevel(true)
		frame:SetFrameStrata("FULLSCREEN_DIALOG")
		frame:SetAllPoints(UIParent)
		frame:EnableMouse(false)
		frame:Hide()
		frame.texture = frame:CreateTexture(nil, "BACKGROUND")
		frame.texture:SetTexture("Interface\\FullScreenTextures\\LowHealth")
		frame.texture:SetAllPoints(UIParent)
		frame.texture:SetBlendMode("ADD")
		frame:SetScript("OnShow", function(self)
			self.elapsed = 0
			self:SetAlpha(0)
		end)
		frame:SetScript("OnUpdate", function(self, elapsed)
			elapsed = self.elapsed + elapsed
			if (HasRF() or not ArdentDefender.db.profile.annoyRF) then
				self:Hide()
			end
			if elapsed < 2.6 then
				local alpha = elapsed % 1.3
				if alpha < 0.15 then
					self:SetAlpha(alpha / 0.15)
				elseif alpha < 0.9 then
					self:SetAlpha(1 - (alpha - 0.15) / 0.6)
				else
					self:SetAlpha(0)
				end
			else
				elapsed = 0
				
			end
			self.elapsed = elapsed
		end)
		self.RFFrame = frame
	end

	self.RFFrame:Show()
end