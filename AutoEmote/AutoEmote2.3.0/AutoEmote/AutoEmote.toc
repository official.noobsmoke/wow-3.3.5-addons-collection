## Interface: 30300
## Title: Blue's AutoEmote
## Notes: Automatically returns an emote that is directed at you.
## Author: Blueleaf of Scarlet Crusade (Base Code by Gearshaft)
## Version:: v3.03
## SavedVariablesPerCharacter: AutoEmoteOptions
AutoEmote.xml
