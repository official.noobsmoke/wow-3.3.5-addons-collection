-- Version : English (by Gearshaft)
-- Last Update : 12/18/2006

-- <= == == == == == == == == == == == == =>
-- => Bindings
-- <= == == == == == == == == == == == == =>



-- <= == == == == == == == == == == == == =>
-- => Frame Variables
-- <= == == == == == == == == == == == == =>
AUTOEMOTEOPTIONSFRAME_TITLE			= "Blues AutoEmote Options";
AUTOEMOTEOPTIONSFRAME_SUBTITLE		= "Other Emote Opions";
AEP_EMOTELIST_TITLE					= "Emote List";

AEOF_SEARCHBOXLABEL					= "Search String";
AEOF_EMOTEBOXLABEL			 		= "Response Emote";

AE_TIMEINTERVAL						= "sec";

AEOF_COOLDOWNTEXT					= "Cooldown:";
AEOF_DELAYTEXT						= "Delay:";

AEFO_ADDEMOTEBUTTON_TOOLTIP			= "Add a new incoming emote search string.";
AEFO_REMOVEEMOTEBUTTON_TOOLTIP		= "Remove current emote.";
AEFO_RESETBUTTON_TOOLTIP			= "Reset to Defaults";
AEFO_DROPDOWN_TOOLTIP				= "Choose an incomming emote to edit it and its response.\nItems beginning in AE_ are reserved emote tags.";


-- <= == == == == == == == == == == == == =>
-- => Misc Interface
-- <= == == == == == == == == == == == == =>
AUTOEMOTE_ONLOAD_TEXT				= "AutoEmote version |c0000ff00%s|r loaded. '/AutoEmote help' or '/ae ?' for help."
AUTOEMOTE_SELF1_TEXT				= " you"
AUTOEMOTE_SELF2_TEXT				= " you"
AUTOEMOTE_SECONDS_TEXT				= "seconds"
AUTOEMOTE_STATUS_TEXT				= "AutoEmote status: %c%s|r"
AUTOEMOTE_ABOUT_TEXT				= AUTOEMOTE_STATUS_TEXT .. " (change with /ae on | off)"
AUTOEMOTE_COOLDOWN_INFO_TEXT			= "AutoEmote Cooldown set to: %c%s|r " .. AUTOEMOTE_SECONDS_TEXT .. "."
AUTOEMOTE_COOLDOWN_STATUS_TEXT		= "AutoEmote Cooldown: %c%s|r " .. AUTOEMOTE_SECONDS_TEXT .. ". "
AUTOEMOTE_SECONDSREMAINING_TEXT		= AUTOEMOTE_SECONDS_TEXT .. " remaining."
AUTOEMOTE_DELAY_INFO_TEXT			= "AutoEmote Response Delay set to: %c%s|r " .. AUTOEMOTE_SECONDS_TEXT .. "."
AUTOEMOTE_DELAY_STATUS_TEXT			= "AutoEmote Response Delay: %c%s|r " .. AUTOEMOTE_SECONDS_TEXT .. "."
AUTOEMOTE_UNKNOWN_TEXT				= "AutoEmote: unknown command: %c%s|r"

AUTOEMOTE_HELP_TEXT				= -- Continues on the next few lines...
"AutoEmote Help: \n"..
"/ae -  Shows this menu of AutoEmote.\n"..
"/ae status -  Shows the status of AutoEmote.\n"..
"/ae on | off : %c%s|r -  Turns the addon on and off.\n"..
"/ae options -  %cShow or Hide the Options window" .. "|r. \n"..
"/ae cooldown ## : %c%s|r seconds. -  Sets the automated response inactivity period where no automated responses will be sent.\n"..
"/ae delay ## : %c%s|r seconds. -  Sets how long your responses will be delayed after an incoming emote is sent to you (Requires Chronos Addon)."

AE_SLASHCMD_ON						= "on"
AE_SLASHCMD_OFF						= "off"
AE_SLASHCMD_OPTIONS					= "options"
AE_SLASHCMD_COOLDOWN				= "cooldown"
AE_SLASHCMD_DELAY					= "delay"
AE_SLASHCMD_HELP					= "help"


-- <= == == == == == == == == == == == == =>
-- => Default Emotes
-- <= == == == == == == == == == == == == =>
AE_EmotesDefault = {
				["cheers"] = "ROAR",
				["point"] = "BLINK",
				["lick"] = "MOAN",
				["giggles"] = "SMILE",
				["flex"] = "CRACK",
				["tickle"] = "GIGGLE",
				["bite"] = "GROWL",
				["cuddle"] = "WINK",
				["dance"] = "DANCE",
				["spit"] = "RASP",
				["AE_Default"] = "WAVE",
				["bored"] = "SHRUG",
				["laugh"] = "SMILE",
				["nods"] = "AGREE",
				["drink"] = "CHEER",
				["whistles"] = "FLEX",
				["cries"] = "COMFORT",
				["motherless"] = "CRACK",
				["poke"] = "GIGGLE",
				["slap"] = "GROWL",
				["sexy"] = "NOSEPICK",
				["pat"] = "SMILE",
				["fart"] = "SNUB",
				["tap"] = "SLAP",
				["moo"] = "SOOTHE",
				["loves"] = "BLUSH",
				["bonk"] = "THREATEN",
				["rude"] = "FROWN",
			}
