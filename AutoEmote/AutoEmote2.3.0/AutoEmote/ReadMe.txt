AutoEmote v. 2.3
  By Gearshaft of Zul'jin
  January 18, 2007

http://www.Orgrimmar.org/


About

 Updated/Modified for 2.4 by Ture, Aggramar
AutoEmote will automatically issue emotes back to the people who emote to you. It has a built-in cooldown period so it won't issue an automated response before the cooldown period is over. The cooldown is triggered when ever your character does a manual (one that you type in) or automated response emote. This lets you interact with others via emotes and not have the auto responses interrupting until the cooldown period is over.
It also offers delayed response functionality. The delay makes automated responses wait for a few seconds before firing off. This gives the impression that you are typing out the response, instead of an automated response.
When someone emotes to you an emotethat isn't on your auto-response list a default response back in given, which you can set to be whatever you like.

AutoEmote offers a UI interface for users who would like to edit their settings like: the cooldown poriod, response delay time or emotes; and even add new emotes to their response list. 


Changes

2.3
    - Updated the UI Version to 2.3

2.22
    - Updated to the latest WoW UI Version

2.21
    - Fixed a bug for when you would type /ae cooldown to check the remaining cooldown time.
    - Fixed a bug that caused a an error message when returning an emote. 

2.2
    - Added a German localization. Yay for Germany! (Thx DownLord)
    - AutoEmote now natively handles the delay option. Now everyone can enjoy the realistic simulation of delayed responses. (Removed the optional dependancy "Chronos")
    - Rewrote localization code completely.

2.1
    - Fixed a major bug with the options window not displaying any emotes on the emote list.

2.0
    - Added support for the Burning Crusade Expansion.
    - Removed the AE_Guild message which acts as the fallback response if you don't have any custom emote responses for when a guild member emotes to you due to an information gathering limitation in WoW 2.0
    - Updated all table refrences to Lua 5.1.1 standard.

1.0
    - Added an Add-Emote button to make the interface more intuitive
    - Emote List is now sorted properly (Finally)
    - Emotes are now accessible via a fancy new scrolling list
    - Resolved the emote limit of 32
    - Updated UI Version to 1.12

0.48
    - Set more intuitive /command help.

0.47
    - Fixed a couple of things
    - Updated the UI Version to 1.10

0.46
    - Changed the variable storage method from custom per-character to Blizzard's method.

0.45
    - Added a save button for more intuitive use.
    - Added more emotes to default set.
    - Created a companion (external) addon that will give you a minimap button to access the options window.
    - Removed the developer button to reload the UI.

0.41
    - Fixed a bug causing an error on start-up of /ae options that made the dropdown and other functions to not work as intended.

0.4
    - A reset button to restore default settings.
    - Ability to add, remove, and change emotes and their responses.
    - Built a Graphical frontend User Interface. Lets you adjust all current settings and new ones. accessable with /ae options
    - Cooldown and Delay are now setable with a slider bar.
    - Updated help command. Now shows all user settings, what tehy do and what they are set to. use /ae help or /ae ?

0.3
    - Added a delay time that is user setable so your auto emotes are delayed by X amount of seconds.
    - Added an actual version number so you can see if your AE is hella-old.
    - Added more custom responses so you don't look foolish saluting someone who just farted on you.
    - Rewrote the emotes so they are stored in a table.

0.2
    - Added /commands and on/off crap
    - Created custom responses to some emotes

0.1
    - First release, Auto responded with wave to everyone, and salute to people just in your guild. Added custom emote responses to specific emotes at you.

