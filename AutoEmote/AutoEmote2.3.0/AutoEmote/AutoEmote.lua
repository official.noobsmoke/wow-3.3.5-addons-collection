--## Updated/Modified for 3.3 by Blueleaf--
--## Updated/Modified for 2.4 by Ture, Aggramar--
local version = "3.3"
local AE_EmoteArray = {}
local colorCode = "|c0000ff00"
local LastEmoteTime = 0
-- local AutoEmoteOptions.SelectedEmote
AutoEmote_UpdateInterval = 1
AutoEmoteLoaded = false
AutoEmoteOptions = {}
AutoEmoteEmoteArray = {}

local function InitializeSetup()
	if(AutoEmoteOptions == nil)						then AutoEmoteOptions = {} end
	if(AutoEmoteOptions.Status == nil)				then AutoEmoteOptions.Status = "On" end
	if(AutoEmoteOptions.Cooldown == nil)			then AutoEmoteOptions.Cooldown = "40" end
	if(AutoEmoteOptions.Delay == nil)				then AutoEmoteOptions.Delay = "3" end
	if(AutoEmoteOptions.Emotes == nil)				then AutoEmoteOptions.Emotes = AE_EmotesDefault end
	-- if(AutoEmoteOptions.QueuedEmotes == nil)		then AutoEmoteOptions.QueuedEmotes = {} end
	-- if(AutoEmoteOptions.SelectedEmote == nil)	then AutoEmoteOptions.SelectedEmote = "AE_Default" end
	                                              
	AutoEmoteOptions.QueuedEmotes = {}
	AutoEmoteOptions.SelectedEmote = "AE_Default"	
	AutoEmoteEmoteArray = {}
end

local function printf(str, ...)
	--local Args = ...
	--local str = Args[0]
	--table.remove(Args,0)
	str = string.gsub(str, "%%c", colorCode)
	DEFAULT_CHAT_FRAME:AddMessage(	string.gsub(str, "%%s", ...) )

	
	--for i = 1, select( "#", ... ) do
	--	say( tostring( select( i, ... ) ) )
	--end
end

local function TableSize(table)
	-- Aparently some yay-who decided that it would be cool to get rid of the automatic table size counter... GFG nub... So I have to do it this way.
	-- local size = #table
	local size = 0
	for key, value in pairs( table ) do
		size = size + 1
	end
	return size
end

function AutoEmote_OnLoad()
	this.TimeSinceLastUpdate = 0
	
	this:RegisterEvent("VARIABLES_LOADED")
	this:RegisterEvent("CHAT_MSG_TEXT_EMOTE")
	-- this:RegisterEvent("CHAT_MSG_EMOTE")
	
	InitializeSetup()
	
	SlashCmdList["AutoEmote"] = AE_SlashCommmando
	SLASH_AutoEmote1 = "/AutoEmote"
	SLASH_AutoEmote2 = "/ae"
	
	AutoEmoteLoaded = 1
	printf(AUTOEMOTE_ONLOAD_TEXT, version)
end

function AutoEmote_OnEvent(event)
	if ( event == "VARIABLES_LOADED" ) then
		this:UnregisterEvent(event)
		InitializeSetup()
		return
	elseif(event == "CHAT_MSG_TEXT_EMOTE") then -- or event == "CHAT_MSG_EMOTE") then
		if(AutoEmoteOptions.Status == "On") then
			local fullemote = arg1
			local who = arg2
			if ( who ~= UnitName("player") ) then 
				-- if ( string.find(fullemote, UnitName("player")) and event == "CHAT_MSG_EMOTE") then
				-- 	AutoEmote_Reply(who,fullemote)
				-- else
				if ( string.find(fullemote, UnitName("player")) or string.find(fullemote, AUTOEMOTE_SELF1_TEXT) or string.find(fullemote, AUTOEMOTE_SELF2_TEXT)) then
					--if (Chronos) then
						--Chronos.schedule(,AutoEmote_Reply,who,fullemote)
					--else
						--AutoEmote_Reply(who,fullemote)
					--end
					AutoEmote_QueueEmote(AutoEmoteOptions.Delay,who,fullemote)
				end
			else
				LastEmoteTime = time()
			end
		end
	end
end

function AE_SlashCommmando(msg)
	if(msg ~= "") then msg = string.lower(msg) end
	local f,u, cmd, param = string.find(msg, "^([^ ]+) (.+)$")
	if ( not cmd ) then
		cmd = msg
		param = ""
	end
	if(cmd == "status") then
		printf(AUTOEMOTE_ABOUT_TEXT, AutoEmoteOptions.Status)
	elseif(cmd == AE_SLASHCMD_ON) then
		AutoEmoteOptions.Status = "On"
		printf(AUTOEMOTE_STATUS_TEXT, AutoEmoteOptions.Status)
		this:RegisterEvent("CHAT_MSG_TEXT_EMOTE")
	elseif(cmd == AE_SLASHCMD_OFF) then
		AutoEmoteOptions.Status = "Off"
		printf(AUTOEMOTE_STATUS_TEXT, AutoEmoteOptions.Status)
		this:UnregisterEvent("CHAT_MSG_TEXT_EMOTE")
	elseif(cmd == AE_SLASHCMD_OPTIONS) then
		if ( AutoEmoteOptionsFrame:IsVisible() ) then
			AutoEmoteOptionsFrame:Hide()
		else
			AutoEmoteOptionsFrame:Show()
		end
	elseif(cmd == AE_SLASHCMD_COOLDOWN or cmd == "cool") then
		if ( string.find(param,"%d+") ) then
			AutoEmoteOptions.Cooldown = param
			printf(AUTOEMOTE_COOLDOWN_INFO_TEXT, AutoEmoteOptions.Cooldown)
		else
			local remaining = ""
			if (time() < LastEmoteTime + AutoEmoteOptions.Cooldown) then
				remaining = "%c" .. LastEmoteTime + AutoEmoteOptions.Cooldown - time() .. "|r " .. AUTOEMOTE_SECONDSREMAINING_TEXT .. " "
			end
			printf(AUTOEMOTE_COOLDOWN_STATUS_TEXT .. remaining, AutoEmoteOptions.Cooldown)
		end
	elseif(cmd == AE_SLASHCMD_DELAY) then
		if ( string.find(param,"%d+") ) then
			AutoEmoteOptions.Delay = param
			printf(AUTOEMOTE_DELAY_INFO_TEXT, AutoEmoteOptions.Delay)
		else
			printf(AUTOEMOTE_DELAY_STATUS_TEXT)
		end
	elseif(cmd == "" or cmd == AE_SLASHCMD_HELP or cmd == "?") then
		printf(AUTOEMOTE_HELP_TEXT, AutoEmoteOptions.Status, AutoEmoteOptions.Cooldown, AutoEmoteOptions.Delay)
	else 
		printf(AUTOEMOTE_UNKNOWN_TEXT, cmd)
	end
end

function AutoEmote_QueueEmote(queuedtime,who,emotetext)
	local NewQueue = {
		["time"] = time() + queuedtime,
		["who"] = who,
		["emote"] = emotetext,
	}
	
	table.insert(AutoEmoteOptions.QueuedEmotes, NewQueue)
end

function AutoEmote_DelayCheck(self, elapsed)
	-- /script AutoEmote_QueueEmote(AutoEmoteOptions.Delay,"Ruzan","wave")
	this.TimeSinceLastUpdate = this.TimeSinceLastUpdate + elapsed 	
	
	while (this.TimeSinceLastUpdate > AutoEmote_UpdateInterval) do
		for key, value in pairs( AutoEmoteOptions.QueuedEmotes ) do
			if (value.time <= time()) then
				value = AutoEmote_Reply(value)
				table.remove(AutoEmoteOptions.QueuedEmotes,key)
			end
		end
		this.TimeSinceLastUpdate = this.TimeSinceLastUpdate - AutoEmote_UpdateInterval
	end
	return
end

function AutoEmote_Reply(EmoteData)
	local myresponse

	if (time() > LastEmoteTime + AutoEmoteOptions.Cooldown) then
		-- The following code cannot be used until a method for discovering if the playername "who" is in the same guild as you.
		--[[TargetByName(who)
		local GuildTarget = {GetGuildInfo("target")}
		local GuildPlayer = {GetGuildInfo("player")}

		-- Guild Member Return Emote
		if ( GuildTarget[1] == GuildPlayer[1] and IsInGuild() ) then
			emote = AutoEmoteOptions.Emotes["AE_Guild"]
		-- All Other
		else
			emote = AutoEmoteOptions.Emotes["AE_Default"]
		end]]
		myresponse = AutoEmoteOptions.Emotes["AE_Default"]
		-- Specific Emote Returns
		for key, response in pairs( AutoEmoteOptions.Emotes ) do
			if ( string.find(EmoteData.emote, key) ~= nil ) then
				myresponse = string.upper(response)
			end
		end
		
		DoEmote(myresponse,EmoteData.who)
		--[[TargetLastTarget()]]
	end
	LastEmoteTime = time()
end

-- The following was taken right out of Sea. I only modified it a little.

-- copy( table [, recursionList ]  )
-- 	Copies the values of a table
-- 	If you use tables as keys, it will probably fail. 
-- 	
-- args:
-- 	table - the table you want copied
-- 	recursionList - don't use this
-- 
-- returns:
-- 	table - the table containing the copy of the values
-- 	
local function tablecopy( t, recursionList ) 
	if ( not recursionList ) then recursionList = {} end
	if ( type(t) ~= "table" ) then 
		return t
	end

	local newTable = {}
	if ( recursionList[t] ) then
		return recursionList[t]
	else
		recursionList[t] = newTable
		for k, v in pairs ( t ) do
			-- If it's a table we want to recurse.  But the second half of this if checks to see if it
			-- is a reference to a frame, which looks like a table, and does a normal copy in such a
			-- case
			if ( ( type(v) == "table" ) and not ( v[0] and ( type(v[0]) == "userdata" ) ) ) then 
				newTable[k] = tablecopy(v, recursionList)
			else
				newTable[k] = v
			end
		end

		return newTable
	end
end

-- ### UI Code ### --
function AutoEmoteOptionsFrame_OnLoad()
	this:RegisterForDrag("LeftButton")
end

function AutoEmoteOptionsFrame_OnDragStart()
	AutoEmoteOptionsFrame:StartMoving()
end

function AutoEmoteOptionsFrame_OnDragStop()
	AutoEmoteOptionsFrame:StopMovingOrSizing()
end

function AutoEmoteOptionsFrameCooldownSlider_OnShow()
	AutoEmoteOptionsFrameCooldownSlider:SetValue(AutoEmoteOptions.Cooldown)
	AutoEmoteOptionsFrameCooldownText:SetText(AutoEmoteOptions.Cooldown .. ' ' .. AE_TIMEINTERVAL)
end

function AutoEmoteOptionsFrameDelaySlider_OnShow()
	AutoEmoteOptionsFrameDelaySlider:SetValue(AutoEmoteOptions.Delay)
	AutoEmoteOptionsFrameDelayText:SetText(AutoEmoteOptions.Delay .. ' ' .. AE_TIMEINTERVAL)
end

function AutoEmoteOptionsFrameCooldownSlider_OnValueChanged(h,i,j)
	AutoEmoteOptions.Cooldown = AutoEmoteOptionsFrameCooldownSlider:GetValue()
	AutoEmoteOptionsFrameCooldownText:SetText(AutoEmoteOptions.Cooldown .. ' ' .. AE_TIMEINTERVAL)
end

function AutoEmoteOptionsFrameDelaySlider_OnValueChanged()
	AutoEmoteOptions.Delay = AutoEmoteOptionsFrameDelaySlider:GetValue()
	AutoEmoteOptionsFrameDelayText:SetText(AutoEmoteOptions.Delay .. ' ' .. AE_TIMEINTERVAL)
end

function AutoEmoteOptionsFrame_OnShow()
	-- Pre-fill the text boxes is something is selected
	if ( AutoEmoteOptions.SelectedEmote ~= nil and AutoEmoteOptions.SelectedEmote ~= "" ) then	
		AutoEmoteOptionsFrame_SetSearchBox(AutoEmoteOptions.SelectedEmote)
		AutoEmoteOptionsFrame_SetEmoteBox(AutoEmoteOptions.Emotes[AutoEmoteOptions.SelectedEmote])
	end
end

function AutoEmoteSetupList()
	AutoEmoteEmoteArray = {}
	local t2 = {}
	local i = 1
	for key, value in pairs( AutoEmoteOptions.Emotes ) do
		if (value ~= "") then
			t2[key] = value
		end
	end
	AutoEmoteOptions.Emotes = tablecopy(t2)

	table.foreach (AutoEmoteOptions.Emotes, function (k) table.insert (AutoEmoteEmoteArray, k) end )

	table.sort (AutoEmoteEmoteArray)

	-- local i = 1
	-- table.sort(AutoEmoteOptions.Emotes)
	for index, key in ipairs (AutoEmoteEmoteArray) do
	-- for key,response in AutoEmoteOptions.Emotes do
	-- for key,response in table.sort(AutoEmoteOptions.Emotes) do
		-- else
			AutoEmoteEmoteArray[index] = key
			-- DEFAULT_CHAT_FRAME:AddMessage("Index " .. index .. ": Adding key: " .. key)
			-- i = i + 1
			--[[info = {}
			info.text = key
			info.func = AutoEmoteOptionsFrameDropDownButton_OnClick
			info.checked = checked
			UIDropDownMenu_AddButton(info)]]
		-- end
	end
	--[[for key, response in pairs( AutoEmoteOptions.Emotes ) do
		DEFAULT_CHAT_FRAME:AddMessage("Find: " .. key .. "... Response: " .. response)
	end]]
	AutoEmoteEmoteScrollFrame_Update(self)
	AutoEmoteButton_Update(self)
end

function AutoEmoteEmoteScrollFrame_Update(self)
	local line -- 1 through 5 of our window to scroll
	-- local listsizemax = #AutoEmoteOptions.Emotes
	-- local listsizemax = select( "#", AutoEmoteOptions.Emotes )
	local listsizemax = TableSize(AutoEmoteOptions.Emotes)
	local listsize = 10
	local lineplusoffset -- an index into our data calculated from the scroll offset
	-- DEFAULT_CHAT_FRAME:AddMessage("listsizemax: " .. listsizemax )

	FauxScrollFrame_Update(AutoEmoteEmoteScrollFrame,listsizemax,listsize+1,16)
	for line=1,listsize do
		lineplusoffset = line + FauxScrollFrame_GetOffset(AutoEmoteEmoteScrollFrame)
		local CurrentListItem = getglobal("AutoEmoteListItem"..line)
		if lineplusoffset < listsizemax then
			-- DEFAULT_CHAT_FRAME:AddMessage("CurrentLine " .. lineplusoffset .. ": Setting text to: " .. AutoEmoteEmoteArray[lineplusoffset])
			getglobal("AutoEmoteListItem"..line.."_Text"):SetText(AutoEmoteEmoteArray[lineplusoffset])
			CurrentListItem:Show()
			if ( AutoEmoteEmoteArray[lineplusoffset] == AutoEmoteOptions.SelectedEmote ) then
				CurrentListItem:LockHighlight()
			else
				CurrentListItem:UnlockHighlight()
				-- DEFAULT_CHAT_FRAME:AddMessage("Array: " .. AutoEmoteEmoteArray[lineplusoffset] .. " != " .. AutoEmoteOptions.SelectedEmote)
			end
		else
			getglobal("AutoEmoteListItem"..line):Hide()
			CurrentListItem:UnlockHighlight()
		end
	end
	AutoEmoteButton_Update(self)
end

function AutoEmoteListItem_OnClick()
	-- UIDropDownMenu_SetSelectedID(AutoEmoteOptionsFrameDropDown, this:GetID())
	AutoEmoteOptions.SelectedEmote = getglobal(this:GetName() .. "_Text"):GetText()
	-- DEFAULT_CHAT_FRAME:AddMessage("Current Emote " .. AutoEmoteOptions.SelectedEmote)
	-- Au
	AutoEmoteEmoteScrollFrame_Update(self)
	AutoEmoteOptionsFrame_SetSearchBox(AutoEmoteOptions.SelectedEmote)
	AutoEmoteOptionsFrame_SetEmoteBox(AutoEmoteOptions.Emotes[AutoEmoteOptions.SelectedEmote])
end

function AutoEmoteOptionsFrame_SetSearchBox(text)
	if (text) then
		AutoEmoteOptionsFrameSearchEditBox:SetText(text)
	else
		AutoEmoteOptionsFrameSearchEditBox:SetText("")
	end
end

function AutoEmoteOptionsFrame_SetEmoteBox(text)
	if (text) then
		AutoEmoteOptionsFrameEmoteEditBox:SetText(text)
	else
		AutoEmoteOptionsFrameEmoteEditBox:SetText("")
	end
end

function AutoEmoteOptionsFrameEditBoxReset()
	AutoEmoteOptions.SelectedEmote = AutoEmoteOptionsFrameSearchEditBox:GetText()
	AutoEmoteOptionsFrame_SetSearchBox(AutoEmoteOptions.SelectedEmote)
	AutoEmoteOptionsFrame_SetEmoteBox(AutoEmoteOptions.Emotes[AutoEmoteOptions.SelectedEmote])
end

function AutoEmoteOptionsFrameEditBoxSave()
	AutoEmoteOptions.SelectedEmote = AutoEmoteOptionsFrameSearchEditBox:GetText()
	AutoEmoteOptions.Emotes[AutoEmoteOptions.SelectedEmote] = AutoEmoteOptionsFrameEmoteEditBox:GetText()
	AutoEmoteSetupList()
end

function AutoEmoteButton_Update(self)
	if ( AutoEmoteOptionsFrameSearchEditBox ) then
		-- AutoEmoteOptions.SelectedEmote = AutoEmoteOptionsFrameSearchEditBox:GetText()
		-- AutoEmoteOptions.SelectedEmote = currentEmote
		-- Add button
		AutoEmoteOptionsFrameAddEmoteButton:Enable()
		
		-- Remove Button
		if ( AutoEmoteOptions.SelectedEmote == "" ) then
			AutoEmoteOptionsFrameRemoveEmoteButton:Disable()
		elseif ( string.find(AutoEmoteOptions.SelectedEmote,"AE_") ~= nil ) then
			AutoEmoteOptionsFrameRemoveEmoteButton:Disable()
		else
			AutoEmoteOptionsFrameRemoveEmoteButton:Enable()
		end
	end
end

function AutoEmoteOptionsFrameAddEmoteButton_OnClick()
	AutoEmoteOptionsFrameEmoteEditBox:ClearFocus()
	AutoEmoteOptionsFrameSearchEditBox:SetFocus()
	AutoEmoteOptionsFrame_SetEmoteBox("")
	AutoEmoteOptionsFrame_SetSearchBox("")
	AutoEmoteButton_Update(self)
end

function AutoEmoteOptionsFrameRemoveEmoteButton_OnClick()
	-- table.remove(AutoEmoteOptions.Emotes,AutoEmoteOptions.SelectedEmote)
	AutoEmoteOptions.Emotes[AutoEmoteOptions.SelectedEmote] = ""
	-- UIDropDownMenu_SetSelectedID(AutoEmoteOptionsFrameDropDown, 2)
	-- UIDropDownMenu_SetSelectedID(AutoEmoteOptionsFrameDropDown, 1)
	-- AutoEmoteOptions.SelectedEmote = UIDropDownMenu_GetText(AutoEmoteOptionsFrameDropDown)
	-- AutoEmoteOptionsFrameRemoveEmoteButton:Disable()
	AutoEmoteEmoteScrollFrame_Update()
	AutoEmoteOptionsFrame_SetSearchBox("")
	AutoEmoteOptionsFrame_SetEmoteBox("")
	AutoEmoteSetupList()
end

function AutoEmoteOptionsFrameResetPrefs()
	AutoEmoteOptions = {}
	InitializeSetup()
	AutoEmoteOptionsFrame:Hide()
	-- UIDropDownMenu_SetSelectedID(AutoEmoteOptionsFrameDropDown, 2)
	-- UIDropDownMenu_SetSelectedID(AutoEmoteOptionsFrameDropDown, 1)
	-- AutoEmoteOptions.SelectedEmote = UIDropDownMenu_GetText(AutoEmoteOptionsFrameDropDown)
	AutoEmoteOptionsFrame_SetSearchBox("")
	AutoEmoteOptionsFrame_SetEmoteBox("")
	AutoEmoteEmoteScrollFrame_Update()
	AutoEmoteOptionsFrame:Show()
end
