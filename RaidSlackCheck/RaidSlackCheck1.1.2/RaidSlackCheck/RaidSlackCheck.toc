﻿## Interface: 30300
## Title: Raid|cff7fff7fSlack|rCheck
## Title-ruRU: Raid|cff7fff7fSlack|rCheck
## Author: Shurshik
## Version: 1.112
## Notes: /rsc or /raidslackcheck for more info
## Notes-ruRU: /rsc или /слак - для открытия инфо
## URL: http://phoenix-wow.ru/
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariablesPerCharacter: rscchatrep,rscraidlrep,rsccolornick,rscwhomustbufflist,rscbuffschat,rscbuffcheckb,rscbuffwhichtrack,rscbufftimers,rscflaskcheckb,rscflaskchat, rscpotionscombatsaves,rscminflaskgood,rscignorzone
Localization\localization-enEN.lua
Localization\localization-ruRU.lua
Localization\localization-deDE.lua
Localization\localization-frFR.lua
Localization\localization-esES.lua
Localization\localization-esMX.lua
Localization\localization-koKR.lua
Localization\localization-zhTW.lua
RaidSlackCheck.lua
RaidSlackCheck.xml
buffrebith.lua
flaskcheck.lua