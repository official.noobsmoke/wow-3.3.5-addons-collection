## Interface: 30300
## X-Compatible-With: 40000
## Title: Alts
## Notes: Manages main-alts relations.
## Author: Talryn
## Version: r5
## OptionalDeps: Ace3, LibDeformat-3.0, lib-st, LibDataBroker-1.1, LibDBIcon-1.0
## X-Embeds: Ace3
## X-Category: Interface Enhancements 
## SavedVariables: AltsDB
## X-Curse-Packaged-Version: r5
## X-Curse-Project-Name: Alts
## X-Curse-Project-ID: alts
## X-Curse-Repository-ID: wow/alts/mainline

embeds.xml

locale.xml

Alts.lua
