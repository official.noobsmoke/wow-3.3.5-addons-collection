local L = LibStub("AceLocale-3.0"):NewLocale("Alts", "enUS", true)

if not L then return end

L["Set main for %s: %s"] = true
L["Usage: /setmain <alt> <main>"] = true
L["Usage: /delalt <alt> <main>"] = true
L["Usage: /getalts <main>"] = true

L["Deleted alt %s for %s"] = true
L["Save"] = true
L["Cancel"] = true
L["Close"] = true
L["Search"] = true
L["Clear"] = true
L["Delete"] = true
L["Edit"] = true
L["Add"] = true
L["Set Main"] = true
L["Add Alt"] = true
L["Main: "] = true
L["Alts: "] = true
L["Main Name"] = true
L["Alt Name"] = true
L["Alts"] = true
L["Edit Alts"] = true
L["No alts found for "] = true
L["Alts for %s: %s"] = true
L["No main found for "] = true
L["Main for %s: %s"] = true
L["Imported the guild '%s'. Mains: %d, Alts: %d."] = true

L["Delete Alt"] = true
L["Are you sure you wish to remove"] = true
L["as an alt of"] = true

L["Delete Main"] = true
L["Are you sure you wish to delete the main and all user-entered alts for:"] = true

L["Minimap Button"] = true
L["Toggle the minimap button"] = true
L["Verbose"] = true
L["Toggles the display of informational messages"] = true

L["Guild Import Options"] = true
L["Auto Import Guild"] = true
L["Toggles if main/alt data should be automaticall imported from guild notes."] = true

L["Display Options"] = true
L["Main Name In Tooltips"] = true
L["Toggles the display of the main name in tooltips"] = true
L["Alt Names In Tooltips"] = true
L["Toggles the display of alt names in tooltips"] = true
L["Main/Alt Info on Friend Logon"] = true
L["Toggles the display of main/alt information when a friend or guild member logs on"] = true
L["Main/Alt Info with /who"] = true
L["Toggles the display of main/alt information with /who results"] = true
L["Single Line for Chat"] = true
L["Toggles whether the main and alt information is on one line or separate lines in the chat window."] = true
L["Single Line for Tooltip"] = true
L["Toggles whether the main and alt information is on one line or separate lines in tooltips."] = true

L["Tooltip Options"] = true
L["Wrap Tooltips"] = true
L["Wrap notes in tooltips"] = true
L["Tooltip Wrap Length"] = true
L["Maximum line length for a tooltip"] = true

L["Left click"] = true
L["to open/close the window"] = true
L["Right click"] = true
L["to open/close the configuration."] = true
