-- French Localization:
-- Done by m0rgoth

if (GetLocale()=="frFR") then
	BINDING_NAME_LOOTHOGTOGGLE = "Show/Hide Window"
	BINDING_HEADER_LOOTHOG = "LootHog"
	LOOTHOG_ROLL_PATTERN = "(.+) obtient un (%d+) %((%d+)%-(%d+)%)"
	LOOTHOG_PASS_PATTERN = "passe"
	LOOTHOG_RESETONWATCH_PATTERN = "gagne avec un:"
	LOOTHOG_RESETONWATCH_PATTERN2 = "tied with"
	
	-- size of the main and options/config window
	LOOTHOG_UI_MAIN_WIDTH = "190"
	LOOTHOG_UI_CONFIG_WIDTH = "410"
	LOOTHOG_UI_CONFIG_HEIGHT = "430"

	LOOTHOG_LABEL_DELIMITER = "------------------"
	LOOTHOG_LABEL_WINNERSDELIMITER = "=================="
	LOOTHOG_LABEL_CHATLISTTOP = "Rolls in Ascending Order"
	LOOTHOG_LABEL_ROLLS = "/random Rolls"
	LOOTHOG_LABEL_READY = "prets..." -- Means that LootHog is ready to receive /random rolls.
	LOOTHOG_LABEL_OPTIONS = "LootHog Options"
	LOOTHOG_LABEL_HOLDING = "attente..." -- Means that the user has clicked "Hold" to prevent the timeout.
	LOOTHOG_LABEL_NOTIMEOUT = "Timeout off"
	LOOTHOG_LABEL_TIMELEFT = "Timeout: %s seconds" -- (seconds)
	LOOTHOG_LABEL_COUNTDOWN = "Countdown"
	LOOTHOG_LABEL_COUNT = "Rolls:%s, Joueurs:%s" -- (roll count, player count)
	LOOTHOG_LABEL_NEW_ROLL_TEXT = ""

	LOOTHOG_BUTTON_CLEAR = "RAZ"
	LOOTHOG_BUTTON_OPTIONS = "Options"
	LOOTHOG_BUTTON_HOLD = "Hold"
	LOOTHOG_BUTTON_UNHOLD = "UnHold"
	LOOTHOG_BUTTON_ANNOUNCE = "Annonnce"
	LOOTHOG_BUTTON_YETTOROLL = "Non roulé"
	LOOTHOG_BUTTON_INFO = "Info"
	LOOTHOG_BUTTON_ROLL = "Roll (1-100)"
	LOOTHOG_BUTTON_PASS = "Passe"
	LOOTHOG_BUTTON_COUNTDOWN = "Countdown"
	
	
	LOOTHOG_OPTION_ENABLE = "Utilise LootHog"
	LOOTHOG_OPTION_AUTOSHOW = "ouvre autaumatiquement la fenetre si qq random"
	LOOTHOG_OPTION_RESETONWATCH = "Reset LootHog if someone else announces the winner"
	LOOTHOG_OPTION_GROUPONLY = "Count rolls from group members only"
	
	LOOTHOG_OPTION_LISTTOCHAT = "Submit top"
	LOOTHOG_OPTION_LISTTOCHAT2 = "rolls to chat when announcing"

	LOOTHOG_OPTION_PREVENT = "empechez les rolls d apparaitre dans le chatlog"
	LOOTHOG_OPTION_CLOSEONANNOUNCE = "fermer la fenetre de loothog apres annonce vainqueur"
	LOOTHOG_OPTION_ACK = "prevenir le joueur avec un /w de son roll"
	
	LOOTHOG_OPTION_REJECT = "rejeter les randoms autres que (1-100)"
	  LOOTHOG_OPTION_ANNOUNCEREJECT = "annoncer les rolls rejetes"
	
	LOOTHOG_OPTION_ANNOUNCEONCLEAR = "Announce on Start:"
	  LOOTHOG_OPTION_ANNOUNCETIMEOUT = "Announce timeout on start"
	
	LOOTHOG_OPTION_TIMEOUT1 = "Auto-timeout apres"
	LOOTHOG_OPTION_TIMEOUT2 = "secondes"
	
  	LOOTHOG_OPTION_AUTOCOUNTDOWN = "Automatically countdown last"
	
    LOOTHOG_OPTION_AUTOEXTEND = "reset timer to"
    LOOTHOG_OPTION_TIMEOUT3 = "seconds if /roll detected in final "
    
    LOOTHOG_OPTION_ANNOUNCEEXTEND = "Announce roll extenstions"
    
	LOOTHOG_OPTION_FINALIZE = "Finir le roulement apr\195\168s les temps expirent"
	LOOTHOG_OPTION_FINALIZEROLLS = "Finalize rolling when all group members have rolled"
	
	
	LOOTHOG_MSG_LOAD = "LootHog v%s charger. pour les options, taper /loothog." -- (version)
	LOOTHOG_MSG_INFO = "LootHog-Information: You may pass on a running roll by typing: %s"  -- (pass pattern)
	LOOTHOG_MSG_CHEAT = "ignore le % roll de %s (%s-%s)." -- (player, roll, max_roll, min_roll)
	LOOTHOG_MSG_ACK = "roll enregistrer (%s). bonne chance" -- (roll)
	LOOTHOG_MSG_ACK_PASS = "Vous avez pass\195\169 pour ce roulement."
	LOOTHOG_MSG_DUPE = "%s a jouer plus d une fois!" -- (player)
	LOOTHOG_MSG_WINNER = "%s%s gagne avec un: %s!  (%s-%s)  %s" --(player, group, roll, min, max, message)
	LOOTHOG_MSG_ROLLS = "%s: %s rolled %s."  -- (roll posistion, player, roll)
	LOOTHOG_MSG_GROUP = "Gruppe"
	LOOTHOG_MSG_NOTGROUP= "Gruppe nicht gefunden"	
	LOOTHOG_MSG_STAT = "Gesamtanzahl der W\195\188rfe: "
	LOOTHOG_MSG_SECONDS = ""
	LOOTHOG_MSG_HOLDCONT = "Loothog is restarting countdown at %s seconds"
	LOOTHOG_MSG_HOLDSTART = "Loothog is holding countdown at %s seconds"
	LOOTHOG_MSG_RESET = "New Roll Detected restating countdown at %s seconds"
	LOOTHOG_MSG_TIMEANNOUNCE = "You have %s seconds to roll!"
	

-- The following variables are used to build a string of this type, in the case of a tie:
-- "Gnomechomsky, Saucytails, and Pionerka tied with 98's!"
-- I don't know if a literal substitution can provide a proper translation. Feedback is welcome. :)
	LOOTHOG_MSG_AND = " et "
	LOOTHOG_MSG_TIED = " tied with %s's!" -- (roll)
	LOOTHOG_MSG_YETTOPASS = "The following people still need to roll or say %s" --(LOOTHOG_PASS_PATTERN)
	LOOTHOG_MSG_REMOVEROLL = "<LootHog>: %ss Ergebnis von %s wurde entfernt." -- (player, roll)
end