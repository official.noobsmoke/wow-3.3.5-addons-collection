loothog_version = "3.1.3"

LH_temp, loothog_line_height,LH_temp = GameTooltipTextSmall:GetFont()
loothog_top_height = 72
loothog_button_height = 80
loothog_default_lines = 10

loothog_upperRollBoundary = 100
loothog_lowerRollBoundary = 1

loothog_countdownStarted = false
loothog_numberOfSecondsToCountDown = 5
loothog_countdownStartTime = 0
loothog_countdownannounce = true
display_time = 0

loothog_titanPanelActive = false --on load check if Titan Panel is present


function loothog_OnLoad()
	this:RegisterEvent("VARIABLES_LOADED")
	this:RegisterEvent("CHAT_MSG_SYSTEM")
	this:RegisterEvent("CHAT_MSG_SAY")
	this:RegisterEvent("CHAT_MSG_PARTY")
	this:RegisterEvent("CHAT_MSG_RAID")
	this:RegisterEvent("CHAT_MSG_RAID_LEADER")

--leave raid bug fix array
	loothog_classes = {}
		
-- Default settings:
	loothog_settings = {}
	
	loothog_timer = 0
	loothog_timeout_held = 0
	loothog_duplicate = false
	
-- Register a slash command:
	SlashCmdList["LOOTHOG"] = loothog_slash;
	SLASH_LOOTHOG1 = "/loothog";
	SLASH_LOOTHOG2 = "/lh";
	
	if (TitanPlugins) then
		loothog_titanPanelActive = true
	else
		loothog_titanPanelActive = false
	end
	
-- Hook the function to add messages to Chat Frame 1:
	loothog_o_AddMessage = ChatFrame1.AddMessage
	ChatFrame1.AddMessage = loothog_AddMessage
	
-- Make the options frame closable with ESC:
--	table.insert(UISpecialFrames,"loothog_config");
end

function loothog_slash(cmd)
	if cmd and (cmd == "options" or cmd == "config") then
	  	return loothog_toggle_options()
	end
	if cmd and cmd == "countdown" then
		return loothog_countdown_clicked()
	end
	if cmd and cmd == "hold" then
		return loothog_holdclicked()
	end
	if cmd and cmd == "kick" then
		return loothog_kickRoll()
	end
	if cmd and cmd == "announce" then
		return loothog_announce_winner()
	end
	if cmd and cmd == "clear" then
		return loothog_clear_clicked()
	end
	if cmd and cmd == "notrolled" then
		return loothog_AnnounceYetToRoll()
	end
	if cmd and cmd == "list" then
		return loothog_listtochat()
	end
	loothog_toggle_visible()
end

function loothog_OnEvent()
	if (event == "VARIABLES_LOADED") then
		loothog_initialize()
	end
	
	if (event == "CHAT_MSG_SYSTEM") then
		loothog_sys_msg_received()
	end
	if (event == "CHAT_MSG_" .. loothog_get_auto_channel() or event == "CHAT_MSG_RAID_LEADER") then
		loothog_chat_msg_received()
	end
end

function loothog_SetDefaultSettings(name, value)
   if (loothog_settings[name] == nil) then
     loothog_settings[name] = value
   end
end

function loothog_initialize()
	if (DEFAULT_CHAT_FRAME) then
		DEFAULT_CHAT_FRAME:AddMessage(string.format(LOOTHOG_MSG_LOAD, loothog_version))
	end
-- initialize default settings	
	loothog_SetDefaultSettings("enable", true)
	loothog_SetDefaultSettings("auto_show", true)
	loothog_SetDefaultSettings("triggered_clear", false)
	loothog_SetDefaultSettings("group_only", false)
	loothog_SetDefaultSettings("listtochat", false)
	loothog_SetDefaultSettings("listtochatno", 5)
	loothog_SetDefaultSettings("suppress_chat", false)
	loothog_SetDefaultSettings("close_on_announce", false)
	loothog_SetDefaultSettings("finalize", false)
	loothog_SetDefaultSettings("finalizerolls", false)
	loothog_SetDefaultSettings("ack_rolls", false)
	loothog_SetDefaultSettings("reject", true)
	loothog_SetDefaultSettings("reject_announce", false)	
	loothog_SetDefaultSettings("timeout", 60)
	loothog_SetDefaultSettings("timeout_enabled", false)
	loothog_SetDefaultSettings("announce_new", false)
	loothog_SetDefaultSettings("timeout_announce", false)
	loothog_SetDefaultSettings("new_roll_text", LOOTHOG_LABEL_NEW_ROLL_TEXT)
	loothog_SetDefaultSettings("autocountdown", false)
	loothog_SetDefaultSettings("autoextend", false)
	loothog_SetDefaultSettings("announceextend", false)
	loothog_SetDefaultSettings("autocounttime", 5)
	loothog_SetDefaultSettings("autoextendtime", 5)
	loothog_SetDefaultSettings("autoresettime", 10)
	
-- Initialize session variables:
	loothog_last_roll = 0	-- The time of the most recent roll
	loothog_active = false
	loothog_hold = false
	loothog_rolls = {}
	loothog_peopleToRoll = {} --list of people in group/raid who have to roll
	
-->Titan
	loothog_titanrolls = ""
	loothog_titannonrolls = ""
	loothog_titanwinner = ""
	loothog_titan_active = false
	loothog_titanstatus = "|cff0000ff"  .. LOOTHOG_LABEL_READY .. FONT_COLOR_CODE_CLOSE
--<Titan
	
	loothog_interval = 10	-- Execute OnUpdate every x frames
	loothog_current_interval = 0
	loothog_player_count = 0
	loothog_players = {}
	local height = loothog_line_height * loothog_default_lines + loothog_top_height + loothog_button_height
	loothog_main:SetHeight(height)

--Setting the width/height for the main and configuration window dependent on the needed height for each localization
	loothog_main:SetWidth(LOOTHOG_UI_MAIN_WIDTH)
	loothog_config:SetWidth(LOOTHOG_UI_CONFIG_WIDTH)
	loothog_config:SetHeight(LOOTHOG_UI_CONFIG_HEIGHT)

	loothog_language = GetDefaultLanguage("player")
-- Set the checkboxes:
	loothog_configEnable:SetChecked(loothog_settings["enable"])
	loothog_configClear:SetChecked(loothog_settings["triggered_clear"])
	loothog_configFinalize:SetChecked(loothog_settings["finalize"])
	loothog_configFinalizeRolls:SetChecked(loothog_settings["finalizerolls"])
	loothog_configAutoShow:SetChecked(loothog_settings["auto_show"])
	loothog_configGroupOnly:SetChecked(loothog_settings["group_only"])
	loothog_configSuppress:SetChecked(loothog_settings["suppress_chat"])
	loothog_configCloseOnAnnounce:SetChecked(loothog_settings["close_on_announce"])
	loothog_configAckRolls:SetChecked(loothog_settings["ack_rolls"])
	loothog_configReject:SetChecked(loothog_settings["reject"])
	loothog_configRejectAnnounce:SetChecked(loothog_settings["reject_announce"])
	loothog_configTimeoutEnabled:SetChecked(loothog_settings["timeout_enabled"])
	loothog_configAutoCountdown:SetChecked(loothog_settings["autocountdown"])
	loothog_configAutoCountTime:SetNumber(loothog_settings["autocounttime"])
	loothog_configAutoExtend:SetChecked(loothog_settings["autoextend"])
	loothog_configExtendTime:SetNumber(loothog_settings["autoextendtime"])
	loothog_configAnnounceExtend:SetChecked(loothog_settings["announceextend"])
	loothog_configTimeout:SetNumber(loothog_settings["timeout"])
	loothog_configAutoResetTime:SetNumber(loothog_settings["autoresettime"])
	loothog_configAnnounceNew:SetChecked(loothog_settings["announce_new"])
	loothog_configListToChatNo:SetNumber(loothog_settings["listtochatno"])
	loothog_configAnnounceTimeout:SetChecked(loothog_settings["timeout_announce"])
	loothog_configAnnouncement:SetText(loothog_settings["new_roll_text"])

-- Use Different Variable names for certain settings.
	loothog_timeout = loothog_settings["timeout"]	
	loothog_autocounttime = loothog_settings["autocounttime"]
	loothog_autoextendtime = loothog_settings["autoextendtime"]
	loothog_autoresettime = loothog_settings["autoresettime"]

-- Set Default Values if settings set to zero.
	if loothog_timeout == 0 then
		loothog_timeout = 60
	end
	if loothog_settings["autocounttime"] == 0 then
		loothog_settings["autocounttime"] = 5
	end
	if loothog_autoextendtime == 0 then
		loothog_autoextendtime = 5
	end

	if loothog_autoresettime == 0 then
		loothog_autoresettime = 10
	end

	
-- Update the count text:
	loothog_update_counts()
-- Hide the roll window:
	loothog_main:Hide()
	loothog_config:Hide()
	
--Add option in Interface/addons
	loothog_addonconf.name="LootHog"
	InterfaceOptions_AddCategory(loothog_addonconf)
end

function loothog_sys_msg_received()
	local msg = arg1
	local itemString = ""

	local pattern = LOOTHOG_ROLL_PATTERN

	-- Check to see if it's a /random roll:
	local player, roll, min_roll, max_roll, report, startIndex, endIndex
	if (loothog_settings["enable"]) then
		if (string.find(msg, pattern)) then
			_, _, player, roll, min_roll, max_roll = string.find(msg, pattern)
--check if player who rolled is in your group or configured to count all rolls
			if (((loothog_isInGroup(player)) and (loothog_settings["group_only"])) or (not loothog_settings["group_only"])) then
				loothog_ProcessRoll(player, roll, min_roll, max_roll)
			end
		end
	end
end

function loothog_chat_msg_received()
	local msg = arg1
	local player = arg2
	if (loothog_settings["enable"]) then
		if (strlower(msg) == LOOTHOG_PASS_PATTERN) then
			loothog_ProcessRoll(player, "0", "1", "100")
		elseif ((string.find(msg, LOOTHOG_RESETONWATCH_PATTERN) or string.find(msg, LOOTHOG_RESETONWATCH_PATTERN2)) and loothog_settings["triggered_clear"]) then
			loothog_make_inactive()
		end
	end
end

function loothog_AddMessage(self, msg, ...)
-- Pass along to the default handler unless it's a /random roll:
	local pattern = LOOTHOG_ROLL_PATTERN
	if (msg==nil) or (not loothog_settings["suppress_chat"]) or (not string.find(msg, pattern)) then
		loothog_o_AddMessage(self, msg, ...)
	end
end

function loothog_postInformation()
	local infoMsg = string.format(LOOTHOG_MSG_INFO, LOOTHOG_PASS_PATTERN)
	loothog_chat(infoMsg)
end


-- Did this for future function calls to elminate reduntant code (e.g. processSystemLootRoll(...))
function growWindow()
-- Grow the window if needed:
	local count = #loothog_rolls
	local count2 = #loothog_peopleToRoll
	count = count + count2
	
	if (count > loothog_default_lines) then
		local height = loothog_line_height * count + loothog_top_height + loothog_button_height
		loothog_main:SetHeight(height)
	end
end

-- Checks if the unit is in the current group of the player, return always true for the local player!
function loothog_isInGroup(playerName)
-- Check if the rolling player is in your group
	local isInParty = false
-- Check if in party or raid group
	if (GetNumRaidMembers() > 0) then
		local raidMemberString = "raid"
		for i = 1, GetNumRaidMembers(), 1 do
			raidMemberString = "raid" --reset string		
			raidMemberString = raidMemberString .. i --add the suffix of corresponding player
			if (UnitName(raidMemberString) == playerName) then
				isInParty = true	
			end
		end

	elseif (GetNumPartyMembers() > 0) then
		local partyMemberString = "party"
		for i = 1, GetNumPartyMembers(), 1 do
			partyMemberString = "party" --reset string		
			partyMemberString = partyMemberString .. i --add the suffix of corresponding player
			if (UnitName(partyMemberString) == playerName) then
				isInParty = true	
			end
		end
	end
	
	if (UnitName("player") == playerName) then
		isInParty = true
	end

	return isInParty
end

-- Returns if true if the roll is valid (meets bound requirements)
function loothog_validRoll(roll, lowerBoundary, upperBoundary)
	local validRoll = false
	if ((lowerBoundary == loothog_lowerRollBoundary)  and (upperBoundary == loothog_upperRollBoundary)) then
		--prevent possible future cheating (totally "sinnfrei" ;-)
		if (((roll <= loothog_upperRollBoundary) and (roll >= loothog_lowerRollBoundary)) or roll == 0) then
			validRoll = true
		end
	else
		validRoll = false
	end
	return validRoll
end

-- Checks if the roll tied, and returns the number of winners. Returns 1 if there was NO tie
function loothog_rollTied()
	local rollTied = false;
	local num_winners = 0;
	if (#loothog_rolls ~= 0) then
-- Determine if there was a tie:
		local winners = {}
		local best = loothog_rolls[1][2]
		winners[1] = loothog_rolls[1][1]
		local i = 2
		local rolls = #loothog_rolls
		while (i <= rolls) and (loothog_rolls[i][2] == best) do
			table.insert(winners, loothog_rolls[i][1])
			i = i + 1
		end
		num_winners = #winners
		if (num_winners > 1) then
			rollTied = true
		end
	end

	return num_winners
end

function loothog_ProcessRoll(player, roll, min_roll, max_roll)
	roll = tonumber(roll)
	min_roll = tonumber(min_roll)
	max_roll = tonumber(max_roll)

-- Reject cheaters
	if (not loothog_validRoll(roll, min_roll, max_roll) and (loothog_settings["reject"])) then
		local cheat_msg = string.format(LOOTHOG_MSG_CHEAT, player, roll, min_roll, max_roll)
		if (loothog_settings["reject_announce"]) then
		-- Bust the cheater if configured to announce rejects:
			loothog_chat(cheat_msg)
		else
		-- Otherwise just notify the local client:
			if (DEFAULT_CHAT_FRAME) then
				DEFAULT_CHAT_FRAME:AddMessage(cheat_msg)
			end
		end
		return
	end
	
-- We've determined it's a valid roll.  Acknowledge if configured to do so and do it only if this is the first roll of the player:
	if (loothog_settings["ack_rolls"] and (not loothog_players[player]) and (UnitName("player") ~= player)) then
		local ack = string.format(LOOTHOG_MSG_ACK, roll)
-- Send pass message to player if he did pass on this roll
		if (roll == 0) then
			ack = LOOTHOG_MSG_ACK_PASS
		end
		SendChatMessage(ack, "WHISPER", loothog_language, player)
	end
	
	if (not loothog_active) then
-- Timeout has expired.  Consider this the first roll of a new set:
		loothog_clear_clicked()
		loothog_timer = GetTime()
		if loothog_settings["timeout_enabled"] then
			loothog_numberOfSecondsToCountDown = loothog_timeout - 1
			if loothog_settings["autocountdown"] then
				loothog_countdown()
			end		
		end

-- Setting up a list of people who are in the players group/raid and "have" to roll
		if (GetNumRaidMembers() > 0) then
			local raidMemberString = "raid"
			for i = 1, GetNumRaidMembers(), 1 do
				raidMemberString = "raid" --reset string		
				raidMemberString = raidMemberString .. i --add the suffix of corresponding player
				table.insert(loothog_peopleToRoll, (UnitName(raidMemberString)))
				loothog_classes[(UnitName(raidMemberString))] = {};
				loothog_classes[(UnitName(raidMemberString))].LocalClass, loothog_classes[UnitName(raidMemberString)].EnglishClass= UnitClass(raidMemberString);
				loothog_classes[(UnitName(raidMemberString))].UnitLevel = UnitLevel(raidMemberString);
			end
		elseif (GetNumPartyMembers() > 0) then
			local partyMemberString = "party"
			for i = 1, GetNumPartyMembers(), 1 do
				partyMemberString = "party" --reset string		
				partyMemberString = partyMemberString .. i --add the suffix of corresponding player
				table.insert(loothog_peopleToRoll, (UnitName(partyMemberString)))
				loothog_classes[(UnitName(partyMemberString))] = {};
				loothog_classes[(UnitName(partyMemberString))].LocalClass, loothog_classes[UnitName(partyMemberString)].EnglishClass= UnitClass(partyMemberString);
				loothog_classes[(UnitName(partyMemberString))].UnitLevel = UnitLevel(partyMemberString);
			end
		end
		
-- Add roll to table of rolls
		loothog_rolls[1] = {player, roll, min_roll, max_roll}
	else
-- Insert the roll into the list at the proper spot:
		if (not loothog_players[player]) then
			local i = 1
			while (loothog_rolls[i] ~= nil) and (loothog_rolls[i][2] >= roll) do i = i+1 end
			table.insert(loothog_rolls, i, {player, roll, min_roll, max_roll})
		end
	end
	
-- Add to the list of players who have rolled:
	if (not loothog_players[player]) then
		loothog_player_count = loothog_player_count + 1
		loothog_players[player] = 1
		loothog_classes[player] = {};
		loothog_classes[player].LocalClass, loothog_classes[player].EnglishClass= UnitClass(loothog_getUnitIdentifier(player));
		loothog_classes[player].UnitLevel = UnitLevel(loothog_getUnitIdentifier(player));
	else
-- Duplicate roll!
		loothog_duplicate = true
		local dupe_msg = string.format(LOOTHOG_MSG_DUPE, player)
		if (DEFAULT_CHAT_FRAME) then
			DEFAULT_CHAT_FRAME:AddMessage(dupe_msg)
		end
	end

-- Remove player who rolled from the list
	for i = 1, #loothog_peopleToRoll, 1 do
		if (loothog_peopleToRoll[i] == player) then
			table.remove(loothog_peopleToRoll, i)
		end
	end
	
-- Repaint loothog window
	loothog_repaint()

-- Auto-show the window if desired:
	if (loothog_settings["auto_show"]) then
		loothog_main:Show()
	end
end

-- Returns the identifier for the supplied player within raid/group (player; party1, party2,... party4; raid1, raid2, ..., raidN), nil if not in group/raid
function loothog_getUnitIdentifier (playerName)
	local identifierString = ""
		if (GetNumRaidMembers() > 0) then
			local raidMemberString = "raid"
			for i = 1, GetNumRaidMembers(), 1 do
				raidMemberString = "raid" --reset string		
				raidMemberString = raidMemberString .. i --add the suffix of corresponding player
				if (UnitName(raidMemberString) == playerName) then
					identifierString = raidMemberString
				end
			end
		elseif (GetNumPartyMembers() > 0) then
			local partyMemberString = "party"
			for i = 1, GetNumPartyMembers(), 1 do
				partyMemberString = "party" --reset string		
				partyMemberString = partyMemberString .. i --add the suffix of corresponding player
				if (UnitName(partyMemberString) == playerName) then
					identifierString = partyMemberString
				end
			end
		end
		
-- Check if supplied player name is the local player
		if (UnitName("player") == playerName) then	
			identifierString = "player"
		end
		
	return identifierString
end

function loothog_announce_winner()
-- If there are no rolls, don't do anything:
	if (#loothog_rolls == 0) then
		return
	end

-- Announce Top Rolls if Configured to do so
	if (loothog_settings["listtochat"]) then
		loothog_listtochat()
	end		
	
-- Determine if there was a tie:
	local winners = {}
	local best = loothog_rolls[1][2]
	winners[1] = loothog_rolls[1][1]
	local i = 2
	local rolls = #loothog_rolls
	while (i <= rolls) and (loothog_rolls[i][2] == best) do
		table.insert(winners, loothog_rolls[i][1])
		i = i + 1
	end
	local num_winners = #winners
-- Announce the winner(s) and prepare for the next set of rolls:
	local report
	if (num_winners == 1) then
		--first get group number, if master looter
		groupnum = "";
		if (GetNumRaidMembers() > 0) then
			--check all 40 raid slots for winner
			i=1;
			found = false;
			while ((i <= 40) and (found == false)) do
				name, rank, subgroup, level, class, fileName, zone, online = GetRaidRosterInfo(i);
					if (name and name == loothog_rolls[1][1]) then
						found = true;
						groupnum = " (" .. LOOTHOG_MSG_GROUP .. " " .. subgroup .. ")";
					end
				i=i+1;
			end
			if (not found) then groupnum = " (" .. LOOTHOG_MSG_NOTGROUP .. ")"; end
		end
		
		report = string.format(LOOTHOG_MSG_WINNER, loothog_rolls[1][1], groupnum, loothog_rolls[1][2], loothog_rolls[1][3], loothog_rolls[1][4])
	else
		report = ""
		for i, v in ipairs(winners) do
			if (i > 1 and num_winners > 2) then report = report..", " end
			if (i == num_winners) then report = report..LOOTHOG_MSG_AND end
			report = report..v
		end
		report = report..string.format(LOOTHOG_MSG_TIED, best)
	end
	loothog_last_roll = 0
	display_time = 0
	loothog_make_inactive()
	loothog_chat(report)
	loothog_numberOfSecondsToCountDown = 0
	if loothog_settings["close_on_announce"] then
		loothog_main:Hide()
	end
end

function loothog_clear_clicked()
	loothog_clear()
-- If so configured, announce the new roll:
	if (loothog_settings["announce_new"]) then
		loothog_StartText = loothog_settings["new_roll_text"]
			if (loothog_settings["timeout_announce"]) and (loothog_settings["timeout_enabled"]) then
				loothog_StartText = loothog_StartText .. "  " .. string.format(LOOTHOG_MSG_TIMEANNOUNCE, loothog_timeout)
			end
		loothog_chat(loothog_StartText)
	end
end

function loothog_clear()
-->Titan
	loothog_titanrolls = ""
	loothog_titannonrolls = ""
	loothog_titanwinner = ""
--<Titan
	loothog_rolls = {}
	loothog_peopleToRoll = {}
	loothog_classes = {}
	loothog_players = {}
	loothog_player_count = 0
	loothog_make_inactive()
	loothog_mainText1:SetText("")
	loothog_mainText2:SetText("")
	local height = loothog_line_height * loothog_default_lines + loothog_top_height + loothog_button_height
	loothog_main:SetHeight(height)
end

function loothog_toggle_visible()
	if loothog_main:IsVisible() then
		loothog_main:Hide()
	else
		loothog_main:Show()
	end
end

function loothog_toggle_options()
	if loothog_config:IsVisible() then
		loothog_config:Hide()
	else
		loothog_config:Show()
	end
end

function loothog_toggle(setting, checked)
	loothog_settings[setting] = checked
end

function loothog_onupdate()

	loothog_current_interval = loothog_current_interval + 1
	if (loothog_current_interval >= loothog_interval) then
		loothog_current_interval = 0
		-- Update the counts:
		loothog_update_counts()
		if(loothog_countdownStarted) then
			loothog_countdown()
		end
		if (loothog_active) then
			-- Update the status text:
			if (loothog_hold) then
				loothog_mainStatusText:SetTextColor(0, 1, 0)
				loothog_mainStatusText:SetText(LOOTHOG_LABEL_HOLDING)
			else
				if loothog_settings["timeout_enabled"] then
					local time_left = math.ceil(loothog_timer + loothog_timeout - GetTime())
					if loothog_countdownclicked then
						loothog_mainStatusText:SetTextColor(1, 1, 0)
						loothog_mainStatusText:SetText(string.format(LOOTHOG_LABEL_TIMELEFT, time_left))
					else
						loothog_mainStatusText:SetTextColor(1, 1, 0)
						loothog_mainStatusText:SetText(string.format(LOOTHOG_LABEL_TIMELEFT, time_left))
					end
				else
					loothog_mainStatusText:SetTextColor(0, 1, 0)
					loothog_mainStatusText:SetText(LOOTHOG_LABEL_NOTIMEOUT)
				end
			end
		
-- See if it's time to go inactive:
			if (not loothog_hold) and (GetTime() - loothog_timer > loothog_timeout) and (loothog_settings["timeout_enabled"]) then
				loothog_alldone()
			end
		end
	end
	local loothog_people_target = table.getn(loothog_peopleToRoll)
	local loothog_current_rolls = table.getn(loothog_rolls)
end

function loothog_update_counts()
-- Set ratio number of players which have rolled/number of players in group
	local partyMemberCount = 1
	if (GetNumRaidMembers() > 0) then
		partyMemberCount = GetNumRaidMembers()
	elseif (GetNumPartyMembers() > 0) then
		partyMemberCount = GetNumPartyMembers() + 1
	end

	local titanPartyMemberCount = partyMemberCount
	
--GetNumRaidMembers()
	loothog_mainCountText:SetText(string.format(LOOTHOG_LABEL_COUNT, #loothog_rolls, partyMemberCount))
	local fontcolor = RED_FONT_COLOR_CODE
	
	if (loothog_player_count < #loothog_rolls) then
		loothog_mainCountText:SetTextColor(1, 0.3, 0.3)
	else
		loothog_mainCountText:SetTextColor(1, 1, 0)
	end
	

-->Titan
	if (loothog_titanPanelActive) then
		local fontcolor = GREEN_FONT_COLOR_CODE
		local rolls = #loothog_rolls
		if (rolls < titanPartyMemberCount)then
			fontcolor = RED_FONT_COLOR_CODE
		end
		if (rolls > 0) then
			local winner = loothog_rolls[1][1]
			local best = loothog_rolls[1][2]
			loothog_titanwinner = GREEN_FONT_COLOR_CODE .. winner .." ("..best.. ")" .. FONT_COLOR_CODE_CLOSE .. fontcolor .. " [" .. #loothog_rolls .. "/" .. titanPartyMemberCount .. "]" .. FONT_COLOR_CODE_CLOSE 
		else
			loothog_titanwinner = ""
		end
	end
--<Titan
	if (loothog_active) and (partyMemberCount == #loothog_rolls) and (loothog_settings["finalizerolls"]) then
		loothog_alldone()
	end
end

function loothog_make_inactive()
	loothog_mainText1:SetTextColor(0.7, 0.7, 0.7)
	loothog_mainText2:SetTextColor(0.7, 0.7, 0.7)
	loothog_mainStatusText:SetTextColor(0.5, 0.5, 1.0)
	loothog_mainStatusText:SetText(LOOTHOG_LABEL_READY)
	loothog_timeout = loothog_settings["timeout"]
	loothog_numberOfSecondsToCountDown = 0
	loothog_active = false
	loothog_duplicate = false
	loothog_countdownclicked = false
	loothog_countingdown = false
	loothog_countdownannounce = true
	loothog_manual = false
--> Titan
	loothog_titan_active = false
	loothog_titanstatus = "|cff0000ff" .. LOOTHOG_LABEL_READY .. FONT_COLOR_CODE_CLOSE 
--< Titan
end

function loothog_holdclicked()
	if (loothog_hold) then
		loothog_hold = false
		loothog_mainHoldButton:SetText(LOOTHOG_BUTTON_HOLD)
		loothog_timeout = loothog_timeout_held
		loothog_timer = GetTime()
		if (loothog_settings["announceextend"]) or loothog_manual then
			HoldInfo =string.format(LOOTHOG_MSG_HOLDCONT, loothog_timeout)
			loothog_chat(HoldInfo)
		end

		if loothog_countdownrunning then
			loothog_numberOfSecondsToCountDown = loothog_timeout
			loothog_countdown()
		end
	else
		loothog_hold = true
		loothog_mainHoldButton:SetText(LOOTHOG_BUTTON_UNHOLD)
		loothog_timeout_held = math.ceil(loothog_timer + loothog_timeout - GetTime())
		if loothog_timeout_held < loothog_settings["autoextendtime"] + 1 then
			loothog_timeout_held = loothog_settings["autoresettime"]
		end
		if (loothog_countdownStarted) then
			HoldInfo = string.format(LOOTHOG_MSG_HOLDSTART, loothog_timeout_held)
			if (loothog_settings["announceextend"]) or loothog_manual then
				loothog_chat(HoldInfo)
			end
			loothog_countdownrunning = true
			loothog_numberOfSecondsToCountDown = 0
		end

	end
end

function loothog_AnnounceYetToRoll()
    local PeopleNeedingToRoll = "";
   	local unitIdentifier = ""	
-- Print list of players which have yet to roll
	for i = 1, #loothog_peopleToRoll, 1 do
		if(i == 1) then
			PeopleNeedingToRoll = loothog_peopleToRoll[i];
		else
			unitIdentifier = loothog_getUnitIdentifier(loothog_peopleToRoll[i])
			PeopleNeedingToRoll = PeopleNeedingToRoll .. ", " .. loothog_peopleToRoll[i];
		end
	end

	if ( string.find(PeopleNeedingToRoll, '%a') ) then
	   loothog_chat(string.format(LOOTHOG_MSG_YETTOPASS, LOOTHOG_PASS_PATTERN))
	   loothog_chat(PeopleNeedingToRoll);
	end
	 
end
function loothog_rollclicked()
	RandomRoll(1, 100)
end

function loothog_countdown_clicked()
	if (not loothog_active) then
		loothog_clear()
		loothog_manual = true
		loothog_active = true
		loothog_countdownclicked = true
		loothog_numberOfSecondsToCountDown = loothog_settings["autocounttime"] 
		loothog_timeout = loothog_settings["autocounttime"] + 1
		loothog_timer = GetTime()
		loothog_countdown()
		return
	end
	if loothog_countingdown then
		if loothog_countdownannounce == true then
			loothog_countdownannounce = false	
		else
			loothog_countdownannounce = true
		end
		return
	end
	if loothog_settings["timeout_enabled"] then
		loothog_numberOfSecondsToCountDown = loothog_settings["autocounttime"] + 1
		loothog_timeout = loothog_settings["autocounttime"]
		loothog_timer = GetTime()
		loothog_countdownclicked = true
	else
		loothog_numberOfSecondsToCountDown = loothog_settings["autocounttime"]
		loothog_countdownclicked = true
		loothog_manual = true
		loothog_countdown()
	end
end

function loothog_countdown()
	if (not loothog_countdownStarted) then
		loothog_countdownStarted = true
		loothog_countdownStartTime = GetTime()
	elseif (loothog_countdownStarted) then
		local currentTime = 0
		local elapsedTime = 0
		currentTime = GetTime()
		elapsedTime = currentTime - loothog_countdownStartTime
		if(elapsedTime >= 1) then
			loothog_countdownStartTime = GetTime()
			if (loothog_numberOfSecondsToCountDown <= loothog_settings["autocounttime"] + 1) then
				loothog_countingdown = true
				if(loothog_numberOfSecondsToCountDown > 1) and (loothog_countdownannounce) then
					loothog_chat(loothog_numberOfSecondsToCountDown - 1)
				end
			end
			loothog_numberOfSecondsToCountDown = loothog_numberOfSecondsToCountDown - 1
		end
		if (loothog_numberOfSecondsToCountDown == 0) then
			loothog_numberOfSecondsToCountDown = loothog_settings["autocounttime"]
			loothog_countdownStarted = false
			if loothog_manual then
				loothog_alldone()
			end
		end
	end
end

function loothog_chat(msg)
-- Send a chat message
	local channel = loothog_get_auto_channel()
	SendChatMessage(msg, channel)
end

function loothog_get_auto_channel()
-- Return an appropriate channel in order of preference: /raid, /p, /s
	local channel = "SAY"
	if (GetNumPartyMembers() > 0) then
		channel = "PARTY"
	end
	if (GetNumRaidMembers() > 0) then
		channel = "RAID"
	end
	return channel
end

--> Titan
function loothog_getwinner()
	return loothog_titanwinner
end

function loothog_getrolls()
	return loothog_titanrolls
end

function loothog_getnonrolls()
	return loothog_titannonrolls
end

function loothog_getstatus()
	if (loothog_titanPanelActive) then
		if (loothog_hold) then
			loothog_titanstatus = RED_FONT_COLOR_CODE  .. LOOTHOG_LABEL_HOLDING .. FONT_COLOR_CODE_CLOSE
		else
			if loothog_settings["timeout_enabled"] then
				local titan_time_left = math.ceil(loothog_last_roll + loothog_timeout - GetTime())
				loothog_titanstatus = GREEN_FONT_COLOR_CODE  .. string.format(LOOTHOG_LABEL_TIMELEFT, titan_time_left) .. FONT_COLOR_CODE_CLOSE
			else
				loothog_titanstatus = GREEN_FONT_COLOR_CODE  .. LOOTHOG_LABEL_NOTIMEOUT .. FONT_COLOR_CODE_CLOSE
			end
		end
-- See if it's time to go inactive:
		if (not loothog_hold) and (GetTime() - loothog_timer > loothog_timeout) and (loothog_settings["timeout_enabled"]) then
			loothog_make_inactive()
		end
	end

	return loothog_titanstatus
end
--<Titan

-- DBM This function should kick out the top roll if the loot leader thinks it was a joke roll, or was otherwise ineligible.  
-- It will broadcast a message explaining the kick, and will make the roller eligible to roll again if they would like.
--
-- This is needed because otherwise there is no way to remove the top roll and use the announce functionality.
-- Functionality to remove any arbitrary roll in the list would be preferred, but is infeasible given the text field structure of the LH main window.
-- 
function loothog_kickRoll()
	local player = ""
	if (#loothog_rolls > 0) then

		player = loothog_rolls[1][1]

		loothog_chat(string.format(LOOTHOG_MSG_REMOVEROLL, player, loothog_rolls[1][2]))
		table.remove(loothog_rolls, 1)
	
-- Delete from rolled list
		loothog_players[player] = false
		loothog_player_count = loothog_player_count - 1
	
-- Add player whose roll was kicked back onto the unrolled list
		table.insert(loothog_peopleToRoll, player)

-- Update screen
		loothog_repaint()
	end
end

-- DBM This function re-paints the main LH window with roll results and people yet to roll, along with the Titan plug-in.
-- I extracted it from it's original location in loothog_processroll it because it is also needed by the new "loothog_kickRoll" function
function loothog_repaint()
-- Update the list onscreen:
	local text1 = ""
	local text2 = ""
-->Titan
	local text3 = ""
	local text4 = ""
--<Titan
	local localClass = ""
	local unitString = ""
	local englishClass = ""
	local unitLevel = "-"
	local unitIdentifier = ""
	

-- Get number of winners
	local winners_num = loothog_rollTied()
	
-- Print those who have already rolled
	for i, p in ipairs(loothog_rolls) do
		if(loothog_isInGroup(p[1])) then
			localClass, englishClass = loothog_classes[p[1]].LocalClass, loothog_classes[p[1]].EnglishClass
			unitLevel = loothog_classes[p[1]].UnitLevel
			
-- Check if supplied player name is the local player
			if (UnitName("player") == p[1]) then
				text1 = text1 .. "|cffff0000" .. p[1] .. "-" .. localClass .."(" .. unitLevel .. ")" .. "|r\n"
				text2 = text2 .. "|cffff0000" .. p[2] .. "|r\n"
			else
				text1 = text1 .. p[1] .. "-" .. localClass .."(" .. unitLevel .. ")" .. "\n"
				text2 = text2 .. p[2] .. "\n"
			end
			if(loothog_titanPanelActive) then
				-->Titan
				text3 = text3 .. GREEN_FONT_COLOR_CODE .. p[1] .. "-" .. localClass .."(" .. unitLevel .. ")" .. FONT_COLOR_CODE_CLOSE  .. "\t" .. GREEN_FONT_COLOR_CODE ..  p[2] .. FONT_COLOR_CODE_CLOSE .. "\n"
				--<Titan
			end
		else
			text1 = text1 .. p[1] .. "\n"
			text2 = text2 .. p[2] .. "\n"
			if(loothog_titanPanelActive) then
				-->Titan
				text3 = text3 .. GREEN_FONT_COLOR_CODE .. p[1] .. "\t" .. p[2] .. FONT_COLOR_CODE_CLOSE  .. "\n"
			end
		end
		
-- Printing delimiter to seperate multiple winners for visual clarity
		if((i == winners_num) and (i > 1)) then
			text1 = text1 .. LOOTHOG_LABEL_WINNERSDELIMITER .. "\n"
			text2 = text2 .. "===" .. "\n"
		end
	end
	
-- Printing a delimiter
	if((#loothog_rolls > 0) and (#loothog_peopleToRoll > 0)) then
		text1 = text1 .. LOOTHOG_LABEL_DELIMITER .. "\n"
	end
	
-- Print list of players which have yet to roll
	local unitIdentifier = ""	
	for i = 1, #loothog_peopleToRoll, 1 do
		localClass, englishClass = loothog_classes[loothog_peopleToRoll[i]].LocalClass, loothog_classes[loothog_peopleToRoll[i]].EnglishClass
		unitLevel = loothog_classes[loothog_peopleToRoll[i]].UnitLevel
		
		text1 = text1 .. loothog_peopleToRoll[i] .. "-" .. localClass .."(" .. unitLevel .. ")" .. "\n"
		if(loothog_titanPanelActive) then
-->Titan
			text4 = text4 .. RED_FONT_COLOR_CODE .. loothog_peopleToRoll[i] .. "-" .. localClass .."(" .. unitLevel .. ")" .. FONT_COLOR_CODE_CLOSE  .. "\n"
--<Titan
		end
	end

	loothog_mainText1:SetText(text1)
	loothog_mainText2:SetText(text2)
	loothog_update_counts()
	
-->Titan
	loothog_titanrolls = text3
	loothog_titannonrolls = text4
	loothog_titan_active = true
--<Titan


-- Make the text white to indicate rolling is active:
	loothog_active = true
	loothog_mainText1:SetTextColor(1, 1, 1)
	loothog_mainText2:SetTextColor(1, 1, 1)
	
-- Grow the window if needed:	
	growWindow()

-- Update the last roll timestamp:
	loothog_last_roll = GetTime()
	if (loothog_settings["autoextend"]) and (loothog_timer + loothog_timeout - GetTime() < loothog_autoextendtime ) and (not loothog_duplicate) then 
		loothog_timer = loothog_last_roll
		loothog_timeout = loothog_autoresettime
		loothog_numberOfSecondsToCountDown = loothog_timeout
		if loothog_settings["announceextend"] then
			ResetInfo = string.format(LOOTHOG_MSG_RESET, loothog_timeout)
		end
		loothog_chat(ResetInfo)
	end
end

function loothog_alldone()
	if (loothog_settings["finalize"]) then
		loothog_announce_winner()
	end
		loothog_make_inactive()
end

function loothog_listtochat()
		loothog_chat(LOOTHOG_LABEL_CHATLISTTOP)
		loothog_chat(LOOTHOG_LABEL_WINNERSDELIMITER)
		local roll_counter_target = table.getn(loothog_rolls)
		if (roll_counter_target > loothog_settings["listtochatno"]) then
			roll_counter_target = loothog_settings["listtochatno"]
		end
		local roll_counter = 1
		local report_roll = ""
		while (roll_counter >= 1 and roll_counter <= roll_counter_target) do
			loothog_chat(string.format(LOOTHOG_MSG_ROLLS, roll_counter, loothog_rolls[roll_counter][1], loothog_rolls[roll_counter][2]))
			roll_counter = roll_counter + 1
		end
		loothog_chat(report_roll)
end