-- German Localization:
-- Done by Suan(Kaz'goroth)

if (GetLocale()=="deDE") then
	BINDING_NAME_LOOTHOGTOGGLE = "Show/Hide Window"
	BINDING_HEADER_LOOTHOG = "LootHog"
	LOOTHOG_ROLL_PATTERN = "(.+) würfelt. Ergebnis: (%d+) %((%d+)%-(%d+)%)" --hopefully fixes the Â character problems
	LOOTHOG_PASS_PATTERN = "passe"
	LOOTHOG_RESETONWATCH_PATTERN = "hat mit einem Wurf von"
	LOOTHOG_RESETONWATCH_PATTERN2 = "dasselbe Ergebnis gehabt!"

-- size of the main and options/config window
	LOOTHOG_UI_MAIN_WIDTH = "190"
	LOOTHOG_UI_CONFIG_WIDTH = "450"
	LOOTHOG_UI_CONFIG_HEIGHT = "430"

	LOOTHOG_LABEL_DELIMITER = "------------------"
	LOOTHOG_LABEL_WINNERSDELIMITER = "=================="
	LOOTHOG_LABEL_CHATLISTTOP = "Würfe in absteigender Reihenfolge"
	LOOTHOG_LABEL_ROLLS = "LootHog Würfelergebnisse"
	LOOTHOG_LABEL_READY = "Bereit..."	-- Means that LootHog is ready to receive /random rolls.
	LOOTHOG_LABEL_OPTIONS = "LootHog Optionen"
	LOOTHOG_LABEL_HOLDING = "Warten..."	-- Means that the user has clicked "Hold" to prevent the timeout.
	LOOTHOG_LABEL_NOTIMEOUT = "TimeOut aus."
	LOOTHOG_LABEL_TIMELEFT = "TimeOut: %s Sekunden"	-- (seconds)
	LOOTHOG_LABEL_COUNTDOWN = "Countdown"
	LOOTHOG_LABEL_COUNT = "Würfe:%s, Spieler:%s"	-- (roll count, player count)
	LOOTHOG_LABEL_NEW_ROLL_TEXT = "Neue Würfelrunde startet jetzt!"
  
	LOOTHOG_BUTTON_CLEAR = "Löschen"
	LOOTHOG_BUTTON_OPTIONS = "Optionen"
	LOOTHOG_BUTTON_HOLD = "Warten"
	LOOTHOG_BUTTON_UNHOLD = "Weiter"
	LOOTHOG_BUTTON_ANNOUNCE = "Ansagen"
	LOOTHOG_BUTTON_YETTOROLL = "Nicht gewürfelt"
	LOOTHOG_BUTTON_INFO = "Entfernen"
	LOOTHOG_BUTTON_ROLL = "Würfeln (1-100)"
	LOOTHOG_BUTTON_PASS = "Passen"
	LOOTHOG_BUTTON_COUNTDOWN = "Countdown"


	LOOTHOG_OPTION_ENABLE = "LootHog aktivieren"	
	LOOTHOG_OPTION_AUTOSHOW = "LootHog automatisch zeigen sobald jemand würfelt"	
	LOOTHOG_OPTION_RESETONWATCH = "LootHog deaktivieren wenn jemand anderes den Gewinner ankündigt"	
	LOOTHOG_OPTION_GROUPONLY = "Würfelergebisse nur von Gruppenmitgliedern anzeigen"
	
	LOOTHOG_OPTION_LISTTOCHAT = "Ausgabe von maximal"
	LOOTHOG_OPTION_LISTTOCHAT2 = "Würfelergebnisse in den Chat"

	LOOTHOG_OPTION_PREVENT = "Ausgabe der /random Befehle im Chat verhindern"
	LOOTHOG_OPTION_CLOSEONANNOUNCE = "LootHog schließen nach Bekanntgabe des Siegers"
	LOOTHOG_OPTION_ACK = "Bestätige Würfe über /whisper an die Spieler"

	LOOTHOG_OPTION_REJECT = "Lehne Würfe mit Werten ausserhalb von (1-100) ab"
    LOOTHOG_OPTION_ANNOUNCEREJECT = "Abgelehnte Würfe im Chatfenster ansagen"
	
	LOOTHOG_OPTION_ANNOUNCEONCLEAR = "Beim start ausgeben:"
    LOOTHOG_OPTION_ANNOUNCETIMEOUT = "TimeOut beim start bekanntgeben"
  	
  LOOTHOG_OPTION_TIMEOUT1 = "LootHog automatisch ausblenden nach"	
  LOOTHOG_OPTION_TIMEOUT2 = "Sekunden"
  	  	
  	LOOTHOG_OPTION_AUTOCOUNTDOWN = "Automatischer Countdown in den letzen"
		
    LOOTHOG_OPTION_AUTOEXTEND = "Timer auf"
	  LOOTHOG_OPTION_TIMEOUT3 = "Sek. setzen, bei einen Wurf in den letzen"
	
	  LOOTHOG_OPTION_ANNOUNCEEXTEND = "Erweiterte Ansagen"

	LOOTHOG_OPTION_FINALIZE = "Beenden der Würfelrunde nach Ablauf des TimeOuts"
	LOOTHOG_OPTION_FINALIZEROLLS = "Beenden der Würfelrunde sobald alle gewürfelt haben"

	
	LOOTHOG_MSG_LOAD = "LootHog v%s wurde geladen. Zum öffnen der Optionen /loothog tippen."	-- (version)
	LOOTHOG_MSG_INFO = "LootHog-Information: Durch Tippen des Textes \"%s\" kann man bei einer Würfelrunde verzichten."	-- (pass pattern)
	LOOTHOG_MSG_CHEAT = "%ss Wurf von %s (%s-%s) wird ignoriert."	-- (player, roll, max_roll, min_roll)
	LOOTHOG_MSG_ACK = "Dein Wurf von (%s) wurde gezählt.	Viel Glück!"	-- (roll)
	LOOTHOG_MSG_ACK_PASS = "Du hast darauf verzichtet zu würfeln!"
	LOOTHOG_MSG_DUPE = "%s hat mehr als einmal gewürfelt. Der Wurf wird nicht gezählt!"	-- (player)
	LOOTHOG_MSG_WINNER = "%s%s hat mit einem Wurf von %s gewonnen! (%s-%s)" --(player, group, roll, min, max)
	LOOTHOG_MSG_ROLLS = "%s: %s - %s"	-- (roll posistion, player, roll)
	LOOTHOG_MSG_GROUP = "Gruppe"
	LOOTHOG_MSG_NOTGROUP= "Gruppe nicht gefunden"
	LOOTHOG_MSG_STAT = "Gesamtanzahl der Würfe: "
	LOOTHOG_MSG_HOLDCONT = "Loothog: Zähle Countdown bei %s Sekunden weiter."
	LOOTHOG_MSG_HOLDSTART = "Loothog: Countdown bei %s Sekunden angehalten."
	LOOTHOG_MSG_RESET = "Loothog: Neuer Wurf gefunden, Countdown auf %s Sekunden zurückgesetzt."
	LOOTHOG_MSG_TIMEANNOUNCE = "Nach %s Sekunden wird die Würfelrunde beendet!"
	

-- The following variables are used to build a string of this type, in the case of a tie:
-- "Gnomechomsky, Saucytails, and Pionerka tied with 98's!"
-- I don't know if a literal substitution can provide a proper translation. Feedback is welcome. :)
	LOOTHOG_MSG_AND = " und "
	LOOTHOG_MSG_TIED = " haben jeweils mit %s dasselbe Ergebnis gehabt!"	-- (roll)
	LOOTHOG_MSG_YETTOPASS = "Folgende Spieler haben noch nicht gew\195\188rfelt oder verzichtet (per schreiben von %s in den Chat):" --(LOOTHOG_PASS_PATTERN)
	LOOTHOG_MSG_REMOVEROLL = "LootHog: %ss Ergebnis von %s wurde entfernt." -- (player, roll)
end