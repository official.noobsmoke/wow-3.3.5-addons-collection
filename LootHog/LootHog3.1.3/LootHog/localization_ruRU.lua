﻿-- Russian Localization:
-- Done by 

if (GetLocale()=="ruRU") then
	BINDING_NAME_LOOTHOGTOGGLE = "Show/Hide Window"
	BINDING_HEADER_LOOTHOG = "LootHog"
	LOOTHOG_ROLL_PATTERN = "(.+) rolls (%d+) %((%d+)%-(%d+)%)" --hopefully fixes the Â character problems
	LOOTHOG_PASS_PATTERN = "pass"
	LOOTHOG_RESETONWATCH_PATTERN = " won with a roll of"
	LOOTHOG_RESETONWATCH_PATTERN2 = "tied with"

	-- size of the main and options/config window
	LOOTHOG_UI_MAIN_WIDTH = "190"
	LOOTHOG_UI_CONFIG_WIDTH = "410"
	LOOTHOG_UI_CONFIG_HEIGHT = "430"

	LOOTHOG_LABEL_DELIMITER = "------------------"
	LOOTHOG_LABEL_WINNERSDELIMITER = "=================="
	LOOTHOG_LABEL_CHATLISTTOP = "Rolls in Ascending Order"
	LOOTHOG_LABEL_ROLLS = "/random Rolls"
	LOOTHOG_LABEL_READY = "Ready..."	-- Means that LootHog is ready to receive /random rolls.
	LOOTHOG_LABEL_OPTIONS = "LootHog Options"
	LOOTHOG_LABEL_HOLDING = "Holding..."	-- Means that the user has clicked "Hold" to prevent the timeout.
	LOOTHOG_LABEL_NOTIMEOUT = "Timeout disabled."
	LOOTHOG_LABEL_TIMELEFT = "Timeout: %s seconds"	-- (seconds)
	LOOTHOG_LABEL_COUNTDOWN = "Countdown"
	LOOTHOG_LABEL_COUNT = "Rolls:%s, Players:%s"	-- (roll count, player count)
	LOOTHOG_LABEL_NEW_ROLL_TEXT = "New roll starting now !"

	LOOTHOG_BUTTON_CLEAR = "Clear"
	LOOTHOG_BUTTON_OPTIONS = "Options"
	LOOTHOG_BUTTON_HOLD = "Hold"
	LOOTHOG_BUTTON_UNHOLD = "UnHold"
	LOOTHOG_BUTTON_ANNOUNCE = "Announce"
	LOOTHOG_BUTTON_YETTOROLL = "Not rolled"
	LOOTHOG_BUTTON_INFO = "Kick Roll"
	LOOTHOG_BUTTON_ROLL = "Roll (1-100)"
	LOOTHOG_BUTTON_PASS = "Pass"
	LOOTHOG_BUTTON_COUNTDOWN = "Countdown"
	LOOTHOG_OPTION_AUTOCOUNTDOWN = "Automatically countdown last"
	LOOTHOG_OPTION_AUTOEXTEND = "reset timer to"
	LOOTHOG_OPTION_ENABLE = "Enable LootHog"
	LOOTHOG_OPTION_FINALIZE = "Announce after timeout expires"
	LOOTHOG_OPTION_FINALIZEROLLS = "Finalize rolling when all group members have rolled"
	LOOTHOG_OPTION_GROUPONLY = "Count rolls from group members only"
	LOOTHOG_OPTION_AUTOSHOW = "Automatically show window when someone rolls"
	LOOTHOG_OPTION_PREVENT = "Prevent /random rolls from appearing in the chatlog"
	LOOTHOG_OPTION_CLOSEONANNOUNCE = "Close LootHog window after announcing a winner"
	LOOTHOG_OPTION_ACK = "Acknowledge rolls via /whisper to roller"
	LOOTHOG_OPTION_REJECT = "Reject rolls with bounds other than (1-100)"
	LOOTHOG_OPTION_ANNOUNCEREJECT = "Announce rejected rolls"
	LOOTHOG_OPTION_ANNOUNCEEXTEND = "Announce roll extenstions"
	LOOTHOG_OPTION_TIMEOUT1 = "Auto-hide window after "
	LOOTHOG_OPTION_TIMEOUT2 = " seconds"
	LOOTHOG_OPTION_TIMEOUT3 = "		 seconds if /roll detected in final "
	LOOTHOG_OPTION_ANNOUNCEONCLEAR = "Announce on Clear: "
	LOOTHOG_OPTION_ANNOUNCETIMEOUT = "Announce timeout on start"
	LOOTHOG_OPTION_RESETONWATCH = "Deactivate LootHog if someone else announces"
	LOOTHOG_OPTION_LISTTOCHAT = "Submit top"
	LOOTHOG_OPTION_LISTTOCHAT2 = "     rolls to chat when announcing"

	LOOTHOG_MSG_LOAD = "LootHog v%s loaded.	For options, type /loothog."	-- (version)
	LOOTHOG_MSG_INFO = "LootHog-Information: You may pass on a running roll by typing: %s"	-- (pass pattern)
	LOOTHOG_MSG_CHEAT = "Ignoring %s's roll of %s (%s-%s)."	-- (player, roll, max_roll, min_roll)
	LOOTHOG_MSG_ACK = "Got your roll (%s).	Good luck!"	-- (roll)
	LOOTHOG_MSG_ACK_PASS = "You have passed on this roll."	-- (pass)
	LOOTHOG_MSG_DUPE = "%s has rolled more than once!"	-- (player)
	LOOTHOG_MSG_WINNER = "%s%s won with a roll of %s!	(%s-%s)	%s"	-- (player, group, roll, min, max, raidpointmsg)
	LOOTHOG_MSG_ROLLS = "%s: %s rolled %s."	-- (roll posistion, player, roll)
	LOOTHOG_MSG_GROUP = "group"
	LOOTHOG_MSG_NOTGROUP= "group not found"
	LOOTHOG_MSG_STAT = "Gesamtanzahl der W\195\188rfe: "
	LOOTHOG_MSG_SECONDS = "seconds"
	LOOTHOG_MSG_HOLDCONT = "Loothog is restarting countdown at"
	LOOTHOG_MSG_HOLDSTART = "Loothog is holding countdown at"
	LOOTHOG_MSG_TIMEANNOUNCE1 = "You have "
	LOOTHOG_MSG_TIMEANNOUNCE2 = " seconds to roll!"
	LOOTHOG_MSG_RESET = "New Roll Detected restating countdown at "

-- The following variables are used to build a string of this type, in the case of a tie:
-- "Gnomechomsky, Saucytails, and Pionerka tied with 98's!"
-- I don't know if a literal substitution can provide a proper translation. Feedback is welcome. :)
	LOOTHOG_MSG_AND = " and "
	LOOTHOG_MSG_TIED = " tied with %s's!	Please re-roll using the same points, if any."	-- (roll)
	LOOTHOG_MSG_YETTOPASS = "The following people still need to roll or say %s" --(LOOTHOG_PASS_PATTERN)
	LOOTHOG_MSG_REMOVEROLL = "<LootHog>: %s's roll of %s removed from consideration."
end