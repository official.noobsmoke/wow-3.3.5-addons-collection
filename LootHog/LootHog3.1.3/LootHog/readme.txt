LootHog detects and tracks rolls which are made with '/random' or '/roll' and sorts them, allowing raid leaders to announce roll winners quickly and easily.

Features/Options
################

* Automatically show window when someone rolls
* Configurable timer: Starts when the first roll is detected.
* Acknowledge rolls via /whisper to roller
* Announce that new roll has started, and announce the time players have to roll.
* Prevent /random rolls from appearing in the chatlog
* Count rolls from group members only or entire raid
* Reject rolls with bounds other than (1-100)
* Reject multiple rolls
* Ability to "Hold" timer.  After releasing the timer, timer will restart from where it left off.
  -  OR if AutoExtend is turned on, it will restart from configured time if timer is lower.
* Countdown - Manually activated or can start in the last few seconds of timer (configurable)
* AutoExtend if roll is detected in the last few seconds of timer (configurable)
* Finalize rolling and announce winner after configurable timer expires
* Announce the group of the winner (Raid)
* Submit complete list of rolls to chat when announcing
* Deactivate LootHog if someone else announces  (using Loothog)
* Close LootHog window after announcing a winner
* MyAddon - Support

Commands:
#########

/lh or /loothog - Open the LootHog window
/lh options or /lh config - Open the options screen
/lh countdown - Starts countdown
/lh kick - Kicks the top roll from consideration
/lh clear - Clears all rolls ready for next set
/lh hold - Holds the countdown at current time
/lh announce - Announces the winner (and top rolls if configured)
/lh notrolled - Lists the members of the party not yet rolled
/lh list - Lists number of rolls as configured.


Autors:
#######

Erytheia
IronEagleNZ


Special Thanks to:
##################

Chompers - for the original addon
Suan(Kaz'goroth) - for addition code and german translation
m0rgoth - for french translation
Codeus

IMPORTANT!
##########

I'm not the original creator of the addon, i only updated some small tings (2.7.1 and up)
Full Credits goes to Chompers!
http://wowui.worldofwar.net/?p=mod&m=3570
http://wowui.worldofwar.net/?p=mod&m=756 

Changelog
#########

3.1.3
-----
* BUGFIX: loading old settings from a previous version should now work
* BUGFIX: design-changes in the option window 
* some minor changes in the localisation files
* Update German localisation
* remove statistic functions (need only memory!)


3.1.2
-------
* BUGFIX : Current rollset will clear when Countdown is clicked if timeout expired or winner has been announced


3.1.1
-------
* Changed Option "Announce on Clear" to "Announce on Start".  Clearing the rollset will still announce
* Added an option to announce the timeout when new roll set started
* Countdown logic changed.  Added option to turn on/off Auto Countdown
* Clicking countdown manually will advance timer to configured countdown time
	- If Timeout is disabled clicking countdown will start countdown at configured time
* Second click of countdown will "silence" the countdown
* Added slash commands : 	/loothog countdown
				/loothog kick
				/loothog clear
				/loothog hold
				/loothog announce
				/loothog notrolled
				/loothog list - Lists number of rolls as configured.
  Addon should now be able to be fully functional without opening window if you prefer
* Option to change the number of rolls to announce.  Was previously all in raid.  (Too much spam for 25 man raids)
* BUGFIX : New rolls will no longer reset timer
	-  If a new roll is detected when timer is below threshold, option to reset to threshold.
* BUGFIX : "Hold" will no longer reset timer, unless timer below threshold and configured to do so
	-  Threshold is configurable.
* Clicking Clear no longer automatically closes the loothog window.
* Added option to finalize when all group members have rolled.

3.1.0
-----
* Only in raid the group number is announced - not only on loot master
* Small Size-Bug corrected - Main Window should now a little bit smaller
* Should now work with a spanish client (not fulled tested! and still not complete translated)
* Move Version to 3.1 - because a old 3.0.0 version exist
* Change title of main window to "LootHog Rolls"
* Show and Option-Button in Interface/Addons
* MyAddons - Support moved into the toc file
* Add "/lh config"
* Little Layout-Change in config window.
* Timeout now announced in chat, when "Finalize rolling after timeout expires" is activ
* Now the countdown start value can be set 

2.7.1
-----
* "Group 1"/"Group not found"-Translation is now possible
* Add support for localization (koKR, zhCN, zhTW, ruRU, esES, esMX)
  (at the moment nothing is translated! when you have translated one file, please
  send it to me (info (ad) gpihome.de)

2.7 (Susan)
-----------
* Update Toc