assert(FatCooldowns, "FatCooldowns not found!")

local MINOR_VERSION = tonumber(("$Revision: 48 $"):match("%d+"))
if MINOR_VERSION > FatCooldowns.MINOR_VERSION then FatCooldowns.MINOR_VERSION = MINOR_VERSION end

local Media = LibStub("LibSharedMedia-3.0")
local textures = Media:List("statusbar")
local fonts = Media:List("font")
--local backgrounds = Media:List("background")
--local borders = Media:List("border")

local pairs, ipairs = _G.pairs, _G.ipairs
local tonumber, tostring = _G.tonumber, _G.tostring

local Display = FatCooldowns:GetModule("Display")

Media.RegisterCallback(Display, "LibSharedMedia_Registered", "UpdateDisplay")
Media.RegisterCallback(Display, "LibSharedMedia_SetGlobal", "UpdateDisplay")

-- Omen rip; most of this file is, actually
local function GetLSMIndex(t, value)
	for k, v in pairs(Media:List(t)) do
		if v == value then
			return k
		end
	end
	return nil
end

local function set(t, value)
	Display.db.profile[t[#t]] = value
	Display:UpdateDisplay()
end
local function get(t)
	return Display.db.profile[t[#t]]
end

local options = {}

local function ProfileChanged()
	
end

local function RefreshFrameListOptionsAndVariables()
	local optArgs = options.args.frames.args
	local classes = {"DEATHKNIGHT", "DRUID", "HUNTER", "MAGE", "PALADIN", "PRIEST", "ROGUE", "SHAMAN", "WARLOCK", "WARRIOR"}
	local Display = Display
	
	for i = 1, 10, 1 do
		if i <= Display.db.profile.frameCount then
			if not Display.db.profile.frames[i] then
				local newFrame = {};
				newFrame.name = "Frame"..i;
				newFrame.growUp = Display.db.profile.frames[i-1].growUp;
				newFrame.hideSelf = Display.db.profile.frames[i-1].hideSelf;
				newFrame.readyMsg = Display.db.profile.frames[i-1].readyMsg;
				newFrame.scale = Display.db.profile.frames[i-1].scale;
				newFrame.alpha = Display.db.profile.frames[i-1].alpha;
				newFrame.width = Display.db.profile.frames[i-1].width;
				newFrame.height = Display.db.profile.frames[i-1].height;
				newFrame.maxTextLength = Display.db.profile.frames[i-1].maxTextLength;
				newFrame.permanent = Display.db.profile.frames[i-1].permanent;
				newFrame.anchor = Display.db.profile.frames[i-1].anchor;
				newFrame.hideFrame = Display.db.profile.frames[i-1].hideFrame;
				newFrame.RaidMsg = Display.db.profile.frames[i-1].RaidMsg;
				--.posX = 0;
				--newFrame.posY = 0;
				newFrame.spells = {}
				
				Display.db.profile.frames[i] = newFrame;
			end
			
			if not optArgs["frame"..i] then
				optArgs["frame"..i] = {
					type = "group",
					name = Display.db.profile.frames[i].name .. " Options",
					desc = "Configure frame options and spells displayed",
					cmdHidden = true,
					order = i,
					args = {
						name = {
							type = "input",
							name = "Frame Name",
							desc = "Choose name of this frame",
							get = function(v) return Display.db.profile.frames[i].name end,
							set = function(info, v) 
								--print(info[#info])
								Display.db.profile.frames[i].name = v; optArgs["frame"..i].name = v .. " Options";
								Display:UpdateDisplay();
							end,
							width = "full",
							order = 1
						},
						maxTextLength = {
							type = "range",
							name = "Nickname characters to display",
							min = 3, max = 12, step = 1, bigStep = 1,
							get = function(v) return Display.db.profile.frames[i].maxTextLength end,
							set = function(info, v) Display.db.profile.frames[i].maxTextLength = v; Display:UpdateDisplay() end,
							order = 2,
							width = "full",
						},
						hide = {
							type = "toggle",
							name = "Hide Frame",
							desc = "Hides this frame.",
							order = 3,
							get = function(v) return Display.db.profile.frames[i].hideFrame end,
							set = function(info, v) Display.db.profile.frames[i].hideFrame = v; Display:UpdateDisplay(); end,
						},
						anchor = {
							type = "toggle",
							name = "Show Anchor",
							desc = "Shows Anchor with frame name.",
							order = 4,
							get = function(v) return Display.db.profile.frames[i].anchor end,
							set = function(info, v) Display.db.profile.frames[i].anchor = v; Display:UpdateDisplay() end,
						},
						hideSelf = {
							type = "toggle",
							name =  "Hide Self",
							desc =  "Hide your own cooldowns.",
							order = 5,
							get = function(v) return Display.db.profile.frames[i].hideSelf end,
							set = function(info, v) Display.db.profile.frames[i].hideSelf = v; Display:UpdateDisplay() end,
						},
						message = {
							type = "toggle",
							name =  "Ready Message",
							desc =  "Print a chat message after a cooldown is ready.",
							order = 6,
							get = function(v) return Display.db.profile.frames[i].readyMsg end,
							set = function(info, v) Display.db.profile.frames[i].readyMsg = v; Display:UpdateDisplay() end,
						},
						permanent = {
							type = "toggle",
							name = "Show when ready",
							desc = "Sets spells in this bar to be displayed for all raid members even when ready.",
							get = function(v) return Display.db.profile.frames[i].permanent end,
							set = function(info, v) 
								if v then
									Display.db.profile.frames[i].permanent = v;  
									if FatCooldowns.raidRoster then Display:RaidUpdate(FatCooldowns.raidRoster,{},i) end
								else
									if FatCooldowns.raidRoster then Display:RaidUpdate({},FatCooldowns.raidRoster,i) end
									Display.db.profile.frames[i].permanent = v;  
								end
								
								Display:UpdateDisplay()
							end,
							order = 7,
						},
						growUp = {
							type = "toggle",
							name =  "Grow Up",
							desc =  "Grow bars upwards",
							get = function(v) return Display.db.profile.frames[i].growUp end,
							set = function(info, v) Display.db.profile.frames[i].growUp = v; Display:UpdateDisplay() end,
							order = 8,
						},
						scale = {
							type = "range",
							name =  "Scale",
							desc =  "Control the scale of the entire FatCooldowns GUI",
							min = 1, max = 150, step = 1, bigStep = 1,
							get = function(v) return Display.db.profile.frames[i].scale end,
							set = function(info, v) Display.db.profile.frames[i].scale = v; Display:UpdateDisplay() end,
							order = 9,
						},
						alpha = {
							type = "range",
							name =  "Alpha",
							desc =  "Control the transparency of the entire FatCooldowns GUI",
							min = 0, max = 100, step = 5, bigStep = 10,
							get = function(v) return Display.db.profile.frames[i].alpha end,
							set = function(info, v) Display.db.profile.frames[i].alpha = v; Display:UpdateDisplay() end,
							order = 10,
						},
						height = {
							type = "range",
							name =  "Height",
							min = 1, max = 20, step = 1, bigStep = 1,
							get = function(v) return Display.db.profile.frames[i].height end,
							set = function(info, v) Display.db.profile.frames[i].height = v; Display:UpdateDisplay() end,
							order = 11,
						},
						width = {
							type = "range",
							name =  "Width",
							min = 50, max = 500, step = 5, bigStep = 10,
							get = function(v) return Display.db.profile.frames[i].width end,
							set = function(info, v) Display.db.profile.frames[i].width = v; Display:UpdateDisplay() end,
							order = 12,
						},
						messageraid = {
							type = "toggle",
							name =  "Ready Message (Raid)",
							desc =  "Print a chat message after a cooldown is ready to raid chat.",
							order = 13,
							get = function(v) return Display.db.profile.frames[i].RaidMsg end,
							set = function(info, v) Display.db.profile.frames[i].RaidMsg = v; Display:UpdateDisplay() end,
						},
						--[[posX = {
							type = 'input',
							name =  "X Position",
							get = function(info) return tostring(Display.db.profile.posX) end,
							set = function(info, v)
								Display.db.profile.posX = tonumber(v)
								Display:SetAnchors(true)
							end,
							order = 8,
							width = "full",
						},
						posY = {
							type = 'input',
							name =  "Y Position",
							get = function(info) return tostring(Display.db.profile.posY) end,
							set = function(info, v)
								Display.db.profile.posY = tonumber(v)
								Display:SetAnchors(true)
							end,
							order = 9,
							width = "full",
						},--]]
					},
				};
				
				
				local tab = optArgs["frame"..i].args
				local orderIndex = 15 -- Sorting
				
				for _, class in ipairs(classes) do
					-- Separate class spells with a header
					tab[class] = {
						type = "header",
						name =  class,
						cmdHidden = true,
						order = orderIndex,
					}
					orderIndex = orderIndex + 1
					
					-- Add an option entry for each spell
					for spellInfo, spellTable in pairs(FatCooldowns.cooldowns[class]) do
						tab[tostring(spellTable.id)] = {
							type = "toggle",
							name = type(spellInfo) == "number" and (GetSpellInfo(spellInfo) .. " [#" .. spellInfo .. "]") or spellInfo,
							desc =  string.format("Display %s cooldowns",spellInfo),
							order = orderIndex,
							get = function(info) return Display.db.profile.frames[i].spells[spellTable.id] end,
							set = function(info, val)
								Display.db.profile.frames[i].spells[spellTable.id] = val
								Display:SpellToggle(i,spellTable.id, val)
							end,
							cmdHidden = true,
						}
						orderIndex = orderIndex + 1
					end
				end
			end
			
			optArgs["frame"..i].name = Display.db.profile.frames[i].name .. " Options";
			
			
		else
			-- hide config
			optArgs["frame"..i] = nil;
		end	
	end
	
	if not options.args.spellOrder.args["WARRIOR"] then
		local tab = options.args.spellOrder.args
		local orderIndex = 2 
		
		for _, class in ipairs(classes) do
			-- Separate class spells with a header
			tab[class] = {
				type = "header",
				name =  class,
				cmdHidden = true,
				order = orderIndex,
			}
			orderIndex = orderIndex + 1
			
			-- Add an option entry for each spell
			for spellInfo, spellTable in pairs(FatCooldowns.cooldowns[class]) do
				tab[tostring(spellTable.id)] = {
					type = "toggle",
					name = type(spellInfo) == "number" and (GetSpellInfo(spellInfo) .. " [#" .. spellInfo .. "]") or spellInfo,
					desc =  string.format("Select %s",spellInfo),
					order = orderIndex,
					get = function(info) return Display.db.profile.orderSpells[spellTable.id] end,
					set = function(info, val)
						if val then
							Display.db.profile.orderSpells[spellTable.id] = val;
						else
							Display.db.profile.orderSpells[spellTable.id] = nil;
							Display.db.profile.orderSpellPriorities[spellTable.id] = nil;
							FatCooldowns.spellInfoTable[spellTable.id] = 500;
						end
						FatCooldowns.RefreshFrameListOptionsAndVariables();
					end,
					cmdHidden = true,
				}
				orderIndex = orderIndex + 1
			end
		end
	end	
	
	options.args.spellOrder.args.spells.args = {
		descr = {
			type = "description",
			name =  "All unselected spells have priority 500 - select higher or lower to move your spell higher or lower in priority list. Higher priority is closer to anchor.",
			width = "full",
			order = 1,
		},
	};
	
	local orderIndex = 2 
	for spellID, on in pairs(Display.db.profile.orderSpells) do
		options.args.spellOrder.args.spells.args[tostring(spellID)] =  {
			type = "range",
			name =  string.format("%s",GetSpellInfo(spellID)),
			desc =  "Set spell's display order priority",
			min = 1, max = 1000, step = 1, bigStep = 10,
			get = function(info) 
				if Display.db.profile.orderSpellPriorities[spellID] then FatCooldowns.spellInfoTable[spellID].order = Display.db.profile.orderSpellPriorities[spellID]; end
				return Display.db.profile.orderSpellPriorities[spellID] or 500 
			end,
			set = function(info, val)
				Display.db.profile.orderSpellPriorities[spellID] = val
			end,
			order = 2,
			width = "full",
		};
		
		orderIndex = orderIndex + 1
	end
	Display:UpdateDisplay()
end


options = {
	type = "group",
	args = {
		test = {
			type = "execute",
			name =  "Test",
			desc =  "Test",
			func = Display.Test,
			width = "1.5"
		},
		reset = {
			type = "execute",
			name =  "Reset",
			desc =  "Reset",
			func = Display.Reset,
			width = "1.5"
		},
		display = {
			type = "group",
			name =  "General Options",
			desc =  "General Options",
			order = 1,
			cmdHidden = true,
			args = {
				locked = {
					type = "toggle",
					name = "Lock Bars",
					desc = "Locks the bars in place and prevents it from being dragged.",
					get = get,
					set = set,
					order = 1,
				},
				clamped = {
					type = 'toggle',
					name =  "Clamp to screen",
					desc =  "Prevent the anchor from moving off screen.",
					get = get,
					set = set,
					order = 2,
				},
				font = {
					type = "description",
					name =  "-------------------------------- Font Options --------------------------------",
					width = "full",
					order = 6,
				},
				texture = {
					type = "select",
					name =  "Texture",
					desc =  "The texture that the bar will use",
					values = textures,
					get = function(info) return GetLSMIndex("statusbar", Display.db.profile.texture) end,
					set = function(info, v)
						Display.db.profile.texture = Media:List("statusbar")[v]
						Display:UpdateDisplay()
					end,
					order = 7,
				},
				fontSize = {
					type = "range",
					name =  "Label Font Size",
					desc =  "The size of the font that the bar labels will use",
					min = 5, max = 30, step = 1, bigStep = 1,
					get = get,
					set = set,
					order = 8,
				},
				fontFace = {
					type = "select",
					name =  "Label Font",
					desc =  "The font that the bar labels will use",
					values = fonts,
					get = function(info) return GetLSMIndex("font", Display.db.profile.fontFace) end,
					set = function(info, v)
						Display.db.profile.fontFace = Media:List("font")[v]
						Display:UpdateDisplay()
					end,
					order = 9,
				},
				orientation = {
					type = "select",
					name =  "Orientation",
					values = {  "Right to Left",  "Left to Right" },
					-- "2" in LibBars is top to bottom, but we only have 2 choices, so this is a bit of a cheat
					get = function(info) return (Display.db.profile.orientation == 3) and 2 or 1 end,
					set = function(info, v)
						Display.db.profile.orientation = (v == 2) and 3 or 1
						Display:UpdateDisplay()
					end,
					order = 10,
					width = nil,
				},
				fontDel = {
					type = "description",
					name =  "---------------------------------------------------------------------------------",
					width = "full",
					order = 11,
				},
				unclickable = {
					type = 'toggle',
					name =  "Make bars unclickable",
					desc =  "Disables right clicks on cooldown bars.",
					width = "full",
					get = function(info) return Display.db.profile.unclickable end,
					set = function(info, v)
						Display.db.profile.unclickable = v
						Display:ClickableChange();
					end,
					order = 12,
				},
				unclickDesc = {
					type = "description",
					name =  "By default cooldown bars can be right clicked to report cooldown to raid and synchronize the cooldown with other addon users.",
					width = "full",
					order = 13,
				},
			},
		},
		frames = {
			type = "group",
			name = "Frames",
			desc = "Configure all cooldown frames",
			cmdHidden = true,
			order = 2,
			args = {
				frameCount = {
					type = "range",
					name = "Frame Count",
					min = 1, max = 10, step = 1, bigStep = 1,
					get = get,
					set = function(t, value)
						Display.db.profile[t[#t]] = value
						FatCooldowns.RefreshFrameListOptionsAndVariables()
					end,
					order = 10,
				},
			},
		},
		spellOrder = {
			type = "group",
			name = "Spell Order",
			desc = "Configure the order in which spells are displayed",
			cmdHidden = true,
			order = 3,
			args = {
				descr = {
					type = "description",
					name =  "Select which spells should have modified priority over the default (class > spellid > caster > cooldown) sorting attributes.",
					width = "full",
					order = 1,
				},
				spells = {
					type = "group",
					name = "Spell priorities",
					desc = "Set priorities of selected spells",
					cmdHidden = true,
					order = 2,
					args = {
						descr = {
							type = "description",
							name =  "All unselected spells have priority 500 - select higher or lower to move your spell higher or lower in priority list.",
							width = "full",
							order = 1,
						},
					},
				},
			},
		},
		config = {
			type = "execute",
			name =  "Configure",
			desc =  "Open the configuration dialog",
			func = Display.ShowConfig,
			guiHidden = true
		}
	}
}

FatCooldowns.RefreshFrameListOptionsAndVariables = RefreshFrameListOptionsAndVariables;


Display.configOptions = options
LibStub("AceConfig-3.0"):RegisterOptionsTable("FatCooldowns", options, "fcd")