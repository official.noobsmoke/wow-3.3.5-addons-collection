
--[[		
			Data explanation
	[GetSpellInfo(12345)] = 		(- mandatory -) -- SPELL ID 
		id = 12345, 				(- mandatory -) -- again SPELL ID 
		cd = 123,   				(- mandatory -) -- Spell's cooldown, in seconds 
	
				- All spells are sorted (from highest to lowest) by order value (500 for all spells by default), if same order value than by class order (table on bottom), if same class than by spellID, if same ID than by current remaining cooldown
				
		spec = true, 					(- optional -) -- means that given spell is only used by a single class spec - therefore isnt automatically shown on pernament bars, only after first use
															
	}
	Example:
	[GetSpellInfo(1337)] = { id = 1337, cd = 1337, ora = 5, order = 999 },           -- spell name
]]

assert(FatCooldowns, "FatCooldowns not found!")

local cooldowns = {
	["DEATHKNIGHT"] = {
		[GetSpellInfo(51052)] = { id = 51052, spec = true, cd = 120 },           -- Anti-magic Zone
		[GetSpellInfo(49222)] = { id = 49222, spec = true, cd = 60 },            -- Bone Shield
		[GetSpellInfo(49576)] = { id = 49576, cd = 35 },            			 -- Death Grip
		[GetSpellInfo(48792)] = { id = 48792, cd = 180, spec = true },           -- Icebound Fortitude
		[GetSpellInfo(49039)] = { id = 49039, spec = true, cd = 120 },           -- Lichborne
		[GetSpellInfo(47528)] = { id = 47528, cd = 10 },            			 -- Mind Freeze
		[GetSpellInfo(56222)] = { id = 56222, cd = 8 },             			 -- Dark Command
		[GetSpellInfo(49016)] = { id = 49016, spec = true, cd = 180 },           -- Hysteria
		[GetSpellInfo(48707)] = { id = 48707, cd = 45 },            			 -- Anti-Magic Shell
		[GetSpellInfo(51271)] = { id = 51271, cd = 60 },           				 -- UA
		[GetSpellInfo(49206)] = { id = 49206, cd = 180 },           			 -- garg
		[GetSpellInfo(47568)] = { id = 47568, cd = 300 },            			 -- ERW
		[71635] = { id = 71635, cd = 60, spec = true},                           -- sindy nm
		[71638] = { id = 71638, cd = 60, spec = true},   						 -- sindy hc
		
	},
	["DRUID"] = {
		[GetSpellInfo(22812)] = { id = 22812, cd = 60, spec = true },            -- Barkskin
		[GetSpellInfo(8983)] = { id = 8983, cd = 30, spec = true },              -- Bash
		[GetSpellInfo(6795)] = { id = 6795, cd = 8, spec = true },               -- Growl
		[GetSpellInfo(5209)]  = { id = 5209,  cd = 180, spec = true  },          -- Challenging Roar
		[GetSpellInfo(29166)] = { id = 29166, cd = 180 },           			 -- Innervate
		[GetSpellInfo(17116)] = { id = 17116, spec = true, cd = 180 },           -- Nature's Swiftness
		[GetSpellInfo(48477)] = { id = 48477, cd = 600},  			 			 -- Rebirth
		[GetSpellInfo(48447)] = { id = 48447, cd = 480 },           			 -- Tranquility
		[GetSpellInfo(61384)] = { id = 61384, spec = true, cd = 30 },            -- Typhoon
		[GetSpellInfo(18562)] = { id = 18562, spec = true, cd = 15 },            -- Swiftmend
		[GetSpellInfo(61336)] = { id = 61336, spec = true, cd = 180 },           -- Survival Instincts
		[GetSpellInfo(53201)] = { id = 53201, spec = true, cd = 60 },            -- Starfall
		[GetSpellInfo(50334)] = { id = 50334, spec = true, cd = 180 },           -- Berserk
		[GetSpellInfo(22842)] = { id = 22842, spec = true, cd = 180 },           -- Frenzied Regeneration
		[GetSpellInfo(5229)] = { id = 5229, spec = true, cd = 60 },              -- Enrage
		[75490] = { id = 75490, cd = 120, spec = true},            				 -- GTS NM
		[75495] = { id = 75495, cd = 120, spec = true},   		 				 -- GTS HC
		[71635] = { id = 71635, cd = 60, spec = true},                           -- sindy nm
		[71638] = { id = 71638, cd = 60, spec = true},   						 -- sindy hc
	},
	["HUNTER"] = {
		[GetSpellInfo(5384)]  = { id = 5384,  cd = 30 },            			 -- Feign Death
		[GetSpellInfo(34477)] = { id = 34477, cd = 30 },            			 -- Misdirection
		[GetSpellInfo(19574)] = { id = 19574, spec = true, cd = 120 },           -- Bestial Wrath
		[GetSpellInfo(19263)] = { id = 19263, cd = 90 },           				 -- Deterrence
		[GetSpellInfo(781)] = { id = 781, cd = 25 },            				 -- Disengage
		[GetSpellInfo(13809)] = { id = 13809, cd = 30 },            			 -- Frost Trap
		[GetSpellInfo(19801)] = { id = 19801, cd = 8 },             			 -- Tranquilizing Shot
		[GetSpellInfo(3045)] = { id = 3045, cd = 180},  						 -- Rapid Fire
		[GetSpellInfo(53434)] = { id = 53434, cd = 300},  						 -- Call of the wild
		[GetSpellInfo(23989)] = { id = 23989, cd = 180},  						 -- Readiness
	},
	["MAGE"] = {
		[GetSpellInfo(2139)]  = { id = 2139,  cd = 24 },           				 -- Counterspell
		[GetSpellInfo(45438)] = { id = 45438, cd = 300 },           			 -- Ice Block
		[GetSpellInfo(1953)] = { id = 1953, cd = 15 },           				 -- Blink
		[GetSpellInfo(12051)] = { id = 12051, cd = 240 },           			 -- Evocation
		[GetSpellInfo(66)] = { id = 66, cd = 180 },           					 -- Invisibility
		[GetSpellInfo(2139)] = { id = 2139, cd = 24 },           				 -- Counterspell
		[GetSpellInfo(55342)] = { id = 55342, cd = 180 },           			 -- Mirror Image
		[GetSpellInfo(11129)] = { id = 1129, cd = 120},  						 -- Combustion
	},
	["PALADIN"] = {
		[GetSpellInfo(19752)] = { id = 19752, cd = 600}, 						 -- Divine Intervention
		[GetSpellInfo(498)]   = { id = 498,   cd = 180, spec = true},            -- Divine Protection
		[GetSpellInfo(64205)] = { id = 64205, spec = true, cd = 120 },           -- Divine Sacrifice
		[GetSpellInfo(642)]   = { id = 642,   cd = 300 },           			 -- Divine Shield
		[GetSpellInfo(10278)] = { id = 10278, cd = 300 },           			 -- Hand of Protection
		[GetSpellInfo(48788)] = { id = 48788, cd = 1200 },          			 -- Lay on Hands
		[GetSpellInfo(1044)] = { id = 1044, cd = 25 },          				 -- Hand of Freedom
		[GetSpellInfo(6940)] = { id = 6940, cd = 120 },          				 -- Hand of Sacrifice
		[GetSpellInfo(1038)] = { id = 1038, cd = 120 },          				 -- Hand of Salvation
		[GetSpellInfo(31821)] = { id = 31821, spec = true, cd = 120 },           -- Aura Mastery
		[GetSpellInfo(20066)] = { id = 20066, spec = true, cd = 60 },          	 -- Repentance
		[GetSpellInfo(10308)] = { id = 10308, cd = 60 },          				 -- Hammer of Justice
		[GetSpellInfo(48817)] = { id = 48817, cd = 30 },          				 -- Holy Wrath	
		[GetSpellInfo(31884)] = { id = 31884, cd = 120 },          				 -- Avenging Wrath	
		[GetSpellInfo(54428)] = { id = 54428, cd = 60 },          				 -- Divine Plea
		[GetSpellInfo(62124)] = { id = 62124, cd = 8, spec = true},          	 -- Hand of Reckoning	
		[GetSpellInfo(31789)] = { id = 31789, cd = 8, spec = true},          	 -- Righteous Defense
		[GetSpellInfo(66233)] = { id = 66233, cd = 120, spec = true},          	 -- Ardent Defender	
		[GetSpellInfo(31842)] = { id = 31842, cd = 180, spec = true},          	 -- Divine Illumination
		[GetSpellInfo(20216)] = { id = 20216, cd = 120, spec = true},          	 -- Divine Favor
		[75490] = { id = 75490, cd = 120, spec = true},            				 -- GTS NM
		[75495] = { id = 75495, cd = 120, spec = true},   		 				 -- GTS HC
		[71635] = { id = 71635, cd = 60, spec = true},                           -- sindy nm
		[71638] = { id = 71638, cd = 60, spec = true},   						 -- sindy hc
	},
	["PRIEST"] = {
		[GetSpellInfo(64843)] = { id = 64843, cd = 480 },           			 -- Divine Hymn
		[GetSpellInfo(6346)]  = { id = 6346,  cd = 180 },           			 -- Fear Ward
		[GetSpellInfo(47788)] = { id = 47788, spec = true, cd = 72 },            -- Guardian Spirit
		[GetSpellInfo(64901)] = { id = 64901, cd = 360 },           			 -- Hymn of Hope
		[GetSpellInfo(33206)] = { id = 33206, spec = true, cd = 180 },           -- Pain Suppression
		[GetSpellInfo(47585)] = { id = 47585, spec = true, cd = 75 },            -- Dispersion
		[GetSpellInfo(10890)] = { id = 10890, cd = 30 },            			 -- Psychic Scream
		[GetSpellInfo(34433)] = { id = 34433, cd = 300 },           			 -- Shadowfiend
		[GetSpellInfo(586)] = { id = 586, cd = 24 },           					 -- Fade
		[GetSpellInfo(10060)] = { id = 10060, cd = 100 },           			 -- Powers Infusion
		--[GetSpellInfo(48066)] = { id = 48066, cd = 4 },             			 -- Power Word: Shield
		[75490] = { id = 75490, cd = 120, spec = true},            				 -- GTS NM
		[75495] = { id = 75495, cd = 120, spec = true},   		 				 -- GTS HC
	},
	["ROGUE"] = {
		[GetSpellInfo(31224)] = { id = 31224, cd = 90 },            			 -- Cloak of Shadows
		[GetSpellInfo(8643)]  = { id = 8643,  cd = 20 },         				 -- Kidney Shot
		[GetSpellInfo(57934)]  = { id = 57934,  cd = 30 },          			 -- Tricks of the Trade
		[GetSpellInfo(1766)]  = { id = 1766,  cd = 10 },            			 -- Kick
		[GetSpellInfo(51690)]  = { id = 51690, spec = true,  cd = 120 },         -- Killing Spree
		[GetSpellInfo(26889)]  = { id = 26889,  cd = 120 },         			 -- Vanish
		[GetSpellInfo(26669)]  = { id = 26669,  cd = 150 },         			 -- Evasion
		[GetSpellInfo(13877)] = { id = 13877, cd = 120 }, 						 -- Blade Flurry
		[GetSpellInfo(13750)] = { id = 13750, cd = 120 }, 						 -- Adrenaline Rush
	},
	["SHAMAN"] = {
		[GetSpellInfo(2825)]  = { id = 2825,  cd = 300 },           			 -- Bloodlust
		[GetSpellInfo(32182)] = { id = 32182, cd = 300 },           			 -- Heroism
		[GetSpellInfo(57994)] = { id = 57994, cd = 6 },             			 -- Wind Shear
		[GetSpellInfo(51514)] = { id = 51514, cd = 45 },            			 -- Hex
		[GetSpellInfo(16190)] = { id = 16190, spec = true, cd = 300 },           -- Mana Tide Totem
		[GetSpellInfo(16188)] = { id = 16188, spec = true, cd = 180 },           -- Nature's Swiftness
		[GetSpellInfo(21169)] = { id = 21169, cd = 1800},			 			 -- Reincarnation
		--[GetSpellInfo(20608)] = { id = 20608, cd = 1800,}, -- old Reincarnation 
		[GetSpellInfo(16166)] = { id = 16166, spec = true, cd = 180 },           -- Elemental Mastery
		[GetSpellInfo(51533)] = { id = 51533, spec = true, cd = 180 },           -- Feral Spirit
		[GetSpellInfo(59159)] = { id = 59159, spec = true, cd = 35 },            -- Thunderstorm
		[75490] = { id = 75490, cd = 120, spec = true},            				 -- GTS NM
		[75495] = { id = 75495, cd = 120, spec = true},   		 				 -- GTS HC
	},
	["WARLOCK"] = {
		[GetSpellInfo(29858)] = { id = 29858, cd = 300 },           			-- Soulshatter
		[GetSpellInfo(19647)] = { id = 19647, cd = 24 },            			-- Spell Lock
		[GetSpellInfo(48020)] = { id = 48020, cd = 30 },            			-- Demonic Circle: Teleport
		[GetSpellInfo(47883)] = { id = 47883, cd = 900},  						-- Soulstone Resurrection
		[GetSpellInfo(47241)] = { id = 47241, spec = true, cd = 126},  			-- Metamorphosis
	},
	["WARRIOR"] = {
		[GetSpellInfo(1161)]  = { id = 1161,  cd = 180, spec = true},           -- Challenging Shout
		[GetSpellInfo(12975)] = { id = 12975, spec = true, cd = 180 },          -- Last Stand
		[GetSpellInfo(6554)]  = { id = 6554,  cd = 10 },           				-- Pummel
		[GetSpellInfo(72)]    = { id = 72,    cd = 12 },            			-- Shield Bash
		[GetSpellInfo(871)]   = { id = 871,   cd = 300, spec = true},           -- Shield Wall
		[GetSpellInfo(3411)]  = { id = 3411,  cd = 30 },            			-- Intervene
		[GetSpellInfo(46924)]  = { id = 46924, spec = true,  cd = 75 },         -- Bladestorm
		[GetSpellInfo(64382)]  = { id = 64382,  cd = 300 },         			-- Shattering Throw
		[GetSpellInfo(12292)]  = { id = 12292, spec = true,  cd = 121 },        -- Death Wish
		[GetSpellInfo(1719)]  = { id = 1719,  cd = 200 },           			-- Recklessness
		[GetSpellInfo(355)]  = { id = 355,  cd = 8, spec = true  },            	-- Taunt
		[71635] = { id = 71635, cd = 60, spec = true},                          -- sindy nm
		[71638] = { id = 71638, cd = 60, spec = true},   						-- sindy hc
	},
	--["ITEMS"] = {
		--[54861] = { id = 54861, cd = 180, spec = true},   						 -- Nitro Boots
		--[75490] = { id = 75490, cd = 120, spec = true},            				 -- GTS NM
		--[75495] = { id = 75495, cd = 120, spec = true},   		 				 -- GTS HC
		--[71635] = { id = 71635, cd = 60, spec = true},                           -- sindy nm
		--[71638] = { id = 71638, cd = 60, spec = true},   						 -- sindy hc
	--},
}

FatCooldowns.classOrder = { -- Must contain all 10 classes and all numbers 1-10 that define class ordering highest to lowest (swapping rows does nothing)
	["DRUID"] = 1,
	["PRIEST"] = 2,
	["SHAMAN"] = 3,
	["DEATHKNIGHT"] = 4,
	["HUNTER"] = 5,
	["MAGE"] = 6,
	["ROGUE"] = 7,
	["WARLOCK"] = 8,
	["WARRIOR"] = 9,
	["PALADIN"] = 10,
--	["ITEMS"] = 11,
}

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------  DO NOT TOUCH THE REST  ---------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FatCooldowns.spellInfoTable = {}
for class, classSpells in pairs(cooldowns) do
	for spellName, spellTable in pairs(classSpells) do
		FatCooldowns.spellInfoTable[spellTable.id] = {id = spellTable.id, cd = spellTable.cd, class = class, name = spellName, order = spellTable.order or 500}
	end
end

FatCooldowns.cooldowns = cooldowns

FatCooldowns.syncIndex = {
	[48477] = 1,
	[21169] = 2,
	[47883] = 3,
	[2825] = 4,
	[32182] = 5,
	[16190] = 6,
	[64901] = 7,
	[64843] = 8,
	[10278] = 9,
	[642] = 10,
	[45438] = 11,
	[498] = 12,
	[871] = 13,
	[47585] = 14,
	[48792] = 15,
	[33206] = 16,
	[48788] = 17,
	[19752] = 18,
	[47788] = 19,
	[29166] = 20,
	[12051] = 21,
	[66] = 22,
	[64205] = 23,
	[6940] = 24,
	[1038] = 25,
	[31821] = 26,
	[34433] = 27,
}

FatCooldowns.syncCount = 27

-- cooldowns[nick] = timeOfCDend

FatCooldowns.syncCooldowns = {
	[1] = {cooldowns = {}, count = 0, id = 48477}, -- Rebirth
	[2] = {cooldowns = {}, count = 0, id = 21169}, -- Reincarnation
	[3] = {cooldowns = {}, count = 0, id = 47883}, -- Soulstone Resurrection
	[4] = {cooldowns = {}, count = 0, id = 2825}, -- Bloodlust
	[5] = {cooldowns = {}, count = 0, id = 32182}, -- Heroism
	[6] = {cooldowns = {}, count = 0, id = 16190}, -- Mana Tide Totem
	[7] = {cooldowns = {}, count = 0, id = 64901}, -- Hymn of Hope
	[8] = {cooldowns = {}, count = 0, id = 64843}, -- Divine Hymn
	[9] = {cooldowns = {}, count = 0, id = 10278}, -- Hand of Protection
	[10] = {cooldowns = {}, count = 0, id = 642}, -- Divine Shield
	[11] = {cooldowns = {}, count = 0, id = 45438}, -- Ice Block
	[12] = {cooldowns = {}, count = 0, id = 498}, -- Divine Protection
	[13] = {cooldowns = {}, count = 0, id = 871}, -- Shield Wall
	[14] = {cooldowns = {}, count = 0, id = 47585}, -- Dispersion
	[15] = {cooldowns = {}, count = 0, id = 48792},  -- Icebound Fortitude
	[16] = {cooldowns = {}, count = 0, id = 33206}, -- Pain Suppression
	[17] = {cooldowns = {}, count = 0, id = 48788}, -- Lay on Hands
	[18] = {cooldowns = {}, count = 0, id = 19752}, -- Divine Intervention
	[19] = {cooldowns = {}, count = 0, id = 47788}, -- Guardian Spirit      
	[20] = {cooldowns = {}, count = 0, id = 29166}, -- Innervate
	[21] = {cooldowns = {}, count = 0, id = 12051}, -- Evocation
	[22] = {cooldowns = {}, count = 0, id = 66}, -- Invisibility
	[23] = {cooldowns = {}, count = 0, id = 64205}, -- Divine Sacrifice
	[24] = {cooldowns = {}, count = 0, id = 6940}, -- Hand of Sacrifice
	[25] = {cooldowns = {}, count = 0, id = 1038}, -- Hand of Salvation
	[26] = {cooldowns = {}, count = 0, id = 31821}, -- Aura Mastery
	[27] = {cooldowns = {}, count = 0, id = 34433}, -- Shadowfiend
}