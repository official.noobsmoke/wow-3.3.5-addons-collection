assert(FatCooldowns, "FatCooldowns not found!")

local MINOR_VERSION = tonumber(("$Revision: 48 $"):match("%d+"))
local AceConfig = LibStub("AceConfigDialog-3.0")
local Media = LibStub("LibSharedMedia-3.0")

local FatCooldowns = FatCooldowns
local mod = FatCooldowns:NewModule("Display", nil, "AceComm-3.0", "AceEvent-3.0", "AceConsole-3.0", "LibBars-1.0")

local pairs = _G.pairs
local UnitName, UnitClass = _G.UnitName, _G.UnitClass
local tonumber, tostring, type = _G.tonumber, _G.tostring, _G.type

local barGroups = nil;

-- local zoneType = select(2, IsInInstance()) --  unused variable (Idan)


local function sortFunc(a, b)
	if a.isTimer ~= b.isTimer then
		return a.isTimer
	end
	
	local aID, bID
	
	for w in string.gmatch(a.name,"_([0-9]*)$") do 
		aID = tonumber(w);
	end
	
	
	for w in string.gmatch(b.name,"_([0-9]*)$") do 
		bID = tonumber(w);
	end
	
	local aInfo = FatCooldowns.spellInfoTable[aID]
	local bInfo = FatCooldowns.spellInfoTable[bID]
		
		
	if (aInfo.order == bInfo.order) then
		if aInfo.class == bInfo.class then
			if aID == bID then
				if a.value == b.value then
					return a.name > b.name
				else
					return a.value > b.value
				end
			else
				return aID > bID;
			end
		else
			return FatCooldowns.classOrder[aInfo.class] > FatCooldowns.classOrder[bInfo.class];
		end
	else
		return aInfo.order > bInfo.order
	end
end

local playerName
local defaults = {
	profile = {
		locked      = false,
		texture     = "Smooth v2",
		fontFace    = nil,
		fontSize    = 10,
		clamped     = true,
		orientation = 1,
		frameCount = 1,
		orderSpells = {},
		orderSpellPriorities = {},
		unclickable = false,
		frames = {
			[1] = {
				name 		= "Frame1",
				scale       = 100.0,
				growUp      = false,
				alpha       = 100.0,
				width  		= 150,
				height 		= 14,
				hideSelf 	= false,
				readyMsg 	= false,
				maxTextLength = 12,
				permanent 	= false,
				anchor		= true,
				hideFrame	= false,
				RaidMsg  = false,
				spells 		= { },
			},
		},
	}
}
local optFrame
function mod:OnInitialize()
	Media:Register("font", "Adventure",				[[Interface\Addons\FatCooldowns\fonts\Adventure.ttf]])
	Media:Register("font", "ABF",					[[Interface\Addons\FatCooldowns\fonts\ABF.ttf]])
	Media:Register("font", "Vera Serif",			[[Interface\Addons\FatCooldowns\fonts\VeraSe.ttf]])
	Media:Register("font", "Diablo",				[[Interface\Addons\FatCooldowns\fonts\Avqest.ttf]])
	Media:Register("font", "Accidental Presidency",	[[Interface\Addons\FatCooldowns\fonts\Accidental Presidency.ttf]])
	Media:Register("statusbar", "Aluminium",		[[Interface\Addons\FatCooldowns\statusbar\Aluminium]])
	Media:Register("statusbar", "Armory",			[[Interface\Addons\FatCooldowns\statusbar\Armory]])
	Media:Register("statusbar", "BantoBar",			[[Interface\Addons\FatCooldowns\statusbar\BantoBar]])
	Media:Register("statusbar", "Glaze2",			[[Interface\Addons\FatCooldowns\statusbar\Glaze2]])
	Media:Register("statusbar", "Gloss",			[[Interface\Addons\FatCooldowns\statusbar\Gloss]])
	Media:Register("statusbar", "Graphite",			[[Interface\Addons\FatCooldowns\statusbar\Graphite]])
	Media:Register("statusbar", "Grid",				[[Interface\Addons\FatCooldowns\statusbar\Grid]])
	Media:Register("statusbar", "Healbot",			[[Interface\Addons\FatCooldowns\statusbar\Healbot]])
	Media:Register("statusbar", "LiteStep",			[[Interface\Addons\FatCooldowns\statusbar\LiteStep]])
	Media:Register("statusbar", "Minimalist",		[[Interface\Addons\FatCooldowns\statusbar\Minimalist]])
	Media:Register("statusbar", "Otravi",			[[Interface\Addons\FatCooldowns\statusbar\Otravi]])
	Media:Register("statusbar", "Outline",			[[Interface\Addons\FatCooldowns\statusbar\Outline]])
	Media:Register("statusbar", "Perl",				[[Interface\Addons\FatCooldowns\statusbar\Perl]])
	Media:Register("statusbar", "Smooth",			[[Interface\Addons\FatCooldowns\statusbar\Smooth]])
	Media:Register("statusbar", "Round",			[[Interface\Addons\FatCooldowns\statusbar\Round]])
	Media:Register("statusbar", "TukTex",			[[Interface\Addons\FatCooldowns\statusbar\normTex]])
end

function mod:OnProfileChange(event, database, newProfileKey)
	self.db = database;
	mod:Reset();
	FatCooldowns:RefreshFrameListOptionsAndVariables();
	self:SetAnchors(true)
end

function mod:OnEnable()
	--[===[@debug@
	self:Print("OnEnable()")
	--@end-debug@]===]
	
	self.db = LibStub("AceDB-3.0"):New("FCD_DB", nil, "Default")
	self.db:RegisterDefaults(defaults)	
	
	optFrame = AceConfig:AddToBlizOptions("FatCooldowns", "FatCooldowns")
	
	-- Profiles
	LibStub("AceConfig-3.0"):RegisterOptionsTable("FCD-Profiles", LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db))
	self.profilesFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("FCD-Profiles", "Profiles", "FatCooldowns")

	
	self.db.RegisterCallback(mod, "OnProfileChanged", "OnProfileChange")
	self.db.RegisterCallback(mod, "OnProfileCopied", "OnProfileChange")
	self.db.RegisterCallback(mod, "OnProfileReset", "OnProfileChange")
	-- TODO WHAT ON PROFILE CHANGE?
	-- load order profile data and other profile shits on corect events and sort order prio do table na sort
	
	
	
	--self:UnregisterAllEvents()
	--self:RegisterEvent("PLAYER_LOGIN")

	playerName = UnitName("player")
	
	self:CreateFrame()
	FatCooldowns:RefreshFrameListOptionsAndVariables();
	self:SetAnchors(true)
	FatCooldowns:RaidScan();
end

function mod:OnDisable()
	--[===[@debug@
	self:Print("OnDisable()")
	--@end-debug@]===]
	
	--self:UnregisterAllEvents()
	--barGroup = nil
end

function mod:SyncCooldown(caster, spellId, cooldown, target, sender, confirmed) -- info comes from caster
	if sender == UnitName("player") and not confirmed then return end
	--print(caster,GetSpellLink(spellId),cooldown,target,sender) -- debug
	if not cooldown or cooldown == 0 then 
		if sender == caster then
			if FatCooldowns.syncIndex[spellId] then
				local info = FatCooldowns.syncCooldowns[FatCooldowns.syncIndex[spellId]]
				info.cooldowns[caster] = nil;
				info.count = info.count - 1;
			end
			self:StopCooldown(caster, spellId)
			return
		else
			if not barGroups then return end
			for i = 1, 10, 1 do
				if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
					if self.db.profile.frames[i].spells[spellId] then 
						local bar = barGroups[i]:GetBar(caster .. "_" .. spellId)
						if bar and not bar.ready then
							local message = bar.spellId .. bar.caster .. math.ceil(bar.value);
							if bar.target then message = message .. bar.target end
							FatCooldowns:SendCommMessage(FatCooldowns.prefixS, message, "RAID")
						end
						return
					end
				end
			end
		end
	end
	
	
	if FatCooldowns.syncIndex[spellId] then 
		local info = FatCooldowns.syncCooldowns[FatCooldowns.syncIndex[spellId]]
		
		if info.cooldowns[caster] then
			if info.cooldowns[caster].timer < GetTime() or not sender or sender == caster then
				if cooldown >= 0 then
					if info.cooldowns[caster].timer > GetTime() and not target then
						info.cooldowns[caster] = {timer = GetTime() + cooldown, target = info.cooldowns[caster].target};
						self:StartCooldown(caster, spellId, cooldown, info.cooldowns[caster].target)
					else
						info.cooldowns[caster] = {timer = GetTime() + cooldown, target = target};
						self:StartCooldown(caster, spellId, cooldown, target)
					end
				else
					local newTimer = info.cooldowns[caster].timer - cooldown
					info.cooldowns[caster] = {timer = newTimer, target = info.cooldowns[caster].target};
					self:StartCooldown(caster, spellId, newTimer - GetTime(), info.cooldowns[caster].target)
				end
			end
		else
			if cooldown < 0 then cooldown = 174 end -- Guardian Spirit procd and didnt see cd - set 174
			info.cooldowns[caster] = {timer = GetTime() + cooldown, target = target};
			self:StartCooldown(caster, spellId, cooldown,target)
			info.count = info.count + 1;
		end
	else
		self:StartCooldown(caster, spellId, cooldown, target, false, sender)
	end 
end

local function buttonClick(self, button)
	if button == "LeftButton" then
		if self.ready then
			local message = self.spellId .. self.caster .. 0;
			FatCooldowns:SendCommMessage(FatCooldowns.prefixS, message, (select(2, IsInInstance()) == "pvp" and "BATTLEGROUND") or (GetNumRaidMembers() > 0 and "RAID") or "PARTY")
			
			message = self.caster .. " has " .. GetSpellLink(self.spellId) .. " ready."
			SendChatMessage(message, (select(2, IsInInstance()) == "pvp" and "BATTLEGROUND") or (GetNumRaidMembers() > 0 and "RAID") or "PARTY")
		else
			local message = self.spellId .. self.caster .. math.ceil(self.value);
			if self.target then message = message .. self.target end
			FatCooldowns:SendCommMessage(FatCooldowns.prefixS, message, (select(2, IsInInstance()) == "pvp" and "BATTLEGROUND") or (GetNumRaidMembers() > 0 and "RAID") or "PARTY")
			
			-- changed message to "will be ready in" instead of "was used" for clarity (Idan)
			--message = self.caster .. "'s " .. GetSpellInfo(self.spellId) .. " was used"
			message = self.caster .. "'s " .. GetSpellLink(self.spellId) .. " will be ready in " -- changed to GetSpellLink instead GetSpellInfo (Idan)
			if self.target then message = self.caster .. "'s " .. GetSpellLink(self.spellId) .. " was used on " .. self.target end
			--if self.target then message = message .. " on " .. self.target end
			
			message = message .. " (CD " .. format("%.1f", self.value) .. ")"
			SendChatMessage(message, (select(2, IsInInstance()) == "pvp" and "BATTLEGROUND") or (GetNumRaidMembers() > 0 and "RAID") or "PARTY")
		end
	end
end
						
function mod:ClickableChange()
	if not barGroups then return end
	self = mod
	for i = 1, 10, 1 do
		local bars = barGroups[i]:GetBars()
		if type(bars) == "table" then
			for k, bar in pairs(bars) do
				bar:EnableMouse(not self.db.profile.unclickable)
			end
		end
	end
end

function mod:StartCooldown(sender, spellId, cooldown,target,test,infoSender)
	if not barGroups then return end
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			if self.db.profile.frames[i].hideSelf and sender == playerName then 
				-- do nothing 
			else
				if self.db.profile.frames[i].spells[spellId] then 
				-- Only show a bar for this spell if the user enabled it
					-- Because we now scan all spells whenever a Spell Cooldown event is fired,
					-- a sync might be sent multiple times for the same spell. This block prevents
					-- the bar from constantly updating its max time, making it look like the
					-- cooldown is being used again. 
					local bar = barGroups[i]:GetBar(sender .. "_" .. spellId)
					if bar == nil then
						if target and target ~= sender then 
							local maxl = self.db.profile.frames[i].maxTextLength;
							bar = barGroups[i]:NewTimerBar((sender .. "_" .. spellId), string.sub(sender,1,maxl) .. " (" .. string.sub(target,1,maxl) ..")", cooldown, cooldown, spellId)
						else
							bar = barGroups[i]:NewTimerBar((sender .. "_" .. spellId), sender, cooldown, cooldown, spellId)
						end
						
						if not test then
							bar:SetScript("OnMouseUp", buttonClick)
							bar:EnableMouse(not self.db.profile.unclickable)
						end
						
						bar.target = target
						bar.permanent = self.db.profile.frames[i].permanent and not test; 
						bar.caster  = sender
						bar.spellId = spellId
					else
						if bar.ready or not infoSender or infoSender == sender then 
							if cooldown < bar.maxValue then
								bar:SetTimer(cooldown, bar.maxValue)
							else
								bar:SetTimer(cooldown, cooldown)
							end
							
						end
					end

					local _, c = UnitClass(sender)
					if not c then
						-- Get the class name from our cooldown table; this is used exclusively for testing
						for k, v in pairs(FatCooldowns.cooldowns) do
							for k2, v2 in pairs(v) do
								if v2.id == spellId then
									c = k
								end
							end
						end
					end
					
					local color = RAID_CLASS_COLORS[c]
					if type(color) == "table" then
						bar:SetColorAt(1.00, color.r, color.g, color.b, 1)
						bar:SetColorAt(0.00, color.r, color.g, color.b, 1)
					end
				end 
			end
		else
			barGroups[i]:Hide();
		end
	end
end


function mod:StopCooldown(sender, spellId)
	if not barGroups then return end
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			local bar = barGroups[i]:GetBar(sender .. "_" .. spellId)
			if bar then
				if self.db.profile.frames[i].permanent then
					bar:SetTimer(0.5, bar.maxValue)
				else
					barGroups[i]:RemoveBar(bar)
					self:UpdateDisplay() -- Removes a gap that appears in edge cases
				end
			end
		else
			barGroups[i]:Hide();
		end
	end
end

function mod:RaidUpdate(newPlayers,leftPlayers,index)	
	if not barGroups then return false end
	self = mod
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame and self.db.profile.frames[i].permanent and (not index or index == i) then
			-- remove leaver CDs if ready
			local bars = barGroups[i]:GetBars()
			if type(bars) == "table" then
				for key, bar in pairs(bars) do
					if leftPlayers[bar.caster] then
						if bar.ready then
							barGroups[i]:RemoveBar(key)
						else
							bar.permanent = nil;
						end
					end
				end
			end
			
			-- add bars for new raid members
			for player, v in pairs(newPlayers) do
				local _, class = UnitClass(player)
				if FatCooldowns.cooldowns[class] and not (UnitName("player") == player and self.db.profile.frames[i].hideSelf) then
					for spellName, spellTable in pairs(FatCooldowns.cooldowns[class]) do
						if self.db.profile.frames[i].spells[spellTable.id] and not spellTable.spec then
							local bar = barGroups[i]:GetBar(player .. "_" .. spellTable.id)
							if bar == nil then
								local bar = barGroups[i]:NewTimerBar((player .. "_" .. spellTable.id), player, 0.5, cooldown, spellTable.id)
												
								bar:SetScript("OnMouseUp", buttonClick)
								bar:EnableMouse(not self.db.profile.unclickable)
								bar.permanent = true; 
								bar.caster  = player
								bar.spellId = spellTable.id
							end
						end
					end
				end
			end
		end
	end
	return true;
end

function mod:PLAYER_LOGIN()
	--[===[@debug@
	--self:Print("PLAYER_LOGIN()")
	--@end-debug@]===]
	--self:UnregisterEvent("PLAYER_LOGIN")
end

--[[		Configuration Methods		]]--

function mod:ShowConfig()
	AceConfig:SetDefaultSize("FatCooldowns", 600, 750)
	AceConfig:Open("FatCooldowns", configFrame)
end

function mod:Test()
	if not barGroups then return end
	self = mod
	local cd = 1
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			for _, classSpells in pairs(FatCooldowns.cooldowns) do
				for _, spell in pairs(classSpells) do
					if self.db.profile.frames[i].spells[spell.id] then
						self:StartCooldown("TestCooldown", spell.id, 10 + (cd * 7) % 20,"onTestTarget",true)
						cd = cd + 1
					end
				end
			end
		else
			barGroups[i]:Hide();
		end
	end
	
end

function mod:Reset()
	if not barGroups then return end
	self = mod
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			local bars = barGroups[i]:GetBars()
			if type(bars) == "table" then
				for k, v in pairs(bars) do
					barGroups[i]:RemoveBar(k)
				end
			end
		else
			barGroups[i]:Hide();
		end
	end
	self:RaidUpdate(FatCooldowns.raidRoster,{})
end

function mod:SpellToggle(barIndex,spell, val)
	if not barGroups then return end
	if val == true then return end -- Don't need to do anything when the spell's enabled
	
	
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			-- Remove any running bars that represent the spell we just disabled
			local bars = barGroups[i]:GetBars()
			local removed = false
			if type(bars) == "table" then
				for k, v in pairs(bars) do
					if spell == v.spellId then
						barGroups[i]:RemoveBar(k)
						removed = true
					end
				end
			end
		else
			barGroups[i]:Hide();
		end
	end
	
	-- A bar was removed, force our anchor to update so that the gap gets removed
	if removed then
		self:UpdateDisplay()
	end
end

--[[		Bar Group Anchor		]]--

function mod:CreateFrame()
	barGroups = {}
	for i = 1, 10, 1 do
		if not barGroups[i] then
			local name = "FatCooldowns"..i
			if i <= self.db.profile.frameCount then name = self.db.profile.frames[i].name end
			barGroups[i] = self:NewBarGroup(name, nil, 100, 100, "RCDD_Anchor");
			barGroups[i]:SetFlashPeriod(0)
			barGroups[i]:SetSortFunction(sortFunc)
			
			-- Callbacks
			barGroups[i].RegisterCallback(self, "AnchorClicked")
			barGroups[i].RegisterCallback(self, "AnchorMoved")
			barGroups[i].RegisterCallback(self, "TimerFinished")
			barGroups[i]:Hide();
		end
	end
end

function mod:UpdateDisplay()
	if not barGroups then return end
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			if self.db.profile.locked then
				barGroups[i]:Lock()
			else
				barGroups[i]:Unlock()
			end
			
			barGroups[i].name = self.db.profile.frames[i].name
			barGroups[i]:SetOrientation(self.db.profile.orientation)
			barGroups[i]:SetClampedToScreen(self.db.profile.clamped)
			barGroups[i]:SetFont(Media:Fetch("font", self.db.profile.fontFace), self.db.profile.fontSize)
			barGroups[i]:SetTexture(Media:Fetch("statusbar", self.db.profile.texture))
			
			barGroups[i]:SetScale(self.db.profile.frames[i].scale / 100.0)
			barGroups[i]:ReverseGrowth(self.db.profile.frames[i].growUp)
			barGroups[i]:SetAlpha(self.db.profile.frames[i].alpha / 100.0)
			barGroups[i]:SetWidth(self.db.profile.frames[i].width)
			barGroups[i]:SetHeight(self.db.profile.frames[i].height)
			
			barGroups[i].name = self.db.profile.frames[i].name;
			if self.db.profile.frames[i].anchor then
				barGroups[i]:ShowAnchor()
			else
				barGroups[i]:HideAnchor()
			end
			
			barGroups[i]:Show();
		else
			barGroups[i]:Hide();
		end
	end
end

-- Omen rip
function mod:SetAnchors(useDB)
	if not barGroups then return end
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			local up = self.db.profile.frames[i].growUp
			local x, y
			if useDB then
				x, y = self.db.profile.frames[i].posX, self.db.profile.frames[i].posY
				if not x and not y then
					barGroups[i]:ClearAllPoints()
					barGroups[i]:SetPoint("CENTER", UIParent, "CENTER", 0, 0)
					return
				end
			elseif up then
				x, y = barGroups[i]:GetLeft(), barGroups[i]:GetBottom()
			else
				x, y = barGroups[i]:GetLeft(), barGroups[i]:GetTop()
			end
			barGroups[i]:ClearAllPoints()
			if up then
				barGroups[i]:SetPoint("BOTTOMLEFT", UIParent, "BOTTOMLEFT", x, y)
			else
				barGroups[i]:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT", x, y)
			end
			self.db.profile.frames[i].posX, self.db.profile.frames[i].posY = x, y
		else
			barGroups[i]:Hide();
		end
	end
end

function mod:AnchorClicked(cbk, group, button)
	if button == "RightButton" then
		self:ShowConfig()
	end
end

function mod:AnchorMoved(cbk, group, x, y)
	self:SetAnchors()
end

function mod:TimerFinished(cbk, group, bar, name)
	local report = false;
	for i = 1, 15, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			if self.db.profile.frames[i].readyMsg and self.db.profile.frames[i].spells[bar.spellId] then report = true end
		end
	end

	if not report then return end
	TimerFinishMsg = string.format("%s's %s is ready!",bar.caster, GetSpellLink(bar.spellId))
	SendChatMessage(TimerFinishMsg, (select(2, IsInInstance()) == "pvp" and "BATTLEGROUND") or (GetNumRaidMembers() > 0 and "RAID") or "PARTY")
end

--[[ 
function mod:TimerFinished(cbk, group, bar, name)
	local report = false;
	--local reportraid = false;
	for i = 1, 10, 1 do
		if i <= self.db.profile.frameCount and not self.db.profile.frames[i].hideFrame then
			if self.db.profile.frames[i].readyMsg and self.db.profile.frames[i].spells[bar.spellId] then report = true end
		end
	end
	
	if report then return
		FatCooldowns:Print(string.format("%s's %s is up.",bar.caster, GetSpellInfo(bar.spellId)))
	end
	
	--if self.db.profile.frames[i].RaidMsg then reportraid = true end
		--TimerFinishMsg = string.format("%s's %s is up.",bar.caster, GetSpellInfo(bar.spellId))
		--SendChatMessage(TimerFinishMsg, (select(2, IsInInstance()) == "pvp" and "BATTLEGROUND") or (GetNumRaidMembers() > 0 and "RAID") or "PARTY")
end
--]] 

