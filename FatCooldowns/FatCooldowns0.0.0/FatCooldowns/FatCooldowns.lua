local MINOR_VERSION = tonumber(("$Revision: 2 $"):match("%d+"))

FatCooldowns = LibStub("AceAddon-3.0"):NewAddon("FatCooldowns", "AceConsole-3.0", "AceComm-3.0", "AceEvent-3.0")
FatCooldowns.MINOR_VERSION = MINOR_VERSION

local UnitName = _G.UnitName
FatCooldowns.raidRoster = nil;

function FatCooldowns:RaidScan()
	if self.raidRoster then
		if UnitInRaid("player") then
			local newRaidRoster = {};
			local newPlayers = {};
			local members = GetNumRaidMembers();
			
			for i = 1,members,1 do
				local name = UnitName("raid" .. i);
				newRaidRoster[name] = true;
				if self.raidRoster[name] then
					self.raidRoster[name] = nil;
				else
					newPlayers[name] = true;
				end
			end
			
			self:GetModule("Display"):RaidUpdate(newPlayers,self.raidRoster);
			self.raidRoster = newRaidRoster;
		else
			self:GetModule("Display"):Reset();
			self.raidRoster = nil;
		end	
	else
		if UnitInRaid("player") then
			local newRaidRoster = {};
			local members = GetNumRaidMembers();
			
			for i = 1,members,1 do
				newRaidRoster[UnitName("raid" .. i)] = true;
			end
		
			local verified = self:GetModule("Display"):RaidUpdate(newRaidRoster,{});
			if verified then
				self.raidRoster = newRaidRoster;
			end
		end	-- otherwise no change
	end			
end

function FatCooldowns:OnInitialize()
	self:SetEnabledState(true)
	
	--[===[@debug@
	self:Print("OnInitialize")
	--@end-debug@]===]

	self.prefix = "FRCD3"
	self.prefixB = "FRCD3B" -- broadcast
	self.prefixR = "FRCD3R" -- request
	self.prefixI = "FRCD3I" -- infodump
	self.prefixS = "FRCD3S" -- singleReport
	self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	self:RegisterEvent("RAID_ROSTER_UPDATE", "RaidScan")
	--self:RegisterEvent("PLAYER_ENTERING_WORLD", "RaidScan")
end


function FatCooldowns:OnEnable()
	self:RegisterComm(self.prefix)
	self:RegisterComm(self.prefixB)
	self:RegisterComm(self.prefixR)
	self:RegisterComm(self.prefixI)
	self:RegisterComm(self.prefixS)
	
	
	local name, module
	for name, module in self:IterateModules() do
		if not module:IsEnabled() then
			self:EnableModule(name)
		end
	end
	
	self:RaidScan();
end


--------------[[		Events		]]--------------
function FatCooldowns:RAID_ROSTER_UPDATE()
	self:RaidScan();
end


function FatCooldowns:COMBAT_LOG_EVENT_UNFILTERED(event, _, eventType, _, srcName, _, _, dstName, _, spellId, spellName, _, ...)
	if not srcName then return end
	if srcName ~= UnitName("player") and not UnitInParty(srcName) and not UnitInRaid(srcName) then return end
	
	if eventType == "SPELL_HEAL" and spellId == 48153 then
		self:GetModule("Display"):SyncCooldown(srcName, 47788, -108,dstName,nil)
		return
	end
	
	if eventType ~= "SPELL_CAST_SUCCESS" and eventType ~= "SPELL_RESURRECT" and (eventType ~= "SPELL_AURA_APPLIED" or spellId ~= 47883) then return end
	
	local _, c = UnitClass(srcName)
	if self.cooldowns[c] then
		local spells = self.cooldowns[c]
		if spells[spellName] or spells[spellId] then
		--print(eventType,srcName,dstName,spellName)
			if spells[spellName] or spells[spellId] then
				local cd = spells[spellName] and spells[spellName].cd or spells[spellId].cd
				if dstName and (UnitInParty(dstName) or UnitInRaid(dstName)) then
				self:GetModule("Display"):SyncCooldown(srcName, spellId, cd,dstName,nil)
			else
				self:GetModule("Display"):SyncCooldown(srcName, spellId, cd,nil,nil)
			end
			return
		end
	end
	
	return
end
end


--------------[[		Comm Methods		]]--------------

function FatCooldowns:OnCommReceived(prefix, msg, distro, sender)
	if self:GetModule("Display").db.profile.hideSelf and sender == playerName then return end
	--print(prefix,msg,distro,sender)
	
	if prefix == self.prefix then
		local classCDs = {}
		local _, class = UnitClass(sender)
		if self.cooldowns[class] then
			for spellName, spellTable in pairs(self.cooldowns[class]) do
				classCDs[spellTable.id] = true;
			end
		end
		
		local spellId, cooldown
		for w in string.gmatch(msg,"([^,]*),") do
			spellId = nil; cooldown = nil;
			spellId, cooldown = select(3, w:find("(%d+)-(%d+)"))
			if not spellId or not cooldown or not tonumber(spellId) or not tonumber(cooldown) then print("Recieving false cooldown data from",sender,"b7rc2"); return end
			spellId = tonumber(spellId)
			cooldown = tonumber(cooldown)
			classCDs[spellId] = nil;
			--print("has CD",spellId)
			self:GetModule("Display"):SyncCooldown(sender, spellId, cooldown, nil, sender,true);
		end
		
		for spellID, v in pairs(classCDs) do
			--print("no CD",spellID)
			self:GetModule("Display"):SyncCooldown(sender, spellID, 0, nil, sender,true);
		end
	elseif prefix == self.prefixB then
		--if sender == UnitName("player") then return end
		--self:ReCountCooldowns();
		local i = 1
		for w in string.gmatch(msg," ([0-9]+)") do 
			if i > FatCooldowns.syncCount then --print("Recieving false cooldown data from",sender,"3k5s"); 
				return 
			end
			
			if tonumber(w) > FatCooldowns.syncCooldowns[i].count then
				self:SendCommMessage(FatCooldowns.prefixR, tostring(i), "WHISPER", sender)
			end
			i = i + 1;
		end
	elseif prefix == self.prefixR then
		if not tonumber(msg) or tonumber(msg) > FatCooldowns.syncCount then print("Recieving false cooldown data from",sender,"4q5d"); return end
		local message = msg .. ",";
		for caster,cdTable in pairs(FatCooldowns.syncCooldowns[tonumber(msg)].cooldowns) do
			if cdTable.timer > GetTime() + 1 then
				message = message .. caster .. math.ceil(cdTable.timer-GetTime());
				if cdTable.target then message = message .. cdTable.target end
				message = message .. ",";
			end
		end
		self:SendCommMessage(FatCooldowns.prefixI, message, "WHISPER", sender)
	elseif prefix == self.prefixI then
		local index = nil;
		for w in string.gmatch(msg,"([^,]*),") do
			if not index then 
				if not tonumber(w) or tonumber(w) > FatCooldowns.syncCount then print("Recieving false cooldown data from",sender,"8hf4"); return end
				index = tonumber(w);
			else
				local caster = nil; local cd = nil; local target = nil;
				caster, cd, target = select(3, w:find("(%a+)(%d+)(%a*)"))
				if not caster or not cd or not tonumber(cd) then print("Recieving false cooldown data from",sender,"s4t3"); return end
				cd = tonumber(cd)
				if target == "" then target = nil; end
				
				if not FatCooldowns.syncCooldowns[index].cooldowns[caster] then
					self:GetModule("Display"):SyncCooldown(caster, FatCooldowns.syncCooldowns[index].id, cd, target, sender);
				end
			end
		end
	elseif prefix == self.prefixS then
		--if sender == UnitName("player") then return end
		--self:ReCountCooldowns();
		local spellId, caster, cooldown, target = select(3, msg:find("(%d+)(%a+)(%d+)(%a*)"))
		if spellId and tonumber(spellId) and caster and cooldown and tonumber(cooldown) then
			if target == "" then target = nil; end
			self:GetModule("Display"):SyncCooldown(caster, tonumber(spellId), tonumber(cooldown), target, sender);
		end
	end
end