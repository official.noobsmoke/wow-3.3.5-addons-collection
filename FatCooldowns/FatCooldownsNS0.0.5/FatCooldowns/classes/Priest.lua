assert(FatCooldowns, "FatCooldowns not found!")
if (select(2, UnitClass("player"))) ~= "PRIEST" then return end

local guardian = GetSpellInfo(47788)
local mod = FatCooldowns:NewModule("Priest", FatCooldowns.ModuleBase, "AceConsole-3.0", "AceEvent-3.0", "AceTimer-3.0")
mod.cooldowns = FatCooldowns.cooldowns["PRIEST"]

function mod:OnEnable()
	--[===[@debug@
	self:Print("OnEnable override")
	--@end-debug@]===]
	
	self:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED", "PriestSpellSuccess")
end

function mod:PriestSpellSuccess(event, unit, spell)
	if unit ~= "player" then return end
	
	if spell == guardian then
		-- Guardian Spirit cast; schedule an event in, like, 12 seconds to scan the
		-- cooldown to see if the Glyph reset it.
		self:ScheduleTimer("GuardianScan", 12)
	end
end

function mod:GuardianScan()
	self.canScanCooldowns = true
	--self:ScanSpells()
end