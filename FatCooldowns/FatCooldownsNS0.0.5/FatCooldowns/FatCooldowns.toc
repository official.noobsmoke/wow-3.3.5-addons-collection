## Interface: 30300
## Title: FatCooldowns
## Notes: Tracks, transmits and displays the cooldowns of your raid.
## Author: tsigo (RaidCooldowns) + Fatline (FatCooldowns) + Idan (Items tracking, more spells, raid reporting and some other adjustments), Noobsmoke (continued)
## Version: 0.0.5
## Thanks: Vendethiel (GladiusEx)
## SavedVariables: FCD_DB
## Dependencies:
## OptionalDeps: Ace3, LibSharedMedia-3.0, LibBars-1.0
## X-Embeds: Ace3, LibSharedMedia-3.0, LibBars-1.0

#@no-lib-strip@
#@end-no-lib-strip@

embeds.xml

# Base
FatCooldowns.lua
Cooldowns.lua
ModuleBase.lua
classes\Druid.lua
classes\Mage.lua
classes\Hunter.lua
classes\Paladin.lua
classes\Priest.lua
classes\Rogue.lua
classes\Shaman.lua
classes\Warlock.lua
classes\Warrior.lua

# Display
Display.lua
Config.lua
