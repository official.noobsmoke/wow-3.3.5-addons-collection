
--[[		
			Data explanation
	[GetSpellInfo(12345)] = 		(- mandatory -) -- SPELL ID 
		id = 12345, 				(- mandatory -) -- again SPELL ID 
		cd = 123,   				(- mandatory -) -- Spell's cooldown, in seconds 
	
				- All spells are sorted (from highest to lowest) by order value (500 for all spells by default), if same order value than by class order (table on bottom), if same class than by spellID, if same ID than by current remaining cooldown
				
		spec = true, 					(- optional -) -- means that given spell is only used by a single class spec - therefore isnt automatically shown on pernament bars, only after first use
															
	}
	Example:
	[GetSpellInfo(1337)] = { id = 1337, cd = 1337, ora = 5, order = 999 },           -- spell name
]]

assert(FatCooldowns, "FatCooldowns not found!")
local hero = (UnitFactionGroup("player") == "Alliance") and 32182 or 2825

local cooldowns = {
	["DEATHKNIGHT"] = {
		[GetSpellInfo(51052)] = { id = 51052, spec = true, cd = 120 },           -- Anti-magic Zone
		[GetSpellInfo(49222)] = { id = 49222, spec = true, cd = 60 },            -- Bone Shield
		[GetSpellInfo(49576)] = { id = 49576, cd = 35 },            			 -- Death Grip
		[GetSpellInfo(48792)] = { id = 48792, cd = 180, spec = true },           -- Icebound Fortitude
		[GetSpellInfo(49039)] = { id = 49039, spec = true, cd = 120 },           -- Lichborne
		[GetSpellInfo(47528)] = { id = 47528, cd = 10 },            			 -- Mind Freeze
		[GetSpellInfo(56222)] = { id = 56222, cd = 8 },             			 -- Dark Command
		[GetSpellInfo(49016)] = { id = 49016, spec = true, cd = 180, hasTarget = true },           -- Hysteria
		[GetSpellInfo(48707)] = { id = 48707, cd = 45 },            			 -- Anti-Magic Shell
		[GetSpellInfo(51271)] = { id = 51271, spec = true, cd = 60 },          	 -- Unbreakable Armor
		[GetSpellInfo(49206)] = { id = 49206, spec = true, cd = 180 },           -- Summon Gargoyle
		[GetSpellInfo(47568)] = { id = 47568, cd = 300 },            			 -- ERW
		[GetSpellInfo(55233)] = { id = 55233, spec = true, cd = 60 },            -- Vamp Blood
		[GetSpellInfo(42650)] = { id = 42650, cd = 600 },         				 -- Army of the Dead
		[GetSpellInfo(49005)] = { id = 49005, spec = true, cd = 180 },        	 -- Mark of Blood
		[GetSpellInfo(47476)] = { id = 47476, cd = 120 },          			     -- Strangulate
		[GetSpellInfo(45529)] = { id = 45529, cd = 60 },          			     -- Blood Tap
		[GetSpellInfo(48982)] = { id = 48982, spec = true, cd = 30 },          	 -- Rune Tap
		[GetSpellInfo(70654)] = { id = 70654, spec = true, cd = 60 },            -- Blood Armor
	},
	["DRUID"] = {
		[GetSpellInfo(22812)] = { id = 22812, cd = 60, spec = true },            -- Barkskin
		[GetSpellInfo(8983)] = { id = 8983, cd = 30, spec = true },              -- Bash
		[GetSpellInfo(6795)] = { id = 6795, cd = 8, spec = true },               -- Growl
		[GetSpellInfo(5209)]  = { id = 5209,  cd = 180, spec = true  },          -- Challenging Roar
		[GetSpellInfo(29166)] = { id = 29166, cd = 180, hasTarget = true },		 -- Innervate
		[GetSpellInfo(17116)] = { id = 17116, spec = true, cd = 180 },           -- Nature's Swiftness
		[GetSpellInfo(48477)] = { id = 48477, cd = 600, hasTarget = true }, 	 -- Rebirth
		[GetSpellInfo(48447)] = { id = 48447, cd = 480 },           			 -- Tranquility
		[GetSpellInfo(53227)] = { id = 53227, spec = true, cd = 20 },            -- Typhoon   			
		[GetSpellInfo(18562)] = { id = 18562, spec = true, cd = 15 },            -- Swiftmend
		[GetSpellInfo(61336)] = { id = 61336, spec = true, cd = 180 },           -- Survival Instincts
		[GetSpellInfo(53201)] = { id = 53201, spec = true, cd = 60 },            -- Starfall
		[GetSpellInfo(50334)] = { id = 50334, spec = true, cd = 180 },           -- Berserk
		[GetSpellInfo(22842)] = { id = 22842, spec = true, cd = 180 },           -- Frenzied Regeneration
		[GetSpellInfo(5229)] = { id = 5229, spec = true, cd = 60 },              -- Enrage
		[GetSpellInfo(33357)] = { id = 33357, spec = true, cd = 180 },           -- Dash
		[GetSpellInfo(16857)] = { id = 16857, spec = true, cd = 6 },             -- Faerie Fire (Feral)
	},
	["HUNTER"] = {
		[GetSpellInfo(60192)]  = { id = 60192,  cd = 30 },            			 -- Freezing Arrow				
		[GetSpellInfo(34477)] = { id = 34477, cd = 30, hasTarget = true },       -- Misdirection
		[GetSpellInfo(19574)] = { id = 19574, spec = true, cd = 120 },           -- Bestial Wrath
		[GetSpellInfo(19263)] = { id = 19263, cd = 90 },           				 -- Deterrence
		[GetSpellInfo(781)] = { id = 781, cd = 25 },            				 -- Disengage
		[GetSpellInfo(13809)] = { id = 13809, cd = 30 },            			 -- Frost Trap
		[GetSpellInfo(19801)] = { id = 19801, cd = 8 },             			 -- Tranquilizing Shot
		[GetSpellInfo(3045)] = { id = 3045, cd = 180},  						 -- Rapid Fire			
		[GetSpellInfo(23989)] = { id = 23989, spec = true, cd = 180 },  		 -- Readiness
		[GetSpellInfo(49067)] = { id = 49067, cd = 30},  						 -- Explosive Trap
		[GetSpellInfo(34600)] = { id = 34600, cd = 30},  						 -- Snake Trap
		[GetSpellInfo(60192)] = { id = 60192, cd = 30},  						 -- Freezing Arrow
		[GetSpellInfo(34490)] = { id = 34490, spec = true, cd = 30 },  			 -- Silencing Shot
		
	},
	["MAGE"] = {
		[GetSpellInfo(2139)]  = { id = 2139,  cd = 24 },           				 -- Counterspell
		[GetSpellInfo(45438)] = { id = 45438, cd = 300 },           			 -- Ice Block
		[GetSpellInfo(1953)] = { id = 1953, cd = 15 },           				 -- Blink
		[GetSpellInfo(12051)] = { id = 12051, cd = 240 },           			 -- Evocation
		[GetSpellInfo(66)] = { id = 66, cd = 180 },           					 -- Invisibility
		[GetSpellInfo(55342)] = { id = 55342, cd = 180 },           			 -- Mirror Image
		
	},
	["PALADIN"] = {
		[GetSpellInfo(19752)] = { id = 19752, spec = true, cd = 600, hasTarget = true }, 						 -- Divine Intervention
		[GetSpellInfo(498)]   = { id = 498,   cd = 180, spec = true },           -- Divine Protection
		[GetSpellInfo(64205)] = { id = 64205, spec = true, cd = 120, hasTarget = true },           -- Divine Sacrifice
		[GetSpellInfo(642)]   = { id = 642, cd = 300 },           				 -- Divine Shield
		[GetSpellInfo(10278)] = { id = 10278, cd = 300, hasTarget = true },      -- Hand of Protection
		[GetSpellInfo(48788)] = { id = 48788, cd = 1200, hasTarget = true },     -- Lay on Hands
		[GetSpellInfo(1044)] = { id = 1044, cd = 25, hasTarget = true },         -- Hand of Freedom
		[GetSpellInfo(6940)] = { id = 6940, cd = 120, hasTarget = true },        -- Hand of Sacrifice
		[GetSpellInfo(1038)] = { id = 1038, cd = 120, hasTarget = true },        -- Hand of Salvation
		[GetSpellInfo(31821)] = { id = 31821, spec = true, cd = 120 },           -- Aura Mastery
		[GetSpellInfo(20066)] = { id = 20066, spec = true, cd = 60 },          	 -- Repentance
		[GetSpellInfo(10308)] = { id = 10308, cd = 60 },          				 -- Hammer of Justice
		[GetSpellInfo(48817)] = { id = 48817, cd = 30 },          				 -- Holy Wrath	
		[GetSpellInfo(31884)] = { id = 31884, cd = 120 },          				 -- Avenging Wrath	
		[GetSpellInfo(54428)] = { id = 54428, cd = 60 },          				 -- Divine Plea
		[GetSpellInfo(62124)] = { id = 62124, cd = 8, spec = true},          	 -- Hand of Reckoning	
		[GetSpellInfo(31789)] = { id = 31789, cd = 8, spec = true},          	 -- Righteous Defense
		[GetSpellInfo(66233)] = { id = 66233, cd = 120, spec = true},          	 -- Ardent Defender  
		[GetSpellInfo(31842)] = { id = 31842, cd = 180, spec = true},          	 -- Divine Illumination
		[GetSpellInfo(20216)] = { id = 20216, cd = 120, spec = true},          	 -- Divine Favor
	},
	["PRIEST"] = {
		[GetSpellInfo(64044)] = { id = 64044, spec = true, cd = 120 },           -- Psychic Horror
		[GetSpellInfo(15487)] = { id = 15487, spec = true, cd = 45 },            -- Silence
		[GetSpellInfo(64843)] = { id = 64843, cd = 480 },           			 -- Divine Hymn
		[GetSpellInfo(6346)]  = { id = 6346,  cd = 180, hasTarget = true },      -- Fear Ward
		[GetSpellInfo(47788)] = { id = 47788, spec = true, cd = 72, hasTarget = true },            -- Guardian Spirit
		[GetSpellInfo(64901)] = { id = 64901, cd = 360 },           			 -- Hymn of Hope
		[GetSpellInfo(33206)] = { id = 33206, spec = true, cd = 180, hasTarget = true },           -- Pain Suppression
		[GetSpellInfo(47585)] = { id = 47585, spec = true, cd = 75 },            -- Dispersion
		[GetSpellInfo(10890)] = { id = 10890, cd = 30 },            			 -- Psychic Scream
		[GetSpellInfo(34433)] = { id = 34433, cd = 300 },           			 -- Shadowfiend
		[GetSpellInfo(586)] = { id = 586, cd = 24 },           					 -- Fade
		[GetSpellInfo(10060)] = { id = 10060, spec = true, cd = 100, hasTarget = true },           -- Powers Infusion
		[GetSpellInfo(48113)] = { id = 48113, spec = true, cd = 7 },           -- Prayer of Mending
	
	},
	["ROGUE"] = {
		[GetSpellInfo(31224)] = { id = 31224, cd = 90 },            			 -- Cloak of Shadows
		[GetSpellInfo(8643)]  = { id = 8643,  cd = 20 },         				 -- Kidney Shot
		[GetSpellInfo(57934)]  = { id = 57934,  cd = 30, hasTarget = true },	 -- Tricks of the Trade
		[GetSpellInfo(1766)]  = { id = 1766,  cd = 10 },            			 -- Kick
		[GetSpellInfo(51690)]  = { id = 51690, spec = true,  cd = 120 },         -- Killing Spree
		[GetSpellInfo(26889)]  = { id = 26889,  cd = 120 },         			 -- Vanish
		[GetSpellInfo(26669)]  = { id = 26669,  cd = 150 },         			 -- Evasion
		[GetSpellInfo(13877)] = { id = 13877, spec = true, cd = 120 }, 			 -- Blade Flurry
		[GetSpellInfo(13750)] = { id = 13750, spec = true, cd = 120 }, 			 -- Adrenaline Rush
		[GetSpellInfo(51722)] = { id = 51722, cd = 60 },					     -- Dismantle
		[GetSpellInfo(11305)] = { id = 11305, cd = 240 },					     -- Sprint
		[GetSpellInfo(2094)] = { id = 2094, cd = 240 },					    	 -- Blind
		[GetSpellInfo(48659)] = { id = 48659, cd = 10 },					     -- Fanit
	
	},
	["SHAMAN"] = {
		[GetSpellInfo(hero)]  = { id = hero,  cd = 300, hasTarget = true },      -- Bloodlust/Heroism
		[GetSpellInfo(57994)] = { id = 57994, cd = 6 },             			 -- Wind Shear
		[GetSpellInfo(51514)] = { id = 51514, cd = 45 },            			 -- Hex    				
		[GetSpellInfo(16190)] = { id = 16190, spec = true, cd = 300, hasTarget = true },           -- Mana Tide Totem
		[GetSpellInfo(16188)] = { id = 16188, spec = true, cd = 180 },           -- Nature's Swiftness
		[GetSpellInfo(21169)] = { id = 21169, cd = 1800},			 			 -- Reincarnation
		[GetSpellInfo(16166)] = { id = 16166, spec = true, cd = 180 },           -- Elemental Mastery
		[GetSpellInfo(51533)] = { id = 51533, spec = true, cd = 180 },           -- Feral Spirit
		[GetSpellInfo(59159)] = { id = 59159, spec = true, cd = 35 },            -- Thunderstorm
		[GetSpellInfo(2894)] = { id = 2894, spec = true, cd = 600 },			 -- Fire Elemental Totem
	
	},
	["WARLOCK"] = {
		[GetSpellInfo(29858)] = { id = 29858, cd = 300 },           			-- Soulshatter					
		[GetSpellInfo(48020)] = { id = 48020, cd = 30 },            			-- Demonic Circle: Teleport
		[GetSpellInfo(47883)] = { id = 47883, cd = 900, hasTarget = true },		-- Soulstone Resurrection
		[GetSpellInfo(47241)] = { id = 47241, spec = true, cd = 126},  			-- Metamorphosis
	
	},
	["WARRIOR"] = {
		[GetSpellInfo(23881)]  = { id = 23881,  cd = 4, spec = true},			-- Bloodthirst
		[GetSpellInfo(1680)]  = { id = 1680,  cd = 8, spec = true},   		    -- Whirlwind
		[GetSpellInfo(70845)]  = { id = 70845,  cd = 60, spec = true},   		-- Stoicism
		[GetSpellInfo(1161)]  = { id = 1161,  cd = 180, spec = true},           -- Challenging Shout
		[GetSpellInfo(12975)] = { id = 12975, spec = true, cd = 180 },          -- Last Stand
		[GetSpellInfo(6552)]  = { id = 6552,  cd = 10 },           				-- Pummel
		[GetSpellInfo(72)]    = { id = 72,    cd = 12 },            			-- Shield Bash
		[GetSpellInfo(871)]   = { id = 871,   cd = 300, spec = true},           -- Shield Wall
		[GetSpellInfo(3411)]  = { id = 3411,  cd = 30 },            			-- Intervene
		[GetSpellInfo(46924)]  = { id = 46924, spec = true,  cd = 75 },         -- Bladestorm
		[GetSpellInfo(64382)]  = { id = 64382,  cd = 300 },         			-- Shattering Throw  		
		[GetSpellInfo(12292)]  = { id = 12292, spec = true,  cd = 121 },        -- Death Wish
		[GetSpellInfo(1719)]  = { id = 1719,  cd = 200 },           			-- Recklessness
		[GetSpellInfo(355)]  = { id = 355,  cd = 8, spec = true  },            	-- Taunt
		[GetSpellInfo(5246)]   = { id = 5246,   cd = 120 },						-- Intimidating Shout
		[GetSpellInfo(676)]  = { id = 676,  cd = 60 },							-- Disarm
		[GetSpellInfo(60970)]  = { id = 60970,  cd = 45 },						-- Heroic Fury		
		[GetSpellInfo(12323)]  = { id = 12323,  cd = 5 },						-- Piercing Howl
	},
	["ITEMS"] = {
		[75490] = { id = 75490, cd = 120, spec = true},           -- GTS NM
		[75495] = { id = 75495, cd = 120, spec = true},   		-- GTS HC
		[71635] = { id = 71635, cd = 60, spec = true},            -- sindy nm
		[71638] = { id = 71638, cd = 60, spec = true},   			-- sindy hc
		[71586] = { id = 71586, cd = 120, spec = true},   		-- Corroded Skeleton Key
		[67753] = { id = 67753, cd = 180, spec = true},   		-- Satrina/Juggernaut Heroic
		[67699] = { id = 67699, cd = 180, spec = true},   		-- Satrina/Juggernaut Normal
		[23133] = { id = 23133, cd = 1200, spec = true},   		-- Gnomish Battle Chicken
		[54861] = { id = 54861, cd = 180, spec = true},   		-- Nitro Boosts
		[26297] = { id = 26297, cd = 180, spec = true},           -- Berserking
		[20594] = { id = 20594, cd = 120, spec = true},           -- Stoneform
		[33702] = { id = 33702, cd = 120, spec = true},           -- Blood Fury
		[33697] = { id = 33697, cd = 120, spec = true},           -- Blood Fury
		[20572] = { id = 20572, cd = 120, spec = true},           -- Blood Fury
	},
	["CONSUMABLES"] = {
		[53908] = { id = 53908, cd = 60, spec = true},   						 -- speed pot
		[53909] = { id = 53909, cd = 60, spec = true},   						 -- wild magic pot
		[53762] = { id = 53762, cd = 60, spec = true},   						 -- Indestructible pot
		[56350] = { id = 56350, cd = 60, spec = true},   						 -- Saronite Bomb
		[56488] = { id = 56488, cd = 300, spec = true},   						 -- Global Thermal Sapper Charge
		[35476] = { id = 35476, cd = 120, spec = true},   						 -- Drums of Battle
		[35477] = { id = 35477, cd = 120, spec = true},   						 -- Drums of Speed
		[28494] = { id = 28494, cd = 60, spec = true},   						 -- Insane Strength Potion
		[2379] = { id = 2379, cd = 60, spec = true},   						 -- Swiftness Potion
		[28714] = { id = 28714, cd = 180, spec = true},   						 -- Flame Cap
	},
}

FatCooldowns.classOrder = { -- Must contain all 10 classes and all numbers 1-10 that define class ordering highest to lowest (swapping rows does nothing)
	["DRUID"] = 1,
	["PRIEST"] = 2,
	["SHAMAN"] = 3,
	["DEATHKNIGHT"] = 4,
	["HUNTER"] = 5,
	["MAGE"] = 6,
	["ROGUE"] = 7,
	["WARLOCK"] = 8,
	["WARRIOR"] = 9,
	["PALADIN"] = 10,
	["ITEMS"] = 11,
	["CONSUMABLES"] = 12,
}

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------  DO NOT TOUCH THE REST  ---------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FatCooldowns.spellInfoTable = {}
for class, classSpells in pairs(cooldowns) do
	for spellName, spellTable in pairs(classSpells) do
		FatCooldowns.spellInfoTable[spellTable.id] = {id = spellTable.id, cd = spellTable.cd, class = class, name = spellName, order = spellTable.order or 500}
	end
end

FatCooldowns.cooldowns = cooldowns

FatCooldowns.syncIndex = {
	[48477] = 1,
	[21169] = 2,
	[47883] = 3,
	[2825] = 4,
	[32182] = 5,
	[16190] = 6,
	[64901] = 7,
	[64843] = 8,
	[10278] = 9,
	[642] = 10,
	[45438] = 11,
	[498] = 12,
	[871] = 13,
	[47585] = 14,
	[48792] = 15,
	[33206] = 16,
	[48788] = 17,
	[19752] = 18,
	[47788] = 19,
	[29166] = 20,
	[12051] = 21,
	[66] = 22,
	[64205] = 23,
	[6940] = 24,
	[1038] = 25,
	[31821] = 26,
	[34433] = 27,
	[19647] = 28,
}

FatCooldowns.syncCount = 27

-- cooldowns[nick] = timeOfCDend

FatCooldowns.syncCooldowns = {
	[1] = {cooldowns = {}, count = 0, id = 48477}, -- Rebirth
	[2] = {cooldowns = {}, count = 0, id = 21169}, -- Reincarnation
	[3] = {cooldowns = {}, count = 0, id = 47883}, -- Soulstone Resurrection
	[4] = {cooldowns = {}, count = 0, id = 2825}, -- Bloodlust
	[5] = {cooldowns = {}, count = 0, id = 32182}, -- Heroism
	[6] = {cooldowns = {}, count = 0, id = 16190}, -- Mana Tide Totem
	[7] = {cooldowns = {}, count = 0, id = 64901}, -- Hymn of Hope
	[8] = {cooldowns = {}, count = 0, id = 64843}, -- Divine Hymn
	[9] = {cooldowns = {}, count = 0, id = 10278}, -- Hand of Protection
	[10] = {cooldowns = {}, count = 0, id = 642}, -- Divine Shield
	[11] = {cooldowns = {}, count = 0, id = 45438}, -- Ice Block
	[12] = {cooldowns = {}, count = 0, id = 498}, -- Divine Protection
	[13] = {cooldowns = {}, count = 0, id = 871}, -- Shield Wall
	[14] = {cooldowns = {}, count = 0, id = 47585}, -- Dispersion
	[15] = {cooldowns = {}, count = 0, id = 48792},  -- Icebound Fortitude
	[16] = {cooldowns = {}, count = 0, id = 33206}, -- Pain Suppression
	[17] = {cooldowns = {}, count = 0, id = 48788}, -- Lay on Hands
	[18] = {cooldowns = {}, count = 0, id = 19752}, -- Divine Intervention
	[19] = {cooldowns = {}, count = 0, id = 47788}, -- Guardian Spirit      
	[20] = {cooldowns = {}, count = 0, id = 29166}, -- Innervate
	[21] = {cooldowns = {}, count = 0, id = 12051}, -- Evocation
	[22] = {cooldowns = {}, count = 0, id = 66}, -- Invisibility
	[23] = {cooldowns = {}, count = 0, id = 64205}, -- Divine Sacrifice
	[24] = {cooldowns = {}, count = 0, id = 6940}, -- Hand of Sacrifice
	[25] = {cooldowns = {}, count = 0, id = 1038}, -- Hand of Salvation
	[26] = {cooldowns = {}, count = 0, id = 31821}, -- Aura Mastery
	[27] = {cooldowns = {}, count = 0, id = 34433}, -- Shadowfiend
}