﻿local addonName = ...;
local E, L, V, P, G, _ = unpack(ElvUI);

P.addOnSkins = {
	embed = {
		embedType = "DISABLE",
		left = "Skada",
		right = "Skada",
		rightChat = true,
		leftWidth = 200,
		belowTop = false,
		hideChat = "NONE",
	}
};

V.addOnSkins = {
	Omen = true,
	Recount = true,
	SexyCooldown = true,
	DBM = true,
	Skada = true,
	Auctionator = true,
	BugSack = true,
	CallToArms = true,
	Postal = true,
	QuestPointer = true,
	Clique = true,
	FloAspectBar = true,
	FloTotemBar = true,
	Spy = true,

	Blizzard_WorldStateFrame = true,
};
