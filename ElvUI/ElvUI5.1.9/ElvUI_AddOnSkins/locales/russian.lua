local AceLocale = LibStub:GetLibrary("AceLocale-3.0");
local L = AceLocale:NewLocale("ElvUI", "ruRU");
if not L then return; end

L["AddOn Skins"] = "Скины аддонов";
L["Blizzard Skins"] = "Скины Blizzard";
L["Embed Type"] = "Тип встраивания";
L["Double"] = "Двойной";
L["Left Panel"] = "Левое окно";
L["Right Panel"] = "Правое окно";
L["Single"] = "Одинарный";