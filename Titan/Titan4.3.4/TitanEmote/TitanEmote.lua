-- ******************************** Constants *******************************
TITAN_EMOTE_ID =  "Emote";
TITAN_EMOTE_VERSION = "1.0.7.30200";
TITAN_EMOTE_BUTTON_ICON = "Interface\\AddOns\\TitanEmote\\TitanEmote";

-- *********************************  Strings *******************************
TITAN_EMOTE_BASE_WORD = "Emote";
TITAN_EMOTE_NO_LABEL = "Emote: No Emote Selected";
TITAN_EMOTE_TOOLTIP_TEXT = "Right click and navigate to select an emote.".."\n".."Left click to resend last selected";
TITAN_EMOTE_CLEAR_FAVOURITES = "Clear favourities";
TITAN_EMOTE_BEGINNING_WITH = "Emotes beginning with...";
TITAN_EMOTE_MENU_EMOTES = "Emotes";
TITAN_EMOTE_MENU_OPTIONS = "Options";
TITAN_EMOTE_MENU_OPTIONS_FAVOURITES = "Favourites";
TITAN_EMOTE_MENU_OPTIONS_CLEAR_FAVOURITES = "Clear Favourites";
TITAN_EMOTE_MENU_OPTIONS_SHOW_FAVOURITES = "Show favourities";
TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES = 5;
TITAN_EMOTE_MENU_OPTIONS_SHOW_LABEL_TEXT = "Show label text";

-- ******************************** Variables *******************************
local TITAN_BUILTIN_EMOTE_TABLE = {};
local MAXEMOTEINDEX = 451;

-- **************************************************************************
-- NAME : TitanPanelEmoteButton_OnLoad()
-- DESC : Registers the mod when it loads
-- **************************************************************************
function TitanPanelEmoteButton_OnLoad(self)
	self.registry = {
		id = TITAN_EMOTE_ID,
		version = TITAN_EMOTE_VERSION,
		menuText = TITAN_EMOTE_ID, 
		tooltipTitle = TITAN_EMOTE_ID.." "..TITAN_EMOTE_VERSION,
		tooltipTextFunction = "TitanPanelEmoteButton_GetToolTipText",
		buttonTextFunction = "TitanPanelEmoteButton_GetButtonText",
		category = "General",
		icon = TITAN_EMOTE_BUTTON_ICON,
		iconWidth = 16,
		savedVariables = {			
			ShowIcon = 1,
			ShowFavourites = 1,
			ShowColoredText = 1,
			ShowLabelText = 1
		}
	};
	
	self:RegisterEvent("PLAYER_ENTERING_WORLD");
	
	if (not EmoteArray) then 
		EmoteArray={};
		EmoteArray["TITAN_EMOTE_LAST_EMOTE"] = "Emote: No Emote Selected";
		EmoteArray["Favourites"] = {};
		EmoteArray["TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES"] = 5;
		EmoteArray["ShowButtonText"] = true;
	end	
	
end


-- **************************************************************************
-- NAME : TitanPanelEmoteButton_OnEvent()
-- DESC : This section will grab the events registered to the add on and act on them
-- **************************************************************************
function TitanPanelEmoteButton_OnEvent(self, event, ...)
  if ( event == "PLAYER_ENTERING_WORLD" ) then
		TITAN_BUILTIN_EMOTE_TABLE = {};
    for i = 1, 171, 1 do
    	ProperEmote = string.upper(string.sub(getglobal("EMOTE"..i.."_TOKEN"),1,1))..string.lower(string.sub(getglobal("EMOTE"..i.."_TOKEN"),2))
      table.insert(TITAN_BUILTIN_EMOTE_TABLE, ProperEmote);
    end
    table.sort(TITAN_BUILTIN_EMOTE_TABLE);
    
    -- Debug out all of the Emotes
    -- EmoteDebugOut = {};
    -- EmoteDebugOut["Emotes"] = {};
    -- for i = 1, MAXEMOTEINDEX, 1 do    	
    --   table.insert(EmoteDebugOut["Emotes"], getglobal("EMOTE"..i.."_TOKEN"));
    -- end
    -- table.sort(EmoteDebugOut["Emotes"]);
    
    	-- Grab the saved variable and put them in the correct place
    TITAN_EMOTE_BUTTON_TEXT = TITAN_EMOTE_BASE_WORD..": "..EmoteArray["TITAN_EMOTE_LAST_EMOTE"];
    TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES = EmoteArray["TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES"]
		return;
	end
end

-- *******************************************************************************************
-- Name: TitanPanelEmoteButton_GetToolTipText()
-- Desc: Gets our tool-tip text, what appears when we hover over our item on the Titan bar.
-- *******************************************************************************************
function TitanPanelEmoteButton_GetToolTipText()	
	return TITAN_EMOTE_TOOLTIP_TEXT;
end

-- *******************************************************************************************
-- Name: TitanPanelEmoteButton_GetButtonText()
-- Desc: Gets our button text, what appears on our button on the Titan bar.
-- *******************************************************************************************
function TitanPanelEmoteButton_GetButtonText()
	-- Set the TITAN_EMOTE_BUTTON_TEXT based on the ShowColoredText settings
	if (TitanGetVar(TITAN_EMOTE_ID, "ShowColoredText")) then
		TITAN_EMOTE_BUTTON_TEXT = TitanUtils_GetGreenText(EmoteArray["TITAN_EMOTE_LAST_EMOTE"]);
  else
  	TITAN_EMOTE_BUTTON_TEXT = EmoteArray["TITAN_EMOTE_LAST_EMOTE"];
  end
  if (TitanGetVar(TITAN_EMOTE_ID, "ShowLabelText")) then
  	TITAN_EMOTE_BUTTON_TEXT = TITAN_EMOTE_BASE_WORD..": "..TITAN_EMOTE_BUTTON_TEXT;
  end
	return TITAN_EMOTE_BUTTON_TEXT;
end

-- *******************************************************************************************
-- Name: TitanPanelRightClickMenu_PrepareEmoteMenu
-- Desc: Builds the right click config menu
-- *******************************************************************************************
function TitanPanelRightClickMenu_PrepareEmoteMenu()
	if ( UIDROPDOWNMENU_MENU_LEVEL == 1 ) then
	  TitanPanelRightClickMenu_AddTitle(TITAN_EMOTE_ID);
	  -- If wanted show the favourites list
	  if (TitanGetVar(TITAN_EMOTE_ID, "ShowFavourites")) then
	  	TitanPanelRightClickMenu_AddSpacer();
		  TitanPanelRightClickMenu_AddTitle(TITAN_EMOTE_MENU_OPTIONS_FAVOURITES.." ("..TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES..")");
		  for FavouriteIndex, FavouriteValue in pairs(EmoteArray["Favourites"]) do
			  MenuItem = {};
        MenuItem.text = FavouriteValue;
        MenuItem.value = FavouriteValue;
        MenuItem.func = TitanPanelRightClickMenu_EmoteOnClick;
	      UIDropDownMenu_AddButton(MenuItem, UIDROPDOWNMENU_MENU_LEVEL);
	    end	    
	    TitanPanelRightClickMenu_AddSpacer();
	  end
	  MenuItem = {};
    MenuItem.text = TITAN_EMOTE_MENU_EMOTES;
    MenuItem.value = TITAN_EMOTE_MENU_EMOTES;
	  MenuItem.hasArrow = 1;
	  UIDropDownMenu_AddButton(MenuItem);
	  TitanPanelRightClickMenu_AddToggleIcon(TITAN_EMOTE_ID);
	  TitanPanelRightClickMenu_AddToggleVar(TITAN_EMOTE_MENU_OPTIONS_SHOW_FAVOURITES, TITAN_EMOTE_ID, "ShowFavourites");	  
	  TitanPanelRightClickMenu_AddToggleColoredText(TITAN_EMOTE_ID);
	  TitanPanelRightClickMenu_AddToggleVar(TITAN_EMOTE_MENU_OPTIONS_SHOW_LABEL_TEXT, TITAN_EMOTE_ID, "ShowLabelText");
	  TitanPanelRightClickMenu_AddSpacer();
	  MenuItem = {};
    MenuItem.text = TITAN_EMOTE_MENU_OPTIONS;
    MenuItem.value = TITAN_EMOTE_MENU_OPTIONS;
	  MenuItem.hasArrow = 1;
	  UIDropDownMenu_AddButton(MenuItem);
	  TitanPanelRightClickMenu_AddSpacer();
	  TitanPanelRightClickMenu_AddCommand(TITAN_PANEL_MENU_HIDE, "Emote", TITAN_PANEL_MENU_FUNC_HIDE);
	elseif ( UIDROPDOWNMENU_MENU_LEVEL == 2 ) then -- we have more than one so check the UIDROPDOWNMENU_MENU_VALUE
		if ( UIDROPDOWNMENU_MENU_VALUE == TITAN_EMOTE_MENU_EMOTES ) then
			-- We are in the Emotes menu
		  -- The title is just simply a helper line to make it obvious what he user is looking at
		  TitanPanelRightClickMenu_AddTitle(TITAN_EMOTE_BEGINNING_WITH, UIDROPDOWNMENU_MENU_LEVEL);		
		  -- Now we iterate through the emotes in the TITAN_EMOTE_TABLE array to build the letter list
		  LetterTable = {};
		  for EmoteEntryIndex, EmoteEntryValue in pairs(TITAN_BUILTIN_EMOTE_TABLE) do
			  if (string.upper(EmoteEntryValue) ~= "UNUSED") then
			    NotThere = true;
			    for LetterEntryIndex, LetterEntryValue in pairs(LetterTable) do
				    if (LetterEntryValue == string.sub(EmoteEntryValue,1,1)) then				
					    NotThere = false;			
			      end
			    end
	  	    if (NotThere) then		  
			      table.insert(LetterTable,string.sub(EmoteEntryValue,1,1));
		      end
		    end
		  end
		  -- We now have the LetterTable populated with the unique first letters of the emotes, let's add them to the menu
		  for LetterEntryIndex, LetterEntryValue in pairs(LetterTable) do			
		    MenuItem = {};
        MenuItem.text = LetterEntryValue;
        MenuItem.value = LetterEntryValue;
	      MenuItem.hasArrow = 1;
	      UIDropDownMenu_AddButton(MenuItem, UIDROPDOWNMENU_MENU_LEVEL);
		  end
		elseif ( UIDROPDOWNMENU_MENU_VALUE == TITAN_EMOTE_MENU_OPTIONS ) then
		  MenuItem = {};
      MenuItem.text = TITAN_EMOTE_MENU_OPTIONS_FAVOURITES;
      MenuItem.value = TITAN_EMOTE_MENU_OPTIONS_FAVOURITES;
	    MenuItem.hasArrow = 1;
	    UIDropDownMenu_AddButton(MenuItem, UIDROPDOWNMENU_MENU_LEVEL);		  	
	  end
	elseif ( UIDROPDOWNMENU_MENU_LEVEL == 3 ) then -- we have more than one so check the UIDROPDOWNMENU_MENU_VALUE
		if ( UIDROPDOWNMENU_MENU_VALUE == TITAN_EMOTE_MENU_OPTIONS_FAVOURITES ) then			 		  
		  TitanPanelRightClickMenu_AddCommand(TITAN_EMOTE_MENU_OPTIONS_CLEAR_FAVOURITES, TITAN_EMOTE_ID, "TitanPanelEmoteButtonClearFavourites_Command", UIDROPDOWNMENU_MENU_LEVEL);
		  MenuItem = {};
      MenuItem.text = TITAN_EMOTE_MENU_OPTIONS_SHOW_FAVOURITES;
      MenuItem.value = TITAN_EMOTE_MENU_OPTIONS_SHOW_FAVOURITES;
	    MenuItem.hasArrow = 1;
	    UIDropDownMenu_AddButton(MenuItem, UIDROPDOWNMENU_MENU_LEVEL);
	  else			
      for EmoteEntryIndex, EmoteEntryValue in pairs(TITAN_BUILTIN_EMOTE_TABLE) do
			  if (UIDROPDOWNMENU_MENU_VALUE == string.sub(EmoteEntryValue,1,1)) then
			    MenuItem = {};
          MenuItem.text = EmoteEntryValue;
          MenuItem.value = EmoteEntryValue;
          MenuItem.func = TitanPanelRightClickMenu_EmoteOnClick;
	        UIDropDownMenu_AddButton(MenuItem, UIDROPDOWNMENU_MENU_LEVEL);
	      end
		  end
		end
	elseif ( UIDROPDOWNMENU_MENU_LEVEL == 4 ) then
		TitanPanelRightClickMenu_AddCommand("5", 5, "TitanPanelEmoteButtonSetFavourites_Command", UIDROPDOWNMENU_MENU_LEVEL);
		TitanPanelRightClickMenu_AddCommand("10", 10, "TitanPanelEmoteButtonSetFavourites_Command", UIDROPDOWNMENU_MENU_LEVEL);
		TitanPanelRightClickMenu_AddCommand("15", 15, "TitanPanelEmoteButtonSetFavourites_Command", UIDROPDOWNMENU_MENU_LEVEL);
	end
end

-- *******************************************************************************************
-- Name: TitanPanelRightClickMenu_EmoteOnClick()
-- Desc: Reacts to a menu item click
-- *******************************************************************************************
function TitanPanelRightClickMenu_EmoteOnClick()
	local InList = false;
	EmoteArray["TITAN_EMOTE_LAST_EMOTE"] = this.value;
	-- Check it's uniqueness and add it to the favourites assuming it is inside the maximum range
	for FavouriteIndex, FavouriteValue in pairs(EmoteArray["Favourites"]) do
	  if (FavouriteValue == this.value) then
		  InList = true;
		end
	end
	if not InList then
		if (#(EmoteArray["Favourites"]) >= TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES) then
			tremove(EmoteArray["Favourites"], 1);
			table.insert(EmoteArray["Favourites"], this.value);	    
		else
	    table.insert(EmoteArray["Favourites"], this.value);
	  end
	end
  DoEmote(this.value, target);
  TitanPanelButton_UpdateButton(TITAN_EMOTE_ID)
end

-- *******************************************************************************************
-- Name: TitanPanelEmoteButton_OnClick(button)
-- Desc: Reacts to a click on the button
-- *******************************************************************************************
function TitanPanelEmoteButton_OnClick(self, button)
	if (button == "LeftButton") then
		if (TitanPanelEmoteButton_GetButtonText(TITAN_EMOTE_ID) ~= TITAN_EMOTE_NO_LABEL) then
		  DoEmote(EmoteArray["TITAN_EMOTE_LAST_EMOTE"], target);
		  TitanPanelButton_UpdateButton(TITAN_EMOTE_ID)
		end
	end
end

-- *******************************************************************************************
-- Name: TitanPanelEmoteButtonClearFavourites_Command()
-- Desc: Clears the favourites
-- *******************************************************************************************
function TitanPanelEmoteButtonClearFavourites_Command()
	EmoteArray["Favourites"] = {};
end

-- *******************************************************************************************
-- Name: TitanPanelEmoteButtonSetFavourites_Command()
-- Desc: Sets the maximum number of favourites
-- *******************************************************************************************

function TitanPanelEmoteButtonSetFavourites_Command()
	TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES = this.value;
	EmoteArray["TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES"] = TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES;
	while (#(EmoteArray["Favourites"]) > TITAN_EMOTE_MENU_OPTIONS_MAX_FAVOURITES) do
    tremove(EmoteArray["Favourites"], 1);
  end
end

-- *******************************************************************************************
-- Name: ToggleButtonText()
-- Desc: Toggles the showing of the 'Emote:' part of the button text
-- *******************************************************************************************
function ToggleButtonText()
	if ( EmoteArray["ShowButtonText"] ) then
		EmoteArray["ShowButtonText"] = false;
	else
		EmoteArray["ShowButtonText"] = true;
	end
end
