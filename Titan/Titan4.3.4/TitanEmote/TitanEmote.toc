## Title: Titan Panel [|cffeda55fEmote|r] |cff00aa001.0.6.30200|r
## Author: Goose
## Notes: A Titan button for easy selection of emotes.
## Version: 1.0.8.30300
## eMail: wowmods@little-barn-cottage.co.uk
## Interface: 30300
## Dependencies: Titan
## SavedVariables: EmoteArray, EmoteDebugOut
TitanEmote.xml
TitanEmote.lua