﻿## Interface: 30300
## Title: Titan Panel [|cff00ff00CamDist|r]
## Author: Wishko
## Version: 1.2.5
## Notes: TitanPanel - Camera Distance, sets a maximum range of camera distance
## Notes-ruRU: TitanPanel - Camera Distance позволяет устанавливать максимальную дальность обзора камеры
## eMail: Wish1250@gmail.com
## RequiredDeps: Titan
## DefaultState: Enabled
## LoadOnDemand: 0
Localization.lua
TitanCamDist.lua
TitanCamDist.xml
