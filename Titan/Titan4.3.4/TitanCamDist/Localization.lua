﻿--[[ 
LOCALIZATION for                       ЛОКАЛИЗАЦИЯ для
TitanPanel: CameraDistance             ТитанПанель: ДистанцияКамеры 

Author: Wishmaster (EU-Thermoplugg)    Автор: Вишмастер (RU-Термоштепсель)   
E-mail: Wish1250@gmail.com             Мыло:  Wish1250@gmail.com    
Create Date: 29 jule 2010              Дата создания: 29 июля 2010  
Locales: RU,EN                         Языки: RU,EN
Last Update: 18 august 2010            Последнее обновление: 18 августа 2010 
For Version: 1.2.5                     Для Версии: 1.2.5
]]--

-- Общая
local L = LibStub("AceLocale-3.0"):NewLocale("TitanCamDist","enUS",true)
if L then 
L["TITAN_CAMDIST_TOOLTIPTITLE"] = "TitanPanel: Camera Distance" 
L["TITAN_CAMDIST_TOOLTIPTEXT"]  = "|cffffff00Changing the maximum camera distance\n  |cff00ff00 0|cffffffff - first-person view\n |cff00ff0015|cffffffff - standard\n |cff00ff0050|cffffffff - maximum\n|cff00ffffBlue|cffffffff color indicates the |cff00ffffdefault|cffffffff setting\n|cff00ff00Right-click to select the distance.|r"
L["TITAN_CAMDIST_MENUTEXT"]     = "Select Distance:"
L["TITAN_CAMDIST_BUTTON_LABEL"] = "Distance: "
L["TITAN_CAMDIST_METERS"]       = " yards"
L["TITAN_CAMDIST_ONE_METER"]    = " yard"
L["TITAN_CAMDIST_TWO_METER"]    = " yards"
L["TITAN_CAMDIST_AUTOZOOM"]     = "Autochange distance"
L["TITAN_CAMDIST_OPTIONS"]      = "Camera Options"
L["TITAN_CAMDIST_OPT_MOVE"]     = "Move"
L["TITAN_CAMDIST_OPT_YAW"]      = "Yaw"
L["TITAN_CAMDIST_OPT_SMOOTH"]   = "Smooth"
L["TITAN_CAMDIST_SMOOTH_TITLE"] = "Smooth Style"
L["CAMDIST_SMOOTH_0"]           = "Never"
L["CAMDIST_SMOOTH_1"]           = "Horizontal when Moving"
L["CAMDIST_SMOOTH_2"]           = "Always"
L["CAMDIST_SMOOTH_3"]           = "Horizontal"
L["CAMDIST_SMOOTH_4"]           = "When Moving"
L["CAMDIST_SMOOTH_5"]           = "Horizontal Instant"
L["TITAN_CAMDIST_YAW_TITLE"]    = "Yaw Speed"
L["TITAN_CAMDIST_MOVE_TITLE"]   = "Move Speed"
L["TITAN_CAMDIST_DEFAULT"]      = "Default"
L["TITAN_CAMDIST_SLOW"]         = "Slow"
L["TITAN_CAMDIST_VERYSLOW"]     = "Very Slow"
L["TITAN_CAMDIST_MAX"]          = "Maximum"
L["TITAN_CAMDIST_QUICK"]        = "Quickly"
L["TITAN_CAMDIST_VERYQUICK"]    = "Very Quickly"
L["TITAN_CAMDIST_ON"]           = "Turn On"
L["TITAN_CAMDIST_OFF"]          = "Turn Off"
L["TITAN_CAMDIST_WCOLL"]        = "Water Collision"
L["TITAN_CAMDIST_TERTILT"]      = "Terrain Tilt"
L["TITAN_CAMDIST_BOBBING"]      = "Bobbing"
end

-- Русская
local L = LibStub("AceLocale-3.0"):NewLocale("TitanCamDist","ruRU")
if L then 
L["TITAN_CAMDIST_TOOLTIPTITLE"] = "ТитанПанель: Дистанция камеры" 
L["TITAN_CAMDIST_TOOLTIPTEXT"]  = "|cffffff00Изменение максимальной дистанции камеры\n  |cff00ff00 0|cffffffff - вид от первого лица\n |cff00ff0015|cffffffff - стандартная\n |cff00ff0050|cffffffff - максимальная\n|cff00ffffГолубой|cffffffff цвет указывает на |cff00ffffстандартные|cffffffff настройки\n|cff00ff00Правый клик для выбора дистанции."
L["TITAN_CAMDIST_MENUTEXT"]     = "Выберите Дистанцию:"
L["TITAN_CAMDIST_BUTTON_LABEL"] = "Дистанция: "
L["TITAN_CAMDIST_METERS"]       = " метров"
L["TITAN_CAMDIST_ONE_METER"]    = " метр"
L["TITAN_CAMDIST_TWO_METER"]    = " метра"
L["TITAN_CAMDIST_AUTOZOOM"]     = "Автоизм. положения"
L["TITAN_CAMDIST_OPTIONS"]      = "Опции камеры"
L["TITAN_CAMDIST_OPT_MOVE"]     = "Прокрутка"
L["TITAN_CAMDIST_OPT_YAW"]      = "Поворот"
L["TITAN_CAMDIST_OPT_SMOOTH"]   = "Выравнивание"
L["TITAN_CAMDIST_SMOOTH_TITLE"] = "Стиль выравнивания"
L["CAMDIST_SMOOTH_0"]           = "Никогда"
L["CAMDIST_SMOOTH_1"]           = "Горизонт. при движении"
L["CAMDIST_SMOOTH_2"]           = "Всегда"
L["CAMDIST_SMOOTH_3"]           = "Горизонтальное"
L["CAMDIST_SMOOTH_4"]           = "При движении"
L["CAMDIST_SMOOTH_5"]           = "Горизонт. мгновенно"
L["TITAN_CAMDIST_YAW_TITLE"]    = "Скорость поворота"
L["TITAN_CAMDIST_MOVE_TITLE"]   = "Скорость прокрутки"
L["TITAN_CAMDIST_DEFAULT"]      = "По умолчанию"
L["TITAN_CAMDIST_SLOW"]         = "Медленно"
L["TITAN_CAMDIST_VERYSLOW"]     = "Очень медленно"
L["TITAN_CAMDIST_MAX"]          = "Максимально"
L["TITAN_CAMDIST_QUICK"]        = "Быстро"
L["TITAN_CAMDIST_VERYQUICK"]    = "Очень быстро"
L["TITAN_CAMDIST_ON"]           = "Включить"
L["TITAN_CAMDIST_OFF"]          = "Отключить"
L["TITAN_CAMDIST_WCOLL"]        = "Брызги воды"
L["TITAN_CAMDIST_TERTILT"]      = "Следование рельефу"
L["TITAN_CAMDIST_BOBBING"]      = "Покачивание головы"
end