﻿--[[                  
  TITAN PANEL: CAMERA DISTANCE
  Описание/Description:
• ru:  Дистанция Камеры - небольшой плагин для Титан Панели. Спомощью него в выпадающем меню можно выбрать максимальную дистанцию обзора камеры: от минимального - 0 метров (вид от первого лица) 
        до максимального - 50 метров
• en:  Camera Distance - a small plug-in for Titan Panel. With him in dropdown menu, you can select the maximum range of camera distance: from minimum - 0 yards (first-person view)
        to maximum - 50 yards

 Author: Wishmaster (EU-Thermoplugg)    Автор: Вишмастер (RU-Термоштепсель)   
 E-mail: Wish1250@gmail.com             Мыло: Wish1250@gmail.com    
 Create Date: 9 june 2010               Дата создания: 9 июня 2010  
 Locales: RU,EN                         Языки: RU,EN
 CurrentVersion: 1.2.5                  Текущая версия: 1.2.5

  Изменения/ChangeLog:
• v1.1.2 (9 june 2010)    - Релиз
                            Initial Release
• v1.1.3 (8 jule 2010)    - Небольшие изменения в загрузке плагина; добавлены, изменены и локализованы некоторые подсказки 
                            Minor changes in plugin loading; added, modified and localized some tips
• v1.2.0 (10 jule 2010)   - Изменена структура аддона. Теперь можно выбрать любую дистанцию, а не каждую вторую. Исправлены небольшие ошибки      
                            Changed addon structure. Now you can choose any distance, not every second. Fixed small bugs
• v1.2.1 (14 jule 2010)   - Исправлены небольшие ошибки
                            Fixed small bugs
• v1.2.2 (29 jule 2010)   - Добавлено отображение выбранной дистанции на кнопке плагина. Локализация упорядочена, вынесена в отдельный файл и теперь использует LibStub("AceLocale-3.0")
                            Added display of selected distance on plug-in button. Localization ordered, moved to separate file
• v1.2.3 (1 august 2010)  - Изменены некоторые подсказки. Теперь дистанция камеры в Интерфейсе автоматически устанавливается на минимальную после выбора любой дистанции в плагине (ранее требовалось сделать это вручную).
                            Удалена кнопка "Помощь". Добавлена функция "Автоизменение дистанции", при выборе которой камера будет автоматически устанавливаться на максимальное расстояние (ранее требовалось сделать это вручную)
                            Если кто-то читает эти ChangeLog'и, то напишите в игре "/CamDist"
                            Modified some tips. Now camera distance in Interface is automatically installed to minimum after selecting any distance in plugin (previously required to do it manually).
                            Deleted "Help" button. Added function "Autochange distance",when it choosing, camera will automatically set at maximum distance (previously required to do it manually)
• v1.2.4 (14 august 2010) - Изменена иконка. Изменены некоторые подсказки.
                            Добавлены новые функции в "Опциях камеры": Изменение скорости прокрутки, Поворота и Стиля Следования камеры.
                            Changed button icon. Modified some tips.
                            In "Camera Options" added new functions: Change the Camera Yaw Speed, Camera Move Speed and Camera Smooth Style
• v1.2.5 (21 august 2010) - Добавлены новые функции в "Опциях камеры": Брызги воды, Следование рельефу, Покачивание головы(при виде от первого лица)
                            Теперь Голубой цвет указывает на стандартные настройки интерфейса. Добавлена опция показа/скрытия Текста Ярлыка
                            In "Camera Options" added new functions: Water Collision, Terrain Tilt, Bobbing (first-person view)
                            Now Blue color indicates default Interface configuration. Added option to show/hide Label text
]]--

local TITAN_CAMDIST_VERSION = "1.2.5"
local TITAN_CAMDIST_ID = "CamDist"
local updateTable = {TITAN_CAMDIST_ID, TITAN_PANEL_UPDATE_ALL};
local L = LibStub("AceLocale-3.0"):GetLocale("TitanCamDist", true)
------------------------------------------------------------------------
function TitanPanelCamDistButton_OnLoad(self)
	self.registry = {
		id = TITAN_CAMDIST_ID,
		category = "Interface",
        version = TITAN_CAMDIST_VERSION,
		menuText = L["TITAN_CAMDIST_MENUTEXT"],
		buttonTextFunction = "TitanPanelCamDist_GetButtonText",
        tooltipTitle = L["TITAN_CAMDIST_TOOLTIPTITLE"],
        tooltipTextFunction = "TitanPanelCamDist_GetTooltipText",
		icon = "Interface\\Icons\\Ability_Hunter_MarkedForDeath",
		iconWidth = 16,
		savedVariables = {
			ShowIcon = 1,
            ShowLabelText = 1,
            CameraAutoZoom = 1,
			}}; 
	self:RegisterEvent("PLAYER_ENTERING_WORLD");
end
------------------------------------------------------------------------
function TitanPanelCamDistButton_OnEvent(self, event, ...)
if event == "PLAYER_ENTERING_WORLD" then self:SetScript("OnUpdate", TitanPanelCamDistButton_OnUpdate) end
end 
function TitanPanelCamDistButton_OnUpdate(self) 
TitanPanelPluginHandle_OnUpdate(updateTable)
end
--------------------Текст--Кнопки---------------------------------------
function TitanPanelCamDist_GetButtonText()
return "|cffffffff"..L["TITAN_CAMDIST_BUTTON_LABEL"],"|cff00ff00"..floor(GetCVar("cameraDistanceMax"))
end
--------------------Текст--Подсказки------------------------------------
function TitanPanelCamDist_GetTooltipText() 
return L["TITAN_CAMDIST_TOOLTIPTEXT"]
end
------------------------------------------------------------------------
-- ГЛАВНАЯ ЧАСТЬ. Описание всех функций.(Ничего лучше придумать не мог чем все это написать))
------------------------------------------------------------------------
-- Максимальная дистанция камеры от 0 до 50 и сброс дистанции камеры в Интерфейсе
local function CamDist0() 
SetCVar("cameraDistanceMax",0)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(0.5) end
end 
local function CamDist1()
SetCVar("cameraDistanceMax",1)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(0.5) end
end 
local function CamDist2()
SetCVar("cameraDistanceMax",2)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(0.5) end
end 
local function CamDist3()
SetCVar("cameraDistanceMax",3)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(0.5) end
end 
local function CamDist4()
SetCVar("cameraDistanceMax",4)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(0.5) end
end 
local function CamDist5()
SetCVar("cameraDistanceMax",5)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(0.5) end
end 
local function CamDist6()
SetCVar("cameraDistanceMax",6)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1) end
end
local function CamDist7()
SetCVar("cameraDistanceMax",7)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1) end
end 
local function CamDist8()
SetCVar("cameraDistanceMax",8)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1) end
end 
local function CamDist9()
SetCVar("cameraDistanceMax",9)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1) end
end 
local function CamDist10()
SetCVar("cameraDistanceMax",10)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1) end
end 
local function CamDist11()
SetCVar("cameraDistanceMax",11)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist12()
SetCVar("cameraDistanceMax",12)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist13()
SetCVar("cameraDistanceMax",13)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end
local function CamDist14()
SetCVar("cameraDistanceMax",14)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist15()
SetCVar("cameraDistanceMax",15)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist16()
SetCVar("cameraDistanceMax",16)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist17()
SetCVar("cameraDistanceMax",17)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist18()
SetCVar("cameraDistanceMax",18)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist19()
SetCVar("cameraDistanceMax",19)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist20()
SetCVar("cameraDistanceMax",20)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end
local function CamDist21()
SetCVar("cameraDistanceMax",21)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist22()
SetCVar("cameraDistanceMax",22)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist23()
SetCVar("cameraDistanceMax",23)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist24()
SetCVar("cameraDistanceMax",24)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end
local function CamDist25()
SetCVar("cameraDistanceMax",25)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(1.5) end
end 
local function CamDist26()
SetCVar("cameraDistanceMax",26)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist27()
SetCVar("cameraDistanceMax",27)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist28()
SetCVar("cameraDistanceMax",28)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist29()
SetCVar("cameraDistanceMax",29)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist30()
SetCVar("cameraDistanceMax",30)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist31()
SetCVar("cameraDistanceMax",31)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist32()
SetCVar("cameraDistanceMax",32)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist33()
SetCVar("cameraDistanceMax",33)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist34()
SetCVar("cameraDistanceMax",34)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2) end
end 
local function CamDist35()
SetCVar("cameraDistanceMax",35)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist36()
SetCVar("cameraDistanceMax",36)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist37()
SetCVar("cameraDistanceMax",37)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist38()
SetCVar("cameraDistanceMax",38)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist39()
SetCVar("cameraDistanceMax",39)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist40()
SetCVar("cameraDistanceMax",40)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist41()
SetCVar("cameraDistanceMax",41)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist42()
SetCVar("cameraDistanceMax",42)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist43()
SetCVar("cameraDistanceMax",43)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist44()
SetCVar("cameraDistanceMax",44)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist45()
SetCVar("cameraDistanceMax",45)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(2.5) end
end 
local function CamDist46()
SetCVar("cameraDistanceMax",46)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(3) end
end 
local function CamDist47()
SetCVar("cameraDistanceMax",47)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(3) end
end 
local function CamDist48()
SetCVar("cameraDistanceMax",48)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(3) end
end 
local function CamDist49()
SetCVar("cameraDistanceMax",49)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(3) end
end 
local function CamDist50()
SetCVar("cameraDistanceMax",50)
SetCVar("cameraDistanceMaxFactor",1)
if (TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom")) then  MoveViewOutStart(3) end
end 
-- Автоизменение дистанции
local function CamDistAutoZoom()
TitanToggleVar(TITAN_CAMDIST_ID, "CameraAutoZoom");
MoveViewOutStop()
TitanPanelPluginHandle_OnUpdate(updateTable)
end
------------------------------------------------------------------------
-- МЕНЮ. Часть функций перенесена сюда из-за ошибки "function has more than 60 upvalues"
------------------------------------------------------------------------
function TitanPanelRightClickMenu_PrepareCamDistMenu()
-- Cкорость поворота камеры (1-360)
local function CamDistYawMoveSpeed02()
SetCVar("cameraYawMoveSpeed",70)
end
local function CamDistYawMoveSpeed05()
SetCVar("cameraYawMoveSpeed",120)
end
local function CamDistYawMoveSpeed1()
SetCVar("cameraYawMoveSpeed",200)
end
local function CamDistYawMoveSpeed12()
SetCVar("cameraYawMoveSpeed",270)
end
local function CamDistYawMoveSpeed14()
SetCVar("cameraYawMoveSpeed",320)
end
local function CamDistYawMoveSpeed18()
SetCVar("cameraYawMoveSpeed",360)
end
-- Стиль Выравнивания камеры (0-5)
local function CamDistSmoothStyle0()
SetCVar("cameraSmoothStyle",0)
end
local function CamDistSmoothStyle1()
SetCVar("cameraSmoothStyle",1)
end
local function CamDistSmoothStyle2()
SetCVar("cameraSmoothStyle",2)
end
local function CamDistSmoothStyle3()
SetCVar("cameraSmoothStyle",3)
end
local function CamDistSmoothStyle4()
SetCVar("cameraSmoothStyle",4)
end
local function CamDistSmoothStyle5()
SetCVar("cameraSmoothStyle",5)
end
-- Cкорость прокуртки камеры (1-50)
local function CamDistMoveSpeed05()
SetCVar("cameraDistanceMoveSpeed",4)
end
local function CamDistMoveSpeed1()
SetCVar("cameraDistanceMoveSpeed",9)
end
local function CamDistMoveSpeed15()
SetCVar("cameraDistanceMoveSpeed",14)
end
local function CamDistMoveSpeed2()
SetCVar("cameraDistanceMoveSpeed",20)
end
local function CamDistMoveSpeed3()
SetCVar("cameraDistanceMoveSpeed",28)
end
local function CamDistMoveSpeed5()
SetCVar("cameraDistanceMoveSpeed",45)
end
-- "Брызги воды"
local function CamDistWCollOn()
SetCVar("cameraWaterCollision",1)
end
local function CamDistWCollOff()
SetCVar("cameraWaterCollision",0)
end
-- Следование рельефу
local function CamDistTerTiltOn()
SetCVar("cameraTerrainTilt",1)
end
local function CamDistTerTiltOff()
SetCVar("cameraTerrainTilt",0)
end
-- Покачивание головы
local function CamDistBobbingOn()
SetCVar("cameraBobbing",1)
end
local function CamDistBobbingOff()
SetCVar("cameraBobbing",0)
end
local info;
------------------------------------------------------------------------
-- 3й УРОВЕНЬ
------------------------------------------------------------------------
        if ( UIDROPDOWNMENU_MENU_LEVEL == 3 ) then
        -- Опция "Скорость прокуртки камеры"
        if ( UIDROPDOWNMENU_MENU_VALUE == "CamDistOptMove" )  then
        TitanPanelRightClickMenu_AddTitle(L["TITAN_CAMDIST_MOVE_TITLE"],3)
        info = {};
		info.text = ("х|cff00ff000.5|r "..L["TITAN_CAMDIST_SLOW"])
        info.func = function() CamDistMoveSpeed05() end;
		UIDropDownMenu_AddButton(info,3) 

        info = {};
		info.text = ("х|cff00ff001.0|r |cff00ffff"..L["TITAN_CAMDIST_DEFAULT"])
        info.func = function() CamDistMoveSpeed1() end;
		UIDropDownMenu_AddButton(info,3) 

        info = {};
		info.text = ("х|cff00ff001.5|r "..L["TITAN_CAMDIST_QUICK"])
        info.func = function() CamDistMoveSpeed15() end;
		UIDropDownMenu_AddButton(info,3)

        info = {};
		info.text = ("х|cff00ff002.0|r "..L["TITAN_CAMDIST_VERYQUICK"])
        info.func = function() CamDistMoveSpeed2() end;
		UIDropDownMenu_AddButton(info,3)

        info = {};
		info.text = ("х|cff00ff003.0|r "..L["TITAN_CAMDIST_VERYQUICK"])
        info.func = function() CamDistMoveSpeed3() end;
		UIDropDownMenu_AddButton(info,3)

        info = {};
		info.text = ("х|cff00ff005.0|r "..L["TITAN_CAMDIST_MAX"])
        info.func = function() CamDistMoveSpeed5() end;
		UIDropDownMenu_AddButton(info,3)
        end

        -- Опция "Скорость поворота камеры"
        if ( UIDROPDOWNMENU_MENU_VALUE == "CamDistOptYaw" ) then
        TitanPanelRightClickMenu_AddTitle(L["TITAN_CAMDIST_YAW_TITLE"],3)
        info = {};
		info.text = ("х|cff00ff000.2|r "..L["TITAN_CAMDIST_VERYSLOW"])
        info.func = function() CamDistYawMoveSpeed02() end;
		UIDropDownMenu_AddButton(info,3) 
    
        info = {};
		info.text = ("х|cff00ff000.5|r "..L["TITAN_CAMDIST_SLOW"])
        info.func = function() CamDistYawMoveSpeed05() end;
		UIDropDownMenu_AddButton(info,3)

        info = {};
		info.text = ("х|cff00ff001.0|r |cff00ffff"..L["TITAN_CAMDIST_DEFAULT"])
        info.func = function() CamDistYawMoveSpeed1() end;
		UIDropDownMenu_AddButton(info,3)

        info = {};
		info.text = ("х|cff00ff001.2|r "..L["TITAN_CAMDIST_QUICK"])
        info.func = function() CamDistYawMoveSpeed12() end;
		UIDropDownMenu_AddButton(info,3)

        info = {};
		info.text = ("х|cff00ff001.4|r "..L["TITAN_CAMDIST_VERYQUICK"])
        info.func = function() CamDistYawMoveSpeed14() end;
		UIDropDownMenu_AddButton(info,3)

        info = {};
		info.text = ("х|cff00ff001.8|r "..L["TITAN_CAMDIST_MAX"])
        info.func = function() CamDistYawMoveSpeed18() end;
		UIDropDownMenu_AddButton(info,3)
        end
        
        -- Опция "Выравнивание камеры" (0-никогда,2-всегда,4-только при движении,3-горизонтальное,1-горизонтальное при движении,5-горизонтальное мгновенное)
        if  ( UIDROPDOWNMENU_MENU_VALUE == "CamDistOptSmooth" ) then
        TitanPanelRightClickMenu_AddTitle(L["TITAN_CAMDIST_SMOOTH_TITLE"],3)
        info = {};
		info.text = (L["CAMDIST_SMOOTH_0"])
        info.func = function() CamDistSmoothStyle0() end;
		UIDropDownMenu_AddButton(info,3)
        info = {};
		info.text = (L["CAMDIST_SMOOTH_2"])
        info.func = function() CamDistSmoothStyle2() end;
		UIDropDownMenu_AddButton(info,3)
        info = {};
		info.text = ("|cff00ffff"..L["CAMDIST_SMOOTH_4"])
        info.func = function() CamDistSmoothStyle4() end;
		UIDropDownMenu_AddButton(info,3)
        info = {};
		info.text = (L["CAMDIST_SMOOTH_3"])
        info.func = function() CamDistSmoothStyle3() end;
		UIDropDownMenu_AddButton(info,3)
        info = {};
		info.text = (L["CAMDIST_SMOOTH_1"])
        info.func = function() CamDistSmoothStyle1() end;
		UIDropDownMenu_AddButton(info,3)
        info = {};
		info.text = (L["CAMDIST_SMOOTH_5"])
        info.func = function() CamDistSmoothStyle5() end;
		UIDropDownMenu_AddButton(info,3)
        end 
        
        if  ( UIDROPDOWNMENU_MENU_VALUE == "CamDistOptWColl" ) then
        TitanPanelRightClickMenu_AddTitle(L["TITAN_CAMDIST_WCOLL"],3)
        info = {};
		info.text = ("|cff00ffff"..L["TITAN_CAMDIST_ON"])
        info.func = function() CamDistWCollOn() end;
		UIDropDownMenu_AddButton(info,3)
        info = {};
		info.text = (L["TITAN_CAMDIST_OFF"])
        info.func = function() CamDistWCollOff() end;
		UIDropDownMenu_AddButton(info,3)
        end
        
        if  ( UIDROPDOWNMENU_MENU_VALUE == "CamDistOptTerTilt" ) then
        TitanPanelRightClickMenu_AddTitle(L["TITAN_CAMDIST_TERTILT"],3)
        info = {};
		info.text = (L["TITAN_CAMDIST_ON"])
        info.func = function() CamDistTerTiltOn() end;
		UIDropDownMenu_AddButton(info,3)
        info = {};
		info.text = ("|cff00ffff"..L["TITAN_CAMDIST_OFF"])
        info.func = function() CamDistTerTiltOff() end;
		UIDropDownMenu_AddButton(info,3)
        end
        
        if  ( UIDROPDOWNMENU_MENU_VALUE == "CamDistOptBobbing" ) then
        TitanPanelRightClickMenu_AddTitle(L["TITAN_CAMDIST_BOBBING"],3)
        info = {};
		info.text = (L["TITAN_CAMDIST_ON"])
        info.func = function() CamDistBobbingOn() end;
		UIDropDownMenu_AddButton(info,3)
        info = {};
		info.text = ("|cff00ffff"..L["TITAN_CAMDIST_OFF"])
        info.func = function() CamDistBobbingOff() end;
		UIDropDownMenu_AddButton(info,3)
        end
------------------------------------------------------------------------
-- 2й УРОВЕНЬ
------------------------------------------------------------------------   
        elseif ( UIDROPDOWNMENU_MENU_LEVEL == 2 ) then   
        -- Меню 2го уровня, Дистанции 1-9 
		if ( UIDROPDOWNMENU_MENU_VALUE == "CamDist0" ) then
        info = {};
		info.text = ("|cff00ff001|r"..L["TITAN_CAMDIST_ONE_METER"].."")
        info.func = function() CamDist1() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff002|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist2() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff003|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist3() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff004|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist4() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff005|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist5() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff006|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist6() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff007|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist7() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff008|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist8() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff009|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist9() end;
		UIDropDownMenu_AddButton(info,2) 
        end

        -- Меню 2го уровня, Дистанции 11-19   
        if ( UIDROPDOWNMENU_MENU_VALUE == "CamDist10" ) then
		info = {};
		info.text = ("|cff00ff0011|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist11() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0012|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist12() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff0013|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist13() end;
		UIDropDownMenu_AddButton(info,2) 
                                
        info = {};
		info.text = ("|cff00ff0014|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist14() end;
		UIDropDownMenu_AddButton(info,2) 
             
        info = {};
		info.text = ("|cff00ffff15"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist15() end;
		UIDropDownMenu_AddButton(info,2) 
                        
        info = {};
		info.text = ("|cff00ff0016|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist16() end;
		UIDropDownMenu_AddButton(info,2) 
                               
        info = {};
		info.text = ("|cff00ff0017|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist17() end;
		UIDropDownMenu_AddButton(info,2)
                        
        info = {};
		info.text = ("|cff00ff0018|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist18() end;
		UIDropDownMenu_AddButton(info,2) 
                             
        info = {};
		info.text = ("|cff00ff0019|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist19() end;
		UIDropDownMenu_AddButton(info,2)     
        end
   
        -- Меню 2го уровня, Дистанции 21-29          
        if ( UIDROPDOWNMENU_MENU_VALUE == "CamDist20" ) then
		info = {};
		info.text = ("|cff00ff0021|r"..L["TITAN_CAMDIST_ONE_METER"].."")
        info.func = function() CamDist21() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0022|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist22() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff0023|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist23() end;
		UIDropDownMenu_AddButton(info,2) 
                
        info = {};
		info.text = ("|cff00ff0024|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist24() end;
		UIDropDownMenu_AddButton(info,2) 
                        
        info = {};
		info.text = ("|cff00ff0025|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist25() end;
		UIDropDownMenu_AddButton(info,2) 
                        
        info = {};
		info.text = ("|cff00ff0026|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist26() end;
		UIDropDownMenu_AddButton(info,2) 
                        
        info = {};
		info.text = ("|cff00ff0027|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist27() end;
		UIDropDownMenu_AddButton(info,2) 
                        
        info = {};
		info.text = ("|cff00ff0028|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist28() end;
		UIDropDownMenu_AddButton(info,2) 
                        
        info = {};
		info.text = ("|cff00ff0029|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist29() end;
		UIDropDownMenu_AddButton(info,2) 
        end
        
        -- Меню 2го уровня, Дистанции 31-39         
        if ( UIDROPDOWNMENU_MENU_VALUE == "CamDist30" ) then
        info = {};
		info.text = ("|cff00ff0031|r"..L["TITAN_CAMDIST_ONE_METER"].."")
        info.func = function() CamDist31() end;
		UIDropDownMenu_AddButton(info,2) 
        
		info = {};
		info.text = ("|cff00ff0032|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist32() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0033|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist33() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0034|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist34() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0035|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist35() end;
		UIDropDownMenu_AddButton(info,2) 
                  
        info = {};
		info.text = ("|cff00ff0036|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist36() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0037|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist37() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0038|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist38() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0039|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist39() end;
		UIDropDownMenu_AddButton(info,2) 
        end
        
        -- Меню 2го уровня, Дистанции 41-49         
        if ( UIDROPDOWNMENU_MENU_VALUE == "CamDist40" ) then
        info = {};
		info.text = ("|cff00ff0041|r"..L["TITAN_CAMDIST_ONE_METER"].."")
        info.func = function() CamDist41() end;
		UIDropDownMenu_AddButton(info,2) 
        
		info = {};
		info.text = ("|cff00ff0042|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist42() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0043|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist43() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0044|r"..L["TITAN_CAMDIST_TWO_METER"].."")
        info.func = function() CamDist44() end;
		UIDropDownMenu_AddButton(info,2) 
           
        info = {};
		info.text = ("|cff00ff0045|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist45() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0046|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist46() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0047|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist47() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0048|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist48() end;
		UIDropDownMenu_AddButton(info,2) 
        
        info = {};
		info.text = ("|cff00ff0049|r"..L["TITAN_CAMDIST_METERS"].."")
        info.func = function() CamDist49() end;
		UIDropDownMenu_AddButton(info,2) 
        end 
                                 
        if ( UIDROPDOWNMENU_MENU_VALUE == "CamDistOpt" ) then
        -- Меню 2го уровня, "Скорость прокуртки камеры" 
        info = {};
		info.text = L["TITAN_CAMDIST_OPT_MOVE"]
        info.value = "CamDistOptMove"
        info.hasArrow = 1;
		UIDropDownMenu_AddButton(info,2) 
        -- Меню 2го уровня, "Скорость поворота камеры"
        info = {};
		info.text = L["TITAN_CAMDIST_OPT_YAW"]
        info.value = "CamDistOptYaw"
        info.hasArrow = 1;
		UIDropDownMenu_AddButton(info,2) 
        -- Меню 2го уровня, "Выравнивание камеры"
        info = {};
		info.text = L["TITAN_CAMDIST_OPT_SMOOTH"]
        info.value = "CamDistOptSmooth"
        info.hasArrow = 1;
		UIDropDownMenu_AddButton(info,2)
        -- Меню 2го уровня, Water Collision
        info = {};
		info.text = L["TITAN_CAMDIST_WCOLL"]
        info.value = "CamDistOptWColl"
        info.hasArrow = 1;
		UIDropDownMenu_AddButton(info,2) 
        -- Меню 2го уровня, Покачивание
        info = {};
		info.text = L["TITAN_CAMDIST_BOBBING"] 
        info.value = "CamDistOptBobbing"
        info.hasArrow = 1;
		UIDropDownMenu_AddButton(info,2) 
        -- Меню 2го уровня, Следование рельефу
        info = {};
		info.text = L["TITAN_CAMDIST_TERTILT"]
        info.value = "CamDistOptTerTilt"
        info.hasArrow = 1;
		UIDropDownMenu_AddButton(info,2) 
        end 
        else       
------------------------------------------------------------------------
-- 1й УРОВЕНЬ
------------------------------------------------------------------------
    -- Дистанции 0,10,20,30,40,50.   
   	TitanPanelRightClickMenu_AddTitle(TitanPlugins[TITAN_CAMDIST_ID].menuText)
   	info = {};
   	info.text = ("   |cff00ff000|r"..L["TITAN_CAMDIST_METERS"].."")
    info.value = "CamDist0"
    info.func = function() CamDist0() end;
    info.hasArrow = 1;
    UIDropDownMenu_AddButton(info);
    
    info = {};
   	info.text = ("|cff00ff0010|r"..L["TITAN_CAMDIST_METERS"].."")
    info.value = "CamDist10"
    info.func = function() CamDist10() end;
    info.hasArrow = 1;
    UIDropDownMenu_AddButton(info);
    
   	info = {};
   	info.text = ("|cff00ff0020|r"..L["TITAN_CAMDIST_METERS"].."")
    info.value = "CamDist20"
    info.func = function() CamDist20() end;
    info.hasArrow = 1;
    UIDropDownMenu_AddButton(info);

   	info = {};
   	info.text = ("|cff00ff0030|r"..L["TITAN_CAMDIST_METERS"].."")
    info.hasArrow = 1;
    info.value = "CamDist30"
    info.func = function() CamDist30() end;
    UIDropDownMenu_AddButton(info);

   	info = {};
   	info.text = ("|cff00ff0040|r"..L["TITAN_CAMDIST_METERS"].."")
    info.value = "CamDist40"
    info.func = function() CamDist40() end;
    info.hasArrow = 1
    UIDropDownMenu_AddButton(info);
    
    info = {};
   	info.text = ("|cff00ff0050|r"..L["TITAN_CAMDIST_METERS"].."")
    info.value = "CamDist50"
    info.func = function() CamDist50() end;
    UIDropDownMenu_AddButton(info);
    
    TitanPanelRightClickMenu_AddSpacer()
    
    -- Дополнительные опции ()
    info = {};
   	info.text = L["TITAN_CAMDIST_OPTIONS"]
    info.value = "CamDistOpt"
    info.hasArrow = 1;
    UIDropDownMenu_AddButton(info); 
    
    info = {};
	info.text = L["TITAN_CAMDIST_AUTOZOOM"];
    info.func = function() CamDistAutoZoom() end;
    info.checked = TitanGetVar(TITAN_CAMDIST_ID, "CameraAutoZoom");
    UIDropDownMenu_AddButton(info)
    
	TitanPanelRightClickMenu_AddToggleIcon(TITAN_CAMDIST_ID); 
    TitanPanelRightClickMenu_AddToggleLabelText(TITAN_CAMDIST_ID);
end
end

-- Ну надо же было добавить сюда чего-нибудь интересного, вот и придумал это)):
SLASH_TITANCAMDIST1 = "/CamDist"
SlashCmdList["TITANCAMDIST"] = TITANCAMDIST_Command
local sfuncoff, sfuncon
if GetLocale() == "ruRU" then sfuncoff = "Секретная функция |cffff0000Отключена|r. Спасибо за использование этого аддона. /Вишмастер" else sfuncoff = "Secret Function |cffff0000OFF|r. Thanks for using this Addon. /Wishmaster" end
if GetLocale() == "ruRU" then sfuncon  = "Незнаю как, но Вы |cff00ff00Включили|r Секретную Функцию. =) Чтобы отключить ее напишите '|cff00ff00/camdist off|r'." else sfuncon = "It's not known how, but you have turn |cff00ff00ON|r a Secret Function. =) To turn off it write '|cff00ff00/camdist off|r'." end
function TITANCAMDIST_Command(msg)
if msg == "off" then
-- отключение
DEFAULT_CHAT_FRAME:AddMessage(sfuncoff)
SetCVar("cameraDistanceMax",25)
MoveViewLeftStop()
MoveViewOutStop()
SetView(4)
else
-- включение
DEFAULT_CHAT_FRAME:AddMessage(sfuncon)
SetView(1)
SetCVar("cameraDistanceMax",50)
MoveViewLeftStart(0.1)
MoveViewOutStart(0.1)
end
end 

--[[ P.S.: Мне с этим аддоном помогли:
 • Заки - самый большой склерозник:  разборки с yard-yards
 • Мизи и ЗлойКот(ака Русский во всех его формах и проявлениях): дефолтные настройки клиента 
]]--