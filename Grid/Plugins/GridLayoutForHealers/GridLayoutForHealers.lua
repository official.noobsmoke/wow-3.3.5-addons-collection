﻿--Grid Healing Layout
local L = AceLibrary("AceLocale-2.2"):new("GridLayoutForHealers")

L:RegisterTranslations("enUS", function()
	return {
		["By Tanks, healers, dps"] = true,
		["By 20 and Tanks"] = true,
		["By 25 and Tanks"] = true,
		["By 40 and Tanks"] = true,
	}
end)

L:RegisterTranslations("koKR", function()
	return {
		["By Tanks, healers, dps"] = "탱커, 힐러, 뎀딜러",
		["By 20 and Tanks"] = "20인 - 탱커들",
		["By 25 and Tanks"] = "25인 - 탱커들",
		["By 40 and Tanks"] = "40인 - 탱커들",
	}
end)

GridLayout:AddLayout(L["By Tanks, healers, dps"], {
	[1] = {
		groupFilter = "WARRIOR",
	},
	[2] = {
		groupFilter = "PRIEST,DRUID,SHAMAN,PALADIN",
	},
	[3] = {
		groupFilter = "MAGE,ROGUE,WARLOCK,HUNTER",
	}
})

GridLayout:AddLayout(L["By 20 and Tanks"], {
	[1] = {
		groupFilter = "WARRIOR",
	},
	[2] = {
		groupFilter = "0",
	},
	[3] = {
		groupFilter = "1",
	},
	[4] = {
		groupFilter = "2",
	},
	[5] = {
		groupFilter = "3",
	},
	[6] = {
		groupFilter = "4",
	}
})

GridLayout:AddLayout(L["By 25 and Tanks"], {
	[1] = {
		groupFilter = "WARRIOR",
	},
	[2] = {
		groupFilter = "0",
	},
	[3] = {
		groupFilter = "1",
	},
	[4] = {
		groupFilter = "2",
	},
	[5] = {
		groupFilter = "3",
	},
	[6] = {
		groupFilter = "4",
	},
	[7] = {
		groupFilter = "5",
	}
})

GridLayout:AddLayout(L["By 40 and Tanks"], {
	[1] = {
		groupFilter = "WARRIOR",
	},
	[2] = {
		groupFilter = "0",
	},
	[3] = {
		groupFilter = "1",
	},
	[4] = {
		groupFilter = "2",
	},
	[5] = {
		groupFilter = "3",
	},
	[6] = {
		groupFilter = "4",
	},
	[7] = {
		groupFilter = "5",
	},
	[8] = {
		groupFilter = "6",
	},
	[9] = {
		groupFilter = "7",
	},
	[10] = {
		groupFilter = "8",
	}
})
