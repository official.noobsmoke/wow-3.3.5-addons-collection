------------------------------------------------------------------------
r55 | kunda | 2010-04-30 01:53:05 +0000 (Fri, 30 Apr 2010) | 1 line
Changed paths:
   A /tags/30300-2 (from /trunk:54)

Tagging as 30300-2
------------------------------------------------------------------------
r54 | kunda | 2010-04-29 02:31:40 +0000 (Thu, 29 Apr 2010) | 4 lines
Changed paths:
   M /trunk/GridStatusRaidIcons-localization-deDE.lua
   M /trunk/GridStatusRaidIcons-localization-enUS.lua
   M /trunk/GridStatusRaidIcons-localization-koKR.lua
   M /trunk/GridStatusRaidIcons.lua
   M /trunk/GridStatusRaidIcons.toc
   D /trunk/icons

- localization update
- removed textures (the new 'Interface\TargetingFrame\UI-RaidTargetingIcon_...' textures are separated and need no TexCoord)

Phanx, please use the official Grid thread at wowace.com if you want to announce changes to Grid that break _every_ third-party Grid module, and not the submit comment here. Thanks.
------------------------------------------------------------------------
r53 | Phanx | 2010-01-08 04:46:48 +0000 (Fri, 08 Jan 2010) | 1 line
Changed paths:
   M /trunk/GridStatusRaidIcons.lua

- Update with proper Grid module references. I don't know why you reverted Greltok's commit. The next Grid release will not expose global references for modules like GridRoster and GridStatus. You *must* use Grid:GetModule("blah").
------------------------------------------------------------------------
