local L = GridSideIndicators_Locales
local AceOO = AceLibrary("AceOO-2.0")
local GridFrame = Grid:GetModule("GridFrame")

local indicators = GridFrame.frameClass.prototype.indicators
table.insert(indicators, { type = "side1", order = 15, name = L["Top Side"] })
table.insert(indicators, { type = "side2", order = 18, name = L["Right Side"] })
table.insert(indicators, { type = "side3", order = 16, name = L["Bottom Side"] })
table.insert(indicators, { type = "side4", order = 17, name = L["Left Side"] })

local statusmap = GridFrame.db.profile.statusmap
if ( not statusmap["side1"] ) then
	statusmap["side1"] = { player_target = true }
	statusmap["side2"] = {}
	statusmap["side3"] = {}
	statusmap["side4"] = {}
end

-- magic hooking sauce
local SideGridFrameClass = AceOO.Class(GridFrame.frameClass)

local _frameClass = nil
if ( not _frameClass ) then
	_frameClass = GridFrame.frameClass
	GridFrame.frameClass = SideGridFrameClass
end

function SideGridFrameClass.prototype:CreateIndicator(indicator)
	SideGridFrameClass.super.prototype.CreateIndicator(self, indicator)
	-- position indicator wherever needed
	if indicator == "side1" then
		self.frame[indicator]:SetPoint("TOP", self.frame, "TOP", 0, -1)
	elseif indicator == "side2" then
		self.frame[indicator]:SetPoint("RIGHT", self.frame, "RIGHT", -1, 0)
	elseif indicator == "side3" then
		self.frame[indicator]:SetPoint("BOTTOM", self.frame, "BOTTOM", 0, 1)
	elseif indicator == "side4" then
		self.frame[indicator]:SetPoint("LEFT", self.frame, "LEFT", 1, 0)
	end
end

function SideGridFrameClass.prototype:SetIndicator(indicator, color, text, value, maxValue, texture, start, duration, stack)
	SideGridFrameClass.super.prototype.SetIndicator(self,indicator, color, text, value, maxValue, texture, start, duration, stack)
	if indicator == "side1"
	or indicator == "side2"
	or indicator == "side3"
	or indicator == "side4"
	then
		-- create indicator on demand if not available yet
		if not self.frame[indicator] then
			self:CreateIndicator(indicator)
		end
		if not color then color = { r = 1, g = 1, b = 1, a = 1 } end
		self.frame[indicator]:SetBackdropColor(color.r, color.g, color.b, color.a)
		self.frame[indicator]:Show()
	end
end

function SideGridFrameClass.prototype:ClearIndicator(indicator)
	SideGridFrameClass.super.prototype.ClearIndicator(self,indicator)
	if indicator == "side1"
	or indicator == "side2"
	or indicator == "side3"
	or indicator == "side4"
	then
		if self.frame[indicator] then
			self.frame[indicator]:SetBackdropColor(1, 1, 1, 1)
			self.frame[indicator]:Hide()
		end
	end
end

function SideGridFrameClass.prototype:SetCornerSize(size)
	SideGridFrameClass.super.prototype.SetCornerSize(self, size)
	for x = 1, 4 do
		local indicator = "side"..x
		if self.frame[indicator] then
			self.frame[indicator]:SetHeight(size)
			self.frame[indicator]:SetWidth(size)
		end
	end
end