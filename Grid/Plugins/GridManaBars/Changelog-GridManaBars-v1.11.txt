------------------------------------------------------------------------
r97 | julith | 2010-04-24 04:29:35 +0000 (Sat, 24 Apr 2010) | 1 line
Changed paths:
   A /tags/v1.11 (from /trunk:96)

Tagging as v1.11
------------------------------------------------------------------------
r96 | julith | 2010-04-23 13:40:26 +0000 (Fri, 23 Apr 2010) | 1 line
Changed paths:
   M /trunk/GridManaBars.toc

GridManaBars: updated version
------------------------------------------------------------------------
r95 | Phanx | 2010-04-23 06:56:24 +0000 (Fri, 23 Apr 2010) | 1 line
Changed paths:
   M /trunk/GridStatusMana.lua

- Updated for removal of unnecessary globals in Grid
------------------------------------------------------------------------
r93 | julith | 2010-04-05 16:17:38 +0000 (Mon, 05 Apr 2010) | 2 lines
Changed paths:
   M /trunk/GridManaBar.lua
   M /trunk/GridManaBars.toc

GridManaBars:
- fixed wrong size of Health-/Healing-bar
------------------------------------------------------------------------
r92 | onyxmaster | 2009-12-28 19:42:46 +0000 (Mon, 28 Dec 2009) | 1 line
Changed paths:
   M /trunk/GridManaBar.lua

GridManaBars:fix for previous fix =)
------------------------------------------------------------------------
