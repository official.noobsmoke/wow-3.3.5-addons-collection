﻿------------------------------------------------------------------------
--	Localization for GridStatusRes
------------------------------------------------------------------------

local L = AceLibrary("AceLocale-2.2"):new("GridStatusRes")

L:RegisterTranslations("enUS", function() return {
	["Resurrection"] = true,
	["Incoming resurrection"] = true,
	["Soulstone"] = true,
	["Resurrected"] = true,
} end)

L:RegisterTranslations("deDE", function() return {
	["Resurrection"] = "Wiederbelebung",
	["Incoming resurrection"] = "Eingehende Wiederbelebung",
	["Soulstone"] = "Seelenstein",
	["Resurrected"] = "Wiederbelebt",
} end)

L:RegisterTranslations("frFR", function() return {
	["Resurrection"] = "Résurrection",
	["Incoming resurrection"] = "Résurrection entrante",
	["Soulstone"] = "Pierre d'âme",
	["Resurrected"] = "Ressuscité",
} end)

L:RegisterTranslations("koKR", function() return {
	["Resurrection"] = "부활",
	["Incoming resurrection"] = "부활 받음",
	["Soulstone"] = "영혼석",
	["Resurrected"] = "부활",
} end)

L:RegisterTranslations("ruRU", function() return {
	["Resurrection"] = "Воскрешение",
	["Incoming resurrection"] = "Входящее Воскрешение",
	["Soulstone"] = "Камень души",
	["Resurrected"] = "Воскрешен",
} end)

L:RegisterTranslations("zhCN", function() return {
	["Resurrection"] = "复活",
	["Incoming resurrection"] = "接受复活",
	["Soulstone"] = "灵魂石",
	["Resurrected"] = "已复活",
} end)

L:RegisterTranslations("zhTW", function() return {
	["Resurrection"] = "復活",
	["Incoming resurrection"] = "接受復活",
	["Soulstone"] = "靈魂石",
	["Resurrected"] = "已復活",
} end)
