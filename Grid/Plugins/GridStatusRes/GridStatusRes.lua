﻿
local ResComm = LibStub("LibResComm-1.0", true)
if not ResComm then return end

local L = AceLibrary("AceLocale-2.2"):new("GridStatusRes")
local GridRoster = Grid:GetModule("GridRoster")

local GridStatusRes = Grid:GetModule("GridStatus"):NewModule("GridStatusRes")
GridStatusRes.menuName = L["Resurrection"]

GridStatusRes.defaultDB = {
	debug = false,
	alert_res = {
		enable = true,
		color = { r = 1, g = 1, b = 0, a = 1 },
		priority = 50,
		range = false,
		text = "+RES",
	},
	alert_ressed = {
		enable = true,
		color = { r = 0, g = 1, b = 0, a = 1 },
		priority = 51,
		range = false,
		text = "RES",
	},
	alert_ss = {
		enable = true,
		color = { r = 1, g = 0, b = 1, a = 1 },
		priority = 49,
		range = false,
		text = "SS",
	},
}

local function GetOptionsForStatus(status_name)
	local L = AceLibrary("AceLocale-2.2"):new("Grid") -- use Grid's translations instead of duplicating them
	local options = {
		["text"] = {
			name = L["Text"],
			desc = L["Text to display on text indicators"],
			type = "text",
			usage = "<text>", -- if someone wants to translate this, go for it
			get = function()
				return GridStatusRes.db.profile[status_name].text
			end,
			set = function(v)
				GridStatusRes.db.profile[status_name].text = v
			end,
		},
	}
	return options
end

function GridStatusRes:OnInitialize()
	self.super.OnInitialize(self)
	self:RegisterStatus("alert_res", L["Incoming resurrection"], GetOptionsForStatus("alert_res"), true)
	self:RegisterStatus("alert_ressed", L["Resurrected"], GetOptionsForStatus("alert_ressed"), true)
	self:RegisterStatus("alert_ss", L["Soulstone"], GetOptionsForStatus("alert_ss"), true)
end

function GridStatusRes:OnEnable()
	self.debugging = self.db.profile.debug
	self:Debug("OnEnable")

	ResComm.RegisterCallback(self, "ResComm_ResStart")
	ResComm.RegisterCallback(self, "ResComm_ResEnd")
	ResComm.RegisterCallback(self, "ResComm_Ressed")
	ResComm.RegisterCallback(self, "ResComm_ResExpired")
	ResComm.RegisterCallback(self, "ResComm_CanRes")
	self:RegisterBucketEvent("UNIT_HEALTH", 0.1, "HealthChanged")
end

function GridStatusRes:OnDisable()
	ResComm.UnregisterCallback(self, "ResComm_ResStart")
	ResComm.UnregisterCallback(self, "ResComm_ResEnd")
	ResComm.UnregisterCallback(self, "ResComm_Ressed")
	ResComm.UnregisterCallback(self, "ResComm_ResExpired")
	ResComm.UnregisterCallback(self, "ResComm_CanRes")
	self:UnregisterBucketEvent("UNIT_HEALTH")
end

function GridStatusRes:ResComm_ResStart(event, _, _, name)
	local settings = self.db.profile.alert_res
	if not settings.enable then return end

	local guid = GridRoster:GetGUIDByFullName(name)
	self:Debug("ResComm_ResStart targetName", name, "guid", guid)
	self.core:SendStatusGained(guid, "alert_res",
		settings.priority,
		(settings.range and 40),
		settings.color,
		settings.text)
end

function GridStatusRes:ResComm_ResEnd(event, _, name)
	if name and not ResComm:IsUnitBeingRessed(name) then
		local guid = GridRoster:GetGUIDByFullName(name)
		self:Debug("ResComm_ResEnd targetName", name, "guid", guid)
		self.core:SendStatusLost(guid, "alert_res")
	end
end

function GridStatusRes:ResComm_Ressed(event, name)
	local settings = self.db.profile.alert_ressed
	if not settings.enable then return end

	local guid = GridRoster:GetGUIDByFullName(name)
	self:Debug("ResComm_ResStart ressed", name, "guid", guid)
	self.core:SendStatusGained(guid, "alert_ressed",
		settings.priority,
		(settings.range and 40),
		settings.color,
		settings.text)
end

function GridStatusRes:ResComm_ResExpired(event, name)
	self.core:SendStatusLost(name, "alert_ressed")
end

function GridStatusRes:ResComm_CanRes(event, name)
	local settings = self.db.profile.alert_ss
	if not settings.enable then return end

	local guid = GridRoster:GetGUIDByFullName(name)
	self:Debug("ResComm_ResStart ressed", name, "guid", guid)
	self.core:SendStatusGained(guid, "alert_ss",
		settings.priority,
		(settings.range and 40),
		settings.color,
		settings.text)
end

function GridStatusRes:HealthChanged(units)
	for unitid in pairs(units) do
		if not UnitIsDead(unitid) then
			local guid = UnitGUID(unitid)
			self.core:SendStatusLost(guid, "alert_res")
			self.core:SendStatusLost(guid, "alert_ressed")
			self.core:SendStatusLost(guid, "alert_ss")
		end
	end
end
