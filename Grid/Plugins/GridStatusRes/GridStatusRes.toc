﻿## Interface: 30300
## Version: v3.00.03.03 release
## X-Curse-Packaged-Version: 55 release
## X-Curse-Project-Name: GridStatusRes
## X-Curse-Project-ID: grid-status-res
## X-Curse-Repository-ID: wow/grid-status-res/mainline

## Title: GridStatusRes
## Notes: Adds a resurrection and soulstone status to Grid. Uses LibResComm-1.0
## Notes-ruRU: Добовляет статус Воскрешения и Камня душ в Grid. Использует LibResComm-1.0
## Notes-deDE: Wiederbelebungs- und Seelenstein-Status für Grid.
## Notes-zhCN: [Grid] 在 Grid 添加复活与灵魂石提示。需要 LibResComm-1.0 库
## Notes-zhTW: [Grid] 在 Grid 添加復活與靈魂石提示。需要 LibResComm-1.0 庫
## Author: DathRarhek
## X-Category: UnitFrame
## X-GridStatusModule: GridStatusRes
## X-Website: http://wow.curse.com/downloads/wow-addons/details/grid-status-res.aspx

## Dependencies: Grid
## OptionalDeps: LibResComm-1.0

#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
Libs\LibResComm-1.0\LibResComm-1.0.lua
#@end-no-lib-strip@

Localization.lua
GridStatusRes.lua