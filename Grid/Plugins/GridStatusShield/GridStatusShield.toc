## Interface: 30300
## Title: GridStatusShield |cff7fff7f -Ace2-|r
## Notes: Status indicator that shows the remaining of value of Powerword shield.
## Author: Julith
## Version: 1.01
## X-Category: UnitFrame
## Dependencies: Grid
## OptionalDeps: Ace2
## X-Curse-Packaged-Version: v1.01
## X-Curse-Project-Name: GridStatusShield
## X-Curse-Project-ID: gsshield
## X-Curse-Repository-ID: wow/gsshield/mainline

LibShieldLeft.lua

GridStatusShield.lua
