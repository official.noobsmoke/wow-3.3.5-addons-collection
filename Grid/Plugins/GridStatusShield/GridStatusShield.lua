--[[
	Name: GridStatusShield
	Revision: $Revision: 82146 $
	Developed by: Julith @ Perenolde-EU (billgatesgut@web.de)
	Description: Publishes the sremaining value of the players power word shield on a unit.

	Credits to oodyboo from WoWAce for the idea of this addon.
]]

--{{{ Libraries
local ShieldLeft = LibStub:GetLibrary("LibShieldLeft-1.0", true)
if not ShieldLeft then return end
local L = AceLibrary("AceLocale-2.2"):new("GridStatusShield")

--}}}

L:RegisterTranslations("enUS", function()
	return {
		["Shield left"] = true,
        ["Aegis detection tolerance"] = true,
        ["Increase this parameter if your Divine Aegis isn't not correctly detected. But can also lead to miss detection the higher the value is."] = true,
        ["Low shield color"] = true,
        ["Medium shield color"] = true,
        ["Low shield threshold"] = true,
        ["Medium shield threshold"] = true,
        ["Color for shield amount below the low shield threshold."] = true,
        ["Color for shield amount below the medium shield threshold."] = true,
        ["The threshold below which a shield is considered low."] = true,
        ["The threshold below which a shield is considered medium."] = true,
	}
end)

L:RegisterTranslations("deDE", function()
	return {
		["Shield left"] = "Verbleibender Schild",
        ["Aegis detection tolerance"] = "Aegis Erkennungs-Toleranz",
        ["Increase this parameter if your Divine Aegis isn't not correctly detected. But can also lead to miss detection the higher the value is."] =
            "Erh\195\182he diesen Wert falls deine G\195\182ttliche Aegis nicht korrekt erkannt wird. Je gr\195\182\195\159er der Wert desto gr\195\182\195\159er allerdings auch die Chance auf Fehlerkennungen.",
        ["Low shield color"] = "Niedriges schild Farbe",
        ["Medium shield color"] = "Mittleres Schild Farbe",
        ["Low shield threshold"] = "Niedriges Schild Schranke",
        ["Medium shield threshold"] = "Mittleres Schild Schranke",
        ["Color for shield amount below the low shield threshold."] = "Farbe fuer Schilde deren verbleibende Staerke unter der niedriges Schild Schranke liegen.",
        ["Color for shield amount below the medium shield threshold."] = "Farbe fuer Schilde deren verbleibende Staerke unter der mittleres Schild Schranke liegen.",
        ["The threshold below which a shield is considered low."] = "Die Schranke unter welcher Schilde als neidrig eingestuft werden.",
        ["The threshold below which a shield is considered medium."] = "Die Schranke unter welcher Schilde als mittel eingestuft werden.",
	}
end)


local GridStatusShield = Grid:GetModule("GridStatus"):NewModule("GridStatusShield")
GridStatusShield.menuName = L["Shield left"]


--{{{ AceDB defaults

GridStatusShield.defaultDB = {
	unitShieldLeft = {
		enable = true,
		priority = 90,
		color = { r = 1, g = 0, b = 0, a = 1 },
        aegisTolerance = 0.3,
        mark1Color = { r = 1, g = 0, b = 0, a = 1 },
        mark1Threshold = 500,
        mark2Color = { r = 1, g = 1, b = 0, a = 1 },
        mark2Threshold = 1000,
	},
}

--}}}

local settings

--{{{ Options
local amountOptions = {
    ["Aegis Tolerance"] = {
		type = "range",
		name = L["Aegis detection tolerance"],
		desc = L["Increase this parameter if your Divine Aegis isn't not correctly detected. But can also lead to miss detection the higher the value is."],
        max = 2.0,
        min = 0.0,
        step = 0.1,
		get = function()
			return settings.unitShieldLeft.aegisTolerance
		end,
		set = function(v)
			settings.unitShieldLeft.aegisTolerance = v
            ShieldLeft:SetAegisTolerance(v)
		end,
	},
    ["mark1color"] = {
        type = "color",
        name = L["Low shield color"],
        desc = L["Color for shield amount below the low shield threshold."],
        order = 90,
        hasAlpha = true,
        get = function ()
                  local color = settings.unitShieldLeft.mark1Color
                  return color.r, color.g, color.b, color.a
              end,
        set = function (r, g, b, a)
                  local color = settings.unitShieldLeft.mark1Color
                  color.r = r
                  color.g = g
                  color.b = b
                  color.a = a or 1
              end,
    },
    ["mark2color"] = {
        type = "color",
        name = L["Medium shield color"],
        desc = L["Color for shield amount below the medium shield threshold."],
        order = 90,
        hasAlpha = true,
        get = function ()
                  local color = settings.unitShieldLeft.mark2Color
                  return color.r, color.g, color.b, color.a
              end,
        set = function (r, g, b, a)
                  local color = settings.unitShieldLeft.mark2Color
                  color.r = r
                  color.g = g
                  color.b = b
                  color.a = a or 1
              end,
    },
    ["mark1threshold"] = {
		type = "range",
		name = L["Low shield threshold"],
		desc = L["The threshold below which a shield is considered low."],
        max = 3000,
        min = 50,
        step = 1,
		get = function()
			return settings.unitShieldLeft.mark1Threshold
		end,
		set = function(v)
			settings.unitShieldLeft.mark1Threshold = v
		end,
	},
    ["mark2threshold"] = {
		type = "range",
		name = L["Medium shield threshold"],
		desc = L["The threshold below which a shield is considered medium."],
        max = 6000,
        min = 100,
        step = 1,
		get = function()
			return settings.unitShieldLeft.mark2Threshold
		end,
		set = function(v)
			settings.unitShieldLeft.mark2Threshold = v
		end,
	},
}
--}}}

function GridStatusShield:OnInitialize()
	self.super.OnInitialize(self)
	self:RegisterStatus("unitShieldLeft", L["Shield left"], amountOptions, true)
    settings = GridStatusShield.db.profile
    ShieldLeft:SetAegisTolerance(settings.unitShieldLeft.aegisTolerance)
end

function GridStatusShield:OnEnable()
    self:RegisterEvent("Grid_UnitJoined")

	-- register callbacks
	ShieldLeft.RegisterCallback(self, "ShieldLeft_NewShield")
	ShieldLeft.RegisterCallback(self, "ShieldLeft_RefreshShield")
	ShieldLeft.RegisterCallback(self, "ShieldLeft_RemoveShield")
    ShieldLeft.RegisterCallback(self, "ShieldLeft_UpdateShield")
end

function GridStatusShield:OnDisable()
	ShieldLeft.UnregisterCallback(self, "ShieldLeft_NewShield")
	ShieldLeft.UnregisterCallback(self, "ShieldLeft_RefreshShield")
	ShieldLeft.UnregisterCallback(self, "ShieldLeft_RemoveShield")
    ShieldLeft.UnregisterCallback(self, "ShieldLeft_UpdateShield")
end

function GridStatusShield:Print(msg)
    --print("GridStatusShield: "..msg)
end

function GridStatusShield:Grid_UnitJoined(unitGUID, unitid)
    self:Clear(unitGUID)
end


function GridStatusShield:Clear(unitGUID)
    self.core:SendStatusLost(unitGUID, "unitShieldLeft")
end

function GridStatusShield:FormatShieldText(amount)
	local shieldText
	if amount > 999 then
		shieldText = string.format("%.1fk", amount/1000.0)
	else
		shieldText = string.format("%3.0f", amount)
	end
	return shieldText
end

function GridStatusShield:SendShieldStatus(unitGUID, amount)
    local color = settings.unitShieldLeft.color
    if amount < settings.unitShieldLeft.mark1Threshold then
        color = settings.unitShieldLeft.mark1Color
    elseif amount < settings.unitShieldLeft.mark2Threshold then
        color = settings.unitShieldLeft.mark2Color
    end

    self.core:SendStatusGained(unitGUID, "unitShieldLeft",
                settings.unitShieldLeft.priority,
                nil,
                color,
                self:FormatShieldText(amount))
end

function GridStatusShield:ShieldLeft_UpdateShield(event, unitGUID, unitName, shieldName, shieldValue, allValue)
    self:Print("Updating "..shieldName.." on "..unitName)

    self:SendShieldStatus(unitGUID, allValue)
end

function GridStatusShield:ShieldLeft_RefreshShield(event, unitGUID, unitName, shieldName, shieldValue, allValue)
    self:Print("Refreshing "..shieldName.." on "..unitName)

    self:SendShieldStatus(unitGUID, allValue)
end

function GridStatusShield:ShieldLeft_NewShield(event, unitGUID, unitName, shieldName, shieldValue, allValue)
    self:Print("New "..shieldName.." with "..allValue.." on "..unitName)

    self:SendShieldStatus(unitGUID, allValue)
end

function GridStatusShield:ShieldLeft_RemoveShield(event, unitGUID, unitName, shieldName, shieldValue, allValue, count)
    self:Print("Remove "..shieldName.." with "..allValue.." on "..unitName)
    if count > 0 then
        self:SendShieldStatus(unitGUID, allValue)
    else
        self.core:SendStatusLost(unitGUID, "unitShieldLeft")
    end
end

