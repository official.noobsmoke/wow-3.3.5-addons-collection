## Interface: 20400
## Title: Grid_QuickHealth
## Notes: Makes grid QuickHealth aware
## Notes-deDE: Schnellere Aktualisierung der Gesundheitsanzeigen mit QuickHealth
## Author: Cncfanatics, Tifi
## X-Embeds: LibQuickHealth-2.0
## Dependencies: Grid
## OptionalDeps: LibQuickHealth-2.0
## X-Donate: PayPal: diego.duclos@gmail.com
## X-Email: diego.duclos@gmail.com

libs\LibQuickHealth-2.0\LibQuickHealth-2.0.xml
Grid_QuickHealth.lua
