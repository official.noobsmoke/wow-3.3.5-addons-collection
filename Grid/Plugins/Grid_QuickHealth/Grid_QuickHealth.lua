local QuickHealth = LibStub("LibQuickHealth-2.0")

-- env changement code taken from interruptus
local proxyEnv = setmetatable(
	{
		UnitHealth = QuickHealth.UnitHealth,
	}, 
	{
		__index    = _G,
		__newindex = function (t, k, v) _G[k] = v end,
	}
)

local GetNumRaidMembers = GetNumRaidMembers
local function UnitHealthUpdated(event, unitID, newHealth)
	if GetNumRaidMembers()>0 and unitID:sub(1,4) ~= "raid" then return end
	GridStatusHealth:UpdateUnit(unitID)
	GridStatusHeals:UpdateHealsForUnit(unitID)
end

local eventFrame = CreateFrame("Frame")
local PLAYER_LOGIN 
function PLAYER_LOGIN()
	QuickHealth.RegisterCallback("Grid_QuickHealth", "UnitHealthUpdated", UnitHealthUpdated)

	setfenv(GridStatusHealth.UpdateUnit, proxyEnv)
	GridStatusHealth:UnregisterEvent("UNIT_HEALTH")
	GridStatusHealth:UnregisterEvent("UNIT_MAXHEALTH")

	setfenv(GridStatusHeals.UpdateIncomingHeals, proxyEnv)
	GridStatusHeals:UnregisterEvent("UNIT_HEALTH")

	eventFrame:SetScript("OnEvent", nil)
	eventFrame:UnregisterAllEvents()
	PLAYER_LOGIN = nil
end

eventFrame:SetScript("OnEvent", PLAYER_LOGIN)
eventFrame:RegisterEvent("PLAYER_LOGIN")
