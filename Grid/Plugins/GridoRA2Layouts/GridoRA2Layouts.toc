## Interface: 20300
## Title: GridoRA2Layouts
## Notes: Adds oRA2 MT and pets Layouts to Grid
## Author: Laric (Based on SquishemHards layouts)
## Version: 2.0.1
## Dependencies: Grid, oRA2
GridoRA2Layouts.lua
