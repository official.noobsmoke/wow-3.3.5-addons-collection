-- This is a bugfix and extension of SquishemHards oRA2 layouts.
--  As far as I can see it is working perfectly including with pets.

--Grid oRA2 Main Tank Layouts
local L = AceLibrary("AceLocale-2.2"):new("GridoRA2Layouts")
GridoRA2Layouts = AceLibrary("AceAddon-2.0"):new("AceEvent-2.0", "AceDebug-2.0")

--Mmmm Engrish
L:RegisterTranslations("enUS", function()
return {
	["By 10 and oRA2 Tanks"] = true,
	["By 20 and oRA2 Tanks"] = true,
	["By 25 and oRA2 Tanks"] = true,
	["By 40 and oRA2 Tanks"] = true,
	["oRA2 Tanks"] = true,
  ["Compact 25 and oRA2"]=true,
  ["Compact 40 and oRA2"]=true,
	["By 10 and oRA2 Tanks+pets"] = true,
	["By 25 and oRA2 Tanks+pets"] = true,
}
end)

--Setup initial layouts
GridoRA2Layouts.Layout1 = {
	[1] = { nameList = "",
        	sortMethod = "INDEX",},
	[2] = { groupFilter = "0",},
	[3] = {	groupFilter = "1",},
	[4] = {	groupFilter = "2",},
}

GridoRA2Layouts.Layout1pets = {
	[1] = { nameList = "",
        	sortMethod = "INDEX",},
	[2] = {	groupFilter = "1",},
	[3] = {	groupFilter = "2",},
	[4] = {	
			isPetGroup = true,
			groupFilter = "WARLOCK,HUNTER",
			unitsPerColumn = 5,
			maxColumns = 5,
			filterOnPet = true,},
}

GridoRA2Layouts.Layout2 = {
	[1] = {	nameList = "",
        	sortMethod = "INDEX",},
	[2] = {	groupFilter = "0",},
	[3] = {	groupFilter = "1",},
	[4] = {	groupFilter = "2",},
	[5] = {	groupFilter = "3",},
	[6] = {	groupFilter = "4",},
}

GridoRA2Layouts.Layout3 = {
	[1] = {	nameList = "",
        	sortMethod = "INDEX",},
	[2] = {	groupFilter = "0",},
	[3] = {	groupFilter = "1",},
	[4] = {	groupFilter = "2",},
	[5] = {	groupFilter = "3",},
	[6] = {	groupFilter = "4",},
	[7] = {	groupFilter = "5",},
}
GridoRA2Layouts.Layout3pets = {
	[1] = {	nameList = "",
        	sortMethod = "INDEX",},
	[2] = {	groupFilter = "0",},
	[3] = {	groupFilter = "1",},
	[4] = {	groupFilter = "2",},
	[5] = {	groupFilter = "3",},
	[6] = {	groupFilter = "4",},
	[7] = {	groupFilter = "5",},
	[8] = {	
			isPetGroup = true,
			groupFilter = "WARLOCK,HUNTER",
			unitsPerColumn = 5,
			maxColumns = 5,
			filterOnPet = true,},
}

GridoRA2Layouts.Layout4 = {
	[1] = {	nameList = "",
        	sortMethod = "INDEX",},
	[2] = {	groupFilter = "0",},
	[3] = {	groupFilter = "1",},
	[4] = {	groupFilter = "2",},
	[5] = {	groupFilter = "3",},
	[6] = {	groupFilter = "4",},
	[7] = {	groupFilter = "5",},
	[8] = {	groupFilter = "6",},
	[9] = {	groupFilter = "7",},
	[10] = {groupFilter = "8",},
}

GridoRA2Layouts.Layout5 = {
	[1] = {	nameList = "",
        	sortMethod = "INDEX",},
}

GridoRA2Layouts.Layout6 = {
	[1] = {	nameList = "",
        	sortMethod = "INDEX",},
	[2] = {	groupFilter = "1",},
	[3] = {	groupFilter = "2",},
	[4] = {	groupFilter = "3",},
	[5] = {	groupFilter = "4",},
	[6] = {	groupFilter = "5",},
}

GridoRA2Layouts.Layout7 = {
	[1] = {	nameList = "",
        	sortMethod = "INDEX",},
	[2] = {	groupFilter = "1",},
	[3] = {	groupFilter = "2",},
	[4] = {	groupFilter = "3",},
	[5] = {	groupFilter = "4",},
	[6] = {	groupFilter = "5",},
	[7] = {	groupFilter = "6",},
	[8] = {	groupFilter = "7",},
	[9] = {groupFilter = "8",},
}

--Add initial layouts to GridLayout
GridLayout:AddLayout(L["By 10 and oRA2 Tanks"], GridoRA2Layouts.Layout1)
GridLayout:AddLayout(L["By 10 and oRA2 Tanks+pets"], GridoRA2Layouts.Layout1pets)
GridLayout:AddLayout(L["By 20 and oRA2 Tanks"], GridoRA2Layouts.Layout2)
GridLayout:AddLayout(L["By 25 and oRA2 Tanks"], GridoRA2Layouts.Layout3)
GridLayout:AddLayout(L["By 25 and oRA2 Tanks+pets"], GridoRA2Layouts.Layout3pets)
GridLayout:AddLayout(L["By 40 and oRA2 Tanks"], GridoRA2Layouts.Layout4)
GridLayout:AddLayout(L["oRA2 Tanks"], GridoRA2Layouts.Layout5)
GridLayout:AddLayout(L["Compact 25 and oRA2"], GridoRA2Layouts.Layout6)
GridLayout:AddLayout(L["Compact 40 and oRA2"], GridoRA2Layouts.Layout7)

--Addon starting point
function GridoRA2Layouts:OnInitialize()
    self:SetDebugging(false)
    
    --Register oRA2 events to catch
    self:Debug("Registering Events")
    self:RegisterEvent("oRA_MainTankUpdate")
    self:RegisterEvent("oRA_JoinedRaid", "oRA_MainTankUpdate")
    self:RegisterEvent("RosterLib_RosterChanged", function() self:oRA_MainTankUpdate() end)
    if not InCombatLockdown() then self:oRA_MainTankUpdate() end
    self:ScheduleLeaveCombatAction(GridoRA2Layouts,"oRA_MainTankUpdate")
end

--oRA2 Triggered Update
function GridoRA2Layouts:oRA_MainTankUpdate()
    self:Debug("oRA_MainTankUpdate Called")
    if not oRA.maintanktable then return end
    
    self:MainTankUpdate()
end

--Main Tank list update function
local mts = {}
function GridoRA2Layouts:MainTankUpdate()
    self:Debug("MainTankUpdate Called")
    
    local showmt
    local tanklist = ""
   -- clean up old tank list
    for k,v in pairs(mts) do
	    mts[k] = nil
    end

    --Copy non-empty elements to new table
    self:Debug("Iterating maintanktable")
    for i = 1, 10 do
    	if oRA.maintanktable[i] then
    		showmt = true
    		table.insert(mts, oRA.maintanktable[i])
    	end
    end
    
    --If the table was not empty, update
    if showmt then
        tanklist = table.concat(mts, ",")
        self:Debug("Set Tank List: " .. tanklist)
    end

    --Set layout nameList to new list
    GridoRA2Layouts.Layout1[1].nameList = tanklist    
    GridoRA2Layouts.Layout2[1].nameList = tanklist
    GridoRA2Layouts.Layout3[1].nameList = tanklist    
    GridoRA2Layouts.Layout4[1].nameList = tanklist        
    GridoRA2Layouts.Layout5[1].nameList = tanklist
    GridoRA2Layouts.Layout6[1].nameList = tanklist
    GridoRA2Layouts.Layout7[1].nameList = tanklist
    GridoRA2Layouts.Layout1pets[1].nameList = tanklist
    GridoRA2Layouts.Layout3pets[1].nameList = tanklist

    --If event exists, schedule ReloadLayout action
    if GridLayout:IsEventRegistered("Grid_ReloadLayout") then
    	self:Debug("ReloadLayout is Registered. Triggering.")
    	self:ScheduleLeaveCombatAction(GridLayout, "ReloadLayout")
    end
end