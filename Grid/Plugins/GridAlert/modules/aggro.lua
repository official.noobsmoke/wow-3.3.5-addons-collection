--[[
GridAlert : Aggro

Provides an audible notification when the player gains aggro on a mob.

Should probably be inversed for maintanks (or not ?)
]]

local GridAlertAggro = GridAlert:NewModule("GridAlertAggro")
GridAlertAggro.defaultState = true
GridAlertAggro.defaultDB = {
	message = nil,
	sound = "Aggro",
	sound_name = nil,
	min_pause = 0.5,
}

function GridAlertAggro:OnEnable()
	local name = self.name
	local core = self.core
	local p = self.db.profile
	core.db.profile.triggers.alert_aggro.gain.player = name
	core:SetAlertOptions(name, p.sound, p.message, p.min_pause)
end

function GridAlertAggro:OnDisable()
	local p = self.core.db.profile.triggers.alert_aggro.gain
	if p.player == self.name then
		p.player = nil
	end
	self.super.OnDisable(self)
end

function GridAlertAggro:OnInitialize()
	self.super.OnInitialize(self)
end
