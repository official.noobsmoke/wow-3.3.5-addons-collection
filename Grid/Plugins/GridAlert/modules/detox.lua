--[[
GridAlert : Detox

Provides an audible notification when any one in the raid gains a debuff that
the player can dispell.

Note that "what the player can dispell" really means what the player's class is
expected to be able to dispell at level 60.
]]

local GridAlertDetox = GridAlert:NewModule("GridAlertDetox")
GridAlertDetox.defaultState = true
GridAlertDetox.defaultDB = {
	message = nil,
	sound = "Detox",
	min_pause = 0.5,
}

function GridAlertDetox:OnEnable()
	local core = self.core
	local name = self.name
	local c = select(2, UnitClass("player"))
	local t = core.db.profile.triggers
	if c == "MAGE" then
		t.debuff_curse.gain.any = name
		t.debuff_disease.gain.any = nil
		t.debuff_magic.gain.any = nil
		t.debuff_poison.gain.any = nil
	elseif c == "DRUID" then
		t.debuff_curse.gain.any = name
		t.debuff_disease.gain.any = nil
		t.debuff_magic.gain.any = nil
		t.debuff_poison.gain.any = name
	elseif c == "PALADIN" then
		t.debuff_curse.gain.any = nil
		t.debuff_disease.gain.any = name
		t.debuff_magic.gain.any = name
		t.debuff_poison.gain.any = name
	elseif c == "SHAMAN" then
		t.debuff_curse.gain.any = nil
		t.debuff_disease.gain.any = name
		t.debuff_magic.gain.any = nil
		t.debuff_poison.gain.any = name
	elseif c == "PRIEST" then
		t.debuff_curse.gain.any = nil
		t.debuff_disease.gain.any = name
		t.debuff_magic.gain.any = name
		t.debuff_poison.gain.any = nil
	else
		return
	end
	local p = self.db.profile
	core:SetAlertOptions(name, p.sound, p.message, p.min_pause)
end

local clear = function (p, name)
    if p.any == name then p.any = nil end
end

function GridAlertDetox:OnDisable()
	local t = self.core.db.profile.triggers
    clear(t.debuff_curse.gain, self.name)
    clear(t.debuff_disease.gain, self.name)
    clear(t.debuff_magic.gain, self.name)
    clear(t.debuff_poison.gain, self.name)
	self.super.OnDisable(self)
end

function GridAlertDetox:OnInitialize()
	self.super.OnInitialize(self)
end
