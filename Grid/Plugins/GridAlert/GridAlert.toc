## Interface: 20300
## Title: GridAlert
## Notes: Alerts for Grid
## Author: Jerry
## Version: $Rev: 72732 $
## DefaultState: enabled
## LoadOnDemand: 0
## Dependencies: Grid
## OptionalDeps: LibSink-2.0, LibSharedMedia-3.0
## X-Embeds: LibSink-2.0, LibSharedMedia-3.0
## X-GridModule: GridAlert
## X-Category: UnitFrame
## X-AceForum: 5749

embeds.xml

locales.xml

GridAlert.lua

modules.xml
