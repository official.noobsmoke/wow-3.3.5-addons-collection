﻿local UA = {};
function UA.print(msg,err)
	DEFAULT_CHAT_FRAME:AddMessage("\124cFFFFAD00[UnderAchiever]"..(err and " [Error]" or "")..": "..msg);
end

local UnderAchieverExceptions = CreateFrame("Frame","UnderAchieverExceptions",UnderAchieverFrame);
UnderAchieverExceptions:Hide();
UnderAchieverExceptions:SetAllPoints(UnderAchieverFrame);



-- While off, fake
local UnderAchieverExceptionsDisabled = CreateFrame("Frame","UnderAchieverExceptionsDisabled",UnderAchieverExceptions);
UnderAchieverExceptionsDisabled:SetPoint("TOPLEFT", UnderAchieverExceptions, 15, -30);
UnderAchieverExceptionsDisabled:SetPoint("RIGHT", UnderAchieverExceptions, "RIGHT", -15, 0);
UnderAchieverExceptionsDisabled:SetHeight(145);
UnderAchieverExceptionsDisabled:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});
local UnderAchieverExceptionsDisabledLabel = UnderAchieverExceptionsDisabled:CreateFontString("UnderAchieverExceptionsDisabledLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverExceptionsDisabledLabel:SetPoint("TOPLEFT", UnderAchieverExceptionsDisabled, 10, -10);
UnderAchieverExceptionsDisabledLabel:SetText("Whilst auto-faking is disabled, keep faking these achievements:");

UA.disabled = {};
for i=1,5 do
	UA.disabled[i] = CreateFrame("Frame","UnderAchieverExceptionsDisabledContainer"..i,UnderAchieverExceptionsDisabled);
	UA.disabled[i]:SetWidth(365);
	UA.disabled[i]:SetHeight(24);
	if i==1 then
		UA.disabled[i]:SetPoint("TOPLEFT",UnderAchieverExceptionsDisabledLabel,"BOTTOMLEFT",10,-5);
	else
		UA.disabled[i]:SetPoint("TOPLEFT",UA.disabled[i-1],"BOTTOMLEFT",0,0);
	end
	UA.disabled[i].achi = UA.disabled[i]:CreateFontString("UnderAchieverExceptionsDisabledContainer"..i.."Achi", "ARTWORK", "GameFontNormal");
	UA.disabled[i].achi:SetPoint("TOPLEFT",UA.disabled[i],"TOPLEFT",0,0);
	UA.disabled[i].achi:SetWidth(50);
	UA.disabled[i].achi:SetText(" ");
	UA.disabled[i].achi:SetJustifyH("CENTER");
	UA.disabled[i].label = UA.disabled[i]:CreateFontString("UnderAchieverExceptionsDisabledContainer"..i.."Label", "ARTWORK", "GameFontHighlight");
	UA.disabled[i].label:SetPoint("TOPLEFT",UA.disabled[i].achi,"TOPRIGHT",10,0);
	UA.disabled[i].label:SetPoint("RIGHT",UA.disabled[i],"RIGHT");
	UA.disabled[i].label:SetText(" ");
	UA.disabled[i].label:SetJustifyH("LEFT");
end

local UnderAchieverExceptionsDisabledScroll = CreateFrame("ScrollFrame","UnderAchieverExceptionsDisabledScroll",UnderAchieverExceptionsDisabled,"FauxScrollFrameTemplate");
UnderAchieverExceptionsDisabledScroll:SetPoint("TOPLEFT",UA.disabled[1],"TOPLEFT",0,0);
UnderAchieverExceptionsDisabledScroll:SetPoint("BOTTOMRIGHT",UA.disabled[5],"BOTTOMRIGHT",-30,10);

local UnderAchieverExceptionsDisabledIDLabel = UnderAchieverExceptionsDisabled:CreateFontString("UnderAchieverExceptionsDisabledIDLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverExceptionsDisabledIDLabel:SetPoint("TOPLEFT", UnderAchieverExceptionsDisabledScroll, "TOPRIGHT", 25, 0);
UnderAchieverExceptionsDisabledIDLabel:SetText("ID:");
UnderAchieverExceptionsDisabledIDLabel:SetWidth(20);

local UnderAchieverExceptionsDisabledIDEdit = CreateFrame("EditBox","UnderAchieverExceptionsDisabledIDEdit",UnderAchieverExceptionsDisabled,"InputBoxTemplate");
UnderAchieverExceptionsDisabledIDEdit:SetPoint("TOPLEFT",UnderAchieverExceptionsDisabledIDLabel,"TOPRIGHT",12,8);
UnderAchieverExceptionsDisabledIDEdit:SetPoint("RIGHT",UnderAchieverExceptionsDisabled,"RIGHT",-5,0);
UnderAchieverExceptionsDisabledIDEdit:SetHeight(28);
UnderAchieverExceptionsDisabledIDEdit:SetHistoryLines(1);
UnderAchieverExceptionsDisabledIDEdit:SetAutoFocus(false);

local UnderAchieverExceptionsDisabledSelected = CreateFrame("Button","UnderAchieverExceptionsDisabledSelected",UnderAchieverExceptionsDisabled,"OptionsButtonTemplate");
UnderAchieverExceptionsDisabledSelected:SetPoint("TOPLEFT",UnderAchieverExceptionsDisabledIDLabel,"BOTTOMLEFT",0,-5);
UnderAchieverExceptionsDisabledSelected:SetPoint("RIGHT",UnderAchieverExceptionsDisabled,"RIGHT",-5,0);
UnderAchieverExceptionsDisabledSelected:SetText("Selection in list");

local UnderAchieverExceptionsDisabledCurrent = CreateFrame("Button","UnderAchieverExceptionsDisabledCurrent",UnderAchieverExceptionsDisabled,"OptionsButtonTemplate");
UnderAchieverExceptionsDisabledCurrent:SetPoint("TOPLEFT",UnderAchieverExceptionsDisabledSelected,"BOTTOMLEFT",0,0);
UnderAchieverExceptionsDisabledCurrent:SetPoint("RIGHT",UnderAchieverExceptionsDisabled,"RIGHT",-5,0);
UnderAchieverExceptionsDisabledCurrent:SetText("Get from link");

local UnderAchieverExceptionsDisabledAdd = CreateFrame("Button","UnderAchieverExceptionsDisabledAdd",UnderAchieverExceptionsDisabled,"OptionsButtonTemplate");
UnderAchieverExceptionsDisabledAdd:SetPoint("TOPLEFT",UnderAchieverExceptionsDisabledCurrent,"BOTTOMLEFT",0,-10);
UnderAchieverExceptionsDisabledAdd:SetPoint("RIGHT",UnderAchieverExceptionsDisabled,"RIGHT",-5,0);
UnderAchieverExceptionsDisabledAdd:SetText("Add");

local UnderAchieverExceptionsDisabledDelete = CreateFrame("Button","UnderAchieverExceptionsDisabledDelete",UnderAchieverExceptionsDisabled,"OptionsButtonTemplate");
UnderAchieverExceptionsDisabledDelete:SetPoint("TOPLEFT",UnderAchieverExceptionsDisabledAdd,"BOTTOMLEFT",0,0);
UnderAchieverExceptionsDisabledDelete:SetPoint("RIGHT",UnderAchieverExceptionsDisabled,"RIGHT",-5,0);
UnderAchieverExceptionsDisabledDelete:SetText("Delete");

function UA.disabled_scroll_update()
	local line;
	local lineplusoffset;
	local total, tbl = 0, {};
	for k,v in pairs(UA_Exceptions_Disabled) do
		total = total + 1;
		tbl[total] = k;
	end
	table.sort(tbl,function(a,b) return a < b end);
	FauxScrollFrame_Update(UnderAchieverExceptionsDisabledScroll,total,5,16);
	for line=1,5 do
		lineplusoffset = line + FauxScrollFrame_GetOffset(UnderAchieverExceptionsDisabledScroll);
		if lineplusoffset <= total then
			UA.disabled[line].achi:SetText(tbl[lineplusoffset]);
			UA.disabled[line].label:SetText(select(2,GetAchievementInfo(tbl[lineplusoffset])));
			UA.disabled[line]:Show();
		else
			UA.disabled[line]:Hide();
		end
	end
end

UnderAchieverExceptionsDisabledScroll:SetScript("OnVerticalScroll",function(self, offset)
	FauxScrollFrame_OnVerticalScroll(this, offset, 16, UA.disabled_scroll_update);
end);

UnderAchieverExceptionsDisabledIDEdit:SetScript("OnEnterPressed",function(...)
	UnderAchieverExceptionsDisabledIDEdit:ClearFocus();
end);

UnderAchieverExceptionsDisabledSelected:SetScript("OnClick",function(...)
	UnderAchieverExceptionsDisabledIDEdit:ClearFocus();
	if AchievementFrame then
		AchievementFrameAchievements_FindSelection();
		local selected;
		for k,v in pairs(AchievementFrameAchievementsContainer.buttons) do
			if v.selected then
				selected = v.id;
				break;
			end
		end
		if selected then
			UnderAchieverExceptionsDisabledIDEdit:SetText(selected);
		else
			UA.print("Cannot find a selected achievement in the achievements list.",true);
		end
	else
		UA.print("Achievement list not loaded, therefore no achievement is selected.",true);
	end
end);

UnderAchieverExceptionsDisabledCurrent:SetScript("OnClick",function(...)
	UnderAchieverExceptionsDisabledIDEdit:ClearFocus();
	local msg = " "..(ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):GetText();
	local id = msg:match("^.+\124c%w%w%w%w%w%w%w%w\124Hachievement:(%-?%d-):%w-:%d-:%d-:%d-:%-?%d-:%d-:%d-:%d-:%d-\124h%[.-%]\124h\124r");
	if id then
		UnderAchieverExceptionsDisabledIDEdit:SetText(id);
	else
		UA.print("Cannot find an achievement link in the chat edit box.",true);
	end
end);

UnderAchieverExceptionsDisabledAdd:SetScript("OnClick",function(...)
	local id = tonumber(UnderAchieverExceptionsDisabledIDEdit:GetText() or "") or 0;
	if GetAchievementInfo(id) then
		UA_Exceptions_Disabled[id] = true;
		UA.disabled_scroll_update();
	else
		UA.print("Achievement doesn't exist.",true);
	end
end);

UnderAchieverExceptionsDisabledDelete:SetScript("OnClick",function(...)
	local id = tonumber(UnderAchieverExceptionsDisabledIDEdit:GetText() or "") or 0;
	if UA_Exceptions_Disabled[id] then
		UA_Exceptions_Disabled[id] = nil;
		UA.disabled_scroll_update();
	else
		UA.print("Exception doesn't exist.",true);
	end
end);


-- While on, dont fake
local UnderAchieverExceptionsEnabled = CreateFrame("Frame","UnderAchieverExceptionsEnabled",UnderAchieverExceptions);
UnderAchieverExceptionsEnabled:SetPoint("TOPLEFT", UnderAchieverExceptionsDisabled, "BOTTOMLEFT", 0, -5);
UnderAchieverExceptionsEnabled:SetPoint("RIGHT", UnderAchieverExceptions, "RIGHT", -15, 0);
UnderAchieverExceptionsEnabled:SetHeight(145);
UnderAchieverExceptionsEnabled:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});
local UnderAchieverExceptionsEnabledLabel = UnderAchieverExceptionsEnabled:CreateFontString("UnderAchieverExceptionsEnabledLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverExceptionsEnabledLabel:SetPoint("TOPLEFT", UnderAchieverExceptionsEnabled, 10, -10);
UnderAchieverExceptionsEnabledLabel:SetText("Whilst auto-faking is enabled, do not fake these achievements:");

UA.enabled = {};
for i=1,5 do
	UA.enabled[i] = CreateFrame("Frame","UnderAchieverExceptionsEnabledContainer"..i,UnderAchieverExceptionsEnabled);
	UA.enabled[i]:SetWidth(365);
	UA.enabled[i]:SetHeight(24);
	if i==1 then
		UA.enabled[i]:SetPoint("TOPLEFT",UnderAchieverExceptionsEnabledLabel,"BOTTOMLEFT",10,-5);
	else
		UA.enabled[i]:SetPoint("TOPLEFT",UA.enabled[i-1],"BOTTOMLEFT",0,0);
	end
	UA.enabled[i].achi = UA.enabled[i]:CreateFontString("UnderAchieverExceptionsEnabledContainer"..i.."Achi", "ARTWORK", "GameFontNormal");
	UA.enabled[i].achi:SetPoint("TOPLEFT",UA.enabled[i],"TOPLEFT",0,0);
	UA.enabled[i].achi:SetWidth(50);
	UA.enabled[i].achi:SetText(" ");
	UA.enabled[i].achi:SetJustifyH("CENTER");
	UA.enabled[i].label = UA.enabled[i]:CreateFontString("UnderAchieverExceptionsEnabledContainer"..i.."Label", "ARTWORK", "GameFontHighlight");
	UA.enabled[i].label:SetPoint("TOPLEFT",UA.enabled[i].achi,"TOPRIGHT",10,0);
	UA.enabled[i].label:SetPoint("RIGHT",UA.enabled[i],"RIGHT");
	UA.enabled[i].label:SetText(" ");
	UA.enabled[i].label:SetJustifyH("LEFT");
end

local UnderAchieverExceptionsEnabledScroll = CreateFrame("ScrollFrame","UnderAchieverExceptionsEnabledScroll",UnderAchieverExceptionsEnabled,"FauxScrollFrameTemplate");
UnderAchieverExceptionsEnabledScroll:SetPoint("TOPLEFT",UA.enabled[1],"TOPLEFT",0,0);
UnderAchieverExceptionsEnabledScroll:SetPoint("BOTTOMRIGHT",UA.enabled[5],"BOTTOMRIGHT",-30,10);

local UnderAchieverExceptionsEnabledIDLabel = UnderAchieverExceptionsEnabled:CreateFontString("UnderAchieverExceptionsEnabledIDLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverExceptionsEnabledIDLabel:SetPoint("TOPLEFT", UnderAchieverExceptionsEnabledScroll, "TOPRIGHT", 25, 0);
UnderAchieverExceptionsEnabledIDLabel:SetText("ID:");
UnderAchieverExceptionsEnabledIDLabel:SetWidth(20);

local UnderAchieverExceptionsEnabledIDEdit = CreateFrame("EditBox","UnderAchieverExceptionsEnabledIDEdit",UnderAchieverExceptionsEnabled,"InputBoxTemplate");
UnderAchieverExceptionsEnabledIDEdit:SetPoint("TOPLEFT",UnderAchieverExceptionsEnabledIDLabel,"TOPRIGHT",12,8);
UnderAchieverExceptionsEnabledIDEdit:SetPoint("RIGHT",UnderAchieverExceptionsEnabled,"RIGHT",-5,0);
UnderAchieverExceptionsEnabledIDEdit:SetHeight(28);
UnderAchieverExceptionsEnabledIDEdit:SetHistoryLines(1);
UnderAchieverExceptionsEnabledIDEdit:SetAutoFocus(false);

local UnderAchieverExceptionsEnabledSelected = CreateFrame("Button","UnderAchieverExceptionsEnabledSelected",UnderAchieverExceptionsEnabled,"OptionsButtonTemplate");
UnderAchieverExceptionsEnabledSelected:SetPoint("TOPLEFT",UnderAchieverExceptionsEnabledIDLabel,"BOTTOMLEFT",0,-5);
UnderAchieverExceptionsEnabledSelected:SetPoint("RIGHT",UnderAchieverExceptionsEnabled,"RIGHT",-5,0);
UnderAchieverExceptionsEnabledSelected:SetText("Selection in list");

local UnderAchieverExceptionsEnabledCurrent = CreateFrame("Button","UnderAchieverExceptionsEnabledCurrent",UnderAchieverExceptionsEnabled,"OptionsButtonTemplate");
UnderAchieverExceptionsEnabledCurrent:SetPoint("TOPLEFT",UnderAchieverExceptionsEnabledSelected,"BOTTOMLEFT",0,0);
UnderAchieverExceptionsEnabledCurrent:SetPoint("RIGHT",UnderAchieverExceptionsEnabled,"RIGHT",-5,0);
UnderAchieverExceptionsEnabledCurrent:SetText("Get from link");

local UnderAchieverExceptionsEnabledAdd = CreateFrame("Button","UnderAchieverExceptionsEnabledAdd",UnderAchieverExceptionsEnabled,"OptionsButtonTemplate");
UnderAchieverExceptionsEnabledAdd:SetPoint("TOPLEFT",UnderAchieverExceptionsEnabledCurrent,"BOTTOMLEFT",0,-10);
UnderAchieverExceptionsEnabledAdd:SetPoint("RIGHT",UnderAchieverExceptionsEnabled,"RIGHT",-5,0);
UnderAchieverExceptionsEnabledAdd:SetText("Add");

local UnderAchieverExceptionsEnabledDelete = CreateFrame("Button","UnderAchieverExceptionsEnabledDelete",UnderAchieverExceptionsEnabled,"OptionsButtonTemplate");
UnderAchieverExceptionsEnabledDelete:SetPoint("TOPLEFT",UnderAchieverExceptionsEnabledAdd,"BOTTOMLEFT",0,0);
UnderAchieverExceptionsEnabledDelete:SetPoint("RIGHT",UnderAchieverExceptionsEnabled,"RIGHT",-5,0);
UnderAchieverExceptionsEnabledDelete:SetText("Delete");

function UA.enabled_scroll_update()
	local line;
	local lineplusoffset;
	local total, tbl = 0, {};
	for k,v in pairs(UA_Exceptions_Enabled) do
		total = total + 1;
		tbl[total] = k;
	end
	table.sort(tbl,function(a,b) return a < b end);
	FauxScrollFrame_Update(UnderAchieverExceptionsEnabledScroll,total,5,16);
	for line=1,5 do
		lineplusoffset = line + FauxScrollFrame_GetOffset(UnderAchieverExceptionsEnabledScroll);
		if lineplusoffset <= total then
			UA.enabled[line].achi:SetText(tbl[lineplusoffset]);
			UA.enabled[line].label:SetText(select(2,GetAchievementInfo(tbl[lineplusoffset])));
			UA.enabled[line]:Show();
		else
			UA.enabled[line]:Hide();
		end
	end
end

UnderAchieverExceptionsEnabledScroll:SetScript("OnVerticalScroll",function(self, offset)
	FauxScrollFrame_OnVerticalScroll(this, offset, 16, UA.enabled_scroll_update);
end);

UnderAchieverExceptionsEnabledIDEdit:SetScript("OnEnterPressed",function(...)
	UnderAchieverExceptionsEnabledIDEdit:ClearFocus();
end);

UnderAchieverExceptionsEnabledSelected:SetScript("OnClick",function(...)
	UnderAchieverExceptionsEnabledIDEdit:ClearFocus();
	if AchievementFrame then
		AchievementFrameAchievements_FindSelection();
		local selected;
		for k,v in pairs(AchievementFrameAchievementsContainer.buttons) do
			if v.selected then
				selected = v.id;
				break;
			end
		end
		if selected then
			UnderAchieverExceptionsEnabledIDEdit:SetText(selected);
		else
			UA.print("Cannot find a selected achievement in the achievements list.",true);
		end
	else
		UA.print("Achievement list not loaded, therefore no achievement is selected.",true);
	end
end);

UnderAchieverExceptionsEnabledCurrent:SetScript("OnClick",function(...)
	UnderAchieverExceptionsEnabledIDEdit:ClearFocus();
	local msg = " "..(ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):GetText();
	local id = msg:match("^.+\124c%w%w%w%w%w%w%w%w\124Hachievement:(%-?%d-):%w-:%d-:%d-:%d-:%-?%d-:%d-:%d-:%d-:%d-\124h%[.-%]\124h\124r");
	if id then
		UnderAchieverExceptionsEnabledIDEdit:SetText(id);
	else
		UA.print("Cannot find an achievement link in the chat edit box.",true);
	end
end);

UnderAchieverExceptionsEnabledAdd:SetScript("OnClick",function(...)
	local id = tonumber(UnderAchieverExceptionsEnabledIDEdit:GetText() or "") or 0;
	if GetAchievementInfo(id) then
		UA_Exceptions_Enabled[id] = true;
		UA.enabled_scroll_update();
	else
		UA.print("Achievement doesn't exist.",true);
	end
end);

UnderAchieverExceptionsEnabledDelete:SetScript("OnClick",function(...)
	local id = tonumber(UnderAchieverExceptionsEnabledIDEdit:GetText() or "") or 0;
	if UA_Exceptions_Enabled[id] then
		UA_Exceptions_Enabled[id] = nil;
		UA.enabled_scroll_update();
	else
		UA.print("Exception doesn't exist.",true);
	end
end);


local UnderAchieverExceptionsLabel = UnderAchieverExceptions:CreateFontString("UnderAchieverExceptionsLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverExceptionsLabel:SetPoint("TOPLEFT", UnderAchieverExceptionsEnabled, "BOTTOMLEFT", 5, 5);
UnderAchieverExceptionsLabel:SetPoint("BOTTOMRIGHT", UnderAchieverExceptions, "BOTTOMRIGHT", -15, 5);
UnderAchieverExceptionsLabel:SetJustifyH("LEFT");
UnderAchieverExceptionsLabel:SetText("Note: By default, UnderAchiever will always use the real date if you have completed the achievement");


UnderAchieverExceptions:SetScript("OnShow",function(...)
	UA.disabled_scroll_update();
	UA.enabled_scroll_update();
end);