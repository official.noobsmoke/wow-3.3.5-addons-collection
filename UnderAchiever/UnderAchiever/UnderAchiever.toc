## Interface: 30300
## Title: UnderAchiever
## Author: Aelixia (The Maelstrom EU)
## Version: 3.3.5b
## Notes: Create fake achievement links.
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: UA_Settings,UA_Saves,UA_Replacements,UA_Exceptions_Disabled,UA_Exceptions_Enabled

UnderAchiever.lua
UA_Options.lua
UA_Advanced.lua
UA_Replacements.lua
UA_Exceptions.lua
UA_About.lua