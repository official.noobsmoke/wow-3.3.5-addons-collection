﻿local UA = {};
function UA.print(msg,err)
	DEFAULT_CHAT_FRAME:AddMessage("\124cFFFFAD00[UnderAchiever]"..(err and " [Error]" or "")..": "..msg);
end

local UnderAchieverAdvanced = CreateFrame("Frame","UnderAchieverAdvanced",UnderAchieverFrame);
UnderAchieverAdvanced:Hide();
UnderAchieverAdvanced:SetAllPoints(UnderAchieverFrame);

UA.achi = 46;
UA.crits = {};
UA.day = date("%d");
UA.month = date("%m");
UA.year = date("%y");

local UnderAchieverAdvancedAchievement = CreateFrame("Frame","UnderAchieverAdvancedAchievement",UnderAchieverAdvanced);
UnderAchieverAdvancedAchievement:SetPoint("TOPLEFT", UnderAchieverAdvanced, 15, -30);
UnderAchieverAdvancedAchievement:SetWidth(250);
UnderAchieverAdvancedAchievement:SetHeight(85);
UnderAchieverAdvancedAchievement:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});
local UnderAchieverAdvancedPlayer = CreateFrame("Frame","UnderAchieverAdvancedPlayer",UnderAchieverAdvanced);
UnderAchieverAdvancedPlayer:SetPoint("TOPLEFT", UnderAchieverAdvancedAchievement, "BOTTOMLEFT", 0, -5);
UnderAchieverAdvancedPlayer:SetPoint("RIGHT", UnderAchieverAdvancedAchievement, "RIGHT", 0, 0);
UnderAchieverAdvancedPlayer:SetHeight(40);
UnderAchieverAdvancedPlayer:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});
local UnderAchieverAdvancedComplete = CreateFrame("Frame","UnderAchieverAdvancedComplete",UnderAchieverAdvanced);
UnderAchieverAdvancedComplete:SetPoint("TOPLEFT", UnderAchieverAdvancedAchievement, "TOPRIGHT", 5, 0);
UnderAchieverAdvancedComplete:SetPoint("RIGHT", UnderAchieverAdvanced, "RIGHT", -15, 0);
UnderAchieverAdvancedComplete:SetPoint("BOTTOM", UnderAchieverAdvancedPlayer, "BOTTOM");
UnderAchieverAdvancedComplete:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});
local UnderAchieverAdvancedScroll = CreateFrame("Frame","UnderAchieverAdvancedScroll",UnderAchieverAdvanced);
UnderAchieverAdvancedScroll:SetPoint("TOPLEFT", UnderAchieverAdvancedPlayer, "BOTTOMLEFT", 0, -5);
UnderAchieverAdvancedScroll:SetPoint("RIGHT", UnderAchieverAdvanced, "RIGHT", -15, 0);
UnderAchieverAdvancedScroll:SetHeight(160);
UnderAchieverAdvancedScroll:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});


-- Achievement
local UnderAchieverAdvancedAchievementLabel = UnderAchieverAdvancedAchievement:CreateFontString("UnderAchieverAdvancedAchievementLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverAdvancedAchievementLabel:SetPoint("TOPLEFT", UnderAchieverAdvancedAchievement, 10, -10);
UnderAchieverAdvancedAchievementLabel:SetText("Achievement ID:");
local UnderAchieverAdvancedAchievementEdit = CreateFrame("EditBox","UnderAchieverAdvancedAchievementEdit",UnderAchieverAdvancedAchievement,"InputBoxTemplate");
UnderAchieverAdvancedAchievementEdit:SetPoint("TOPLEFT",UnderAchieverAdvancedAchievementLabel,"TOPRIGHT",12,8);
UnderAchieverAdvancedAchievementEdit:SetWidth(60);
UnderAchieverAdvancedAchievementEdit:SetHeight(28);
UnderAchieverAdvancedAchievementEdit:SetHistoryLines(1);
UnderAchieverAdvancedAchievementEdit:SetAutoFocus(false);
UnderAchieverAdvancedAchievementEdit:SetText(UA.achi);
local UnderAchieverAdvancedAchievementSet = CreateFrame("Button","UnderAchieverAdvancedAchievementSet",UnderAchieverAdvancedAchievement,"OptionsButtonTemplate");
UnderAchieverAdvancedAchievementSet:SetPoint("TOPLEFT",UnderAchieverAdvancedAchievementEdit,"TOPRIGHT",6,-4);
UnderAchieverAdvancedAchievementSet:SetWidth(50);
UnderAchieverAdvancedAchievementSet:SetText("Set");
local UnderAchieverAdvancedAchievementSelected = CreateFrame("Button","UnderAchieverAdvancedAchievementSelected",UnderAchieverAdvancedAchievement,"OptionsButtonTemplate");
UnderAchieverAdvancedAchievementSelected:SetPoint("TOPLEFT",UnderAchieverAdvancedAchievementLabel,"BOTTOMLEFT",0,-10);
UnderAchieverAdvancedAchievementSelected:SetPoint("RIGHT",UnderAchieverAdvancedAchievementSet,"RIGHT");
UnderAchieverAdvancedAchievementSelected:SetText("Selection in achievement list");
local UnderAchieverAdvancedAchievementCurrent = CreateFrame("Button","UnderAchieverAdvancedAchievementCurrent",UnderAchieverAdvancedAchievement,"OptionsButtonTemplate");
UnderAchieverAdvancedAchievementCurrent:SetPoint("TOPLEFT",UnderAchieverAdvancedAchievementSelected,"BOTTOMLEFT",0,-2);
UnderAchieverAdvancedAchievementCurrent:SetPoint("RIGHT",UnderAchieverAdvancedAchievementSet,"RIGHT");
UnderAchieverAdvancedAchievementCurrent:SetText("Get from link in current message");


-- Player
local UnderAchieverAdvancedPlayerLabel = UnderAchieverAdvancedPlayer:CreateFontString("UnderAchieverAdvancedPlayerLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverAdvancedPlayerLabel:SetPoint("TOPLEFT",UnderAchieverAdvancedPlayer,"TOPLEFT",10,-12);
UnderAchieverAdvancedPlayerLabel:SetText("Player:");
local UnderAchieverAdvancedPlayerDropdown = CreateFrame("Frame", "UnderAchieverAdvancedPlayerDropdown", UnderAchieverAdvancedPlayer, "UIDropDownMenuTemplate");
UnderAchieverAdvancedPlayerDropdown:ClearAllPoints();
UnderAchieverAdvancedPlayerDropdown:SetPoint("TOPLEFT", UnderAchieverAdvancedPlayerLabel, "TOPRIGHT", 0, 6);
UnderAchieverAdvancedPlayerDropdown:Show();
UIDropDownMenu_SetWidth(UnderAchieverAdvancedPlayerDropdown, 100);
UIDropDownMenu_SetButtonWidth(UnderAchieverAdvancedPlayerDropdown, 124);
UIDropDownMenu_JustifyText(UnderAchieverAdvancedPlayerDropdown, "LEFT");
local UnderAchieverAdvancedPlayerEdit = CreateFrame("EditBox","UnderAchieverAdvancedPlayerEdit",UnderAchieverAdvancedPlayer,"InputBoxTemplate");
UnderAchieverAdvancedPlayerEdit:SetWidth(30);
UnderAchieverAdvancedPlayerEdit:SetHeight(28);
UnderAchieverAdvancedPlayerEdit:SetHistoryLines(1);
UnderAchieverAdvancedPlayerEdit:SetPoint("TOPLEFT",UnderAchieverAdvancedPlayerDropdown,"TOPRIGHT",5,0);
UnderAchieverAdvancedPlayerEdit:SetAutoFocus(false);
UnderAchieverAdvancedPlayerEdit:SetText(1);
UnderAchieverAdvancedPlayerEdit:Hide();


-- Scroll
local UnderAchieverAdvancedScrollLabel = UnderAchieverAdvancedScroll:CreateFontString("UnderAchieverAdvancedScrollLabel", "ARTWORK", "GameFontNormal");
UnderAchieverAdvancedScrollLabel:SetPoint("TOPLEFT",UnderAchieverAdvancedScroll,"TOPLEFT",10,-10);
UnderAchieverAdvancedScrollLabel:SetPoint("RIGHT",UnderAchieverAdvancedScroll,"RIGHT",-10,-10);
UnderAchieverAdvancedScrollLabel:SetJustifyH("LEFT");
UA.containers = {};
for i=1,5 do
	UA.containers[i] = CreateFrame("Frame","UnderAchieverAdvancedScrollContainer"..i,UnderAchieverAdvancedScroll);
	UA.containers[i]:SetWidth(340);
	UA.containers[i]:SetHeight(24);
	if i==1 then
		UA.containers[i]:SetPoint("TOPLEFT",UnderAchieverAdvancedScrollLabel,"BOTTOMLEFT",0,-5);
	else
		UA.containers[i]:SetPoint("TOPLEFT",UA.containers[i-1],"BOTTOMLEFT",0,0);
	end
	UA.containers[i].check = CreateFrame("CheckButton","UnderAchieverAdvancedScrollContainer"..i.."Check",UA.containers[i],"UICheckButtonTemplate");
	UA.containers[i].check:SetPoint("TOPLEFT",UA.containers[i],"TOPLEFT");
	UA.containers[i].label = UA.containers[i]:CreateFontString("UnderAchieverAdvancedScrollContainer"..i.."Label", "ARTWORK", "GameFontHighlight");
	UA.containers[i].label:SetPoint("TOPLEFT",UA.containers[i].check,"TOPRIGHT",2,-8);
	UA.containers[i].label:SetPoint("RIGHT",UA.containers[i],"RIGHT");
	UA.containers[i].label:SetText(" ");
	UA.containers[i].label:SetJustifyH("LEFT");
	UA.containers[i].check:SetScript("OnClick",function(...)
		UA.crits[UA.containers[i].id] = not UA.crits[UA.containers[i].id];
		UA.containers[i].check:SetChecked(UA.crits[UA.containers[i].id]);
		UA.containers[i].label:SetTextColor(1,1,1,UA.crits[UA.containers[i].id] and 1 or 0.5);
	end);
end
local UnderAchieverAdvancedScrollFrame = CreateFrame("ScrollFrame","UnderAchieverAdvancedScrollFrame",UnderAchieverAdvancedScroll,"FauxScrollFrameTemplate");
UnderAchieverAdvancedScrollFrame:SetPoint("TOPRIGHT",UA.containers[1],"TOPRIGHT",0,0);
UnderAchieverAdvancedScrollFrame:SetPoint("BOTTOMLEFT",UA.containers[5],"BOTTOMLEFT",0,0);
local UnderAchieverAdvancedScrollButtons = CreateFrame("Frame","UnderAchieverAdvancedScrollButtons",UnderAchieverAdvancedScroll);
UnderAchieverAdvancedScrollButtons:SetAllPoints(UnderAchieverAdvancedScroll);
local UnderAchieverAdvancedScrollButtonsAll = CreateFrame("Button","UnderAchieverAdvancedScrollButtonsAll",UnderAchieverAdvancedScrollButtons,"OptionsButtonTemplate");
UnderAchieverAdvancedScrollButtonsAll:SetPoint("TOPLEFT",UnderAchieverAdvancedScrollFrame,"TOPRIGHT",25,0);
UnderAchieverAdvancedScrollButtonsAll:SetPoint("RIGHT",UnderAchieverAdvancedScrollButtons,"RIGHT",-10,0);
UnderAchieverAdvancedScrollButtonsAll:SetText("Select All");
local UnderAchieverAdvancedScrollButtonsNone = CreateFrame("Button","UnderAchieverAdvancedScrollButtonsNone",UnderAchieverAdvancedScrollButtons,"OptionsButtonTemplate");
UnderAchieverAdvancedScrollButtonsNone:SetPoint("TOPLEFT",UnderAchieverAdvancedScrollButtonsAll,"BOTTOMLEFT",0,0);
UnderAchieverAdvancedScrollButtonsNone:SetPoint("RIGHT",UnderAchieverAdvancedScrollButtons,"RIGHT",-10,0);
UnderAchieverAdvancedScrollButtonsNone:SetText("Select None");
local UnderAchieverAdvancedScrollButtonsInvert = CreateFrame("Button","UnderAchieverAdvancedScrollButtonsInvert",UnderAchieverAdvancedScrollButtons,"OptionsButtonTemplate");
UnderAchieverAdvancedScrollButtonsInvert:SetPoint("TOPLEFT",UnderAchieverAdvancedScrollButtonsNone,"BOTTOMLEFT",0,0);
UnderAchieverAdvancedScrollButtonsInvert:SetPoint("RIGHT",UnderAchieverAdvancedScrollButtons,"RIGHT",-10,0);
UnderAchieverAdvancedScrollButtonsInvert:SetText("Invert Selection");
local UnderAchieverAdvancedScrollButtonsReal = CreateFrame("Button","UnderAchieverAdvancedScrollButtonsReal",UnderAchieverAdvancedScrollButtons,"OptionsButtonTemplate");
UnderAchieverAdvancedScrollButtonsReal:SetPoint("TOPLEFT",UnderAchieverAdvancedScrollButtonsInvert,"BOTTOMLEFT",0,0);
UnderAchieverAdvancedScrollButtonsReal:SetPoint("RIGHT",UnderAchieverAdvancedScrollButtons,"RIGHT",-10,0);
UnderAchieverAdvancedScrollButtonsReal:SetText("Real Completed");
local UnderAchieverAdvancedScrollButtonsData = CreateFrame("Button","UnderAchieverAdvancedScrollButtonsData",UnderAchieverAdvancedScrollButtons,"OptionsButtonTemplate");
UnderAchieverAdvancedScrollButtonsData:SetPoint("TOPLEFT",UnderAchieverAdvancedScrollButtonsReal,"BOTTOMLEFT",0,0);
UnderAchieverAdvancedScrollButtonsData:SetPoint("RIGHT",UnderAchieverAdvancedScrollButtons,"RIGHT",-10,0);
UnderAchieverAdvancedScrollButtonsData:SetText("Get from link");


-- Complete
local UnderAchieverAdvancedCompleteCheck = CreateFrame("CheckButton","UnderAchieverAdvancedCompleteCheck",UnderAchieverAdvancedComplete,"UICheckButtonTemplate");
UnderAchieverAdvancedCompleteCheck:SetPoint("TOPLEFT",UnderAchieverAdvancedComplete,"TOPLEFT",10,-6);
UnderAchieverAdvancedCompleteCheck:SetChecked(true);
local UnderAchieverAdvancedCompleteLabel = UnderAchieverAdvancedComplete:CreateFontString("UnderAchieverAdvancedCompleteLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverAdvancedCompleteLabel:SetPoint("TOPLEFT",UnderAchieverAdvancedCompleteCheck,"TOPRIGHT",2,-8);
UnderAchieverAdvancedCompleteLabel:SetText("Completed");
local UnderAchieverAdvancedCompleteOptions = CreateFrame("Frame","UnderAchieverAdvancedCompleteOptions",UnderAchieverAdvancedComplete);
UnderAchieverAdvancedCompleteOptions:SetAllPoints(UnderAchieverAdvancedComplete);
local UnderAchieverAdvancedCompleteOptionsDayLabel = UnderAchieverAdvancedCompleteOptions:CreateFontString("UnderAchieverAdvancedCompleteOptionsDayLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverAdvancedCompleteOptionsDayLabel:SetText("Day:");
UnderAchieverAdvancedCompleteOptionsDayLabel:SetPoint("TOPLEFT", UnderAchieverAdvancedCompleteCheck, "TOPLEFT", 8, -42);
UnderAchieverAdvancedCompleteOptionsDayLabel:SetWidth(50);
UnderAchieverAdvancedCompleteOptionsDayLabel:SetJustifyH("RIGHT");
local UnderAchieverAdvancedCompleteOptionsDayEdit = CreateFrame("EditBox","UnderAchieverAdvancedCompleteOptionsDayEdit",UnderAchieverAdvancedCompleteOptions,"InputBoxTemplate");
UnderAchieverAdvancedCompleteOptionsDayEdit:SetWidth(30);
UnderAchieverAdvancedCompleteOptionsDayEdit:SetHeight(28);
UnderAchieverAdvancedCompleteOptionsDayEdit:SetHistoryLines(1);
UnderAchieverAdvancedCompleteOptionsDayEdit:SetPoint("TOPLEFT",UnderAchieverAdvancedCompleteOptionsDayLabel,"TOPLEFT",60,7);
UnderAchieverAdvancedCompleteOptionsDayEdit:SetAutoFocus(false);
UnderAchieverAdvancedCompleteOptionsDayEdit:SetText(UA.day);
local UnderAchieverAdvancedCompleteOptionsMonthLabel = UnderAchieverAdvancedCompleteOptions:CreateFontString("UnderAchieverAdvancedCompleteOptionsMonthLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverAdvancedCompleteOptionsMonthLabel:SetText("Month:");
UnderAchieverAdvancedCompleteOptionsMonthLabel:SetPoint("TOPLEFT", UnderAchieverAdvancedCompleteOptionsDayLabel, "BOTTOMLEFT", 0, -10);
UnderAchieverAdvancedCompleteOptionsMonthLabel:SetWidth(50);
UnderAchieverAdvancedCompleteOptionsMonthLabel:SetJustifyH("RIGHT");
local UnderAchieverAdvancedCompleteOptionsMonthEdit = CreateFrame("EditBox","UnderAchieverAdvancedCompleteOptionsMonthEdit",UnderAchieverAdvancedCompleteOptions,"InputBoxTemplate");
UnderAchieverAdvancedCompleteOptionsMonthEdit:SetWidth(30);
UnderAchieverAdvancedCompleteOptionsMonthEdit:SetHeight(28);
UnderAchieverAdvancedCompleteOptionsMonthEdit:SetHistoryLines(1);
UnderAchieverAdvancedCompleteOptionsMonthEdit:SetPoint("TOPLEFT",UnderAchieverAdvancedCompleteOptionsMonthLabel,"TOPLEFT",60,7);
UnderAchieverAdvancedCompleteOptionsMonthEdit:SetAutoFocus(false);
UnderAchieverAdvancedCompleteOptionsMonthEdit:SetText(UA.month);
local UnderAchieverAdvancedCompleteOptionsYearLabel = UnderAchieverAdvancedCompleteOptions:CreateFontString("UnderAchieverAdvancedCompleteOptionsYearLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverAdvancedCompleteOptionsYearLabel:SetText("Year:");
UnderAchieverAdvancedCompleteOptionsYearLabel:SetPoint("TOPLEFT", UnderAchieverAdvancedCompleteOptionsMonthLabel, "BOTTOMLEFT", 0, -10);
UnderAchieverAdvancedCompleteOptionsYearLabel:SetWidth(50);
UnderAchieverAdvancedCompleteOptionsYearLabel:SetJustifyH("RIGHT");
local UnderAchieverAdvancedCompleteOptionsYearEdit = CreateFrame("EditBox","UnderAchieverAdvancedCompleteOptionsYearEdit",UnderAchieverAdvancedCompleteOptions,"InputBoxTemplate");
UnderAchieverAdvancedCompleteOptionsYearEdit:SetWidth(30);
UnderAchieverAdvancedCompleteOptionsYearEdit:SetHeight(28);
UnderAchieverAdvancedCompleteOptionsYearEdit:SetHistoryLines(1);
UnderAchieverAdvancedCompleteOptionsYearEdit:SetPoint("TOPLEFT",UnderAchieverAdvancedCompleteOptionsYearLabel,"TOPLEFT",60,7);
UnderAchieverAdvancedCompleteOptionsYearEdit:SetAutoFocus(false);
UnderAchieverAdvancedCompleteOptionsYearEdit:SetText(UA.year);
local UnderAchieverAdvancedCompleteOptionsRandom = CreateFrame("Button","UnderAchieverAdvancedCompleteOptionsRandom",UnderAchieverAdvancedCompleteOptions,"OptionsButtonTemplate");
UnderAchieverAdvancedCompleteOptionsRandom:SetPoint("TOPLEFT",UnderAchieverAdvancedCompleteOptionsDayLabel,"TOPRIGHT",50,15);
UnderAchieverAdvancedCompleteOptionsRandom:SetPoint("RIGHT",UnderAchieverAdvancedCompleteOptions,"RIGHT",-10,0);
UnderAchieverAdvancedCompleteOptionsRandom:SetText("Randomise Date");
local UnderAchieverAdvancedCompleteOptionsToday = CreateFrame("Button","UnderAchieverAdvancedCompleteOptionsToday",UnderAchieverAdvancedCompleteOptions,"OptionsButtonTemplate");
UnderAchieverAdvancedCompleteOptionsToday:SetPoint("TOPLEFT",UnderAchieverAdvancedCompleteOptionsRandom,"BOTTOMLEFT",0,0);
UnderAchieverAdvancedCompleteOptionsToday:SetPoint("RIGHT",UnderAchieverAdvancedCompleteOptions,"RIGHT",-10,0);
UnderAchieverAdvancedCompleteOptionsToday:SetText("Today's Date");
local UnderAchieverAdvancedCompleteDate = CreateFrame("Button","UnderAchieverAdvancedCompleteDate",UnderAchieverAdvancedComplete,"OptionsButtonTemplate");
UnderAchieverAdvancedCompleteDate:SetPoint("TOPLEFT",UnderAchieverAdvancedCompleteOptionsToday,"BOTTOMLEFT",0,0);
UnderAchieverAdvancedCompleteDate:SetPoint("RIGHT",UnderAchieverAdvancedComplete,"RIGHT",-10,0);
UnderAchieverAdvancedCompleteDate:SetText("Real Date");
local UnderAchieverAdvancedCompleteLink = CreateFrame("Button","UnderAchieverAdvancedCompleteLink",UnderAchieverAdvancedComplete,"OptionsButtonTemplate");
UnderAchieverAdvancedCompleteLink:SetPoint("TOPLEFT",UnderAchieverAdvancedCompleteDate,"BOTTOMLEFT",0,0);
UnderAchieverAdvancedCompleteLink:SetPoint("RIGHT",UnderAchieverAdvancedComplete,"RIGHT",-10,0);
UnderAchieverAdvancedCompleteLink:SetText("Get from link");


-- Creation
local UnderAchieverAdvancedChat = CreateFrame("Button","UnderAchieverAdvancedChat",UnderAchieverAdvanced,"OptionsButtonTemplate");
UnderAchieverAdvancedChat:SetPoint("TOPLEFT",UnderAchieverAdvancedScroll,"BOTTOMLEFT",-1,-5);
UnderAchieverAdvancedChat:SetWidth(125);
UnderAchieverAdvancedChat:SetText("Add to message");
local UnderAchieverAdvancedPrint = CreateFrame("Button","UnderAchieverAdvancedPrint",UnderAchieverAdvanced,"OptionsButtonTemplate");
UnderAchieverAdvancedPrint:SetPoint("TOPLEFT",UnderAchieverAdvancedChat,"TOPRIGHT",0,0);
UnderAchieverAdvancedPrint:SetWidth(125);
UnderAchieverAdvancedPrint:SetText("Test this link");
local UnderAchieverAdvancedSave = CreateFrame("Button","UnderAchieverAdvancedSave",UnderAchieverAdvanced,"OptionsButtonTemplate");
UnderAchieverAdvancedSave:SetPoint("TOPLEFT",UnderAchieverAdvancedPrint,"TOPRIGHT",10,0);
UnderAchieverAdvancedSave:SetWidth(80);
UnderAchieverAdvancedSave:SetText("Save");
local UnderAchieverAdvancedLoad = CreateFrame("Button","UnderAchieverAdvancedLoad",UnderAchieverAdvanced,"OptionsButtonTemplate");
UnderAchieverAdvancedLoad:SetPoint("TOPLEFT",UnderAchieverAdvancedSave,"TOPRIGHT",0,0);
UnderAchieverAdvancedLoad:SetWidth(80);
UnderAchieverAdvancedLoad:SetText("Load");
local UnderAchieverAdvancedDelete = CreateFrame("Button","UnderAchieverAdvancedDelete",UnderAchieverAdvanced,"OptionsButtonTemplate");
UnderAchieverAdvancedDelete:SetPoint("TOPLEFT",UnderAchieverAdvancedLoad,"TOPRIGHT",0,0);
UnderAchieverAdvancedDelete:SetWidth(80);
UnderAchieverAdvancedDelete:SetText("Delete");


-- Scroll script
function UA.scroll_update()
	local line;
	local lineplusoffset;
	local total = #UA.crits;
	FauxScrollFrame_Update(UnderAchieverAdvancedScrollFrame,total,5,16);
	for line=1,5 do
		lineplusoffset = line + FauxScrollFrame_GetOffset(UnderAchieverAdvancedScrollFrame);
		if lineplusoffset <= total then
			UA.containers[line].label:SetText(GetAchievementCriteriaInfo(UA.achi,lineplusoffset));
			UA.containers[line].label:SetTextColor(1,1,1,UA.crits[lineplusoffset] and 1 or 0.5);
			UA.containers[line].check:SetChecked(UA.crits[lineplusoffset]);
			UA.containers[line].id = lineplusoffset;
			UA.containers[line]:Show();
		else
			UA.containers[line]:Hide();
		end
	end
end

UnderAchieverAdvancedScrollFrame:SetScript("OnVerticalScroll",function(self, offset)
	FauxScrollFrame_OnVerticalScroll(this, offset, 16, UA.scroll_update);
end);


-- Achievement scripts
function UA.achi_update()
	UA.crits = {};
	if GetAchievementInfo(UA.achi) then
		for i=1,GetAchievementNumCriteria(UA.achi) do
			UA.crits[i] = true;
		end
	end
	local name = select(2,GetAchievementInfo(UA.achi));
	if name then
		UnderAchieverAdvancedScrollLabel:SetTextColor(1.0,0.82,0,1);
		UnderAchieverAdvancedScrollButtons:Show();
	else
		UnderAchieverAdvancedScrollLabel:SetTextColor(1,0,0,1);
		UnderAchieverAdvancedScrollButtons:Hide();
	end
	UnderAchieverAdvancedScrollLabel:SetText(name or "Achievement doesn't exist");
	if UA_Saves[UA.achi] then
		UnderAchieverAdvancedLoad:Enable();
		UnderAchieverAdvancedDelete:Enable();
	else
		UnderAchieverAdvancedLoad:Disable();
		UnderAchieverAdvancedDelete:Disable();
	end
	UA.scroll_update();
end
UA.achi_update();

UnderAchieverAdvancedAchievementEdit:SetScript("OnEscapePressed",function(...) 
	UnderAchieverAdvancedAchievementEdit:ClearFocus();
	UnderAchieverAdvancedAchievementEdit:SetText(UA.achi);
end);

UnderAchieverAdvancedAchievementEdit:SetScript("OnEnterPressed",function(...) 
	UnderAchieverAdvancedAchievementEdit:ClearFocus();
	UA.achi = tonumber(UnderAchieverAdvancedAchievementEdit:GetText() or "") or 0;
	UA.achi_update();
end);

UnderAchieverAdvancedAchievementSet:SetScript("OnClick",function(...)
	UnderAchieverAdvancedAchievementEdit:ClearFocus();
	UA.achi = tonumber(UnderAchieverAdvancedAchievementEdit:GetText() or "") or 0;
	UA.achi_update();
end);

UnderAchieverAdvancedAchievementSelected:SetScript("OnClick",function(...)
	UnderAchieverAdvancedAchievementEdit:ClearFocus();
	if AchievementFrame then
		AchievementFrameAchievements_FindSelection();
		local selected;
		for k,v in pairs(AchievementFrameAchievementsContainer.buttons) do
			if v.selected then
				selected = v.id;
				break;
			end
		end
		if selected then
			UnderAchieverAdvancedAchievementEdit:SetText(selected);
			UA.achi = selected;
			UA.achi_update();
		else
			UA.print("Cannot find a selected achievement in the achievements list.",true);
		end
	else
		UA.print("Achievement list not loaded, therefore no achievement is selected.",true);
	end
end);

UnderAchieverAdvancedAchievementCurrent:SetScript("OnClick",function(...)
	UnderAchieverAdvancedAchievementEdit:ClearFocus();
	local msg = " "..(ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):GetText();
	local id = msg:match("^.+\124c%w%w%w%w%w%w%w%w\124Hachievement:(%-?%d-):%w-:%d-:%d-:%d-:%-?%d-:%d-:%d-:%d-:%d-\124h%[.-%]\124h\124r");
	if id then
		id = tonumber(id);
		UnderAchieverAdvancedAchievementEdit:SetText(id);
		UA.achi = id;
		UA.achi_update();
	else
		UA.print("Cannot find an achievement link in the chat edit box.",true);
	end
end);


-- Player scripts
UnderAchieverAdvancedPlayerEdit:SetScript("OnEnterPressed",function(...)
	UnderAchieverAdvancedPlayerEdit:ClearFocus();
end);

UA.player_list = {
	"Player",
	"Target",
	"Focus",
	"Party",
	"Raid",
	"None",
};

function UA.player_click(self)
	local id = self:GetID();
	UIDropDownMenu_SetSelectedID(UnderAchieverAdvancedPlayerDropdown, id);
	if id == 4 or id == 5 then
		UnderAchieverAdvancedPlayerEdit:Show();
	else
		UnderAchieverAdvancedPlayerEdit:Hide();
	end
end

function UA.player_init(self, level)
	local info;
	for k,v in ipairs(UA.player_list) do
		info = UIDropDownMenu_CreateInfo();
		info.text = v;
		info.value = v;
		info.func = UA.player_click;
		UIDropDownMenu_AddButton(info, level);
	end
end
UIDropDownMenu_Initialize(UnderAchieverAdvancedPlayerDropdown, UA.player_init);
UIDropDownMenu_SetSelectedID(UnderAchieverAdvancedPlayerDropdown, 1);


-- Complete scripts
UnderAchieverAdvancedCompleteCheck:SetScript("OnClick",function(...)
	UnderAchieverAdvancedCompleteCheck:SetChecked(UnderAchieverAdvancedCompleteCheck:GetChecked() == 1 and true or false);
	if UnderAchieverAdvancedCompleteCheck:GetChecked() then
		UnderAchieverAdvancedCompleteOptions:Show();
	else
		UnderAchieverAdvancedCompleteOptions:Hide();
	end
end);

UnderAchieverAdvancedCompleteOptionsDayEdit:SetScript("OnEnterPressed",function(...)
	UnderAchieverAdvancedCompleteOptionsDayEdit:ClearFocus();
end);

UnderAchieverAdvancedCompleteOptionsMonthEdit:SetScript("OnEnterPressed",function(...)
	UnderAchieverAdvancedCompleteOptionsMonthEdit:ClearFocus();
end);

UnderAchieverAdvancedCompleteOptionsYearEdit:SetScript("OnEnterPressed",function(...)
	UnderAchieverAdvancedCompleteOptionsYearEdit:ClearFocus();
end);

UnderAchieverAdvancedCompleteOptionsRandom:SetScript("OnClick",function(...)
	local current = time();
	current = random(current - UA_Settings.max_value * 60 * 60 * 24 * (UA_Settings.max_type == 2 and 7 or UA_Settings.max_type == 3 and 30 or UA_Settings.max_type == 4 and 364 or 1), current - UA_Settings.min_value * 60 * 60 * 24 * (UA_Settings.min_type == 2 and 7 or UA_Settings.min_type == 3 and 30 or UA_Settings.min_type == 4 and 364 or 1));
	local day,month,year,year_check = date("%d",current), date("%m",current), date("%y",current), tonumber(date("%Y",current));
	UnderAchieverAdvancedCompleteOptionsDayEdit:SetText(day);
	UnderAchieverAdvancedCompleteOptionsMonthEdit:SetText(month);
	UnderAchieverAdvancedCompleteOptionsYearEdit:SetText(year_check >= 2000 and year or 0);
end);

UnderAchieverAdvancedCompleteOptionsToday:SetScript("OnClick",function(...)
	UnderAchieverAdvancedCompleteOptionsDayEdit:SetText(UA.day);
	UnderAchieverAdvancedCompleteOptionsMonthEdit:SetText(UA.month);
	UnderAchieverAdvancedCompleteOptionsYearEdit:SetText(UA.year);
end);

UnderAchieverAdvancedCompleteDate:SetScript("OnClick",function(...)
	local _, name, _, complete, month, day, year = GetAchievementInfo(UA.achi);
	if name then
		if complete then
			UnderAchieverAdvancedCompleteCheck:SetChecked(true);
			UnderAchieverAdvancedCompleteOptions:Show();
			UnderAchieverAdvancedCompleteOptionsDayEdit:SetText(day);
			UnderAchieverAdvancedCompleteOptionsMonthEdit:SetText(month);
			UnderAchieverAdvancedCompleteOptionsYearEdit:SetText(year);
		else
			UnderAchieverAdvancedCompleteCheck:SetChecked(false);
			UnderAchieverAdvancedCompleteOptions:Hide();
		end
	else
		UA.print("Achievement doesn't exist.",true);
	end
end);


UnderAchieverAdvancedCompleteLink:SetScript("OnClick",function(...)
	local msg = " "..(ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):GetText();
	local id,completed,month,day,year = msg:match("^.+\124c%w%w%w%w%w%w%w%w\124Hachievement:(%-?%d-):%w-:(%d-):(%d-):(%d-):(%-?%d-):%d-:%d-:%d-:%d-\124h%[.-%]\124h\124r");
	if id and completed and day and month and year then
		if completed == "1" then
			UnderAchieverAdvancedCompleteCheck:SetChecked(true);
			UnderAchieverAdvancedCompleteOptions:Show();
			UnderAchieverAdvancedCompleteOptionsDayEdit:SetText(day);
			UnderAchieverAdvancedCompleteOptionsMonthEdit:SetText(month);
			UnderAchieverAdvancedCompleteOptionsYearEdit:SetText(year);
		else
			UnderAchieverAdvancedCompleteCheck:SetChecked(false);
			UnderAchieverAdvancedCompleteOptions:Hide();
		end
	else
		UA.print("Cannot find an achievement link in the chat edit box.",true);
	end
end);


-- Select scripts
UnderAchieverAdvancedScrollButtonsAll:SetScript("OnClick",function(...)
	for i=1,#UA.crits do
		UA.crits[i] = true;
	end
	UA.scroll_update();
end);

UnderAchieverAdvancedScrollButtonsNone:SetScript("OnClick",function(...)
	for i=1,#UA.crits do
		UA.crits[i] = false;
	end
	UA.scroll_update();
end);

UnderAchieverAdvancedScrollButtonsInvert:SetScript("OnClick",function(...)
	for i=1,#UA.crits do
		UA.crits[i] = not UA.crits[i];
	end
	UA.scroll_update();
end);

UnderAchieverAdvancedScrollButtonsReal:SetScript("OnClick",function(...)
	if GetAchievementInfo(UA.achi) then
		for i=1,#UA.crits do
			UA.crits[i] = select(3,GetAchievementCriteriaInfo(UA.achi,i));
		end
		UA.scroll_update();
	else
		UA.print("Achievement doesn't exist.",true);
	end
end);

UnderAchieverAdvancedScrollButtonsData:SetScript("OnClick",function(...)
	local msg = " "..(ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):GetText();
	local id,num1,num2,num3,num4 = msg:match("^.+\124c%w%w%w%w%w%w%w%w\124Hachievement:(%-?%d-):%w-:%d-:%d-:%d-:%-?%d-:(%d-):(%d-):(%d-):(%d-)\124h%[.-%]\124h\124r");
	if id and num1 and num2 and num3 and num4 then
		for k,v in pairs(UA.crits) do
			local value = 0;
			local data = 0;
			if k <= 32 then
				value = 2^(k-1);
				data = num1;
			elseif k <= 64 then
				value = 2^(k-1-32);
				data = num2;
			elseif k <= 96 then
				value = 2^(k-1-64);
				data = num3;
			elseif k <= 128 then
				value = 2^(k-1-96);
				data = num4
			end
			UA.crits[k] = bit.band(value, data) > 0 and true or false;
		end
		UA.scroll_update();
	else
		UA.print("Cannot find an achievement link in the chat edit box.",true);
	end
end);


-- Creation scripts
function UA.link(arg)
	local name = select(2,GetAchievementInfo(UA.achi));
	if name then
		local unit = UA.player_list[UIDropDownMenu_GetSelectedID(UnderAchieverAdvancedPlayerDropdown)];
		if unit == "Party" or unit == "Raid" then
			unit = unit..UnderAchieverAdvancedPlayerEdit:GetText();
		end
		local guid;
		if unit ~= "None" then
			guid = UnitGUID(unit);
		else
			guid = "FFFFFFFFFFFFFFFFFF";
		end
		if guid then
			local num1,num2,num3,num4 = 0,0,0,0;
			for i=1,#UA.crits do
				if UA.crits[i] then
					if i <= 32 then
						num1 = num1 + 2^(i-1);
					elseif i <= 64 then
						num2 = num2 + 2^(i-1-32);
					elseif i <= 96 then
						num3 = num3 + 2^(i-1-64);
					elseif i <= 128 then
						num4 = num4 + 2^(i-1-96);
					end
				end
			end
			local day,month,year = tonumber(UnderAchieverAdvancedCompleteOptionsDayEdit:GetText()), tonumber(UnderAchieverAdvancedCompleteOptionsMonthEdit:GetText()), tonumber(UnderAchieverAdvancedCompleteOptionsYearEdit:GetText());
			if not day then
				day = UA.day;
				UnderAchieverAdvancedCompleteOptionsDayEdit:SetText(day);
			end
			if not month then
				month = UA.month;
				UnderAchieverAdvancedCompleteOptionsMonthEdit:SetText(month);
			end
			if not year then
				year = UA.year;
				UnderAchieverAdvancedCompleteOptionsYearEdit:SetText(year);
			end
			local complete = UnderAchieverAdvancedCompleteCheck:GetChecked();
			local link = "\124cffffff00\124Hachievement:"..UA.achi..":"..strsub(guid,3)..":"..(complete and "1" or "0")..":"..(complete and UnderAchieverAdvancedCompleteOptionsMonthEdit:GetText() or "0")..":"..(complete and UnderAchieverAdvancedCompleteOptionsDayEdit:GetText() or "0")..":"..(complete and UnderAchieverAdvancedCompleteOptionsYearEdit:GetText() or "0")..":"..num1..":"..num2..":"..num3..":"..num4.."\124h["..name.."]\124h\124r";
			if arg then
				(ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):SetText((ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):GetText()..link);
				(ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):Show();
			else
				UA.print(link);
			end
		else
			UA.print("Invalid player.",true);
		end
	else
		UA.print("Invalid achievement.",true);
	end
end

UnderAchieverAdvancedChat:SetScript("OnClick",function(...)
	UA.link(true);
end);

UnderAchieverAdvancedPrint:SetScript("OnClick",function(...)
	UA.link(false);
end);

UnderAchieverAdvancedSave:SetScript("OnClick",function(...)
	local name = select(2,GetAchievementInfo(UA.achi));
	if name then
		UA_Saves[UA.achi] = {
			crits = {},
			complete = UnderAchieverAdvancedCompleteCheck:GetChecked() and true or false,
			day = UnderAchieverAdvancedCompleteOptionsDayEdit:GetText(),
			month = UnderAchieverAdvancedCompleteOptionsMonthEdit:GetText(),
			year = UnderAchieverAdvancedCompleteOptionsYearEdit:GetText(),
			player = UIDropDownMenu_GetSelectedID(UnderAchieverAdvancedPlayerDropdown),
			unit = UnderAchieverAdvancedPlayerEdit:GetText(),
		};
		for k,v in pairs(UA.crits) do
			UA_Saves[UA.achi].crits[k] = v;
		end
		UnderAchieverAdvancedLoad:Enable();
		UnderAchieverAdvancedDelete:Enable();
		UA.print("Link saved.");
	else
		UA.print("Invalid achievement.",true);
	end
end);


UnderAchieverAdvancedLoad:SetScript("OnClick",function(...)
	if UA_Saves[UA.achi] then
		UA.crits = {};
		for k,v in pairs(UA_Saves[UA.achi].crits) do
			UA.crits[k] = v;
		end
		UA.scroll_update();
		UnderAchieverAdvancedCompleteCheck:SetChecked(UA_Saves[UA.achi].complete);
		if UnderAchieverAdvancedCompleteCheck:GetChecked() then
			UnderAchieverAdvancedCompleteOptions:Show();
		else
			UnderAchieverAdvancedCompleteOptions:Hide();
		end
		if UA_Saves[UA.achi].player == 4 or UA_Saves[UA.achi].player == 5 then
			UnderAchieverAdvancedPlayerEdit:Show();
		else
			UnderAchieverAdvancedPlayerEdit:Hide();
		end
		UnderAchieverAdvancedCompleteOptionsDayEdit:SetText(UA_Saves[UA.achi].day);
		UnderAchieverAdvancedCompleteOptionsMonthEdit:SetText(UA_Saves[UA.achi].month);
		UnderAchieverAdvancedCompleteOptionsYearEdit:SetText(UA_Saves[UA.achi].year);
		UIDropDownMenu_SetSelectedID(UnderAchieverAdvancedPlayerDropdown, UA_Saves[UA.achi].player);
		UIDropDownMenu_SetText(UnderAchieverAdvancedPlayerDropdown, UA.player_list[UIDropDownMenu_GetSelectedID(UnderAchieverAdvancedPlayerDropdown)]);
		UnderAchieverAdvancedPlayerEdit:SetText(UA_Saves[UA.achi].unit);
	else
		UA.print("You have no save for the current achievement.",true);
	end
end);


UnderAchieverAdvancedDelete:SetScript("OnClick",function(...)
	if UA_Saves[UA.achi] then
		UA_Saves[UA.achi] = nil;
		UnderAchieverAdvancedLoad:Disable();
		UnderAchieverAdvancedDelete:Disable();
	else
		UA.print("Save doesn't exist.",true);
	end
end);


UnderAchieverAdvanced:SetScript("OnShow",function(...)
	UIDropDownMenu_SetText(UnderAchieverAdvancedPlayerDropdown, UA.player_list[UIDropDownMenu_GetSelectedID(UnderAchieverAdvancedPlayerDropdown)]);
	if UA_Saves[UA.achi] then
		UnderAchieverAdvancedLoad:Enable();
		UnderAchieverAdvancedDelete:Enable();
	else
		UnderAchieverAdvancedLoad:Disable();
		UnderAchieverAdvancedDelete:Disable();
	end
end);