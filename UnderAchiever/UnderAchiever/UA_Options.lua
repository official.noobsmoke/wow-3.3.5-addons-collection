﻿local UA = {};
function UA.print(msg,err)
	DEFAULT_CHAT_FRAME:AddMessage("\124cFFFFAD00[UnderAchiever]"..(err and " [Error]" or "")..": "..msg);
end

local UnderAchieverOptions = CreateFrame("Frame","UnderAchieverOptions",UnderAchieverFrame);
UnderAchieverOptions:Show();
UnderAchieverOptions:SetAllPoints(UnderAchieverFrame);


-- Auto-Faking
local UnderAchieverOptionsFaking = CreateFrame("Frame","UnderAchieverOptionsFaking",UnderAchieverOptions);
UnderAchieverOptionsFaking:SetPoint("TOPLEFT", UnderAchieverOptions, 15, -30);
UnderAchieverOptionsFaking:SetPoint("RIGHT", UnderAchieverOptions, "RIGHT", -15, 0);
UnderAchieverOptionsFaking:SetHeight(190);
UnderAchieverOptionsFaking:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});

local UnderAchieverOptionsFakingCheck = CreateFrame("CheckButton","UnderAchieverOptionsFakingCheck",UnderAchieverOptionsFaking,"UICheckButtonTemplate");
UnderAchieverOptionsFakingCheck:SetPoint("TOPLEFT",UnderAchieverOptionsFaking,"TOPLEFT",12,-8);
local UnderAchieverOptionsFakingLabel = UnderAchieverOptionsFaking:CreateFontString("UnderAchieverOptionsFakingLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsFakingLabel:SetPoint("TOPLEFT",UnderAchieverOptionsFakingCheck,"TOPRIGHT",2,-8);
UnderAchieverOptionsFakingLabel:SetText("Auto-Faking");

local UnderAchieverOptionsFakingText = UnderAchieverOptionsFaking:CreateFontString("UnderAchieverOptionsFakingText", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsFakingText:SetPoint("TOP", UnderAchieverOptionsFakingCheck, "BOTTOM", 0, -4);
UnderAchieverOptionsFakingText:SetPoint("LEFT", UnderAchieverOptionsFaking, "LEFT", 15, 0);
UnderAchieverOptionsFakingText:SetWidth(160);
UnderAchieverOptionsFakingText:SetJustifyH("LEFT");
UnderAchieverOptionsFakingText:SetText("Start with auto-faking:");

local UnderAchieverOptionsFakingDropdown = CreateFrame("Frame", "UnderAchieverOptionsFakingDropdown", UnderAchieverOptionsFaking, "UIDropDownMenuTemplate");
UnderAchieverOptionsFakingDropdown:ClearAllPoints();
UnderAchieverOptionsFakingDropdown:SetPoint("TOPLEFT", UnderAchieverOptionsFakingText, "TOPRIGHT", 0, 6);
UnderAchieverOptionsFakingDropdown:Show();
UIDropDownMenu_SetWidth(UnderAchieverOptionsFakingDropdown, 130);
UIDropDownMenu_SetButtonWidth(UnderAchieverOptionsFakingDropdown, 154);
UIDropDownMenu_JustifyText(UnderAchieverOptionsFakingDropdown, "LEFT");
UA.faking_list = {
	"Enabled",
	"Disabled",
	"Same as last session",
};
function UA.faking_click(self)
	local id = self:GetID();
	UIDropDownMenu_SetSelectedID(UnderAchieverOptionsFakingDropdown, id);
end
function UA.faking_init(self, level)
	local info;
	for k,v in ipairs(UA.faking_list) do
		info = UIDropDownMenu_CreateInfo();
		info.text = v;
		info.value = v;
		info.func = UA.faking_click;
		UIDropDownMenu_AddButton(info, level);
	end
end
UIDropDownMenu_Initialize(UnderAchieverOptionsFakingDropdown, UA.faking_init);

local UnderAchieverOptionsFakingSaveCheck = CreateFrame("CheckButton","UnderAchieverOptionsFakingSaveCheck",UnderAchieverOptionsFaking,"UICheckButtonTemplate");
UnderAchieverOptionsFakingSaveCheck:SetPoint("TOPLEFT",UnderAchieverOptionsFakingCheck,"BOTTOMLEFT",0,-26);
local UnderAchieverOptionsFakingSaveLabel = UnderAchieverOptionsFaking:CreateFontString("UnderAchieverOptionsFakingSaveLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsFakingSaveLabel:SetPoint("TOPLEFT",UnderAchieverOptionsFakingSaveCheck,"TOPRIGHT",2,-8);
UnderAchieverOptionsFakingSaveLabel:SetText("Use advanced saves in auto-faking");

local UnderAchieverOptionsFakingDateCheck = CreateFrame("CheckButton","UnderAchieverOptionsFakingDateCheck",UnderAchieverOptionsFaking,"UICheckButtonTemplate");
UnderAchieverOptionsFakingDateCheck:SetPoint("TOPLEFT",UnderAchieverOptionsFakingSaveCheck,"BOTTOMLEFT",0,4);
local UnderAchieverOptionsFakingDateLabel = UnderAchieverOptionsFaking:CreateFontString("UnderAchieverOptionsFakingDateLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsFakingDateLabel:SetPoint("TOPLEFT",UnderAchieverOptionsFakingDateCheck,"TOPRIGHT",2,-8);
UnderAchieverOptionsFakingDateLabel:SetText("Use the real date/link if I have completed the achievement");

local UnderAchieverOptionsFakingDisableCheck = CreateFrame("CheckButton","UnderAchieverOptionsFakingDisableCheck",UnderAchieverOptionsFaking,"UICheckButtonTemplate");
UnderAchieverOptionsFakingDisableCheck:SetPoint("TOPLEFT",UnderAchieverOptionsFakingDateCheck,"BOTTOMLEFT",0,4);
local UnderAchieverOptionsFakingDisableLabel = UnderAchieverOptionsFaking:CreateFontString("UnderAchieverOptionsFakingDisableLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsFakingDisableLabel:SetPoint("TOPLEFT",UnderAchieverOptionsFakingDisableCheck,"TOPRIGHT",2,-8);
UnderAchieverOptionsFakingDisableLabel:SetText("Don't auto-fake whilst the Advanced Faking page is open");

local UnderAchieverOptionsFakingRealCheck = CreateFrame("CheckButton","UnderAchieverOptionsFakingRealCheck",UnderAchieverOptionsFaking,"UICheckButtonTemplate");
UnderAchieverOptionsFakingRealCheck:SetPoint("TOPLEFT",UnderAchieverOptionsFakingDisableCheck,"BOTTOMLEFT",0,4);
local UnderAchieverOptionsFakingRealLabel = UnderAchieverOptionsFaking:CreateFontString("UnderAchieverOptionsFakingRealLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsFakingRealLabel:SetPoint("TOPLEFT",UnderAchieverOptionsFakingRealCheck,"TOPRIGHT",2,-5);
UnderAchieverOptionsFakingRealLabel:SetPoint("RIGHT",UnderAchieverOptionsFaking,"RIGHT",-5,0);
UnderAchieverOptionsFakingRealLabel:SetPoint("BOTTOM",UnderAchieverOptionsFaking,"BOTTOM",0,5);
UnderAchieverOptionsFakingRealLabel:SetJustifyH("LEFT");
UnderAchieverOptionsFakingRealLabel:SetJustifyV("TOP");
UnderAchieverOptionsFakingRealLabel:SetText("Whilst auto-faking is enabled, change links to my real achievement data (instead of faking)");



-- Min / Max
local UnderAchieverOptionsDate = CreateFrame("Frame","UnderAchieverOptionsDate",UnderAchieverOptions);
UnderAchieverOptionsDate:SetPoint("TOPLEFT", UnderAchieverOptionsFaking, "BOTTOMLEFT", 0, -5);
UnderAchieverOptionsDate:SetPoint("RIGHT", UnderAchieverOptions, "RIGHT", -15, 0);
UnderAchieverOptionsDate:SetHeight(100);
UnderAchieverOptionsDate:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});

local UnderAchieverOptionsDateText = UnderAchieverOptionsDate:CreateFontString("UnderAchieverOptionsDateText", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsDateText:SetPoint("TOPLEFT", UnderAchieverOptionsDate, "TOPLEFT", 15, -10);
UnderAchieverOptionsDateText:SetPoint("RIGHT", UnderAchieverOptionsDate, "RIGHT", -15, 0);
UnderAchieverOptionsDateText:SetJustifyH("LEFT");
UnderAchieverOptionsDateText:SetText("Pick random dates between the following times:");

local UnderAchieverOptionsDateMin = CreateFrame("EditBox","UnderAchieverOptionsDateMin",UnderAchieverOptionsDate,"InputBoxTemplate");
UnderAchieverOptionsDateMin:SetWidth(60);
UnderAchieverOptionsDateMin:SetHeight(28);
UnderAchieverOptionsDateMin:SetHistoryLines(1);
UnderAchieverOptionsDateMin:SetPoint("TOPLEFT",UnderAchieverOptionsDateText,"BOTTOMLEFT",12,-6);
UnderAchieverOptionsDateMin:SetAutoFocus(false);
UnderAchieverOptionsDateMin:SetText(UA_Settings.min_value);
UnderAchieverOptionsDateMin:SetScript("OnEnterPressed",UnderAchieverOptionsDateMin.ClearFocus);


local UnderAchieverOptionsDateMinDropdown = CreateFrame("Frame", "UnderAchieverOptionsDateMinDropdown", UnderAchieverOptionsDate, "UIDropDownMenuTemplate");
UnderAchieverOptionsDateMinDropdown:ClearAllPoints();
UnderAchieverOptionsDateMinDropdown:SetPoint("TOPLEFT", UnderAchieverOptionsDateMin, "TOPRIGHT", -4, 0);
UnderAchieverOptionsDateMinDropdown:Show();
UIDropDownMenu_SetWidth(UnderAchieverOptionsDateMinDropdown, 90);
UIDropDownMenu_SetButtonWidth(UnderAchieverOptionsDateMinDropdown, 114);
UIDropDownMenu_JustifyText(UnderAchieverOptionsDateMinDropdown, "LEFT");
UA.date_list = {
	"Days",
	"Weeks",
	"Months",
	"Years",
};
function UA.min_click(self)
	local id = self:GetID();
	UIDropDownMenu_SetSelectedID(UnderAchieverOptionsDateMinDropdown, id);
end
function UA.min_init(self, level)
	local info;
	for k,v in ipairs(UA.date_list) do
		info = UIDropDownMenu_CreateInfo();
		info.text = v;
		info.value = v;
		info.func = UA.min_click;
		UIDropDownMenu_AddButton(info, level);
	end
end
UIDropDownMenu_Initialize(UnderAchieverOptionsDateMinDropdown, UA.min_init);

local UnderAchieverOptionsDateMinText = UnderAchieverOptionsDate:CreateFontString("UnderAchieverOptionsDateMinText", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsDateMinText:SetPoint("TOPLEFT", UnderAchieverOptionsDateMinDropdown, "TOPRIGHT", -6, -6);
UnderAchieverOptionsDateMinText:SetWidth(50);
UnderAchieverOptionsDateMinText:SetJustifyH("LEFT");
UnderAchieverOptionsDateMinText:SetText("ago");

local UnderAchieverOptionsDateMax = CreateFrame("EditBox","UnderAchieverOptionsDateMax",UnderAchieverOptionsDate,"InputBoxTemplate");
UnderAchieverOptionsDateMax:SetWidth(60);
UnderAchieverOptionsDateMax:SetHeight(28);
UnderAchieverOptionsDateMax:SetHistoryLines(1);
UnderAchieverOptionsDateMax:SetPoint("TOPLEFT",UnderAchieverOptionsDateMin,"BOTTOMLEFT",0,-6);
UnderAchieverOptionsDateMax:SetAutoFocus(false);
UnderAchieverOptionsDateMax:SetText(UA_Settings.max_value);
UnderAchieverOptionsDateMax:SetScript("OnEnterPressed",UnderAchieverOptionsDateMax.ClearFocus);

local UnderAchieverOptionsDateMaxDropdown = CreateFrame("Frame", "UnderAchieverOptionsDateMaxDropdown", UnderAchieverOptionsDate, "UIDropDownMenuTemplate");
UnderAchieverOptionsDateMaxDropdown:ClearAllPoints();
UnderAchieverOptionsDateMaxDropdown:SetPoint("TOPLEFT", UnderAchieverOptionsDateMax, "TOPRIGHT", -4, 0);
UnderAchieverOptionsDateMaxDropdown:Show();
UIDropDownMenu_SetWidth(UnderAchieverOptionsDateMaxDropdown, 90);
UIDropDownMenu_SetButtonWidth(UnderAchieverOptionsDateMaxDropdown, 114);
UIDropDownMenu_JustifyText(UnderAchieverOptionsDateMaxDropdown, "LEFT");
function UA.max_click(self)
	local id = self:GetID();
	UIDropDownMenu_SetSelectedID(UnderAchieverOptionsDateMaxDropdown, id);
end
function UA.max_init(self, level)
	local info;
	for k,v in ipairs(UA.date_list) do
		info = UIDropDownMenu_CreateInfo();
		info.text = v;
		info.value = v;
		info.func = UA.max_click;
		UIDropDownMenu_AddButton(info, level);
	end
end
UIDropDownMenu_Initialize(UnderAchieverOptionsDateMaxDropdown, UA.max_init);

local UnderAchieverOptionsDateMaxText = UnderAchieverOptionsDate:CreateFontString("UnderAchieverOptionsDateMaxText", "ARTWORK", "GameFontHighlight");
UnderAchieverOptionsDateMaxText:SetPoint("TOPLEFT", UnderAchieverOptionsDateMaxDropdown, "TOPRIGHT", -6, -6);
UnderAchieverOptionsDateMaxText:SetWidth(50);
UnderAchieverOptionsDateMaxText:SetJustifyH("LEFT");
UnderAchieverOptionsDateMaxText:SetText("ago");


local UnderAchieverOptionsSave = CreateFrame("Button","UnderAchieverOptionsSave",UnderAchieverOptions,"OptionsButtonTemplate");
UnderAchieverOptionsSave:SetPoint("TOPRIGHT",UnderAchieverOptionsDate,"BOTTOMRIGHT",0,-5);
UnderAchieverOptionsSave:SetWidth(120);
UnderAchieverOptionsSave:SetText("Save Changes");
UnderAchieverOptionsSave:SetScript("OnClick",function(...)
	UA_Settings.state = UnderAchieverOptionsFakingCheck:GetChecked() and true or false;
	UA_Settings.default = UIDropDownMenu_GetSelectedID(UnderAchieverOptionsFakingDropdown);
	UA_Settings.saves = UnderAchieverOptionsFakingSaveCheck:GetChecked() and true or false;
	UA_Settings.date = UnderAchieverOptionsFakingDateCheck:GetChecked() and true or false;
	UA_Settings.real = UnderAchieverOptionsFakingRealCheck:GetChecked() and true or false;
	UA_Settings.disable = UnderAchieverOptionsFakingDisableCheck:GetChecked() and true or false;
	local min_value, min_type = tonumber(UnderAchieverOptionsDateMin:GetText() or "") or 1, UIDropDownMenu_GetSelectedID(UnderAchieverOptionsDateMinDropdown);
	local max_value, max_type = tonumber(UnderAchieverOptionsDateMax:GetText() or "") or 1, UIDropDownMenu_GetSelectedID(UnderAchieverOptionsDateMaxDropdown);
	local real_min = min_value * 60 * 60 * 24 * (min_type == 2 and 7 or min_type == 3 and 30 or min_type == 4 and 364 or 1);
	local real_max = max_value * 60 * 60 * 24 * (max_type == 2 and 7 or max_type == 3 and 30 or max_type == 4 and 364 or 1);
	local final_min, final_max = min(real_min, real_max), max(real_min, real_max);
	UA_Settings.min_value = final_min == real_min and min_value or max_value;
	UA_Settings.min_type = final_min == real_min and min_type or max_type;
	UA_Settings.max_value = final_max == real_max and max_value or min_value; 
	UA_Settings.max_type = final_max == real_max and max_type or min_type;
	UA.print("Options saved.");
end);


UnderAchieverOptions:SetScript("OnShow",function(...)
	UnderAchieverOptionsFakingCheck:SetChecked(UA_Settings.state);
	UIDropDownMenu_SetSelectedID(UnderAchieverOptionsFakingDropdown, UA_Settings.default);
	UIDropDownMenu_SetText(UnderAchieverOptionsFakingDropdown, UA.faking_list[UA_Settings.default]);
	UnderAchieverOptionsFakingSaveCheck:SetChecked(UA_Settings.saves);
	UnderAchieverOptionsFakingDateCheck:SetChecked(UA_Settings.date);
	UnderAchieverOptionsFakingRealCheck:SetChecked(UA_Settings.real);
	UnderAchieverOptionsFakingDisableCheck:SetChecked(UA_Settings.disable);
	UnderAchieverOptionsDateMin:SetText(UA_Settings.min_value);
	UIDropDownMenu_SetSelectedID(UnderAchieverOptionsDateMinDropdown, UA_Settings.min_type);
	UIDropDownMenu_SetText(UnderAchieverOptionsDateMinDropdown, UA.date_list[UA_Settings.min_type]);
	UnderAchieverOptionsDateMax:SetText(UA_Settings.max_value);
	UIDropDownMenu_SetSelectedID(UnderAchieverOptionsDateMaxDropdown, UA_Settings.max_type);
	UIDropDownMenu_SetText(UnderAchieverOptionsDateMaxDropdown, UA.date_list[UA_Settings.max_type]);
end);