﻿local UA = {};
function UA.print(msg,err)
	DEFAULT_CHAT_FRAME:AddMessage("\124cFFFFAD00[UnderAchiever]"..(err and " [Error]" or "")..": "..msg);
end

local UnderAchieverReplacements = CreateFrame("Frame","UnderAchieverReplacements",UnderAchieverFrame);
UnderAchieverReplacements:Hide();
UnderAchieverReplacements:SetAllPoints(UnderAchieverFrame);


local UnderAchieverReplacementsScroll = CreateFrame("Frame","UnderAchieverReplacementsScroll",UnderAchieverReplacements);
UnderAchieverReplacementsScroll:SetPoint("TOPLEFT", UnderAchieverReplacements, 15, -30);
UnderAchieverReplacementsScroll:SetPoint("BOTTOM", UnderAchieverReplacements, "BOTTOM", 0, 20);
UnderAchieverReplacementsScroll:SetWidth(365);
UnderAchieverReplacementsScroll:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});


local UnderAchieverReplacementsControls = CreateFrame("Frame","UnderAchieverReplacementsControls",UnderAchieverReplacements);
UnderAchieverReplacementsControls:SetPoint("TOPLEFT", UnderAchieverReplacementsScroll, "TOPRIGHT", 5, 0);
UnderAchieverReplacementsControls:SetPoint("BOTTOM", UnderAchieverReplacements, "BOTTOM", 0, 20);
UnderAchieverReplacementsControls:SetPoint("RIGHT", UnderAchieverReplacements, "RIGHT", -15, 0);
UnderAchieverReplacementsControls:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});

local UnderAchieverReplacementsControlsText = UnderAchieverReplacementsControls:CreateFontString("UnderAchieverReplacementsControlsText", "ARTWORK", "GameFontNormal");
UnderAchieverReplacementsControlsText:SetPoint("TOPLEFT", UnderAchieverReplacementsControls, 5, -10);
UnderAchieverReplacementsControlsText:SetPoint("RIGHT", UnderAchieverReplacementsControls, "RIGHT", -5, 0);
UnderAchieverReplacementsControlsText:SetText("Replacement");

local UnderAchieverReplacementsControlsTextLabel = UnderAchieverReplacementsControls:CreateFontString("UnderAchieverReplacementsControlsTextLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverReplacementsControlsTextLabel:SetPoint("TOPLEFT", UnderAchieverReplacementsControlsText, "TOPLEFT", 0, -20);
UnderAchieverReplacementsControlsTextLabel:SetText("%");
UnderAchieverReplacementsControlsTextLabel:SetWidth(12);
UnderAchieverReplacementsControlsTextLabel:SetJustifyH("LEFT");

local UnderAchieverReplacementsControlsTextEdit = CreateFrame("EditBox","UnderAchieverReplacementsControlsTextEdit",UnderAchieverReplacementsControls,"InputBoxTemplate");
UnderAchieverReplacementsControlsTextEdit:SetPoint("TOPLEFT",UnderAchieverReplacementsControlsTextLabel,"TOPRIGHT",5,8);
UnderAchieverReplacementsControlsTextEdit:SetPoint("RIGHT", UnderAchieverReplacementsControlsText, "RIGHT", -2, 0);
UnderAchieverReplacementsControlsTextEdit:SetHeight(28);
UnderAchieverReplacementsControlsTextEdit:SetHistoryLines(1);
UnderAchieverReplacementsControlsTextEdit:SetAutoFocus(false);

local UnderAchieverReplacementsControlsID = UnderAchieverReplacementsControls:CreateFontString("UnderAchieverReplacementsControlsID", "ARTWORK", "GameFontNormal");
UnderAchieverReplacementsControlsID:SetPoint("TOPLEFT", UnderAchieverReplacementsControlsText, "BOTTOMLEFT", 0, -40);
UnderAchieverReplacementsControlsID:SetPoint("RIGHT", UnderAchieverReplacementsControlsText, "RIGHT", 0, 0);
UnderAchieverReplacementsControlsID:SetText("Achievement");

local UnderAchieverReplacementsControlsIDLabel = UnderAchieverReplacementsControls:CreateFontString("UnderAchieverReplacementsControlsIDLabel", "ARTWORK", "GameFontHighlight");
UnderAchieverReplacementsControlsIDLabel:SetPoint("TOPLEFT", UnderAchieverReplacementsControlsID, "TOPLEFT", 0, -20);
UnderAchieverReplacementsControlsIDLabel:SetText("ID:");
UnderAchieverReplacementsControlsIDLabel:SetWidth(20);

local UnderAchieverReplacementsControlsIDEdit = CreateFrame("EditBox","UnderAchieverReplacementsControlsIDEdit",UnderAchieverReplacementsControls,"InputBoxTemplate");
UnderAchieverReplacementsControlsIDEdit:SetPoint("TOPLEFT",UnderAchieverReplacementsControlsIDLabel,"TOPRIGHT",12,8);
UnderAchieverReplacementsControlsIDEdit:SetPoint("RIGHT",UnderAchieverReplacementsControlsID,"RIGHT",-2,0);
UnderAchieverReplacementsControlsIDEdit:SetHeight(28);
UnderAchieverReplacementsControlsIDEdit:SetHistoryLines(1);
UnderAchieverReplacementsControlsIDEdit:SetAutoFocus(false);

local UnderAchieverReplacementsControlsSelected = CreateFrame("Button","UnderAchieverReplacementsControlsSelected",UnderAchieverReplacementsControls,"OptionsButtonTemplate");
UnderAchieverReplacementsControlsSelected:SetPoint("TOPLEFT",UnderAchieverReplacementsControlsIDLabel,"BOTTOMLEFT",0,-5);
UnderAchieverReplacementsControlsSelected:SetPoint("RIGHT",UnderAchieverReplacementsControlsID,"RIGHT",0,0);
UnderAchieverReplacementsControlsSelected:SetText("Selection in list");

local UnderAchieverReplacementsControlsCurrent = CreateFrame("Button","UnderAchieverReplacementsControlsCurrent",UnderAchieverReplacementsControls,"OptionsButtonTemplate");
UnderAchieverReplacementsControlsCurrent:SetPoint("TOPLEFT",UnderAchieverReplacementsControlsSelected,"BOTTOMLEFT",0,0);
UnderAchieverReplacementsControlsCurrent:SetPoint("RIGHT",UnderAchieverReplacementsControlsID,"RIGHT",0,0);
UnderAchieverReplacementsControlsCurrent:SetText("Get from link");

local UnderAchieverReplacementsControlsAdd = CreateFrame("Button","UnderAchieverReplacementsControlsAdd",UnderAchieverReplacementsControls,"OptionsButtonTemplate");
UnderAchieverReplacementsControlsAdd:SetPoint("TOPLEFT",UnderAchieverReplacementsControlsCurrent,"BOTTOMLEFT",0,-20);
UnderAchieverReplacementsControlsAdd:SetPoint("RIGHT",UnderAchieverReplacementsControlsID,"RIGHT",0,0);
UnderAchieverReplacementsControlsAdd:SetText("Add");

local UnderAchieverReplacementsControlsDelete = CreateFrame("Button","UnderAchieverReplacementsControlsDelete",UnderAchieverReplacementsControls,"OptionsButtonTemplate");
UnderAchieverReplacementsControlsDelete:SetPoint("TOPLEFT",UnderAchieverReplacementsControlsAdd,"BOTTOMLEFT",0,0);
UnderAchieverReplacementsControlsDelete:SetPoint("RIGHT",UnderAchieverReplacementsControlsID,"RIGHT",0,0);
UnderAchieverReplacementsControlsDelete:SetText("Delete");


UA.containers = {};
for i=1,13 do
	UA.containers[i] = CreateFrame("Frame","UnderAchieverReplacementsScrollContainer"..i,UnderAchieverReplacementsScroll);
	UA.containers[i]:SetPoint("RIGHT",UnderAchieverReplacementsScroll,0,0);
	UA.containers[i]:SetHeight(24);
	if i==1 then
		UA.containers[i]:SetPoint("TOPLEFT",UnderAchieverReplacementsScroll,"TOPLEFT",10,-10);
	else
		UA.containers[i]:SetPoint("TOPLEFT",UA.containers[i-1],"BOTTOMLEFT",0,0);
	end
	UA.containers[i].achi = UA.containers[i]:CreateFontString("UnderAchieverReplacementsScrollContainer"..i.."Achi", "ARTWORK", "GameFontNormal");
	UA.containers[i].achi:SetPoint("TOPLEFT",UA.containers[i],"TOPLEFT",0,0);
	UA.containers[i].achi:SetWidth(50);
	UA.containers[i].achi:SetText(" ");
	UA.containers[i].achi:SetJustifyH("CENTER");
	UA.containers[i].label = UA.containers[i]:CreateFontString("UnderAchieverReplacementsScrollContainer"..i.."Label", "ARTWORK", "GameFontHighlight");
	UA.containers[i].label:SetPoint("TOPLEFT",UA.containers[i].achi,"TOPRIGHT",10,0);
	UA.containers[i].label:SetPoint("RIGHT",UA.containers[i],"RIGHT");
	UA.containers[i].label:SetText(" ");
	UA.containers[i].label:SetJustifyH("LEFT");
end

local UnderAchieverReplacementsScrollFrame = CreateFrame("ScrollFrame","UnderAchieverReplacementsScrollFrame",UnderAchieverReplacementsScroll,"FauxScrollFrameTemplate");
UnderAchieverReplacementsScrollFrame:SetPoint("TOPLEFT",UnderAchieverReplacementsScroll,"TOPLEFT",10,-10);
UnderAchieverReplacementsScrollFrame:SetPoint("BOTTOMRIGHT",UnderAchieverReplacementsScroll,"BOTTOMRIGHT",-30,10);

function UA.scroll_update()
	local line;
	local lineplusoffset;
	local total, tbl = 0, {};
	for k,v in pairs(UA_Replacements) do
		total = total + 1;
		tbl[total] = k;
	end
	table.sort(tbl,function(a,b) return UA_Replacements[a] < UA_Replacements[b] end);
	FauxScrollFrame_Update(UnderAchieverReplacementsScrollFrame,total,13,16);
	for line=1,13 do
		lineplusoffset = line + FauxScrollFrame_GetOffset(UnderAchieverReplacementsScrollFrame);
		if lineplusoffset <= total then
			UA.containers[line].achi:SetText(UA_Replacements[tbl[lineplusoffset]]);
			UA.containers[line].label:SetText("%"..tbl[lineplusoffset]);
			UA.containers[line]:Show();
		else
			UA.containers[line]:Hide();
		end
	end
end

UnderAchieverReplacementsScrollFrame:SetScript("OnVerticalScroll",function(self, offset)
	FauxScrollFrame_OnVerticalScroll(this, offset, 16, UA.scroll_update);
end);

UnderAchieverReplacementsControlsTextEdit:SetScript("OnEnterPressed",function(...)
	UnderAchieverReplacementsControlsTextEdit:ClearFocus();
end);

UnderAchieverReplacementsControlsIDEdit:SetScript("OnEnterPressed",function(...)
	UnderAchieverReplacementsControlsIDEdit:ClearFocus();
end);

UnderAchieverReplacementsControlsSelected:SetScript("OnClick",function(...)
	UnderAchieverReplacementsControlsIDEdit:ClearFocus();
	if AchievementFrame then
		AchievementFrameAchievements_FindSelection();
		local selected;
		for k,v in pairs(AchievementFrameAchievementsContainer.buttons) do
			if v.selected then
				selected = v.id;
				break;
			end
		end
		if selected then
			UnderAchieverReplacementsControlsIDEdit:SetText(selected);
		else
			UA.print("Cannot find a selected achievement in the achievements list.",true);
		end
	else
		UA.print("Achievement list not loaded, therefore no achievement is selected.",true);
	end
end);

UnderAchieverReplacementsControlsCurrent:SetScript("OnClick",function(...)
	UnderAchieverReplacementsControlsIDEdit:ClearFocus();
	local msg = " "..(ChatEdit_GetActiveWindow() or ChatEdit_GetLastActiveWindow()):GetText();
	local id = msg:match("^.+\124c%w%w%w%w%w%w%w%w\124Hachievement:(%-?%d-):%w-:%d-:%d-:%d-:%-?%d-:%d-:%d-:%d-:%d-\124h%[.-%]\124h\124r");
	if id then
		UnderAchieverReplacementsControlsIDEdit:SetText(id);
	else
		UA.print("Cannot find an achievement link in the chat edit box.",true);
	end
end);

UnderAchieverReplacementsControlsAdd:SetScript("OnClick",function(...)
	local id = tonumber(UnderAchieverReplacementsControlsIDEdit:GetText() or "") or 0;
	if GetAchievementInfo(id) then
		local text = UnderAchieverReplacementsControlsTextEdit:GetText() or "";
		text = text:match("^([^%s%%]+)");
		if text and text > "" then
			UA_Replacements[text] = id;
			UA.scroll_update();
		else
			UA.print("Invalid replacement text.",true);
		end
	else
		UA.print("Achievement doesn't exist.",true);
	end
end);

UnderAchieverReplacementsControlsDelete:SetScript("OnClick",function(...)
	local text = UnderAchieverReplacementsControlsTextEdit:GetText() or "";
	text = text:match("^(%S+)");
	if text and text > "" and UA_Replacements[text] then
		UA_Replacements[text] = nil;
		UA.scroll_update();
	else
		UA.print("Replacement doesn't exist.",true);
	end
end);


UnderAchieverReplacements:SetScript("OnShow",UA.scroll_update);