﻿local UA = {};
function UA.print(msg,err)
	DEFAULT_CHAT_FRAME:AddMessage("\124cFFFFAD00[UnderAchiever]"..(err and " [Error]" or "")..": "..msg);
end

local UnderAchieverAbout = CreateFrame("Frame","UnderAchieverAbout",UnderAchieverFrame);
UnderAchieverAbout:Hide();
UnderAchieverAbout:SetAllPoints(UnderAchieverFrame);


-- UnderAchiever
local UnderAchieverAboutAddon = CreateFrame("Frame","UnderAchieverAboutAddon",UnderAchieverAbout);
UnderAchieverAboutAddon:SetPoint("TOPLEFT", UnderAchieverAbout, 15, -30);
UnderAchieverAboutAddon:SetPoint("RIGHT", UnderAchieverAbout, "RIGHT", -15, 0);
UnderAchieverAboutAddon:SetHeight(160);
UnderAchieverAboutAddon:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});
local UnderAchieverAboutAddonTexture = UnderAchieverAboutAddon:CreateTexture("UnderAchieverAboutAddonTexture", "ARTWORK");
UnderAchieverAboutAddonTexture:SetPoint("TOPLEFT", UnderAchieverAboutAddon, 10, -15);
UnderAchieverAboutAddonTexture:SetHeight(128);
UnderAchieverAboutAddonTexture:SetWidth(128);
UnderAchieverAboutAddonTexture:SetTexture("Interface\\AddOns\\UnderAchiever\\Textures\\UA");
local UnderAchieverAboutAddonTitleLabel = UnderAchieverAboutAddon:CreateFontString("UnderAchieverAboutAddonTitleLabel", "ARTWORK", "GameFontNormal");
UnderAchieverAboutAddonTitleLabel:SetPoint("TOPLEFT", UnderAchieverAboutAddonTexture, "TOPRIGHT", 10, 0);
UnderAchieverAboutAddonTitleLabel:SetText("AddOn:");
local UnderAchieverAboutAddonTitleText = UnderAchieverAboutAddon:CreateFontString("UnderAchieverAboutAddonTitleText", "ARTWORK", "GameFontHighlight");
UnderAchieverAboutAddonTitleText:SetPoint("TOPLEFT", UnderAchieverAboutAddonTitleLabel, "TOPRIGHT", 10, 0);
UnderAchieverAboutAddonTitleText:SetText("UnderAchiever");
local UnderAchieverAboutAddonDescriptionLabel = UnderAchieverAboutAddon:CreateFontString("UnderAchieverAboutAddonDescriptionLabel", "ARTWORK", "GameFontNormal");
UnderAchieverAboutAddonDescriptionLabel:SetPoint("TOPLEFT", UnderAchieverAboutAddonTitleLabel, "BOTTOMLEFT", 0, -10);
UnderAchieverAboutAddonDescriptionLabel:SetText("Description:");
local UnderAchieverAboutAddonDescriptionText = UnderAchieverAboutAddon:CreateFontString("UnderAchieverAboutAddonDescriptionText", "ARTWORK", "GameFontHighlight");
UnderAchieverAboutAddonDescriptionText:SetPoint("TOPLEFT", UnderAchieverAboutAddonDescriptionLabel, "TOPRIGHT", 10, 0);
UnderAchieverAboutAddonDescriptionText:SetPoint("RIGHT", UnderAchieverAboutAddon, "RIGHT", -10, 0);
UnderAchieverAboutAddonDescriptionText:SetPoint("BOTTOM", UnderAchieverAboutAddon, "BOTTOM", 10, 0);
UnderAchieverAboutAddonDescriptionText:SetText("UnderAchiever is an AddOn that allows players to fake achievement links automatically. Players can also create replacement texts, which are exchanged for achievement links when typing a chat message.");
UnderAchieverAboutAddonDescriptionText:SetJustifyH("LEFT");
UnderAchieverAboutAddonDescriptionText:SetJustifyV("TOP");


-- Aelobin
local UnderAchieverAboutAuthor = CreateFrame("Frame","UnderAchieverAboutAuthor",UnderAchieverAbout);
UnderAchieverAboutAuthor:SetPoint("TOPLEFT", UnderAchieverAboutAddon, "BOTTOMLEFT", 0, -5);
UnderAchieverAboutAuthor:SetWidth(300);
UnderAchieverAboutAuthor:SetHeight(160);
UnderAchieverAboutAuthor:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});
local UnderAchieverAboutAuthorTexture = UnderAchieverAboutAuthor:CreateTexture("UnderAchieverAboutAuthorTexture", "ARTWORK");
UnderAchieverAboutAuthorTexture:SetPoint("TOPLEFT", UnderAchieverAboutAuthor, 10, -15);
UnderAchieverAboutAuthorTexture:SetHeight(128);
UnderAchieverAboutAuthorTexture:SetWidth(128);
UnderAchieverAboutAuthorTexture:SetTexture("Interface\\AddOns\\UnderAchiever\\Textures\\Aelo");
local UnderAchieverAboutAuthorTitleLabel = UnderAchieverAboutAuthor:CreateFontString("UnderAchieverAboutAuthorTitleLabel", "ARTWORK", "GameFontNormal");
UnderAchieverAboutAuthorTitleLabel:SetPoint("TOPLEFT", UnderAchieverAboutAuthorTexture, "TOPRIGHT", 10, 0);
UnderAchieverAboutAuthorTitleLabel:SetText("Author:");
local UnderAchieverAboutAuthorTitleText = UnderAchieverAboutAuthor:CreateFontString("UnderAchieverAboutAuthorTitleText", "ARTWORK", "GameFontHighlight");
UnderAchieverAboutAuthorTitleText:SetPoint("TOPLEFT", UnderAchieverAboutAuthorTitleLabel, "TOPRIGHT", 10, 0);
UnderAchieverAboutAuthorTitleText:SetText("Aelobin");


-- Reset buttons
local UnderAchieverAboutReset = CreateFrame("Frame","UnderAchieverAboutReset",UnderAchieverAbout);
UnderAchieverAboutReset:SetPoint("TOPLEFT", UnderAchieverAboutAuthor, "TOPRIGHT", 5, 0);
UnderAchieverAboutReset:SetPoint("RIGHT", UnderAchieverAbout, -15, 0);
UnderAchieverAboutReset:SetPoint("BOTTOM", UnderAchieverAboutAuthor, "BOTTOM");
UnderAchieverAboutReset:SetBackdrop({ 
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16, 
	insets = { left = 5, right = 5, top = 5, bottom = 5 }
});

local UnderAchieverAboutResetOptions = CreateFrame("Button","UnderAchieverAboutResetOptions",UnderAchieverAbout,"OptionsButtonTemplate");
UnderAchieverAboutResetOptions:SetPoint("TOPLEFT",UnderAchieverAboutReset,"TOPLEFT",5,-10);
UnderAchieverAboutResetOptions:SetPoint("RIGHT",UnderAchieverAboutReset,"RIGHT",-5,0);
UnderAchieverAboutResetOptions:SetText("Default Options");
UnderAchieverAboutResetOptions:SetScript("OnClick",function(...)
	UA_Settings.min_value = 7;
	UA_Settings.min_type = 1;
	UA_Settings.max_value = 14;
	UA_Settings.max_type = 1;
	UA_Settings.state = true;
	UA_Settings.default = 1;
	UA_Settings.saves = false;
	UA_Settings.date = true;
	UA_Settings.real = false;
	UA_Settings.disable = false;
	UA.print("Options have been reset to defaults.");
end);

local UnderAchieverAboutResetSaves = CreateFrame("Button","UnderAchieverAboutResetSaves",UnderAchieverAbout,"OptionsButtonTemplate");
UnderAchieverAboutResetSaves:SetPoint("TOPLEFT",UnderAchieverAboutResetOptions,"BOTTOMLEFT",0,0);
UnderAchieverAboutResetSaves:SetPoint("RIGHT",UnderAchieverAboutReset,"RIGHT",-5,0);
UnderAchieverAboutResetSaves:SetText("Clear Advanced Saves");
UnderAchieverAboutResetSaves:SetScript("OnClick",function(...)
	UA_Saves = {};
	UA.print("All advanced saves have been deleted.");
end);

local UnderAchieverAboutResetReplacements = CreateFrame("Button","UnderAchieverAboutResetReplacements",UnderAchieverAbout,"OptionsButtonTemplate");
UnderAchieverAboutResetReplacements:SetPoint("TOPLEFT",UnderAchieverAboutResetSaves,"BOTTOMLEFT",0,0);
UnderAchieverAboutResetReplacements:SetPoint("RIGHT",UnderAchieverAboutReset,"RIGHT",-5,0);
UnderAchieverAboutResetReplacements:SetText("Default Replacements");
UnderAchieverAboutResetReplacements:SetScript("OnClick",function(...)
	UA_Replacements = {
		epic = 556,
		naxx10 = 576,
		naxx25 = 577,
		os10 = 1876,
		os25 = 625,
		eoe10 = 622,
		eoe25 = 623,
		uld10 = 2894,
		uld25 = 2895,
		toc10 = 3917,
		toc25 = 3916,
		togc10 = 3918,
		togc25 = 3812,
		ony10 = 4396,
		ony25 = 4397,
		voa10 = 4585,
		voa25 = 4586,
		icc10 = 4532,
		icc10hc = 4636,
		icc25 = 4608,
		icc25hc = 4637,
		ewf10 = 4016,
		ewf25 = 4017,
		rs10 = 4817,
		rs10hc = 4818,
		rs25 = 4815,
		rs25hc = 4816,
	};
	UA.print("Replacements have been reset to defaults.");
end);

local UnderAchieverAboutResetDisabledExceptions = CreateFrame("Button","UnderAchieverAboutResetDisabledExceptions",UnderAchieverAbout,"OptionsButtonTemplate");
UnderAchieverAboutResetDisabledExceptions:SetPoint("TOPLEFT",UnderAchieverAboutResetReplacements,"BOTTOMLEFT",0,0);
UnderAchieverAboutResetDisabledExceptions:SetPoint("RIGHT",UnderAchieverAboutReset,"RIGHT",-5,0);
UnderAchieverAboutResetDisabledExceptions:SetText("Clear Disabled Exceptions");
UnderAchieverAboutResetDisabledExceptions:SetScript("OnClick",function(...)
	UA_Exceptions_Disabled = {};
	UA.print("All disabled exceptions have been deleted.");
end);

local UnderAchieverAboutResetEnabledExceptions = CreateFrame("Button","UnderAchieverAboutResetEnabledExceptions",UnderAchieverAbout,"OptionsButtonTemplate");
UnderAchieverAboutResetEnabledExceptions:SetPoint("TOPLEFT",UnderAchieverAboutResetDisabledExceptions,"BOTTOMLEFT",0,0);
UnderAchieverAboutResetEnabledExceptions:SetPoint("RIGHT",UnderAchieverAboutReset,"RIGHT",-5,0);
UnderAchieverAboutResetEnabledExceptions:SetText("Clear Enabled Exceptions");
UnderAchieverAboutResetEnabledExceptions:SetScript("OnClick",function(...)
	UA_Exceptions_Enabled = {};
	UA.print("All enabled exceptions have been deleted.");
end);

local UnderAchieverAboutResetAll = CreateFrame("Button","UnderAchieverAboutResetAll",UnderAchieverAbout,"OptionsButtonTemplate");
UnderAchieverAboutResetAll:SetPoint("LEFT",UnderAchieverAboutResetEnabledExceptions,"LEFT");
UnderAchieverAboutResetAll:SetPoint("RIGHT",UnderAchieverAboutResetEnabledExceptions,"RIGHT");
UnderAchieverAboutResetAll:SetPoint("BOTTOM",UnderAchieverAboutReset,"BOTTOM", 0, 10);
UnderAchieverAboutResetAll:SetText("Reset/Clear All");
UnderAchieverAboutResetAll:SetScript("OnClick",function(...)
	UA_Settings.min_value = 7;
	UA_Settings.min_type = 1;
	UA_Settings.max_value = 14;
	UA_Settings.max_type = 1;
	UA_Settings.state = true;
	UA_Settings.default = 1;
	UA_Settings.saves = false;
	UA_Settings.date = true;
	UA_Settings.real = false;
	UA_Settings.disable = false;
	
	UA_Saves = {};
	
	UA_Replacements = {
		epic = 556,
		naxx10 = 576,
		naxx25 = 577,
		os10 = 1876,
		os25 = 625,
		eoe10 = 622,
		eoe25 = 623,
		uld10 = 2894,
		uld25 = 2895,
		toc10 = 3917,
		toc25 = 3916,
		togc10 = 3918,
		togc25 = 3812,
		ony10 = 4396,
		ony25 = 4397,
		voa10 = 4585,
		voa25 = 4586,
		icc10 = 4532,
		icc10hc = 4636,
		icc25 = 4608,
		icc25hc = 4637,
		ewf10 = 4016,
		ewf25 = 4017,
		rs10 = 4817,
		rs10hc = 4818,
		rs25 = 4815,
		rs25hc = 4816,
	};
	
	UA_Exceptions_Disabled = {};
	UA_Exceptions_Enabled = {};
	
	UA.print("UnderAchiever has been reset to defaults.");
end);