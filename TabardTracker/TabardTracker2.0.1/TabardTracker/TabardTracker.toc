## Interface: 40000
## Title:Tabard|cff66ff66Tracker|r
## Notes: Remembers what Tabards you have equiped for the Achievement
## Version: 20101127
## DefaultState: enabled
## Author: Rakreo of Duskwood
## X-Website: http://www.Rakreo.com
## SavedVariablesPerCharacter: TabardTrackerDB

TabardTracker.lua