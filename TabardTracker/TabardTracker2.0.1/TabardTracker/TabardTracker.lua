-- Capture the current version of the addon from the .toc file
local CURRENT_VERSION = GetAddOnMetadata('TabardTracker', 'Version');

------------------------------------------------------------------------------------------------------------------------------
-- Create a Frame for this addon so we can attach scripts and define the array for the functions that need to be registered --
------------------------------------------------------------------------------------------------------------------------------
local TabardTracker, events = CreateFrame('Frame'), {};
---------------------------------------------------------------------
-- Fired after the Saved Variables for this addon have been loaded --
---------------------------------------------------------------------
function events:ADDON_LOADED(...)
 -- Fired after the Saved Variables for this addon have been loaded
	if ... == 'TabardTracker' then
		-- Make sure the database has been populated with the defaults
		TabardTracker:InitializeDB();
	end;
end;
-----------------------------------------------------------------------------------------------------------------------------
-- Fired when the games first loads or when entering/leaving an instance (Basically anytime the 'Loading' screen is shown) --
-----------------------------------------------------------------------------------------------------------------------------
function events:PLAYER_ENTERING_WORLD(...)
	-- Check to see our current progress on the Tabard Achievements
	TabardTracker:ProgressCheck();
	-- Once the player has Entered the World check to see what Tabard they are wearing
	TabardTracker:CheckTabard();
end;
-------------------------------------------
-- Fired when Equipment has been changed --
-------------------------------------------
function events:PLAYER_EQUIPMENT_CHANGED(...)
	-- If we detect a Equipment change make sure we know if there is a new TabardTracker
	TabardTracker:CheckTabard();
end;
-----------------------------------------------------
-- Fired when an Achievement has had progress made --
-----------------------------------------------------
function events:CRITERIA_UPDATE(...)
	-- Check to see our current progress on the Tabard Achievements
	TabardTracker:ProgressCheck();
end;
----------------------------------------------------------------------------------------------------------
-- Set the 'OnEvent' trigger assigned to PetShop and execute the function based on the Event that fired --
----------------------------------------------------------------------------------------------------------
TabardTracker:SetScript('OnEvent', function(self, event, ...) events[event](self, ...); end);
-- Loop thru all the 'events' array and register the 'Events'
for k, v in pairs(events) do
	-- Register all events for which handlers have been defined
	TabardTracker:RegisterEvent(k);
end;
----------------------------------------
-- Create a hook into the GameTooltip --
----------------------------------------
GameTooltip:HookScript('OnTooltipSetItem', function(tooltip, ...)
	-- Define a few things
	local text = '|cff33ff33Already Equiped|r';
	-- Get the ItemLink and ItemEquipmentLocation for the item we are looking at
	local _, itemLink, _, _, _, _, _, _, itemEquipLoc = GetItemInfo(select(2, tooltip:GetItem()));
	-- If we are looking at a Tabard then process it
	if itemEquipLoc == "INVTYPE_TABARD" then
		-- Strip out just the ItemID
		local _, id = strsplit(":", itemLink);
		-- See if we have already seen this Tabard
		if not table.find(TabardTrackerDB['Tabards'], id) then
			-- Change the text to show we need to equip this Tabard
			text = '|cffff3333Need to Equip|r';
		end;
		-- Add our line to the GameTooltip
		GameTooltip:AddDoubleLine(text, TabardTrackerDB['Progress']);
		-- Redraw the GameTooltip
		GameTooltip:Show();
	end;
end)

-- Function to scan an Array for a 'Value'
function table.find(t, v)
	--Loop thru the table 't'
	for k, val in pairs(t) do
		-- if the 'Value' mathes the current array element the return the array[index]
		if v == val then return k end;
	end;
	--The 'Value' did not exist so return false
	return false;
end;

-----------------------------------------------------------
-- Function to make sure the DataBase has default values --
-----------------------------------------------------------
function TabardTracker:InitializeDB()
	-- If there is not TabardTrackerDB then set it to a blank array
	if not TabardTrackerDB then TabardTrackerDB = {}; end;
	-- Make sure the DataBase version gets updated
	TabardTrackerDB['Version'] = CURRENT_VERSION;
	-- Make sure the DataBase has a default ['Tabards'] array
	TabardTrackerDB['Tabards'] = TabardTrackerDB['Tabards'] or {};
	-- Make sure the DataBase has a default ['Progress'] text
	TabardTrackerDB['Progress'] = TabardTrackerDB['Progress'] or '|cffff00000|r|cffffff00/|r|cff00ff001|r';
end;

function TabardTracker:CheckTabard()
	-- See if we can capture the ItemLink of their Tabard
	local tabardlink = GetInventoryItemLink("player",GetInventorySlotInfo("TabardSlot"));
	-- If they were wearing a Tabard then find out the Item ID
	if tabardlink then
		-- Strip out just the ItemID
		local _, id = strsplit(":", tabardlink);
		-- See if we have already seen this Tabard
		if not table.find(TabardTrackerDB['Tabards'], id) then
			-- Save the ItemID in the DataBase
			table.insert(TabardTrackerDB['Tabards'], id);
		end;
	end;
end;

function TabardTracker:ProgressCheck()
	-- Define some stuff
	local achivementID = 1020;
	-- Check the progress of the Achievement "Represent (621)"
	local Completed = select(4, GetAchievementInfo(621));
	-- If we have completed it then we need to check "Ten Tabards (1020)"
	if Completed == true then
		-- Check the progress of the Achievement "Ten Tabards (1020)"
		Completed = select(4, GetAchievementInfo(achivementID));
		-- If we have not finished this addon then create the "progress" text
		if Completed == true then
			-- Change achievementID to be "Twenty Five Tabards (1021)"
			achivementID = 1021;
		end;
		-- Get our current progress for this Achievement
		local x, y = select(4, GetAchievementCriteriaInfo(achivementID, 1));
		-- Build the colorized progress string
		TabardTrackerDB['Progress'] = '|cffff0000'..x..'|r|cffffff00/|r|cff00ff00'..y..'|r';
	end;
end;