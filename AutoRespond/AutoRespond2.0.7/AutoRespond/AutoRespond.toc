## Interface: 20003
## Title: AutoRespond
## Author: Ayradyss/Chef de Loup
## Notes: Automatically sends a whispered response to someone who whispers you using the prescribed string.
## SavedVariables: AutoRespondOptions, AutoRespondMainOptions
AutoRespond.xml
