AUTO_RESPOND_LOADED_TEXT = "AutoRespond version %s loaded";
AUTO_RESPOND_ADD_NEW_TEXT = "AutoRespond: default respond added.";
AUTO_RESPOND_DELETED_TEXT = "AutoRespond: respond deleted.";
AUTO_RESPOND_EMPTY_LIST_TEXT = "AutoRespond: respondlist is empty";

AUTO_RESPOND_LABEL_KEYWORDS_TEXT = "Keywords";
AUTO_RESPOND_LABEL_RESPONSE_TEXT = "Response";
AUTO_RESPOND_BUTTON_BEFORE_TEXT = "Previous";
AUTO_RESPOND_BUTTON_NEXT_TEXT = "Next";
AUTO_RESPOND_BUTTON_NEW_TEXT = "New";
AUTO_RESPOND_BUTTON_ACTIVE_TEXT = "respond is active";
AUTO_RESPOND_BUTTON_SCRIPT_TEXT = "response is script";
AUTO_RESPOND_BUTTON_DELETE_TEXT = "Delete";
AUTO_RESPOND_BUTTON_LINK_TEXT = "Add link";
AUTO_RESPOND_BUTTON_REAGENT_TEXT = "Reagents";
AUTO_RESPOND_BUTTON_EXIT_TEXT = "Exit";

AUTO_RESPOND_BUTTON_CANGROUP_TEXT = "only members of party";
AUTO_RESPOND_BUTTON_CANRAID_TEXT = "only members of raid";
AUTO_RESPOND_BUTTON_CANFRIENDS_TEXT = "only friends";
AUTO_RESPOND_BUTTON_CANGUILD_TEXT = "only members of guild";
AUTO_RESPOND_BUTTON_CANNAMES_TEXT = "only players with given name(s)";

AUTO_RESPOND_BUTTON_RESPONDTOGROUP_TEXT = "response in partychat";
AUTO_RESPOND_BUTTON_RESPONDTORAID_TEXT = "response in raidchat";
AUTO_RESPOND_BUTTON_RESPONDTOGUILD_TEXT = "response in guildchat";

AUTO_RESPOND_OPTIONS_BUTTON_ACTIVEMOD = "mod is active";
AUTO_RESPOND_OPTIONS_BUTTON_INFIGHT = "respond with busy message in fight";

AUTO_RESPOND_DEFAULT_KEYWORD_TEXT = "auto-answer";
AUTO_RESPOND_DEFAULT_RESPONSE_TEXT = "Hello, this is an automated response from AutoRespond!";
AUTO_RESPOND_DEFAULT_INFIGHT_MESSAGE = "Sorry i'm busy please try again later";

AUTO_RESPOND_NO_SELECTED_SKILL = "You must select a skill item.";
AUTO_RESPOND_NO_LINK_ITEM = "The selected entry is no linkable item!";

AUTO_RESPOND_PROMOTE_CHANNEL_ERROR = "You must specify a number for the channel to promote to.";