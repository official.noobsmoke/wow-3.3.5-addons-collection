AutoRespond ReadMe File 
Last updated 12th February 2007 by Chef de Loup
Version 2.0.7 Beta

Introducing AutoRespond, for those who want a quick and easy automated whisper response

Essentially, whenever someone whispers you and that whisper contains a customizable keyword (it can be mixed in among other things), you'll automatically respond with a standardized whisper.

AutoInvite loads defaulted to on, using the keyword "auto-answer", responding "Hello, this is an automated response from AutoRespond!".

Intended use: For those tradeskillers who want to stay off the channels: You can set the keyword, for example, to "enchantments", and then respond automatically with "I have +2 STR [Bracers], +3 STA [Nosepiece], etc, just 10 silver!" whenever anyone whispers you "what enchantments do you sell?"

Usage: 	
"/ar" or "/autorespond" : Show the AutoRespond Frame
"/ar options" or "/autorespond options" for showing the main options frame
"/ar spam" or "/autorespond spam" for switching the status of spam check 

Hints: To use the add link button, you must select an item of your tradeskill and press the add link button.

Version log:
	2.0.7 : fixed some Bugs and included the french loacalization by Kubik
	2.0.6 : Beta with visual options for spam check
	2.0.5 : Beta for spam check
	2.0.4 : bitbyte fixed some stuff
	2.0.1 : recreated the window, including new options and a better view/handling, this is a Beta Version for the 2.0 WoW-Client and BC-Beta-Client
	1.55 : auto save for keyword and response
	1.54 : fixed the draggingsystem, you must now drag the bag item to the 'link' button instead of textfield 
	1.5 : drag system included to create links of bag items by dragging into the textfield
	1.41 : set responsefield to unlimited letters; add button to include reagents of trade/craftskills
	1.4 : fixed the problems with shift-clicking system, fixed a bug with responses, the system will now continue with searching of keywords while finding the first
	1.3.1 : fixed a bug with shift-clicking on chat items
	1.3 : reinclude the linking from chat, new function to promote responses to any channel
	1.2 : removed function to link from chat, responses can now be handled as scripts
	1.1 : include shift-click to add links from chat, enchanter can now add links from enchant-window
	1.0 : fixed a major bug within saving responds
	0.7 : include functionality to add tradeskill item links
	0.6 : include localization for german client
	0.5 Beta : Including ui-frames for handling responds
	0.4 : Fixed bug for special chars.
	0.3 : Added new functions for multiply respond and responselines
	0.1 : Initial public release


HowTo promote responses to channels
Use the function Auto_Respond_PromoteToChannel(index,channel[,number]); in your makro.
  - index is the index of the respond to promote, the respond must be active and no script!
  - channel is the system to where to promote to, the following systems are available
	"GUILD" - promote to guildchannel
	"PARTY" - promote to partychannel
	"SAY" - promote as say
	"YELL" - promote as yell
	"RAID" - promote to raidchannel
	"CHANNEL" - promote to channel with the given number
  - number is a optional parameter, only used with system "CHANNEL"! the number is the number of the channel you want to promote to mostly 2 is the channelnumber for tradechannel

Example in makro:
	/script Auto_Respond_PromoteToChannel(1,"GUILD"); -- this will promote the first respond to the guildchannel
	/script Auto_Respond_PromoteToChannel(1,"CHANNEL",2); -- this will promote the first respond to the channel with number 2 (mostly tradechannel)