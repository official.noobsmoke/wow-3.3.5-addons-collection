## Interface: 30300
## Title: Ingela's Rapture
## Notes: Displays a bar to see when your Rapture is ready again.
## Author: Ingela
## Version: 1.2
## SavedVariables: RaptureDB
## X-Curse-Packaged-Version: v1.3
## X-Curse-Project-Name: Ingela's Rapture
## X-Curse-Project-ID: ingelasrapture
## X-Curse-Repository-ID: wow/ingelasrapture/mainline

rapture.xml

core.lua
