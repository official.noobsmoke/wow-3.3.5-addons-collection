Rapture = LibStub("AceAddon-3.0"):NewAddon("Rapture", "AceConsole-3.0", "AceEvent-3.0")
RaptureGUI = LibStub("AceGUI-3.0")
local rapmedia = LibStub("LibSharedMedia-3.0")
local rapcandy = LibStub("LibCandyBar-3.0")
local db

local defaults = {
    profile = {
        scale = 1.0,
        xpos = 0,
        ypos = 0,
        colorr = 0, 
        colorg = 0.5, 
        colorb = 1, 
        colora = 1,        
        bgcolorr = 0, 
        bgcolorg = 0.5, 
        bgcolorb = 1, 
        bgcolora = 0.5,
        width = 200,
        height = 19,
        texture = "Blizzard",
        font = "Friz Quadrata TT",
        fontscale = 12,
        border = "Blizzard Dialog",
        bordersize = 16,
        borderinset = 16,
        borderheight = 23,
        borderwidth = 203,
        outline = 4,
    },
}

local options = {
  type = "group",
  childGroups = "tab",
  args = {
        info = {
		  order = 1,
          name = "Info",
          desc = "Shows info about the addon",
          type = 'execute',
          func = function()
            Rapture:Print("This addon shows a neat bar too see how long cooldown is left on your rapture ability. Made by Ingela @ Twilight's Hammer(EU)")
          end,
        },
        test = {
		  order = 2,
          name = "Run Bar",
          desc = "Test rapture cooldown bar",
          type = 'execute',
          func = function()
          Rapture:Runbar()
          end,
        },
    bar = {
      type = "group",
      name = "Bar options",
      args = {
        barposdesc = {
          order = 1,
          type = "description",
          name = "Bar positioning\n",
          cmdHidden = true
        },
		barposdesc = {
          order = 1,
          type = "description",
          name = "WARNING! When you change these options, do NOT run testbars, like BigWigs.\nKittens will die if you do not proceed with caution!\n",
          cmdHidden = true
        },
        xpos = {
          order = 3,
          name = "X",
          desc = "Change horizontal position",
          type = 'range',
          min = -500,
          max = 500,
          step = 1,
          get = function(info) return db.xpos end,
          set = function(info, x) 
          db.xpos = x
          Rapture:Runtestbar()
          end,
        },
        ypos = {
          order = 6,
          name = "Y",
          desc = "Change veritcal position",
          type = 'range',
          min = -500,
          max = 500,
          step = 1,
          get = function(info) return db.ypos end,
          set = function(info, y) 
          db.ypos = y
          Rapture:Runtestbar()
          end,
          },
         nudgeright = {
          order = 4,
          name = "Right",
          desc = "Nudge right once",
          type = 'execute',
          get = function(info) return db.xpos end,
          func = function() 
          db.xpos = db.xpos + 1
          Rapture:Runtestbar()
          end,
         },
         nudgeleft = {
          order = 2,
          name = "Left",
          desc = "Nudge left once",
          type = 'execute',
          get = function(info) return db.xpos end,
          func = function() 
          db.xpos = db.xpos - 1
          Rapture:Runtestbar()
          end,
         },
         nudgeup = {
          order = 5,
          name = "Up",
          desc = "Nudge up once",
          type = 'execute',
          get = function(info) return db.ypos end,
          func = function() 
          db.ypos = db.ypos + 1
          Rapture:Runtestbar()
          end,
         },
         nudgedown = {
          order = 7,
          name = "Down",
          desc = "Nudge down once",
          type = 'execute',
          get = function(info) return db.ypos end,
          func = function() 
          db.ypos = db.ypos - 1
          Rapture:Runtestbar()
          end,
         },
        scaledesc = {
          order = 9,
          type = "description",
          name = "\n\nBar options\n",
          cmdHidden = true
        },
        scale = {
          order = 10,
          name = "Scale",
          desc = "Set scale",
          type = 'range',
          min = 0.5,
          max = 2,
          step = 0.1,
          get = function(info) return db.scale end,
          set = function(info, s) 
          db.scale = s
          Rapture:Runtestbar()
          end,
        },
        width = {
          order = 11,
          name = "Width",
          desc = "Set width",
          type = 'range',
          min = 10,
          max = 600,
          step = 1,
          get = function(info) return db.width end,
          set = function(info, w) 
          db.width = w
          Rapture:Runtestbar()
          end,
        },
        height = {
          order = 12,
          name = "Height",
          desc = "Set height",
          type = 'range',
          min = 1,
          max = 60,
          step = 1,
          get = function(info) return db.height end,
          set = function(info, h) 
          db.height = h
          Rapture:Runtestbar()
          end,
        },
        texture = {
          order = 14,
          type = "select",
          name = "Texture",
          desc = "Set the statusbar texture.",
          values = rapmedia:HashTable("statusbar"),
          dialogControl = "LSM30_Statusbar",
          get = function(info) return db.texture end,
          set = function(info, text) 
          db.texture = text
          Rapture:Runtestbar()
          end,
        },
        font = {
          order = 14,
          type = "select",
          name = "Font",
          desc = "Set the font.",
          values = rapmedia:HashTable("font"),
          dialogControl = "LSM30_Font",
          get = function(info) return db.font end,
          set = function(info, f) 
          db.font = f
          Rapture:Runtestbar()
          end,
        },
        fontscale = {
          order = 15,
          name = "Font Scale",
          desc = "Set the font scale",
          type = 'range',
          min = 1,
          max = 50,
          step = 1,
          get = function(info) return db.fontscale end,
          set = function(info, fontsc) 
          db.fontscale = fontsc
          Rapture:Runtestbar()
          end,
        },
        outline = {
          order = 16,
          name = "Outline",
          desc = "Outline",
          type = 'select',
          values = {["OUTLINE"]="Outline", ["THICKOUTLINE"]="Thickoutline", ["MONOCHROME"]="Monochrome", ["NONE"]="None"},
          get = function(info) return db.outline end,
          set = function(info, line)
          db.outline = line
          Rapture:Runtestbar()
          end,
        },
--[[        borderdesc = {
          order = 16,
          type = "description",
          name = "\n\nBorder Options\n",
          cmdHidden = true
        },
        border = {
          order = 17,
          type = "select",
          name = "Border",
          desc = "Border",
          dialogControl = 'LSM30_Border',
          values = rapmedia:HashTable("border"),
          get = function(info) return db.border end,
          set = function(info, b) 
          db.border = b
          Rapture:Runbar()
          end,
        }, 
        borderSize = {
          order = 18,
          type = "range",
          name = "Bordersize",
          desc = "Border size",
          min = 4,
          max = 24,
          step = 1,
          get = function(info) return db.bordersize end,
          set = function(info, bsize) 
          db.bordersize = bsize
          Rapture:Runbar()
          end,
        },
        borderInset = {
          order = 19,
          type = "range",
          name = "Borderinset",
          desc = "Border inset",
          min = -25,
          max = 25,
          step = 1,
          get = function(info) return db.borderinset end,
          set = function(info, bins) 
          db.borderinset = bins
          Rapture:Runbar()
          end,
        },
        borderwidth = {
          order = 20,
          name = "Border width",
          desc = "Set border width",
          type = 'range',
          min = 10,
          max = 600,
          step = 1,
          get = function(info) return db.borderwidth end,
          set = function(info, bw) 
          db.borderwidth = bw
          Rapture:Runbar()
          end,
        },
        borderheight = {
          order = 21,
          name = "Border height",
          desc = "Set border height",
          type = 'range',
          min = 1,
          max = 60,
          step = 1,
          get = function(info) return db.borderheight end,
          set = function(info, bh) 
          db.borderheight = bh
          Rapture:Runbar()
          end,
        },]]
      },
    },
    colors = {
      type = "group",
      name = "Colors",
      args = {
          barcolordesc = {
          order = 1,
          type = "header",
          name = "Bar color\n",
          cmdHidden = true
        },
        barcolor = {
          order = 2,
          name = "Bar color",
          desc = "Set color",
          type = 'color',
          hasAlpha = true,
          get = function(info) return db.colorr, db.colorg, db.colorb, db.colora end,
          set = function(info, r, g, b, a)
          db.colorr, db.colorg, db.colorb, db.colora = r, g, b, a
          Rapture:Runtestbar()
          end,
        },
        barcolorrgbadesc = {
          order = 3,
          type = "description",
          name = "\n\nRGBA changer\n",
          cmdHidden = true
        },
        barR = {
          order = 4,
          name = "Red",
          desc = "Set red color",
          type = 'range',
          min = 0,
          max = 1,
          step = 0.01,
          get = function(info) return db.colorr end,
          set = function(info, r)
          db.colorr = r
          Rapture:Runtestbar()
          end,
        },
        barG = {
          order = 5,
          name = "Green",
          desc = "Set green color",
          type = 'range',
          min = 0,
          max = 1,
          step = 0.01,
          get = function(info) return db.colorg end,
          set = function(info, g)
          db.colorg = g
          Rapture:Runtestbar()
          end,
        },
        barB = {
          order = 6,
          name = "Blue",
          desc = "Set blue color",
          type = 'range',
          min = 0,
          max = 1,
          step = 0.01,
          get = function(info) return db.colorb end,
          set = function(info, b)
          db.colorb = b
          Rapture:Runtestbar()
          end,
        },
        barA = {
          order = 7,
          name = "Alpha",
          desc = "Set the alpha",
          type = 'range',
          min = 0,
          max = 1,
          step = 0.01,
          get = function(info) return db.colora end,
          set = function(info, a)
          db.colora = a
          Rapture:Runtestbar()
          end,
        },
        bgcolordesc = {
          order = 9,
          type = "header",
          name = "Bar background color\n",
          cmdHidden = true
        },
        bgcolor = {
          order = 10,
          name = "Bar background color",
          desc = "Set color",
          type = 'color',
          hasAlpha = true,
          get = function(info) return db.bgcolorr, db.bgcolorg, db.bgcolorb, db.bgcolora end,
          set = function(info, bgr, bgg, bgb, bga)
          db.bgcolorr, db.bgcolorg, db.bgcolorb, db.bgcolora = bgr, bgg, bgb, bga
          Rapture:Runtestbar()
          end,
        },
        bgcolorrgbadesc = {
          order = 11,
          type = "description",
          name = "\n\nRGBA changer\n",
          cmdHidden = true
        },
        bgR = {
          order = 12,
          name = "Red",
          desc = "Set red color",
          type = 'range',
          min = 0,
          max = 1,
          step = 0.01,
          get = function(info) return db.bgcolorr end,
          set = function(info, bgr)
          db.bgcolorr = bgr
          Rapture:Runtestbar()
          end,
        },
        bgG = {
          order = 13,
          name = "Green",
          desc = "Set green color",
          type = 'range',
          min = 0,
          max = 1,
          step = 0.01,
          get = function(info) return db.bgcolorg end,
          set = function(info, bgg)
          db.bgcolorg = bgg
          Rapture:Runtestbar()
          end,
        },
        bgB = {
          order = 14,
          name = "Blue",
          desc = "Set blue color",
          type = 'range',
          min = 0,
          max = 1,
          step = 0.01,
          get = function(info) return db.bgcolorb end,
          set = function(info, bgb)
          db.bgcolorb = bgb
          Rapture:Runtestbar()
          end,
        },
        bgA = {
          order = 15,
          name = "Alpha",
          desc = "Set the alpha",
          type = 'range',
          min = 0,
          max = 1,
          step = 0.01,
          get = function(info) return db.bgcolora end,
          set = function(info, bga)
          db.bgcolora = bga  
          Rapture:Runtestbar()
          end,
        },
      },
    },
  },
}

function Rapture:OnInitialize()
    self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("Rapture", "Rapture")
    LibStub("AceConfig-3.0"):RegisterOptionsTable("Rapture", options, {"rapture", "rap", "Rapture", "Rap"})
    LibStub("AceConfigRegistry-3.0"):RegisterOptionsTable("Rapture", options)
    self.db = LibStub("AceDB-3.0"):New("RaptureDB", defaults)
	db = self.db.profile
	options.args.profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
    self:RegisterChatCommand("rap", "ChatCommand")
    self:RegisterChatCommand("Rapture", "ChatCommand")
	self.db.RegisterCallback(self, "OnProfileChanged", "RefreshConfig")
	self.db.RegisterCallback(self, "OnProfileCopied", "RefreshConfig")
	self.db.RegisterCallback(self, "OnProfileReset", "RefreshConfig")
    function barstopped( callback, bar )
        if RaptureBAR and RaptureBAR == bar then
            RaptureBAR = nil
        end
    end
    rapcandy.RegisterCallback(self, "LibCandyBar_Stop", barstopped)
	RaptureTEST = rapcandy:New(rapmedia:Fetch("statusbar", db.texture), db.width, db.height)    
end

function Rapture:OnEnable()
    self:Print("Rapture Loaded")
    self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    if not rapmedia:Fetch("statusbar", db.texture, true) then db.texture = "Blizzard" end
    if not rapmedia:Fetch("font", db.font, true) then db.font = "Friz Quadrata TT" end
end

function Rapture:RefreshConfig()
	db = self.db.profile
end

function Rapture:Runbar()
    if not RaptureBAR then
        RaptureBAR = rapcandy:New(rapmedia:Fetch("statusbar", db.texture), db.width, db.height)
        RaptureBAR:Set("none", none)
        RaptureBAR:SetTexture(rapmedia:Fetch("statusbar", db.texture))
        RaptureBAR:SetWidth(db.width)
        RaptureBAR:SetHeight(db.height)
        RaptureBAR:SetScale(db.scale)
        RaptureBAR:SetPoint("CENTER", UIParent, "CENTER", db.xpos, db.ypos)
        RaptureBAR:SetColor(db.colorr, db.colorg, db.colorb, db.colora)
        RaptureBAR.candyBarLabel:SetFont(rapmedia:Fetch("font", db.font), db.fontscale, db.outline~="NONE" and db.outline or nil)
        RaptureBAR.candyBarDuration:SetFont(rapmedia:Fetch("font", db.font), db.fontscale, db.outline~="NONE" and db.outline or nil)
        RaptureBAR.candyBarBackground:SetVertexColor(db.bgcolorr, db.bgcolorg, db.bgcolorb, db.bgcolora)
        RaptureBAR.candyBarLabel:SetJustifyH("LEFT")
        RaptureBAR:SetLabel("Rapture Cooldown")
    end
    RaptureBAR:SetDuration(12)
    RaptureBAR:Start()
end

function Rapture:Runtestbar()
    RaptureTEST:Set("none", none)
    RaptureTEST:SetTexture(rapmedia:Fetch("statusbar", db.texture))
    RaptureTEST:SetWidth(db.width)
    RaptureTEST:SetHeight(db.height)
    RaptureTEST:SetScale(db.scale)
    RaptureTEST:SetPoint("CENTER", UIParent, "CENTER", db.xpos, db.ypos)
    RaptureTEST:SetColor(db.colorr, db.colorg, db.colorb, db.colora)
    RaptureTEST.candyBarLabel:SetFont(rapmedia:Fetch("font", db.font), db.fontscale, db.outline~="NONE" and db.outline or nil)
    RaptureTEST.candyBarDuration:SetFont(rapmedia:Fetch("font", db.font), db.fontscale, db.outline~="NONE" and db.outline or nil)
    RaptureTEST.candyBarBackground:SetVertexColor(db.bgcolorr, db.bgcolorg, db.bgcolorb, db.bgcolora)
    RaptureTEST.candyBarLabel:SetJustifyH("LEFT")
    RaptureTEST:SetLabel("Rapture Cooldown")
	RaptureTEST:SetDuration(2)
	RaptureTEST:Start()
	if RaptureBAR then
	RaptureBAR:Stop()
	end
end

function Rapture:SPELL_ENERGIZE(timestamp, event, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, spellId, spellName, spellSchool, auraType, amount)
    local player_name = UnitName("player")
    if destName == player_name and sourceName == player_name and spellId == 47755 then
        Rapture:Runbar()
    end
end

function Rapture:COMBAT_LOG_EVENT_UNFILTERED(_, timestamp, event, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, amount, spellID, ...)
    local func = self[event]
    if (func) then
        func(self, timestamp, event, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, amount, spellID, ...)
    end
end

function Rapture:ChatCommand(input)
    if not input or input:trim() == "" then
        InterfaceOptionsFrame_OpenToCategory(self.optionsFrame)
    else
        LibStub("AceConfigCmd-3.0").HandleCommand(Rapture, "rap", "Rapture", input)
    end
end
