local PCVM = LibStub("AceAddon-3.0"):NewAddon("PCValkMon", "AceConsole-3.0", "AceTimer-3.0", "AceEvent-3.0");

local version = {major=1, minor=13};
local barCache = {};
local trackingLookup = {};
local health = {};
local maxHealth = {};
local drawFrame;
local tracking = {};
local nameCache = {};
local currentTrackIndex = 1;
local currentVehicleIndex = 1;
local anchor;
local opt;
local inVehicle = {};
local baseColor = {r = 0.2, g = 0.7, b = 0.0};
local redIncrement = (0.7 - baseColor.r) / 50;
local greenDecrement = baseColor.g / 50;
local phase = 1;
local isDragging = false;
local activePhase = 2;
local currentRoster = {};
local simulating = false;
local maxBars = 3;
local elapsed = 0;
local roundIndex = 1;
local switchTimes = {};
local currentSpawnTime;
local onDR = {};
local stunNames = {
  "Bash", "Concussion Blow", "Deep Freeze", "Demon Charge", "Gnaw",
  "Holy Wrath", "Infero Effect", "Hammer of Justice", "Intercept",
  "Intimidation", "Kidney Shot", "Maim", "Ravage", "Shadowfury",
  "Shockwave", "Sonic Blast", "War Stomp", "Cheap Shot"
};
local stunIDs = {
  8983, -- Bash
  12809, -- Concussion Blow
  44572, -- Deep Freeze
  54785, -- Demon Charge
  47481, -- Gnaw
  48817, -- Holy Wrath
  22703, -- Inferno Effect
  10308, -- Hammer of Justice
  20252, -- Intercept (Warrior)
  30197, -- Intercept (Felguard)
  19577, -- Intimidation
  8643, -- Kidney Shot
  49802, -- Maim
  53562, -- Ravage (pet)
  47847, -- Shadowfury
  46968, -- Shockwave
  53568, -- Sonic Blast (pet)
  20549, -- War Stomp
  1833, -- Cheap Shot
};
local slowNames = {
  "Concussive Shot", "Wing Clip", "Frost Trap", "Frostbolt", "Slow", "Cone of Cold",
  "Mind Flay", "Crippling Poison", "Deadly Throw", "Frost Shock", "Earthbind",
  "Piercing Howl", "Hamstring", "Infected Wounds", "Desecration"
};
local slowIDs = {
  5116, -- Concussive Shot
  2974, -- Wing Clip
  13809, -- Frost Trap
  42842, -- Frostbolt
  31589, -- Slow
  42931, -- Cone of Cold
  48156, -- Mind Flay
  30981, -- Crippling Poison
  48674, -- Deadly Throw
  49236, -- Frost Shock
  3600, -- Earthbind (totem)
  12323, -- Piercing Howl
  1715, -- Hamstring
  58181, -- Infected Wounds
  68766, -- Desecration
};

local stuns = {};
local slows = {};
local stunSpells = {};
local slowSpells = {};
local activeStuns = {};
local damageTracker = {};
local vehicleNames = {};
local running = false;
local transitionSpellIDs = {
  68981, 74270, 74271, 74272, 68981, 74270, 74271, 74272
};
local transitionSpells = {};

--*********************************************************
--Initialization for the mod
--*********************************************************
function PCVM:OnInitialize()
  PCVM:Print("v"..version.major.."."..version.minor.." Loaded - /pcvm to position");

  drawFrame = CreateFrame("Frame", "PCVMRoot", UIParent);
  drawFrame:ClearAllPoints();
  drawFrame:SetAllPoints(UIParent);
  drawFrame:SetPoint("BOTTOMLEFT", 0, 0);
  drawFrame:SetFrameStrata("LOW");
  drawFrame:SetScript("OnUpdate", function(self, delta) PCVM:OnUpdate(delta); end);

  PCVM:RegisterChatCommand("pcvm", "InputOptions");
  PCVM:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
  PCVM:RegisterEvent("PLAYER_REGEN_DISABLED");
  PCVM:RegisterEvent("PLAYER_REGEN_ENABLED", "CheckForWipe");
  PCVM:RegisterEvent("CHAT_MSG_MONSTER_YELL");  

  anchor = PCVM:CreateAnchor();
  opt = PCVM:CreateStunOptions();

  for i = 1, #stunNames do
    stuns[stunNames[i]] = true;
  end
  for i = 1, #slowNames do
    slows[slowNames[i]] = true;
  end
  for i = 1, #transitionSpellIDs do 
    transitionSpells[transitionSpellIDs[i]] = true;
  end
  for i = 1, #stunIDs do
    stunSpells[stunIDs[i]] = true;
  end
  for i = 1, #slowIDs do
    slowSpells[slowIDs[i]] = true;
  end
end

--*********************************************************
--*********************************************************
function PCVM:OnEnable()
end

--*********************************************************
--*********************************************************
function PCVM:BeginCombat()
  if simulating then
    return;
  end
  if anchor then
    anchor:Hide();
  end
  phase = 1;
  roundIndex = 1;
  PCVM:ResetMonitor();
  PCVM:CreateRoster();
end

--*********************************************************
--*********************************************************
function PCVM:CHAT_MSG_MONSTER_YELL(_, msg)
  if msg:find("I'll keep you alive to witness the end, Fordring.") then
    PCVM:BeginCombat();
    PCVM:Print("The Lich King engaged!");
    running = true;
  end
end

--*********************************************************
--*********************************************************
local toScan = {"target", "targettarget", "focus", "focustarget", "mouseover", "mouseovertarget"};
for i = 1, 40 do
  toScan[#toScan+1] = ("raid%dtarget"):format(i);
end

--*********************************************************
--*********************************************************
local function scan()
  for _, unit in next, toScan do
    if UnitExists(unit) and tonumber(UnitGUID(unit):sub(-12, -7), 16) == 36597 and UnitAffectingCombat(unit) then
      return unit;
    end
  end
end

--*********************************************************
--*********************************************************
function PCVM:CheckForWipe()
  if not running then 
    return;
  end
  if not scan() then
    PCVM:EndCombat();
    running = false;
  else
    PCVM:ScheduleTimer("CheckForWipe", 2);
  end
end

--*********************************************************
--*********************************************************
function PCVM:EndCombat()
  if simulating then
    return;
  end
  if anchor then
    PCVM:Print("Combat ended");
    anchor:Hide();
  end
end

--*********************************************************
--Create the options dialog
--*********************************************************
function PCVM:CreateStunOptions()
  local f = CreateFrame("Frame", "PCVMStunOptions", UIParent, "PCStunOptions");
  f:Hide();
  return f;
end

--*********************************************************
--Parse input to the command interface
--*********************************************************
function PCVM:InputOptions(input)
  local c = input;
  if c then
    c = string.lower(c);
  end
  if c == "sim" then
    simulating = true;
    PCVM:ResetMonitor();
    PCVM:CreateRoster();
    PCVM:Print("Simulation enabled - Moonfire to add bars");
  elseif c == "reset" then
    if anchor then
      anchor:SetPoint("TOPLEFT", UIParent, "TOPLEFT", 0, 0);
      PCVM:Print("Resetting anchor");
      anchor:Show();
      isDragging = true;
    end
  else
    PCVM:ToggleDisplay(anchor);
  end
end

--*********************************************************
--Toggle a display frame
--*********************************************************
function PCVM:ToggleDisplay(f)
  if f then
    if f:IsShown() then
      f:Hide();
      isDragging = false;
    else
      isDragging = true;
      f:Show();
    end
  end
end

--*********************************************************
--Create a roster for the current play environment.
--Really only useful so you can test the bar functionality
--without being in 25 raid dungeon =)
--*********************************************************
function PCVM:CreateRoster()
  local prefix = "player";
  local alone = true;
  local cap = 0;
  if GetNumRaidMembers() > 0 then
    prefix = "raid";
    alone = false;
    cap = GetNumRaidMembers();
  elseif GetNumPartyMembers() > 0 then
    prefix = "party";
    alone = false;
    cap = GetNumPartyMembers();
  else
    currentRoster = {"player"};
  end

  if not alone then
    currentRoster = {};
    for i = 1, cap do
      currentRoster[i] = (prefix .. i);
    end
    if prefix == "party" then
      currentRoster[cap + 1] = "player";
    end
  end
end

--*********************************************************
--Get the best stun to display for the given unit
--*********************************************************
function PCVM:GetActiveStun(unit)
  local ex = 0;
  local bestStun = nil;
  local isSlowed = nil;
  for i = 1, 40 do
    local n, _, t, _, _, _, e, _, _, _, id = UnitDebuff(unit, i);
    if slows[n] or slowSpells[id] then
      isSlowed = t;
    end
    if (stuns[n] or stunSpells[id]) and e > ex then
      bestStun = {texture = t, expires = e, name = n, id = id;};
      ex = e;
    end
  end
  return bestStun, isSlowed;
end

--*********************************************************
--Update the health bars.
--If someone is targeting a mob being tracked, get the
--health value directly. Otherwise, we'll estimate it based
--on the combat log.
--*********************************************************
function PCVM:OnUpdate(delta)
  elapsed = elapsed + delta;
  if elapsed < 0.025 then
    return;
  end

  elapsed = 0;

  if phase ~= activePhase then
    if anchor and anchor:IsShown() and not isDragging then
      anchor:Hide();
    end
    return;
  end

  local populatedBars = {};
  for i = 1, #currentRoster do
    local tguid = UnitGUID(currentRoster[i].."target");
    if tguid then
      local tindex = tracking[tguid];
      if tindex then
	if not switchTimes[UnitName(currentRoster[i])] then
	  switchTimes[UnitName(currentRoster[i])] = GetTime();
	end
	local b = barCache[tindex];
	if not populatedBars[b] then
	  --populatedBars[b] = true;
	  local activeStun, isSlowed = PCVM:GetActiveStun(currentRoster[i].."target");
	  if activeStun and (activeStun.name ~= "Cheap Shot" and activeStun.id ~= 1833) then
	    b.seenStun = true;
	    b.DR = nil;
	  else
	    if b.seenStun and b.DR == nil then
	      b.DR = GetTime();
	    end
	  end

	  if b.DR and (GetTime() - b.DR > 15) then
	    b.DR = nil;
	    b.seenStun = false;
	  end

	  PCVM:SetStun(b, activeStun, isSlowed);
	  PCVM:SetBarIcon(b, GetRaidTargetIndex(currentRoster[i].."target"));
	  if damageTracker[tguid] then
	    damageTracker[tguid].icon = GetRaidTargetIndex(currentRoster[i].."target");
	  end
	  health[tguid] = UnitHealth(currentRoster[i].."target");
	  maxHealth[tguid] = UnitHealthMax(currentRoster[i].."target");
	end
      end
    end
    if UnitInVehicle(currentRoster[i]) and currentVehicleIndex <= maxBars and not inVehicle[i] then
      tguid = trackingLookup[currentVehicleIndex];
      if tguid then
	nameCache[tguid] = UnitName(currentRoster[i]);
	inVehicle[i] = true;
	vehicleNames[currentVehicleIndex] = UnitName(currentRoster[i]);
	currentVehicleIndex = currentVehicleIndex + 1;
      end
    end
  end

  for bb = 1, maxBars do
    local tguid = trackingLookup[bb];
    if tguid and health[tguid] and maxHealth[tguid] then
      local healthPercent = (health[tguid] / maxHealth[tguid]) * 100;
      PCVM:SetBarPercent(barCache[bb], healthPercent, nameCache[tguid]);
    end
  end
end

--*********************************************************
--Attach a stun to a bar
--*********************************************************
function PCVM:SetStun(b, stun, slow)
  local stunIcon = _G[b:GetName().."BarBorderStunIcon"];
  local slowIcon = _G[b:GetName().."BarSlowIcon"];
  local stunIconParent = _G[b:GetName().."BarBorder"];
  local DRIcon = _G[b:GetName().."BarBorderDRIcon"];
  if stun then
    local stunIcon = _G[b:GetName().."BarBorderStunIcon"];
    if not b.currentStun or b.currentStun.name ~= stun.name or b.currentStun.expires ~= stun.expires then
      b.currentStun = stun;
      b.currentStun.applied = GetTime();
      stunIcon:SetTexture(stun.texture);
      stunIcon:Show();
      stunIcon:SetPoint("RIGHT", stunIconParent, "RIGHT", 0, 0);
      if b.seenStunIndex < 6 then
	local seenStun = _G[b:GetName().."BarBorderUsedStun"..b.seenStunIndex];
	b.seenStunIndex = b.seenStunIndex + 1;
	seenStun:SetTexture(stun.texture);
	seenStun:Show();
      end
    else
      local e = GetTime() - b.currentStun.applied;
      local d = b.currentStun.expires - b.currentStun.applied;
      local r = e / d;
      local x = 175 * r;
      stunIcon:SetPoint("RIGHT", stunIconParent, "RIGHT", -x, 0);
    end
  else
    stunIcon:SetTexture(nil);
  end

  if b.DR then
    local e = GetTime() - b.DR;
    local r = e / 15;
    local x = 190 * r;
    DRIcon:Show();
    DRIcon:SetPoint("RIGHT", stunIconParent, "RIGHT", -x, 0);
  else
    DRIcon:Hide();
  end

  if slow then
    slowIcon:SetTexture(slow);
  else
    slowIcon:SetTexture(nil);
  end
end

--*********************************************************
--Add damage dealt to a tracked mob
--*********************************************************
function PCVM:AddDamage(destGUID, srcName, damage)
  if health[destGUID] then
    health[destGUID] = health[destGUID] - damage;
    if not damageTracker[destGUID] then
      damageTracker[destGUID] = {};
      damageTracker[destGUID].damage = 0;
      damageTracker[destGUID].icon = 1;
      damageTracker[destGUID].sources = {};
    end
    if not damageTracker[destGUID].sources[srcName] then
      damageTracker[destGUID].sources[srcName] = 0;
    end
    if not damageTracker.sources then
      damageTracker.sources = {};
    end
    if not damageTracker.sources[srcName] then
      damageTracker.sources[srcName] = 0;
    end
    if not damageTracker.damage then
      damageTracker.damage = 0;
    end
    damageTracker.damage = damageTracker.damage + damage;
    damageTracker.sources[srcName] = damageTracker.sources[srcName] + damage;
    damageTracker[destGUID].damage = damageTracker[destGUID].damage + damage;
    damageTracker[destGUID].sources[srcName] = damageTracker[destGUID].sources[srcName] + damage;
  end
end

--*********************************************************
--Reset the Valk monitor
--*********************************************************
function PCVM:ResetMonitor()
  tracking = {};
  trackingLookup = {};
  currentTrackIndex = 1;
  currentVehicleIndex = 1;
  health = {};
  maxHealth = {};
  nameCache = {};
  inVehicle = {};
  damageTracker = {};
  switchTimes = {};
  vehicleNames = {};
  for bb = 1, maxBars do
    barCache[bb].seenStun = false;
    barCache[bb].DR = nil;
    barCache[bb].seenStunIndex = 1;
    for seenStuns = 1, 5 do
      local ss = _G[barCache[bb]:GetName().."BarBorderUsedStun"..seenStuns];
      ss:Hide();
    end
  end
  if simulating then
    phase = activePhase;
  end
end

--*********************************************************
--Combat begins...anything special here?
--*********************************************************
function PCVM:PLAYER_REGEN_DISABLED()
end

--*********************************************************
--Determine if an event should cause a bar proc.
--If we're doing a simulation, this is a moonfire event..
--Otherwise, it's when the LK summons a Valk
--*********************************************************
function PCVM:IsBarProc(subevent, spellName, spellID)
  if simulating then
    if subevent == "SPELL_DAMAGE" and spellName == "Moonfire" then
      return true;
    end
    return false;
  end
  if subevent == "SPELL_SUMMON" and spellID == 69037 then
    return true;
  end
  return false;
end

--*********************************************************
--First round of localization - try it in English first,
--then use the spellID. This will eventually be reworked.
--*********************************************************
function PCVM:IsTransitionProc(subevent, spellName, spellID)
  if spellName and spellName == "Remorseless Winter" and subevent == "SPELL_CAST_START" then
    return true;
  end
  if spellID and transitionSpells[spellID] and subevent == "SPELL_CAST_START" then
    return true;
  end
  return false;
end

--*********************************************************
--Track damage dealth, units dying, and the summon
--event itself. We're only interested in tracking mobs
--which are summoned during P2
--*********************************************************
function PCVM:COMBAT_LOG_EVENT_UNFILTERED(eventName, timestamp, subevent, ...)
  local srcGUID, srcName, srcFlags = select(1, ...);
  local destGUID, destName, destFlags = select(4, ...);
  local spellID, spellName = select(7, ...);

  local tguid = destGUID;

  if PCVM:IsTransitionProc(subevent, spellName, spellID) then
    phase = phase + 1;
    PCVM:Print("Entering phase: "..phase);
  end

  if phase ~= activePhase then
    return;
  end

  if subevent == "SPELL_DAMAGE" then
    local damage = select(10, ...);
    PCVM:AddDamage(tguid, srcName, damage);
  elseif subevent == "SPELL_PERIODIC_DAMAGE" then
    local damage = select(10, ...);
    PCVM:AddDamage(tguid, srcName, damage);
  elseif subevent == "SPELL_DRAIN" then
    local damage = select(10, ...);
    PCVM:AddDamage(tguid, srcName, damage);
  elseif subevent == "SPELL_LEECH" then
    local damage = select(10, ...);
    PCVM:AddDamage(tguid, srcName, damage);
  elseif subevent == "SWING_DAMAGE" then
    local damage = select(7, ...);
    PCVM:AddDamage(tguid, srcName, damage);
  elseif subevent == "RANGE_DAMAGE" then
    local damage = select(10, ...);
    PCVM:AddDamage(tguid, srcName, damage);
  end

  if subevent == "UNIT_DIED" then
    if tracking[tguid] then
      barCache[tracking[tguid]]:Hide();
    end
  end

  if PCVM:IsBarProc(subevent, spellName, spellID) then
    if anchor then
      anchor:Show();
      anchor:EnableMouse(false);
    end
    if currentTrackIndex == (maxBars + 1) then
      for i = 1, maxBars do
	barCache[i]:Hide();
      end
      PCVM:ResetMonitor();
    end
    tracking[tguid] = currentTrackIndex;
    barCache[currentTrackIndex]:Show();
    trackingLookup[currentTrackIndex] = tguid;
    nameCache[tguid] = destName;
    currentTrackIndex = currentTrackIndex + 1;
    if currentTrackIndex == 2 and UnitName("player") == "Thomphoolery" then
      currentSpawnTime = GetTime();
      PCVM:ScheduleTimer("DumpDamageTracker", 38);
    end
  end
end

--*********************************************************
--*********************************************************
function PCVM:DumpDamageTracker()
  SendChatMessage("--ROUND "..roundIndex.."---------------------", "CHANNEL", nil, GetChannelName("valkstats"));

  roundIndex = roundIndex + 1;
  local a = {};
  if not damageTracker or not damageTracker.sources then
    return;
  end
  for n in pairs(damageTracker.sources) do
    table.insert(a, n);
  end
  table.sort(a, function(n1, n2)
    return damageTracker.sources[n1] > damageTracker.sources[n2];
  end);
  for i, n in ipairs(a) do
    local reaction = "N/A";
    if switchTimes[n] then
      reaction = math.floor(switchTimes[n] - currentSpawnTime);
    end
    SendChatMessage(i..": "..n.." ("..damageTracker.sources[n]..", "..math.floor((damageTracker.sources[n]/damageTracker.damage)*100).."%) ("..reaction.."s)", "CHANNEL", nil, GetChannelName("valkstats"));
  end
end

--*********************************************************
--Create the anchor window and 3 trackable health bars
--*********************************************************
function PCVM:CreateAnchor()
  local f = CreateFrame("Frame", "PCVMAnchor", UIParent, "PCValkAnchor");
  f:SetScript("OnMouseDown", function()
    if arg1 == "LeftButton" then
      this:StartMoving();
    end
  end);
  f:SetScript("OnMouseUp", function()
    if arg1 == "LeftButton" then
      this:StopMovingOrSizing();
    end
  end);
  for i = 1, maxBars do
    local bar = PCVM:CreateHealthBar(f, i);
    PCVM:SetBarPercent(bar, i*25);
    bar:Hide();
  end
  f:Hide();
  return f;
end

--*********************************************************
--Set the icon for one health bar
--*********************************************************
function PCVM:SetBarIcon(b, icon)
  if b then
    local barIcon = _G[b:GetName().."BarIcon"];
    if icon then
      barIcon:SetTexture("Interface\\TARGETINGFRAME\\UI-RaidTargetingIcon_"..icon);
    else
      barIcon:SetTexture(nil);
    end
  end
end

--*********************************************************
--Set the health percent for one health bar
--*********************************************************
function PCVM:SetBarPercent(b, p, t, pt)
  local barStatus = _G[b:GetName().."Bar"];
  local barName = _G[b:GetName().."BarName"];
  local barTimer = _G[b:GetName().."BarTimer"];
  p = math.floor(p);

  local red = baseColor.r + (redIncrement * (100-p));
  local green = baseColor.g - (greenDecrement * (100-p));
  local blue = baseColor.b;
  if t then
    local lc, ec = UnitClass(t);
    if ec == "WARLOCK" then
      red = 0.58;
      green = 0.51;
      blue = 0.79;
    end
  end

  barStatus:SetStatusBarColor(red, green, blue);
  barTimer:SetText(p.."%");
  barName:SetText(t);
  barStatus:SetValue(p);
end

--*********************************************************
--Create a single health bar
--*********************************************************
function PCVM:CreateHealthBar(anchor, index)
  local index = #barCache;
  index = index + 1;
  local b = CreateFrame("Frame", "PCVMBar"..index, anchor, "PCValkMonBar");
  local barIcon = _G[b:GetName().."BarIcon"];
  barIcon:SetTexture(nil);
  b:ClearAllPoints();
  --b:SetAllPoints(anchor);
  b:SetWidth(200);
  b:SetHeight(20);
  b:SetPoint("TOPLEFT", anchor, "TOPLEFT", 70, -((index-1) * 35) - 5);
  b:Show();
  table.insert(barCache, index, b);
  b.seenStun = false;
  return b;
end

