--[[
Finds Speech bubbles that have items in them then gives them correct color codes as they
were displayed originally when Speech Bubbles were added.
]]--

local select = select
local GetCVar = GetCVar

local message
local pattern = "|c.-|h%[(.-)%]|h|r"


local function onUpdate(self)
	self.updates = self.updates + 1
	-- seems to take one update for the text to properly appear in a chat bubble
	if message and (self.updates > 1 or self:Scan(message)) then
		message = nil
		self.updates = 0
		self:SetScript("OnUpdate", nil)
	end
end

local addon = CreateFrame("Frame")
addon:RegisterEvent("CHAT_MSG_SAY")
addon:RegisterEvent("CHAT_MSG_YELL")
addon:RegisterEvent("CHAT_MSG_PARTY")
addon:RegisterEvent("CHAT_MSG_PARTY_LEADER")
addon:SetScript("OnEvent", function(self, event, msg)
	local partyEvent = event == "CHAT_MSG_PARTY" or event == "CHAT_MSG_PARTY_LEADER"
	if (partyEvent and GetCVar("ChatBubblesParty")) or (not partyEvent and GetCVar("ChatBubbles")) then
		message = msg
		self:SetScript("OnUpdate", onUpdate)
	end
end)

addon.updates = 0


function addon:Scan(msg)
	if msg:find(pattern) then
		local plainMsg, numLinks = msg:gsub(pattern, "%1")
		
		for i = 1, WorldFrame:GetNumChildren() do
			local child = select(i, WorldFrame:GetChildren())
			for j = 1, child:GetNumRegions() do
				local region = select(j, child:GetRegions())
				if region and not region:GetName() and region:IsVisible() and region.GetText and region:GetText() == plainMsg then
					local oldTextWidth = region:GetStringWidth()
					local oldHeight = region:GetHeight()
					region:SetText(msg)
					local bracketWidth = (region:GetStringWidth() - oldTextWidth) / numLinks / 2 -- how much width we need to add to the bubble to compensate for the added brackets

					-- add one bracket width until the bubble to longer has to create an extra line
					while region:GetHeight() > oldHeight do
						region:SetWidth(region:GetWidth() + bracketWidth)
					end
					return true
				end
			end
		end
	else
		return true
	end
end