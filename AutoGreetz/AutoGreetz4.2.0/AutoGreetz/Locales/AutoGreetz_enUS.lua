local L = LibStub("AceLocale-3.0"):NewLocale("AutoGreetz", "enUS")
if not L then return end

L["Loaded"] = "AutoGreetz loaded."

L["autogreetz"] = true
L["agr"] = true

L["Delay"] = "Delay"
L["Delay Desc"] = "The time(in seconds) it takes for the addon to reply"

L["Greet Message Label"] = "Greeting Message"
L["Greet Message Desc"] = "The message, that will be shown, when a guild member comes online."
L["Greet Message Usage"] = "Use '#n' for the player's name, use ';' to seperate messages"
L["Greet Message"] = "Hello #n"

L["WB Message Label"] = "WB Message"
L["WB Message Desc"] = "The message, that will be shown, when a guild member relogs within 15 minutes."
L["WB Message Usage"] = "Use '#n' for the player's name, use ';' to seperate messages"
L["WB Message"] = "Welcome back, #n"

L["Run"] = "Activate addon?"

L["UseWB"] = "Send WB?"
L["UseWB Desc"] = "Do you want to send a message, defined in WB Message, if a member relogs within 15 minutes? If disabled, no message will be sent."

L["Afk"] = "Run When AFK"
L["Afk Desc"] = "Toggles if the addon will run when you are afk"

L["Realname"] = "Use Realname"
L["Realname Desc"] = "If activated, use (if avaiable) the real name from the guild note of the player. This requires, that the name is entered between brackets, i.e. Note: (Michael)"

L["config"] = "Configuration"
L["config_desc"] = "Open configuration dialog."