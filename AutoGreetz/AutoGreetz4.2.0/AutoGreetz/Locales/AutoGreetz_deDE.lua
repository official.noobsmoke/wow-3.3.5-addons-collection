local L = LibStub("AceLocale-3.0"):NewLocale("AutoGreetz", "deDE")
if not L then return end

L["Loaded"] = "AutoGreetz geladen."

L["autogreetz"] = true
L["agr"] = true

L["Delay"] = "Verz\195\182gerung"
L["Delay Desc"] = "Die Zeit (in Sekunden) die das Addon braucht, um zu antworten."

L["Greet Message Label"] = "Begr\195\188ssungsnachricht"
L["Greet Message Desc"] = "Die Nachricht, die angezeigt wird, wenn ein Gildenmitglied online kommt."
L["Greet Message Usage"] = "Nutze '#n' f\195\188r den Spielernamen, nutze ';' um Nachrichten zu trennen"
L["Greet Message"] = "Hallo #n"

L["WB Message Label"] = "WB Nachricht"
L["WB Message Desc"] = "Die Nachricht, die angezeigt wird, wenn sich ein Gildenmitglied innerhalb von 15 Minuten neu einloggt."
L["WB Message Usage"] = "Nutze '#n' f\195\188r den Spielernamen, nutze ';' um Nachrichten zu trennen"
L["WB Message"] = "Willkommen zur\195\188ck, #n"

L["Run"] = "Addon aktivieren?"

L["UseWB"] = "WB senden?"
L["UseWB Desc"] = "Soll eine der in WB Nachricht definierten Nachrichten gesendet werden, wenn sich ein Gildenmitglied innerhalb von 15 Minuten neu einloggt? Wenn deaktiviert, wird keine Nachricht gesendet."

L["Afk"] = "Ausf\195\188hren wenn AFK"
L["Afk Desc"] = "Aktivieren, wenn das Addon auch dann ausgef\195\188hrt werden soll, w\195\164hrend du AFK bist"

L["Realname"] = "Realname nutzen"
L["Realname Desc"] = "Wenn aktiviert, wird (falls vorhanden) der Realname aus der Gildennotiz des Spielers genutzt. Dazu muss der Name in Klammern eingetragen sein, z.B. Notiz: (Michael)"

L["config"] = "Konfiguration"
L["config_desc"] = "Konfigurationsdialog \195\182ffnen."