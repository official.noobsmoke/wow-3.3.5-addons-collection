------------------------
---      Module      ---
------------------------

AutoGreetz  = LibStub("AceAddon-3.0"):NewAddon("AutoGreetz",
												"AceConsole-3.0",
												"AceEvent-3.0",
												"AceTimer-3.0")

----------------------------
--      Localization      --
----------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("AutoGreetz")

local Options = {
	type = "group",
	handler = AutoGreetz,
	get = function(item) return AutoGreetz.db.profile[item[#item]] end,
	set = function(item, value) AutoGreetz.db.profile[item[#item]] = value end,	
	args = {
		Run = {
			type = "toggle",
			name = L["Run"],
			order = 1,
			width = "normal",			
		},
		UseWB = {
			type = "toggle",
			name = L["UseWB"],
			desc = L["UseWB Desc"],			
			order = 1,
			width = "normal",			
		},		
		spacer1 = {
			order	= 2,
			type	= "description",
			name	= "\n",
		},	
		GreetMessage = {
			type = "input",
			name = L["Greet Message Label"],
			desc = L["Greet Message Desc"],
			usage = L["Greet Message Usage"],
			order = 3,
			width = "full",
			get = "GetGreetMessage",
			set = "SetGreetMessage",
		},
		WBMessage = {
			type = "input",
			name = L["WB Message Label"],
			desc = L["WB Message Desc"],
			usage = L["WB Message Usage"],
			order = 3,
			width = "full",
			get = "GetWBMessage",
			set = "SetWBMessage",
		},		
		Delay = {
			type = "range",
			name = L["Delay"],
			desc = L["Delay Desc"],
			order = 4,
			min = 0,
			max = 30,
			step = 1,
			set = "SetDelay",
		},	
		spacer2 = {
			order	= 5,
			type	= "description",
			name	= "\n",
		},		
		Afk = {
			type = "toggle",
			name = L["Afk"],
			desc = L["Afk Desc"],
			order = 6,
			width = "normal",
		},
		RealName = {
			type = "toggle",
			name = L["Realname"],
			desc = L["Realname Desc"],
			order = 6,
			width = "normal",
		},		
	},
}

local Defaults = {
	profile =  {
		Run = true,
		UseWB = true,
		Delay = 5,
		GreetMessage = {L["Greet Message"]},
		WBMessage = {L["WB Message"]},
		Afk = false,
		RealName = false,
		lastlogout = {}
	},	
}

----------------------
---    Commands    ---
----------------------

local SlashOptions = {
	type = "group",
	handler = AutoGreetz,
	get = function(item) return AutoGreetz.db.profile[item[#item]] end,
	set = function(item, value) AutoGreetz.db.profile[item[#item]] = value end,
	args = {
		config = {
			type = "execute",
			name = L["config"],
			desc = L["config_desc"],
			func = function()
				InterfaceOptionsFrame_OpenToCategory(AutoGreetz.optionFrames.main)
			end,
		},
	},
}

local SlashCmds = {
	"agr",
	"autogreetz",
}

----------------------
---      Init      ---
----------------------

function AutoGreetz:OnInitialize()
	-- Load our database.
	self.db = LibStub("AceDB-3.0"):New("AutoGreetzDB", Defaults, "Default")
	
	if not self.db then
		self:Print("Error: Database not loaded correctly. Please exit out of WoW and delete the AG database file (AutoGreetz.lua) found in: \\World of Warcraft\\WTF\\Account\\<Account Name>>\\SavedVariables\\")
		return
	end		

	-- Set up our config options.
	local profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
  
	local config = LibStub("AceConfig-3.0")
	config:RegisterOptionsTable("AutoGreetz", SlashOptions, SlashCmds)

	local registry = LibStub("AceConfigRegistry-3.0")
	registry:RegisterOptionsTable("AutoGreetz Options", Options)
	registry:RegisterOptionsTable("AutoGreetz Profiles", profiles)

	local dialog = LibStub("AceConfigDialog-3.0")
	self.optionFrames = {
		main = dialog:AddToBlizOptions("AutoGreetz Options", "AutoGreetz"),
		profiles = dialog:AddToBlizOptions("AutoGreetz Profiles", "Profiles", "AutoGreetz")
	}
	
	print(L["Loaded"])	
end

function AutoGreetz:OnEnable()
	self:RegisterEvent("CHAT_MSG_SYSTEM", "HandleGreeting")
end

--------------------------------
---      Event Handlers      ---
--------------------------------

function AutoGreetz:HandleGreeting(event, msg)
	-- Stupid, but working :p
	local _, _, onplayer = string.find(msg, ".*%[(.+)%]%S*"..string.sub(ERR_FRIEND_ONLINE_SS, 20))
	local _, _, offplayer = string.find(msg, string.gsub(ERR_FRIEND_OFFLINE_S, "%%s", "(.+)"))
	local name, _, _, _, _, _, note = {}
	local greetagain = time()-900
	
	if self.db.profile.Run and IsInGuild() then
	
		GuildRoster()
	
		if onplayer ~= nil and (self.db.profile.Afk or UnitIsAFK("player") == nil) then
			for i=1, GetNumGuildMembers(true) do
				name, _, _, _, _, _, note = GetGuildRosterInfo(i)
				
				if onplayer == name then
					if self.db.profile.RealName and note ~= "" then
						match = string.match(note, "%((.+)%)")
						if match ~= nil then
							name = match
						end
					end
					
					-- Greet again, if the player has been last seen 15 minutes ago or longer
					if self.db.profile.lastlogout[onplayer] == nil or self.db.profile.lastlogout[onplayer] <= greetagain then
						self.Handle = self:ScheduleTimer("SendGreeting", self.db.profile.Delay, {name, 1})
					-- Send "Welcome Back", if the player has been last seen 15 minutes ago or earlier
					elseif self.db.profile.lastlogout[onplayer] > greetagain and self.db.profile.UseWB then
						self.Handle = self:ScheduleTimer("SendGreeting", self.db.profile.Delay, {name, 2})						
					end
				end
				
			end
		elseif offplayer ~= nil then
			self.db.profile.lastlogout[offplayer] = time()
		end
	end
end

---------------------------
---      Functions      ---
---------------------------

function AutoGreetz:SendGreeting(Table)

	self.message = "x"

	if Table[2] == 1 then
		self.message = string.gsub(self.db.profile.GreetMessage[random(1,#(self.db.profile.GreetMessage))], "#n", Table[1])
	elseif Table[2] == 2 then
		self.message = string.gsub(self.db.profile.WBMessage[random(1,#(self.db.profile.WBMessage))], "#n", Table[1])
	else
		self.message = string.gsub(self.db.profile.GreetMessage[random(1,#(self.db.profile.GreetMessage))], "#n", Table[1])
	end
	
	SendChatMessage(self.message, "GUILD", nil)
end

---------------------------
---     Subfunctions    ---
---------------------------

function AutoGreetz:SetDelay(info, newValue)
	if newValue == 0 then
		self.db.profile.Delay = 0.1
	else
		self.db.profile.Delay = newValue
	end
end

function AutoGreetz:GetGreetMessage(info)
	self.string = ""
	for index,value in ipairs(self.db.profile.GreetMessage) do 
		self.string = self.string..value..";"
	end
	return self.string
end

function AutoGreetz:SetGreetMessage(info, newValue)
	self.db.profile.GreetMessage = { strsplit(";", newValue) }
end

function AutoGreetz:GetWBMessage(info)
	self.string = ""
	for index,value in ipairs(self.db.profile.WBMessage) do 
		self.string = self.string..value..";"
	end
	return self.string
end

function AutoGreetz:SetWBMessage(info, newValue)
	self.db.profile.WBMessage = { strsplit(";", newValue) }
end