## Interface: 40200
## Title: AutoGreetz |cff00aa004.2|r
## Notes: Automatically greets guildies, when they log in.
## Author: W�ldmeister (Tichondrius, EU)
## SavedVariables: AutoGreetzDB
## Dependencies:
## Version: 4.2.0
## OptionalDeps: Ace3
## X-Embeds: Ace3
## X-Translators: W�ldmeister (de, en)
## X-Category: Chat & Communication
## X-Website: http://wow.curseforge.com/addons/autogreetz/
## X-License: All rights reserved

AutoGreetz.xml