﻿
--[[


		$project:		Give Me Peace
		$copyright:		© Copyright Michael Boyle.
						All Rights Reserved.
		
		$email:			michael.boyle@softrix.co.uk
		$website:		www.softrix.co.uk
														
]]--


PEACE_VERSION = "40300-R1";	-- rDDMMYY	
PEACE_SUMMARY = "Give Me Peace has been protecting you since %s and has blocked a total of %s unauthorised whispers across all characters.";

-- options
Peace_Options = {
	true,						-- im i turned on?
	true, true,					-- auto authorise guild/friends list
	false, false,				-- tell me when whispers are blocked / play a sound
	true,						-- inform the unauthorised they were blocked.
	0,							-- number of whispers blocked since installation.
	0,							-- number of whispers let through.
	0,							-- number of gm whispers let through.
	true,						-- show login about message.
	true,						-- show "added" messages on login.
	nil,						-- time/date of installation.
	true,						-- add players who i whispered first to my authorised list.
	false,						-- advertise the addon?   *no longer in use*
	true						-- delete blocked whispers history on login.
};

Peace_CustomMsg = {
	"Auto Reply: I'm currently busy in %s, please try again later or send me an in-game mail.",		
	"Auto Reply: I'm currently busy, please try again later or send me an in-game mail.",		-- position of custom message.
	false
};	

-- sounds
Peace_BlockedSND = "Interface\\Addons\\Peace\\blocked.mp3";

-- storage
Peace_FriendList = {};		-- friends and guild are stored here.
Peace_ManualList = {};		-- anyone manually added is stored here.
Peace_IgnoreList = {};		-- anyone to be ingnored is stored here.
Peace_Temporary = {};		-- anyone in my party/raid are added here.
Peace_BlockedMsg = {};		-- blocked messages are stored here.

--
--	On_Load()
--
function Peace_OnLoad()
	Peace_Main:RegisterEvent("ADDON_LOADED");
	Peace_Main:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
	Peace_Main:RegisterEvent("PLAYER_LOGIN");
	Peace_Main:RegisterEvent("FRIENDLIST_UPDATE");
	Peace_Main:RegisterEvent("GUILD_ROSTER_UPDATE");
	Peace_Main:RegisterEvent("CHAT_MSG_WHISPER");
	Peace_Main:RegisterEvent("CHAT_MSG_WHISPER_INFORM");
	SlashCmdList["PEACE"] = Peace_Commandline;
	SLASH_PEACE1="/peace";	
end;

--
--	Command Line Parse
--
function Peace_Commandline(args)
	local lowerargs = strlower(args);
	if(lowerargs == "blocked") then				
		Peace_ListWhispers();
	elseif(lowerargs == "scan") then	
		if(Peace_Options[2]) then Peace_ScanGuild(); end;
		if(Peace_Options[3]) then Peace_ScanFriends(); end;		
	elseif(lowerargs == "toggle") then	
		if(Peace_Options[1]) then 
			Peace_Options[1] = false;
			DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE : |cffffffff Turned [OFF].");
		else 
			Peace_Options[1] = true; 
			DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE : |cffffffff Turned [ON].");
		end;
	else
		-- show other options in chat.
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffOther options available in the 'Give Me Peace' addon are as follows:");
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ff/peace blocked : |cffffffff Shows a list of whispers for the current character and session.");
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ff/peace toggle : |cffffffff Toggle On/Off the blocking of whispers by the addon.");
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ff/peace scan : |cffffffff Manually scan your guild and friends list and authorise them.");
		local msgOptions = string.format(PEACE_SUMMARY, Peace_Options[12], Peace_Options[7]);
		OptionsBlockedSummary:SetText(msgOptions);
		Peace_CustomMsgEditBox:SetText(Peace_CustomMsg[2]);
		Peace_ProcessOptions();
		Peace_OptionsFrame:Show();
	end;
end;

--
--	On_Update();
--
function Peace_OnUpdate()
end;

--
--	Initialise Addon
--
function Peace_Initialise()
	Peace_Localisation();
	if(Peace_Options[10]) then 
		if(Peace_Options[12]) then
			DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffGive Me Peace ("..PEACE_VERSION..") loaded! |cffffffffType /peace for options. "..Peace_Options[7].." whispers have been blocked since installing this addon on "..Peace_Options[12].."."); 
		else
			DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffGive Me Peace ("..PEACE_VERSION..") loaded! |cffffffffType /peace for options."); 
		end;
	end;
end;

--
--	Monitor Events
--
function Peace_EventHandler(event, ...)
	
	if(event == "ADDON_LOADED") then
		local addon = ...;
		if(addon == "Peace") then
			Peace_Initialise();
		end;
	end;			
	if(event == "PLAYER_LOGIN") then
		if(IsInGuild()) then
			GuildRoster();
		end;
		ShowFriends();
		-- if ive set to clear blocked history then do it here.
		Peace_CleanBlockedHistory();
		-- if i dont have a initial time/date set then store it
		if(not(Peace_Options[12])) then 
			local hour, minute = GetGameTime();
			local weekday, month, day, year = CalendarGetDate();
			Peace_Options[12] = day.."/"..month.."/"..year.." "..hour..":"..minute; 
		end;
	end;		
	if(event == "FRIENDLIST_UPDATE") then
		if(not(loginFriendsLoad)) then
			loginFriendsLoad = true;
			if(Peace_Options[3]) then Peace_ScanFriends(); end;
		end;	
	elseif(event == "GUILD_ROSTER_UPDATE") then
		if(not(loginGuildList)) then
			loginGuildList= true;
			if(IsInGuild()) then
				Peace_ScanGuild();
			end;
		end;
	end;				
end;

--
--	Scan Guild
--
function Peace_ScanGuild()
	if(IsInGuild() and Peace_Options[2]) then
		GuildRoster();
		local numGuild = 0;
		local numMembers = GetNumGuildMembers(true)
		for i=1, numMembers do
			local memberName = strlower(GetGuildRosterInfo(i));
			if(memberName and not(Peace_IsAuthorised(memberName))) then 
				numGuild = numGuild + 1;
				tinsert(Peace_FriendList, memberName);
			end;
		end;
		if(Peace_Options[11] and numGuild > 0) then DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Added "..numGuild.." guild members to the safe list."); end;
	end;
end;

--
--	Scan Friends
--
function Peace_ScanFriends()
	ShowFriends();
	local numPeeps = 0;
	local numFriends = GetNumFriends();
	for i = 1, numFriends do
		local friendName = GetFriendInfo(i);
		if(not(friendName)) then
			ShowFriends();
			return;
		end;
		friendName = strlower(friendName);
		if(friendName and not(Peace_IsAuthorised(friendName))) then
			numPeeps = numPeeps + 1;
			tinsert(Peace_FriendList, friendName);
		end;
	end;
	-- now do the manual friends.
	for count,value in ipairs(Peace_ManualList) do 
		if(not(Peace_IsAuthorised(value))) then 
			-- doesnt exist already so add it.	
			tinsert(Peace_FriendList, value);
			numPeeps = numPeeps + 1;
		end; 
	end;			
	if(Peace_Options[11] and numPeeps > 0) then DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Added "..numPeeps.." friends to the safe list."); end;
end;

--
--	Friend Check
--
function Peace_IsAuthorised(name)
	for count,value in ipairs(Peace_FriendList) do 
		if(value == name) then 
			return(true);
		end; 
	end;
	return(false);
end;

--
--	Ignore Check
--
function Peace_IsIgnored(name)
	for count,value in ipairs(Peace_IgnoreList) do 
		if(value == name) then 
			return(true);
		end; 
	end;
	return(false);
end;

--
--	Incoming Whisper Function
--
ChatFrame_AddMessageEventFilter("CHAT_MSG_WHISPER", function(...)
	local msg = ...;
	if(not(Peace_Options[1])) then return; end;
	local hour, minute = GetGameTime();
	local weekday, month, day, year = CalendarGetDate();
	local msgTime = hour..":"..minute;
	local msgDate = day.."/"..month.."/"..year;
	local incomingPlayer = strlower(select(4, ...));
	local whisperFlag = select(8, ...);
	if(whisperFlag == "GM") then return; end;
	if(Peace_IsAuthorised(incomingPlayer) and not(Peace_IsIgnored(incomingPlayer))) then return; end;	
	local msgBlocked = string.format(MSG_BLOCKED1, UnitName("player"), UnitName("player"), UnitName("player"));
	if(Peace_Options[6]) then 
		if(Peace_CustomMsg[3]) then 
			SendChatMessage(Peace_CustomMsg[2], "WHISPER", nil, incomingPlayer);
		else
			SendChatMessage(msgBlocked, "WHISPER", nil, incomingPlayer);	
		end;
	end;
	if(Peace_Options[4]) then DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE : |cffffffff"..incomingPlayer.." sent a whisper which was blocked. Type /peace blocked to view any blocked whispers."); end;
	tinsert(Peace_BlockedMsg, {incomingPlayer, msg, msgDate, msgTime});
	if(not(lastPlayerMsg == msg)) then
		lastPlayerMsg = msg;
		Peace_Options[7] = tonumber(Peace_Options[7]) + 1
		-- do sound here so that its not repeated multiple times.
		if(Peace_Options[5]) then PlaySoundFile(Peace_BlockedSND); end;
	end;
	return(true);
end);

--
--	Ongoing Whisper Function
--
ChatFrame_AddMessageEventFilter("CHAT_MSG_WHISPER_INFORM", function(_,_,msg,player)
	if(not(Peace_Options[1])) then return; end;
	local lowerPlayer = strlower(player);
	local msgBlocked = string.format(MSG_BLOCKED1, UnitName("player"), UnitName("player"),UnitName("player"));
	if(msg == msgBlocked or msg == Peace_CustomMsg[2] or msg == PEACE_ADVERT) then return(true); end;
	if(Peace_Options[13]) then
		if(not(Peace_IsAuthorised(lowerPlayer))) then
			tinsert(Peace_FriendList, lowerPlayer);
			DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Initiated first whisper to "..player..", adding to authorise list for any replies.");
		end;
	end;
	return;
end);

--
--	List Blocked Whispers.
--
function Peace_ListWhispers()
	for count,value in ipairs(Peace_BlockedMsg) do 
		if(value[1] and value[2]) then 
			DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ff["..value[3].." "..value[4].."] "..value[1].." said|cffffffff : "..value[2]);
		end; 
	end;
end;

--
--	Wipe the whisper history.
--
function Peace_CleanBlockedHistory()
	if(Peace_Options[15]) then
		for i in pairs(Peace_BlockedMsg) do 
			Peace_BlockedMsg[i] = nil;
		end;
	end;
end;

-- ******************* CREDITS **********************

--
--	Hide Credits
--
function PeaceCloseCreditsFrame_OnClick()
	Peace_ShowCreditsFrame:Hide();
	Peace_OptionsFrame:Show();
end;


-- ******************* MAIN OPTIONS *****************

--
--	Close Options
--
function Peace_CloseOptionsFrame_OnClick()
	local customText = Peace_CustomMsgEditBox:GetText();
	Peace_CustomMsg[2] = customText;
	Peace_OptionsFrame:Hide();
end;

--
--	Show Credits
--
function Peace_CreditsButton_OnClick()
	Peace_OptionsFrame:Hide();	
	Peace_ShowCreditsFrame:Show();
end;

--
--	Manual Scan guild and friends (Button)
--
function Peace_ScanGuildFriendsButton_OnClick()
	if(Peace_Options[2]) then Peace_ScanGuild(); end;
	if(Peace_Options[3]) then Peace_ScanFriends(); end;		
end;


--
--	Manual Clear any whisper history (Button)
--
function Peace_ManualClearHistoryButton_OnClick()
	for i in pairs(Peace_BlockedMsg) do 
		Peace_BlockedMsg[i] = nil;
	end;
	DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Deleted any saved blocked whispers. ");
end;

--
--	Process Options
--
function Peace_ProcessOptions()
	if(Peace_Options[1]) then Peace_EnableAddonCheck:SetChecked(true); end;
	if(Peace_Options[2]) then Peace_AutoAddGuildCheck:SetChecked(true); end;
	if(Peace_Options[3]) then Peace_AutoAddFriendsCheck:SetChecked(true); end;
	if(Peace_Options[6]) then Peace_NotifyBlockedPlayersCheck:SetChecked(true); end;
	if(Peace_Options[4]) then Peace_InformWhenPlayerBlockedCheck:SetChecked(true); end;
	if(Peace_Options[5]) then Peace_DoSoundOnBlockCheck:SetChecked(true); end;
	if(Peace_Options[13]) then Peace_AutoAddWhisperedPlayersCheck:SetChecked(true); end;
	if(Peace_Options[15]) then Peace_ResetBlockedWhisperHistoryCheck:SetChecked(true); end;
	if(Peace_CustomMsg[3]) then Peace_UseCustomMessageTextCheck:SetChecked(true); end;
end;

--
--	Checkboxes
--
function Peace_EnableAddonCheck_OnClick()
	local CheckboxEnabled = Peace_EnableAddonCheck:GetChecked();
	if(CheckboxEnabled == 1) then Peace_Options[1] = true; 
	else Peace_Options[1] = false; end;	
end;
function Peace_AutoAddGuildCheck_OnClick()
	local CheckboxEnabled = Peace_AutoAddGuildCheck:GetChecked();
	if(CheckboxEnabled == 1) then 
		Peace_Options[2] = true; 
		Peace_ScanGuild();
	else Peace_Options[2] = false; end;	
end;
function Peace_AutoAddFriendsCheck_OnClick()
	local CheckboxEnabled = Peace_AutoAddFriendsCheck:GetChecked();
	if(CheckboxEnabled == 1) then 
		Peace_Options[3] = true; 
		Peace_ScanFriends();
	else Peace_Options[3] = false; end;	
end;
function Peace_AutoAddWhisperedPlayersCheck_OnClick()
	local CheckboxEnabled = Peace_AutoAddWhisperedPlayersCheck:GetChecked();
	if(CheckboxEnabled == 1) then Peace_Options[13] = true; 
	else Peace_Options[13] = false; end;	
end;
function Peace_NotifyBlockedPlayersCheck_OnClick()
	local CheckboxEnabled = Peace_NotifyBlockedPlayersCheck:GetChecked();
	if(CheckboxEnabled == 1) then Peace_Options[6] = true; 
	else Peace_Options[6] = false; end;	
end;
function Peace_InformWhenPlayerBlockedCheck_OnClick()
	local CheckboxEnabled = Peace_InformWhenPlayerBlockedCheck:GetChecked();
	if(CheckboxEnabled == 1) then Peace_Options[4] = true; 
	else Peace_Options[4] = false; end;	
end;
function Peace_UseCustomMessageTextCheck_OnClick()
	local CheckboxEnabled = Peace_UseCustomMessageTextCheck:GetChecked();
	if(CheckboxEnabled == 1) then Peace_CustomMsg[3] = true; 
	else Peace_CustomMsg[3] = false; end;	
end;
function Peace_ResetBlockedWhisperHistoryCheck_OnClick()
	local CheckboxEnabled = Peace_ResetBlockedWhisperHistoryCheck:GetChecked();
	if(CheckboxEnabled == 1) then Peace_Options[15] = true; 
	else Peace_Options[15] = false; end;	
end;
function Peace_DoSoundOnBlockCheck_OnClick()
	local CheckboxEnabled = Peace_DoSoundOnBlockCheck:GetChecked();
	if(CheckboxEnabled == 1) then Peace_Options[5] = true; 
	else Peace_Options[5] = false; end;	
end;

--
--	Add Player to manual list.
--
function AddNewAuthorisedButton_OnClick()
	local playerName = strlower(EnteredPlayerNameBox:GetText());
	if(playerName and not(Peace_IsAuthorised(playerName))) then
		tinsert(Peace_ManualList, playerName);
		-- now add to the main list.
		for count,value in ipairs(Peace_ManualList) do 
			if(not(Peace_IsAuthorised(value))) then 
				-- doesnt exist already so add it.	
				tinsert(Peace_FriendList, value);
			end; 
		end;
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Added "..playerName.." to the authorised list.");
	else
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : "..playerName.." already exists in your authorised list.");
	end;
end;

--
--	Is a player in the manual list?
--
function DoesPlayerExistManual(player)
	if(player) then
		for count,value in ipairs(Peace_ManualList) do 
			if(value == player) then return(true); end; 
		end;		
	end;
end;

--
--	Delete Player
--
function DeleteAuthoriseButton_OnClick()
	local playerName = strlower(EnteredPlayerNameBox:GetText());
	if(playerName and Peace_IsAuthorised(playerName)) then
		for count,value in ipairs(Peace_FriendList) do 
			if(value == playerName) then 
				tremove(Peace_FriendList, count);
				DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Removed "..playerName.." from the authorised list.");
			end; 
		end;		
	else
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : "..playerName.." doesnt exist in the automatic authorised list.");
	end;
	-- now the manual list quietly
	if(playerName and DoesPlayerExistManual(playerName)) then
		for count,value in ipairs(Peace_ManualList) do 
			if(value == playerName) then 
				tremove(Peace_ManualList, count);
			end; 
		end;		
	end;
	if(playerName and Peace_IsIgnored(playerName)) then
		for count,value in ipairs(Peace_IgnoreList) do 
			if(value == playerName) then 
				tremove(Peace_IgnoreList, count);
				DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Removed "..playerName.." from the ignore list.");
			end; 
		end;		
	end;
	if(Peace_Options[2]) then Peace_ScanGuild(); end;
	if(Peace_Options[3]) then Peace_ScanFriends(); end;	
end;

--
--	Ignore Player
--
function IgnorePlayerWhisperButton_OnClick()
	local playerName = strlower(EnteredPlayerNameBox:GetText());
	if(playerName and not(Peace_IsIgnored(playerName))) then
		tinsert(Peace_IgnoreList, playerName);
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Added "..playerName.." to the ignore list.");
	else
		DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : "..playerName.." is already ignored.");
	end;
end;

--
--	Wipe the lists.
--
function Peace_CleanWholePlayerList_OnClick()
	local refreshCount = 0;
	for i in pairs(Peace_FriendList) do 
		Peace_FriendList[i] = nil;
		refreshCount = refreshCount + 1;
	end;
	DEFAULT_CHAT_FRAME:AddMessage("|cff00e0ffPEACE|cffffffff : Removed "..refreshCount.." players from the authorised list. No ignored entries have been deleted and those will need to be removed manually.");
	if(Peace_Options[2]) then Peace_ScanGuild(); end;
	if(Peace_Options[3]) then Peace_ScanFriends(); end;	
end;

