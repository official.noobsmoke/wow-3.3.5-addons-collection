## Author: MIchael Boyle (www.softrix.co.uk)
## Interface: 40300
## Notes: Give Me Peace is an addon created by Michael Boyle at Softrix Studios.  For support please use the wowinterface.com or curse.com pages.  Thanks for choosing to use this addon for your anti-spam needs.
## SavedVariables: Peace_Options,Peace_CustomMsg,Peace_BlockedMsg,Peace_FriendList,Peace_ManualList,
## Title: Give Me Peace
## Version: 40300-R1
Peace.xml
Options.xml
Credits.xml
Local.lua
Peace.lua
