## Interface: 30300
## Title: QuickMark
## Notes: QuickMark allows you to set the raid target icon of units quickly rather than having to go through the right-click menu or create keybindings.
## Author: stphung, MarZk44
## Version: 3.1415926
## SavedVariables: QuickMarkDB

embeds.xml
AceGUIWidget-QuickMarkFrame.lua
Core.lua