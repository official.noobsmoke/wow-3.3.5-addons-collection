TARGETCHARMS_VERSION = GetAddOnMetadata("TargetCharms", "Version");

local Defaults =
{   ["Version"] = TARGETCHARMS_VERSION,
	["TargetCharms"] = {
		["show"] = 2,
		["barscale"] = 1.0,
		["Xspacing"] = 0,
		["Yspacing"] = 0,
		["draggable"] = 1,
		["alphaVal"] = 0.5,
		["showontarget"] = 0,
		["buttonSetup"] = "v1>5v6<2v3>7v8<4vX>0",
		},
	["ReadyCharm"] = {
		["show"] = 2,
		["barscale"] = 1.0,			
		["draggable"] = 1,
		["alphaVal"] = 0.5,
		["width"] = 60,
		["text"] = TARGETCHARMS_READYCHECK_TEXT,
		},
	
};
local frameNames = {"TargetCharms","TopCharm","ReadyCharm","TopReady"};
local buttonCharm = {["TargetCharms"]={}};

function TargetCharms_msg(text)
	DEFAULT_CHAT_FRAME:AddMessage(TARGETCHARMS_MSG_TAG..text);
end

function TargetCharms_Command(msg)
	orig_msg = msg
	msg = string.lower(msg)
	a,b,cmd,arg = string.find(msg,"(%S+)%s*(%S*)")	
	if cmd==TargetCharms_CMDS[1] then
		TargetCharms_Options = Defaults
		local tmpFrame = getglobal(frameNames[1]);
		tmpFrame:SetScale(TargetCharms_Options[frameNames[1]]["barscale"]);
		--tmpFrame:SetAlpha(1);
		SetDraggable(TargetCharms_Options[frameNames[1]]["draggable"]);
		tmpFrame:SetPoint("TOPLEFT", 0, 0);
		local tmpFrame = getglobal(frameNames[2]);
		tmpFrame:ClearAllPoints() 
		tmpFrame:SetPoint("TOPLEFT", getglobal("UIParent"),"TOP", 100, -20);
		tmpFrame:SetAlpha(TargetCharms_Options[frameNames[1]]["alphaVal"]);
		tmpFrame = getglobal(frameNames[4]);
		tmpFrame:SetAlpha(1);
		tmpFrame:SetScale(1);
		tmpFrame:ClearAllPoints();
		tmpFrame:SetPoint("TOPLEFT",getglobal("UIParent"),"TOP", 100, 0);
		tmpFrame = getglobal(frameNames[3]);
		tmpFrame:SetAlpha(TargetCharms_Options[frameNames[3]]["alphaVal"]);
		tmpFrame:SetScale(1);
		tmpFrame:ClearAllPoints();
		tmpFrame:SetPoint("TOPLEFT", 0, 0);
		tmpFrame = getglobal("TargetCharmsSetup");
		tmpFrame:ClearAllPoints();
		tmpFrame:SetPoint("CENTER", 0, 0);	
		TargetCharms_msg("Position and Values Reset");
	elseif cmd==TargetCharms_CMDS[2] then
		TargetCharmsSetup:Show();
	elseif cmd==TargetCharms_CMDS[3] then
		TargetCharms_Options[frameNames[1]]["show"] = 2
	elseif cmd==TargetCharms_CMDS[4] then
		TargetCharms_Options[frameNames[1]]["show"] = 3
	elseif cmd==TargetCharms_CMDS[5] then		
		TargetCharms_Options[frameNames[1]]["show"] = 1
	else
		TargetCharms_msg(TARGETCHARMS_CMD_HELP);
	end
    CheckReadyButtonViewState();
    CheckFrameViewState();
end

function TargetCharms_onLoad()
	this:RegisterEvent("VARIABLES_LOADED");
	this:RegisterEvent("PARTY_LEADER_CHANGED");
	this:RegisterEvent("PARTY_MEMBERS_CHANGED");
	this:RegisterEvent("PLAYER_TARGET_CHANGED");
	SetTargetHideShow();
	SLASH_TargetCharms1 = TARGETCHARMS_SLASH1;
	SLASH_TargetCharms2 = TARGETCHARMS_SLASH2;
	SlashCmdList["TargetCharms"] = TargetCharms_Command;
end

--Code by Grayhoof (SCT)
function CloneTable(t)				-- return a copy of the table t
	local new = {};					-- create a new table
	local i, v = next(t, nil);		-- i is an index of t, v = t[i]
	while i do
		if type(v)=="table" then 
			v=CloneTable(v);
		end 
		new[i] = v;
		i, v = next(t, i);			-- get next index
	end
	return new;
end

function CopyOldValues(t,f)	
	if(f~=nil) then
	if ( f["TargetCharms"]~=nil) then
		for key,value in pairs(f["TargetCharms"]) do 
		t["TargetCharms"][key]=value ;
		end	
	end 
	if ( f["ReadyCharm"]~=nil) then
		for key,value in pairs(f["ReadyCharm"]) do 
		t["ReadyCharm"][key]=value ;
		end
	end
	end
	return t;
end

function CheckFrameViewState()
	local charmBar = getglobal(frameNames[2]);
	
	if (TargetCharmsSetup:IsShown()==1) then
		if(charmBar:IsShown()~=1) then
			charmBar:Show();
		end
	else
		if TargetCharms_Options["TargetCharms"]["show"] == 2 then
			SetHideShow()
		elseif TargetCharms_Options["TargetCharms"]["show"] == 3 then
	    		if(charmBar:IsShown()==1) then
				charmBar:Hide();
			end
		else
			if (((GetNumPartyMembers()>0) and not UnitInRaid("player")) or (UnitInRaid("player") and (IsRaidOfficer()==1 or IsRaidLeader()==1)))  then
				SetHideShow();
			else
				if(charmBar:IsShown()==1) then
					charmBar:Hide();
				end               
			end
		end
	end	
end

function CheckReadyButtonViewState()
	charmBar = getglobal(frameNames[4]);
	charmBar:Show();
	charmBar = getglobal(frameNames[3]);
	charmBar:Show();
	if (TargetCharmsSetup:IsShown()==1) then
		if(charmBar:IsShown()~=1) then
			charmBar:Show();
			TargetCharms_msg("shown1");
		end
	else
		if TargetCharms_Options[frameNames[3]]["show"] == 2 then
			if (charmBar:IsShown()~=1) then
				charmBar:Show();
				TargetCharms_msg("shown2");
			end
		elseif TargetCharms_Options[frameNames[3]]["show"] == 3 then
	    		if(charmBar:IsShown()==1) then
				charmBar:Hide();
			end
		else
			if (((GetNumPartyMembers()>0 or UnitInRaid("player")) and (IsRaidOfficer()==1 or IsRaidLeader()==1)))  then
				if (charmBar:IsShown()~=1) then
					charmBar:Show();
					TargetCharms_msg("shown3");
				end
			else
				if(charmBar:IsShown()==1) then
					charmBar:Hide();
				end               
			end
		end
	end
end

function SetHideShow()
 local charmBar = getglobal(frameNames[2]);
 if (TargetCharms_Options[frameNames[1]]["showontarget"] == 1) then
	
	if UnitExists("target") then
		if (charmBar:IsShown()~=1) then
			charmBar:Show();
		end
	else
		if (charmBar:IsShown()==1) then
			charmBar:Hide();
		end
	end
 else	
	if (charmBar:IsShown()~=1) then
		charmBar:Show();
	end	
 end
end

function SetTargetHideShow()
	RegisterUnitWatch(getglobal(frameNames[2]), true) 
end

function TargetCharms_onEvent()
	if event=="VARIABLES_LOADED" then
		if TargetCharms_Options == nil or TARGETCHARMS_VERSION ~= TargetCharms_Options["Version"] then
			TargetCharms_Options = CopyOldValues(CloneTable(Defaults),TargetCharms_Options);
			TargetCharms_Options["Version"] = TARGETCHARMS_VERSION;
		end
		local tmpFrame = getglobal(frameNames[1]);
		tmpFrame:SetScale(TargetCharms_Options[frameNames[1]]["barscale"]);
		local tmpFrame = getglobal(frameNames[2]);
		tmpFrame:SetAlpha(TargetCharms_Options[frameNames[1]]["alphaVal"]);
		SetDraggable(TargetCharms_Options[frameNames[1]]["draggable"]);
				
		SetupButtons(frameNames[1],frameNames[1]);		
		SetUpReadyButton();

		TargetCharms_msg(TARGETCHARMS_VERSION.." - "..TARGETCHARMS_LOADED);
	end
	if event~= "PLAYER_TARGET_CHANGED" then
		CheckReadyButtonViewState();
	end
	CheckFrameViewState();
end

function SetUpReadyButton()
	local tmpFrame = getglobal(frameNames[4]);
	tmpFrame:SetAlpha(TargetCharms_Options[frameNames[3]]["alphaVal"]);
	tmpFrame:SetScale(TargetCharms_Options[frameNames[3]]["barscale"]);
	tmpFrame:SetWidth(TargetCharms_Options[frameNames[3]]["width"]);
	tmpFrame = getglobal(frameNames[3]);
	tmpFrame:SetWidth(TargetCharms_Options[frameNames[3]]["width"]);
	tmpFrame:SetText(TargetCharms_Options[frameNames[3]]["text"]);
end

function SetupButtons(frameInfo,frameTarget)
	local buttonString = TargetCharms_Options[frameInfo]["buttonSetup"];
	local maxlen = strlen(buttonString);
	if mod(maxlen,2) == 1 then
		maxlen=maxlen-1
	end
	local buttonNum=1;
	if maxlen > 40 then
		maxlen = 40;
	end
	
	local t
	for t=1,maxlen,2 do
		FormatButton(frameTarget, buttonNum, strsub(buttonString,t,t), strsub(buttonString,t+1,t+1),TargetCharms_Options[frameInfo]["Xspacing"],TargetCharms_Options[frameInfo]["Yspacing"]);
		buttonNum=buttonNum+1;
	end

	for buttonNum=buttonNum,20 do
		getglobal(frameTarget.."Charm"..buttonNum):Hide();
	end
end 

function FormatButton(frame, buttonNum ,posChar, typeNum, xSpacing, ySpacing)
	local button = getglobal(frame.."Charm"..buttonNum)
	button:ClearAllPoints();
	if strlower(posChar) == TARGETCHARMS_POSITION_DOWN then
		button:SetPoint("TOPLEFT", getglobal(frame.."Charm"..tostring(buttonNum-1)),"BOTTOMLEFT",0,0-ySpacing);
	elseif posChar == TARGETCHARMS_POSITION_UP then
		button:SetPoint("BOTTOMLEFT", getglobal(frame.."Charm"..tostring(buttonNum-1)),"TOPLEFT",0,0+ySpacing);
	elseif posChar == TARGETCHARMS_POSITION_RIGHT then
		button:SetPoint("TOPLEFT", getglobal(frame.."Charm"..tostring(buttonNum-1)),"TOPRIGHT",0+xSpacing,0);
	elseif posChar == TARGETCHARMS_POSITION_LEFT then
		button:SetPoint("TOPRIGHT", getglobal(frame.."Charm"..tostring(buttonNum-1)),"TOPLEFT",0-xSpacing,0);
	else
	--ERROR--
		button:SetPoint("TOPLEFT", getglobal(frame.."Charm"..tostring(buttonNum-1)),"TOPRIGHT",0,0-ySpacing);
		buttonCharm[frame][buttonNum] = 0;
        button:Hide();
        --message(TARGETCHARMS_ERROR_INVALIDCHAR);
	print(TARGETCHARMS_ERROR_INVALIDCHAR); 
		return false;
	end


	if typeNum == TARGETCHARMS_CHARM0 then	
		buttonCharm[frame][buttonNum] = 0;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT", button,"TOPLEFT",0,0);
		texture:SetWidth(32);
		texture:SetHeight(32);
		texture:SetTexture("interface\\buttons\\UI-Quickslot.blp");
		texture:SetTexCoord(0.15,0.85,0.15,0.85);	
	elseif typeNum == TARGETCHARMS_CHARM1 then
		buttonCharm[frame][buttonNum] = 1;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT", button,"TOPLEFT",2,-2);
		texture:SetTexture("interface\\targetingframe\\UI-RaidTargetingIcons.blp");
		texture:SetTexCoord(0,0.25,0,0.25);
		texture:SetWidth(28);
		texture:SetHeight(28);
	elseif typeNum == TARGETCHARMS_CHARM2 then
		buttonCharm[frame][buttonNum] = 2;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT", button,"TOPLEFT",2,-2);
		texture:SetTexture("interface\\targetingframe\\UI-RaidTargetingIcons.blp");
		texture:SetTexCoord(0.25,0.5,0,0.25);
		texture:SetWidth(28);
		texture:SetHeight(28);
	elseif typeNum == TARGETCHARMS_CHARM3 then
		buttonCharm[frame][buttonNum] = 3;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT",button,"TOPLEFT",2,-2);
		texture:SetTexture("interface\\targetingframe\\UI-RaidTargetingIcons.blp");
		texture:SetTexCoord(0.5,0.75,0,0.25);
		texture:SetWidth(28);
		texture:SetHeight(28);
	elseif typeNum == TARGETCHARMS_CHARM4 then
		buttonCharm[frame][buttonNum] = 4;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT",button,"TOPLEFT",2,-2);
		texture:SetTexture("interface\\targetingframe\\UI-RaidTargetingIcons.blp");
		texture:SetTexCoord(0.75,1,0,0.25);
		texture:SetWidth(28);
		texture:SetHeight(28);
	elseif typeNum == TARGETCHARMS_CHARM5 then
		buttonCharm[frame][buttonNum] = 5;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT",button,"TOPLEFT",2,-2);
		texture:SetTexture("interface\\targetingframe\\UI-RaidTargetingIcons.blp");
		texture:SetTexCoord(0,0.25,0.25,0.5);
		texture:SetWidth(28);
		texture:SetHeight(28);
	elseif typeNum == TARGETCHARMS_CHARM6 then
		buttonCharm[frame][buttonNum] = 6;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT",button,"TOPLEFT",2,-2);
		texture:SetTexture("interface\\targetingframe\\UI-RaidTargetingIcons.blp");
		texture:SetTexCoord(0.25,0.5,0.25,0.5);
		texture:SetWidth(28);
		texture:SetHeight(28);
	elseif typeNum == TARGETCHARMS_CHARM7 then
		buttonCharm[frame][buttonNum] = 7;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT",button,"TOPLEFT",2,-2);
		texture:SetTexture("interface\\targetingframe\\UI-RaidTargetingIcons.blp");
		texture:SetTexCoord(0.5,0.75,0.25,0.5);
		texture:SetWidth(28);
		texture:SetHeight(28);
	elseif typeNum == TARGETCHARMS_CHARM8 then
		buttonCharm[frame][buttonNum] = 8;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT",button,"TOPLEFT",2,-2);
		texture:SetTexture("interface\\targetingframe\\UI-RaidTargetingIcons.blp");
		texture:SetTexCoord(0.75,1,0.25,0.5);
		texture:SetWidth(28);
		texture:SetHeight(28);
	elseif typeNum == TARGETCHARMS_CHARM9 then
		buttonCharm[frame][buttonNum] = 8;
		local texture = getglobal(button:GetName().."CharmTex");
		texture:SetPoint("TOPLEFT",button,"TOPLEFT",0,0);
		texture:SetTexture("interface\\icons\\ability_hunter_snipershot.blp");
		texture:SetTexCoord(0,1,0,1);
		texture:SetWidth(32);
		texture:SetHeight(32);
	else
        button:Hide();
		return false;
	end
	button:Show();
	return true;
end

function SelectTarget(frameId, targetId)
	local charmId = buttonCharm[frameNames[frameId]][tonumber(targetId)];
	SetRaidTarget("target", charmId);
end

function SetDraggable(drag)
   local button = getglobal(frameNames[1].."Charm0");
   if drag == nil or drag == 2 then
	if (button:IsShown()==1) then
   		button:Hide();
	end
   else
	if (button:IsShown()~=1) then
   		button:Show();   
	end
   end
end

function SetFrameScale(scale)
	local tmpFrame = getglobal(frameNames[1]);
	tmpFrame:SetScale(scale);
end

function FormDropDownType_Initialize()
	local info;
	for i = 1, getn(TargetCharms_ShowTypes), 1 do
		info = {
			text = TargetCharms_ShowTypes[i];
			func = FormDropDownType_OnClick;
		};
		UIDropDownMenu_AddButton(info);
	end
end

function FormDropDownType2_Initialize()
	local info;
	for i = 1, getn(TargetCharms_ShowTypes), 1 do
		info = {
			text = TargetCharms_ShowTypes[i];
			func = FormDropDownType2_OnClick;
		};
		UIDropDownMenu_AddButton(info);
	end
end

function FormDropDownType_OnShow()
	UIDropDownMenu_Initialize(FormDropDownType, FormDropDownType_Initialize);
	UIDropDownMenu_SetSelectedID(FormDropDownType, TargetCharms_Options[frameNames[1]]["show"]);
	UIDropDownMenu_SetWidth(FormDropDownType,100);
	CheckFrameViewState();
end

function FormDropDownType2_OnShow()
	UIDropDownMenu_Initialize(FormDropDownType2, FormDropDownType2_Initialize);
	UIDropDownMenu_SetSelectedID(FormDropDownType2, TargetCharms_Options[frameNames[3]]["show"]);
	UIDropDownMenu_SetWidth(FormDropDownType2,100);
	CheckReadyButtonViewState();
end

function FormDropDownType_OnClick()
	local i = this:GetID();
	TargetCharms_Options[frameNames[1]]["show"] = i;
	UIDropDownMenu_SetSelectedID(FormDropDownType, i);
	FormDropDownType_OnShow();
end

function FormDropDownType2_OnClick()
	local i = this:GetID();
	TargetCharms_Options[frameNames[3]]["show"] = i;
	UIDropDownMenu_SetSelectedID(FormDropDownType2, i);
	FormDropDownType2_OnShow();
end

function DropDownPresetOptions_Initialize()
	local info;
	for i = 1, getn(TargetCharms_LayoutDefaults), 1 do
		info = {
			text = TargetCharms_LayoutDefaults[i][1];
			func = DropDownPresetOptions_OnClick;
		};
		UIDropDownMenu_AddButton(info);
	end
end

function DropDownPresetOptions_OnShow(di)
	UIDropDownMenu_Initialize(DropDownPresetOptions , DropDownPresetOptions_Initialize);
	UIDropDownMenu_SetSelectedID(DropDownPresetOptions , di);
	UIDropDownMenu_SetWidth(DropDownPresetOptions,100);
end

function DropDownPresetOptions_OnClick()
	local i = this:GetID();
	UIDropDownMenu_SetSelectedID(DropDownPresetOptions, i);
	--DropDownPresetOptions_OnShow(i)
end

function LoadButton_OnClick()
	local i = UIDropDownMenu_GetSelectedID(DropDownPresetOptions);
	local tmpEditBox = getglobal("EditBox");
	tmpEditBox:SetText(TargetCharms_LayoutDefaults[i][2]);
end