## Interface: 30300
## Title: TargetCharms
## Notes: Toolbar for setting charms/raid icons on targets. v.9/15/08
## Author: Gander
## Version: 1.3 
## SavedVariablesPerCharacter: TargetCharms_Options
localization.en.lua

TargetCharms.xml
TargetCharmsSetup.xml