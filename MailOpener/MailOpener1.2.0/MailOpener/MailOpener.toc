## Interface: 30300
## Title: Mail Opener
## Notes: Mail Opener will automatically retrieve all your mail from the mailbox.
## Author: Zerotorescue
## Version: v1.2.0
## SavedVariables: MailOpenerDB
## LoadManagers: AddonLoader
## X-LoadOn-Slash: /mo, /mailopen, /mailopener
## X-LoadOn-Mailbox: true
## X-LoadOn-InterfaceOptions: Mail Opener
## X-Curse-Packaged-Version: v1.2.0
## X-Curse-Project-Name: MailOpener
## X-Curse-Project-ID: mailopener
## X-Curse-Repository-ID: wow/mailopener/mainline

embeds.xml

Localization\enUS.lua

Localization\enUS.lua
Localization\deDE.lua
Localization\esES.lua
Localization\esMX.lua
Localization\frFR.lua
Localization\koKR.lua
Localization\ruRU.lua
Localization\zhCN.lua
Localization\zhTW.lua

Core.lua

Modules\BeanCounterSupport.lua
Modules\Collected.lua
Modules\Config.lua
Modules\FailSafe.lua
Modules\OpenAll.lua