GuildExport = CreateFrame("Button", "GuildExportHiddenFrame", UIParent)
local addonName = "GuildExport"
local revision = tonumber(("$Revision: 21 $"):match("%d+"))
local loaded = false
local debugging = false

GuildExportData = GuildExportData or {}

local function debug(msg)
  if debugging then
     DEFAULT_CHAT_FRAME:AddMessage("\124cFFFF0000"..addonName.."\124r: "..msg)
  end
end

function GuildExport:Load()
  loaded = true
  local revstr 
  revstr = GetAddOnMetadata("GuildExport", "X-Curse-Packaged-Version")
  if not revstr then
    revstr = GetAddOnMetadata("GuildExport", "Version")
  end
  if not revstr or string.find(revstr, "@") then
    revstr = "r"..tostring(revision)
  end
  print("GuildExport "..revstr.." loaded.")
end

function GuildExport:Unload()
  loaded = false
  GuildExport:ExportToWtf()
  print("GuildExport unloaded.")
end

function GuildExport:ExportToWtf()
	if IsInGuild() then
		local count = GetNumGuildMembers(true)
		local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
		if count>0 then
			print("GuildExport exports " .. count .. " members to wtf-file...")
			--clear saved data
			GuildExportData = {
				GuildExport = {
					guild = {
						guildName=guildName,
						members={},
					},
					ExportTime = time(),
				}
			}
			for i=1, count do
				name, rank, rankIndex, level, class, zone, note, officernote, online, status, classFileName, achievementPoints, achievementRank, isMobile = GetGuildRosterInfo(i);
				debug(name.." "..rank.." "..note)
				GuildExportData.GuildExport.guild.members[classFileName] = GuildExportData.GuildExport.guild.members[classFileName] or {}
				GuildExportData.GuildExport.guild.members[classFileName][name] = {
--					name=name,
--					rank=rank,
					rankIndex=rankIndex,
					note=note,
					level=level,
--					classFileName=classFileName,
--					class=class,
				}
			end
			print("GuildExport export done.")
		end
		return count
	else
		return false
	end
end

--[[
<?xml version="1.0" encoding="UTF-8"?>
<Danish Battle Hawks>GetGuildXml() not implemented yet!</Danish Battle Hawks>
]]--
function GuildExport:GetXmlStringHelper(data,inset)
	local result = ""
	for k,v in pairs(data) do
		result = result .. inset .. "<"..k..">" ..
		(
			(type(v) == "table")
			and (GuildExport:GetXmlStringHelper(v,inset.."  ") .. inset)
			or v
		) .. "</"..k..">"
		--[[
		result = result .. inset .. "<"..k..">"
		if type(v) == "table" then
			result = result .. GuildExport:GetXmlStringHelper(v,inset.."  ") .. inset
		else
			result = result .. v
		end
		result = result .. "</"..k..">"
		]]--
	end
	return result
end

function GuildExport:GetXmlString()
	return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"..GuildExport:GetXmlStringHelper(GuildExportData,"\n")
end

function GuildExport:Export()
	if GuildExport:ExportToWtf() then
		StaticPopup_Show ("GUILDEXPORT", "succes")
	else
		StaticPopup_Show ("GUILDEXPORT", "failed")
	end
end

function GuildExport_OnEvent(self, event, arg1)
--  if event == "ADDON_LOADED" and arg1 == "GuildExport" then
--    GuildExport:Load()
--  end
  if event == "VARIABLES_LOADED" then
	GuildExport:UnregisterEvent("VARIABLES_LOADED")
    GuildExport:Load()
  end
end
GuildExport:SetScript("OnEvent", GuildExport_OnEvent)
--GuildExport:RegisterEvent("ADDON_LOADED")
GuildExport:RegisterEvent("VARIABLES_LOADED")

SLASH_GUILDEXPORT1 = "/guildexport"
SLASH_GUILDEXPORT2 = "/ge"
SlashCmdList["GUILDEXPORT"] = function(msg)
  local cmd = msg:lower()
  if cmd == "load" or cmd == "on" or cmd == "ver" then
    GuildExport:Load()
  elseif cmd == "unload" or cmd == "off" then
    GuildExport:Unload()
  elseif cmd == "exportwtf" then
    GuildExport:ExportToWtf()
  elseif cmd == "export" then
    GuildExport:Export()
  elseif cmd == "debug" then
    debugging = not debugging
    print("GuildExport debugging "..(debugging and "enabled" or "disabled"))
  else
    print("/guildexport [ on | off | export | debug ]")
  end
end

StaticPopupDialogs["GUILDEXPORT"] = {
  text = "GuildExport %s\npress ctrl-a then ctrl-c to copy",
  button1 = "Ok",
--  button2 = "No",
  OnAccept = function()
      GreetTheWorld()
  end,
  timeout = 0,
  whileDead = true,
  hideOnEscape = true,
  OnShow = function (self, data)
    self.editBox:SetText(GuildExport:GetXmlString())
  end,
  OnAccept = function (self, data, data2)
    local text = self.editBox:GetText()
    -- do whatever you want with it
  end,
  hasEditBox = true,
  editBoxWidth = 350,
}
