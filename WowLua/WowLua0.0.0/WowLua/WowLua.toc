## Interface: 60200
## Title: WowLua
## Version: v60200-1.0.0-42cd3ec 
## Author: Cladhaire
## Notes: Interactive Lua interpreter and scratchpad
## LoadManagers: AddonLoader
## X-LoadOn-Slash: /wowlua, /lua, /wowluarun, /luarun
## SavedVariables: WowLua_DB

Localization.enUS.lua

WowLua.lua
FAIAP.lua
WowLua.xml
WowLua_Config.lua
