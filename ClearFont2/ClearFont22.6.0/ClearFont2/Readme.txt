-- /////////////////////////////////////////////////////////////////////////////
-- =============================================================================
--  CLEARFONT2 BY KIRKBURN
--  Official website:  http://www.clearfont.co.uk/
-- -----------------------------------------------------------------------------
--  README.TXT - THIS IS *NOT* A COMPLETE README, PLEASE SEE THE WEBSITE FOR MORE
--	 A. THE INFAMOUS STEP TWO!
--	 B. HOW TO GET MORE FONT CHOICES
--	 C. FREQUENT QUESTIONS
--	 D. MORE HELP AND SUPPORT
-- =============================================================================
-- /////////////////////////////////////////////////////////////////////////////


-- =============================================================================
--  A. THE INFAMOUS STEP TWO!
-- =============================================================================

"Step Two" does two things:
- It overrides *all* the WoW fonts whether or not ClearFont does it via code (including the login screens!)
- It can also help with some addons which can directly reference the WoW font files. (I don't think they should, but hey, we can't all be perfect ;)

To do this, create four (oddly-named) font files in a <WoW directory>\Fonts\ directory (you may need to crate this directory).

The files you want to create should be copies of the current font you are using in WoW, using the exact name listed below.
Normally, I use a copy of the 'normal' version for each (rather than bold, etc).

ARIALN.ttf		- login screens, numbers, chat font
FRIZQT__.ttf	- the main WoW fonts
MORPHEUS.ttf	- quest log
skurri.ttf		- damage font

This "Step Two" is not included with the download as it would increase the file size too much and wouldn't work with WowAceUpdater (WAU).

Note that doing this does NOT 'hack' WoW, it just places fonts in a location that results in WoW using them instead of the defaults (by design).
More about Step Two can be found on the official website.



-- =============================================================================
--  B. HOW TO GET MORE FONT CHOICES
-- =============================================================================

-- -----------------------------------------------------------------------------
-- SHAREDMEDIA (and SHAREDMEDIALIB)
-- -----------------------------------------------------------------------------

Get these addons from WowAce.com!
SharedMedia comes with several extra fonts, plus sounds and textures, for supporting Ace addons.
Fonts included in SharedMedia and those added to the SML list will seamlessly be added to the list of available CF2 fonts.


-- -----------------------------------------------------------------------------
-- FONT PACKS
-- -----------------------------------------------------------------------------

ClearFont2_FontPack, available on the Ace SVN, comes with several extra fonts.

It's easy to create your own font pack - I suggest obtaining a copy of ClearFont2_FontPack from the Ace SVN.
Take a look at the very simple code - you can either add to this, or perhaps even make your own font pack addon using the same code (I recommend the latter).

What occurs is that the info from the 'plugin' addon is seamlessly added to the master table of font options that CF2 uses.
It then becomes a choice on the CF options, with no other input required. :)

FontPack fonts are also automatically added to the SharedMediaLibrary for use by other supporting addons.



-- =============================================================================
--  C. FREQUENT QUESTIONS
-- =============================================================================

Q. Tradeskill windows are unreadable, showing '...'!
A. This comes up frequently. This is not really due to CF, but due to a limitation in WoW. It's easily solved - please check this section on the official website - http://www.clearfont.co.uk/readme.html#KnownIssues

Q. I cannot set MT/MA from the Blizzard raid windows!
A. This is due to some annoying tainting issues. Check near the start of section B of Design.lua for an easy fix!

Other questions like this are answered on the website. Please check it out :)



-- =============================================================================
--  D. MORE HELP AND SUPPORT
-- =============================================================================

This is in no way a full and complete readme, and there are several other locations with more info 

* The official website has a far more extensive readme: http://www.clearfont.co.uk
* The Ace wiki page with many useful links: http://www.wowace.com/wiki/ClearFont2 (and http://www.wowace.com/wiki/ClearFont2_fonts)
* The official CF2 Ace forum thread: http://www.wowace.com/forums/index.php?topic=3513.0
* The Ace SVN files repository: http://www.wowace.com/files/

Note that I certainly do not mind ninja-commits to the Ace SVN, so long as it doesn't break CF2. Files must be kept as UTF-8!

Cheers, and thanks for using ClearFont2!
Kirkburn