-- /////////////////////////////////////////////////////////////////////////////
-- =============================================================================
--  CLEARFONT2 BY KIRKBURN
--  Official website:  http://www.clearfont.co.uk/
-- -----------------------------------------------------------------------------
--  DESIGN.LUA - THE CLEARFONT DESIGN CODE
--	 A. SETTING UP THE ADDON
--	 B. WOW UI DESIGN  <--  the important bit to edit!
--	 C. DYNAMIC ADDON UPDATES
--	 D. AND FINALLY...
-- =============================================================================
-- /////////////////////////////////////////////////////////////////////////////


-- =============================================================================
--  A. SETTING UP THE ADDON
-- =============================================================================

-- -----------------------------------------------------------------------------
-- Funky local fun! (ckknight)
-- -----------------------------------------------------------------------------

local tmp = {}
local function iter(t, i)
	i = i + 1
	local x = t[i]
	if not x then
		return
	end
	return i, x
end
local function fake_ipairs(...)
	for k in pairs(tmp) do
		tmp[k] = nil
	end
	local n = select('#', ...)
	for i = 1, n do
		tmp[i] = select(i, ...)
	end
	return iter, tmp, 0
end

local last_scale = nil
local last_font1 = nil
local last_font2 = nil
local last_font3 = nil
local last_font4 = nil
local last_font5 = nil


-- -----------------------------------------------------------------------------
-- Create function to check existence of certain fonts (useful for patch changes)
-- -----------------------------------------------------------------------------

local function CanSetFont(object)
	return (type(object)=="table"
		and object.SetFont and object.IsObjectType 
			and not object:IsObjectType("SimpleHTML")); 
end

	
-- -----------------------------------------------------------------------------
-- The function called by Core.lua, and locals
-- -----------------------------------------------------------------------------

function ApplyClearFontDesign(f)

	local scale					= ClearFont.db.profile.currentScale
	local scalechange 			= last_scale ~= scale
	last_scale 					= scale

	
-- -----------------------------------------------------------------------------
-- The different styles available in Core.lua
-- -----------------------------------------------------------------------------

	local NORMAL_TEXT_FONT
	local BOLD_TEXT_FONT
	local BOLDITALIC_TEXT_FONT
	local ITALIC_TEXT_FONT
	local NUMBER_TEXT_FONT
	if type(f) == "number" then
		NORMAL_TEXT_FONT     = ClearFont.fonts[f].normal
		BOLD_TEXT_FONT       = ClearFont.fonts[f].bold
		BOLDITALIC_TEXT_FONT = ClearFont.fonts[f].bolditalic
		ITALIC_TEXT_FONT     = ClearFont.fonts[f].italic
		NUMBER_TEXT_FONT     = ClearFont.fonts[f].number
	else
		NORMAL_TEXT_FONT     = f
		BOLD_TEXT_FONT       = f
		BOLDITALIC_TEXT_FONT = f
		ITALIC_TEXT_FONT     = f
		NUMBER_TEXT_FONT     = f
	end



-- =============================================================================
--  B. WOW UI DESIGN
-- =============================================================================
--  This is the most important section to edit! I recommend using Notepad++ (http://notepad-plus.sourceforge.net)
--  Main font styles are listed first, the rest follow alphabetically
--  Listed lines are only where CF2 alters the defaults - not every aspect of a font may be shown (such as shadows)
-- =============================================================================
--  The following code snippets can be used:
--  No outline:		Font:SetFont(SOMETHING_TEXT_FONT, x * scale)
--  Normal outline:		Font:SetFont(SOMETHING_TEXT_FONT, x * scale, "OUTLINE")
--  Thick outline:		Font:SetFont(SOMETHING_TEXT_FONT, x * scale, "THICKOUTLINE")
--  Text colour:		Font:SetTextColor(r, g, b)
--  Shadow colour:		Font:SetShadowColor(r, g, b) 
--  Shadow position:	Font:SetShadowOffset(x, y) 
--  Transparency:		Font:SetAlpha(x)
-- =============================================================================


-- -----------------------------------------------------------------------------
-- Special game world '3D' fonts (thanks to Dark Imakuni)
-- Note that CF2 cannot define sizes or styles for these, just the fonts
-- THESE LINES MAY CAUSE PROBLEMS WHEN TRYING TO SET MT/MA FROM THE BLIZZARD FRAMES
-- If you do not raid or set MT/MA, you can probably leave these lines uncommented without problem!
-- To comment out a line, add "--" at the start
-- -----------------------------------------------------------------------------

-- Names above character's heads, free floating text (normally FRIZQT_.ttf)
UNIT_NAME_FONT = NORMAL_TEXT_FONT

-- Names above character's head, in nameplates (normally FRIZQT_.ttf)
NAMEPLATE_FONT = NORMAL_TEXT_FONT

-- Damage pop-up over targets , *not* SCT/SDT (normally FRIZQT_.ttf)
DAMAGE_TEXT_FONT = NORMAL_TEXT_FONT

-- Chat bubbles (normally FRIZQT_.ttf)
STANDARD_TEXT_FONT = NORMAL_TEXT_FONT


-- -----------------------------------------------------------------------------
-- Drop-down menu font size
-- THESE LINES MAY CAUSE PROBLEMS WHEN TRYING TO SET MT/MA FROM THE BLIZZARD FRAMES
-- If you do not raid or set MT/MA, you can probably leave these lines uncommented without problem!
-- To comment out a line, add "--" at the start
-- -----------------------------------------------------------------------------

UIDROPDOWNMENU_DEFAULT_TEXT_HEIGHT = 12 * scale


-- -----------------------------------------------------------------------------
-- System Font
-- The overall font, mostly used for inheritance purposes
-- -----------------------------------------------------------------------------

SystemFont_Tiny:SetFont(BOLD_TEXT_FONT, 9 * scale)
SystemFont_Small:SetFont(BOLD_TEXT_FONT, 11 * scale)
SystemFont_Outline_Small:SetFont(BOLD_TEXT_FONT, 11 * scale)
SystemFont_Shadow_Small:SetFont(BOLD_TEXT_FONT, 11 * scale)
SystemFont_InverseShadow_Small:SetFont(BOLD_TEXT_FONT, 11 * scale)
SystemFont_Med1:SetFont(NORMAL_TEXT_FONT, 13 * scale)
SystemFont_Shadow_Med1:SetFont(NORMAL_TEXT_FONT, 13 * scale)
SystemFont_Med2:SetFont(NORMAL_TEXT_FONT, 14 * scale)
SystemFont_Med3:SetFont(BOLD_TEXT_FONT, 15 * scale)
SystemFont_Shadow_Med3:SetFont(BOLD_TEXT_FONT, 15 * scale)
SystemFont_Large:SetFont(BOLD_TEXT_FONT, 17 * scale)
SystemFont_Shadow_Large:SetFont(BOLD_TEXT_FONT, 17 * scale)
SystemFont_Shadow_Huge1:SetFont(BOLD_TEXT_FONT, 20 * scale)
SystemFont_OutlineThick_Huge2:SetFont(BOLD_TEXT_FONT, 22 * scale)
SystemFont_Shadow_Outline_Huge2:SetFont(BOLD_TEXT_FONT, 22 * scale)
SystemFont_Shadow_Huge3:SetFont(BOLD_TEXT_FONT, 25 * scale)
SystemFont_OutlineThick_Huge4:SetFont(BOLD_TEXT_FONT, 26 * scale)
SystemFont_OutlineThick_WTF:SetFont(BOLD_TEXT_FONT, 102 * scale)

DialogButtonNormalText:SetFont(BOLD_TEXT_FONT, 17 * scale)

-- -----------------------------------------------------------------------------
-- Primary Game Fonts
-- Used as the main font used, e.g. window titles
-- Normally uses FRIZQT_.ttf, size 12, darker GameFontDisable
-- -----------------------------------------------------------------------------

GameFontNormal:SetFont(NORMAL_TEXT_FONT, 13 * scale)
GameFontHighlight:SetFont(NORMAL_TEXT_FONT, 13 * scale)

GameFontDisable:SetFont(NORMAL_TEXT_FONT, 13 * scale)
GameFontDisable:SetTextColor(0.6, 0.6, 0.6)

GameFontGreen:SetFont(NORMAL_TEXT_FONT, 13 * scale)
GameFontRed:SetFont(NORMAL_TEXT_FONT, 13 * scale)
GameFontBlack:SetFont(NORMAL_TEXT_FONT, 13 * scale)
GameFontWhite:SetFont(NORMAL_TEXT_FONT, 13 * scale)


-- -----------------------------------------------------------------------------
-- Small Game Fonts
-- Used as the main small font, e.g. character sheets, buff timers, macro titles
-- Normally uses FRIZQT_.ttf, size 10, darker GameFontDisableSmall and GameFontDarkGraySmall
-- -----------------------------------------------------------------------------

GameFontNormalSmall:SetFont(BOLD_TEXT_FONT, 11 * scale)

GameFontHighlightSmall:SetFont(BOLD_TEXT_FONT, 11 * scale)
GameFontHighlightSmallOutline:SetFont(BOLD_TEXT_FONT, 11 * scale, "OUTLINE")

GameFontDisableSmall:SetFont(BOLD_TEXT_FONT, 11 * scale)
GameFontDisableSmall:SetTextColor(0.6, 0.6, 0.6)

GameFontDarkGraySmall:SetFont(BOLD_TEXT_FONT, 11 * scale)
GameFontDarkGraySmall:SetTextColor(0.4, 0.4, 0.4)

GameFontGreenSmall:SetFont(BOLD_TEXT_FONT, 11 * scale)
GameFontRedSmall:SetFont(BOLD_TEXT_FONT, 11 * scale)


-- -----------------------------------------------------------------------------
-- Large Game Fonts
-- Used for window titles, etc
-- Normally uses FRIZQT_.ttf, size 16, darker GameFontDisableLarge
-- -----------------------------------------------------------------------------

GameFontNormalLarge:SetFont(BOLD_TEXT_FONT, 17 * scale)
GameFontHighlightLarge:SetFont(NORMAL_TEXT_FONT, 17 * scale)

GameFontDisableLarge:SetFont(NORMAL_TEXT_FONT, 17 * scale)
GameFontDisableLarge:SetTextColor(0.6, 0.6, 0.6)

GameFontGreenLarge:SetFont(NORMAL_TEXT_FONT, 17 * scale)
GameFontRedLarge:SetFont(NORMAL_TEXT_FONT, 17 * scale)


-- -----------------------------------------------------------------------------
-- Huge Game Fonts
-- Used for raid warnings
-- Normally uses FRIZQT_.ttf
-- -----------------------------------------------------------------------------

GameFontNormalHuge:SetFont(BOLD_TEXT_FONT, 20 * scale)

-- -----------------------------------------------------------------------------
-- Number Fonts
-- e.g. Auction House, money, icon keybinding & quantity overlays
-- Normally uses ARIALN.ttf, size 14/14/12/12/16/30
-- -----------------------------------------------------------------------------

NumberFont_Shadow_Small:SetFont(NUMBER_TEXT_FONT, 13 * scale, "OUTLINE")
NumberFont_OutlineThick_Mono_Small:SetFont(NUMBER_TEXT_FONT, 13 * scale, "OUTLINE")
NumberFont_Shadow_Med:SetFont(NUMBER_TEXT_FONT, 15 * scale, "OUTLINE")
NumberFont_Outline_Med:SetFont(NUMBER_TEXT_FONT, 15 * scale, "OUTLINE")
NumberFont_Outline_Large:SetFont(NUMBER_TEXT_FONT, 17 * scale, "OUTLINE")
NumberFont_Outline_Huge:SetFont(NUMBER_TEXT_FONT, 30 * scale, "OUTLINE")
NumberFont_Outline_Huge:SetAlpha(30)

-- -----------------------------------------------------------------------------
-- ChatFrame
-- Used for chat fonts and sizes
-- Normally uses ARIALN.ttf
-- -----------------------------------------------------------------------------

-- Chat and edit box font, edit box font size
ChatFontNormal:SetFont(NORMAL_TEXT_FONT, 14 * scale)

-- Added in 2.1
ChatFontSmall:SetFont(NORMAL_TEXT_FONT, 12 * scale)

-- Extra chat font size options (add more if you wish!)
CHAT_FONT_HEIGHTS = {
	[1] = 7,
	[2] = 8,
	[3] = 9,
	[4] = 10,
	[5] = 11,
	[6] = 12,
	[7] = 13,
	[8] = 14,
	[9] = 15,
	[10] = 16,
	[11] = 17,
	[12] = 18,
	[13] = 19,
	[14] = 20,
	[15] = 21,
	[16] = 22,
	[17] = 23,
	[18] = 24
}

-- These would be used to set individual chat frames if they didn't just break everything :)
-- ChatFrame1:SetFont(NORMAL_TEXT_FONT, 14 * scale)
-- ChatFrame2:SetFont(NORMAL_TEXT_FONT, 14 * scale)
-- ChatFrame3:SetFont(NORMAL_TEXT_FONT, 14 * scale)
-- ChatFrame4:SetFont(NORMAL_TEXT_FONT, 14 * scale)
-- ChatFrame5:SetFont(NORMAL_TEXT_FONT, 14 * scale)
-- ChatFrame6:SetFont(NORMAL_TEXT_FONT, 14 * scale)
-- ChatFrame7:SetFont(NORMAL_TEXT_FONT, 14 * scale)


-- -----------------------------------------------------------------------------
-- CombatTextFont
-- In-built SCT-style info
-- Normally uses FRIZQT_.ttf, size 25
-- -----------------------------------------------------------------------------

CombatTextFont:SetFont(NORMAL_TEXT_FONT, 26 * scale)


-- -----------------------------------------------------------------------------
-- Quest Log
-- e.g. quest log, books
-- -----------------------------------------------------------------------------

-- Quest titles, normally MORPHEUS.ttf, size 18
QuestFont_Large:SetFont(BOLD_TEXT_FONT, 19 * scale)
QuestFont_Large:SetShadowColor(0.54, 0.4, 0.1)

-- Quest descriptions, normally FRIZQT_.ttf, size 13
QuestFont:SetFont(ITALIC_TEXT_FONT, 14 * scale)
QuestFont:SetTextColor(0.15, 0.09, 0.04)

-- Quest objectives, normally FRIZQT_.ttf, size 12
QuestFontNormalSmall:SetFont(BOLD_TEXT_FONT, 13 * scale)
QuestFontNormalSmall:SetShadowColor(0.54, 0.4, 0.1)

-- Normally FRIZQT_.ttf, size 14
QuestFontHighlight:SetFont(NORMAL_TEXT_FONT, 15 * scale)


-- -----------------------------------------------------------------------------
-- Dialog Buttons
-- e.g. "Accept" 
-- Normally uses FRIZQT_.ttf, size 16
-- -----------------------------------------------------------------------------

DialogButtonNormalText:SetFont(NORMAL_TEXT_FONT, 17 * scale)
DialogButtonHighlightText:SetFont(NORMAL_TEXT_FONT, 17 * scale)


-- -----------------------------------------------------------------------------
-- Error Log
-- e.g. "Another Action is in Progress" 
-- Normally uses FRIZQT_.ttf, no alpha
-- -----------------------------------------------------------------------------

ErrorFont:SetFont(ITALIC_TEXT_FONT, 16 * scale)
ErrorFont:SetAlpha(60)


-- -----------------------------------------------------------------------------
-- Invoice Text
-- Auction House sale invoices
-- Normally uses FRIZQT_.ttf, size 12/10
-- -----------------------------------------------------------------------------

InvoiceFont_Med:SetFont(ITALIC_TEXT_FONT, 13 * scale)
InvoiceFont_Med:SetTextColor(0.15, 0.09, 0.04)

InvoiceFont_Small:SetFont(ITALIC_TEXT_FONT, 11 * scale)
InvoiceFont_Small:SetTextColor(0.15, 0.09, 0.04)


-- -----------------------------------------------------------------------------
-- Item Info
-- Used for <Right click to read> item windows, such as [Lament of the Highborne]
-- Normally uses MORPHEUS.ttf, size 15
-- -----------------------------------------------------------------------------

ItemTextFontNormal:SetFont(NORMAL_TEXT_FONT, 16 * scale)


-- -----------------------------------------------------------------------------
-- Mail Text
-- In-game mails
-- Normally uses MORPHEUS.ttf, no shadow
-- -----------------------------------------------------------------------------

MailFont_Large:SetFont(ITALIC_TEXT_FONT, 15 * scale)
MailFont_Large:SetTextColor(0.15, 0.09, 0.04)
MailFont_Large:SetShadowColor(0.54, 0.4, 0.1)
MailFont_Large:SetShadowOffset(1, -1)


-- -----------------------------------------------------------------------------
-- PvPInfo
-- PvP objectives info
-- Normally uses FRIZQT_.ttf
-- -----------------------------------------------------------------------------

PVPInfoTextFont:SetFont(NORMAL_TEXT_FONT, 22 * scale, "THICKOUTLINE")


-- -----------------------------------------------------------------------------
-- Spell Book Subtitles
-- Spell and ability subtitles
-- Normally uses FRIZQT_.ttf, size 10
-- -----------------------------------------------------------------------------

SubSpellFont:SetFont(BOLD_TEXT_FONT, 11 * scale)


-- -----------------------------------------------------------------------------
-- Status Bars
-- Numbers on the unit frames, Damage Meters
-- -----------------------------------------------------------------------------

-- Normally FRIZQT_.ttf, size 10
TextStatusBarText:SetFont(NUMBER_TEXT_FONT, 13 * scale, "OUTLINE")

-- Normally ARIALN.ttf, size 12
if (CanSetFont(TextStatusBarTextSmall)) then TextStatusBarTextSmall:SetFont(NORMAL_TEXT_FONT, 13 * scale) end


-- -----------------------------------------------------------------------------
-- Tooltips
-- Text used in tooltips!
-- Normally uses FRIZQT_.ttf, size 12/10/14
-- -----------------------------------------------------------------------------

-- Main tooltip text
GameTooltipText:SetFont(NORMAL_TEXT_FONT, 13 * scale)
GameTooltipTextSmall:SetFont(BOLD_TEXT_FONT, 12 * scale)

-- Tooltip title
GameTooltipHeaderText:SetFont(BOLD_TEXT_FONT, 15 * scale, "OUTLINE")


-- -----------------------------------------------------------------------------
-- World Map
-- Map hover location titles
-- Normally uses FRIZQT_.ttf, size 102 (!), no alpha
-- -----------------------------------------------------------------------------

WorldMapTextFont:SetFont(BOLDITALIC_TEXT_FONT, 31 * scale, "THICKOUTLINE")
WorldMapTextFont:SetShadowColor(0, 0, 0)
WorldMapTextFont:SetShadowOffset(1, -1)
WorldMapTextFont:SetAlpha(40)


-- -----------------------------------------------------------------------------
-- Zone Text
-- On-screen notifications of zone changes
-- Normally uses FRIZQT_.ttf, size 102/26
-- -----------------------------------------------------------------------------

-- Zone changes (colour cannot be set)
ZoneTextFont:SetFont(BOLDITALIC_TEXT_FONT, 31 * scale, "THICKOUTLINE")
ZoneTextFont:SetShadowColor(0, 0, 0)
ZoneTextFont:SetShadowOffset(1, -1)

-- Subzone changes (colour cannot be set)
SubZoneTextFont:SetFont(BOLDITALIC_TEXT_FONT, 27 * scale, "THICKOUTLINE")


-- -----------------------------------------------------------------------------
-- CombatLogFont
-- Appears to be unused
-- Normally uses FRIZQT_.ttf, size 12
-- -----------------------------------------------------------------------------

CombatLogFont:SetFont(NORMAL_TEXT_FONT, 13 * scale)


-- -----------------------------------------------------------------------------
-- BossEmoteNormalHuge
-- Used for boss emotes, one would think
-- Normally uses FRIZQT_.ttf, size 25
-- -----------------------------------------------------------------------------

BossEmoteNormalHuge:SetFont(NORMAL_TEXT_FONT, 25 * scale)


-- =============================================================================
--  C. DYNAMIC ADDON UPDATES
-- =============================================================================
--  Code. It does stuff, apparently (thanks to ckknight)
-- =============================================================================

if not scalechange and ClearFont.db.profile.applyAll then
	local frame
	local last_font1 = last_font1
	local last_font2 = last_font2
	local last_font3 = last_font3
	local last_font4 = last_font4
	local last_font5 = last_font5
	ClearFont:ScheduleRepeatingEvent("ClearFont2-DynamicUpdate", function()
		local t = GetTime() + 0.05
		while true do
			if GetTime() > t then
				return
			end
			frame = EnumerateFrames(frame)
			if not frame then
				break
			end
			for _, region in fake_ipairs(frame:GetRegions()) do
				if region:IsObjectType("FontString") and not region:IsProtected() then
					local obj = region:GetFontObject()
					if obj then
						local font,size,style = region:GetFont()
						if font == last_font1 or font == last_font2 or font == last_font3 or font == last_font4 or font == last_font5 then
							font = obj:GetFont()
							if font then
								region:SetFont(font,size,style)
							end
						end
					end
				end
			end
		end
		ClearFont:CancelScheduledEvent("ClearFont2-DynamicUpdate")
	end, 0)
end

last_font1 = NORMAL_TEXT_FONT
last_font2 = BOLD_TEXT_FONT
last_font3 = BOLDITALIC_TEXT_FONT
last_font4 = ITALIC_TEXT_FONT
last_font5 = NUMBER_TEXT_FONT



-- =============================================================================
--  D. AND FINALLY...
-- =============================================================================

end
