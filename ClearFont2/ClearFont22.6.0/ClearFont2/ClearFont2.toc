﻿## Interface: 30000
## X-Curse-Packaged-Version: v2.6

## Title: ClearFont|cffffffff2|r |cff7fff7f -Ace2-|r
## Notes: Funky font switcher
## Notes-esES: Modificador de fuente marchoso
## Notes-frFR: Commutateur génial de la police
## Notes-deDE: Der abgefahrene Schriftartenaustauscher
## Notes-koKR: Funky font switcher
## Notes-zhTW: Funky font switcher
## Notes-zhCN: Funky font switcher
## Author: Kirkburn
## Version: 2.6

## X-Credits: Evil Elvis, ckknight, Iriel, nevcairiel, vhaarr et al
## X-Website: http://www.clearfont.co.uk
## X-Email: kirkburn@gmail.com
## X-Category: Interface Enhancements
## X-RelSite-WoWI: 4283
## X-RelSite-Curse: 1820
## X-RelSite-UI.WoW: 1424
## X-AceForum: 3513
## X-WoWIPortal: kirkburn

## DefaultState: enabled
## LoadOnDemand: 0

## OptionalDeps: Ace2, DewdropLib, TabletLib, FuBarPlugin-2.0, SharedMediaLib, LibSharedMedia-2.0, LibSharedMedia-3.0, FuBar, SharedMedia
## X-Embeds: Ace2, DewdropLib, FuBarPlugin-2.0, LibSharedMedia-3.0, TabletLib

## SavedVariables: ClearFontDB

embeds.xml

Design.lua
## CustomDesign.lua
Core.lua
