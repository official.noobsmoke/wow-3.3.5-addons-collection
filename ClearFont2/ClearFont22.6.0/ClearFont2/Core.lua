﻿-- /////////////////////////////////////////////////////////////////////////////
-- =============================================================================
--  CLEARFONT2 BY KIRKBURN
--  Official website:  http://www.clearfont.co.uk/
-- -----------------------------------------------------------------------------
--  CORE.LUA - CODE TO RUN CLEARFONT2
--	 A. CREATE INSTANCE OF THE ACE2 ADDON
--	 B. FONT LOCALS (THE DEFAULT FONTS AVAILABLE)
--	 C. SETTING UP THE ADDON
--	 D. APPLY THE FONT CHOICES
-- =============================================================================
-- /////////////////////////////////////////////////////////////////////////////


-- =============================================================================
--  A. CREATE INSTANCE OF THE ACE2 ADDON
-- =============================================================================

ClearFont = AceLibrary("AceAddon-2.0"):new("AceEvent-2.0", "AceConsole-2.0", "AceDB-2.0", "FuBarPlugin-2.0")



-- =============================================================================
--  B. FONT LOCALS (THE DEFAULT FONTS AVAILABLE)
-- =============================================================================

-- -----------------------------------------------------------------------------
-- Path for WoW to find the fonts
-- -----------------------------------------------------------------------------

local fontLoc = "Interface\\AddOns\\ClearFont2\\Fonts\\"


-- -----------------------------------------------------------------------------
-- The list of default fonts available
-- -----------------------------------------------------------------------------

ClearFont.fonts = 
{
	{["name"]="Default (Calibri v1)",
		["normal"]	=fontLoc.."Calibri_v1\\Calibri1.ttf",
		["bold"]		=fontLoc.."Calibri_v1\\CalibriBold.ttf",
		["italic"]	=fontLoc.."Calibri_v1\\CalibriItalic.ttf",
		["bolditalic"]	=fontLoc.."Calibri_v1\\CalibriBoldItalic.ttf", 
		["number"]	=fontLoc.."Calibri_v1\\CalibriBold.ttf"},

	{["name"]="Baar Philos",
		["normal"]	=fontLoc.."BaarPhilos\\BaarPhilos.ttf",
		["bold"]		=fontLoc.."BaarPhilos\\BaarPhilosBold.ttf", 
		["italic"]	=fontLoc.."BaarPhilos\\BaarPhilosItalic.ttf", 
		["bolditalic"]	=fontLoc.."BaarPhilos\\BaarPhilosBoldItalic.ttf",
		["number"]	=fontLoc.."BaarPhilos\\BaarPhilosBold.ttf"},

	{["name"]="Baar Sophia",
		["normal"]	=fontLoc.."BaarSophia\\BaarSophia.ttf",
		["bold"]		=fontLoc.."BaarSophia\\BaarSophiaBold.ttf",
		["italic"]	=fontLoc.."BaarSophia\\BaarSophiaItalic.ttf",
		["bolditalic"]	=fontLoc.."BaarSophia\\BaarSophiaBoldItalic.ttf",
		["number"]	=fontLoc.."BaarSophia\\BaarSophiaBold.ttf"},

	{["name"]="Calibri v0.9",
		["normal"]	=fontLoc.."Calibri_v0.9\\Calibri.ttf",
		["bold"]		=fontLoc.."Calibri_v0.9\\CalibriBold.ttf", 
		["italic"]	=fontLoc.."Calibri_v0.9\\CalibriItalic.ttf",
		["bolditalic"]	=fontLoc.."Calibri_v0.9\\CalibriBoldItalic.ttf",
		["number"]	=fontLoc.."Calibri_v0.9\\CalibriBold.ttf"},

	{["name"]="Francophil Sans",
		["normal"]	=fontLoc.."Franc\\Franc.ttf",
		["bold"]		=fontLoc.."Franc\\FrancBold.ttf",
		["italic"]	=fontLoc.."Franc\\Franc.ttf",
		["bolditalic"]	=fontLoc.."Franc\\FrancBold.ttf",
		["number"]	=fontLoc.."Franc\\FrancBold.ttf"},

	{["name"]="Lucida Grande",
		["normal"]	=fontLoc.."LucidaGrande\\LucidaGrande.ttf",
		["bold"]		=fontLoc.."LucidaGrande\\LucidaGrande.ttf",
		["italic"]	=fontLoc.."LucidaGrande\\LucidaGrande.ttf",
		["bolditalic"]	=fontLoc.."LucidaGrande\\LucidaGrande.ttf",
		["number"]	=fontLoc.."LucidaGrande\\LucidaGrande.ttf"},

	{["name"]="Lucida Sans Demibold",
		["normal"]	=fontLoc.."LucidaSD\\LucidaSD.ttf",
		["bold"]		=fontLoc.."LucidaSD\\LucidaSD.ttf",
		["italic"]	=fontLoc.."LucidaSD\\LucidaSDItalic.ttf",
		["bolditalic"]	=fontLoc.."LucidaSD\\LucidaSDItalic.ttf",
		["number"]	=fontLoc.."LucidaSD\\LucidaSD.ttf"},

	{["name"]="Perspective Sans",
		["normal"]	=fontLoc.."PerspectiveSans\\PerspectiveSans.ttf",
		["bold"]		=fontLoc.."PerspectiveSans\\PerspectiveSansBold.ttf",
		["italic"]	=fontLoc.."PerspectiveSans\\PerspectiveSansItalic.ttf",
		["bolditalic"]	=fontLoc.."PerspectiveSans\\PerspectiveSansBoldItalic.ttf",
		["number"]	=fontLoc.."PerspectiveSans\\PerspectiveSansBold.ttf"},

	{["name"]="Tin Birdhouse",
		["normal"]	=fontLoc.."TinBirdhouse\\TinBirdhouse.ttf",
		["bold"]		=fontLoc.."TinBirdhouse\\TinBirdhouse.ttf",
		["italic"]	=fontLoc.."TinBirdhouse\\TinBirdhouse.ttf",
		["bolditalic"]	=fontLoc.."TinBirdhouse\\TinBirdhouse.ttf",
		["number"]	=fontLoc.."TinBirdhouse\\TinBirdhouse.ttf"},
}

local numberOfFonts = #ClearFont.fonts

-- =============================================================================
--  C. SETTING UP THE ADDON
-- =============================================================================

-- -----------------------------------------------------------------------------
-- Options localisation (enUS, enGB, esES, frFR, deDE, koKR)
-- Need zhTW and zhCN!
-- -----------------------------------------------------------------------------

local L = AceLibrary("AceLocale-2.2"):new("ClearFont")

-- enUS (US English)
L:RegisterTranslations("enUS", function() return {
	["Font face"] = true,
	["Changes the font face for the user interface. Some changes (such as the ChatFrame) may require a UI reload (/rl)."] = true,
	["Font size"] = true,
	["Changes the font size for the user interface by a percentage."] = true,
	["|cffeda55fRight-Click|r or type |cffeda55f/cf|r to customize settings."] = true,
	["Apply to addons"] = true,
	["Attempts to apply the font settings to all addons. This option is rudimentary, and is not generally required."] = true,
} end)

-- esES (Spanish)
L:RegisterTranslations("esES", function() return {
	["Font face"] = "Fuente",
	["Changes the font face for the user interface. Some changes (such as the ChatFrame) may require a UI reload (/rl)."] = "Cambia la fuente a utilizar en la interfaz de usuario. Algunos cambios (como en los de la ventana de chat) pueden requerir una recarga de la interfaz (/rl).",
	["Font size"] = "Tamaño de fuente",
	["Changes the font size for the user interface by a percentage."] = "Cambia el tamaño de la fuente para la interfaz de usuario en un porcentaje.",
	["|cffeda55fRight-Click|r or type |cffeda55f/cf|r to customize settings."] = "|cffeda55fClicDerecho|r o escribe |cffeda55f/cf|r para personalizar los ajustes.",
	["Apply to addons"] = "Aplicar a accesorios",
	["Attempts to apply the font settings to all addons. This option is rudimentary, and is not generally required."] = "Intenta aplicar los ajustes de fuente a todos los accesorios. Esta opción es rudimentaria y generalmente no es requerida.",
} end)

-- frFR (French)
L:RegisterTranslations("frFR", function() return {
	["Font face"] = "Police",
	["Changes the font face for the user interface. Some changes (such as the ChatFrame) may require a UI reload (/rl)."] = "Change le style de police pour l'interface. Certains changements ne seront appliqués qu'après un relancement de l'UI (/rl).",
	["Font size"] = "Taille de police",
	["Changes the font size for the user interface by a percentage."] = "|cffeda55fClic Droit|r pour accéder aux options (ou tapez |cffeda55f/cf|r).",
	["|cffeda55fRight-Click|r or type |cffeda55f/cf|r to customize settings."] = "Appliquer les changements aux add-ons également.",
	["Apply to addons"] = "Appliquer les changements aux add-ons également.",
	["Attempts to apply the font settings to all addons. This option is rudimentary, and is not generally required."] = "Applique les changements de police aux add-ons. Cette option n'est généralement pas nécéssaire.",
} end)

-- deDE (German)
L:RegisterTranslations("deDE", function() return {
	["Font face"] = "Schriftart",
	["Changes the font face for the user interface. Some changes (such as the ChatFrame) may require a UI reload (/rl)."] = "Ändert die Schriftart des Spielinterfaces. Einige Einstellungen (zum Beispiel die Schriftart des Chatfensters) werden möglicherweise erst nach dem Neuladen des Spielinterfaces (/rl) übernommen.",
	["Font size"] = "Schriftgröße",
	["Changes the font size for the user interface by a percentage."] = "Ändert die Schriftgröße des Spielinterfaces um einen bestimmten Prozentsatz.",
	["|cffeda55fRight-Click|r or type |cffeda55f/cf|r to customize settings."] = "|cffeda55fRechtsklick|r oder |cffeda55f/cf|r um die Einstellungen zu ändern.",
	["Apply to addons"] = "Auf Addons anwenden",
	["Attempts to apply the font settings to all addons. This option is rudimentary, and is not generally required."] = "Versucht, die Schrifteinstellungen auch auf andere Addons anzuwenden. Diese Einstellung ist nicht ausgereift und wird normalerweise nicht benötigt.",
} end)

-- zhTW (Traditional Chinese) by Youngway@水晶之刺
L:RegisterTranslations("zhTW", function() return {
   ["Font face"] = "字型選單",
   ["Changes the font face for the user interface. Some changes (such as the ChatFrame) may require a UI reload (/rl)."] = "更改使用者介面的字型。有些變動可能需要重新載入介面 。(指令：/rl)",
   ["Font size"] = "字體大小",
   ["Changes the font size for the user interface by a percentage."] = "變換您使用者介面中的字體大小。",
   ["|cffeda55fRight-Click|r or type |cffeda55f/cf|r to customize settings."] = "|cffeda55f右鍵|r 或輸入 |cffeda55f/cf|r 設定。",
   ["Apply to addons"] = "套用到其他插件",
   ["Attempts to apply the font settings to all addons. This option is rudimentary, and is not generally required."] = "將字型的設定套用到所有的插件上。這個選項還沒完全作好，也不是必要的。",
} end)

-- zhCN (Simplified Chinese) by 馭雷@米奈希爾
L:RegisterTranslations("zhCN", function() return {
   ["Font face"] = "字型菜单",
   ["Changes the font face for the user interface. Some changes (such as the ChatFrame) may require a UI reload (/rl)."] = "更改使用者介面的字型。有些变动可能需要重新载入介面 。(指令：/rl)",
   ["Font size"] = "字体大小",
   ["Changes the font size for the user interface by a percentage."] = "变换您使用者介面中的字体大小。",
   ["|cffeda55fRight-Click|r or type |cffeda55f/cf|r to customize settings."] = "|cffeda55f右键|r 或输入 |cffeda55f/cf|r 设置。",
   ["Apply to addons"] = "套用到其他插件",
   ["Attempts to apply the font settings to all addons. This option is rudimentary, and is not generally required."] = "将字型的设置套用到所有的插件上。这个选项还没完全作好，也不是必要的。",
} end)

-- koKR (Korean)
L:RegisterTranslations("koKR", function() return {
	["Font face"] = "폰트",
	["Changes the font face for the user interface. Some changes (such as the ChatFrame) may require a UI reload (/rl)."] = "게임의 폰트를 변경합니다. 몇가지 변경은 UI를 재시작해야 합니다.(/rl)",
	["Font size"] = "폰트 크기",
	["Changes the font size for the user interface by a percentage."] = "폰트의 크기를 %단위로 변경합니다.",
	["|cffeda55fRight-Click|r or type |cffeda55f/cf|r to customize settings."] = "|cffeda55f오른쪽-클릭|r 또는 |cffeda55f/cf|r 를 입력하면 설정창을 불러옵니다.",
	["Apply to addons"] = "애드온에 적용",
	["Attempts to apply the font settings to all addons. This option is rudimentary, and is not generally required."] = "와우에 사용하는 애드온에도 폰트를 적용합니다.",
} end)

-- -----------------------------------------------------------------------------
-- Set default options
-- -----------------------------------------------------------------------------

local defaults = {
	currentFont = "Default (Calibri v1)",
	currentScale = 1.0,
	applyAll = true,
}


-- -----------------------------------------------------------------------------
-- Dewdrop menu options
-- -----------------------------------------------------------------------------

local menu = {
	type = "group",
	args = {
		font = {
			type = "text",
			name = L["Font face"],
			desc = L["Changes the font face for the user interface. Some changes (such as the ChatFrame) may require a UI reload (/rl)."],
			get  = function() return ClearFont.db.profile.currentFont end,
			set  = function(v)
				ClearFont.db.profile.currentFont = v
				ClearFont:ApplyFont()
			end,
			usage = "<font>",
			order = 1,
		},
		scale = {
			type = "range",
			name = L["Font size"],
			desc = L["Changes the font size for the user interface by a percentage."],
			get = function() return ClearFont.db.profile.currentScale end,
			set = function(v)
				ClearFont.db.profile.currentScale = v
				ClearFont:ApplyFont()
				end,
			min = 0.6,
			max = 1.4,
			step = 0.02,
			isPercent = true,
			order = 2,
		},
		applyaddons = {
			name = L["Apply to addons"],
			type = 'toggle',
			desc = L["Attempts to apply the font settings to all addons. This option is rudimentary, and is not generally required."],
			get = function()
				return ClearFont.db.profile.applyAll
			end,
			set = function(option)
				ClearFont.db.profile.applyAll = option
				ClearFont:ApplyFont()
			end,
			order = 3,
		},
	},
}


-- -----------------------------------------------------------------------------
-- FuBar 2.0 options
-- -----------------------------------------------------------------------------

ClearFont.OnMenuRequest = menu
ClearFont.hasIcon = true
ClearFont.defaultMinimapPosition = 200
ClearFont.cannotDetachTooltip = true
ClearFont.independentProfile = true
ClearFont.defaultPosition = "RIGHT"
ClearFont.hideWithoutStandby = true
ClearFont.hasNoColor = true

local tablet = AceLibrary("Tablet-2.0")
function ClearFont:OnTooltipUpdate()
	tablet:SetHint(L["|cffeda55fRight-Click|r or type |cffeda55f/cf|r to customize settings."])
end


-- -----------------------------------------------------------------------------
-- Startup call when addon is loaded, also adds fonts to SML
-- -----------------------------------------------------------------------------

local SM1 = AceLibrary:HasInstance("SharedMedia-1.0") and AceLibrary("SharedMedia-1.0") or nil
local SM2 = LibStub:GetLibrary("LibSharedMedia-2.0", true) or nil
local SM3 = LibStub:GetLibrary("LibSharedMedia-3.0", true) or nil
local media = SM3 or SM2 or SM1 or nil

function ClearFont:OnInitialize()
	self:RegisterDB("ClearFontDB")
	self:RegisterDefaults("profile", defaults)
	self:RegisterChatCommand("/clearfont", "/clearfont2", "/cf", "/cf2", menu, "CLEARFONT")
	self:RegisterEvent("ADDON_LOADED", "ApplyFont")

	if media then
		for i, v in ipairs(self.fonts) do
			if SM3 then SM3:Register("font", v.name, v.normal) end
			if SM2 then SM2:Register("font", v.name, v.normal) end
			if SM1 then SM1:Register("font", v.name, v.normal) end
		end
		menu.args.font.validate = media:List("font")
	else
		menu.args.font.validate = self:GetFonts()
	end
end


-- -----------------------------------------------------------------------------
-- Get the font choices
-- -----------------------------------------------------------------------------

function ClearFont:GetFonts()
	local fontNames = {}
	for i in pairs(ClearFont.fonts) do
		table.insert(fontNames,ClearFont.fonts[i].name)
	end
	table.sort(fontNames)
	return fontNames
end


-- ----------------------------------------------------------------------------
-- Called by addon packs once they've added fonts
-- ----------------------------------------------------------------------------

function ClearFont:UpdateFontList()
	local newNumberOfFonts = #self.fonts

	if media then
		for i=numberOfFonts+1, newNumberOfFonts do
			if SM3 then SM3:Register("font", self.fonts[i].name, self.fonts[i].normal) end
			if SM2 then SM2:Register("font", self.fonts[i].name, self.fonts[i].normal) end
			if SM1 then SM1:Register("font", self.fonts[i].name, self.fonts[i].normal) end
		end
		numberOfFonts = newNumberOfFonts
	end

	menu.args.font.validate = self:GetFonts()
end


-- =============================================================================
--  D. APPLY THE FONT CHOICES
-- =============================================================================

function ClearFont:ApplyFont()
	local font = self.db.profile.currentFont
	
	local index
	for i in pairs(ClearFont.fonts) do
		if ClearFont.fonts[i].name == font then
			index = i
			break
		end
	end
	if media and not index then
		local f = media:Fetch("font", font)
		if type(ApplyClearFontDesign) == "function" then
			ApplyClearFontDesign(f)
		end
	else
		if not index then index = 1 end
		if not ClearFont.fonts[index].normal then return end
		if type(ApplyClearFontDesign) == "function" then
			ApplyClearFontDesign(index)
		end
	end

--[[
	Code that can be used to insert your own custom design file instead of the default
	if type(ApplyCustomClearFontDesign) == "function" then
		ApplyCustomClearFontDesign()
	end
]]

end