InfoPanel
---------
InfoPanel is a lightweight information panel that will place itself at the bottom or top the screen, it slightly resembles other addons such as Titan Panel or FuBar, but I've tried to keep this one simple.
It supports plugins, which each will add different kind of information to the bar for quick review during combat without having to bring up other kind of interface.

The slash command for this addon is "/ip".

Why Use InfoPanel?
------------------
With all the other major panel mods out there, you might wonder why even bother with this addon over the others which has more options and more plugins.
Well, this one was made with the thought of being lightweight in mind.
If you only have the use for a few plugins for your panel, such as one showing time, durability and your money. This addon is ideal, and it hardly takes any resources.

User Friendliness & Settings
----------------------------
As this is an early release, I have not put in much time to improve the user friendliness, I run WoW with action bar and unit frame addons,
so if you use the normal blizzard UI, there might be some problems with those frames overlapping InfoPanel. Let me know.

Setting up InfoPanel is quite limited in this early release, but to adjust the order in which the plugins appear,
you have to edit the "Plugins.xml" file and adjust the order of the plugins as you prefer to have them show.
If you wish to disable some of the plugins, simply just remove or comment out its entry in this file.

Included Plugins (17)
---------------------

Durability
- Lists the current condition of your gear, both equipped and bagged gear.

Bag
- Keeps track of the amount of free bag space.

Money
- This plugin will always show the amount of gold you have on you.

Performance
- Displays the frame rate, network latency and addon memory usage.

Attack Power
- Shows: Attack Speed - Crit Rate - Attack Power. It is active for all classes except mages, warlocks and priests.

Spell Power
- Shows: Haste - Crit Rate - Spell Power. The counterpart to the Attack Power plugin, not active for hunters, warriors, rogues and death knights.

Zone
- Shows the current player X and Y coordinates and different zone information.

XP
- Displays various information related to experience. Only loads for players under max level.

Combat Timer
- Records the duration of the last combat you were in.

Timer
- Simple timer to record and time any event you like.

Honor
- Shows basic honor details. Also shows the amount of honor gained during the current session, current zone and during the last zone.

Group Info
- Shows raid/party health and mana, as well as health and mana for all classes. In addition it will also show number of people afk, dead, out of range etc.

Reputation
- Shows a brief summary of how many reputations you have at each level, friendly, honored etc. Also shows the total amount of rep you have gained for each faction during the current session.

Speed
- Displays the current movement speed relative to normal running speed. Mousing over the plugin in the panel will also show the speed for your target, pet and vehicle.

Ammo Counter
- Counts the amount of ammo in your bags, includes all types of ammo into the count. Only loads for Hunters.

Shard Counter
- Shard Counter. Only loads for Warlocks.

Clock
- This plugin is aligned to the right side of the panel and will show the current system time, not game time.