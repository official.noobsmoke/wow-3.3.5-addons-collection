local _G = getfenv(0);
local f = InfoPanel.AddPlugin();
local sessionRep = {};
local FACTION_COLORS = CUSTOM_FACTION_COLORS or FACTION_BAR_COLORS;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Reputation Info",1,1,1);
	-- Scan Factions
	local _, factionName, standing, minValue, maxValue, earnedValue, isHeader;
	local factionStandings = {};
	local factionCount = 0;
	local repChange, color;
	-- Loop
	for i = 1, GetNumFactions() do
		factionName, _, standing, minValue, maxValue, earnedValue, _, _, isHeader = GetFactionInfo(i);
		if (not isHeader) then
			factionStandings[standing] = (factionStandings[standing] or 0) + 1;
			factionCount = (factionCount + 1);
		end
		if (IsShiftKeyDown()) then
			color = FACTION_COLORS[standing];
			tooltip:AddDoubleLine((isHeader and "|cffffffff- " or "")..factionName,_G["FACTION_STANDING_LABEL"..standing].." - "..(earnedValue - minValue).."/"..(maxValue - minValue),nil,nil,nil,color.r,color.g,color.b);
		elseif (sessionRep[factionName]) then
			repChange = (earnedValue - sessionRep[factionName]);
			if (repChange ~= 0) then
				repChange = ((repChange > 0 and "|cff80ff80+") or (repChange < 0 and "|cffff8080-") or "")..abs(repChange);
				tooltip:AddDoubleLine((isHeader and "|cffffffff- " or "")..factionName,repChange,nil,nil,nil,1,1,1);
			end
		end
	end
	-- Add Faction Standings
	if (not IsShiftKeyDown()) then
		if (tooltip:NumLines() > 1) then
			tooltip:AddLine(" ");
			tooltip:AddLine("Standings",1,1,1);
		end;
		tooltip:AddDoubleLine("Known Factions",factionCount,nil,nil,nil,1,1,1);
		for standing, factionCount in next, factionStandings do
			color = FACTION_COLORS[standing]
			tooltip:AddDoubleLine(_G["FACTION_STANDING_LABEL"..standing],factionCount,nil,nil,nil,color.r,color.g,color.b);
		end
		-- watched Faction
		local name, standing, min, max, value = GetWatchedFactionInfo();
		if (name) then
			tooltip:AddLine(" ");
			tooltip:AddDoubleLine("Watched Faction",name,nil,nil,nil,1,1,1);
		end
	end
end

-- OnClick
local function OnClick(self,button)
	if (ChatEdit_GetActiveWindow():IsShown()) then
		local name, standing, min, max, value = GetWatchedFactionInfo();
		if (name) then
			ChatEdit_GetActiveWindow():Insert(format("%s: %d / %d",name,value - min,max - min));
		else
			ChatEdit_GetActiveWindow():Insert("n/a");
		end
	else
		ToggleCharacter("ReputationFrame");
	end
end

-- Gather Info
function f:UPDATE_FACTION(event)
	-- Check faction count
	local nFactions = GetNumFactions();
	if (nFactions == 0) then
		return;
	end
	-- Set initial Rep for factions not yet saved
	local _, factionName, earnedValue;
	for i = 1, nFactions do
		factionName, _, _, _, _, earnedValue = GetFactionInfo(i);
		if (not sessionRep[factionName]) then
			sessionRep[factionName] = earnedValue;
		end
	end
	-- Update Rep
	local name, standing, min, max, value = GetWatchedFactionInfo();
	if (name) then
		local color = FACTION_COLORS[standing];
		self:SetText(_G["FACTION_STANDING_LABEL"..standing].." - "..(value - min).."/"..(max - min));
		f.text:SetTextColor(color.r,color.g,color.b);
	else
		self:SetText("Rep");
		f.text:SetTextColor(1,1,1);
	end;
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\Achievement_Reputation_01");
f:SetScript("OnClick",OnClick);
f:RegisterEvent("UPDATE_FACTION");