local f = InfoPanel.AddPlugin();
local pName = UnitName("player");
local combStart, combTime, dmgTotal;
local prefix, suffix;
local GetTime = GetTime;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("DPS Info",1,1,1);
	if (dmgTotal) then
		tooltip:AddDoubleLine("Total Damage",dmgTotal,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Combat Time",InfoPanel.FormatTime2(combTime),nil,nil,nil,1,1,1);
	end
end

-- Show DPS
local function UpdateDPS()
	if (combTime == 0) then
		self:SetText("0");
	else
		self:SetText("%.1f",dmgTotal / combTime);
	end
end

-- Combat End
function f:PLAYER_REGEN_ENABLED()
	f:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
	combTime = (GetTime() - combStart);
	UpdateDPS();
end

-- Combat Start
function f:PLAYER_REGEN_DISABLED()
	combStart = GetTime();
	combTime = 0;
	dmgTotal = 0;
	f:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
end

-- Combat Log Events
function f:COMBAT_LOG_EVENT_UNFILTERED(event,time,type,sourceGUID,sourceName,sourceFlags,destGUID,destName,destFlags,...)
	if (sourceName == pName) and (destName ~= pName) then
		prefix, suffix = type:match("(.-)_(.+)");
		if (suffix == "DAMAGE" or suffix == "PERIODIC_DAMAGE" or suffix == "SHIELD") then
--			AzMsg(type.." = "..select(prefix == "SWING" and 1 or 4,...));
			dmgTotal = (dmgTotal + select(prefix == "SWING" and 1 or 4,...));
			combTime = (GetTime() - combStart);
			UpdateDPS();
		end
	end
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f:SetText("0");
f.icon:SetTexture("Interface\\Icons\\Ability_Warrior_Rampage");
f:RegisterEvent("PLAYER_REGEN_ENABLED");
f:RegisterEvent("PLAYER_REGEN_DISABLED");