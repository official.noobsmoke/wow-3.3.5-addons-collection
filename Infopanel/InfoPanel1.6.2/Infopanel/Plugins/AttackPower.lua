do
	local class = select(2,UnitClass("player"));
	if (class == "MAGE" or class == "WARLOCK" or class == "PRIEST") then
		return;
	end
end

local f = InfoPanel.AddPlugin();
local attackSpeed, critChance, attackPower, hasBattleShout, attackPowerMax;
local GetAP, GetCrit, GetSpeed;
local battleShout = GetSpellInfo(6673);

--------------------------------------------------------------------------------------------------------
--                                           Button Scripts                                           --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Attack Power Info",1,1,1);

	tooltip:AddDoubleLine("Attack Speed",format("%.2f",attackSpeed),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Crit Chance",format("%.2f%%",critChance),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Hit Chance",format("%.2f%%",GetCombatRatingBonus(CR_HIT_MELEE)),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Armor Penetration",format("%.2f%%",GetArmorPenetration()),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Attack Power",attackPower,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Max Session AP",attackPowerMax,nil,nil,nil,attackPowerMax > attackPower and 0 or 1,1,attackPowerMax > attackPower and 0 or 1);

	local lowDmg, hiDmg, offlowDmg, offhiDmg, posBuff, negBuff, percentmod = UnitDamage("player");
	local _, offhandSpeed = GetSpeed("player")
	tooltip:AddLine(" ");
	tooltip:AddLine("Weapon Damage",1,1,1);
	tooltip:AddDoubleLine("Modifier",format("%.1f%%",percentmod * 100),nil,nil,nil,percentmod == 1 and 1 or 0,1,percentmod == 1 and 1 or 0);
	tooltip:AddDoubleLine("Mainhand",format("%.1f - %.1f",lowDmg,hiDmg),nil,nil,nil,1,1,1);
	if (OffhandHasWeapon()) then
		tooltip:AddDoubleLine("Offhand",format("%.1f - %.1f",offlowDmg,offhiDmg),nil,nil,nil,1,1,1);
	end
	tooltip:AddDoubleLine("Mainhand DPS",format("%.1f",(lowDmg + hiDmg) / 2 / attackSpeed),nil,nil,nil,1,1,1);
	if (OffhandHasWeapon()) then
		tooltip:AddDoubleLine("Offhand DPS",format("%.1f",(offlowDmg + offhiDmg) / 2 / offhandSpeed),nil,nil,nil,1,1,1);
	end
end

-- OnClick
local function OnClick(self,button)
	if (ChatEdit_GetActiveWindow():IsShown()) then
		ChatEdit_GetActiveWindow():Insert(format("%.2f%% Crit, %d AP",critChance,attackPower));
	end
end

--------------------------------------------------------------------------------------------------------
--                                               Events                                               --
--------------------------------------------------------------------------------------------------------

function f:UpdateDisplay()
	self:SetText("|cffffffff%.2f  |cff80c0ff%.2f%%  %s%d",attackSpeed,critChance,(hasBattleShout and "|cff52ff52" or "|cffff5252"),attackPower);
end

-- AP Change
function f:UNIT_ATTACK_POWER(event)
	local base, posBuff, negBuff = GetAP("player");
	attackPower = max(0,base + posBuff + negBuff);
	if (attackPower > attackPowerMax) then
		attackPowerMax = attackPower;
	end
	self:UpdateDisplay();
end
f.UNIT_RANGED_ATTACK_POWER = f.UNIT_ATTACK_POWER;

-- Attack Speed Change (show main hand only)
function f:UNIT_ATTACK_SPEED(event)
	attackSpeed = GetSpeed("player");
	self:UpdateDisplay();
end

-- Damage Mods Change (crit chance etc)
function f:PLAYER_DAMAGE_DONE_MODS(event)
	critChance = GetCrit();
	self:UpdateDisplay();
end

-- Check for BattleShout
function f:UNIT_AURA(event,unit)
	if (unit == "player") then
		hasBattleShout = (UnitAura("player",battleShout,nil,"HELPFUL") ~= nil);
		self:UpdateDisplay();
	end
end

-- Player Died
function f:PLAYER_DEAD(event)
	hasBattleShout = nil;
	self:UpdateDisplay();
end

-- Login Event
function f:PLAYER_LOGIN(event)
	hasBattleShout = (select(2,UnitClass("player")) ~= "WARRIOR") or (UnitAura("player",battleShout,nil,"HELPFUL"));

	local base, posBuff, negBuff = GetAP("player");
	attackPower = max(0,base + posBuff + negBuff);
	critChance = GetCrit();
	attackSpeed = GetSpeed("player");
	attackPowerMax = attackPower;

	self[event] = nil;
	self:UnregisterEvent(event);
	self:UpdateDisplay();
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

do
	f.icon:SetTexture("Interface\\Icons\\Ability_Warrior_Battleshout");
	f:SetScript("OnClick",OnClick);

	local _, class = UnitClass("player");
	GetAP = (class == "HUNTER" and UnitRangedAttackPower or UnitAttackPower);
	GetCrit = (class == "HUNTER" and GetRangedCritChance or GetCritChance);
	GetSpeed = (class == "HUNTER" and UnitRangedDamage or UnitAttackSpeed);

	if (class == "WARRIOR") then
		f:RegisterEvent("UNIT_AURA");
		f:RegisterEvent("PLAYER_DEAD");
	else
		f.UNIT_AURA = nil;
		f.PLAYER_DEAD = nil;
	end
	if (class == "HUNTER") then
		f:RegisterEvent("UNIT_RANGED_ATTACK_POWER");
	else
		f:RegisterEvent("UNIT_ATTACK_POWER");
	end
	f:RegisterEvent("PLAYER_DAMAGE_DONE_MODS");
	f:RegisterEvent("UNIT_ATTACK_SPEED");
	f:RegisterEvent("PLAYER_LOGIN");
end