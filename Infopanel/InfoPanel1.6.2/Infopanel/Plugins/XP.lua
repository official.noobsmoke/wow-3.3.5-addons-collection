if (UnitLevel("player") == MAX_PLAYER_LEVEL) then
	return;
end

local f = InfoPanel.AddPlugin();
local sessionStart = GetTime();
local xpNow, xpMax;
local xpLast, xpLastMax = 0;
local xpSession, xpCombat, xpGain = 0, 0;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- Splash Message
local function ScreenSplash(header,subtext,r,g,b)
	-- Set Header, Subtext and colors
	ZoneTextString:SetText(header);
	ZoneTextString:SetTextColor(r,g,b);
	SubZoneTextString:SetText(subtext);
	SubZoneTextString:SetTextColor(r,g,b);
	-- Clear this so it wont show: Alliance/Horde/Contested territory or Sanctuary
	PVPInfoTextString:SetText("");
	PVPArenaTextString:SetText("");
	-- Show Frames
	ZoneTextFrame.startTime = GetTime();
	ZoneTextFrame:Show();
	SubZoneTextFrame.startTime = GetTime();
	SubZoneTextFrame:Show();
end

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Experience Info",1,1,1);
	tooltip:AddDoubleLine("Current XP",xpNow,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Maximum XP",xpMax,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("XP to Next Level",xpMax - xpNow,nil,nil,nil,1,1,1);
	local xpRest = GetXPExhaustion();
	if (xpRest) then
		tooltip:AddDoubleLine("Rested XP",xpRest,nil,nil,nil,0,1,0);
	end

	if (xpGain) then
		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("Last XP Gain",xpGain,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Equal Gains to Level",format("%.2f",(xpMax - xpNow) / xpGain),nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Gain Percentage",format("%.2f%%",xpGain / xpMax * 100),nil,nil,nil,1,1,1);
		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("Last Combat XP Gain",xpCombat,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Gain Percentage",format("%.2f%%",xpCombat / xpMax * 100),nil,nil,nil,1,1,1);
	end

	if (xpSession > 0) then
		local xpPerHour = (xpSession / ((GetTime() - sessionStart) / 3600));
		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("XP Gain This Session",xpSession,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("XP Gain Per Hour",format("%.1f",xpPerHour),nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Time to Next Level",InfoPanel.FormatTime3((xpMax - xpNow) / xpPerHour * 3600),nil,nil,nil,1,1,1);
	end
end

-- OnClick
local function OnClick(self,button)
	if (ChatEdit_GetActiveWindow():IsShown()) then
		ChatEdit_GetActiveWindow():Insert(format("%d / %d (%.2f%%)",xpNow,xpMax,xpNow / xpMax * 100));
	elseif (IsShiftKeyDown()) and (button == "RightButton") then
		sessionStart = GetTime();
		xpLast, xpLastMax = 0, nil;
		xpSession, xpCombat, xpGain = 0, 0, nil;
		OnEvent(f,"PLAYER_LOGIN");
		AzMsg("|2InfoPanel|r: Experience Info has been reset");
	end
end

-- Login
function f:PLAYER_LOGIN()
	xpNow, xpMax = UnitXP("player"), UnitXPMax("player");
	xpLast, xpLastMax = xpNow, xpMax;
	self:SetText("%.2f%%",xpNow / xpMax * 100);
	self.PLAYER_LOGIN = nil;
end

-- XP Update
function f:PLAYER_XP_UPDATE(event)
	xpNow, xpMax = UnitXP("player"), UnitXPMax("player");
	xpGain = (xpNow > xpLast) and (xpNow - xpLast) or (xpLastMax - xpLast + xpNow);
	xpSession = (xpSession + xpGain);
	xpCombat = (xpCombat + xpGain);
	xpLast, xpLastMax = xpNow, xpMax;
	self:SetText("%.2f%%",xpNow / xpMax * 100);
end

-- One Up!
function f:PLAYER_LEVEL_UP(event,level)
	ScreenSplash("Level "..level,"Congratulations!",0.5,0.75,1);
end

-- Combat Start
function f:PLAYER_REGEN_DISABLED(event)
	xpCombat = 0;
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\Spell_Fire_FelFlameRing");
f:SetScript("OnClick",OnClick);
f:RegisterEvent("PLAYER_LOGIN");
f:RegisterEvent("PLAYER_XP_UPDATE");
f:RegisterEvent("PLAYER_LEVEL_UP");
f:RegisterEvent("PLAYER_REGEN_DISABLED");