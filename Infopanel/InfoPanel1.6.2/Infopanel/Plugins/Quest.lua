local f = InfoPanel.AddPlugin();

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- Returns colored text string
local function BoolCol(bool)
	return (bool and "|cff80ff80" or "|cffff8080");
end

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Quest Watch",1,1,1);
	tooltip:AddDoubleLine("Accepted Quests",format("%.2d / %.2d",select(2,GetNumQuestLogEntries()),MAX_QUESTLOG_QUESTS),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Daily Quests Completed",format("%.2d / %.2d",GetDailyQuestsCompleted(),GetMaxDailyQuests()),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Number of Watched Quests",GetNumQuestWatches(),nil,nil,nil,1,1,1);

	local objectives;
	local text, type, finished;
	local questTitle, level, questTag, suggestedGroup, isHeader, isCollapsed, isComplete, isDaily;
	local index = 1;
--	for i = 1, GetNumQuestLogEntries() do
	while (1) do
--		index = GetQuestIndexForWatch(i);
		questTitle, level, questTag, suggestedGroup, isHeader, isCollapsed, isComplete, isDaily = GetQuestLogTitle(index);
		if (not questTitle) then
			break;
		end
		objectives = GetNumQuestLeaderBoards(index);
		if (objectives > 0) then
			tooltip:AddLine(" ");
			tooltip:AddLine(questTitle,1,1,1);
			for o = 1, objectives do
				text, type, finished = GetQuestLogLeaderBoard(o,index)
				tooltip:AddDoubleLine(BoolCol(finished)..text);
			end
		end
		index = (index + 1);
	end
end

-- Gather Info
local function OnEvent(self,event,...)
	local nComplete = 0;
	local nEntries, nQuests = GetNumQuestLogEntries();
	local index = 1;
	while (GetQuestLogTitle(index)) do
		if (select(7,GetQuestLogTitle(index))) then
			nComplete = (nComplete + 1);
		end
		index = (index + 1);
	end
	self:SetText("%.2d / %.2d",nComplete,nQuests);
	if (nComplete > 0) then
		f.text:SetTextColor(0.5,1,0.5);
	else
		f.text:SetTextColor(1,1,1);
	end
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\INV_Misc_Book_09");
f:SetScript("OnEvent",OnEvent);
--f:RegisterEvent("PLAYER_LOGIN");

f:RegisterEvent("QUEST_LOG_UPDATE");
f:RegisterEvent("UPDATE_FACTION");
f:RegisterEvent("UNIT_QUEST_LOG_CHANGED");
f:RegisterEvent("QUEST_WATCH_UPDATE");