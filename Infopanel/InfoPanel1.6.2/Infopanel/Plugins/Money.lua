local f = InfoPanel.AddPlugin();
local sessionStart = GetTime();
local startMoney;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	local money = GetMoney();
	tooltip:AddLine("Money Info",1,1,1);
	tooltip:AddDoubleLine("Initial Money",InfoPanel.FormatCopper(startMoney));
	if (money ~= startMoney) then
		local text = (money > startMoney) and "Gained" or "Lost";
		local moneyPerHour = (money - startMoney) / ((GetTime() - sessionStart) / 60 / 60);
		tooltip:AddDoubleLine("Money "..text,InfoPanel.FormatCopper(abs(money - startMoney)));
		tooltip:AddDoubleLine("Money "..text.." / Hour",InfoPanel.FormatCopper(floor(abs(moneyPerHour))));
	end
end

-- OnClick
local function OnClick(self,button)
	if (ChatEdit_GetActiveWindow():IsShown()) then
		ChatEdit_GetActiveWindow():Insert(f.text:GetText():gsub("|c%x%x%x%x%x%x%x%x",""):gsub("|r",""));
	elseif (IsShiftKeyDown()) and (button == "RightButton") then
		sessionStart = GetTime();
		startMoney = GetMoney();
	end
end

-- Money Change
function f:PLAYER_MONEY(event)
	self:SetText(GetCoinTextureString(GetMoney(),16));
end

-- Login
function f:PLAYER_LOGIN(self,event)
	startMoney = GetMoney();
	f:UnregisterAllEvents();
	f:RegisterEvent("PLAYER_MONEY");
	f.PLAYER_LOGIN = nil;
	f:PLAYER_MONEY();
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\INV_Misc_Coin_01");
f:SetScript("OnClick",OnClick);
f:RegisterEvent("PLAYER_LOGIN");