local plugin = InfoPanel.AddPlugin();
local UPDATE_INTERVAL = 1;
local nextUpdate = 0;
local addons = {};
local fps, down, up, latency, mem;

local GetFramerate = GetFramerate;
local GetNetStats = GetNetStats;
local gcinfo = gcinfo;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

local function SortAddonsFunc(a,b)
	return a.mem > b.mem;
end

-- OnUpdate
local function OnUpdate(self,elapsed)
	nextUpdate = (nextUpdate - elapsed);
	if (nextUpdate <= 0) then
		fps = GetFramerate();
		down, up, latency = GetNetStats();
		mem = gcinfo();

		self:SetText("|cff00ff00%.1f  |cffffd100%dms|r  %.2f Mb",fps,latency,mem / 1024);
		nextUpdate = UPDATE_INTERVAL;
	end
end

-- Modifier Key Change -- Az: Bad hack, plugins should be able to request the core to update the tooltip
function plugin:MODIFIER_STATE_CHANGED(key,state)
	if (GameTooltip:IsOwned(self)) then
		self:GetScript("OnEnter")(self);
	end
end

-- OnShowTooltip
function plugin:OnShowTooltip(tooltip)
	-- Normal Info
	if (not IsShiftKeyDown()) then
		tooltip:AddLine("Performance Info",1,1,1);

		tooltip:AddDoubleLine("Frame Rate",format("%.2f",fps),nil,nil,nil,1,1,1);

		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("Bandwidth Down",format("%.2f bytes/s",down * 1024),nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Bandwidth Up",format("%.2f bytes/s",up * 1024),nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Latency",format("%d ms",latency),nil,nil,nil,1,1,1);

		local counter = 0;
		for _ in next, _G do
			counter = (counter + 1);
		end

		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("Global Namespace Entries",counter,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("UI Memory Usage",format("%d kb",mem),nil,nil,nil,1,1,1);
		tooltip:AddLine("<Press Shift for Detailed Addon Info>",0.5,0.5,0.5);
	-- Addon Details
	else
		UpdateAddOnMemoryUsage();
		local memTotal = 0;
		for i = 1, GetNumAddOns() do
			if (not addons[i]) then
				addons[i] = {};
			end
			addons[i].mem = GetAddOnMemoryUsage(i);
			addons[i].name = GetAddOnInfo(i);
			memTotal = (memTotal + addons[i].mem);
		end
		sort(addons,SortAddonsFunc);

		tooltip:AddLine("Addon Memory Usage ("..#addons..")",1,1,1);
		for index, addon in ipairs(addons) do
			if (addon.mem == 0) or (index > 50) then
				break;
			end
			tooltip:AddDoubleLine(format("|cffffffff%.2d|r %s",index,addon.name),format("%.2f kb",addon.mem),nil,nil,nil,1,1,1);
		end
		tooltip:AddLine(strrep("-",58),1,1,1);
		tooltip:AddDoubleLine("Addon Memory Usage",format("%d kb",memTotal),nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("UI Memory Usage",format("%d kb",mem),nil,nil,nil,1,1,1);
	end
end

-- OnClick - Perform a GC
local function OnClick()
	UpdateAddOnMemoryUsage();
	local memBefore = gcinfo();
	collectgarbage();
	UpdateAddOnMemoryUsage();
	local memAfter = gcinfo();
	AzMsg(("|2Garbage Collection|r Before: |1%d|r kb, After: |1%d|r kb, Reduction: |1%d|r kb."):format(memBefore,memAfter,memBefore-memAfter));
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

plugin.icon:SetTexture("Interface\\Icons\\Ability_Druid_LunarGuidance");
plugin:RegisterEvent("MODIFIER_STATE_CHANGED");
plugin:SetScript("OnUpdate",OnUpdate);
plugin:SetScript("OnClick",OnClick);