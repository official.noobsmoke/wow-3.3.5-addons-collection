local plugin = InfoPanel.AddPlugin();
local meta = { __index = function(t,k) t[k] = { count = 0, entries = {} }; return t[k]; end };
local addons = setmetatable({},meta);
local types = setmetatable({},meta);
local players = setmetatable({},meta);
local totalCount, startTime;

AddonSpam = { addons = addons, types = types, players = players };

-- Init Plugin
plugin.icon:SetTexture("Interface\\Icons\\Spell_Shadow_AbominationExplosion");
plugin:RegisterEvent("CHAT_MSG_ADDON");
plugin:RegisterEvent("MODIFIER_STATE_CHANGED");

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- Tooltip Show
function plugin:OnShowTooltip(tooltip)
	-- Addon Spam
	if (IsShiftKeyDown()) then
		tooltip:AddLine("Player Spam",1,1,1);
		for name, tbl in next, players do
			tooltip:AddDoubleLine(name,tbl.count,nil,nil,nil,1,1,1);
		end
	-- Type Spam
	elseif (IsControlKeyDown()) then
		tooltip:AddLine("Type Spam",1,1,1);
		for name, tbl in next, types do
			tooltip:AddDoubleLine(name,tbl.count,nil,nil,nil,1,1,1);
		end
	-- Player Spam
	else
		tooltip:AddLine("Addon Spam",1,1,1);
		for name, tbl in next, addons do
			tooltip:AddDoubleLine(name,tbl.count,nil,nil,nil,1,1,1);
		end
	end
	-- Total
	tooltip:AddLine(strrep("-",40),1,1,1);
	tooltip:AddDoubleLine("Spam per Sec",format("%.1f",totalCount / (GetTime() - startTime)),nil,nil,nil,1,1,1);
end

-- Addon Chat Message
function plugin:CHAT_MSG_ADDON(event,prefix,msg,type,sender)
	local tbl;
	-- Player Spam
	tbl = players[sender];
	tbl.count = (tbl.count + 1);
	tbl.entries[prefix] = (tbl.entries[prefix] or 0) + 1;
	-- Type Spam
	tbl = types[type];
	tbl.count = (tbl.count + 1);
	tbl.entries[prefix] = (tbl.entries[prefix] or 0) + 1;
	-- Addon Spam
	tbl = addons[prefix];
	tbl.count = (tbl.count + 1);
	tbl.entries[sender] = (tbl.entries[sender] or 0) + 1;
	tbl.msgs = tbl.msgs or {};
	tbl.msgs[msg] = (tbl.msgs[msg] or 0) + 1;
	-- Show Total Count
	totalCount = (totalCount + 1);
	plugin:SetText(totalCount);
end

-- Modifier Key Change -- Az: Bad hack, plugins should be able to request the core to update the tooltip
function plugin:MODIFIER_STATE_CHANGED(key,state)
	if (GameTooltip:IsOwned(self)) then
		self:GetScript("OnEnter")(self);
	end
end

-- Resets Data
local function ResetData()
	wipe(types);
	wipe(addons);
	wipe(players);
	totalCount = 0;
	startTime = GetTime();
	plugin:SetText("0");
end

-- OnClick
plugin:SetScript("OnClick",function(self,button)
	if (not IsShiftKeyDown()) then
		return;
	elseif (button == "RightButton") then
		ResetData();
	elseif (ChatEdit_GetActiveWindow():IsShown()) then
		ChatEdit_GetActiveWindow():Insert(format("Addon Chat Messages Recieved: %d; Per Second: %.1f",totalCount,totalCount / (GetTime() - startTime)));
	else
		SendChatMessage("--- Addon Spam ---","GUILD");
		for name, tbl in next, addons do
			SendChatMessage(name..": "..tbl.count,"GUILD");
		end
	end
end);

-- Init
ResetData();