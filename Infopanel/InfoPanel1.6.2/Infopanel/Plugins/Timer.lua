local f = InfoPanel.AddPlugin();
local UPDATE_INTERVAL = 1;
local nextUpdate = 0;
local startTime, pauseTime;
local FormatTime = InfoPanel.FormatTime;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Timer",1,1,1);
	tooltip:AddLine("<Left Click> to Start / Pause the timer");
	tooltip:AddLine("<Right Click> to reset the timer");
end

-- OnUpdate
local function OnUpdate(self,elapsed)
	nextUpdate = (nextUpdate - elapsed);
	if (nextUpdate <= 0) then
		if (pauseTime) then
			self:SetText(FormatTime(pauseTime - startTime));
		else
			self:SetText(FormatTime(GetTime() - startTime));
		end
		nextUpdate = UPDATE_INTERVAL;
	end
end

-- OnClick
local function OnClick(self,button)
	-- Right Click
	if (button == "RightButton") then
		f.text:SetTextColor(1,1,1);
		self:SetText("No Timer");
		f:SetScript("OnUpdate",nil);
		startTime = nil;
		pauseTime = nil;
	-- Left Click
	elseif (ChatEdit_GetActiveWindow():IsShown()) then
		ChatEdit_GetActiveWindow():Insert(f.text:GetText());
	else
		nextUpdate = 0;
		if (pauseTime) then
			f.text:SetTextColor(0,1,0);
			startTime = (GetTime() - (pauseTime - startTime));
			pauseTime = nil;
		elseif (startTime) then
			f.text:SetTextColor(1,0.5,0);
			pauseTime = GetTime();
		else
			f.text:SetTextColor(0,1,0);
			f:SetScript("OnUpdate",OnUpdate);
			startTime = GetTime();
		end
	end
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f:SetText("No Timer");
f.icon:SetTexture("Interface\\Icons\\Spell_Shadow_LastingAffliction");
f:SetScript("OnClick",OnClick);