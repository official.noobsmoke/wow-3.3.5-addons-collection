if (select(2,UnitClass("player")) ~= "HUNTER") then
	return;
end

local f = InfoPanel.AddPlugin();
local ammoSlot = GetInventorySlotInfo("AmmoSlot");
local ammoCount;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Ammo Count",1,1,1)
end

-- OnEvent
local function CountAmmo(self,event)
	ammoCount = GetInventoryItemCount("player",ammoSlot);
	if (ammoCount == 1) and (not GetInventoryItemTexture("player",ammoSlot)) then
    	ammoCount = 0;
	end;
	f:SetText("%.3d",ammoCount);
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\INV_Ammo_Arrow_02");
f:SetScript("OnEvent",CountAmmo);
f:RegisterEvent("BAG_UPDATE");
f:RegisterEvent("PLAYER_LOGIN");