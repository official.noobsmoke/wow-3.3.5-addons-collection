local f = InfoPanel.AddPlugin();
local CLASS_COLORS = CUSTOM_CLASS_COLORS or RAID_CLASS_COLORS;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Group Info",1,1,1);

	local numParty, numRaid = GetNumPartyMembers(), GetNumRaidMembers();
	local members = (numRaid > 0 and numRaid or numParty);
	-- Show Instance Lockouts
	if (IsShiftKeyDown()) then
		RequestRaidInfo();
		local instanceName, instanceID, instanceReset, instanceDifficulty;
		for i = 1, GetNumSavedInstances() do
			instanceName, instanceID, instanceReset, instanceDifficulty = GetSavedInstanceInfo(i)
			tooltip:AddDoubleLine("|cffffffff"..instanceID.."|r "..instanceName.." |cffc0c0c0(".._G["DUNGEON_DIFFICULTY"..instanceDifficulty]..")",InfoPanel.FormatTime3(instanceReset),nil,nil,nil,1,1,1);
		end
	-- Player is Solo :(
	elseif (members == 0) then
		tooltip:AddLine("Not in a Raid or Party");
	-- Show Group Info
	else
		-- locals
		local unit, unitHealth, unitMana;
		local raidHealth, raidMana, unitsWithMana, validMemebers = 0, 0, 0, 0;
		local deadCount, afkCount, offlineCount, outsideRange = 0, 0, 0, 0;
		local classes = {};
		local class, color;
		-- Function to Scan Units
		local function ScanUnit(unit)
			class = UnitClass(unit);
			-- Init class table
			if (class) and (not classes[class]) then
				color = CLASS_COLORS[select(2,UnitClass(unit))];
				classes[class] = { count = 0, validCount = 0, health = 0, mana = 0, color = ("|cff%.2x%.2x%.2x"):format(color.r*255,color.g*255,color.b*255) };
			end
			classes[class].count = (classes[class].count + 1);
			-- count misc
			if (UnitIsDead(unit) or UnitIsGhost(unit)) then
				deadCount = (deadCount + 1);
			end
			if (UnitIsAFK(unit)) then
				afkCount = (afkCount + 1);
			end
			if (not UnitIsConnected(unit)) then
				offlineCount = (offlineCount + 1);
			end
			if (not UnitIsVisible(unit)) then
				outsideRange = (outsideRange + 1);
			end
			-- Gather Health / Mana Info
			if (not UnitIsDead(unit)) and (not UnitIsGhost(unit)) and (UnitIsConnected(unit)) then
				validMemebers = (validMemebers + 1);
				classes[class].validCount = (classes[class].validCount + 1);

				unitHealth = (UnitHealth(unit) / UnitHealthMax(unit) * 100);
				raidHealth = (raidHealth + unitHealth);
				classes[class].health = (classes[class].health + unitHealth);

				if (UnitPowerType(unit) == 0) then
					unitMana = (UnitPower(unit) / UnitPowerMax(unit) * 100);
					raidMana = (raidMana + unitMana);
					classes[class].mana = (classes[class].mana + unitMana);
					unitsWithMana = (unitsWithMana + 1);
				end
			end
		end
		-- Loop Members
		local unitPrefix = (numRaid > 0 and "raid" or "party");
		for i = 1, members do
			ScanUnit(unitPrefix..i);
		end
		if (unitPrefix == "party") then
			ScanUnit("player");
			members = (members + 1);
		end
		-- Find Group Number and Leader
		local leader;
		if (numRaid > 0) then
			local name, rank, subGroup;
			for i = 1, numRaid do
				name, rank, subGroup = GetRaidRosterInfo(i);
				if (rank == 2) then
					leader = i;
				end
				if (name == UnitName("player")) then
					tooltip:AddDoubleLine("Your Group",subGroup,nil,nil,nil,1,1,1);
				end
			end
		elseif (numParty > 0) then
			leader = GetPartyLeaderIndex();
		end
		-- Add Tip Lines
		local leaderUnit = (leader == 0 and "player" or (unitPrefix..leader));
		color = CLASS_COLORS[select(2,UnitClass(leaderUnit))];
		tooltip:AddDoubleLine("Leader",UnitName(leaderUnit),nil,nil,nil,color.r,color.g,color.b);
		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("Members",members,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Out of Range",outsideRange == 0 and "-" or outsideRange,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Dead or Ghost",deadCount == 0 and "-" or deadCount,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("AFKs",afkCount == 0 and "-" or afkCount,nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Offline",offlineCount == 0 and "-" or offlineCount,nil,nil,nil,1,1,1);
		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("Raid Health",(validMemebers > 0 and format("%.2f%%",raidHealth / validMemebers) or ""),nil,nil,nil,0,1,0);
		if (unitsWithMana > 0) then
			tooltip:AddDoubleLine("Raid Mana",format("%.2f%%",raidMana / unitsWithMana),nil,nil,nil,0.4,0.8,0.93);
		end
		tooltip:AddLine(" ");
		for class, tbl in next, classes do
			if (tbl.mana == 0) then
				tooltip:AddDoubleLine(tbl.count..tbl.color.."  "..class,(tbl.validCount > 0 and format("%.2f%%",(tbl.health / tbl.validCount)) or ""),nil,nil,nil,0,1,0);
			else
				tooltip:AddDoubleLine(tbl.count..tbl.color.."  "..class,format("|cff68ccef%.2f%%|cffffffff |||r %.2f%%",(tbl.mana / tbl.validCount),(tbl.health / tbl.validCount)),nil,nil,nil,0,1,0);
			end
		end
	end
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f:SetText("Group");
f.icon:SetTexture("Interface\\Icons\\Spell_unused2");
f:SetScript("OnClick",function(self) ToggleFriendsFrame(5); end);