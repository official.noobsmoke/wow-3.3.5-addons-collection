local f = InfoPanel.AddPlugin();
local nextUpdate = 0;
local GetUnitSpeed = GetUnitSpeed;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

local function OnUpdate(self,elapsed)
	nextUpdate = (nextUpdate - elapsed);
	if (nextUpdate <= 0) then
		self:SetText("%.1f%%",GetUnitSpeed("player") / 7 * 100);
		nextUpdate = 0.5;
	end
end

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Movement Speed",1,1,1);
	tooltip:AddDoubleLine("Base Speed (7)",format("%.3f",GetUnitSpeed("player")),nil,nil,nil,1,1,1);
	if (UnitExists("target")) then
		tooltip:AddDoubleLine("Target Speed",format("%.2f%%",GetUnitSpeed("target") / 7 * 100),nil,nil,nil,1,1,1);
	end
	if (UnitExists("pet")) then
		tooltip:AddDoubleLine("Pet Speed",format("%.2f%%",GetUnitSpeed("pet") / 7 * 100),nil,nil,nil,1,1,1);
	end
	if (UnitExists("vehicle")) then
		tooltip:AddDoubleLine("Vehicle Speed",format("%.2f%%",GetUnitSpeed("vehicle") / 7 * 100),nil,nil,nil,1,1,1);
	end
end

local function OnClick(self,button)
	if (ChatEdit_GetActiveWindow():IsShown()) then
		ChatEdit_GetActiveWindow():Insert(f.text:GetText());
	end
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\Ability_Rogue_Sprint");
f:SetScript("OnUpdate",OnUpdate);
f:SetScript("OnClick",OnClick);