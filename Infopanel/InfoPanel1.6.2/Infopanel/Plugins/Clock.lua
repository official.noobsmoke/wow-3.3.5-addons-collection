local f = InfoPanel.AddPlugin("right");
local UPDATE_INTERVAL = 1;
local nextUpdate = 0;
local sessionStart = GetTime();
local gameTimeOffset = (time() % 60);
local gameTimeMin = select(2,GetGameTime());
local hrs, min, sec;

-- Az: Temp config until I setup the settings properly
local cfg = { showRealTime = true, show24HourClock = true };

local timeFmt = (cfg.show24HourClock and "%H:%M:%S" or "%I:%M:%S %p");

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip -- Time format help: http://www.opengroup.org/onlinepubs/007908799/xsh/strftime.html
function f:OnShowTooltip(tooltip)
	hrs, min = GetGameTime();
	tooltip:AddLine("Clock",1,1,1);
	tooltip:AddDoubleLine("Game Time",format("%.2d:%.2d:%.2d",hrs,min,(time() - gameTimeOffset) % 60),nil,nil,nil,1,1,1);
	tooltip:AddLine(" ");
	tooltip:AddDoubleLine("Local Time",date(timeFmt),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Date",date("%A, %B %d, %Y"),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Day of Year",tonumber(date("%j")),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Week of Year",(tonumber(date("%W")) + 1),nil,nil,nil,1,1,1);
	tooltip:AddLine(" ");
	tooltip:AddDoubleLine("Session Time",InfoPanel.FormatTime2(GetTime() - sessionStart),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("System Uptime",InfoPanel.FormatTime2(GetTime()),nil,nil,nil,1,1,1);
end

-- OnUpdate
local function OnUpdate(self,elapsed)
	nextUpdate = (nextUpdate - elapsed);
	if (nextUpdate <= 0) then
		-- Sync GameTime Seconds
		if (gameTimeMin) and (select(2,GetGameTime()) ~= gameTimeMin) then
			gameTimeOffset = (time() % 60);
			gameTimeMin = nil;
		end
		-- Show Time
		if (cfg.showRealTime) then
			self:SetText(date(timeFmt));
		else
			hrs, min = GetGameTime();
			sec = (time() - gameTimeOffset) % 60;
			self:SetText("%.2d:%.2d:%.2d",hrs,min,sec);
		end
		nextUpdate = UPDATE_INTERVAL;
	end
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\Ability_Hunter_Readiness");
f:SetScript("OnUpdate",OnUpdate);
f:SetScript("OnClick",function() GameTimeFrame:Click() end);