local f = InfoPanel.AddPlugin();
local UPDATE_INTERVAL = 1;
local nextUpdate = 0;
local startTime, endTime, combatTime;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnUpdate
local function OnUpdate(self,elapsed)
	nextUpdate = (nextUpdate - elapsed);
	if (nextUpdate <= 0) then
		combatTime = (GetTime() - startTime);
		self:SetText("%.2d:%.2d",floor(combatTime / 60),floor(combatTime % 60));
		nextUpdate = UPDATE_INTERVAL;
	end
end

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Combat Timer",1,1,1);
	if (combatTime) then
		tooltip:AddDoubleLine("In Seconds",format("%.3f",combatTime),nil,nil,nil,1,1,1);
	end
	if (startTime) then
		tooltip:AddDoubleLine("Start Time",date("%H:%M:%S",time() - GetTime() + startTime),nil,nil,nil,1,1,1);
	end
	if (endTime) then
		tooltip:AddDoubleLine("End Time",date("%H:%M:%S",time() - GetTime() + endTime),nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Time Since",InfoPanel.FormatTime2(GetTime() - endTime),nil,nil,nil,1,1,1);
	end
	if (InCombatLockdown()) and (UnitExists("target")) then
		local val, max = UnitHealth("target"), UnitHealthMax("target");
		local pp = (combatTime / val * max);
		tooltip:AddDoubleLine("Estimated",InfoPanel.FormatTime2(pp),nil,nil,nil,1,1,1);
	end
end

-- OnClick
local function OnClick(self,button)
	if (ChatEdit_GetActiveWindow():IsShown()) and (combatTime) then
		ChatEdit_GetActiveWindow():Insert(format("%d min %.2d sec",floor(combatTime / 60),floor(combatTime % 60)));
	end
end

-- Combat Start
function f:PLAYER_REGEN_DISABLED()
	f.text:SetTextColor(0,1,0);
	f:SetScript("OnUpdate",OnUpdate);
	startTime = GetTime();
	nextUpdate = 0;
end

-- Combat End
function f:PLAYER_REGEN_ENABLED()
	f.text:SetTextColor(0.5,0.75,1);
	f:SetScript("OnUpdate",nil);
	endTime = GetTime();
	OnUpdate(f,0);
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\Spell_Shadow_LastingAfflictions");
f:SetScript("OnClick",OnClick);
f:RegisterEvent("PLAYER_REGEN_DISABLED");
f:RegisterEvent("PLAYER_REGEN_ENABLED");
f:SetText("00:00");