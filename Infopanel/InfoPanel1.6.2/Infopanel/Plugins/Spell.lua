local class = select(2,UnitClass("player"));
if (class == "WARRIOR" or class == "HUNTER" or class == "ROGUE" or class == "DEATHKNIGHT") then
	return;
end

local plugin = InfoPanel.AddPlugin();
local bloodlust = GetSpellInfo(UnitFactionGroup("player") == FACTION_ALLIANCE and 32182 or 2825);
local woaTotem = GetSpellInfo(2895);
local HEAL_MOD = 1.88;

local GetCombatRatingBonus = GetCombatRatingBonus;
local GetSpellBonusHealing = GetSpellBonusHealing;
local GetSpellBonusDamage = GetSpellBonusDamage;
local GetSpellCritChance = GetSpellCritChance;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

local function GetHaste()
	local haste = GetCombatRatingBonus(CR_HASTE_SPELL);
	if (UnitAura("player",bloodlust)) then
		haste = (haste + 30 * (1 + haste / 100));
	end
	if (UnitAura("player",woaTotem)) then
		haste = (haste + 5 * (1 + haste / 100));
	end
	return haste;
end

-- OnShowTooltip
function plugin:OnShowTooltip(tooltip)
	tooltip:AddLine("Spell Info",1,1,1);
	local base, casting = GetManaRegen();
	local healing = GetSpellBonusHealing();
	tooltip:AddDoubleLine("Healing",healing,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Actual Healing",floor(healing * HEAL_MOD),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Hit Rating",format("%.2f%%",GetCombatRatingBonus(CR_HIT_SPELL)),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Mana Regen Base",format("%.1f",floor(base * 5)),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Mana Regen Casting",format("%.1f",floor(casting * 5)),nil,nil,nil,1,1,1);

	tooltip:AddLine(" ");
	tooltip:AddLine("Magic Schools",1,1,1);
	for i = 2, MAX_SPELL_SCHOOLS do
		local line = format("%d (%.2f%%)",GetSpellBonusDamage(i),GetSpellCritChance(i));
		tooltip:AddDoubleLine(_G["DAMAGE_SCHOOL"..i],line,NORMAL_FONT_COLOR.r,NORMAL_FONT_COLOR.g,NORMAL_FONT_COLOR.b,1,1,1);
		tooltip:AddTexture("Interface\\PaperDollInfoFrame\\SpellSchoolIcon"..i);
	end
end

local function OnClick(self,button)
	if (ChatEdit_GetActiveWindow():IsShown()) then
		ChatEdit_GetActiveWindow():Insert(format("%d Spell Power, %.2f%% Haste, %.2f%% Crit, %.2f%% Hit",GetSpellBonusDamage(2),GetHaste(),GetSpellCritChance(2),GetCombatRatingBonus(CR_HIT_SPELL)));
	end
end

local function OnEvent(self,event,p1,...)
	if (event == "UNIT_AURA") and (p1 ~= "player") then
		return;
	end
	self:SetText("|cffffffff%.2f%%  |cff80c0ff%.2f%%  |cff52ff52%d",GetHaste(),GetSpellCritChance(2),max(GetSpellBonusDamage(2),GetSpellBonusHealing()));
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

plugin.icon:SetTexture("Interface\\Icons\\Spell_Holy_HolyBolt");
plugin:SetScript("OnEvent",OnEvent);
plugin:SetScript("OnClick",OnClick);
plugin:RegisterEvent("PLAYER_LOGIN");
plugin:RegisterEvent("COMBAT_RATING_UPDATE");
plugin:RegisterEvent("PLAYER_DAMAGE_DONE_MODS");
plugin:RegisterEvent("UNIT_AURA");