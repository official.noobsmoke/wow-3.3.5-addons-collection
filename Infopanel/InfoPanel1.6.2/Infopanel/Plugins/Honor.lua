local f = InfoPanel.AddPlugin();
local sessionHonor, honorThisZone, honorPrevZone;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Honor Details",1,1,1);
	local kills, points = GetPVPSessionStats();
	-- Session/Zone Honor
	tooltip:AddDoubleLine("This Session's Honor",GetHonorCurrency() - sessionHonor,nil,nil,nil,1,1,1);
	if (honorThisZone) then
		tooltip:AddDoubleLine("This Zone's Honor",points - honorThisZone,nil,nil,nil,1,1,1);
		if (honorPrevZone) then
			tooltip:AddDoubleLine("Last Zone's Honor",honorThisZone - honorPrevZone,nil,nil,nil,1,1,1);
		end
		tooltip:AddLine(" ");
	end
	-- Today's values
	tooltip:AddDoubleLine("Today's Kills",kills,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Today's Points",points,nil,nil,nil,1,1,1);
	tooltip:AddLine(" ");
	-- Yesterday's values
	kills, points = GetPVPYesterdayStats();
	tooltip:AddDoubleLine("Yesterday's Kills",kills,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Yesterday's Points",points,nil,nil,nil,1,1,1);
	tooltip:AddLine(" ");
	-- Lifetime stats
	local highestRank;
	kills, highestRank = GetPVPLifetimeStats();
	tooltip:AddDoubleLine("Lifetime Kills",kills,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Highest Rank",highestRank,nil,nil,nil,1,1,1);
	tooltip:AddLine(" ");
	-- Rank
	local rankName, rankNumber = GetPVPRankInfo(highestRank);
	if ( not rankName ) then
		rankName = NONE;
	end
	tooltip:AddDoubleLine("Rank Name",rankName,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Rank Number",rankNumber,nil,nil,nil,1,1,1);
	tooltip:AddLine(" ");
	-- Points
	tooltip:AddDoubleLine("Honor Points",GetHonorCurrency(),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Arena Points",GetArenaCurrency(),nil,nil,nil,1,1,1);
	-- Az: Wintergrasp wait time
	if (GetWintergraspWaitTime) then
		local timeLeft = GetWintergraspWaitTime();
		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("Wintergrasp Time",type(timeLeft) == "number" and InfoPanel.FormatTime2(timeLeft) or UNKNOWN,nil,nil,nil,1,1,1);
	end
	-- PvP Time
	local pvpTime = GetPVPTimer();
	if (pvpTime) and (pvpTime < 301000) then
		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("PvP Flag Time",InfoPanel.FormatTime(pvpTime / 1000),nil,nil,nil,1,1,1);
	end;
end

-- Zone Change
function f:ZONE_CHANGED_NEW_AREA()
	honorPrevZone = honorThisZone;
	honorThisZone = select(2,GetPVPSessionStats());
end
f.PLAYER_ENTERING_WORLD = f.ZONE_CHANGED_NEW_AREA;

-- Honor Update
function f:HONOR_CURRENCY_UPDATE()
	self:SetText(GetHonorCurrency());
end

-- Login
function f:PLAYER_LOGIN()
	sessionHonor = GetHonorCurrency();
	self:SetText(sessionHonor);
	f.PLAYER_LOGIN = nil;
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture(UnitFactionGroup("player") == "Alliance" and "Interface\\Icons\\INV_BannerPVP_02" or "Interface\\Icons\\INV_BannerPVP_01");
f:SetScript("OnClick",function(self) ToggleFrame(PVPFrame); end);
f:RegisterEvent("PLAYER_LOGIN");
f:RegisterEvent("HONOR_CURRENCY_UPDATE");
--f:RegisterEvent("PLAYER_ENTERING_WORLD");
f:RegisterEvent("ZONE_CHANGED_NEW_AREA");