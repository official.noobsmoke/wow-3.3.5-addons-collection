local f = InfoPanel.AddPlugin();
local minDur;
local itemSlots = { "HeadSlot", "ShoulderSlot", "ChestSlot", "WristSlot", "HandsSlot", "WaistSlot", "LegsSlot", "FeetSlot", "MainHandSlot", "SecondaryHandSlot", "RangedSlot" };
for index, slotName in ipairs(itemSlots) do
	itemSlots[slotName] = { slot = GetInventorySlotInfo(slotName) };
	itemSlots[index] = nil;
end

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnEvent
function f:UPDATE_INVENTORY_DURABILITY(event)
	minDur = 100;
	-- Equip
	for slotName, tbl in next, itemSlots do
		tbl.min, tbl.max = GetInventoryItemDurability(tbl.slot);
		if (tbl.min and tbl.max) then
			minDur = min(minDur,tbl.min / tbl.max * 100);
		end
	end
	-- Set Label
	self:SetText("%d%%",minDur);
	self:SetTextColor(minDur,20,60);
end

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Durability",1,1,1);
	local equipCost, bagCost = 0, 0;
	-- Equip
	for slotName, tbl in next, itemSlots do
		tbl.cost = select(3,IPWorkTip:SetInventoryItem("player",tbl.slot)) or 0;
		if (tbl.cost > 0) then
			equipCost = (equipCost + tbl.cost);
			minDur = min(minDur,tbl.min / tbl.max * 100);
		end
	end
	-- Bags
	bagCost = 0;
	for bag = 0, NUM_BAG_FRAMES do
		for slot = 1, GetContainerNumSlots(bag) do
			local _, repairCost = IPWorkTip:SetBagItem(bag,slot);
			if (repairCost and repairCost > 0) then
				bagCost = (bagCost + repairCost);
			end
		end
	end
	-- Add Slot Lines
	for slotName, tbl in next, itemSlots do
		if (tbl.cost > 0) then
			--tooltip:AddDoubleLine(format("|cff00ff00%d / %d|r %s",tbl.min,tbl.max,_G[slotName:upper()]),InfoPanel.FormatCopper(tbl.cost));
			tooltip:AddDoubleLine(format("|cff00ff00%d%%|r %s",tbl.min / tbl.max * 100,_G[slotName:upper()]),InfoPanel.FormatCopper(tbl.cost));
		end
	end
	if (tooltip:NumLines() > 1) then
		tooltip:AddLine(" ");
	end
	-- Total Cost
	local costTotal = (equipCost + bagCost);
	if (costTotal > 0) then
		if (bagCost > 0) then
			tooltip:AddDoubleLine("Bagged Gear",InfoPanel.FormatCopper(bagCost));
			tooltip:AddLine(" ");
		end
		tooltip:AddLine("Repair Cost",1,1,1);
		tooltip:AddDoubleLine("Neutral",InfoPanel.FormatCopper(costTotal));
		tooltip:AddDoubleLine("Friendly",InfoPanel.FormatCopper(costTotal * 0.95));
		tooltip:AddDoubleLine("Honored",InfoPanel.FormatCopper(costTotal * 0.90));
		tooltip:AddDoubleLine("Revered",InfoPanel.FormatCopper(costTotal * 0.85));
		tooltip:AddDoubleLine("Exalted",InfoPanel.FormatCopper(costTotal * 0.80));
	else
		tooltip:AddLine("Nothing damaged");
	end
end

-- OnClick
local function OnClick(self,button)
	if (ChatEdit_GetActiveWindow():IsShown()) then
		local costTotal = (equipCost + bagCost);
		local fmtNeutral = InfoPanel.FormatCopper(costTotal):gsub("|c%x%x%x%x%x%x%x%x",""):gsub("|r","");
		ChatEdit_GetActiveWindow():Insert("Neutral Repair Cost: "..fmtNeutral);
	else
		self:UPDATE_INVENTORY_DURABILITY();
	end
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\Trade_BlackSmithing");
f:SetScript("OnClick",OnClick);
f:RegisterEvent("UPDATE_INVENTORY_DURABILITY");