local f = InfoPanel.AddPlugin();
local UPDATE_INTERVAL = 0.5;
local nextUpdate = 0;
local zoneChange, lastZoneChange;
local GetPlayerMapPosition = GetPlayerMapPosition;

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnUpdate
local function OnUpdate(self,elapsed)
	nextUpdate = (nextUpdate - elapsed);
	if (nextUpdate <= 0) then
		local x, y = GetPlayerMapPosition("player");
		if (x == 0 and y == 0) then
			self:SetText("n/a");
		else
			self:SetText("%.1f |cffffff00|||r %.1f",x * 100,y * 100);
		end
		nextUpdate = UPDATE_INTERVAL;
	end
end

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Zone Info",1,1,1);

	tooltip:AddDoubleLine("Zone",GetZoneText(),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Real Zone",GetRealZoneText(),nil,nil,nil,1,1,1);
--	tooltip:AddDoubleLine("Sub Zone",GetSubZoneText(),nil,nil,nil,1,1,1); -- Az: Sometimes returns nothing
	tooltip:AddDoubleLine("Minimap Zone",GetMinimapZoneText(),nil,nil,nil,1,1,1);

	local pvpType, isFFA, faction = GetZonePVPInfo();
	if (pvpType) then
		tooltip:AddLine(" ");
		tooltip:AddDoubleLine("Zone PvP Type",(pvpType or ""):gsub(".",strupper,1),nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Zone Free for All",tostring(isFFA),nil,nil,nil,1,1,1);
		tooltip:AddDoubleLine("Zone Faction",tostring(faction),nil,nil,nil,1,1,1);
	end

	local inInstance, instanceType = IsInInstance();
	tooltip:AddLine(" ");
	tooltip:AddDoubleLine("In Instance",tostring(inInstance and "true" or "false"),nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Instance Type",tostring(instanceType),nil,nil,nil,1,1,1);

	tooltip:AddLine(" ");
	tooltip:AddDoubleLine("This Zone's Timer",InfoPanel.FormatTime2(GetTime() - zoneChange),nil,nil,nil,1,1,1);
	if (lastZoneChange) then
		tooltip:AddDoubleLine("Last Zone's Timer",InfoPanel.FormatTime2(zoneChange - lastZoneChange),nil,nil,nil,1,1,1);
	end
end

-- OnEvent
local function OnEvent(self,event)
	if (not zoneChange) or (GetTime() - zoneChange > 2) then
		lastZoneChange = zoneChange;
		zoneChange = GetTime();
	end
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\Achievement_Zone_Northrend_01");
f:SetScript("OnUpdate",OnUpdate);
f:SetScript("OnClick",function() ToggleFrame(WorldMapFrame); end);
f:SetScript("OnEvent",OnEvent);
f:RegisterEvent("PLAYER_ENTERING_WORLD");
f:RegisterEvent("ZONE_CHANGED_NEW_AREA");