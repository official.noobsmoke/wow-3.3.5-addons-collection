local f = InfoPanel.AddPlugin();
local info = {};
local free, type;

for i = 0, NUM_BAG_FRAMES do
	info[i] = {};
end

--------------------------------------------------------------------------------------------------------
--                                             Functions                                              --
--------------------------------------------------------------------------------------------------------

-- OnShowTooltip
function f:OnShowTooltip(tooltip)
	tooltip:AddLine("Bag Info",1,1,1);
	for bag = 0, NUM_BAG_FRAMES do
		if (GetBagName(bag)) then
			tooltip:AddDoubleLine(GetBagName(bag),format("%.2d / %.2d",info[bag].slots - info[bag].used,info[bag].slots),nil,nil,nil,1,1,1);
		end
	end
	tooltip:AddLine(" ");
	tooltip:AddDoubleLine("Used Slots",info.usedTotal.." / "..info.maxTotal,nil,nil,nil,1,1,1);
	tooltip:AddDoubleLine("Free Slots",(info.maxTotal - info.usedTotal).." / "..info.maxTotal,nil,nil,nil,1,1,1);
end

-- OnEvent
local function OnEvent(self,event)
	info.maxTotal = 0;
	info.usedTotal = 0;
	-- Loop
	for bag = 0, NUM_BAG_FRAMES do
		info[bag].slots = GetContainerNumSlots(bag);
		free, type = GetContainerNumFreeSlots(bag);
		info[bag].used = (info[bag].slots - free);
		-- Don't include quivers
		if (type ~= 1) then
			info.usedTotal = (info.usedTotal + info[bag].used);
			info.maxTotal = (info.maxTotal + info[bag].slots);
		end
	end
	self:SetText("%d / %d",info.maxTotal - info.usedTotal,info.maxTotal);
end

--------------------------------------------------------------------------------------------------------
--                                              Finalize                                              --
--------------------------------------------------------------------------------------------------------

f.icon:SetTexture("Interface\\Icons\\INV_Misc_Bag_EnchantedMageweave");
f:SetScript("OnEvent",OnEvent);
f:SetScript("OnClick",function() OpenAllBags(); end);
f:RegisterEvent("BAG_UPDATE");
f:RegisterEvent("PLAYER_LOGIN");