-- InfoPanel Work Tip (for plugins to use)
CreateFrame("GameTooltip","IPWorkTip",nil,"GameTooltipTemplate");
IPWorkTip:SetOwner(UIParent,"ANCHOR_NONE");

--------------------------------------------------------------------------------------------------------
--                                          Shared Functions                                          --
--------------------------------------------------------------------------------------------------------

-- Format Copper String
function InfoPanel.FormatCopper(copper)
	copper = floor(copper);
	local fmt = "|cFFD7844D"..(copper % 100).."c|r";
	if (copper >= 100) then
		fmt = "|cFFAEAEAE"..floor(copper % 10000 / 100).."s|r "..fmt;
	end
	if (copper >= 10000) then
		fmt = "|cFFF0E52B"..floor(copper / 10000).."g|r "..fmt;
	end
	return fmt;
end

-- Format Time (Simple) -- 12345 == "03:25:45"
function InfoPanel.FormatTime(time)
	local sec = (time % 60);
	local min = (floor(time / 60) % 60);
	local hrs = (floor(time / 60 / 60) % 24);
	local day = floor(time / 60 / 60 / 24);
	return (day == 0 and "" or day.." days, ")..format("%.2d:%.2d:%.2d",hrs,min,sec);
end

-- Format Time (Ext) -- 12345 == "03 hrs 25 min 45 sec"
function InfoPanel.FormatTime2(time)
	local fmt = format("%.2d sec",floor(time % 60));
	if (time >= 60) then
		time = floor(time / 60);
		fmt = format("%.2d min ",time % 60)..fmt;
		if (time >= 60) then
			time = floor(time / 60);
			fmt = format("%.2d hrs ",time % 24)..fmt;
			if (time >= 24) then
				time = floor(time / 24);
				fmt = format("%d days ",time)..fmt;
			end
		end
	end
	return fmt;
end

-- Format Time (Ext Short) -- 12345 == "03h 25m"
function InfoPanel.FormatTime3(time)
	local color = "|cffffff80";
	-- bugged?
	if (time < 0) then
		return "n/a";
	-- under a min
	elseif (time < 60) then
		return format(color.."%.2f |rs",time);
	-- less than 1 hour
	elseif (time < 60*60) then
		return format(color.."%d|rm"..color.."%.2d|rs",time / 60,time % 60);
	-- less than 1 day
	elseif (time < 60*60*24) then
		time = (time/60);
		return format(color.."%d|rh"..color.."%.2d|rm",time / 60,time % 60);
	-- above 1 day
	else
		time = (time/60/60);
		return format(color.."%d|rd"..color.."%.2d|rh",time / 24,time % 24);
	end
end