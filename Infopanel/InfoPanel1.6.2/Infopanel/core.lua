-- Global Chat Message Function
function AzMsg(msg) DEFAULT_CHAT_FRAME:AddMessage(tostring(msg):gsub("|1","|cffffff80"):gsub("|2","|cffffffff"),0.5,0.75,1.0); end

-- Config
local cfg;
local DefaultConfig = {
	topAnchor = false,
	defaultTip = false,
};

local lastPlugin;
local iconSize = 18;

-- Init Specific Mod Vars
InfoPanel = {};
local f = CreateFrame("Frame",nil,UIParent);
local modName = "InfoPanel";

--------------------------------------------------------------------------------------------------------
--                                           Misc Functions                                           --
--------------------------------------------------------------------------------------------------------

-- AdjustAnchors
local function AdjustAnchors()
	-- InfoPanel
	f:ClearAllPoints();
	if (cfg.topAnchor) then
		f:SetPoint("TOPLEFT",-1,1);
		f:SetPoint("TOPRIGHT",1,1);
	else
		f:SetPoint("BOTTOMLEFT",-1,-1);
		f:SetPoint("BOTTOMRIGHT",1,-1);
	end
	-- MainMenuBar
	if (MainMenuBar:IsShown()) then
		MainMenuBar:ClearAllPoints();
		if (cfg.topAnchor) then
			MainMenuBar:SetPoint("BOTTOM");
		else
			MainMenuBar:SetPoint("BOTTOM",f,"TOP");
		end
	end
end

-- OnEvent
local function OnEvent(self,event)
	if (not InfoPanel_Config) then
		InfoPanel_Config = {};
	end
	cfg = setmetatable(InfoPanel_Config,{ __index = DefaultConfig });
	AdjustAnchors();
	OnEvent = nil;
end

-- Setup the Main Frame
f:SetHeight(26);
f:SetToplevel(1);
f:SetBackdrop({ bgFile = "Interface\\Tooltips\\UI-Tooltip-Background", edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 8, insets = { left = 2, right = 2, top = 2, bottom = 2 } });
f:SetBackdropColor(0.1,0.22,0.35,1);
f:SetBackdropBorderColor(0.1,0.1,0.1,1);
f:Show();
f:SetScript("OnEvent",OnEvent);
f:RegisterEvent("VARIABLES_LOADED");

--------------------------------------------------------------------------------------------------------
--                                           Slash Handling                                           --
--------------------------------------------------------------------------------------------------------
_G["SLASH_"..modName.."1"] = "/ip";
SlashCmdList[modName] = function(cmd)
	-- Extract Paramters
	local param1, param2 = cmd:match("^([^%s]+)%s*(.*)$");
	param1 = (param1 and param1:lower() or cmd:lower());
	-- Show / Hide
	if (param1 == "show") then
		if (f:IsVisible()) then
			f:Hide();
		else
			f:Show();
		end
	-- Swap Anchor
	elseif (param1 == "anchor") then
		cfg.topAnchor = not cfg.topAnchor;
		AdjustAnchors();
	-- Tip
	elseif (param1 == "tip") then
		cfg.defaultTip = not cfg.defaultTip;
	-- Invalid or No Command
	else
		UpdateAddOnMemoryUsage();
		AzMsg(format("----- |2%s|r |1%s|r ----- |1%.2f |2kb|r -----",modName,GetAddOnMetadata(modName,"Version"),GetAddOnMemoryUsage(modName)));
		AzMsg("The following |2parameters|r are valid for this addon:");
		AzMsg(" |2anchor|r = Swap anchor position between top and bottom");
		AzMsg(" |2tip|r = Toggles between plugin anchored tip or default anchored tip");
	end
end

--------------------------------------------------------------------------------------------------------
--                                       Plugin Helper Functions                                      --
--------------------------------------------------------------------------------------------------------

-- Standard OnEvent Script
local function Default_OnEvent(self,event,...)
	if (self[event]) then
		self[event](self,event,...);
	end
end

-- Standard OnEnter Script
local function Default_OnEnter(self)
	if (self.OnShowTooltip) then
		local placement = (cfg.topAnchor and "ANCHOR_BOTTOM" or "ANCHOR_TOP"); -- Az: this is still not acceptable, but will have to do for now
		if (cfg.defaultTip) then
			GameTooltip_SetDefaultAnchor(GameTooltip,self);
		elseif (self.isRightAligned) then
			GameTooltip:SetOwner(self,placement.."RIGHT",4,0);
		else
			GameTooltip:SetOwner(self,placement.."LEFT");
		end
		self:OnShowTooltip(GameTooltip);
		GameTooltip:Show();
	end
end

-- Standard OnLeave Script
local function HideGTT()
	GameTooltip:Hide();
end

-- Quick SetText for Plugins to use
local function SetText(self,text,...)
	-- text
	if (select("#",...) > 0) then
		self.text:SetFormattedText(text,...);
	else
		self.text:SetText(text);
	end
	-- width
	local oldWidth = self:GetWidth();
	local newWidth = (self.text:GetWidth() + iconSize + 8);
	if (abs(oldWidth - newWidth) > 4) then
		self:SetWidth(newWidth);
	end
	-- tip
	if (GameTooltip:IsOwned(self)) and (self:GetScript("OnEnter")) then
		self:GetScript("OnEnter")(self);
	end
end

-- SetTextColor
local function SetTextColor(self,value,lowBoundry,highBoundry)
	if (value > highBoundry) then
		self.text:SetTextColor(0,1,0);
	elseif (value > lowBoundry) then
		self.text:SetTextColor(1,1,0);
	else
		self.text:SetTextColor(1,0,0);
	end
end

-- Add Plugin
function InfoPanel.AddPlugin(align)
	local frame = CreateFrame("Button",nil,f);

--frame:SetBackdrop({ bgFile = "Interface\\Tooltips\\UI-Tooltip-Background", tileSize = 8, edgeSize = 8, insets = { left = 0, right = 0, top = 0, bottom = 0 } });
--frame:SetBackdropColor(0,1,0);

	frame:SetWidth(iconSize);
	frame:SetHeight(f:GetHeight() - 2);
	frame.icon = frame:CreateTexture(nil,"OVERLAY");
	frame.icon:SetPoint("LEFT");
	frame.icon:SetWidth(iconSize);
	frame.icon:SetHeight(iconSize);
	frame.icon:SetTexCoord(0.07,0.93,0.07,0.93);
	frame.text = frame:CreateFontString(nil,"OVERLAY","GameFontHighlight");
	frame.text:SetPoint("LEFT",frame.icon,"RIGHT",4,0);
	frame:EnableMouse(1);
	frame:RegisterForClicks("AnyUp");
	frame:SetScript("OnEvent",Default_OnEvent);
	frame:SetScript("OnEnter",Default_OnEnter);
	frame:SetScript("OnLeave",HideGTT);
	-- Functions
	frame.SetText = SetText;
	frame.SetTextColor = SetTextColor;
	-- Anchor
	if (align == "right") then
		frame.isRightAligned = 1;
		frame:SetPoint("RIGHT",-4,0);
	elseif (lastPlugin) then
		frame:SetPoint("LEFT",lastPlugin,"RIGHT",6,0);
	else
		frame:SetPoint("LEFT",4,0);
	end
	lastPlugin = frame;
	-- Return
	return frame;
end