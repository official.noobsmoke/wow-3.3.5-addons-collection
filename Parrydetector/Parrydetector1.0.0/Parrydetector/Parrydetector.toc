## Interface: 30300
## Title: Parrydetector
## Author: Vôô (Kel'thuzad)
## Version: 1.0
## Notes: For any one who needs to know when boss is parrying attacks
## LoadManagers: AddonLoader
## SavedVariables: ParrydetectorDB
## X-Embeds: Ace3
## OptionalDeps: Ace3

embeds.xml
Parrydetector.lua