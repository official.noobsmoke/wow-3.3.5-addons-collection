Parrydetector = LibStub("AceAddon-3.0"):NewAddon("Parrydetector", "AceConsole-3.0", "AceEvent-3.0")

	local enabled = true
	local VERSION = "0.1"

	function Parrydetector:OnEnable()
		enabled = true
		self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	end
	function Parrydetector:OnDisable()
		enabled = false
		self:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
		self:Print("ParryDetector enabled.")
	end

	function Parrydetector:COMBAT_LOG_EVENT_UNFILTERED(...)
		local eventType, sourceGUID, sourceName, sourceFlags,destGUID, destName, _, prefix1, prefix2, prefix3 = select(3,...)
		local PlayerGUID = UnitGUID("player")
		if (eventType == "SWING_MISSED" and prefix1 == "PARRY" and destGUID == UnitGUID("target")) then
			Parrydetector:Announce("PARRY",sourceName,destName)
		end
	end
	--sourceGUID ~=PlayerGUID and

	function Parrydetector:Announce(type,name,target)
		if (type == "PARRY") then
		local s = name.." has caused a PARRY in "..target.."!"
		self:Print(s)
	end
end