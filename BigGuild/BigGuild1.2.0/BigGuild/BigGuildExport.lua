--
--	BigGuildExport
--
--	BigGuildExport is a window in the BigGuild addon. This window adds the ability to export
--  a list of guildmembers names in a text list that can be cut-and-pasted out of WoW.
--
--

function BigGuildExport_OnLoad()

	--Allows BigGuild to be closed with the Escape key
	tinsert(UISpecialFrames, "BigGuildExportFrame");
	
	--Dragging involves some special registration
	BigGuildExportFrame:RegisterForDrag("LeftButton");
end

function BigGuildExport_ShowWindow()
	local printClass = BigGuildExportShowClass:GetChecked();
	local printLevel = BigGuildExportShowLevel:GetChecked();
	local printRank  = BigGuildExportShowRank:GetChecked();
	local printLastOnline  = BigGuildExportShowLastOnline:GetChecked();
	
	local exportString = "";
	
	-- Iterate BigGuildFilteredRoster, add in the name of each member
	for i = 1, table.getn(BigGuildFilteredRoster), 1 do
		local memberTable = nil;
		local memberTableIndex = BigGuildFilteredRoster[i];
		if memberTableIndex then
			memberTable = BigGuildRoster[memberTableIndex];
		end
		
		if memberTable ~= nil then
			exportString = exportString .. memberTable["name"];
			
			if printClass then 
				exportString = exportString .. ", " .. memberTable.class;
			end
			if printLevel then
				exportString = exportString .. ", " .. memberTable.level;
			end
			if printRank then
				exportString = exportString .. ", " .. memberTable.rank;
			end
			if printLastOnline then
				exportString = exportString .. ", " .. memberTable.lastOnlineString;
			end
			exportString = exportString .. "\n";
		end
	end
	
	BigGuildExportFrame:Show(); 
	BigGuildExportEditBox:SetFocus();
	BigGuildExportEditBox:SetText(exportString);
	BigGuildExportEditBox:HighlightText();
end