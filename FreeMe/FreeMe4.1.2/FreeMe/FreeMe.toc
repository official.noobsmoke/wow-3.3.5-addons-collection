## Interface: 30200
## Title: FreeMe
## Notes: A simple buff remover.
## X-Category: Buffs
## X-License: All Rights Reserved
## Author: Funkydude
## SavedVariables: FREEME_TBL, FREEME_OFF
## Version: 4.12
## X-Curse-Packaged-Version: v4.12
## X-Curse-Project-Name: FreeMe: Simple Buff Remover
## X-Curse-Project-ID: free-me
## X-Curse-Repository-ID: wow/free-me/mainline

FreeMe.lua

