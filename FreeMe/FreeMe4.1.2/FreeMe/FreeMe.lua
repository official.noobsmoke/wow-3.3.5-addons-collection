--[[        FREE ME! Buff Remover        ]]--

--[[ Startup ]]--
local pName = UnitName("player")
if type(_G["FREEME_TBL"]) ~= "table" then
	_G["FREEME_TBL"] = {}
	_G["FREEME_TBL"][1038] = true --Salv
	_G["FREEME_TBL"][25895] = true --G Salv
	_G["FREEME_TBL"][30300] = true --NP
	_G["FREEME_TBL"][30299] = true --NP
	_G["FREEME_TBL"][30301] = true --NP
	_G["FREEME_TBL"][30302] = true --NP
end
local function Print(msg)
	_G.ChatFrame1:AddMessage("|cff33ff99FreeMe|r: "..msg)
end

--[[ Localization ]]--
local REM = "%s Removed."
local ADD = "%s Added."
local FREEME_ON = "On"
local FREEME_STANDBY = "Off"
local FREEME = "Commands are /freeme on and /freeme off\nTo add or remove spells type /freeme [spellID]"

--[[
local L = GetLocale()
if L == "frFR" then
	REM = ""
end
Add locale here!
]]

--[[  Register the buff gain events and cancel the buff when its found, also print to chat that we cancelled it  ]]--
local frame = CreateFrame("Frame", "FreeMe")

frame:SetScript("OnEvent", function(_, _, _, event, _, _, _, _, player, _, spellID)
	if event == "SPELL_AURA_APPLIED" and not _G["FREEME_OFF"] and player == pName then
		if (UnitInBattleground("player") == nil) and _G["FREEME_TBL"][spellID] then
			local n = GetSpellInfo(spellID)
			frame:CancelBuff(n)
			--Print(format(REM, n))
		end
	end
end)
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")

--[[ Small OnUpdate script, we need this to delay the time between the gain and the buff cancel or it won't work ]]--
local elap
function frame:CancelBuff(name)
	elap = 0
	frame:SetScript("OnUpdate", function(_, e)
	elap = elap + e
		if elap >= 0.1 then
			CancelUnitBuff("player", name)
			self:SetScript("OnUpdate", nil)
		end
	end)
end

--[[ Register the slash command /freeme on and /freeme off and work with spell ID's]]--
_G["SlashCmdList"]["FREEME"] = function(msg)
	local id = tonumber(msg)
	if id then
		if _G["FREEME_TBL"][id] then
			_G["FREEME_TBL"][id] = nil
			Print(format(REM, GetSpellInfo(id)))
		else
			_G["FREEME_TBL"][id] = true
			Print(format(ADD, GetSpellInfo(id)))
		end
	else
		msg = string.lower(msg)
		if msg == "on" then
			_G["FREEME_OFF"] = nil
			Print(FREEME_ON)
		elseif msg == "off" then
			_G["FREEME_OFF"] = true
			Print(FREEME_STANDBY)
		elseif msg == "" then
			Print(FREEME)
		end
	end
end
_G["SLASH_FREEME1"] = "/freeme"

