--[[ Special Thanks to a couple of my guild mates for testing some stuff
	out with me. Favvi, Dometi, Miri, Vis... Mainly Favvi though!
-- ]]

local default = {
	["autoyes"] = false,
	["autono"] = false,
	["silence"] = false,
	["alwaysrcs"] = false,
	["volume"] = {vol=.5, du=false}
}

local EventFrame = CreateFrame("Frame", "AutoReadyCheck");
EventFrame:RegisterEvent("READY_CHECK")
EventFrame:RegisterEvent("ADDON_LOADED")

---------------------------
-- Slash Commands
---------------------------

SLASH_AUTOREADYCHECK1, SLASH_AUTOREADYCHECK2 = "/autoreadycheck", "/arc"

local function slashCommandHandler(msg, editbox)
	local msg, restOfCmd = msg:match("^(%S*)%s*(.-)$")
	msg, restOfCmd = strlower(msg), strlower(restOfCmd)
	
	if msg == "autoyes" then
	
		if arc[msg] ~= true then
		
			arc[msg] = true
			arc["autono"] = false
			print("Auto select \"Ready\" enabled.")
			
		else
		
			arc[msg] = false
			print("Auto select \"Ready\" disabled.")
			
		end
		
	elseif msg == "autono" then
	
		if arc[msg] ~= true then
		
			arc[msg] = true
			arc["autoyes"] = false
			print("Auto select \"Not Ready\" enabled.")
			
		else
		
			arc[msg] = false
			
			print("Auto select \"Not Ready\" disabled.")
			
		end
		
	elseif msg == "silence" then
	
		if arc[msg] ~= true then
		
			arc[msg] = true
			arc["alwaysrcs"] = false
			silenceReadyCheck()
			print("Silencing Ready Check Sound")
			
		else
		
			arc[msg] = false
			unsilenceReadyCheck()
			print("Playing Ready Check Sound")
			
		end
		
	elseif msg == "alwaysrcs" then
		
		if arc[msg] ~= true then
		
			arc[msg] = true
			arc["silence"] = false
			SetCVar("Sound_EnableSoundWhenGameIsInBG", 1)
			unsilenceReadyCheck()
			print("Always playing the Ready Check sound, no matter what!")
			
		else
		
			arc[msg] = false
			print("Only playing the Ready Check sound when your sound is on.")
			
		end
		
	elseif msg == "volume" then
	
		if restOfCmd ~= "" then
		
			if not pcall( function () return restOfCmd/100 end, restOfCmd) then
			
				print('ERROR: "'..restOfCmd..'" is not a number.')
				
			else 
			
				arc["alwaysrcs"] = true
				arc["volume"].vol = restOfCmd/100
				print("Ready Check sound volume is set to ".. arc["volume"].vol*100 .."%.")
				
			end
			
		else
			
			if arc[msg].du ~= true then
			
				arc[msg].du = true
				print("Changing game volume temporarily when ready checks happen.")
				
			else
			
				arc[msg].du = false
				print("No longer changing game volume temporarily when ready checks happen.")
			
			end
			
		end
	
	elseif msg == "default" then
	
		arc = default
		
	else
	
		print("AutoReadyCheck Options:")
		print("    /arc autoyes  -- Toggle Auto Select \"Ready\"")
		print("    /arc autono   -- Toggle Auto Select \"Not Ready\"")
		print("    /arc silence   -- Toggle Ready Check Sound")
		print("    /arc alwaysrcs   -- Toggle Always playing the Ready Check sound, no matter what!")
		print("    /arc volume <##>  -- Toggles Ready Check volume selection, or if ## given, then sets the sound volume that ARC will play ready checks.")
		print("    /arc default   -- Reset to default settings (ready checks function normally).")
		
	end
end

SlashCmdList["AUTOREADYCHECK"] = slashCommandHandler

---------------------------
-- Un/Silence Ready Check
---------------------------

function silenceReadyCheck()

	ReadyCheckListenerFrame:SetScript("OnShow", nil)
	ReadyCheckListenerFrame:SetScript("OnShow", function(...) PlaySoundFile("") end)
	
end

function unsilenceReadyCheck()

	ReadyCheckListenerFrame:SetScript("OnShow", nil)
	ReadyCheckListenerFrame:SetScript("OnShow", function(...) PlaySound("ReadyCheck") end)
	
end

---------------------------
-- Get/Set Sound Settings
---------------------------

local soundVars
local function getSoundSettings()

	soundVars = {
		["Sound_EnableAllSound"] = GetCVar("Sound_EnableAllSound"),
		["Sound_EnableSFX"] = GetCVar("Sound_EnableSFX"),
		["Sound_EnableSoundWhenGameIsInBG"] = GetCVar("Sound_EnableSoundWhenGameIsInBG"), -- Yes, it is redundant @line 94.
		["Sound_MasterVolume"] = GetCVar("Sound_MasterVolume"),
		["Sound_SFXVolume"] = GetCVar("Sound_SFXVolume")
	}
	
end

local function setSoundSettings(i)
   
	if i then
      
		for k in pairs(soundVars) do
			SetCVar(k, i)
		end
      
	else
		  
		for k,v in pairs(soundVars) do
			SetCVar(k, v)
		end
      
	end
	
end

---------------------------
-- Always Ready Check Sound
---------------------------

local SoundFrame = CreateFrame("Frame")
function playReadyCheck()
	
	-- Addon is changing the user's sound.
	-- Make note that it was the addon that did it.
	local changed = true
	
	getSoundSettings()
	if arc["volume"].du and arc["alwaysrcs"] then
	
		-- ALWAYS play sound and set volume.
		setSoundSettings(1)
		SetCVar("Sound_MasterVolume", arc["volume"].vol)
		SetCVar("Sound_SFXVolume", arc["volume"].vol)
		
	elseif arc["volume"].du then
	
		-- Just Change volume.
		SetCVar("Sound_MasterVolume", arc["volume"].vol)
		SetCVar("Sound_SFXVolume", arc["volume"].vol)
		
	else
		-- Just ALWAYS play sound.
		setSoundSettings(1)
	end
	
	-- Play Ready Check, duh!
	PlaySound("ReadyCheck")
	
	-- Wait for ready check to finish playing.
	local curTime = 0.0
	local function wait(self, elapsed)
   
		curTime = curTime + elapsed
   
		if (curTime < 3) then
			return
		end
		
		-- Revert sound to what it was before ADDON changed it.
		if changed then
		
			setSoundSettings()
			
			-- Addon is no longer reverting sounds.
			changed = false
		
		end
		
	end
	
	SoundFrame:SetScript("OnUpdate", wait)
	SoundFrame:Show()
	
end


---------------------------
-- Events
---------------------------

function EventFrame:OnEvent(event, addon)

	if event == "ADDON_LOADED" and addon == "AutoReadyCheck" then
	
		if not arc then
			arc = default
		end
	
		if arc["silence"] then
			silenceReadyCheck()
		end
		
		if arc["alwaysrcs"] then
			SetCVar("Sound_EnableSoundWhenGameIsInBG", 1)
		end
		
	elseif event == "READY_CHECK" then
		
		if arc["alwaysrcs"] or arc["volume"].du then
			playReadyCheck()
		end
		
		if arc["autoyes"] then
		
			ReadyCheckFrame:Hide()
			ConfirmReadyCheck(1)
			print("You weren't AFK.")
			print("You automatically selected READY for a ready check.")
				
		elseif arc["autono"] then
		
			ReadyCheckFrame:Hide()
			ConfirmReadyCheck(0)
			print("You weren't AFK.")
			print("You automatically selected NOT READY for a ready check.")
			
		end
		
	end
end

EventFrame:SetScript("OnEvent", EventFrame.OnEvent)