## Interface: 30300
## Title: Internal Cooldown Monitor
## Notes: An addon that tracks internal cooldown of your equipment with proc effects.
## Author: Cannadrys / Mug'thol-US
## X-eMail: ewhenn@yahoo.com
## X-Website: 
## SavedVariables: icd_tracking, icd_sound

Libs\LibStub\LibStub.lua
icdmon.xml

