-- Globals Section
--Number of ICD slots USED. 1 = 0 in this case
icd_used = 1                             
icd1dur = 0               
icd1name = "nada"    
icd2dur = 0               
icd2name = "nada"   
icd3dur = 0               
icd3name = "nada"   
icd4dur = 0               
icd4name = "nada"   
icd5dur = 0               
icd5name = "nada"   
icd_tracking = 5
icd_sound = 1  -----play sounds 0=no 1=yes
                          





--ACE 3 TIMER SECTION
MyAddon = LibStub("AceAddon-3.0"):NewAddon("TimerTest", "AceTimer-3.0")

function MyAddon:OnEnable()
  self.timerCount = 0
  self.testTimer = self:ScheduleRepeatingTimer("TimerFeedback", 1)
end

--------------------------
--UPDATE TIMERS/TEXT-------
--------------------------
function MyAddon:TimerFeedback()

-----Decrement Counters----
if (icd1dur > 0) then 
 icd1dur=icd1dur-1
end
if (icd2dur > 0) then 
 icd2dur=icd2dur-1
end

if (icd3dur > 0) then 
 icd3dur=icd3dur-1
end

if (icd4dur > 0) then 
 icd4dur=icd4dur-1
end

if (icd5dur > 0) then 
 icd5dur=icd5dur-1
end


-------Play sound at 3 seconds to Proc Ready if enabled
if (icd1dur == 3 or icd2dur == 3 or icd3dur == 3 or icd4dur == 3 or icd5dur == 3) then
	if (icd_sound == 1) then
		PlaySoundFile("Interface\\AddOns\\icdmon\\warn.mp3")
	end

end

----BUILD STRING TO DISPLAY-------
  
icdstring=""
icdstring2=""
icdstring3=""
icdstring4=""
icdstring5=""

if (icd1name ~= "nada") then
  icdstring = icd1name.." "
  if icd1dur > 12 then
    icdstring=icdstring.."|cFFFF0000"..icd1dur.."|r sec"
  elseif icd1dur > 3 and icd1dur < 13 then
    icdstring=icdstring.."|cF0F00FF0"..icd1dur.."|r sec"
  elseif icd1dur == 0 then
    icdstring=icdstring.."|cFF00FF00".."Ready".."|r"
  else
    icdstring=icdstring.."|cFF00FF00"..icd1dur.."|r sec"	
  end
end 
if (icd2name ~= "nada") then
  icdstring2 = icd2name.." "
  if icd2dur > 12 then
    icdstring2=icdstring2.."|cFFFF0000"..icd2dur.."|r sec"
  elseif icd2dur > 3 and icd2dur < 13 then
    icdstring2=icdstring2.."|cF0F00FF0"..icd2dur.."|r sec"
  elseif icd2dur == 0 then
    icdstring2=icdstring2.."|cFF00FF00".."Ready".."|r"
  else
    icdstring2=icdstring2.."|cFF00FF00"..icd2dur.."|r sec"	
  end
end 
if (icd3name ~= "nada") then
  icdstring3 = icd3name.." "
  if icd3dur > 12 then
    icdstring3=icdstring3.."|cFFFF0000"..icd3dur.."|r sec"
  elseif icd3dur > 3 and icd3dur < 13 then
    icdstring3=icdstring3.."|cF0F00FF0"..icd3dur.."|r sec"
  elseif icd3dur == 0 then
    icdstring3=icdstring3.."|cFF00FF00".."Ready".."|r"
  else
    icdstring3=icdstring3.."|cFF00FF00"..icd3dur.."|r sec"	
  end
end 
if (icd4name ~= "nada") then
  icdstring4 = icd4name.." "
  if icd4dur > 12 then
    icdstring4=icdstring4.."|cFFFF0000"..icd4dur.."|r sec"
  elseif icd4dur > 3 and icd4dur < 13 then
    icdstring4=icdstring4.."|cF0F00FF0"..icd4dur.."|r sec"
  elseif icd4dur == 0 then
    icdstring4=icdstring4.."|cFF00FF00".."Ready".."|r"
  else
    icdstring4=icdstring4.."|cFF00FF00"..icd4dur.."|r sec"	
  end
end 
if (icd5name ~= "nada") then
  icdstring5 = icd5name.." "
  if icd5dur > 12 then
    icdstring5=icdstring5.."|cFFFF0000"..icd5dur.."|r sec"
  elseif icd5dur > 3 and icd5dur < 13 then
    icdstring5=icdstring5.."|cF0F00FF0"..icd5dur.."|r sec"
  elseif icd5dur == 0 then
    icdstring5=icdstring5.."|cFF00FF00".."Ready".."|r"
  else
    icdstring5=icdstring5.."|cFF00FF00"..icd5dur.."|r sec"	
  end
end 


-----DISPLAY STRING---
parent_Line1:SetText(icdstring)
parent_Line2:SetText(icdstring2)
parent_Line3:SetText(icdstring3)
parent_Line4:SetText(icdstring4)
parent_Line5:SetText(icdstring5)


end





--ADD TIMER INTO ICDMON WINDOW
function icdmon_addtimer(duration9, sname)
  
	---icd1name = sname
	---icd1dur = duration9
	

	--parent_Line1:SetText("got here")



-----Flag if we add
local flagged = 0

------Check to see if Cooldown has been tracked, if it does, refresh ICD tracker
  if (sname == icd1name) then
	icd1dur=duration9
	flagged = 1
  end
  if (sname == icd2name) then
	icd2dur=duration9
	flagged = 1
  end
  if (sname == icd3name) then
	icd3dur=duration9
	flagged = 1
  end
  if (sname == icd4name) then
	icd4dur=duration9
	flagged = 1
  end
  if (sname == icd5name) then
	icd5dur=duration9
	flagged = 1
  end		


------------------------------If not detected add it into the earliest spot
if (icd_used == 1 and flagged == 0) then
	icd1name = sname
	icd1dur = duration9
	icd_used = icd_used+1
	flagged = 1
end

if (icd_used == 2 and flagged == 0) then
	icd2name = sname
	icd2dur = duration9
	icd_used = icd_used+1
	flagged = 1
end

if (icd_used == 3 and flagged == 0) then
	icd3name = sname
	icd3dur = duration9
	icd_used = icd_used+1
	flagged = 1
end

if (icd_used == 4 and flagged == 0) then
	icd4name = sname
	icd4dur = duration9
	icd_used = icd_used+1
	flagged = 1
end

if (icd_used == 5 and flagged == 0) then
	icd5name = sname
	icd5dur = duration9
	icd_used = icd_used+1
	flagged = 1
end
	



end









function icdmon_OnEvent(self, event, ...)
	if(event=="COMBAT_LOG_EVENT_UNFILTERED") then
		local timestamp, type, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags = select(1, ...);

		if(type=="SPELL_AURA_APPLIED") then
			local spellId, spellName = select(9, ...);

			-- Make sure it is your own trinket proc			
			if (UnitName("player") == sourceName) then  


				------------------------------
                        	--Edit Spell IDS / Names below
				------------------------------


				--Whispering Fanged Skull
				if (spellId==71402 or spellId==71540 or spellName=="Icy Rage") then
					icdmon_addtimer(45, "Whispering Fang Skull")                            
				end

				--Ashen Ring Proc
				if (spellId==72413 or spellId==72412 or spellId==72416 or spellName=="Frostforged Champion" or spellName=="Frostforged Sage") then
					icdmon_addtimer(60, "Ashen Verdict Ring")                            
				end

				--DMC Greatness
				if (spellId==60234 or spellId==60229 or spellId==60235 or spellId==60233 or spellName=="UNKNOWN buFFFF ID") then         
					icdmon_addtimer(45, "DMC: Greatness")                            
				end

				--Deaths Choice Normal
				if (spellId==67708 or spellId==67703 or spellName=="UNKNOWN buFFFF ID") then               
					icdmon_addtimer(45, "Deaths Choice")                            
				end

				--Deaths Choice Heroic
				if (spellId==67773 or spellId==67772 or spellName=="UNKNOWN buFFFF ID") then            
					icdmon_addtimer(45, "H: Deaths Choice")                            
				end

				--Deathbringers Will
				if (spellId==71492 or spellId==71560 or spellId==71556 or spellId==71485 or spellId==71492 or spellId==71486 or spellId==71484 or spellId==71491 or spellId==71487 or spellId==71561 or spellId==71599 or spellId==71558 or spellId==71557 or spellName=="UNKNOWN buFFFF ID") then             
					icdmon_addtimer(105, "Deathbringer Will")                            
				end

				--Mirror of truth/coren
				if (spellId==60065 or spellId==60065 or spellName=="Reflection of Torment") then             
					icdmon_addtimer(45, "Mirror/Corens")                            
				end

				--Banner of Victory
				if (spellId==67671 or spellId==67671 or spellName=="UNKNOWN BUFFFFp") then             
					icdmon_addtimer(45, "Banner of Victory")                            
				end

				--Pyrite Infuser
				if (spellId==65014 or spellId==65014 or spellName=="Pyrite Infusion") then               
					icdmon_addtimer(50, "Pyrite Infuser")                            
				end

				--Blood of the old god
				if (spellId==64790 or spellId==64790 or spellName=="Blood of the Old God") then     
					icdmon_addtimer(50, "Blood Old God")                            
				end

				--Dark Matter
				if (spellId==65024 or spellId==65024 or spellName=="Implosion") then            
					icdmon_addtimer(45, "Dark Matter")                            
				end

				--Comets Trail
				if (spellId==64772 or spellId==64772 or spellName=="Comet's Trail") then           
					icdmon_addtimer(45, "Comets Trail")                            
				end

				--Grim Toll
				if (spellId==60437 or spellId==60437 or spellName=="Grim Toll") then   
					icdmon_addtimer(45, "Grim Toll")                            
				end

				--Mjolnir Runestone
				if (spellId==65019 or spellId==65019 or spellName=="Mjolnir Runestone") then     
					icdmon_addtimer(45, "Mjolnir Runestone")                            
				end

				--Needle Scorpion
				if (spellId==71403 or spellId==71403 or spellName=="Fatal Flaws") then            
					icdmon_addtimer(45, "Needle Enc Scorpion")                            
				end

				--Sharpened Twilight Scale
				if (spellId==75457 or spellId==75455 or spellName=="Piercing Twilight") then            
					icdmon_addtimer(45, "Twilight Scale")                            
				end
			

			end
			

		end


	end

	if(event == "ADDON_LOADED") then
		----print(arg1)
		---icd_tracking =  select(1,...)		
		icd_winsize = icd_tracking * 16
		icd_winsize = icd_winsize + 15
		icdmon_resize(icd_winsize)
	end
end

function icdmon_reset()

	icd_used = 1                             
	icd1dur = 0               
	icd1name = "nada"    
	icd2dur = 0               
	icd2name = "nada"   
	icd3dur = 0               
	icd3name = "nada"   
	icd4dur = 0               
	icd4name = "nada"   
	icd5dur = 0               
	icd5name = "nada"   
end


function icdmon_resize(newsize)

		--icd_height = icdtext:GetHeight()
		--icd_y = icdtext:GetTop() 
		--icd_x = icdtext:GetLeft()
		--icd_diff = icd_height - newsize
		--icd_offset = icd_height - icd_diff
		--icdtext:SetPoint("BOTTOMLEFT", icd_x, icd_y-icd_offset);
		icdtext:SetHeight(newsize);
		icdtext:SetWidth(200);
end




function icdmon_SlashCommandHandler(msg)

	msg = string.lower(msg)

	if (msg == "") then
		print("ICDMON USAGE: Control click to drag to a different part of your screen.")
		print("/icdmon 1-5 - Sets number of procs to watch for and sizes window appropriately between 1 and 5, example: /icd 3")
		print("/icdmon hide  - hides the proc window")
		print("/icdmon show  - shows the proc window")
		print("/icdmon sound on|off - sets 3 sec warning on ICD on or off, example: /icd sound on")
		print("/icdmon reset - resets the proc list")
	end

	if (msg == "reset") then
		icdmon_reset()
	end	

	if (msg == "sound on") then
		icd_sound = 1
	end	

	if (msg == "sound off") then
		icd_sound = 0
	end	
	
	if (msg == "1") then		
		icdmon_resize(33)
		icd_tracking = 1
	end	

	if (msg == "2") then		
		icdmon_resize(48)
		icd_tracking = 2
	end

	if (msg == "3") then		
		icdmon_resize(63)
		icd_tracking = 3
	end

	if (msg == "4") then		
		icdmon_resize(78)
		icd_tracking = 4
	end

	if (msg == "5") then		
		icdmon_resize(94)
		icd_tracking = 5
	end

	if (msg == "hide") then
		icdtext:Hide()
	end	


	if (msg == "show") then
		icdtext:Show()
	end	



end



function icdmon_OnLoad()
	icdmon:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
	icdmon:RegisterEvent("ADDON_LOADED");
  	SLASH_icdmon1 = "/icdmon";
  	SLASH_icdmon2 = "/icd";
 	 SlashCmdList["icdmon"] = function(msg)
		icdmon_SlashCommandHandler(msg);
	end


end
