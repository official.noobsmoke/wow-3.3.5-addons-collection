-----------------------------------------------
--[[ Raeli's Spell Announcement Addon Core ]]--
-----------------------------------------------
RSA = LibStub("AceAddon-3.0"):NewAddon("RSA", "AceConsole-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale("RSA")

-------------------------------
--[[ Some random variables ]]--
-------------------------------
local db
local pName = UnitName("player")
local ReminderSpell = ""

--------------------------
--[[ Default Settings ]]--
--------------------------
local defaults = {
	global = {
	},
	profile = {
		class = "",	
		channel = "MyCustomChannel",
		oldwheeldown = "",
		oldwheelup = "",
		raid_warn = 1,
		RTime = 15,
		CTime = 10,
		pally = {
			HoF = {--Hand of Freedom
				HoFW = true,
				HoFR = false,
				HoFC = true,
				HoFS = false,	
			},
			HoSac = {--Hand of Sacrifice
				HoSacW = true,
				HoSacR = false,
				HoSacC = true,
				HoSacS = false,
			},
			HoSalv = {--Hand of Salvation
				HoSalvW = true,
				HoSalvR = false,
				HoSalvC = true,
				HoSalvS = false,
			},
			HoP = {--Hand of Protection
				HoPW = true,
				HoPR = false,
				HoPC = true,
				HoPS = false,
			},
			LoH = {--Lay on Hands
				LoHW = true,
				LoHR = false,
				LoHC = true,
				LoHS = false,
			},
			DI = {--Divine Intervention
				DIW = true,
				DIR = true,
				DIC = false,
				DIS = false,
			},			
			DSac = {--Divine Sacrifice
				DSacR1 = false,
				DSacC1 = false,
				DSacS1 = false,
				DSacR2 = false,
				DSacC2 = false,
				DSacS2 = false,
				DSacRM = true,
				DSacCM = true,
				DSacSM = false,
				Merge = false,
			},
			DP = {--Divine Protection
				DPR = true,
				DPC = false,
				DPS = false,
			},
			AM = {--Aura Mastery
				AMR = true,
				AMC = true,
				AMS = false,
			},			
			AD = {--Ardent Defender
				ADR = true,
				ADC = false,
				ADS = false,
			},			
			Forbearance = {--Forbearance
				--ForbearanceR = false,
				--ForbearanceC = false,
				ForbearanceS = true,
			},
			DPlea = {--Divine Plea
				--DPleaR = false,
				--DPleaC = false,
				DPleaS = true,
			},
			RD = {--Righteous Defense
				RDR = true,
				RDS = false,
				RDC = false,
				RDResist = true,
				RDSuccess = true,
			},
			HoR = {--Hand of Reckoning
				HoRR = true,
				HoRS = false,
				HoRC = false,
				HoRResist = true,
				HoRSuccess = true,
			},
			RF = {--Righteous Fury
				RFPD = false,
				RFSW = false,
				RFOn = false,
				RFR = false,
			},
		},-- End Pally Section
		priest = {
			GS = {--Guardian Spirit
				GSW = true,
				GSR = true,
				GSC = false,
				GSS = false,
			},
			PS = {--Pain Suppression
				PSW = true,
				PSR = true,
				PSC = false,
				PSS = false,
			},
			PI = {--Power Infusion
				PIW = true,
				PIR = false,
				PIC = false,
				PIS = false,
			},
			FW = {--Fear Ward
				FWW = true,
				FWR = false,
				FWC = true,
				FWS = false,
			},
			DH = {--Divine Hymn
				--DHW = true,
				DHR = false,
				DHC = true,
				DHS = false,
			},
			HoH = {--Hymn of Hope
				--HoHW = true,
				HoHR = false,
				HoHC = true,
				HoHS = false,
			},
			IF = {--Inner Fire
				IFSW = false,
				IFOn = false,
				IFR = false,
			},			
		},-- End Priest Section
		warrior = {
			Taunt = {--Taunt
				TauntR = true,
				TauntS = false,
				TauntC = false,
				TauntResist = true,
				TauntSuccess = true,
			},
			SWall = {--Shield Wall
				SWallR = true,
				SWallC = false,
				SWallS = false,
			},
			LStand = {--Last Stand
				LStandR = true,
				LStandC = false,
				LStandS = false,
			},				
		},-- End Warrior Section
	},
}

-------------------------------
--[[ Configuration Options ]]--
-------------------------------
local options = {
    name = L["Raeli's Spell Announcer"],
    handler = RSA,
    type = 'group',
	childGroups = "tab",
    args = {
		maindesc = {
		type = "description",
		name = L["Configuration settings for Raeli's Spell Announcer."],
		},
		chansettings = {
			type = "group",
			name = L["General Settings"],
			order = 0,
			args ={
				msghead = {
					type = "header",
					name = L["Custom Channel Settings"],
					order = 0,					
				},
				msddesc = {
					type = "description",
					name = L["Here you can set your own custom channel. This allows you to announce spells in a channel of your choice."],
					order = 1,
				},
				msg = {
					type = "input",
					name = L["Channel Name"],
					usage = L["[Channel Name]"],
					order = 2,
					get = function(info)
							return RSA.db.profile.channel
						end,
					set = function(info, value)
							RSA.db.profile.channel = value
						end,
				},
				raid_warn1 = {
					type = "header",
					name = L["Local Message Options"],
					order = 3,
				},
				raid_warn2 = {
					type = "description",
					name = L.raid_warn2,
					order = 4,
				},
				raid_warn3 = {
					type = "select",
					name = L["Select"], 
					order = 5,
					values = { L["Only to chat"], L["Only to Raid Warning"], L["Send to Both"] },
					get = function(info)
							return RSA.db.profile.raid_warn
						end,
					set = function(info, value)
							RSA.db.profile.raid_warn = value
						end,					
				},			
			},
		},	
		
		pally = {
			type = "group",
			name = L["Spells"],
			desc = L["Announcement settings for your Spells."],
			order = 1,
			hidden = function() 
						return RSA.db.profile.class ~= "PALADIN" 
					end,
			childGroups = "select",
			args = {
			
				RD = {
				type = "group",
				name = L["Righteous Defense"],
				desc = L["Settings for Righteous Defense"],
				args = {
						RD0 = {
							type = "header",
							name = L["Channel Settings"],
							order = 0,
						},
						RD1 = {
							type = "description",
							name = L["Here you can set which channels you want to send announcements to."],
							order = 1,
						},
						RDR = {
						type = "toggle",
						name = L["Raid/Party"],
						desc = L.raidpartydesc,
						order = 3,
						get = function(info) 
								return RSA.db.profile.pally.RD.RDR
							  end,
						set = function (info, value)
								RSA.db.profile.pally.RD.RDR = value
							  end,	  
						},
						RDC = {
						type = "toggle",
						name = L["Custom Channel"],
						desc = L["Announces in your custom channel."],
						order = 2,
						get = function(info) 
								return RSA.db.profile.pally.RD.RDC
							  end,
						set = function (info, value)
								RSA.db.profile.pally.RD.RDC = value
							  end,	  
						},
						RDS = {
						type = "toggle",
						name = L["Local"],
						desc = L["Sends a local message to your default chat window that only you can see."],
						order = 4,
						get = function(info) 
								return RSA.db.profile.pally.RD.RDS
							  end,
						set = function (info, value)
								RSA.db.profile.pally.RD.RDS = value
							  end,	  
						},
						RD5 = {
							type = "header",
							name = L["Announcement Settings"],
							order = 5,
						},
						RD6 = {
							type = "description",
							name = L.rd_desc,
							order = 6,
						},
						RD7 = {
							type = "toggle",
							name = L["Successful Taunts"],
							desc = L["Check this to announce upon successful taunt."],
							order = 7,
							get = function(info) 
									return RSA.db.profile.pally.RD.RDSuccess
								  end,
							set = function (info, value)
									RSA.db.profile.pally.RD.RDSuccess = value
								  end,							
						},
						RD8 = {
							type = "toggle",
							name = L["Resists"],
							desc = L["Check this to announce upon taunt resists."],
							order = 8,
							get = function(info) 
									return RSA.db.profile.pally.RD.RDResist
								  end,
							set = function (info, value)
									RSA.db.profile.pally.RD.RDResist = value
								  end,							
						},
					},
				},-- End of Righteous Defense #####
				
				HoR = {
					type = "group",
					name = L["Hand of Reckoning"],
					desc = L["Settings for Hand of Reckoning"],
					args = {
						HoR0 = {
							type = "header",
							name = "Channel Settings",
							order = 0,
						},
						HoR1 = {
							type = "description",
							name = "Here you can set which channels you want to send announcements to.",
							order = 1,
						},
						HoRR = {
							type = "toggle",
							name = L["Raid/Party"],
							desc = L.raidpartydesc,
							order = 3,
							get = function(info) 
									return RSA.db.profile.pally.HoR.HoRR
								  end,
							set = function (info, value)
									RSA.db.profile.pally.HoR.HoRR = value
								  end,	  
						},
						HoRC = {
							type = "toggle",
							name = L["Custom Channel"],
							desc = L["Announces in your custom channel."],
							order = 2,
							get = function(info) 
									return RSA.db.profile.pally.HoR.HoRC
								  end,
							set = function (info, value)
									RSA.db.profile.pally.HoR.HoRC = value
								  end,	  
						},
						HoRS = {
							type = "toggle",
							name = L["Local"],
							desc = L["Sends a local message to your default chat window that only you can see."],
							order = 4,
							get = function(info) 
									return RSA.db.profile.pally.HoR.HoRS
								  end,
							set = function (info, value)
									RSA.db.profile.pally.HoR.HoRS = value
								  end,	  
						},
						HoR5 = {
							type = "header",
							name = L["Announcement Settings"],
							order = 5,
						},
						HoR6 = {
							type = "description",
							name = L["Here you can set whether Hand of Reckoning announces on successful casts, resists, or both."],
							order = 6,
						},
						HoR7 = {
							type = "toggle",
							name = L["Successful Taunts"],
							desc = L["Check this to announce upon successful taunt."],
							order = 7,
							get = function(info) 
									return RSA.db.profile.pally.HoR.HoRSuccess
								  end,
							set = function (info, value)
									RSA.db.profile.pally.HoR.HoRSuccess = value
								  end,	 							
						},
						HoR8 = {
							type = "toggle",
							name = L["Resists"],
							desc = L["Check this to announce upon taunt resists."],
							order = 8,
							get = function(info) 
									return RSA.db.profile.pally.HoR.HoRResist
								  end,
							set = function (info, value)
									RSA.db.profile.pally.HoR.HoRResist = value
								  end,	 							
						},
						
					},
				},-- End of Hand of Reckoning #####
			
				DSac = {
					type = "group",
					name = L["Divine Sacrifice"],
					desc = L["Channel settings for Divine Sacrifice"],
					args = {
						DSac1 = {
						type = "toggle",
						name = "Merge initial cast announcements",
						order = 0,
						width = "full",
						get = function(info) 
								return RSA.db.profile.pally.DSac.Merge
							  end,
						set = function (info, value)
								RSA.db.profile.pally.DSac.Merge = value
							  end,					
						},
						DSac2 = {
						type = "description",
						name = "Enabling this option will merge the casting announcements for Divine Sacrifice and Guardian, so the initial Messsage will note that both have activated. You will still, however, send out seperate messages when each ends.",
						order = 1,
						},
						DSacM = {
						type = "header",
						name = "Merged Divine Sacrifice and Guardian",
						order = 2,
						hidden = function()
									return RSA.db.profile.pally.DSac.Merge == false
								end,							
						},
							DSacRM = {
							type = "toggle",
							name = L["Raid/Party"],
							desc = L.raidpartydesc,
							order = 4,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacRM
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacRM = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == false
									end,							  
							},
							DSacCM = {
							type = "toggle",
							name = L["Custom Channel"],
							desc = L["Announces in your custom channel."],
							order = 4,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacCM
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacCM = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == false
									end,								  
							},
							DSacSM = {
							type = "toggle",
							name = L["Local"],
							desc = L["Sends a local message to your default chat window that only you can see."],
							order = 4,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacSM
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacSM = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == false
									end,								  
							},
						
						DSac3 = {
						type = "header",
						name = "Divine Sacrifice Channels",
						order = 3,
						hidden = function()
									return RSA.db.profile.pally.DSac.Merge == true
								end,
						},
							DSacR = {
							type = "toggle",
							name = L["Raid/Party"],
							desc = L.raidpartydesc,
							order = 4,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacR1
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacR1 = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == true
									end,							  
							},
							DSacC = {
							type = "toggle",
							name = L["Custom Channel"],
							desc = L["Announces in your custom channel."],
							order = 4,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacC1
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacC1 = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == true
									end,								  
							},
							DSacS = {
							type = "toggle",
							name = L["Local"],
							desc = L["Sends a local message to your default chat window that only you can see."],
							order = 4,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacS1
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacS1 = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == true
									end,								  
							},						
						
						DSac4 = {
						type = "header",
						name = "Divine Guardian Channels",
						order = 5,
						hidden = function()
									return RSA.db.profile.pally.DSac.Merge == true
								end,						
						},
							DSacR2 = {
							type = "toggle",
							name = L["Raid/Party"],
							desc = L.raidpartydesc,
							order = 6,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacR2
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacR2 = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == true
									end,							  
							},
							DSacC2 = {
							type = "toggle",
							name = L["Custom Channel"],
							desc = L["Announces in your custom channel."],
							order = 6,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacC2
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacC2 = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == true
									end,								  
							},
							DSacS2 = {
							type = "toggle",
							name = L["Local"],
							desc = L["Sends a local message to your default chat window that only you can see."],
							order = 6,
							get = function(info) 
									return RSA.db.profile.pally.DSac.DSacS2
								  end,
							set = function (info, value)
									RSA.db.profile.pally.DSac.DSacS2 = value
								  end,
							hidden = function()
										return RSA.db.profile.pally.DSac.Merge == true
									end,								  
							},
						
						
						--[[hidden = function() 
									return RSA.db.profile.class ~= "PALADIN" 
								end,]]--

					},
				},-- End of Divine Sacrifice #####
				
				DP = {
				type = "group",
				name = L["Divine Protection"],
				desc = L["Channel settings for Divine Protection"],
				args = {
					DPR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.DP.DPR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DP.DPR = value
						  end,	  
					},
					DPC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.DP.DPC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DP.DPC = value
						  end,	  
					},
					DPS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.DP.DPS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DP.DPS = value
						  end,	  
					},
				},
				},-- End of Divine Protection #####				
				
				HoF = {
				type = "group",
				name = L["Hand of Freedom"],
				desc = L["Channel settings for Hand of Freedom"],
				args = {
					HoFW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.pally.HoF.HoFW
						  end,
						--get = "GetHoFW",  
					set = function (info, value)
							RSA.db.profile.pally.HoF.HoFW = value
						  end,
						
					},
					HoFR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.HoF.HoFR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoF.HoFR = value
						  end,	  
					},
					HoFC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.HoF.HoFC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoF.HoFC = value
						  end,	  
					},
					HoFS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.HoF.HoFS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoF.HoFS = value
						  end,	  
					},
				},
				},-- End of Hand of Freedom #####
				
				LoH = {
				type = "group",
				name = L["Lay on Hands"],
				desc = L["Channel settings for Lay on Hands"],
				args = {
					LoHW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.pally.LoH.LoHW
						  end,
						--get = "GetLoHW",  
					set = function (info, value)
							RSA.db.profile.pally.LoH.LoHW = value
						  end,
						
					},
					LoHR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.LoH.LoHR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.LoH.LoHR = value
						  end,	  
					},
					LoHC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.LoH.LoHC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.LoH.LoHC = value
						  end,	  
					},
					LoHS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.LoH.LoHS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.LoH.LoHS = value
						  end,	  
					},
				},
				},-- End of Lay on Hands #####				
				
				HoSac = {
				type = "group",
				name = L["Hand of Sacrifice"],
				desc = L["Channel settings for Hand of Sacrifice"],
				args = {
					HoSacW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.pally.HoSac.HoSacW
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoSac.HoSacW = value
						  end,
						
					},
					HoSacR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.HoSac.HoSacR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoSac.HoSacR = value
						  end,	  
					},
					HoSacC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.HoSac.HoSacC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoSac.HoSacC = value
						  end,	  
					},
					HoSacS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.HoSac.HoSacS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoSac.HoSacS = value
						  end,	  
					},
				},
				},-- End of Hand of Sacrifice #####
				
				HoSalv = {
				type = "group",
				name = L["Hand of Salvation"],
				desc = L["Channel settings for Hand of Salvation"],
				args = {
					HoSalvW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.pally.HoSalv.HoSalvW
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoSalv.HoSalvW = value
						  end,
						
					},
					HoSalvR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.HoSalv.HoSalvR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoSalv.HoSalvR = value
						  end,	  
					},
					HoSalvC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.HoSalv.HoSalvC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoSalv.HoSalvC = value
						  end,	  
					},
					HoSalvS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.HoSalv.HoSalvS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoSalv.HoSalvS = value
						  end,	  
					},
				},
				},-- End of Hand of Salvation #####
				
				HoP = {
				type = "group",
				name = L["Hand of Protection"],
				desc = L["Channel settings for Hand of Protection"],
				args = {
					HoPW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.pally.HoP.HoPW
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoP.HoPW = value
						  end,
						
					},
					HoPR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.HoP.HoPR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoP.HoPR = value
						  end,	  
					},
					HoPC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.HoP.HoPC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoP.HoPC = value
						  end,	  
					},
					HoPS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.HoP.HoPS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.HoP.HoPS = value
						  end,
					},
				},
				},-- End of Hand of Protection #####
				
				DI = {
				type = "group",
				name = L["Divine Intervention"],
				desc = L["Channel settings for Divine Intervention"],
				args = {
					DIW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.pally.DI.DIW
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DI.DIW = value
						  end,
						
					},
					DIR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.DI.DIR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DI.DIR = value
						  end,	  
					},
					DIC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.DI.DIC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DI.DIC = value
						  end,	  
					},
					DIS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.DI.DIS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DI.DIS = value
						  end,
					},
				},
				},-- End of Divine Intervention #####
				
				Forbearance = {
				type = "group",
				name = L["Forbearance"],
				desc = L["Channel settings for Forbearance"],
				args = {
					--[[ForbearanceR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.Forbearance.ForbearanceR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.Forbearance.ForbearanceR = value
						  end,	  
					},
					ForbearanceC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.Forbearance.ForbearanceC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.Forbearance.ForbearanceC = value
						  end,	  
					},]]-- -- Why would we want to announce Forbearance at all except to ourselves?
					ForbearanceS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.Forbearance.ForbearanceS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.Forbearance.ForbearanceS = value
						  end,
					},
				},
				},-- End of Forbearance #####
				
				DPlea = {
				type = "group",
				name = L["Divine Plea"],
				desc = L["Channel settings for Divine Plea"],
				args = {
					--[[DPleaR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.DPlea.DPleaR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DPlea.DPleaR = value
						  end,	  
					},
					DPleaC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.DPlea.DPleaC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DPlea.DPleaC = value
						  end,	  
					},]]-- -- Why would we want to announce Divine Plea at all except to ourselves?
					DPleaS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.DPlea.DPleaS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.DPlea.DPleaS = value
						  end,
					},
				},
				},-- End of DPlea #####

				AD = {
				type = "group",
				name = L["Ardent Defender"],
				desc = L["Channel settings for Ardent Defender"],
				args = {
					ADR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.AD.ADR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.AD.ADR = value
						  end,	  
					},
					ADC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.AD.ADC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.AD.ADC = value
						  end,	  
					},
					ADS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.AD.ADS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.AD.ADS = value
						  end,	  
					},
				},
				},-- End of Ardent Defender #####
				
				AM = {
				type = "group",
				name = L["Aura Mastery"],
				desc = L["Channel settings for Aura Mastery"],
				args = {
					AMR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.pally.AM.AMR
						  end,
					set = function (info, value)
							RSA.db.profile.pally.AM.AMR = value
						  end,	  
					},
					AMC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.pally.AM.AMC
						  end,
					set = function (info, value)
							RSA.db.profile.pally.AM.AMC = value
						  end,	  
					},
					AMS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.pally.AM.AMS
						  end,
					set = function (info, value)
							RSA.db.profile.pally.AM.AMS = value
						  end,	  
					},
				},
				},-- End of Aura Mastery #####				
			},-- End of pally group args
		},-- End of Pally section
		
		priest = {
			type = "group",
			name = "Spells",
			desc = "Announcement settings for your Spells.",
			hidden = function()
						return RSA.db.profile.class ~= "PRIEST" 
					end,
			childGroups = "select",
			order = 1,
			args = {

				GS = {
				type = "group",
				name = "Guardian Spirit",
				desc = L["Channel settings for Guardian Spirit"],
				args = {
					GSW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.priest.GS.GSW
						  end,
					set = function (info, value)
							RSA.db.profile.priest.GS.GSW = value
						  end,						
					},				
					GSR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.priest.GS.GSR
						  end,
					set = function (info, value)
							RSA.db.profile.priest.GS.GSR = value
						  end,	  
					},
					GSC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.priest.GS.GSC
						  end,
					set = function (info, value)
							RSA.db.profile.priest.GS.GSC = value
						  end,	  
					},
					GSS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.priest.GS.GSS
						  end,
					set = function (info, value)
							RSA.db.profile.priest.GS.GSS = value
						  end,	  
					},
				},
				},-- End of Guardian Spirit #####
				
				PS = {
				type = "group",
				name = "Pain Suppression",
				desc = L["Channel settings for Pain Suppression"],
				args = {
					PSW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.priest.PS.PSW
						  end,
						--get = "GetPSW",  
					set = function (info, value)
							RSA.db.profile.priest.PS.PSW = value
						  end,
						
					},				
					PSR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.priest.PS.PSR
						  end,
					set = function (info, value)
							RSA.db.profile.priest.PS.PSR = value
						  end,	  
					},
					PSC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.priest.PS.PSC
						  end,
					set = function (info, value)
							RSA.db.profile.priest.PS.PSC = value
						  end,	  
					},
					PSS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.priest.PS.PSS
						  end,
					set = function (info, value)
							RSA.db.profile.priest.PS.PSS = value
						  end,	  
					},
				},
				},-- End of Pain Suppression #####
				
				PI = {
				type = "group",
				name = "Power Infusion",
				desc = L["Channel settings for Power Infusion"],
				args = {
					PIW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.priest.PI.PIW
						  end,
					set = function (info, value)
							RSA.db.profile.priest.PI.PIW = value
						  end,
						
					},
					PIR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.priest.PI.PIR
						  end,
					set = function (info, value)
							RSA.db.profile.priest.PI.PIR = value
						  end,	  
					},
					PIC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.priest.PI.PIC
						  end,
					set = function (info, value)
							RSA.db.profile.priest.PI.PIC = value
						  end,	  
					},
					PIS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.priest.PI.PIS
						  end,
					set = function (info, value)
							RSA.db.profile.priest.PI.PIS = value
						  end,	  
					},
				},
				},-- End of Power Infusion #####
				
				FW = {
				type = "group",
				name = "Fear Ward",
				desc = L["Channel settings for Fear Ward"],
				args = {
					FWW = {
					type = "toggle",
					name = L["Whisper"],
					desc = L["Whispers the target of your spell, unless the target is yourself."],
					get = function (info) 
							return RSA.db.profile.priest.FW.FWW
						  end,
					set = function (info, value)
							RSA.db.profile.priest.FW.FWW = value
						  end,
						
					},
					FWR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.priest.FW.FWR
						  end,
					set = function (info, value)
							RSA.db.profile.priest.FW.FWR = value
						  end,	  
					},
					FWC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.priest.FW.FWC
						  end,
					set = function (info, value)
							RSA.db.profile.priest.FW.FWC = value
						  end,	  
					},
					FWS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.priest.FW.FWS
						  end,
					set = function (info, value)
							RSA.db.profile.priest.FW.FWS = value
						  end,	  
					},
				},
				},-- End of Fear Ward #####

				DH = {
				type = "group",
				name = "Divine Hymn",
				desc = L["Channel settings for Divine Hymn"],
				args = {
					DHR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.priest.DH.DHR
						  end,
					set = function (info, value)
							RSA.db.profile.priest.DH.DHR = value
						  end,	  
					},
					DHC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.priest.DH.DHC
						  end,
					set = function (info, value)
							RSA.db.profile.priest.DH.DHC = value
						  end,	  
					},
					DHS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.priest.DH.DHS
						  end,
					set = function (info, value)
							RSA.db.profile.priest.DH.DHS = value
						  end,	  
					},
				},
				},-- End of Divine Hymn #####
				
				HoH = {
				type = "group",
				name = "Hymn of Hope",
				desc = L["Channel settings for Hymn of Hope"],
				args = {
					HoHR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.priest.HoH.HoHR
						  end,
					set = function (info, value)
							RSA.db.profile.priest.HoH.HoHR = value
						  end,	  
					},
					HoHC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.priest.HoH.HoHC
						  end,
					set = function (info, value)
							RSA.db.profile.priest.HoH.HoHC = value
						  end,	  
					},
					HoHS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.priest.HoH.HoHS
						  end,
					set = function (info, value)
							RSA.db.profile.priest.HoH.HoHS = value
						  end,	  
					},
				},
				},-- End of Hymn of Hope #####			
			},--End of priest group args
		},-- End of priest section
		
		warrior = {
			type = "group",
			name = "Spells",
			desc = "Announcement settings for your spells.",
			hidden = function()
						return RSA.db.profile.class ~= "WARRIOR"
					end,
			childGroups = "select",
			order = 1,
			args = {
			
				Taunt = {
					type = "group",
					name = "Taunt",
					desc = "Settings for Taunt",
					args = {
						Taunt0 = {
							type = "header",
							name = "Channel Settings",
							order = 0,
						},
						Taunt1 = {
							type = "description",
							name = "Here you can set which channels you want to send announcements to.",
							order = 1,
						},
						TauntR = {
							type = "toggle",
							name = L["Raid/Party"],
							desc = L.raidpartydesc,
							order = 3,
							get = function(info) 
									return RSA.db.profile.warrior.Taunt.TauntR
								  end,
							set = function (info, value)
									RSA.db.profile.warrior.Taunt.TauntR = value
								  end,	  
						},
						TauntC = {
							type = "toggle",
							name = L["Custom Channel"],
							desc = L["Announces in your custom channel."],
							order = 2,
							get = function(info) 
									return RSA.db.profile.warrior.Taunt.TauntC
								  end,
							set = function (info, value)
									RSA.db.profile.warrior.Taunt.TauntC = value
								  end,	  
						},
						TauntS = {
							type = "toggle",
							name = L["Local"],
							desc = L["Sends a local message to your default chat window that only you can see."],
							order = 4,
							get = function(info) 
									return RSA.db.profile.warrior.Taunt.TauntS
								  end,
							set = function (info, value)
									RSA.db.profile.warrior.Taunt.TauntS = value
								  end,	  
						},
						Taunt5 = {
							type = "header",
							name = L["Announcement Settings"],
							order = 5,
						},
						Taunt6 = {
							type = "description",
							name = "Here you can set whether Taunt announces on successful casts, resists, or both.",
							order = 6,
						},
						Taunt7 = {
							type = "toggle",
							name = "Successful Taunts",
							desc = "Check this to announce upon successful taunt.",
							order = 7,
							get = function(info) 
									return RSA.db.profile.warrior.Taunt.TauntSuccess
								  end,
							set = function (info, value)
									RSA.db.profile.warrior.Taunt.TauntSuccess = value
								  end,	 							
						},
						Taunt8 = {
							type = "toggle",
							name = L["Resists"],
							desc = L["Check this to announce upon taunt resists."],
							order = 8,
							get = function(info) 
									return RSA.db.profile.warrior.Taunt.TauntResist
								  end,
							set = function (info, value)
									RSA.db.profile.warrior.Taunt.TauntResist = value
								  end,	 							
						},						
					},
				},-- End of Taunt #####

				SWall = {
				type = "group",
				name = "Shield Wall",
				desc = "Channel settings for Shield Wall",
				args = {
					SWallR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.warrior.SWall.SWallR
						  end,
					set = function (info, value)
							RSA.db.profile.warrior.SWall.SWallR = value
						  end,	  
					},
					SWallC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.warrior.SWall.SWallC
						  end,
					set = function (info, value)
							RSA.db.profile.warrior.SWall.SWallC = value
						  end,	  
					},
					SWallS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.warrior.SWall.SWallS
						  end,
					set = function (info, value)
							RSA.db.profile.warrior.SWall.SWallS = value
						  end,	  
					},
				},
				},-- End of Shield Wall #####
				
				LStand = {
				type = "group",
				name = "Last Stand",
				desc = "Channel settings for Last Stand",
				args = {
					LStandR = {
					type = "toggle",
					name = L["Raid/Party"],
					desc = L.raidpartydesc,
					get = function(info) 
							return RSA.db.profile.warrior.LStand.LStandR
						  end,
					set = function (info, value)
							RSA.db.profile.warrior.LStand.LStandR = value
						  end,	  
					},
					LStandC = {
					type = "toggle",
					name = L["Custom Channel"],
					desc = L["Announces in your custom channel."],
					get = function(info) 
							return RSA.db.profile.warrior.LStand.LStandC
						  end,
					set = function (info, value)
							RSA.db.profile.warrior.LStand.LStandC = value
						  end,	  
					},
					LStandS = {
					type = "toggle",
					name = L["Local"],
					desc = L["Sends a local message to your default chat window that only you can see."],
					get = function(info) 
							return RSA.db.profile.warrior.LStand.LStandS
						  end,
					set = function (info, value)
							RSA.db.profile.warrior.LStand.LStandS = value
						  end,	  
					},
				},
				},-- End of Last Stand #####
			
			},-- End of Warrior group args
		}, --End of Warrior section
		
		
		ReminderSettingsGroup = {
			type = "group",
			name = "Reminders",
			desc = "Settings for Spell Reminders.",
			--childGroups = "select",
			hidden = function()
						return RSA.db.profile.class == "WARRIOR"
					end,
			order = 20,
			args = {
				RemindHead = {
					type = "header",
					name = "Spell Reminder Settings",
					order = 60,				
				},
				RemindDesc = {
					type = "description",
					name = "Here you can set how often you want to be reminded when a spell is missing.",
					order = 61,
				},
				ReminderTime = {
					type = "range",
					name = L["Remind Time"],
					desc = L["Set how often you want to be reminded in seconds."],
					order = 62,
					width = "full",
					min = 1,
					max = 60,
					step = 0.5,									
					get = function(info) 
							return RSA.db.profile.RTime
						  end,
					set = function (info, value)
							RSA.db.profile.RTime = value
						  end,							
				},
				BlankSpace = {
					type = "description",
					name = "\n\n",
					order = 59,
				},
				CheckHead = {
					type = "header",
					name = "Update settings",
					order = 63,
				},
				CheckDesc = {
					type = "description",
					name = "Here you can set how often RSA will check for your missing buffs. Increasing the time between checks will lower CPU usage, but also lower accuracy.\n10-15 Seconds is recommended.",
					order = 64,
				},
				CheckTime = {
					type = "range",
					name = "Check Time",
					desc = "Set how often you want to check buffs in seconds.",
					order = 65,
					width = "full",
					min = 1,
					max = 60,
					step = 1,									
					get = function(info) 
							return RSA.db.profile.CTime
						  end,
					set = function (info, value)
							RSA.db.profile.CTime = value
						  end,							
				},				
				
				RF = { -- Righteous Fury Reminder Options
					type = "group",
					order = 0,
					name = "Righteous Fury",
					--desc = L["Settings for Righteous Fury"],
					inline = true,
					width = "full",
					hidden = function() 
						return RSA.db.profile.class ~= "PALADIN" 
					end,				
					args = {
						RFEnabled1 = {
							type = "header",
							name = L["Tracking"],
							order = 0,
						},
						RFEnabled2 = {
							type = "description",
							name = L["Do you want to enable Righteous Fury tracking?"],
							order = 1,
						},
						RFEnabled3 = {
							type = "toggle",
							name = L["Enabled"],
							desc = L["Check to turn on Righteous Fury detection."],
							order = 2,
							get = function(info) 
									return RSA.db.profile.pally.RF.RFOn
								  end,
							set = function (info, value)
									RSA.db.profile.pally.RF.RFOn = value
									if RSA.db.profile.pally.RF.RFOn == true then
										RSA_Rebuffer:SetScript("OnEvent", Rebuffer)
									elseif RSA.db.profile.pally.RF.RFOn == false then
										RSA_BindUpdate()
										SetBinding("MOUSEWHEELUP", RSA.db.profile.oldwheelup)
										SetBinding("MOUSEWHEELDOWN", RSA.db.profile.oldwheeldown)	
										UIParent:SetScript("OnMouseWheel", nil)
										UIParent:EnableMouseWheel(0)										
									end
								  end,								
						},
						RF0 = {
							type = "header",
							name = L["Protection Detection"],
							order = 3,							
						},
						RF1 = {
							type = "description",
							name = L["Enable this to only track Righteous Fury when you are Protection."],
							order = 4,
						},
						RF2 = {
							type = "toggle",
							name = L["Enabled"],
							desc = L["Check to turn on protection detection."],
							order = 5,
							disabled =	function ()
											return RSA.db.profile.pally.RF.RFOn ~= true
										end,
							get = function(info) 
									return RSA.db.profile.pally.RF.RFPD
								  end,
							set = function (info, value)
									RSA.db.profile.pally.RF.RFPD = value
									if (RSA.db.profile.pally.RF.RFPD == true and RSA_GetSpec(player) ~="Protection" and RSA.db.profile.pally.RF.RFOn == true) then
										RSA_BindUpdate()
										SetBinding("MOUSEWHEELUP", RSA.db.profile.oldwheelup)
										SetBinding("MOUSEWHEELDOWN", RSA.db.profile.oldwheeldown)	
										UIParent:SetScript("OnMouseWheel", nil)
										UIParent:EnableMouseWheel(0)
									elseif RSA.db.profile.pally.RF.RFPD == false and RSA.db.profile.pally.RF.RFOn == true then
										RSA_Rebuffer:SetScript("OnEvent", Rebuffer)
									end
								  end,								
						},
						RF3 = {
							type = "header",
							name = L["Binding Settings"],
							order = 6,
						},
						RF4 = {
							type = "description",
							name = L["Allow RSA to rebind your scrollwheel, this will bind your scroll wheel to Righteous Fury until cast."],
							order = 7,
						},
						RF5 = {
							type = "toggle",
							name = L["Scroll Wheel Bind"],
							desc = L.scrollwheelbind,
							order = 8,
							disabled =	function ()
											return RSA.db.profile.pally.RF.RFOn ~= true
										end,							
							get = function(info) 
									return RSA.db.profile.pally.RF.RFSW
								  end,
							set = function (info, value)
									RSA.db.profile.pally.RF.RFSW = value
									if RSA.db.profile.pally.RF.RFSW == false then
										RSA_BindUpdate()
										SetBinding("MOUSEWHEELUP", RSA.db.profile.oldwheelup)
										SetBinding("MOUSEWHEELDOWN", RSA.db.profile.oldwheeldown)	
										UIParent:SetScript("OnMouseWheel", nil)
										UIParent:EnableMouseWheel(0)
									elseif RSA.db.profile.pally.RF.RFSW == true then
										RSA_Rebuffer:SetScript("OnEvent", Rebuffer)
									end
								  end,								
						},
						RF6 = {
							type = "header",
							name = "Reminder",
							order = 9,
						},
						RF7 = {
							type = "description",
							name = "Enable or Disable the spell reminder. You can set how often the reminder is sent below.",
							order = 10,
						},						
						RF8 = {
							type = "toggle",
							name = L["Enabled"],
							desc = L["Enable or Disable the spell reminder. This is the message you recieve whenever Righteous Fury is not up."],
							order = 11,
							disabled =	function ()
											return RSA.db.profile.pally.RF.RFOn ~= true
										end,							
							get = function(info) 
									return RSA.db.profile.pally.RF.RFR
								  end,
							set = function (info, value)
									RSA.db.profile.pally.RF.RFR = value
								  end,								
							},
					},
				},-- End of Righteous Fury #####
				
				IF = { -- Inner Fire Reminder Options
					type = "group",
					order = 0,
					name = "Inner Fire",
					--desc = L["Settings for Inner Fire"],
					inline = true,
					width = "full",
					hidden = function() 
						return RSA.db.profile.class ~= "PRIEST" 
					end,				
					args = {
						IFEnabled1 = {
							type = "header",
							name = L["Tracking"],
							order = 0,
						},
						IFEnabled2 = {
							type = "description",
							name = "Do you want to enable Inner Fire tracking?",
							order = 1,
						},
						IFEnabled3 = {
							type = "toggle",
							name = L["Enabled"],
							desc = "Check to turn on Inner Fire detection.",
							order = 2,
							get = function(info) 
									return RSA.db.profile.priest.IF.IFOn
								  end,
							set = function (info, value)
									RSA.db.profile.priest.IF.IFOn = value
									if RSA.db.profile.priest.IF.IFOn == true then
										RSA_Rebuffer:SetScript("OnEvent", Rebuffer)
									elseif RSA.db.profile.priest.IF.IFOn == false then
										RSA_BindUpdate()
										SetBinding("MOUSEWHEELUP", RSA.db.profile.oldwheelup)
										SetBinding("MOUSEWHEELDOWN", RSA.db.profile.oldwheeldown)	
										UIParent:SetScript("OnMouseWheel", nil)
										UIParent:EnableMouseWheel(0)										
									end
								  end,								
						},
						IF3 = {
							type = "header",
							name = L["Binding Settings"],
							order = 6,
						},
						IF4 = {
							type = "description",
							name = "Allow RSA to rebind your scrollwheel, this will bind your scroll wheel to Inner Fire until cast.",
							order = 7,
						},
						IF5 = {
							type = "toggle",
							name = L["Scroll Wheel Bind"],
							desc = L.scrollwheelbind,
							order = 8,
							disabled =	function ()
											return RSA.db.profile.priest.IF.IFOn ~= true
										end,							
							get = function(info) 
									return RSA.db.profile.priest.IF.IFSW
								  end,
							set = function (info, value)
									RSA.db.profile.priest.IF.IFSW = value
									if RSA.db.profile.priest.IF.IFSW == false then
										RSA_BindUpdate()
										SetBinding("MOUSEWHEELUP", RSA.db.profile.oldwheelup)
										SetBinding("MOUSEWHEELDOWN", RSA.db.profile.oldwheeldown)	
										UIParent:SetScript("OnMouseWheel", nil)
										UIParent:EnableMouseWheel(0)
									elseif RSA.db.profile.priest.IF.IFSW == true then
										RSA_Rebuffer:SetScript("OnEvent", Rebuffer)
									end
								  end,								
						},
						IF6 = {
							type = "header",
							name = "Reminder",
							order = 9,
						},
						IF7 = {
							type = "description",
							name = "Enable or Disable the spell reminder. You can set how often the reminder is sent below.",
							order = 10,
						},						
						IF8 = {
							type = "toggle",
							name = L["Enabled"],
							desc = "Enable or Disable the spell reminder. This is the message you recieve whenever Inner Fire is not up.",
							order = 11,
							disabled =	function ()
											return RSA.db.profile.priest.IF.IFOn ~= true
										end,							
							get = function(info) 
									return RSA.db.profile.priest.IF.IFR
								  end,
							set = function (info, value)
									RSA.db.profile.priest.IF.IFR = value
								  end,								
							},
					},
				},-- End of Inner Fire #####				
				
			},
		},-- End of Reminder Options		
		
		
    },-- End of main options args	
}-- End of Options

-----------------------
--[[ Ace functions ]]--
-----------------------
function RSA:OnInitialize()
    -- Called when the addon is loaded
    self.db = LibStub("AceDB-3.0"):New("RSADB", defaults) -- Setup Saved Variables
	LibStub("AceConfig-3.0"):RegisterOptionsTable("RSA", options) -- Register Options
    self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("RSA", "RSA") -- Add options to Blizzard interface
    self:RegisterChatCommand("RSA", "ChatCommand") -- Register /rsa command
	options.args.profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db) -- Add profile management to Blizzard interface
	
	-- We might have to use this if the addon has substantial changes.
	--[[if self.db.global.version < RSA_Version then
		Print_Self("Addon has been updated. Resetting default data.");
		self.db.global.version = RSA_Version]]--
		-- Then do the stuff to reset values.
	--end
	
	-- Check what class we are and save it. Used to determine what options to show.
	local pClass = select(2, UnitClass("player"))	
	self.db.profile.class = pClass
	self.db.RegisterCallback(self, "OnProfileChanged", "RefreshConfig")
	self.db.RegisterCallback(self, "OnProfileCopied", "RefreshConfig")
	self.db.RegisterCallback(self, "OnProfileReset", "RefreshConfig")

end

function RSA:RefreshConfig()
	local pClass = select(2, UnitClass("player"))	
		self.db.profile.class = pClass
end

function RSA:OnProfileEnable()
	self.db = self.db.profile
	local pClass = select(2, UnitClass("player"))	
		self.db.profile.class = pClass
end

function RSA:ChatCommand(input)
--   if not input or input:trim() == "" then
        InterfaceOptionsFrame_OpenToCategory(self.optionsFrame)
--    else
--        LibStub("AceConfigCmd-3.0").HandleCommand(RSA, "RSA", input)
--    end
end

--------------------------------
--[[ Announcement Functions ]]--
--------------------------------
local function Print_Self(message) -- Sends a message to your default chat window.
	_G.ChatFrame1:AddMessage("|cFFFF75B3RSA:|r " .. format(message, spellinfo))
end

local function Print_Raid(message) -- Sends a message to the raid if you're in a raid, or a party if you're in a party or nothing if you aren't in either.
	if GetNumRaidMembers()>0 then
		SendChatMessage(format(message, spellinfo), "RAID", nil)
	elseif GetNumPartyMembers()>0 then
		SendChatMessage(format(message, spellinfo), "PARTY", nil)
end
end

local function Print_Channel(message) -- Sends a message to the custom channel that the user defines.
	if GetNumRaidMembers()>0 or GetNumPartyMembers()>0 then
		SendChatMessage(format(message, spellinfo), "CHANNEL", nil, GetChannelName(RSA.db.profile.channel))
	end
end

local function Print_Self_RW(message) -- Sends a message locally to the raid warning frame. Only visible to the user.
	local RWColor = {r=1, g=1, b=1}
	RaidNotice_AddMessage(RaidWarningFrame, "|cFFFF75B3RSA:|r " .. format(message, spellinfo), RWColor)
end

local function Print_RW(message) -- Sends a proper message to the raid warning frame. 
	SendChatMessage(format(message, spellinfo), "RAID_WARNING", nil)
end

local function Print_Whisper(message, target) -- Sends a whisper to the target.
	SendChatMessage(format(message, spellinfo), "WHISPER", nil, target)
end

----------------------------
--[[ Reminder Functions ]]--
----------------------------
function RSA_BindUpdate() -- Check what our old key binds were for Mouse Wheel Up and Down. We must do this before rebinding.
	local i;
	for i = 1, GetNumBindings(), 1 do
		local command, key1 = GetBinding(i);
		if (key1 == "MOUSEWHEELUP" and command ~= "SPELL") then
			RSA.db.profile.oldwheelup = command
			--print("Old Wheel Up: " .. command)
		elseif (key1 == "MOUSEWHEELDOWN" and command ~= "SPELL") then
			RSA.db.profile.oldwheeldown = command
			--print("Old Wheel Down: " .. command)
		end
	end
end

--[[ Detect what talent spec we are ]]--
	local numTalents, maxTemp, returnIndex
	local tt = {}
function RSA_GetSpec() -- GetPrimarySpecInfo(player) gets player's talent spec.
	numTalents = 0
	tt = wipe(tt)
	local name, iconTexture, pointsSpent, background
	local level = UnitLevel("player");
	local hasSpec = not ( level > 0 and level < 10 )
	for i = 1, GetNumTalentTabs() do
	  local name, iconTexture, pointsSpent, background = GetTalentTabInfo(i)
	  if numTalents <= pointsSpent then
		 numTalents = pointsSpent
		 tt[i] = {name, iconTexture, pointsSpent, background}
		 returnIndex = i
	  end
	end
	-- returns name, iconTexture, pointsSpent, background
	if hasSpec and GetNumTalentTabs() > 0 then
	  return tt[returnIndex][1] -- Spec Name
	  --, tempTable[returnIndex][2], tempTable[returnIndex][3], tempTable[returnIndex][4]
	end
end

--[[ Update Scroll Wheel Bind ]]--
local function Rebinder()
	if (RSA.db.profile.pally.RF.RFSW == true and RSA.db.profile.pally.RF.RFOn and RSA.db.profile.class == "PALADIN")
	or (RSA.db.profile.priest.IF.IFSW == true and RSA.db.profile.priest.IF.IFOn and RSA.db.profile.class == "PRIEST") == true then
		if InCombatLockdown() or UnitIsDeadOrGhost("player") then 
			UIParent:SetScript("OnMouseWheel", nil) -- This is probably bad, but there appear to be no side effects thus far.
			UIParent:EnableMouseWheel(0)		
		return end	
			SetBindingSpell("MOUSEWHEELUP", ReminderSpell)
			SetBindingSpell("MOUSEWHEELDOWN", ReminderSpell)
			UIParent:SetScript("OnMouseWheel", nil)
			UIParent:EnableMouseWheel(0)
	end
end

--[[ Spell Reminder ]]--
local ReminderTimeElapsed = 0.0
RSA_Reminder = CreateFrame("Frame", "RSA_Reminder")
local function Reminder(self, elapsed)
	if InCombatLockdown() then return end -- If we're in combat, do nothing.
	ReminderTimeElapsed = ReminderTimeElapsed + elapsed
	if ReminderTimeElapsed < RSA.db.profile.RTime then return end
	ReminderTimeElapsed = ReminderTimeElapsed - floor(ReminderTimeElapsed)
	local name, rank, icon, count, debuffType, duration, expirationTime, source, isStealable = UnitBuff("player", ReminderSpell)
	if name == ReminderSpell then return end -- If the spell we're reminding about is up, stop reminding straight away.
	if UnitIsDeadOrGhost("player") == 1 then return	end
	if (RSA.db.profile.pally.RF.RFR == true and RSA.db.profile.pally.RF.RFOn == true and RSA.db.profile.class == "PALADIN" and UnitIsDeadOrGhost("player") ~= 1)
	or (RSA.db.profile.priest.IF.IFR == true and RSA.db.profile.class == "PRIEST" and UnitIsDeadOrGhost("player") ~= 1) then
		Print_Self(ReminderSpell .. " is missing!")
	end
end

--[[ Main Buff Checking Function ]]--
RSA_Rebuffer = CreateFrame("Frame", "RSA_Rebuffer")
local function Rebuffer(arg1, event, target)
	if InCombatLockdown() or UnitIsDeadOrGhost("player") then return end
	if RSA.db.profile.class == "PALADIN" then -- Check we're a Paladin.
		if RSA.db.profile.pally.RF.RFOn == true then -- Start Righteous Fury
			if (RSA.db.profile.pally.RF.RFPD == true and RSA_GetSpec(player) == "Protection") or (RSA.db.profile.pally.RF.RFPD == false) then
			local name, rank, icon, count, debuffType, duration, expirationTime, source, isStealable = UnitBuff("player", "Righteous Fury")
				if name == "Righteous Fury" then -- Righteous Fury is up.
					RSA_Reminder:SetScript("OnUpdate", nil)
				elseif name ~= "Righteous Fury" then	
					ReminderSpell = "Righteous Fury"
					if RSA.db.profile.pally.RF.RFR == true and not UnitIsDeadOrGhost("player") then
						RSA_Reminder:SetScript("OnUpdate", Reminder)
					end						
					if RSA.db.profile.pally.RF.RFSW == true and not UnitIsDeadOrGhost("player") then
						RSA_BindUpdate()
						UIParent:SetScript("OnMouseWheel", Rebinder)
						UIParent:EnableMouseWheel(1)
					end
				end
			elseif RSA.db.profile.pally.RF.RFPD == true and RSA_GetSpec(player) ~= "Protection" then
				RSA_Reminder:SetScript("OnUpdate", nil)
			end
		end
	end -- End Paladin Section
	
	if RSA.db.profile.class == "PRIEST" then -- Check we're a Priest.
		if RSA.db.profile.priest.IF.IFOn == true then -- Start Inner Fire
			local name, rank, icon, count, debuffType, duration, expirationTime, source, isStealable = UnitBuff("player", "Inner Fire")
			if name == "Inner Fire" then -- Inner Fire is up.
				RSA_Reminder:SetScript("OnUpdate", nil)
			elseif name ~= "Inner Fire" then	
				ReminderSpell = "Inner Fire"
				if RSA.db.profile.priest.IF.IFR == true and not UnitIsDeadOrGhost("player") then
					RSA_Reminder:SetScript("OnUpdate", Reminder)
				end						
				if RSA.db.profile.priest.IF.IFSW == true and not UnitIsDeadOrGhost("player") then
					RSA_BindUpdate()
					UIParent:SetScript("OnMouseWheel", Rebinder)
					UIParent:EnableMouseWheel(1)
				end
			end
		end
	end -- End Priest Section	
	
	RSA_Rebuffer:SetScript("OnEvent", nil)	
end
RSA_Rebuffer:RegisterEvent("UNIT_AURA")
RSA_Rebuffer:RegisterEvent("PLAYER_REGEN_ENABLED")
RSA_Rebuffer:RegisterEvent("ADDON_LOADED")
RSA_Rebuffer:RegisterEvent("UNIT_MANA")
RSA_Rebuffer:RegisterEvent("PLAYER_TALENT_UPDATE")

--[[ Buff Check Delay ]]--
RSA_BuffMonitor = CreateFrame("Frame", "RSA_BuffMonitor")
local MonitorTimeElapsed = 0.0
local function Monitor(self, elapsed)
	if InCombatLockdown() then return end -- If we're in combat, do nothing.
	MonitorTimeElapsed = MonitorTimeElapsed + elapsed
	if MonitorTimeElapsed < RSA.db.profile.CTime then return end -- If we haven't waited at least <Config Amount> seconds, then do nothing.
	MonitorTimeElapsed = MonitorTimeElapsed - floor(MonitorTimeElapsed)
	RSA_Rebuffer:SetScript("OnEvent", Rebuffer) -- Start the buff checker.
end--End Monitor Function
RSA_BuffMonitor:SetScript("OnUpdate", Monitor)

----------------------------------------------------------
--[[ The event checking section. Look for our spells! ]]--
----------------------------------------------------------
local spells = CreateFrame("Frame", "RSA_SPELLS")
spells:SetScript("OnEvent", function(_, _, _, event, _, source, _, _, dest, _, spellID, _, _, missType)
	if source == pName then	
		if event == "SPELL_CAST_SUCCESS" then -- Start event check for spells that have actually been cast.
		
			--[[ Start Paladin Spells ]]--
			if RSA.db.profile.class == "PALADIN" then -- Check we're a Paladin
			
				if RSA.db.profile.pally.RF.RFOn == true then -- Righteous Fury auto recast and announce.
					if spellID == 25780 then
						--RSA_Reminder:SetScript("OnUpdate", nil)
						RSA_Rebuffer:SetScript("OnEvent", Rebuffer) -- This is the important line now.
						Print_Self("Righteous Fury has been refreshed.")
						if RSA.db.profile.pally.RF.RFSW == true then
							SetBinding("MOUSEWHEELUP", RSA.db.profile.oldwheelup)
							SetBinding("MOUSEWHEELDOWN", RSA.db.profile.oldwheeldown)
							UIParent:SetScript("OnMouseWheel", nil)
							UIParent:EnableMouseWheel(0)				
						end
					end
				end
				
				if spellID == 498 then -- Divine Protection
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.DP.DPR == true then
						Print_Raid("%s activated!")
					end
					if RSA.db.profile.pally.DP.DPC == true then
						Print_Channel("%s activated!")
					end
					if RSA.db.profile.pally.DP.DPS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s activated!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s activated!")
						end
					end
				end
				
				if spellID == 1022 or spellID == 5599 or spellID == 10278 then -- Hand of Protection
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.HoP.HoPR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoP.HoPC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoP.HoPS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.pally.HoP.HoPW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end
				
				if spellID == 6940 then -- Hand of Sacrifice
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.HoSac.HoSacR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoSac.HoSacC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoSac.HoSacS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.pally.HoSac.HoSacW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end
				
				if spellID == 1038 then -- Hand of Salvation
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.HoSalv.HoSalvR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoSalv.HoSalvC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoSalv.HoSalvS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.pally.HoSalv.HoSalvW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end
				
				if spellID == 1044 then -- Hand of Freedom
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.HoF.HoFR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoF.HoFC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoF.HoFS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.pally.HoF.HoFW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end
				
				if spellID == 19752 then -- Divine Intervention
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.DI.DIR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.DI.DIC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.DI.DIS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.pally.DI.DIW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end
				
				if spellID == 31821 then -- Aura Mastery	
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.AM.AMR == true then
						Print_Raid("%s activated!")
					end
					if RSA.db.profile.pally.AM.AMC == true then
						Print_Channel("%s activated!")
					end
					if RSA.db.profile.pally.AM.AMS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s activated!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s activated!")
						end
					end
				end
				
				if spellID == 48788 or spellID == 27154 or spellID == 10310 or spellID == 2800 or spellID == 633  then -- Lay on Hands
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.LoH.LoHR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.LoH.LoHC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.pally.LoH.LoHS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.pally.LoH.LoHW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end
				
			end
			
			
			--[[ Start Priest Spells ]]--
			if RSA.db.profile.class == "PRIEST" then -- Check we're a Priest
			
				if RSA.db.profile.priest.IF.IFOn == true then -- Inner Fire auto recast and announce.
					if spellID == 48168 or spellID == 48040 or spellID == 25431 or spellID == 10952 or spellID == 10951 or spellID == 1006 or spellID == 602 or spellID == 7128 or spellID == 588 then
						--RSA_Reminder:SetScript("OnUpdate", nil)
						RSA_Rebuffer:SetScript("OnEvent", Rebuffer) -- This is the important line now.
						Print_Self("Inner Fire has been refreshed.")
						if RSA.db.profile.priest.IF.IFSW == true then
							SetBinding("MOUSEWHEELUP", RSA.db.profile.oldwheelup)
							SetBinding("MOUSEWHEELDOWN", RSA.db.profile.oldwheeldown)
							UIParent:SetScript("OnMouseWheel", nil)
							UIParent:EnableMouseWheel(0)				
						end
					end
				end
			
				if spellID == 10060 then -- Power Infusion
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.PI.PIR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.priest.PI.PIC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.priest.PI.PIS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.priest.PI.PIW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end			
			
				if spellID == 6346 then -- Fear Ward
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.FW.FWR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.priest.FW.FWC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.priest.FW.FWS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.priest.FW.FWW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end
				
				if spellID == 64843 then -- Divine Hymn
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.DH.DHR == true then
						Print_Raid("%s activated!")
					end
					if RSA.db.profile.priest.DH.DHC == true then
						Print_Channel("%s activated!")
					end
					if RSA.db.profile.priest.DH.DHS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s activated!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s activated!")
						end
					end
				end
				
				if spellID == 64901 then -- Hymn of Hope
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.HoH.HoHR == true then
						Print_Raid("%s activated!")
					end
					if RSA.db.profile.priest.HoH.HoHC == true then
						Print_Channel("%s activated!")
					end
					if RSA.db.profile.priest.HoH.HoHS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s activated!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s activated!")
						end
					end
				end
				
			end
			
			
			--[[ Start Warrior Spells ]]--
			if RSA.db.profile.class == "WARRIOR" then -- Check we're a Warrior
			
				if spellID == 871 then -- Shield Wall
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.warrior.SWall.SWallR == true then
						Print_Raid("%s activated!")
					end
					if RSA.db.profile.warrior.SWall.SWallC == true then
						Print_Channel("%s activated!")
					end
					if RSA.db.profile.warrior.SWall.SWallS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s activated!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s activated!")
						end
					end
				end
				
				if spellID == 12975 then -- Last Stand
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.warrior.LStand.LStandR == true then
						Print_Raid("%s activated!")
					end
					if RSA.db.profile.warrior.LStand.LStandC == true then
						Print_Channel("%s activated!")
					end
					if RSA.db.profile.warrior.LStand.LStandS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s activated!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s activated!")
						end
					end
				end				
				
			end
			

		elseif event == "SPELL_AURA_REMOVED" then -- Start event check for spells that are ending. We don't want to monitor every spell ending though.
		
			--[[ Start Paladin Spells ]]--
			if RSA.db.profile.class == "PALADIN" then -- Check we're a Paladin
			
				if RSA.db.profile.pally.DSac.Merge == true and dest == pName then
					if spellID == 70940 then -- Divine Guardian Merged
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.DSac.DSacRM == true then
							Print_Raid("%s has ended!")
						end
						if RSA.db.profile.pally.DSac.DSacCM == true then
							Print_Channel("%s has ended!")
						end
						if RSA.db.profile.pally.DSac.DSacSM == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self("%s has ended!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW("%s has ended!")
							end
						end
					end
					if spellID == 64205 then -- Divine Sacrifice Merged
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.DSac.DSacRM == true then
							Print_Raid("%s has ended!")
						end
						if RSA.db.profile.pally.DSac.DSacCM == true then
							Print_Channel("%s has ended!")
						end
						if RSA.db.profile.pally.DSac.DSacSM == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self("%s has ended!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW("%s has ended!")
							end
						end
					end					
				elseif RSA.db.profile.pally.DSac.Merge == false and dest == pName  then
					if spellID == 64205 then -- Divine Sacrifice
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.DSac.DSacR1 == true then
							Print_Raid("%s has ended!")
						end
						if RSA.db.profile.pally.DSac.DSacC1 == true then
							Print_Channel("%s has ended!")
						end
						if RSA.db.profile.pally.DSac.DSacS1 == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self("%s has ended!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW("%s has ended!")
							end
						end
					end
					if spellID == 70940 then -- Divine Guardian
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.DSac.DSacR2 == true then
							Print_Raid("%s has ended!")
						end
						if RSA.db.profile.pally.DSac.DSacC2 == true then
							Print_Channel("%s has ended!")
						end
						if RSA.db.profile.pally.DSac.DSacS2 == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self("%s has ended!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW("%s has ended!")
							end
						end
					end					
				end				
				
				if spellID == 498 and dest == pName then -- Divine Protection
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.DP.DPR == true then
						Print_Raid("%s has ended!")
					end
					if RSA.db.profile.pally.DP.DPC == true then
						Print_Channel("%s has ended!")
					end
					if RSA.db.profile.pally.DP.DPS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end		
				end
				
				if spellID == 25771 and dest == pName then -- Forbearance
					spellinfo = GetSpellInfo(spellID)
					--[[if RSA.db.profile.pally.Forbearance.ForbearanceR == true then
						Print_Raid("%s has ended!")
					end
					if RSA.db.profile.pally.Forbearance.ForbearanceC == true then
						Print_Channel("%s has ended!")
					end]]-- -- We don't need to run this check as the options for announcing in these places has been disabled.
					if RSA.db.profile.pally.Forbearance.ForbearanceS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end		
				end
				
				if spellID == 54428 and dest == pName then -- Divine Plea
					spellinfo = GetSpellInfo(spellID)
					--[[if RSA.db.profile.pally.DPlea.DPleaR == true then
						Print_Raid("%s has ended!")
					end
					if RSA.db.profile.pally.DPlea.DPleaC == true then
						Print_Channel("%s has ended!")
					end]]-- -- We don't need to run this check as the options for announcing in these places has been disabled.
					if RSA.db.profile.pally.DPlea.DPleaS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end		
				end

				if spellID == 66233 and dest == pName then -- Ardent Defender
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.AD.ADR == true then
						Print_Raid("%s is active again!")
					end
					if RSA.db.profile.pally.AD.ADC == true then
						Print_Channel("%s is active again!")
					end
					if RSA.db.profile.pally.AD.ADS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s is active again!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s is active again!")
						end
					end		
				end
				
				if spellID == 31821 and dest == pName then -- Aura Mastery
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.AM.AMR == true then
						Print_Raid("%s has ended!")
					end
					if RSA.db.profile.pally.AM.AMC == true then
						Print_Channel("%s has ended!")
					end
					if RSA.db.profile.pally.AM.AMS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end		
				end
			
			end
			
			
			--[[ Start Priest Spells ]]--
			if RSA.db.profile.class == "PRIEST" then -- Check we're a Priest
			
				if spellID == 47788 then -- Guardian Spirit
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.GS.GSR == true then
						Print_Raid("%s on " .. dest .. " has ended!")
					end
					if RSA.db.profile.priest.GS.GSC == true then
						Print_Channel("%s on " .. dest .. " has ended!")
					end
					if RSA.db.profile.priest.GS.GSS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end
				end

				if spellID == 33206 then -- Pain Suppression
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.PS.PSR == true then
						Print_Raid("%s on " .. dest .. " has ended!")
					end
					if RSA.db.profile.priest.PS.PSC == true then
						Print_Channel("%s on " .. dest .. " has ended!")
					end
					if RSA.db.profile.priest.PS.PSS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. " has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. " has ended!")
						end
					end			
				end

				if spellID == 64843 then -- Divine Hymn
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.DH.DHR == true then
						Print_Raid("%s has ended!")
					end
					if RSA.db.profile.priest.DH.DHC == true then
						Print_Channel("%s has ended!")
					end
					if RSA.db.profile.priest.DH.DHS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end	
				end

				if spellID == 64901 then -- Hymn of Hope
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.HoH.HoHR == true then
						Print_Raid("%s has ended!")
					end
					if RSA.db.profile.priest.HoH.HoHC == true then
						Print_Channel("%s has ended!")
					end
					if RSA.db.profile.priest.HoH.HoHS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end				
				end				
			
			end
			
			
			--[[ Start Warrior Spells ]]--
			if RSA.db.profile.class == "WARRIOR" then -- Check we're a Warrior
			
				if spellID == 871 and dest == pName then -- Shield Wall
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.warrior.SWall.SWallR == true then
						Print_Raid("%s has ended!")
					end
					if RSA.db.profile.warrior.SWall.SWallC == true then
						Print_Channel("%s has ended!")
					end
					if RSA.db.profile.warrior.SWall.SWallS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end		
				end
				
				if spellID == 12976 and dest == pName then -- Last Stand
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.warrior.LStand.LStandR == true then
						Print_Raid("%s has ended!")
					end
					if RSA.db.profile.warrior.LStand.LStandC == true then
						Print_Channel("%s has ended!")
					end
					if RSA.db.profile.warrior.LStand.LStandS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has ended!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has ended!")
						end
					end		
				end				
				
			end
			

		elseif event == "SPELL_AURA_APPLIED" then -- Start check for procs that aren't cast but apply auras, such as Ardent Defender.
		
			--[[ Start Paladin Spells ]]--
			if RSA.db.profile.class == "PALADIN" then -- Check we're a Paladin
			
				if spellID == 66233 and dest == pName then -- Ardent Defender
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.AD.ADR == true then
						Print_Raid("%s has just saved my life!")
					end
					if RSA.db.profile.pally.AD.ADC == true then
						Print_Channel("%s has just saved my life!")
					end
					if RSA.db.profile.pally.AD.ADS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s has just saved my life!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s has just saved my life!")
						end
					end		
				end

				if spellID == 31790 and dest ~= pName and RSA.db.profile.pally.RD.RDSuccess == true then -- Righteous Defense
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.RD.RDR == true then
						Print_Raid("Taunted " .. dest .. "!")
					end
					if RSA.db.profile.pally.RD.RDC == true then
						Print_Channel("Taunted " .. dest .. "!")
					end
					if RSA.db.profile.pally.RD.RDS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("Taunted " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("Taunted " .. dest .. "!")
						end
					end		
				end

				if spellID == 62124 and dest ~= pName and RSA.db.profile.pally.HoR.HoRSuccess == true then -- Hand of Reckoning
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.pally.HoR.HoRR == true then
						Print_Raid("Taunted " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoR.HoRC == true then
						Print_Channel("Taunted " .. dest .. "!")
					end
					if RSA.db.profile.pally.HoR.HoRS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("Taunted " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("Taunted " .. dest .. "!")
						end
					end		
				end
				
				if RSA.db.profile.pally.DSac.Merge == true and dest == pName then
					if spellID == 70940 then -- Divine Guardian & Sacrifice Merged
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.DSac.DSacRM == true then
							Print_Raid("Divine Sacrifice and Guardian activated!")
						end
						if RSA.db.profile.pally.DSac.DSacCM == true then
							Print_Channel("Divine Sacrifice and Guardian activated!")
						end
						if RSA.db.profile.pally.DSac.DSacSM == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self("Divine Sacrifice and Guardian activated!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW("Divine Sacrifice and Guardian activated!")
							end
						end
					end
				elseif RSA.db.profile.pally.DSac.Merge == false and dest == pName  then
					if spellID == 64205 then -- Divine Sacrifice
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.DSac.DSacR1 == true then
							Print_Raid("%s activated!")
						end
						if RSA.db.profile.pally.DSac.DSacC1 == true then
							Print_Channel("%s activated!")
						end
						if RSA.db.profile.pally.DSac.DSacS1 == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self("%s activated!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW("%s activated!")
							end
						end
					end
					if spellID == 70940 then -- Divine Guardian
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.DSac.DSacR2 == true then
							Print_Raid("%s activated!")
						end
						if RSA.db.profile.pally.DSac.DSacC2 == true then
							Print_Channel("%s activated!")
						end
						if RSA.db.profile.pally.DSac.DSacS2 == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self("%s activated!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW("%s activated!")
							end
						end
					end					
				end			
			end
			
			
			--[[ Start Priest Spells ]]--
			if RSA.db.profile.class == "PRIEST" then -- Check we're a Priest
			
				if spellID == 47788 then -- Guardian Spirit
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.GS.GSR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.priest.GS.GSC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.priest.GS.GSS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.priest.GS.GSW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end

				if spellID == 33206 then -- Pain Suppression
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.priest.PS.PSR == true then
						Print_Raid("%s on " .. dest .. "!")
					end
					if RSA.db.profile.priest.PS.PSC == true then
						Print_Channel("%s on " .. dest .. "!")
					end
					if RSA.db.profile.priest.PS.PSS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("%s on " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("%s on " .. dest .. "!")
						end
					end
					if RSA.db.profile.priest.PS.PSW == true and dest ~= pName then
						Print_Whisper("%s on You!", dest)
					end	
				end
			
			end	

			
			--[[ Start Warrior Spells ]]--
			if RSA.db.profile.class == "WARRIOR" then -- Check we're a Warrior
			
				if spellID == 355 and dest ~= pName and RSA.db.profile.warrior.Taunt.TauntSuccess == true then -- Taunt
					spellinfo = GetSpellInfo(spellID)
					if RSA.db.profile.warrior.Taunt.TauntR == true then
						Print_Raid("Taunted " .. dest .. "!")
					end
					if RSA.db.profile.warrior.Taunt.TauntC == true then
						Print_Channel("Taunted " .. dest .. "!")
					end
					if RSA.db.profile.warrior.Taunt.TauntS == true then
						if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
							Print_Self("Taunted " .. dest .. "!")
						end
						if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
							Print_Self_RW("Taunted " .. dest .. "!")
						end
					end		
				end
			
			end
			
			
		elseif event == "SPELL_MISSED" then -- We only need this for things like taunt resists.
			if missType ~= "IMMUNE" then	
				if RSA.db.profile.class == "PALADIN" then -- Check we're a Paladin
				
					if spellID == 31790 and dest ~= pName and RSA.db.profile.pally.RD.RDResist == true then -- Righteous Defense
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.RD.RDR == true then
							Print_Raid(dest .. " has resisted my taunt!")
						end
						if RSA.db.profile.pally.RD.RDC == true then
							Print_Channel(dest .. " has resisted my taunt!")
						end
						if RSA.db.profile.pally.RD.RDS == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self(dest .. " has resisted my taunt!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW(dest .. " has resisted my taunt!")
							end
						end		
					end
					
					if spellID == 62124 and dest ~= pName and RSA.db.profile.pally.HoR.HoRResist == true then -- Hand of Reckoning
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.HoR.HoRR == true then
							Print_Raid(dest .. " has resisted my taunt!")
						end
						if RSA.db.profile.pally.HoR.HoRC == true then
							Print_Channel(dest .. " has resisted my taunt!")
						end
						if RSA.db.profile.pally.HoR.HoRS == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self(dest .. " has resisted my taunt!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW(dest .. " has resisted my taunt!")
							end
						end		
					end
				
				end
				
				
				if RSA.db.profile.class == "WARRIOR" then -- Check we're a Warrior
				
					if spellID == 355 and dest ~= pName and RSA.db.profile.warrior.Taunt.TauntResist == true then -- Taunt
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.warrior.Taunt.TauntR == true then
							Print_Raid(dest .. " has resisted my taunt!")
						end
						if RSA.db.profile.warrior.Taunt.TauntC == true then
							Print_Channel(dest .. " has resisted my taunt!")
						end
						if RSA.db.profile.warrior.Taunt.TauntS == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self(dest .. " has resisted my taunt!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW(dest .. " has resisted my taunt!")
							end
						end		
					end				
				
				end
				

			elseif missType ==	"IMMUNE" then
				if RSA.db.profile.class == "PALADIN" then -- Check we're a Paladin
				
					if spellID == 31790 and dest ~= pName and RSA.db.profile.pally.RD.RDResist == true then -- Righteous Defense
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.RD.RDR == true then
							Print_Raid(dest .. " is immune to my taunt!")
						end
						if RSA.db.profile.pally.RD.RDC == true then
							Print_Channel(dest .. " is immune to my taunt!")
						end
						if RSA.db.profile.pally.RD.RDS == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self(dest .. " is immune to my taunt!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW(dest .. " is immune to my taunt!")
							end
						end		
					end
					
					if spellID == 62124 and dest ~= pName and RSA.db.profile.pally.HoR.HoRResist == true then -- Hand of Reckoning
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.pally.HoR.HoRR == true then
							Print_Raid(dest .. " is immune to my taunt!")
						end
						if RSA.db.profile.pally.HoR.HoRC == true then
							Print_Channel(dest .. " is immune to my taunt!")
						end
						if RSA.db.profile.pally.HoR.HoRS == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self(dest .. " is immune to my taunt!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW(dest .. " is immune to my taunt!")
							end
						end
					end
				
				end
				
				
				if RSA.db.profile.class == "WARRIOR" then -- Check we're a Warrior
				
					if spellID == 355 and dest ~= pName and RSA.db.profile.warrior.Taunt.TauntResist == true then -- Taunt
						spellinfo = GetSpellInfo(spellID)
						if RSA.db.profile.warrior.Taunt.TauntR == true then
							Print_Raid(dest .. " is immune to my taunt!")
						end
						if RSA.db.profile.warrior.Taunt.TauntC == true then
							Print_Channel(dest .. " is immune to my taunt!")
						end
						if RSA.db.profile.warrior.Taunt.TauntS == true then
							if RSA.db.profile.raid_warn == 1 or RSA.db.profile.raid_warn == 3 then
								Print_Self(dest .. " is immune to my taunt!")
							end
							if RSA.db.profile.raid_warn == 2 or RSA.db.profile.raid_warn == 3 then
								Print_Self_RW(dest .. " is immune to my taunt!")
							end
						end
					end				
				
				end
				
				
			end		
		end -- End of the If checking which event is fired.		
	end -- Ends if source is player.
end) -- Final end for the OnEvent function.
spells:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")