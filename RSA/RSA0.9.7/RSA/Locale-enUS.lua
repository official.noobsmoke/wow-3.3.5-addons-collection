local L = LibStub("AceLocale-3.0"):NewLocale("RSA", "enUS", true)


L["Raeli's Spell Announcer"] = true


-- General Options
L["Configuration settings for Raeli's Spell Announcer."] = true
L["General Settings"] = true
L["Custom Channel Settings"] = true
L["Here you can set your own custom channel. This allows you to announce spells in a channel of your choice."] = true
L["Channel Name"] = true
L["[Channel Name]"] = true
L["Local Message Options"] = true
L.raid_warn2 = "Here you can choose if messages sent to your local chat window are also sent to the raid warning frame. To clarify, this only affects spells where you have checked the \"Local\" box.\n|cFFFF0000Note:|r Messages sent to the Raid Warning frame are still only visible by you."
L["Select"] = true
L["Only to chat"] = true
L["Only to Raid Warning"] = true
L["Send to Both"] = true

-- Repeated things
L["Spells"] = true
L["Announcement settings for your Spells."] = true
L["Enabled"] = true
L["Tracking"] = true
L["Protection Detection"] = true
L["Binding Settings"] = true
L["Scroll Wheel Bind"] = true
L["Reminder Settings"] = true
L["Remind Time"] = true
L["Set how often you want to be reminded in seconds."] = true
L["Channel Settings"] = true
L["Announcement Settings"] = true


-- Channel Settings
L["Raid/Party"] = true
L.raidpartydesc = "Announces in raid if you're in a raid, party if you're only in a party, or not at all if you're ungrouped."

L["Custom Channel"] = true
L["Announces in your custom channel."] = true

L["Local"] = true
L["Sends a local message to your default chat window that only you can see."] = true

L["Whisper"] = true
L["Whispers the target of your spell, unless the target is yourself."] = true


-- Taunts
L["Successful Taunts"] = true
L["Check this to announce upon successful taunt."] = true

L["Resists"] = true
L["Check this to announce upon taunt resists."] = true



-- Paladin
-- Spell Names
L["Righteous Fury"] = true
L["Righteous Defense"] = true
L["Hand of Reckoning"] = true
L["Divine Sacrifice"] = true
L["Divine Protection"] = true
L["Hand of Freedom"] = true
L["Hand of Sacrifice"] = true
L["Hand of Salvation"] = true
L["Hand of Protection"] = true
L["Lay on Hands"] = true
L["Divine Intervention"] = true
L["Forbearance"] = true
L["Divine Plea"] = true
L["Ardent Defender"] = true
L["Aura Mastery"] = true


-- Righteous Fury Settings
L["Settings for Righteous Fury"] = true
L["Do you want to enable Righteous Fury tracking?"] = true
L["Check to turn on Righteous Fury detection."] = true
L["Enable this to only track Righteous Fury when you are Protection."] = true
L["Check to turn on protection detection."] = true
L["Allow RSA to rebind your scrollwheel, this will bind your scroll wheel to Righteous Fury until cast."] = true
L.scrollwheelbind = "Check to enable scrollwheel rebinding. |cFFFF0000Note:|r This option is still in testing, please report any bugs to me on curse.com, thanks!"
L["Enable or Disable the spell reminder. This is the message you recieve whenever Righteous Fury is not up."] = true

-- Righteous Defense Settings
L["Settings for Righteous Defense"] = true
L["Here you can set which channels you want to send announcements to."] = true
L.rd_desc = "Here you can set if Righteous Defense announces upon successful taunt, or only taunt resists.\n|cFFFF0000Note:|r This may send up to three messages. It will send a message for every target it taunts."

-- Hand of Reckoning Settings
L["Settings for Hand of Reckoning"] = true
L["Here you can set whether Hand of Reckoning announces on successful casts, resists, or both."] = true

-- Channel Settings Descriptions
L["Channel settings for Divine Sacrifice"] = true
L["Channel settings for Divine Protection"] = true
L["Channel settings for Lay on Hands"] = true
L["Channel settings for Hand of Freedom"] = true
L["Channel settings for Hand of Sacrifice"] = true
L["Channel settings for Hand of Salvation"] = true
L["Channel settings for Hand of Protection"] = true
L["Channel settings for Divine Intervention"] = true
L["Channel settings for Forbearance"] = true
L["Channel settings for Divine Plea"] = true
L["Channel settings for Ardent Defender"] = true
L["Channel settings for Aura Mastery"] = true


-- End Paladin



-- Priest
-- Spell Names



-- Channel Settings Descriptions
L["Channel settings for Guardian Spirit"] = true
L["Channel settings for Pain Suppression"] = true
L["Channel settings for Power Infusion"] = true
L["Channel settings for Fear Ward"] = true
L["Channel settings for Divine Hymn"] = true
L["Channel settings for Hymn of Hope"] = true














