﻿local db = {};

function LKA:debug(message)
  if(db.debug) then
    self:message("|cFF00FFFFDebug|r|cFFFFFF00 - "..message);
  end
end

function LKA:format_number(number)
  local formatted = (number > 100000 and format("%.1f", number/1000000).."M") or (number > 1000 and format("%i", number/1000)..","..(number%1000)) or number;
  return formatted;
end

function LKA:ratio_to_hex_color(r, g, b)
  local red = r * 255;
  local green = g * 255;
  local blue = b * 255;
  return self:dec_to_hex_color(red, green, blue);
end

function LKA:dec_to_hex_color(r, g, b)
  local red = self:pad(string.format("%X", r));
  local green = self:pad(string.format("%X", g));
  local blue = self:pad(string.format("%X", b));
  return red..green..blue
end

function LKA:GUID_to_NPC(GUID)
  if not(GUID) then
    return nil;
  end
  return tonumber(string.sub(GUID, 9, 12), 16);
end

function LKA:pad(input)
    if(string.len(input) == 1) then
        return "0"..input;
    elseif(string.len(input) == 0) then
        return "00";
    else
        return input;
    end
end

function LKA:message(message)
  print("|TInterface\\Icons\\achievement_boss_lichking"..LKA.ZOOMED_ICON.."|t|cFFFF0000LKAnnounce|r|cFFFFFF00: "..message);
end

function LKA:printraid(message)
  if(GetNumRaidMembers() > 0) then
    SendChatMessage(message, "RAID", nil, nil);
  else
    self:message(message);
  end
end

local ADDON_NAME = "LKAnnounce";
local talentLib = LibStub and LibStub:GetLibrary("LibGroupTalents-1.0", true)
local LDB = LibStub and LibStub:GetLibrary("LibDataBroker-1.1");
local timer = LibStub and LibStub("AceTimer-3.0");

local frame;
local pending_defile = false;
local cooldowns = {};
local phase = 1;
local raid_positions = {};
local infest_count = 1;
local last_infest = 0;
local trap_targets = {};
local picked = {};
local num_picked = 0;

local attempt = 0;
local section = 0;

local valks_timer;
local infest_timer;
local traps_timer;
local plague_timers = {};

local lichking_guid = "";
local horror_names = {};
local valkyr_guids = {};
local raging_guids = {};
local current_horror = 1;
local raging_targeting = {};
local num_valkyr = 0;
local encounter_enabled = false;
local defeated = false;
local num_infests = 0;

local NONE = " none";
local NONE_DISPLAY = "|cFFFF0000None";

local NORAID = "|cFFFF0000Not in a Raid Group|r";
local loaded_header = NORAID;

local ZONE = "Icecrown Citadel";
local SUBZONE = "The Frozen Throne";
local LKPULL = "So the Light's vaunted justice has finally arrived? Shall I lay down Frostmourne and throw myself at your mercy, Fordring?";

--NPC codes that can be calculated from GUIDs
local N = {
  THE_LICH_KING = 36597,
  SHAMBLING_HORROR = 37698,
  DRUDGE_GHOUL = 37695,
  VALKYR_SHADOWGUARD = 36609,
  RAGING_SPIRIT = 36701
};

--Get localized spell names
local S = {
  CURSE_OF_THE_ELEMENTS = GetSpellInfo(47865),
  EARTH_AND_MOON = GetSpellInfo(48511),
  EBON_PLAGUE = GetSpellInfo(51726),
  FRENZY = GetSpellInfo(28747),
  ENRAGE = GetSpellInfo(72148),
  NECROTIC_PLAGUE = GetSpellInfo(73914),
  DEFILE = GetSpellInfo(72762),
  INFEST = GetSpellInfo(73781),
  HARVEST_SOUL = GetSpellInfo(74325),
  REMORSELESS_WINTER = GetSpellInfo(74275),
  SHADOW_TRAP = GetSpellInfo(73529),
  ICE_BURST = GetSpellInfo(73775),
  SUMMON_VALKYR = GetSpellInfo(69037),
  FURY_OF_FROSTMOURNE = GetSpellInfo(72350)
};

--Data from Data.lua
local ZOOMED_ICON = LKA.ZOOMED_ICON;
local ZOOMED_ICON_SMALL = LKA.ZOOMED_ICON_SMALL;
local RAID_TARGET = LKA.RAID_TARGET;

local ASSIGNMENT_HELP = LKA.ASSIGNMENT_HELP;
local channel_colors = LKA.channel_colors;
local channels = LKA.channels;
local class_colors = LKA.class_colors;

local stuns = LKA.stuns;
local rotation_stuns = LKA.rotation_stuns;
local slows = LKA.slows;
local aoe_stuns = LKA.aoe_stuns;
local aoe_slows = LKA.aoe_slows;
local infests = LKA.infests;

local shapes = LKA.shapes;
local shapes_sets = LKA.shapes_sets;
local possible_roles = LKA.possible_roles;
local possible_stuns = LKA.possible_stuns;
local possible_slows = LKA.possible_slows;
local possible_aoe_stuns = LKA.possible_aoe_stuns;
local possible_aoe_slows = LKA.possible_aoe_slows;
local possible_infests = LKA.possible_infests;

local add_to_dps = 1;

local dps = {
  names = function(self, num)
    local ret = {};
    for i1,v1 in pairs(self.content) do
      local formatted_name = i1;
      local _, class = UnitClass(i1);
      if(class) then
        formatted_name = class_colors[class]..formatted_name;
      end
      --Check every available stun
      for i2,v2 in pairs(stuns.content) do
        --If this stun matches the player's name
        if(v2.playerName == i1) then
          local assigned = false;
          --Check every main stun assignment
          for s=1,3 do
            if(db.valks.stun[s] == i2) then
              --Adds a shape to the display if the person is legitimately assigned to stun that shape
              formatted_name = shapes.content[db.valks.shapes[s]].texture.." "..formatted_name;
            end
          end
        end
      end
      --Check every available slow
      for i2,v2 in pairs(slows.content) do
        --If this slow matches the player's name
        if(v2.playerName == i1) then
          local assigned = false;
          --Check every main slow assignment
          for s=1,3 do
            if(db.valks.slow[s] == i2) then
              --Adds a shape to the display if the person is legitimately assigned to slow that shape
              formatted_name = shapes.content[db.valks.shapes[s]].texture.." "..formatted_name;
            end
          end
        end
      end
      if(db.valks.dps[i1] == num) then
        ret[i1] = formatted_name;
      elseif(num == 0 and not db.valks.dps[i1]) then
        ret[i1] = formatted_name;
      end
    end
    
    return ret;
  end,
  content = {}
}

local options = {
  type = "group",
  args = {}
}

local function tooltip_draw()
  GameTooltip:ClearLines();
	GameTooltip:AddDoubleLine("LKAnnounce", loaded_header);
	GameTooltip:AddDoubleLine("By "..class_colors.MAGE.."Mirrored|r of Dragonmaw(US) Horde", "");
	GameTooltip:AddLine(" ");
	GameTooltip:AddLine("|cffeda55fClick|r for options", 0.2, 1, 0.2);
	GameTooltip:AddLine("|cffeda55fShift-Click|r to announce all assignments", 0.2, 1, 0.2);
	GameTooltip:Show();
end

local tooltip_update_frame = CreateFrame("FRAME");
local LKADB = LDB:NewDataObject("LKAnnounce", {
  type = "data source",
  text = "LKAnnounce",
  icon = "Interface\\Icons\\achievement_boss_lichking",
	OnClick = function(self, button)
		if(IsShiftKeyDown()) then
			LKA:Announce_Stuns();
      LKA:Announce_Slows();
      LKA:AnnounceInfests();
      LKA:AnnounceDPS();
		else
			InterfaceOptionsFrame_OpenToCategory("LKAnnounce");
		end
	end,
	OnEnter = function(self)
		local elapsed = 0;
		local delay = 1;
		tooltip_update_frame:SetScript("OnUpdate", function(self, elap)
      elapsed = elapsed + elap;
      if(elapsed > delay) then
        elapsed = 0;
        tooltip_draw();
      end
	  end);
		
		--Section the screen into 6 quadrants (sextrants?) and define the tooltip anchor position based on which quadrant the cursor is in
		local max_x = GetScreenWidth();
		local max_y = GetScreenHeight();
		local x, y = GetCursorPosition();
		local horizontal = (x < (max_x/3) and "LEFT") or ((x >= (max_x/3) and x < ((max_x/3)*2)) and "") or "RIGHT";
		local tooltip_vertical = (y < (max_y/2) and "BOTTOM") or "TOP";
		local anchor_vertical = (y < (max_y/2) and "TOP") or "BOTTOM";
    
		GameTooltip:SetOwner(self, "ANCHOR_NONE");
		GameTooltip:SetPoint(tooltip_vertical..horizontal, self, anchor_vertical..horizontal);
		tooltip_draw();
	end,
	OnLeave = function(self)
		tooltip_update_frame:SetScript("OnUpdate", nil);
	end
});

local alertFrame = CreateFrame("FRAME", UIParent);
alertFrame:SetFrameStrata("TOOLTIP");
alertFrame:SetAllPoints(UIParent);
alertFrame.texture = UIParent:CreateTexture();
alertFrame.texture:SetAllPoints(alertFrame);
alertFrame.elapsed = 0;
alertFrame:SetScript("OnUpdate", function(self, elaps)
  self.elapsed = self.elapsed + elaps;
  if(self.elapsed > 0.5) then
    self.texture:SetAlpha(2 - (2 * self.elapsed));
  end
  if(self.elapsed > 1) then
    self.elapsed = 0;
    self:Hide();
  end
end);

function LKA.ChatColors(frame, event, ...)
  channel_colors.RAID = LKA:ratio_to_hex_color(ChatTypeInfo.RAID.r,ChatTypeInfo.RAID.g,ChatTypeInfo.RAID.b);
  channel_colors.RAID_WARNING = LKA:ratio_to_hex_color(ChatTypeInfo.RAID_WARNING.r, ChatTypeInfo.RAID_WARNING.g, ChatTypeInfo.RAID_WARNING.b);
  channel_colors.YELL = LKA:ratio_to_hex_color(ChatTypeInfo.YELL.r, ChatTypeInfo.YELL.g, ChatTypeInfo.YELL.b);
  channel_colors.PARTY = LKA:ratio_to_hex_color(ChatTypeInfo.PARTY.r, ChatTypeInfo.PARTY.g, ChatTypeInfo.PARTY.b);
  channel_colors.SAY = LKA:ratio_to_hex_color(ChatTypeInfo.SAY.r, ChatTypeInfo.SAY.g, ChatTypeInfo.SAY.b);
  channel_colors.GUILD = LKA:ratio_to_hex_color(ChatTypeInfo.GUILD.r, ChatTypeInfo.GUILD.g, ChatTypeInfo.GUILD.b);
  
  channels.RAID = "|cFF"..channel_colors.RAID.."Raid|r";
  channels.RAID_WARNING = "|cFF"..channel_colors.RAID_WARNING.."RW|r";
  channels.YELL = "|cFF"..channel_colors.YELL.."Yell|r";
  channels.PARTY = "|cFF"..channel_colors.PARTY.."Party|r";
  channels.SAY = "|cFF"..channel_colors.SAY.."Say|r";
  channels.GUILD = "|cFF"..channel_colors.GUILD.."Guild|r";
end

local chat_colors_frame = CreateFrame("FRAME");
chat_colors_frame:RegisterEvent("UPDATE_CHAT_COLOR");
chat_colors_frame:SetScript("OnEvent", LKA.ChatColors);

function LKA.Initialize(givenframe, event, ...)
  local self = LKA;
  frame:RegisterEvent("PARTY_MEMBERS_CHANGED");
  
  if not(LKAnnounceSaved) then
    LKAnnounceSaved = {};
  end
  db = LKAnnounceSaved;
  if not(db.valks) then
    self:DefaultAssignments();
  else
    for i,v in pairs(self:GetDefaultAssignments()) do
      if not(db.valks[i]) then
        db.valks[i] = v;
      end
    end
  end
  if not(db.options) then
    self:DefaultOptions()
  else
    for i1,v1 in pairs(self:GetDefaultOptions()) do
      if not(db.options[i1]) then
        db.options[i1] = v1;
      else
        for i2,v2 in pairs(v1) do
          if not(db.options[i1][i2]) then
            db.options[i1][i2] = v2;
          end
        end
      end
    end
  end
  
  options = {
    type = "group",
    args = {
      debug = {
        type = "execute",
        name = "Debug",
        hidden = true,
        desc = "Enable debug messages",
        func = function()
          db.debug = true;
          self:debug("Debug messages enabled");
        end,
        guiHidden = true
      },
      nodebug = {
        type = "execute",
        name = "No Debug",
        desc = "Turn off debug messages",
        func = function()
          db.debug = false;
          self:message("Debug messages disabled");
        end,
        guiHidden = true
      },
      config = {
        type = "execute",
        name = "Configure",
        order = 0,
        desc = "Open the configuration dialog",
        func = function()
          InterfaceOptionsFrame_OpenToCategory("LKAnnounce"); --Opens the config frame
        end,
        guiHidden = true
      },
      roles = {
        type = "group",
        name = "Raid Member Talents",
        order = 10,
        childGroups = "tree",
        desc = "Roles and talent specs of members of the raid.",
        args = {
          loaded = {
            type = "header",
            name = function(info) return loaded_header end,
            order = 0
          },
          main = {
            type = "group",
            name = "   ---Main Raid---",
            inline = false,
            disabled = true,
            order = 5,
            args = {}
          },
          extra = {
            type = "group",
            name = "   ---Extras---",
            inline = false,
            disabled = true,
            hidden = function() return db.options.general.options.ignore_extras end,
            order = function(info)
              local difficulty = GetRaidDifficulty();
              if(difficulty == 1 or difficulty == 3) then
                return 25;
              elseif(difficulty == 2 or difficulty == 4) then
                return 55;
              end
              return 0;
            end,
            args = {}
          }
        }
      },
      assignments = {
        type = "group",
        name = "Assignments",
        order = 20,
        childGroups = "tab",
        desc = "Assignments for stuns and slows for Val'Kyr Shadowguards in phase 2, Infest cooldowns, and healing throughout the fight.",
        args = {
          loaded = {
            type = "header",
            name = function(info) return loaded_header end,
            order = 0
          },
          stuns = {
            type = "group",
            name = "Stuns",
            order = 10,
            desc = "Single target stuns for phase 2 Val'kyr",
            get = function(info) return self:Get_Assignment(info[#info-1], tonumber(info[#info])) end,
            set = function(info, v) self:Set_Assignment(info[#info-1], tonumber(info[#info]), v) end,
            args = {
              help = {
                type = "execute",
                name = "Help",
                width = "half",
                confirm = function(info) return ASSIGNMENT_HELP end,
                order = 1,
                desc = "Get help with assignments",
                func = function() --[[doesn't actually do anything]] end
              },
              wipe = {
                type = "execute",
                name = "Wipe",
                width = "half",
                confirm = function(info) return "Are you sure you want to wipe all stun assignments?" end,
                order = 2,
                desc = "Delete all assignments",
                func = function()
                  wipe(db.valks.stun);
                  wipe(db.valks.aoestun);
                end
              },
              announce = {
                type = "execute",
                name = "Announce",
                order = 3,
                desc = "Announce stun assignments to the raid",
                func = function() self:Announce_Stuns(); end
              },
              stun = {
                type = "group",
                order = 10,
                inline = true,
                name = function() return "Single Target Stuns"..((db.options.valkyr.stun.enabled and "") or " - |cFFFF0000Announcements Disabled") end,
                args = {
                  ["1"] = {
                    type = "select",
                    name = function(info) return shapes.content[db.valks.shapes[1]].name end,
                    order = 10,
                    width = "double",
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  },
                  ["11"] = {
                    type = "select",
                    name = "",
                    order = 15,
                    width = "double",
                    hidden = function(info) return not rotation_stuns.content[db.valks.stun[1]] end,
                    desc = function(info) return "Second in stun rotation for "..shapes.content[db.valks.shapes[1]].name end,
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  },
                  ["2"] = {
                    type = "select",
                    name = function(info) return shapes.content[db.valks.shapes[2]].name end,
                    order = 20,
                    width = "double",
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  },
                  ["12"] = {
                    type = "select",
                    name = "",
                    order = 25,
                    width = "double",
                    hidden = function(info) return not rotation_stuns.content[db.valks.stun[2]] end,
                    desc = function(info) return "Second in stun rotation for "..shapes.content[db.valks.shapes[2]].name end,
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  },
                  ["3"] = {
                    type = "select",
                    name = function(info) return shapes.content[db.valks.shapes[3]].name end,
                    order = 30,
                    width = "double",
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  },
                  ["13"] = {
                    type = "select",
                    name = "",
                    order = 35,
                    width = "double",
                    hidden = function(info) return not rotation_stuns.content[db.valks.stun[3]] end,
                    desc = function(info) return "Second in stun rotation for "..shapes.content[db.valks.shapes[3]].name end,
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  },
                  ["4"] = {
                    type = "select",
                    name = "First backup stun",
                    order = 40,
                    width = "double",
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  },
                  ["5"] = {
                    type = "select",
                    name = "Second backup stun",
                    order = 45,
                    width = "double",
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  },
                  ["6"] = {
                    type = "select",
                    name = "Third backup stun",
                    order = 50,
                    width = "double",
                    values = function(info) return stuns:names("stun", tonumber(info[#info])) end
                  }
                }
              },
              aoestun = {
                type = "group",
                order = 20,
                inline = true,
                name = function() return "AoE Stuns"..((db.options.valkyr.aoestun.enabled and "") or " - |cFFFF0000Announcements Disabled") end,
                args = {
                  ["1"] = {
                    type = "select",
                    name = "First AoE stun",
                    width = "double",
                    order = 81,
                    values = function(info) return aoe_stuns:names("aoestun", 1) end,
                  },
                  ["2"] = {
                    type = "select",
                    name = "Second AoE stun",
                    width = "double",
                    order = 82,
                    values = function(info) return aoe_stuns:names("aoestun", 2) end,
                  },
                  ["3"] = {
                    type = "select",
                    name = "Third AoE stun",
                    width = "double",
                    order = 83,
                    values = function(info) return aoe_stuns:names("aoestun", 3) end,
                  }
                }
              }
            }
          },
          slows = {
            type = "group",
            name = "Slows",
            order = 20,
            desc = "Single target slows for phase 2 Val'kyr",
            get = function(info) return self:Get_Assignment(info[#info-1], tonumber(info[#info])) end,
            set = function(info, v) self:Set_Assignment(info[#info-1], tonumber(info[#info]), v) end,
            args = {
              help = {
                type = "execute",
                name = "Help",
                width = "half",
                confirm = function(info) return ASSIGNMENT_HELP end,
                order = 1,
                desc = "Get help with assignments",
                func = function() --[[doesn't actually do anything]] end
              },
              wipe = {
                type = "execute",
                name = "Wipe",
                width = "half",
                order = 2,
                confirm = function(info) return "Are you sure you want to wipe all stun assignments?" end,
                desc = "Delete all assignments",
                func = function()
                  wipe(db.valks.slow);
                  wipe(db.valks.aoeslow);
                end
              },
              announce = {
                type = "execute",
                name = "Announce",
                order = 3,
                desc = "Announce slow assignments to the raid",
                func = function() self:Announce_Slows(); end
              },
              slow = {
                type = "group",
                order = 10,
                inline = true,
                name = function() return "Single Target Slows"..((db.options.valkyr.slow.enabled and "") or " - |cFFFF0000Announcements Disabled") end,
                args = {
                  ["1"] = {
                    type = "select",
                    name = function(info) return shapes.content[db.valks.shapes[1]].name end,
                    width = "double",
                    order = 10,
                    values = function(info) return slows:names("slow", 1) end
                  },
                  ["2"] = {
                    type = "select",
                    name = function(info) return shapes.content[db.valks.shapes[2]].name end,
                    width = "double",
                    order = 15,
                    values = function(info) return slows:names("slow", 2) end
                  },
                  ["3"] = {
                    type = "select",
                    name = function(info) return shapes.content[db.valks.shapes[3]].name end,
                    width = "double",
                    order = 20,
                    values = function(info) return slows:names("slow", 3) end,
                  },
                  ["4"] = {
                    type = "select",
                    name = "First backup slow",
                    width = "double",
                    order = 25,
                    values = function(info) return slows:names("slow", 4) end,
                  },
                  ["5"] = {
                    type = "select",
                    name = "Second backup slow",
                    width = "double",
                    order = 30,
                    values = function(info) return slows:names("slow", 5) end,
                  },
                  ["6"] = {
                    type = "select",
                    name = "Third backup slow",
                    width = "double",
                    order = 35,
                    values = function(info) return slows:names("slow", 6) end,
                  }
                }
              },
              aoeslow = {
                type = "group",
                order = 20,
                inline = true,
                name = function() return "AoE Slows"..((db.options.valkyr.aoeslow.enabled and "") or " - |cFFFF0000Announcements Disabled") end,
                args = {
                  ["1"] = {
                    type = "select",
                    name = "First AoE slow",
                    width = "double",
                    order = 10,
                    values = function(info) return aoe_slows:names("aoeslow", 1) end,
                  },
                  ["2"] = {
                    type = "select",
                    name = "Second AoE slow",
                    width = "double",
                    order = 15,
                    values = function(info) return aoe_slows:names("aoeslow", 2) end,
                  },
                  ["3"] = {
                    type = "select",
                    name = "Third AoE slow",
                    width = "double",
                    order = 20,
                    values = function(info) return aoe_slows:names("aoeslow", 3) end,
                  }
                }
              }
            }
          },
          dps = {
            type = "group",
            name = "DPS",
            order = 25,
            childGroups = "select",
            desc = "DPS assignments for Val'kyr",
            get = function() return true end,
            set = function(info, v) db.valks.dps[v] = nil end,
            args = {
              announce = {
                type = "execute",
                name = "Announce",
                order = 0,
                func = function() self:AnnounceDPS() end
              },
              header = {
                type = "header",
                name = "",
                order = 5
              },
              desc = {
                type = "description",
                fontSize = "medium",
                width = "half",
                name = "Add to:",
                order = 6,
              },
              add1 = {
                type = "toggle",
                name = function() return shapes.content[db.valks.shapes[1]].texture end,
                width = "half",
                get = function() return add_to_dps == 1; end,
                set = function(info, v) add_to_dps = 1; end,
                order = 7
              },
              add2 = {
                type = "toggle",
                name = function() return shapes.content[db.valks.shapes[2]].texture end,
                width = "half",
                get = function() return add_to_dps == 2; end,
                set = function(info, v) add_to_dps = 2; end,
                order = 8
              },
              add3 = {
                type = "toggle",
                name = function() return shapes.content[db.valks.shapes[3]].texture end,
                width = "half",
                get = function() return add_to_dps == 3; end,
                set = function(info, v) add_to_dps = 3; end,
                order = 9
              },
              select = {
                type = "multiselect",
                name = "",
                order = 20,
                width = "half",
                values = function() return dps:names(0) end,
                hidden = function()
                  local num = 0;
                  for i,v in pairs(dps:names(0)) do
                    num = num + 1;
                  end
                  if(num > 0) then
                    return false;
                  else
                    return true;
                  end
                end,
                get = function() return false end,
                set = function(info, v) db.valks.dps[v] = add_to_dps end
              },
              dps1 = {
                type = "multiselect",
                name = function() return shapes.content[db.valks.shapes[1]].name end,
                width = "half",
                order = function()
                  if(add_to_dps == 1) then
                    return 25;
                  else
                    return 30;
                  end
                end,
                values = function() return dps:names(1) end,
              },
              dps2 = {
                type = "multiselect",
                name = function() return shapes.content[db.valks.shapes[2]].name end,
                width = "half",
                order = function()
                  if(add_to_dps == 2) then
                    return 25;
                  else
                    return 40;
                  end
                end,
                values = function() return dps:names(2) end,
              },
              dps3 = {
                type = "multiselect",
                name = function() return shapes.content[db.valks.shapes[3]].name end,
                width = "half",
                order = function()
                  if(add_to_dps == 3) then
                    return 25;
                  else
                    return 50;
                  end
                end,
                values = function() return dps:names(3) end,
              }
            }
          },
          infests = {
            type = "group",
            name = "Infests",
            order = 30,
            desc = "Infests cooldown assignments for phases 1 and 2",
            args = {
              add = {
                type = "execute",
                order = 2,
                name = "+",
                width = "half",
                desc = "Add an infest assignment",
                func = function() self:AddInfest() end
              },
              remove = {
                type = "execute",
                order = 4,
                name = "-",
                width = "half",
                disabled = true,
                desc = "Add an infest assignment",
                func = function() self:RemoveInfest() end
              },
              announce = {
                type = "execute",
                order = 6,
                name = "Announce",
                width = "normal",
                desc = "Announce infest assignments",
                func = function() self:AnnounceInfests() end
              }
            }
          },
          heals = {
            type = "group",
            name = "Healing",
            order = 40,
            desc = "Healing assignments for phase 2",
            args = {
              name = {
                type = "input",
                order = 10,
                name = "Add Healer",
                get = function(info) end,
                set = function(info, v) self:AddHealer(v); end
              },
              announce = {
                type = "execute",
                order = 20,
                name = "Announce",
                func = function(info) self:AnnounceHealingAssignments() end
              }
            }
          }
        }
      },
      options = {
        type = "group",
        name = "Options",
        order = 30,
        childGroups = "tree",
        desc = "Options for announcements throughout the fight",
        get = function(info) return db.options[info[#info-2]][info[#info-1]][info[#info]] end,
        set = function(info, v) db.options[info[#info-2]][info[#info-1]][info[#info]] = v end,
        args = {
          loaded = {
            type = "header",
            name = function(info) return loaded_header end,
            order = 0
          },
          general = {
            type = "group",
            name = "General",
            order = 5,
            args = {
              defaultAssignments = {
                type = "execute",
                name = "Default Assignments",
                confirm = function() return "Are you sure you want to return all assignments to default settings?"; end,
                func = function() self:DefaultAssignments(); end,
                order = 2
              },
              defaultOptions = {
                type = "execute",
                name = "Default Options",
                confirm = function() return "Are you sure you want to return all options to default settings?"; end,
                func = function() self:DefaultOptions(); end,
                order = 4
              },
              options = {
                type = "group",
                name = "Options",
                inline = true,
                order = 10,
                args = {
                  announce_desc = {
                    type = "description",
                    name = "Allow announcements globally (LKAnnounce will not announce anything if this is not checked)",
                    order = 10
                  },
                  announce = {
                    type = "toggle",
                    name = "Enable",
                    order = 20
                  },
                  downgrade_desc = {
                    type = "description",
                    name = "Automatically announce to Raid instead of Raid Warning if not promoted",
                    order = 30
                  },
                  downgrade = {
                    type = "toggle",
                    name = "Enable",
                    order = 40
                  },
                  messages_desc = {
                    type = "description",
                    name = "Allow customization of announcement messages",
                    order = 50
                  },
                  messages = {
                    type = "toggle",
                    name = "Enable",
                    order = 60
                  },
                  ignore_desc = {
                    type = "description",
                    name = "Ignore extra raid members (those in group 6-8 in 25m or 3-8 in 10m)",
                    order = 70
                  },
                  ignore_extras = {
                    type = "toggle",
                    name = "Enable",
                    order = 80
                  }
                }
              }
            }
          },
          plague = {
            type = "group",
            name = "Plagues & Horrors",
            order = 10,
            desc = "Options for Necrotic Plague and Shambling Horror announcements",
            args = {
              track = {
                type = "group",
                name = "Track Plagues",
                inline = true,
                order = 10,
                args = {
                  desc = {
                    type = "description",
                    order = 10,
                    name = "Announce health and estimated Frenzy and death times of Shambling Horrors due to Necrotic Plague."
                  },
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "half",
                    order = 20
                  },
                  channel = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 30,
                    values = channels,
                    disabled = function() return not db.options.plague.track.enabled end
                  },
                  death_frenzy_desc = {
                    type = "description",
                    name = "Only announce when the plague would cause the Shambling Horror to die or Frenzy",
                    order = 40
                  },
                  death_frenzy_only = {
                    type = "toggle",
                    name = "Enable",
                    disabled = function() return not db.options.plague.track.enabled end,
                    order = 50
                  }
                }
              },
              enrage = {
                type = "group",
                order = 20,
                name = "Enrage",
                inline = true,
                args = {
                  desc = {
                    type = "description",
                    order = 10,
                    name = "Announce when Shambling Horrors cast Enrage."
                  },
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "half",
                    order = 12
                  },
                  channel = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 14,
                    values = channels,
                    disabled = function(info) return not db.options.plague.enrage.enabled end
                  },
                  message = {
                    type = "input",
                    name = "Message",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 40
                  }
                }
              },
              frenzy = {
                type = "group",
                order = 30,
                name = "Frenzy",
                inline = true,
                args = {
                  desc = {
                    type = "description",
                    order = 10,
                    name = "Announce when Shambling Horrors cast Frenzy (heroic only)"
                  },
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "half",
                    order = 20,
                  },
                  channel = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 30,
                    values = channels,
                    disabled = function(info) return not db.options.plague.frenzy.enabled end
                  },
                  message = {
                    type = "input",
                    name = "Message",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 40
                  }
                }
              }
            }
          },
          infest = {
            type = "group",
            name = "Infests",
            order = 20,
            childGroups = "tree",
            desc = "Options for Infest announcements",
            args = {
              number = {
                type = "group",
                order = 10,
                name = "Numbered Infests",
                inline = true,
                args = {
                  desc = {
                    type = "description",
                    order = 10,
                    name = "Number infests and preemptively announce when Infests are about to occur."
                  },
                  enabled1 = {
                    type = "toggle",
                    name = "Phase 1",
                    width = "half",
                    order = 20
                  },
                  channel1 = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 30,
                    values = channels,
                    disabled = function(info) return not db.options.infest.number.enabled1 end
                  },
                  message1 = {
                    type = "input",
                    name = "Message",
                    desc = "$n - Number of Infest\n$s - Spell name\n$p - Player name",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 32
                  },
                  whisper1 = {
                    type = "toggle",
                    name = "Whisper Assignment",
                    order = 35
                  },
                  whisperMessage1 = {
                    type = "input",
                    name = "Message",
                    desc = "$n - Number of Infest\n$s - Spell name\n$p - Player name",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 38
                  },
                  enabled2 = {
                    type = "toggle",
                    name = "Phase 2",
                    width = "half",
                    order = 40
                  },
                  channel2 = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 50,
                    values = channels,
                    disabled = function(info) return not db.options.infest.number.enabled2 end
                  },
                  message2 = {
                    type = "input",
                    name = "Message",
                    desc = "$n - Number of Infest\n$s - Spell name\n$p - Player name",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 60
                  },
                  whisper2 = {
                    type = "toggle",
                    name = "Whisper Assignment",
                    order = 70
                  },
                  whisperMessage2 = {
                    type = "input",
                    name = "Message",
                    desc = "$n - Number of Infest\n$s - Spell name\n$p - Player name",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 80
                  }
                }
              }
            }
          },
          valkyr = {
            type = "group",
            name = "Val'kyr",
            order = 30,
            childGroups = "tree",
            desc = "Options for Val'kyr Shadowguard announcements",
            get = function(info) return db.options.valkyr[info[#info-2]][info[#info-1]][info[#info]] end,
            set = function(info, v) db.options.valkyr[info[#info-2]][info[#info-1]][info[#info]] = v end,
            args = {
              picked = {
                type = "group",
                name = "Val'kyr Picks",
                inline = true,
                order = 5,
                get = function(info) return db.options.valkyr.picked[info[#info]] end,
                set = function(info, v) db.options.valkyr.picked[info[#info]] = v end,
                args = {
                  picked_desc = {
                    type = "description",
                    name = "Announce who is picked by Val'kyr Shadowguards",
                    order = 10
                  },
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "half",
                    order = 20
                  },
                  channel = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 30,
                    values = channels,
                    disabled = function(info) return not db.options.valkyr.picked.enabled end
                  },
                  message = {
                    type = "input",
                    name = "Message",
                    desc = "$p - Player name",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 35
                  }
                }
              },
              mark = {
                type = "group",
                name = "Raid Icons",
                inline = true,
                order = 10,
                args = {
                  marks = {
                    type = "select",
                    name = "Shapes",
                    order = 50,
                    values = function(info) return shapes_sets:names() end,
                    get = function(info)
                      local set = nil;
                      for i,v in pairs(shapes_sets.content) do
                        if(v.marks[1] == db.valks.shapes[1] and v.marks[2] == db.valks.shapes[2] and v.marks[3] == db.valks.shapes[3]) then
                          set = i;
                          break;
                        end
                      end
                      return set;
                    end,
                    set = function(info, v)
                      local set = shapes_sets.content[v];
                      db.valks.shapes[1] = set.marks[1];
                      db.valks.shapes[2] = set.marks[2];
                      db.valks.shapes[3] = set.marks[3];
                    end
                  },
                  mark = {
                    type = "toggle",
                    name = "Mark Val'kyr",
                    order = 60,
                    get = function(info) return db.options.valkyr.picked.mark end,
                    set = function(info, v) db.options.valkyr.picked.mark = v end
                  }
                }
              },
              announce = {
                type = "group",
                name = "Modules",
                inline = true,
                order = 20,
                args = {
                  stun_enabled = {
                    type = "toggle",
                    name = "Stuns",
                    order = 10,
                    get = function(info) return db.options.valkyr.stun.enabled end,
                    set = function(info, v) db.options.valkyr.stun.enabled = v end
                  },
                  aoestun_enabled = {
                    type = "toggle",
                    name = "AoE Stuns",
                    order = 20,
                    get = function(info) return db.options.valkyr.aoestun.enabled end,
                    set = function(info, v) db.options.valkyr.aoestun.enabled = v end
                  },
                  slow_enabled = {
                    type = "toggle",
                    name = "Slows",
                    order = 30,
                    get = function(info) return db.options.valkyr.slow.enabled end,
                    set = function(info, v) db.options.valkyr.slow.enabled = v end
                  },
                  aoeslow_enabled = {
                    type = "toggle",
                    name = "AoE Slows",
                    order = 40,
                    get = function(info) return db.options.valkyr.aoeslow.enabled end,
                    set = function(info, v) db.options.valkyr.aoeslow.enabled = v end
                  }
                }
              },
              stun = {
                type = "group",
                name = "Stuns",
                order = 20,
                hidden = function(info) return not db.options.valkyr.stun.enabled end,
                args = {
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "full",
                    order = 0,
                    hidden = false,
                    get = function(info) return db.options.valkyr.stun.enabled end,
                    set = function(info, v) db.options.valkyr.stun.enabled = v end
                  },
                  backup = {
                    type = "group",
                    name = "Backups",
                    inline = true,
                    order = 2,
                    args = {
                      desc = {
                        type = "description",
                        name = "Announce Val'kyr stun backups when the main assigned raiders are disabled",
                        order = 4,
                      },
                      enabled = {
                        type = "toggle",
                        name = "Enable",
                        width = "half",
                        order = 6
                      },
                      channel = {
                        type = "select",
                        name = "Channel",
                        width = "half",
                        order = 8,
                        values = channels,
                        disabled = function(info) return not db.options.valkyr.stun.backup.enabled end
                      },
                      message = {
                        type = "input",
                        name = "Message",
                        desc = "$d - Disabled player name\n$o - Reason player is disabled\n$p - Assigned player name\n$x - Shape",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.stun.enabled) end,
                        order = 40
                      },
                      whisper = {
                        type = "toggle",
                        name = "Whisper Assignments",
                        width = "full",
                        order = 50
                      },
                      whisperMessage = {
                        type = "input",
                        name = "Message",
                        desc = "$d - Disabled player name\n$o - Reason player is disabled\n$p - Assigned player name\n$x - Shape",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.stun.enabled) end,
                        order = 60
                      }
                    }
                  },
                  main = {
                    type = "group",
                    name = "Main",
                    inline = true,
                    order = 12,
                    args = {
                      desc = {
                        type = "description",
                        name = "Announce Val'kyr stuns even when they do not differ from the main assignment",
                        order = 14
                      },
                      enabled = {
                        type = "toggle",
                        name = "Enable",
                        width = "half",
                        order = 16
                      },
                      channel = {
                        type = "select",
                        name = "Channel",
                        width = "half",
                        order = 18,
                        values = channels,
                        disabled = function(info) return not db.options.valkyr.stun.main.enabled end
                      },
                      message = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name\n$x - Shape",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.stun.enabled) end,
                        order = 40
                      },
                      whisper = {
                        type = "toggle",
                        name = "Whisper Assignments",
                        width = "full",
                        order = 50
                      },
                      whisperMessage = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name\n$x - Shape",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.stun.enabled) end,
                        order = 60
                      }
                    }
                  }
                }
              },
              aoestun = {
                type = "group",
                name = "AoE Stuns",
                order = 30,
                hidden = function(info) return not db.options.valkyr.aoestun.enabled end,
                args = {
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "full",
                    order = 0,
                    hidden = false,
                    get = function(info) return db.options.valkyr.aoestun.enabled end,
                    set = function(info, v) db.options.valkyr.aoestun.enabled = v end
                  },
                  backup = {
                    type = "group",
                    name = "Backups",
                    order = 2,
                    inline = true,
                    args = {
                      desc = {
                        type = "description",
                        name = "Announce a Val'kyr AoE stun backup when the main assigned raider is disabled",
                        order = 4
                      },
                      enabled = {
                        type = "toggle",
                        name = "Enable",
                        width = "half",
                        order = 6
                      },
                      channel = {
                        type = "select",
                        name = "Channel",
                        width = "half",
                        order = 8,
                        values = channels,
                        disabled = function(info) return not db.options.valkyr.aoestun.backup.enabled end
                      },
                      message = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.aoestun.enabled) end,
                        order = 40
                      },
                      whisper = {
                        type = "toggle",
                        name = "Whisper Assignment",
                        width = "full",
                        order = 50
                      },
                      whisperMessage = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.aoestun.enabled) end,
                        order = 60
                      }
                    }
                  },
                  main = {
                    type = "group",
                    name = "Main",
                    order = 12,
                    inline = true,
                    args = {
                      desc = {
                        type = "description",
                        name = "Announce the Val'kyr AoE stun even when it does not differ from the main assignment",
                        order = 14
                      },
                      enabled = {
                        type = "toggle",
                        name = "Enable",
                        width = "half",
                        order = 16
                      },
                      channel = {
                        type = "select",
                        name = "Channel",
                        width = "half",
                        order = 18,
                        values = channels,
                        disabled = function(info) return not db.options.valkyr.aoestun.main.enabled end
                      },
                      message = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.aoestun.enabled) end,
                        order = 40
                      },
                      whisper = {
                        type = "toggle",
                        name = "Whisper Assignment",
                        width = "full",
                        order = 50
                      },
                      whisperMessage = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.aoestun.enabled) end,
                        order = 60
                      }
                    }
                  }
                }
              },
              slow = {
                type = "group",
                name = "Slows",
                order = 40,
                hidden = function(info) return not db.options.valkyr.slow.enabled end,
                args = {
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "full",
                    order = 0,
                    hidden = false,
                    get = function(info) return db.options.valkyr.slow.enabled end,
                    set = function(info, v) db.options.valkyr.slow.enabled = v end
                  },
                  backup = {
                    type = "group",
                    name = "Backups",
                    order = 2,
                    inline = true,
                    args = {
                      desc = {
                        type = "description",
                        name = "Announce Val'kyr slow backups when the main assigned raiders are disabled.",
                        order = 4
                      },
                      enabled = {
                        type = "toggle",
                        name = "Enable",
                        width = "half",
                        order = 6
                      },
                      channel = {
                        type = "select",
                        name = "Channel",
                        width = "half",
                        order = 8,
                        values = channels,
                        disabled = function(info) return not db.options.valkyr.slow.backup.enabled end
                      },
                      message = {
                        type = "input",
                        name = "Message",
                        desc = "$d - Disabled player name\n$o - Reason player is disabled\n$p - Assigned player name\n$x - Shape",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.slow.enabled) end,
                        order = 40
                      },
                      whisper = {
                        type = "toggle",
                        name = "Whisper Assignments",
                        width = "full",
                        order = 50
                      },
                      whisperMessage = {
                        type = "input",
                        name = "Message",
                        desc = "$d - Disabled player name\n$o - Reason player is disabled\n$p - Assigned player name\n$x - Shape",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.slow.enabled) end,
                        order = 60
                      }
                    }
                  },
                  main = {
                    type = "group",
                    name = "Main",
                    order = 12,
                    inline = true,
                    args = {
                      desc = {
                        type = "description",
                        name = "Announce Val'kyr slows even when they do not differ from the main assignment.",
                        order = 14
                      },
                      enabled = {
                        type = "toggle",
                        name = "Enable",
                        width = "half",
                        order = 16
                      },
                      channel = {
                        type = "select",
                        name = "Channel",
                        width = "half",
                        order = 18,
                        values = channels,
                        disabled = function(info) return not db.options.valkyr.slow.main.enabled end
                      },
                      message = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name\n$x - Shape",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.slow.enabled) end,
                        order = 40
                      },
                      whisper = {
                        type = "toggle",
                        name = "Whisper Assignments",
                        width = "full",
                        order = 50
                      },
                      whisperMessage = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name\n$x - Shape",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.slow.enabled) end,
                        order = 60
                      }
                    }
                  }
                }
              },
              aoeslow = {
                type = "group",
                name = "AoE Slows",
                order = 50,
                hidden = function(info) return not db.options.valkyr.aoeslow.enabled end,
                args = {
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "full",
                    order = 0,
                    hidden = false,
                    get = function(info) return db.options.valkyr.aoeslow.enabled end,
                    set = function(info, v) db.options.valkyr.aoeslow.enabled = v end
                  },
                  backup = {
                    type = "group",
                    name = "Backups",
                    order = 2,
                    inline = true,
                    args = {
                      desc = {
                        type = "description",
                        name = "Announce a Val'kyr AoE stun backup when the main assigned raider is disabled",
                        order = 4
                      },
                      enabled = {
                        type = "toggle",
                        name = "Enable",
                        width = "half",
                        order = 6
                      },
                      channel = {
                        type = "select",
                        name = "Channel",
                        width = "half",
                        order = 8,
                        values = channels,
                        disabled = function(info) return not db.options.valkyr.aoeslow.backup.enabled end
                      },
                      message = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.aoeslow.enabled) end,
                        order = 40
                      },
                      whisper = {
                        type = "toggle",
                        name = "Whisper Assignment",
                        width = "full",
                        order = 50
                      },
                      whisperMessage = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.aoeslow.enabled) end,
                        order = 60
                      }
                    }
                  },
                  main = {
                    type = "group",
                    name = "Main",
                    order = 12,
                    inline = true,
                    args = {
                      desc = {
                        type = "description",
                        name = "Announce the Val'kyr AoE stun even when it does not differ from the main assignment",
                        order = 14
                      },
                      enabled = {
                        type = "toggle",
                        name = "Enable",
                        width = "half",
                        order = 16
                      },
                      channel = {
                        type = "select",
                        name = "Channel",
                        width = "half",
                        order = 18,
                        values = channels,
                        disabled = function(info) return not db.options.valkyr.aoeslow.main.enabled end
                      },
                      message = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.aoeslow.enabled) end,
                        order = 40
                      },
                      whisper = {
                        type = "toggle",
                        name = "Whisper Assignment",
                        width = "full",
                        order = 50
                      },
                      whisperMessage = {
                        type = "input",
                        name = "Message",
                        desc = "$p - Assigned player name",
                        hidden = function(info) return not (db.options.general.options.messages and db.options.valkyr.aoeslow.enabled) end,
                        order = 60
                      }
                    }
                  }
                }
              }
            }
          },
          defile = {
            type = "group",
            name = "Defile",
            order = 40,
            childGroups = "tree",
            desc = "Options for Defile announcements",
            args = {
              warning_header1 = {
                type = "header",
                order = 0,
                name = "|cFFFF0000Warning: |r"
              },
              warning1 = {
                type = "description",
                fontSize = "medium",
                order = 1,
                name = "Defile announcements are more reliable if you put The Lich King on /focus"
              },
              --[[warning_header2 = {
                type = "header",
                order = 2,
                name = "|cFFFF0000Warning: |r"
              },
              warning2 = {
                type = "description",
                fontSize = "medium",
                order = 3,
                name = "Defile announcements do not register for the main tank (because The Lich King does not change targets)"
              },]]
              announce = {
                type = "group",
                name = "Announce",
                order = 10,
                args = {
                  header = {
                    type = "header",
                    order = 0,
                    name = "Announce"
                  },
                  desc = {
                    type = "description",
                    order = 2,
                    name = "Announce Defile targets."
                  },
                  enabled2 = {
                    type = "toggle",
                    name = "Phase 2",
                    width = "half",
                    order = 4
                  },
                  channel2 = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 6,
                    values = channels,
                    disabled = function(info) return not db.options.defile.announce.enabled2 end
                  },
                  message2 = {
                    type = "input",
                    name = "Message",
                    desc = "$p - Player name\n$r - Player's role",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 7
                  },
                  enabled3 = {
                    type = "toggle",
                    name = "Phase 3",
                    width = "half",
                    order = 8
                  },
                  channel3 = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 10,
                    values = channels,
                    disabled = function(info) return not db.options.defile.announce.enabled3 end
                  },
                  message3 = {
                    type = "input",
                    name = "Message",
                    desc = "$p - Player name\n$r - Player's role",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 12
                  },
                  whisper = {
                    type = "toggle",
                    name = "Whisper Target",
                    width = "normal",
                    order = 14
                  },
                  whisperMessage = {
                    type = "input",
                    name = "Message",
                    desc = "$p - Player name",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 60
                  }
                }
              },
              alert = {
                type = "group",
                name = "Alert",
                order = 15,
                args = {
                  header = {
                    type = "header",
                    order = 16,
                    name = "Alert"
                  },
                  desc = {
                    type = "description",
                    order = 18,
                    name = "Flash your screen a certain color when defile is casting"
                  },
                  self = {
                    type = "toggle",
                    name = "On you",
                    width = "normal",
                    order = 20
                  },
                  self_color = {
                    type = "color",
                    name = "Color",
                    width = "half",
                    order = 22,
                    disabled = function(info) return not db.options.defile.alert.self end,
                    hasAlpha = true,
                    get = function(info)
                      local c = db.options.defile.alert.self_color;
                      return c.r, c.g, c.b, c.a;
                    end,
                    set = function(info, r, g, b, a)
                      local c = db.options.defile.alert.self_color;
                      c.r, c.g, c.b, c.a = r, g, b, a;
                    end
                  },
                  self_test = {
                    type = "execute",
                    name = "Test",
                    width = "half",
                    order = 24,
                    disabled = function(info) return not db.options.defile.alert.self end,
                    func = function(info)
                      local c = db.options.defile.alert.self_color;
                      self:FlashScreen(c.r, c.g, c.b, c.a);
                    end
                  },
                  other = {
                    type = "toggle",
                    name = "On others",
                    width = "normal",
                    order = 26
                  },
                  other_color = {
                    type = "color",
                    name = "Color",
                    width = "half",
                    order = 28,
                    disabled = function(info) return not db.options.defile.alert.other end,
                    hasAlpha = true,
                    get = function(info)
                      c = db.options.defile.alert.other_color;
                      return c.r, c.g, c.b, c.a;
                    end,
                    set = function(info, r, g, b, a)
                      c = db.options.defile.alert.other_color;
                      c.r, c.g, c.b, c.a = r, g, b, a;
                    end
                  },
                  other_test = {
                    type = "execute",
                    name = "Test",
                    width = "half",
                    order = 30,
                    disabled = function(info) return not db.options.defile.alert.other end,
                    func = function(info)
                      local c = db.options.defile.alert.other_color;
                      self:FlashScreen(c.r, c.g, c.b, c.a);
                    end
                  }
                }
              }
            }
          },
          heal = {
            type = "group",
            name = "Healing",
            order = 50,
            childGroups = "tree",
            desc = "Options for healing assignment announcements throughout the fight",
            get = function(info) return db.options.heal[info[#info]] end,
            set = function(info, v) db.options.heal[info[#info]] = v end,
            args = {
              disabled_header = {
                type = "header",
                name = "Disabled Announcements",
                order = 0
              },
              disabled_desc = {
                type = "description",
                name = "Announce a healer's assignment if he/she is disabled (see the Assignments tab for healing assignments)",
                order = 2
              },
              channel = {
                type = "select",
                name = "Channel",
                width = "half",
                order = 3,
                values = channels,
                disabled = function(info)
                  return not (db.options.heal.disabled.death or db.options.heal.disabled.leave_raid or db.options.heal.disabled.valkyr or db.options.heal.disabled.harvest_soul);
                end
              },
              message = {
                type = "input",
                name = "Message",
                desc = "$p - Player name\n$o - Reason player is disabled\n$a - Assignment",
                hidden = function(info) return not db.options.general.options.messages end,
                order = 30
              },
              disabled_announce = {
                type = "multiselect",
                name = "Announce on:",
                width = "double",
                order = 40,
                values = db.options.heal.options_list,
                get = function(info, v)
                  return db.options.heal.disabled[v];
                end,
                set = function(info, v)
                  db.options.heal.disabled[v] = not db.options.heal.disabled[v];
                end
              }
            }
          },
          trap = {
            type = "group",
            name = "Traps & Spheres",
            order = 55,
            desc = "Options for Traps and Spheres",
            args = {
              fly = {
                type = "group",
                name = "Announce",
                order = 10,
                inline = true,
                args = {
                  desc = {
                    type = "description",
                    order = 10,
                    name = "Announce when a Shadow Trap or Ice Sphere hits someone"
                  },
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "half",
                    order = 20
                  },
                  channel = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 30,
                    values = channels,
                    disabled = function(info) return not db.options.trap.fly.enabled end
                  },
                  message = {
                    type = "input",
                    name = "Message",
                    desc = "$l - List of players\n$v - (is/are)\n$g - (he/she/it/they)",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 40
                  }
                }
              }
            }
          },
          fail = {
            type = "group",
            name = "Fails",
            order = 55,
            childGroups = "tree",
            desc = "Options for announcements of failures",
            args = {
              raging = {
                type = "group",
                name = "Raging Spirits",
                order = 0,
                inline = true,
                args = {
                  desc = {
                    type = "description",
                    order = 10,
                    name = "Announce when someone pulls aggro on a Raging Spirit (when its target changes from a tank to a non-tank)"
                  },
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "half",
                    order = 20
                  },
                  channel = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 30,
                    values = channels,
                    disabled = function(info) return not db.options.fail.raging.enabled end
                  },
                  message = {
                    type = "input",
                    name = "Message",
                    desc = "$p - Player name",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 40
                  }
                }
              },
              infest = {
                type = "group",
                name = "Infest Cooldowns",
                order = 40,
                inline = true,
                args = {
                  desc = {
                    type = "description",
                    order = 50,
                    name = "Announce when a cooldown on the Infest priority list is used at the wrong time (does not coincide with an Infest)"
                  },
                  enabled = {
                    type = "toggle",
                    name = "Enable",
                    width = "half",
                    order = 60
                  },
                  channel = {
                    type = "select",
                    name = "Channel",
                    width = "half",
                    order = 70,
                    values = channels,
                    disabled = function(info) return not db.options.fail.infest.enabled end
                  },
                  message = {
                    type = "input",
                    name = "Message",
                    desc = "$p - Player name\n$s - Spell name",
                    hidden = function(info) return not db.options.general.options.messages end,
                    order = 80
                  }
                }
              }
            }
          }--[[,
          cooldowns = {
            type = "group",
            name = "Cooldowns",
            order = 60,
            childGroups = "tree",
            desc = "Options for external cooldown announcements",
            args = {
              nyi = {
                type = "header",
                name = "|cFFFF0000Not Yet Implemented|r"
              }
            }
          }]]
        }
      }
    }
  };
  
  for i,v in pairs(db.valks.heal) do
    self:AddHealer(i);
  end

  for i,v in pairs(db.valks.infest) do
    self:AddInfest();
  end
  
  LibStub("AceConfig-3.0"):RegisterOptionsTable("LKAnnounce", options, {"lkannounce", "lka"});
  local aboutFrame = LibStub("LibAboutPanel").new(nil, "LKAnnounce");
  LibStub("AceConfigDialog-3.0"):AddToBlizOptions("LKAnnounce", "Talents Loaded", "LKAnnounce", "roles");
  LibStub("AceConfigDialog-3.0"):AddToBlizOptions("LKAnnounce", "Assignments", "LKAnnounce", "assignments");
  LibStub("AceConfigDialog-3.0"):AddToBlizOptions("LKAnnounce", "Options", "LKAnnounce", "options");
  
  talentLib.RegisterCallback("LKAnnounce", "LibGroupTalents_Update", function(self, GUID, UID, spec, t1, t2, t3)
    LKA:UpdateHeaders();
    LibStub("AceConfigRegistry-3.0"):NotifyChange("LKAnnounce");
  end);
	
	local modelFrame = CreateFrame("FRAME", nil, aboutFrame);
	modelFrame:SetWidth(240);
	modelFrame:SetHeight(240);
	modelFrame:SetPoint("BOTTOM", aboutFrame, "BOTTOM", 0, 10);
	modelFrame:SetBackdrop({
		bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
    edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
    tile = true, tileSize = 16, edgeSize = 16, 
		insets = {
			left = 4,
			right = 4,
			top = 4,
			bottom = 4
		}
	});
	modelFrame:SetBackdropColor(0, 0, 0, 0.8);

	local model = CreateFrame("PLAYERMODEL", nil, modelFrame);
  local model_time = 0;
	modelFrame:SetScript("OnShow", function()
		model:SetPoint("BOTTOMLEFT", modelFrame, "BOTTOMLEFT", 4, 4);
		model:SetPoint("TOPRIGHT", modelFrame, "TOPRIGHT", -4, -4);
		model:SetModel("Creature/Arthaslichking/arthaslichking.m2");
		--model:SetPosition(0, 0, 0);
		--model:SetLight(1, 0, 0, -0.707, -0.707, 0.7, 1.0, 1.0, 1.0, 0.8, 1.0, 1.0, 0.8);
	end);
	modelFrame:SetScript("OnUpdate", function(self, elapsed)
    model_time = model_time + (elapsed * 1000)
    if(model_time > 5000) then
      model_time = 0;
    end
		model:SetSequenceTime(84, model_time)
	end);
  
  if(db.debug) then
    self:debug("Debug messages enabled");
  end
end

do
	local UIDsfromGUID = {};
	local GUIDfromUID = {};
	
	function LKA:SetUID(GUID, UID)
		self:ReleaseUID(UID);
		if not(UIDsfromGUID[GUID]) then
			UIDsfromGUID[GUID] = {};
		end
		UIDsfromGUID[GUID][UID] = true;
		GUIDfromUID[UID] = GUID;
	end
	
	function LKA:ReleaseUID(UID)
		if(GUIDfromUID[UID]) then
			if(UIDsfromGUID[GUIDfromUID[UID]] and UIDsfromGUID[GUIDfromUID[UID]][UID]) then
				UIDsfromGUID[GUIDfromUID[UID]][UID] = nil;
			else
				--If this code is reached, it means there was some kind of coordination error between the two lists
				--This shouldn't ever happen, but it is recoverable
				self:debug("|cFFFF0000Error!|r - Coordination error in target tracking tables");
				--Search through the whole UIDsfromGUID table and remove all instances of UID
				for GUID,UIDs in pairs(UIDsfromGUID) do
					for iUID,v in pairs(UIDs) do
						if(iUID == UID or iUID == UID) then
							UIDs[iUID] = nil;
						end
					end
				end
			end
		end
		GUIDfromUID[UID] = nil;
	end
	
	function LKA:GetUID(GUID)
		if not(UIDsfromGUID[GUID]) then
			return nil;
		end
		--iterate through key/value pairs from the table of UIDs that are registered for this GUID, until a *confirmed* match is found
    --confirming is necessary in case UIDs are not always released correctly (which may actually be close to impossible)
		for returnUID,v in pairs(UIDsfromGUID[GUID]) do
      --check the validity of this entry 
      if(UnitGUID(returnUID) == GUID) then
        return returnUID;
      else
        self:ReleaseUID(returnUID);
      end
    end
    return nil;
	end
end

function LKA:Announce(option, replacements, whisperTarget, special)
  if((option["enabled"..phase] or option.enabled) and db.options.general.options.announce) then
    local channel = option["channel"..phase] or option.channel;
    local message = (special and (option["specialMessage"..phase] or option.specialMessage)) or option["message"..phase] or option.message;
    if(message and replacements) then
      for symbol,str in pairs(replacements) do
        message = message:gsub(symbol, str);
      end
    end
    
    if(channel == "RAID_WARNING" and not IsRaidOfficer()) then
      if(db.options.general.options.downgrade) then
        channel = "RAID";
      else
        self:message("Tried to announce in Raid Warning but couldn't due to lack of assist");
        channel = nil;
      end
    end
    
    if(message and channel) then
      SendChatMessage(message, channel);
    else
      self:debug(((special and "Special ") or "").."Announcement of "..(message or "nothing").." failed on channel "..(channel or "none"));
    end
  end
  
  if((option["whisper"..phase] or option.whisper) and db.options.general.options.announce and not special) then
    local whisperMessage = option["whisperMessage"..phase] or option.whisperMessage;
    if(whisperMessage and replacements) then
      for symbol,str in pairs(replacements) do
        whisperMessage = whisperMessage:gsub(symbol, str);
      end
    end
    
    if(whisperMessage and whisperTarget) then
      SendChatMessage(whisperMessage, "WHISPER", nil, whisperTarget);
    else
      self:debug("Whisper of "..(whisperMessage or "nothing").." to "..(whisperTarget or "none").." failed");
    end
  end
end

function LKA:FlashScreen(r, g, b, a)
  alertFrame.texture:SetAlpha(1);
	alertFrame.texture:SetTexture(r, g, b, a);
  alertFrame.texture:Show();
	alertFrame:Show();
end

function LKA:DefileTimeout()
  if(pending_defile) then
    self:debug("Lich King target change timed out - getting target manually (if this is not the tank, consider this an error!)");
    pending_defile = false;
    local lichking_uid = self:GetUID(lichking_guid);
    if(lichking_uid) then
      self:AnnounceDefile(UnitName(lichking_uid.."target"));
    end
  end
end

function LKA.CooldownReady(key)
  LKA:debug(key.." ready.");
  cooldowns[key] = nil;
end

function LKA.CheckInfestWasted(key)
  local entry = infests.content[key];
  if(entry) then
    local usedTime = GetTime() - entry.duration;
    local now = GetTime();
    if not(usedTime < last_infest and last_infest < now) then
      LKA:debug(key.." used at "..usedTime..", last infest at "..last_infest..", "..now.." now");
      if(db.options.fail.infest.enabled) then
        LKA:Announce(db.options.fail.infest, {["$p"] = entry.playerName, ["$s"] = entry.spellName});
      end
    end
  end
end

function LKA:Get_Assignment(assign, index)
  if(db.valks[assign][index]) then
    return db.valks[assign][index];
  else
    return NONE;
  end
end

function LKA:Set_Assignment(assign, index, value)
  if(value == NONE) then
    db.valks[assign][index] = nil;
  else
    db.valks[assign][index] = value;
    for i,v in pairs(db.valks[assign]) do
      if(i ~= index and v == value) then
        db.valks[assign][i] = nil;
      end
    end
  end
end

function LKA:RemoveRotationSelect(index)
  local lengthenIndex = "stun"..index;
  local name = "stun"..index.."b";
  
  options.args.assignments.args.stuns.args[lengthenIndex].width = "double";
  options.args.assignments.args.stuns.args[name] = nil;
end

function LKA:AddInfest()
  if(num_infests < 9) then
    num_infests = num_infests + 1;
    local num = num_infests;
    
    options.args.assignments.args.infests.args["infest"..num] = {
      type = "select",
      name = "Infest "..num,
      order = num_infests * 10,
      desc = "#"..num.." in the rotation for cooldowns during infests",
      values = function(info) return infests:names("infest", num) end,
      width = "double",
      get = function(info) return self:Get_Assignment("infest", num, infests_list) end,
      set = function(info, v) self:Set_Assignment("infest", num, v) end
    }
    if(num_infests >= 1) then
      options.args.assignments.args.infests.args.remove.disabled = false;
    end
    if(num_infests >= 9) then
      options.args.assignments.args.infests.args.add.disabled = true;
    end
  end
end

function LKA:RemoveInfest()
  if(num_infests > 0) then
    local num = num_infests;
    options.args.assignments.args.infests.args["infest"..num] = nil;
    db.valks.infest[num] = nil;
    num_infests = num_infests - 1;
    if(num_infests < 1) then
      options.args.assignments.args.infests.args.remove.disabled = true;
    end
    if(num_infests < 9) then
      options.args.assignments.args.infests.args.add.disabled = false;
    end
  end
end

function LKA:AnnounceDPS()
  local announce = {{}, {}, {}};
  for i,v in pairs(db.valks.dps) do
    if(dps.content[i]) then
      tinsert(announce[v], i);
    end
  end
  if(#announce[1] > 0) then
    self:printraid(shapes.content[db.valks.shapes[1]].chatName.." "..table.concat(announce[1], ", "));
  else
    self:message("There are no DPS assigned to "..shapes.content[db.valks.shapes[1]].name);
  end
  if(#announce[2] > 0) then
    self:printraid(shapes.content[db.valks.shapes[2]].chatName.." "..table.concat(announce[2], ", "));
  else
    self:message("There are no DPS assigned to "..shapes.content[db.valks.shapes[2]].name);
  end
  if(#announce[3] > 0) then
    self:printraid(shapes.content[db.valks.shapes[3]].chatName.." "..table.concat(announce[3], ", "));
  else
    self:message("There are no DPS assigned to "..shapes.content[db.valks.shapes[3]].name);
  end
end

function LKA:AnnounceInfests()
  local num = 0;
  for i,v in ipairs(db.valks.infest) do
    if(infests.content[v]) then
      num = num + 1;
      break;
    end
  end
  if(num > 0) then
    self:printraid("Infest Cooldowns:");
    for i,v in ipairs(db.valks.infest) do
      if(infests.content[v]) then
        self:printraid("  "..i..": "..v);
      end
    end
  else
    self:message("No Infest cooldowns to announce (old assignments are not announced)");
  end
end

function LKA:AnnounceHealingAssignments()
  local num = 0;
  for i,v in pairs(db.valks.heal) do
    if(UnitInRaid(i)) then
      num = num + 1;
      break;
    end
  end
  if(num > 0) then
    for i,v in pairs(db.valks.heal) do
      if(UnitInRaid(i)) then
        SendChatMessage(i..": "..v);
      end
    end
  else
    self:message("No healing assignments to announce (people not in raid are not announced)");
  end
end
      

function LKA:AddHealer(name)
  if(name and name ~= "") then
    local coloredName = name;
    local _, class = UnitClass(name);
    if(class) then
      coloredName = class_colors[class]..name.."|r";
    end
    
    options.args.assignments.args.heals.args[name] = {
      type = "group",
      childGroups = "tree",
      name = coloredName,
      args = {
        assignment = {
          type = "input",
          order = 10,
          name = "Assignment",
          multiline = true,
          width = "full",
          get = function(info) return db.valks.heal[info[#info-1]] end,
          set = function(info, v) db.valks.heal[info[#info-1]] = v end
        },
        remove = {
          type = "execute",
          order = 20,
          name = "Remove",
          width = "normal",
          func = function(info) self:RemoveHealer(info[#info-1]) end
        }
      }
    };
  end
end

function LKA:RemoveHealer(name)
  db.valks.heal[name] = nil;
  options.args.assignments.args.heals.args[name] = nil;
end

function LKA:Announce_Stuns()
  if(db.options.valkyr.stun.enabled) then
    local mains = "Stuns: ";
    for i=1,3 do
      if(stuns.content[db.valks.stun[i]]) then
        if(rotation_stuns.content[db.valks.stun[i]] and rotation_stuns.content[db.valks.stun[i + 10]]) then
          mains = mains..shapes.content[db.valks.shapes[i]].chatName.." "..rotation_stuns.content[db.valks.stun[i]].playerName.." and "..rotation_stuns.content[db.valks.stun[i + 10]].playerName..", ";
        else
          mains = mains..shapes.content[db.valks.shapes[i]].chatName.." "..stuns.content[db.valks.stun[i]].playerName..", ";
        end
      end
    end
    if(mains ~= "Stuns: ") then
      mains = string.sub(mains, 0, -3) --Cut off the trailing comma
      self:printraid(mains);
    else
      --If there were no stun assignments
      self:message("No stuns to announce (old assignments are not announced)");
    end
    
    local backups = "Backups: ";
    for i=4,6 do
      if(stuns.content[db.valks.stun[i]]) then
        backups = backups..stuns.content[db.valks.stun[i]].playerName..", ";
      end
    end
    if(backups ~= "Backups: ") then
      backups = string.sub(backups, 0, -3) --Cut off the trailing comma
      self:printraid(backups);
    else
      --If there were no backup stun assignments
      self:message("No backup stuns to announce (old assignments are not announced)");
    end
  else
    self:message("Stuns module is disabled");
  end
  
  if(db.options.valkyr.aoestun.enabled) then
    local aoe = "AoE Stuns: ";
    for i=1,3 do
      if(aoe_stuns.content[db.valks.aoestun[i]]) then
        aoe = aoe..aoe_stuns.content[db.valks.aoestun[i]].playerName..", ";
      end
    end
    if(aoe ~= "AoE Stuns: ") then
      mains = string.sub(aoe, 0, -3) --Cut off the trailing comma
      self:printraid(aoe);
    else
      --If there were no aoe stun assignments
      self:message("No AoE stuns to announce (old assignments are not announced)");
    end
  else
    self:message("AoE Stuns module is disabled");
  end
end

function LKA:Announce_Slows()
  if(db.options.valkyr.slow.enabled) then
    local mains = "Slows: ";
    for i=1,3 do
      if(slows.content[db.valks.slow[i]]) then
        mains = mains..shapes.content[db.valks.shapes[i]].chatName.." "..slows.content[db.valks.slow[i]].playerName..", ";
      end
    end
    if(mains ~= "Slows: ") then
      --If there were no stun assignments, don't announce them at all
      mains = string.sub(mains, 0, -3) --Cut off the trailing comma
      self:printraid(mains);
    else
      --If there were no slow assignments
      self:message("No slows to announce (old assignments are not announced)");
    end
    
    local backups = "Backups: ";
    for i=4,6 do
      if(slows.content[db.valks.slow[i]]) then
        backups = backups..slows.content[db.valks.slow[i]].playerName..", ";
      end
    end
    if(backups ~= "Backups: ") then
      --If there were no backup assignments, don't announce them at all
      backups = string.sub(backups, 0, -3) --Cut off the trailing comma
      self:printraid(backups);
    else
      --If there were no back slow assignments
      self:message("No backup slows to announce (old assignments are not announced)");
    end
  else
    self:message("Slows module is disabled");
  end
  
  if(db.options.valkyr.aoeslow.enabled) then
    local aoe = "AoE Slows: ";
    for i=1,3 do
      if(aoe_stuns.content[db.valks.aoeslow[i]]) then
        aoe = aoe..aoe_stuns.content[db.valks.aoeslow[i]].playerName..", ";
      end
    end
    if(aoe ~= "AoE Stuns: ") then
      --If there were no aoe slow assignments, don't announce them at all
      aoe = string.sub(aoe, 0, -3) --Cut off the trailing comma
      self:printraid(aoe);
    else
      --If there were no aoe slow assignments
      self:message("No AoE slows to announce (old assignments are not announced)");
    end
  else
    self:message("AoE Slows module is disabled");
  end
end

function LKA.OnLoad()
  frame = CreateFrame("FRAME", "LKAMessageFrame");
  frame:RegisterEvent("RAID_ROSTER_UPDATE");
  frame:RegisterEvent("PLAYER_REGEN_DISABLED");
  frame:RegisterEvent("ZONE_CHANGED_NEW_AREA");
  frame:RegisterEvent("CHAT_MSG_MONSTER_YELL");
  frame:SetScript("OnEvent", LKA.OnEvent);
  
  LKA:message("Loaded! Type |cFFFF0000/lka config|cFFFFFF00 to open the configuration window.");
end

function LKA.OnEvent(frame, event, ...)
  self = LKA;
  if(event == "COMBAT_LOG_EVENT_UNFILTERED") then
    self:HandleCombatMessage(...);
  elseif(event == "UNIT_TARGET") then
    local unit = select(1,...);
    self:UpdateTarget(unit);
  elseif(event == "PLAYER_REGEN_DISABLED") then
    if(GetZoneText() == ZONE and GetSubZoneText() == SUBZONE and not encounter_enabled) then
      self:StartEncounter();
    end
  elseif(event == "ZONE_CHANGED_NEW_AREA") then
    --Ends the encounter if you change zones
    --This does nothing if the encounter is not already enabled
    self:EndEncounter();
  elseif(event == "RAID_ROSTER_UPDATE") then
    self:UpdateRaidRoster();
  elseif(event == "CHAT_MSG_MONSTER_YELL") then
    local msg = select(1,...);
    if(msg == LKPULL) then
      if(db.options.defile.alert.self or db.options.defile.alert.other or db.options.defile.announce.enabled2 or db.options.defile.announce.enabled3) then
        if(self:GUID_to_NPC(UnitGUID("focus")) ~= N.THE_LICH_KING) then
          self:AdviseFocus();
        end
      end
    end
  end
end

function LKA:UpdateRaidRoster()
  local changed = false;
  --Determine the highest subgroup number that is part of the "raid proper"
  local groupCutoff = 8;
  if(db.options.general.options.ignore_extras) then
    local difficulty = GetRaidDifficulty();
    if(difficulty == 1 or difficulty == 3) then
      groupCutoff = 2;
    elseif(difficulty == 2 or difficulty == 4) then
      groupCutoff = 5;
    end
  end
      
  --Delete options of persons who are no longer in the raid
  for i,v in pairs(options.args.roles.args) do
    if (i ~= "loaded" and i ~= "main" and i ~= "extra") then
      if not(UnitInRaid(i)) then
        self:LeftRaid(i);
        changed = true;
      end
    end
  end
  
  for i=1,GetNumRaidMembers() do
    local name, _, subgroup = GetRaidRosterInfo(i);
    raid_positions[name] = subgroup * 10;
    if(subgroup <= groupCutoff) then 
      if not(options.args.roles.args[name]) then
        self:JoinedRaid(name);
        if not(name) then
          self:debug("Trying to add nobody("..i..")");
        end
        changed = true;
      end
    else
      if(options.args.roles.args[name]) then
        self:LeftRaid(name);
        changed = true;
      end
    end
  end
  if(changed) then
    LibStub("AceConfigRegistry-3.0"):NotifyChange("LKAnnounce");
  end
end

function LKA:AdviseFocus()
  local adviseFrame = CreateFrame("FRAME", nil, UIParent);
  adviseFrame:SetFrameStrata("FULLSCREEN_DIALOG");
  adviseFrame:SetAllPoints(UIParent);
  adviseFrame.text = adviseFrame:CreateFontString(nil, "OVERLAY", "GameFontNormalHuge");
  adviseFrame.text:SetText("Put The Lich King on /focus!");
  adviseFrame.text:SetPoint("CENTER", adviseFrame, "CENTER");
  adviseFrame.elapsed = 0;
  adviseFrame:SetScript("OnUpdate", function(self, elapsed)
    self.elapsed = self.elapsed + elapsed;
    if(self.elapsed > 8) then
      self.text:SetAlpha(5 - (self.elapsed/2));
    end
    if(self.elapsed > 10 or LKA:GUID_to_NPC(UnitGUID("focus")) == N.THE_LICH_KING) then
      self:Hide();
      self:SetParent(nil);
    end
  end);
end

function LKA:InitTargets()
  for i=1,GetNumRaidMembers() do
    local unit = "raid"..i;
    self:UpdateTarget(unit);
  end
end

function LKA:UpdateTarget(unit)
  local GUID = UnitGUID(unit);
  local NPC = self:GUID_to_NPC(GUID);
  local target = ((unit == "player" and "") or unit).."target";
  local targetGUID = UnitGUID(target);
  if(targetGUID) then
    local targetNPC = self:GUID_to_NPC(targetGUID);
    if(targetNPC == N.SHAMBLING_HORROR) then
      --If this horror doesn't have a name, give it a name
      if not(horror_names[targetGUID]) then
        local name = "Shambling Horror "..current_horror;
        current_horror = current_horror + 1;
        horror_names[targetGUID] = name;
      end
      --If this horror does not have a previous target, check horrors for plagues
      --A plague could have snuck onto it while nobody was looking!
      if not(self:GetUID(targetGUID)) then
        self:SetUID(targetGUID, target);
      	self:Calculate_Plagues();
      end
    elseif(targetNPC == N.VALKYR_SHADOWGUARD) then
      --If this valkyr is new, mark it as seen
      if not(valkyr_guids[targetGUID]) then
        valkyr_guids[targetGUID] = true;
        num_valkyr = num_valkyr + 1;
        if(db.options.valkyr.picked.mark) then
          if(GetRaidTargetIndex(target)) then
            self:debug("Val'kyr is already marked with "..GetRaidTargetIndex(target)..", would have marked it with "..db.valks.shapes[(num_valkyr % 3) + 1]);
          else
            self:debug("Marking Val'kyr with "..db.valks.shapes[(num_valkyr % 3) + 1]);
            SetRaidTarget(target, db.valks.shapes[(num_valkyr % 3) + 1]);
          end
        else
          self:debug("Would have marked Val'kyr with "..db.valks.shapes[(num_valkyr % 3) + 1]);
        end
      end
    elseif(targetNPC == N.RAGING_SPIRIT) then
      if not(raging_guids[targetGUID]) then
        raging_guids[targetGUID] = true;
      end
    elseif(targetNPC == N.THE_LICH_KING) then
      lichking_guid = targetGUID;
    end
    
    if(NPC == N.RAGING_SPIRIT) then
      self:debug("RAGING SPIRIT "..UnitName(unit).."("..GUID..") changed target to "..UnitName(target));
      --If this spirit's target changed, and there is a record of who it was targeting before, and its previous target was a tank, and it new target's role is known, and it's new target's role is not a tank
      if(raging_targeting[GUID] and talentLib:GetUnitRole(raging_targeting[GUID]) == "tank" and talentLib:GetUnitRole(target) and talentLib:GetUnitRole(target) ~= "tank") then
        if(db.options.fail.raging.enabled) then
          self:Announce(db.options.fail.raging, {["$p"] = UnitName(target)});
        end
      end
      raging_targeting[GUID] = UnitName(target);
    end
    
    if(NPC == N.THE_LICH_KING) then
      lichking_guid = GUID;
      if(pending_defile) then
        pending_defile = false;
        self:AnnounceDefile(UnitName(target));
      end
    end
    
  	self:SetUID(targetGUID, target);
  else
		--This code is reached when "target" appended to the unit whose target changed is a nonexistant UID
		--Since that UID is no longer valid, release it
    self:ReleaseUID(target);
    self:ReleaseUID(target.."target");
    
    if(raging_targeting[GUID]) then
      raging_targeting[GUID] = nil;
    end
  end
end

function LKA:CheckWipe()
  if not(self:GetUID(lichking_guid)) then
    self:EndEncounter();
  end
end

function LKA:StartEncounter()
  if not(defeated) then
    lichking_guid = "";
    infest_count = 1;
    phase = 1;
    current_horror = 1;
    num_valkyr = 0;
    wipe(horror_names);
    wipe(raging_targeting);
    wipe(valkyr_guids);
    
    self:message("Lich King encounter started!");
    encounter_enabled = true;
    frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
    frame:RegisterEvent("UNIT_TARGET");
    self:InitTargets();
  end
end

function LKA:EndEncounter()
  if(encounter_enabled) then
    self:message("Lich King encounter ended.");
    encounter_enabled = false;
    timer:CancelTimer(infest_timer, true);
    frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
    frame:UnregisterEvent("UNIT_TARGET");
  end
end
  
function LKA:JoinedRaid(name)
  self:AddRole(name);
  self:AddToAssignments(name);
  self:UpdateHeaders();
end

function LKA:LeftRaid(name)
  if(encounter_enabled and db.options.heal.disabled.leave_raid and db.valks.heal[name]) then
    self:AnnounceHealingAssignment(name, "left the raid");
  end
  self:RemoveRole(name);
  self:RemoveFromAssignments(name);
  self:UpdateHeaders();
end

function LKA:UpdateHeaders()
  local num_raid = GetNumRaidMembers();
  local num_main_raid = 0;
  if(num_raid > 0) then
    --Determine the highest subgroup number that is part of the "raid proper"
    local groupCutoff = 8;
    if(db.options.general.options.ignore_extras) then
      local difficulty = GetRaidDifficulty();
      if(difficulty == 1 or difficulty == 3) then
        groupCutoff = 2;
      elseif(difficulty == 2 or difficulty == 4) then
        groupCutoff = 5;
      end
    end
    
    local num_loaded = 0;
    for i=1,num_raid do
      local name, _, subgroup = GetRaidRosterInfo(i);
      if((db.options.general.options.ignore_extras and subgroup <= groupCutoff) or not db.options.general.options.ignore_extras) then
        num_main_raid = num_main_raid + 1;
        if(name and talentLib:GetUnitTalentSpec(name)) then
          self:AddToAssignments(name);
          num_loaded = num_loaded + 1;
        end
      end
    end
    
    local g = (num_loaded/num_main_raid) * 2;
    local r = 2 - g;
    g = (g > 1 and 1) or g;
    r = (r > 1 and 1) or r;
    local color = self:ratio_to_hex_color(r, g, 0);
    loaded_header = "Loaded: |cFF"..color..num_loaded.."/"..num_main_raid.."|r";
  else
    loaded_header = NORAID;
  end
end

function LKA:AddRole(name)
  if(name) then
    _, class = UnitClass(name);
    if(class) then
      local colored_name = class_colors[class]..name.."|r";
    else
      colored_name = name;
    end
    options.args.roles.args[name] = {
      type = "group",
      name = function(info)
        local name = info[#info];
        local coloredName = name;
        local _, class = UnitClass(name);
        if(class) then
          coloredName = class_colors[class]..coloredName.."|r";
          local spec = talentLib:GetUnitTalentSpec(name);
          if(spec) then
            coloredName = possible_roles[class][spec]..coloredName
          end
        end
        return coloredName;
      end,
			desc = "",
			descStyle = "inline",
      order = function(info) return raid_positions[info[#info]]; end,
      args = {
        header = {
          type = "header",
          name = function(info)
            local spec, t1, t2, t3 = talentLib:GetUnitTalentSpec(name);
            if(spec) then
              _, class = UnitClass(name);
              return class_colors[class]..spec.." ("..(t1 or "0").."/"..(t2 or "0").."/"..(t3 or "0")..")|r";
            end
            return "Not Loaded";
          end,
          order = 0
        }
      }
    };
    
    local stuns_list;
    for i,v in pairs(possible_stuns) do
      if(v.require.class == class) then
        if not(stuns_list) then
          stuns_list = {};
        end
        stuns_list[v.spellName] = v.icon.." "..v.spellName;
      end
    end
    if(stuns_list) then
      options.args.roles.args[name].args.stuns = {
        type = "multiselect",
        name = "Stuns",
        values = stuns_list,
        get = function(info, v) return stuns.content[info[#info-1].." - "..v] end,
        set = function(info, v) end,
        disabled = function(info) return not talentLib:GetUnitTalentSpec(name) end,
        order = 10
      };
    end
    
    local aoe_stuns_list;
    for i,v in pairs(possible_aoe_stuns) do
      if(v.require.class == class) then
        if not(aoe_stuns_list) then
          aoe_stuns_list = {};
        end
        aoe_stuns_list[v.spellName] = v.icon.." "..v.spellName;
      end
    end
    if(aoe_stuns_list) then
      options.args.roles.args[name].args.aoe_stuns = {
        type = "multiselect",
        name = "AoE Stuns",
        values = aoe_stuns_list,
        get = function(info, v) return aoe_stuns.content[info[#info-1].." - "..v] end,
        set = function(info, v) end,
        disabled = function(info) return not talentLib:GetUnitTalentSpec(name) end,
        order = 20
      };
    end
    
    local slows_list;
    for i,v in pairs(possible_slows) do
      if(v.require.class == class) then
        if not(slows_list) then
          slows_list = {};
        end
        slows_list[v.spellName] = v.icon.." "..v.spellName;
      end
    end
    if(slows_list) then
      options.args.roles.args[name].args.slows = {
        type = "multiselect",
        name = "Slows",
        values = slows_list,
        get = function(info, v) return slows.content[info[#info-1].." - "..v] end,
        set = function(info, v) end,
        disabled = function(info) return not talentLib:GetUnitTalentSpec(name) end,
        order = 30
      };
    end
    
    local aoe_slows_list;
    for i,v in pairs(possible_aoe_slows) do
      if(v.require.class == class) then
        if not(aoe_slows_list) then
          aoe_slows_list = {};
        end
        aoe_slows_list[v.spellName] = v.icon.." "..v.spellName;
      end
    end
    if(aoe_slows_list) then
      options.args.roles.args[name].args.aoe_slows = {
        type = "multiselect",
        name = "AoE Slows",
        values = aoe_slows_list,
        get = function(info, v) return aoe_slows.content[info[#info-1].." - "..v] end,
        set = function(info, v) end,
        disabled = function(info) return not talentLib:GetUnitTalentSpec(name) end,
        order = 40
      };
    end
    
    local infests_list;
    for i,v in pairs(possible_infests) do
      if(v.require.class == class) then
        if not(infests_list) then
          infests_list = {};
        end
        infests_list[v.spellName] = v.icon.." "..v.spellName;
      end
    end
    if(infests_list) then
      options.args.roles.args[name].args.infests = {
        type = "multiselect",
        name = "Infest Cooldowns",
        values = infests_list,
        get = function(info, v) return infests.content[info[#info-1].." - "..v] end,
        set = function(info, v) end,
        disabled = function(info) return not talentLib:GetUnitTalentSpec(name) end,
        order = 50
      };
    end
  end
end

function LKA:RemoveRole(name)
  options.args.roles.args[name] = nil
end
  
function LKA:AddToAssignments(name)
  self:RemoveFromAssignments(name);
  if(name and UnitClass(name)) then
    local _, class = UnitClass(name);
    local previous;
    local priority;
    local qualify;
    
    priority = 0;
    previous = "";
    for i1,v1 in pairs(possible_stuns) do
      qualify = true;
      for i2,v2 in pairs(v1.require) do
        if(i2 == "class" and qualify) then
          if(class ~= v2) then qualify = false end
        elseif(i2 == "talent" and qualify) then
          local talent = talentLib:UnitHasTalent(name, v2);
          if not(talent) then
            qualify = false;
          end
        elseif(i2 == "spec" and qualify) then
          local spec = talentLib:GetUnitTalentSpec(name);
          if not(spec == v2) then
            qualify = false;
          end
        end
      end
      if(qualify and (v1.priority >= priority)) then
        if(v1.priority > priority and priority ~= 0) then
          rotation_stuns.content[previous] = nil;
          stuns.content[previous] = nil;
        end
        priority = v1.priority;
        previous = name.." - "..v1.spellName
        if(v1.cooldown > 40) then
          rotation_stuns.content[name.." - "..v1.spellName] = {
            name = class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
            inUseName = "*"..class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
            playerName = name,
            cooldown = v1.cooldown,
            spellName = v1.spellName
          }
        end
        stuns.content[name.." - "..v1.spellName] = {
          name = class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          inUseName = "*"..class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          playerName = name,
          cooldown = v1.cooldown,
          spellName = v1.spellName
        }
      end
    end
    
    priority = 0;
    previous = "";
    for i1,v1 in pairs(possible_aoe_stuns) do
      qualify = true;
      for i2,v2 in pairs(v1.require) do
        if(i2 == "class" and qualify) then
          if(class ~= v2) then qualify = false end
        elseif(i2 == "talent" and qualify) then
          local talent = talentLib:UnitHasTalent(name, v2);
          if not(talent) then
            qualify = false;
          end
        elseif(i2 == "spec" and qualify) then
          local spec = talentLib:GetUnitTalentSpec(name);
          if not(spec == v2) then
            qualify = false;
          end
        end
      end
      if(qualify and (v1.priority >= priority)) then
        if(v1.priority > priority and priority ~= 0) then
          aoe_stuns.content[previous] = nil;
        end
        priority = v1.priority;
        previous = name.." - "..v1.spellName;
        aoe_stuns.content[name.." - "..v1.spellName] = {
          name = class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          inUseName = "*"..class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          playerName = name,
          cooldown = v1.cooldown,
          spellName = v1.spellName
        }
      end
    end
    
    priority = 0;
    previous = "";
    for i1,v1 in pairs(possible_slows) do
      qualify = true;
      for i2,v2 in pairs(v1.require) do
        if(i2 == "class" and qualify) then
          if(class ~= v2) then qualify = false end
        elseif(i2 == "talent" and qualify) then
          local talent = talentLib:UnitHasTalent(name, v2);
          if not(talent) then
            qualify = false;
          end
        elseif(i2 == "spec" and qualify) then
          local spec = talentLib:GetUnitTalentSpec(name);
          if not(spec == v2) then
            qualify = false;
          end
        end
      end
      if(qualify and (v1.priority >= priority)) then
        if(v1.priority > priority and priority ~= 0) then
          slows.content[previous] = nil;
        end
        priority = v1.priority;
        previous = name.." - "..v1.spellName;
        slows.content[name.." - "..v1.spellName] = {
          name = class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          inUseName = "*"..class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          playerName = name,
          cooldown = v1.cooldown,
          spellName = v1.spellName
        }
      end
    end
    
    priority = 0;
    previous = "";
    for i1,v1 in pairs(possible_aoe_slows) do
      qualify = true;
      for i2,v2 in pairs(v1.require) do
        if(i2 == "class" and qualify) then
          if(class ~= v2) then qualify = false end
        elseif(i2 == "talent" and qualify) then
          local talent = talentLib:UnitHasTalent(name, v2);
          if not(talent) then
            qualify = false;
          end
        elseif(i2 == "spec" and qualify) then
          local spec = talentLib:GetUnitTalentSpec(name);
          if not(spec == v2) then
            qualify = false;
          end
        end
      end
      if(qualify and (v1.priority >= priority)) then
        if(v1.priority > priority and priority ~= 0) then
          aoe_slows.content[previous] = nil;
        end
        priority = v1.priority;
        previous = name.." - "..v1.spellName;
        aoe_slows.content[name.." - "..v1.spellName] = {
          name = class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          inUseName = "*"..class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          playerName = name,
          cooldown = v1.cooldown,
          spellName = v1.spellName
        }
      end
    end
    
    priority = 0;
    previous = "";
    for i1,v1 in pairs(possible_infests) do
      qualify = true;
      for i2,v2 in pairs(v1.require) do
        if(i2 == "class" and qualify) then
          if(class ~= v2) then qualify = false end
        elseif(i2 == "talent" and qualify) then
          local talent = talentLib:UnitHasTalent(name, v2);
          if not(talent) then
            qualify = false;
          end
        elseif(i2 == "spec" and qualify) then
          local spec = talentLib:GetUnitTalentSpec(name);
          if not(spec == v2) then
            qualify = false;
          end
        end
      end
      if(qualify and (v1.priority >= priority)) then
        if(v1.priority > priority and priority ~= 0) then
          infests[previous] = nil;
        end
        priority = v1.priority;
        previous = name.." - "..v1.spellName;
        infests.content[name.." - "..v1.spellName] = {
          name = class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          inUseName = "*"..class_colors[class]..name.."|r:  "..v1.icon.." "..i1,
          playerName = name,
          cooldown = v1.cooldown,
          duration = v1.duration,
          spellName = v1.spellName
        }
      end
    end
    
    local role = talentLib:GetUnitRole(name);
    if(role == "melee" or role == "caster") then
      dps.content[name] = true;
    end
  end
end

function LKA:RemoveFromAssignments(name)
  if(name) then
    for i,v in pairs(stuns.content) do
      if(v.playerName == name) then
        stuns.content[i] = nil;
      end
    end
    for i,v in pairs(rotation_stuns.content) do
      if(v.playerName == name) then
        rotation_stuns.content[i] = nil;
      end
    end
    for i,v in pairs(aoe_stuns.content) do
      if(v.playerName == name) then
        aoe_stuns.content[i] = nil;
      end
    end
    for i,v in pairs(slows.content) do
      if(v.playerName == name) then
        slows.content[i] = nil;
      end
    end
    for i,v in pairs(aoe_slows.content) do
      if(v.playerName == name) then
        aoe_slows.content[i] = nil;
      end
    end
    for i,v in pairs(infests.content) do
      if(v.playerName == name) then
        infests.content[i] = nil;
      end
    end
    for i,v in pairs(dps.content) do
      if(i == name) then
        dps.content[i] = nil;
      end
    end
  end
end

function LKA:AnnounceDefile(person)
  local role = talentLib:GetUnitRole(person) or "?"
  local ROLE = (talentLib:GetUnitRole(person) and string.upper(talentLib:GetUnitRole(person))) or "?"
  self:Announce(db.options.defile.announce, {["$r"] = role, ["$R"] = ROLE, ["$p"] = person}, person);
	if(person == UnitName("player")) then
		if(db.options.defile.alert.self) then
			local c = db.options.defile.alert.self_color;
			self:FlashScreen(c.r, c.g, c.b, c.a);
		end
	else
		if(db.options.defile.alert.other) then
			local c = db.options.defile.alert.other_color;
			self:FlashScreen(c.r, c.g, c.b, c.a);
		end
	end
end

function LKA.AnnounceInfest(num)
  cooldown = "None available!";
  local entry;
  for i,v in pairs(db.valks.infest) do
    entry = infests.content[v];
    if(entry and UnitIsConnected(entry.playerName) and not(UnitIsDeadOrGhost(entry.playerName) or UnitInVehicle(entry.playerName) or cooldowns[v])) then
      LKA:debug("Cooldown "..v.." chosen for infest "..num);
      cooldown = v;
      break;
    end
  end
  LKA:Announce(db.options.infest.number, {["$n"] = num, ["$p"] = ((entry and entry.playerName) or "No One"), ["$s"] = ((entry and entry.spellName) or "None")}, (entry and entry.playerName));
end

function LKA:AnnounceHealingAssignment(name, occurrence)
  if(db.valks.heal[name]) then
    self:Announce(db.options.heal, {["$p"] = name, ["$o"] = occurrence, ["$a"] = db.valks.heal[name]});
  end
end

function LKA:HandleCombatMessage(timestamp, type, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, ...)
  local sourceNPC, destNPC = self:GUID_to_NPC(sourceGUID), self:GUID_to_NPC(destGUID);
  if(type == "SPELL_AURA_APPLIED") then
    local spellName = select(2, ...);
    if(spellName==S.FRENZY and sourceNPC == N.SHAMBLING_HORROR) then
      self:Announce(db.options.plague.frenzy);
    end
    if(spellName==S.ENRAGE and sourceNPC == N.SHAMBLING_HORROR) then
      self:Announce(db.options.plague.enrage);
    end
  elseif(type == "SPELL_CAST_START") then
    local spellName = select(2, ...);
    if(spellName == S.DEFILE) then
      pending_defile = true;
      timer:ScheduleTimer(self.DefileTimeout, 0.5, self);
    elseif(spellName == S.INFEST) then
      if(infest_count == 1) then
        self.AnnounceInfest(infest_count);
      end
      last_infest = GetTime() + 2;
      infest_count = infest_count + 1;
      infest_timer = timer:ScheduleTimer(self.AnnounceInfest, 17, infest_count);
    elseif(spellName == S.REMORSELESS_WINTER) then
      infest_count = 1;
      if(timer:CancelTimer(infest_timer, true)) then
        infest_timer = nil;
      end
			phase = phase + 1;
    elseif(spellName == S.FURY_OF_FROSTMOURNE) then
      self:EndEncounter();
      defeated = true;
      self:message("You have defeated The Lich King! Congratulations!");
    end
  elseif(type == "SPELL_CAST_SUCCESS") then
    local spellId, spellName, spellSchool = select(1, ...)
    local amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = select(4, ...)
    if(spellName == S.HARVEST_SOUL) then
      self:debug(spellName.." on "..destName);
      if(db.options.heal.disabled.harvest_soul) then
        self:AnnounceHealingAssignment(destName, "has "..spellName);
      end
    else
      if not(sourceName) then
        self:debug("WHA:"..spellName);
      end
      local key = sourceName.." - "..spellName;
      for i,v in pairs(db.valks.stun) do
        if(v == key and stuns.content[v] and stuns.content[v].cooldown ~= 0) then
          self:debug(key.." used.");
          cooldowns[key] = GetTime();
          timer:ScheduleTimer(self.CooldownReady, stuns.content[v].cooldown, key);
        end
      end
      for i,v in pairs(db.valks.aoestun) do
        if(v == key and aoe_stuns.content[v] and aoe_stuns.content[v].cooldown ~= 0) then
          self:debug(key.." used.");
          cooldowns[key] = GetTime();
          timer:ScheduleTimer(self.CooldownReady, aoe_stuns.content[v].cooldown, key);
        end
      end
      for i,v in pairs(db.valks.infest) do
        if(v == key and infests.content[v] and infests.content[v].cooldown ~= 0) then
          self:debug(key.." used.");
          cooldowns[key] = GetTime();
          timer:ScheduleTimer(self.CooldownReady, infests.content[v].cooldown, key);
          if(phase == 1 or phase == 2) then
            self:debug("Infest cooldown "..key.." used in phase "..phase.." - checking if it was wasted");
            timer:ScheduleTimer(self.CheckInfestWasted, infests.content[v].duration, key);
          end
        end
      end
    end
  elseif(type == "SPELL_PERIODIC_DAMAGE") then
    local spellName = select(2, ...)
    if(spellName == S.NECROTIC_PLAGUE) then
      --A plague just damaged someone - give it time to jump, then see if it can be detected - this is only necessary 1 out of every 3 ticks, but it's easier than trying to track when a plague actually kills something
      timer:ScheduleTimer(self.Calculate_Plagues, 0.5, self);
    end
  elseif(type == "SPELL_DISPEL") then
    local extraSpellName = select(5, ...)
    if(extraSpellName == S.NECROTIC_PLAGUE) then
      --A plague was just dispelled off someone - give it time to jump, then see if it can be detected
      timer:ScheduleTimer(self.Calculate_Plagues, 0.5, self);
    end
  elseif(type == "SPELL_DAMAGE") then
    local spellId, spellName, spellSchool = select(1, ...)
    local amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = select(4, ...)
    if(spellName == S.SHADOW_TRAP or spellName == S.ICE_BURST) then
      if(UnitIsPlayer(destName)) then
        local found = false;
        for i,v in ipairs(trap_targets) do
          if(v == destName) then
            found = true;
          end
        end
        if not(found) then
          tinsert(trap_targets, destName);
        end
        timer:CancelTimer(traps_timer, true);
        traps_timer = timer:ScheduleTimer(self.Announce_Trap_Hits, 0.5, self);
      end
    end
  elseif(type == "SPELL_SUMMON") then
    local spellId, spellName, spellSchool = select(1, ...)
    if(spellName == S.DEFILE) then
      pending_defile = false;
    elseif(spellName == S.SUMMON_VALKYR and sourceNPC == N.THE_LICH_KING) then
      timer:ScheduleTimer(self.ValkyrTimeout, 10, self);
      if not(valks_timer and timer:TimeLeft(valks_timer)) then
        wipe(picked);
        num_picked = 0;
        valks_timer = timer:ScheduleRepeatingTimer(self.Scan_Valks, 0.5, self);
      end
    end
  elseif(type == "SPELL_AURA_APPLIED_DOSE") then
    --One plague merged into another plague
    --This refreshes the stack, so the old lockout timer for announcing the plague should be removed
    --self:Calculate_Plagues() does not need to be called because it will be triggered by the combat message that caused the merger plague to jump in the first place
    if(timer:CancelTimer(plague_timers[sourceGUID])) then
      plague_timers[sourceGUID] = nil;
    end
  elseif(type == "UNIT_DIED") then
    --If a Shambling Horror or Drudge Ghoul dies, it's usually from plague, which means the plague is bouncing
    if(destNPC == N.SHAMBLING_HORROR or destNPC == N.DRUDGE_GHOUL) then
      horror_names[destGUID] = nil;
      timer:ScheduleTimer(self.Calculate_Plagues, 0.5, self);
    end
    
    if(destNPC == N.RAGING_SPIRIT) then
      raging_guids[destGUID] = nil;
    end
    
    if(db.options.heal.disabled.death) then
      --AnnounceHealingAssignment doesn't do anything if there is no relevent healing assignment, so this call does not need to be conditional
      self:AnnounceHealingAssignment(destName, "died");
    end
    
    if(UnitInRaid(destName)) then
      timer:ScheduleTimer(self.CheckWipe, 5, self);
    end
  end
end

function LKA:ValkyrTimeout()
  if(timer:CancelTimer(valks_timer, true)) then
    valks_timer = nil;
  end
end

function LKA:Calculate_Plagues()
  --first, construct a list of detectable horrors from the list of people who are targetting horrors - this basically reverses the targeting_horrors table by reversing the keys and values and thereby collapsing repeat values
  local horrors = {};
  for i,v in pairs(horror_names) do
    --don't do any detection for horrors who have already had their current stack of plague announced
    if not(plague_timers[i] and timer:TimeLeft(plague_timers[i])) then
      horrors[i] = self:GetUID(i);
    end
  end
  --now we should have a table where the keys are unique GUIDs (unique horrors) and the values are corresponding UIDs
  
  --check the buffs of every relevant horror
  for GUID,UID in pairs(horrors) do
    local CoE = 1;
    local pTime, pCount, pTicks, pDamage;
    local frenzy = false;
    local name, count, expirationTime;
    for i=1,40 do
      name, _, _, count, _, _, expirationTime = UnitDebuff(UID,i);
      if(name == S.NECROTIC_PLAGUE) then
        pCount = count;
        pTime = expirationTime - GetTime();
        pTicks = math.ceil(pTime/5);
      end
      if(name == S.CURSE_OF_THE_ELEMENTS) then
        CoE = 1.13;
      end
      if(name == S.EARTH_AND_MOON) then
        CoE = 1.13;
      end
      if(name == S.EBON_PLAGUE) then
        CoE = 1.13;
      end
    end
    
    local difficulty = GetRaidDifficulty();
    local _, _, _, _, _, heroic = GetInstanceInfo();
    if(difficulty == 1) then
      pDamage = 50000;
      if(heroic == 1) then
        pDamage = 75000;
        frenzy = true;
      end
    elseif(difficulty == 2) then
      pDamage = 100000;
      if(heroic == 1) then
        pDamage = 150000;
        frenzy = true;
      end
    elseif(difficulty == 3) then
      pDamage = 75000;
      frenzy = true;
    elseif(difficulty == 4) then
      pDamage = 150000;
      frenzy = true;
    end
    
    --pCount will be null if the horror didn't have a plague
    if(pCount) then
      --Mark that this horror's plague has been announced, and set that announcement mark to wear off at the same as the plague will wear off (in pTime seconds)
      plague_timers[GUID] = timer:ScheduleTimer(self.PlagueExpired, pTime, GUID);
      
      local horrorName = horror_names[GUID] or "Shambling Horror "..GUID;
      local diseaseDamage = pCount * pDamage * CoE;
      local health = UnitHealth(UID);
      local maxHealth = UnitHealthMax(UID);
      local frenzyHealth = UnitHealthMax(UID) * "0.20";
      local percent = (health / maxHealth) * 100;
      local ticks = math.ceil(pTime / 5);
      local remainder = pTime % 5;
      
      --self:debug(pCount.." stacks, "..pDamage.." damage per stack("..difficulty.."), "..diseaseDamage.." total");
      
      local message = horrorName.." health: "..self:format_number(health).."("..format("%.1f", percent).."%)";
      local timeTillDeath = 0;
      local timeTillFrenzy = 0;
      for i = 1, ticks do
        local timeUntil = ((i - 1) * 5) + remainder;
        local projectedHealth = health - (diseaseDamage * i);
        if(projectedHealth <= 0) then
          message = message.." - Plague will kill it in "..format("%.1f", timeUntil).." seconds.";
          if(timeTillDeath ~= 0) then
            self:debug("Break error");
          end
          timeTillDeath = timeUntil;
          break;
        end
        if(frenzy and projectedHealth <= frenzyHealth) then
          message = message.." - It will Frenzy in "..format("%.1f", timeUntil).." seconds!";
          timeTillFrenzy = timeUntil;
        end
        if(i == ticks) then
          local projectedPercent = (projectedHealth / maxHealth) * 100;
          message = message.." - Plague will leave it at "..self:format_number(projectedHealth).."("..format("%.1f", projectedPercent).."%)";
        end
      end
      
      if(db.options.plague.track.enabled) then
        local channel = db.options.plague.track.channel;
        if(channel == "RAID_WARNING" and not IsRaidOfficer()) then
          if(db.options.general.options.downgrade) then
            channel = "RAID";
          else
            self:message("Tried to announce in Raid Warning but couldn't due to lack of assist");
            channel = nil;
          end
        end
        if(channel) then
          if(db.options.plague.track.death_frenzy_only) then
            if(frenzy) then
              if(timeTillFrenzy > 0) then
                if(timeTillDeath == 0) then
                  SendChatMessage(horrorName.." Frenzy in "..format("%.1f", timeTillFrenzy).."s! Plague will not kill it!", channel);
                elseif(timeTillDeath - timeTillFrenzy > 0) then
                  SendChatMessage(horrorName.." Frenzy in "..format("%.1f", timeTillFrenzy).."s! Plague will kill it "..format("%.0f", (timeTillDeath - timeTillFrenzy)).."s later.", channel);
                else
                  self:debug("Plague calculation error");
                end
              else
                self:debug("Plague tracked successfully but not announced because no death or Frenzy would occur");
              end
            else
              if(timeTillDeath ~= 0) then
                SendChatMessage("Plague will kill "..horrorName.." in "..format("%.0f", (timeTillDeath - timeTillFrenzy)).."s.", channel);
              else
                self:debug("Plague tracked successfully but not announced because no death would occur");
              end
            end
          else
            SendChatMessage(message, channel);
          end
        end
      end
    end
  end
end

function LKA.PlagueExpired(GUID)
  LKA:debug(GUID.."'s plague wore off; it is now eligible for announcements again");
  plague_timers[GUID] = nil;
end

function LKA:Announce_Trap_Hits()
  local targets_string = table.concat(trap_targets, ", ", 1, #trap_targets-1);
  targets_string = targets_string..((#trap_targets > 1 and " and ") or "")..trap_targets[#trap_targets];

  local r = {["$l"] = targets_string};
  r["$v"] = (#trap_targets > 1 and "are") or "is";
  local g = UnitSex(targets_string);
  r["$g"] = (#trap_targets > 1 and "they") or (g == 2 and "he") or (g == 3 and "she") or "it";

  self:Announce(db.options.trap.fly, r)
  wipe(trap_targets);
end

function LKA:Scan_Valks()
  for i=1,GetNumRaidMembers() do
    local UID = "raid"..i;
    local name = UnitName(UID);
    if(UnitInVehicle(UID) and not picked[name]) then
      self:debug(name.." picked up!");
      local announced = false;
      if(db.valks.heal[name]) then
        if(db.options.heal.disabled.valkyr) then
          self:AnnounceHealingAssignment(name, "picked up");
          announced = true;
        end
      end
      if not(announced) then
        self:debug("Should announce Val'kyr on "..name);
        self:Announce(db.options.valkyr.picked, {["$p"] = name});
      end
      num_picked = num_picked + 1;
      picked[name] = "picked up";
    end
  end
  
  local numValks = 1;
  local diff = GetInstanceDifficulty();
  if(diff == 2 or diff == 4) then
    numValks = 3;
  end
  
  if(num_picked >= numValks) then
    if(timer:CancelTimer(valks_timer)) then
      valks_timer = nil;
    end
    self:Handle_Valks();
  end
end

function LKA:Handle_Valks()
  self:debug("Handling valks");
  --Add disabled players to the picked list
  for i=1,GetNumRaidMembers() do
    local UID = "raid"..i;
    if(UnitIsDeadOrGhost(UID)) then
      picked[UnitName(UID)] = "dead";
    end
    if not(UnitIsConnected(UID)) then
      picked[UnitName(UID)] = "offline";
    end
    for ragingGUID,_ in pairs(raging_guids) do
      ragingUID = self:GetUID(ragingGUID);
      if(ragingUID and UnitName(ragingUID.."target") == UnitName(UID)) then
        picked[UnitName(UID)] = "tanking Raging Spirit";
      end
    end
  end
  
  --first figure out who is available for back ups
  local backupstun = {};
  for i=4,6 do
    v = stuns.content[db.valks.stun[i]];
    if(v) then
      if not(UnitIsDeadOrGhost(v.playerName) or picked[v.playerName] or cooldowns[db.valks.stun[i]]) then
        tinsert(backupstun, v.playerName);
      end
    end
  end
  
  local backupslow = {};
  for i=4,6 do
    v = slows.content[db.valks.slow[i]];
    if(v) then
      if not(UnitIsDeadOrGhost(v.playerName) or picked[v.playerName] or cooldowns[db.valks.slow[i]]) then
        tinsert(backupslow, v.playerName);
      end
    end
  end
  
  --assign people to aoe assignments
  if(db.options.valkyr.aoestun.enabled) then
    local aoestun = nil;
    local num_aoe = 0;
    for i,v in pairs(db.valks.aoestun) do
      aoestun = aoe_stuns.content[v];
      if(aoestun) then
        num_aoe = num_aoe + 1;
        if not(UnitIsDeadOrGhost(aoestun.playerName) or picked[aoestun.playerName] or cooldowns[v]) then
          --elligible to stun (in raid, not dead, not picked, stun not on cooldown)
          break;
        else
          aoestun = nil;
        end
      end
    end
    if(aoestun) then
      if(num_aoe == 1) then
        --The AoE stunner picked was the first one
        self:Announce(db.options.valkyr.aoestun.main, {["$p"] = aoestun.playerName}, aoestun.playerName);
      else
        self:Announce(db.options.valkyr.aoestun.backup, {["$p"] = aoestun.playerName}, aoestun.playerName);
      end
    else
      self:Announce(db.options.valkyr.aoestun.backup, {}, nil, true);
    end
  end
  
  if(db.options.valkyr.aoeslow.enabled) then
    local aoeslow = nil;
    local num_aoe = 0;
    for i,v in pairs(db.valks.aoeslow) do
      aoeslow = aoe_slows.content[v];
      if(aoeslow) then
        num_aoe = num_aoe + 1;
        if not(UnitIsDeadOrGhost(aoeslow.playerName) or picked[aoeslow.playerName] or cooldowns[v]) then
          --elligible to stun (in raid, not dead, not picked, stun not on cooldown)
          break;
        else
          aoeslow = nil;
        end
      end
    end
    if(aoeslow) then
      if(num_aoe == 1) then
        --The AoE slower picked was the first one
        self:Announce(db.options.valkyr.aoeslow.main, {["$p"] = aoeslow.playerName}, aoeslow.playerName);
      else
        self:Announce(db.options.valkyr.aoeslow.backup, {["$p"] = aoeslow.playerName}, aoeslow.playerName);
      end
    else
      self:Announce(db.options.valkyr.aoeslow.backup, {}, nil, true);
    end
  end
  
  --now check for picked people for each assignment and assign suitable backups
  if(db.options.valkyr.stun.enabled) then
    for i=1,3 do
      v = stuns.content[db.valks.stun[i]];
      if(v and (picked[v.playerName] or cooldowns[db.valks.stun[i]])) then
        --this will only occur when someone in the picked list has the same name as someone in the assigned stuns list
        i1 = db.valks.stun[i + 10];
        v1 = stuns.content[i1];
        local i2, v2;
        local has_backup = false;
        if(v1) then
          v2 = v1.playerName;
          has_backup = true;
        end
        if not(v1 and not(picked[v1.playerName] or cooldowns[i1])) then
          --This could either occur if v1 doesn't exist, or if v1 is ineligible due to being picked, etc.
          if(#backupstun > 0) then
            --find the first element in the backupstun table
            for i3, v3 in pairs(backupstun) do
              has_backup = true;
              i2, v2 = i3, v3;
              break;
            end
          end
        end
        if(has_backup) then
          self:Announce(db.options.valkyr.stun.backup, {["$d"] = v.playerName, ["$o"] = (picked[v.playerName] or "on cooldown"), ["$p"] = v2, ["$x"] = shapes.content[db.valks.shapes[i]].chatName}, v2);
          --this backup stun is no longer available on this round of valkyr
          if(i2) then
            self:debug(backupstun[i2].." stun used, no longer valid for this pick");
          else
            self:debug("Rotation stun used, backup not affected");
          end
          tremove(backupstun, i2);
        else
          --no stuns available
          self:Announce(db.options.valkyr.stun.backup, {["$d"] = v.playerName, ["$o"] = (picked[v.playerName] or "on cooldown"), ["$x"] = shapes.content[db.valks.shapes[i]].chatName}, nil, true);
        end
      elseif(v) then
        --the Main assignment is available for this stun
        self:Announce(db.options.valkyr.stun.main, {["$p"] = v.playerName, ["$x"] = shapes.content[db.valks.shapes[i]].chatName}, v.playerName);
      end
    end
  end
  
  if(db.options.valkyr.slow.enabled) then
    for i=1,3 do
      v = slows.content[db.valks.slow[i]];
      if(v and (picked[v.playerName] or cooldowns[db.valks.slow[i]])) then
        --this will only occur when someone in the picked list has the same name as someone in the assigned slows list
        i1 = db.valks.slow[i + 10];
        v1 = slows.content[i1];
        local i2, v2;
        local has_backup = false;
        if(v1) then
          v2 = v1.playerName;
          has_backup = true;
        end
        if not(v1 and not(picked[v1.playerName] or cooldowns[i1])) then
          --This could either occur if v1 doesn't exist, or if v1 is ineligible due to being picked, etc.
          if(#backupslow > 0) then
            --find the first element in the backupslow table
            for i3, v3 in pairs(backupslow) do
              has_backup = true;
              i2, v2 = i3, v3;
              break;
            end
          end
        end
        if(has_backup) then
          self:Announce(db.options.valkyr.slow.backup, {["$d"] = v.playerName, ["$o"] = (picked[v.playerName] or "on cooldown"), ["$p"] = v2, ["$x"] = shapes.content[db.valks.shapes[i]].chatName}, v2);
          --this backup stun is no longer available on this round of valkyr
          self:debug(backupslow[i2].." slow used, no longer valid for this pick");
          tremove(backupslow, i2);
        else
          --no slows available
          self:Announce(db.options.valkyr.slow.backup, {["$d"] = v.playerName, ["$o"] = (picked[v.playerName] or "on cooldown"), ["$x"] = shapes.content[db.valks.shapes[i]].chatName}, nil, true);
        end
      elseif(v) then
        --the Main assignment is available for this slow
        self:Announce(db.options.valkyr.slow.main, {["$p"] = v.playerName, ["$x"] = shapes.content[db.valks.shapes[i]].chatName}, v.playerName);
      end
    end
  end
  
  wipe(picked);
end

function LKA:GetDefaultAssignments()
  return {
    aoestun = {},
    aoeslow = {},
    stun = {},
    slow = {},
    dps = {},
    infest = {},
    heal = {},
    shapes = {1, 2, 3}
  };
end

function LKA:DefaultAssignments()
  db.valks = self:GetDefaultAssignments();
end

function LKA:GetDefaultOptions()
  return {
    general = {
      options = {
        messages = false,
        downgrade = true,
        announce = true,
        ignore_extras = true
      }
    },
    trap = {
      fly = {
        enabled = true,
        message = "$l $v like a bi-ird, $g'll only fly awa-ay...",
        channel = "YELL"
      }
    },
    plague = {
      track = {
        enabled = true,
        channel = "RAID_WARNING",
        death_frenzy_only = true
      },
      enrage = {
        enabled = true,
        message = "Shambling Horror used Enrage - Stun/Tranq/Cooldown now!",
        channel = "RAID"
      },
      frenzy = {
        enabled = true,
        message = "{skull}Shambling Horror Frenzy - Cooldowns on add tank!{skull}",
        channel = "RAID_WARNING"
      }
    },
    infest = {
      number = {
        enabled1 = true,
        message1 = "Infest #$n coming up!",
        channel1 = "RAID_WARNING",
        whisper1 = false,
        whisperMessage1 = "Use $s for next Infest",
        enabled2 = true,
        message2 = "Infest #$n coming up! - Cooldown: $s - $p",
        channel2 = "RAID_WARNING",
        whisper2 = true,
        whisperMessage2 = "Use $s for next Infest"
      }
    },
    valkyr = {
      picked = {
        enabled = false,
        message = "$p picked up!",
        channel = "RAID",
        mark = false
      },
      stun = {
        enabled = true,
        backup = {
          enabled = true,
          message = "$d is $o! $p needs to stun $x!",
          channel = "RAID",
          whisper = true,
          whisperMessage = "Stun $x!",
          specialMessage = "No stun available for $x!"
        },
        main = {
          enabled = false,
          message = "$p needs to stun $x!",
          channel = "RAID",
          whisper = false,
          whisperMessage = "Stun $x!"
        }
      },
      aoestun = {
        enabled = true,
        backup = {
          enabled = true,
          message = "$p needs to AoE stun!",
          channel = "RAID",
          whisper = true,
          whisperMessage = "AoE stun!",
          specialMessage = "No AoE stun!"
        },
        main = {
          enabled = false,
          message = "$p needs to AoE stun!",
          channel = "RAID",
          whisper = false,
          whisperMessage = "AoE stun!"
        }
      },
      slow = {
        enabled = true,
        backup = {
          enabled = true,
          message = "$d is $o! $p needs to slow $x!",
          channel = "RAID",
          whisper = true,
          whisperMessage = "Slow $x!",
          specialMessage = "No slow available for $x!"
        },
        main = {
          enabled = false,
          message = "$p needs to slow $x!",
          channel = "RAID",
          whisper = false,
          whisperMessage = "Slow $x!"
        }
      },
      aoeslow = {
        enabled = true,
        backup = {
          enabled = true,
          message = "$p needs to AoE slow!",
          channel = "RAID",
          whisper = true,
          whisperMessage = "AoE slow!",
          specialMessage = "No AoE slow!"
        },
        main = {
          enabled = false,
          message = "$p needs to AoE slow!",
          channel = "RAID",
          whisper = false,
          whisperMessage = "AoE slow!"
        }
      }
    },
    defile = {
      announce = {
        enabled2 = true,
        message2 = "{skull}Defile on $r $p{skull}",
        channel2 = "RAID",
        enabled3 = true,
        message3 = "{skull}Defile on $r $p{skull}",
        channel3 = "RAID",
        whisper = true,
        whisperMessage = "{skull}DEFILE ON YOU!{skull}"
      },
      alert = {
        self = true,
        self_color = {r = 1, g = 0, b = 0, a = 0.3},
        other = false,
        other_color = {r = 0, g = 1, b = 0, a = 0.3}
      }
    },
    heal = {
      enabled = true,
      channel = "RAID",
      message = "$p $o! Backup assignment: $a",
      options_list = {
        death = "Death",
        leave_raid = "Leave Raid",
        valkyr = "Valkyr (phase 2)",
        harvest_soul = "Harvest Soul (phase 3)"
      },
      disabled = {
        death = true,
        leave_raid = true,
        valkyr = true,
        harvest_soul = true
      }
    },
    fail = {
      raging = {
        enabled = true,
        message = "$p pulled aggro on a Raging Spirit!",
        channel = "RAID"
      },
      infest = {
        enabled = true,
        message = "$p used $s but did it not coincide with an infest",
        channel = "RAID"
      }
    }
  }
end

function LKA:DefaultOptions()
  db.options = self:GetDefaultOptions();
end

local variables_loaded_frame = CreateFrame("FRAME");
variables_loaded_frame:RegisterEvent("VARIABLES_LOADED");
variables_loaded_frame:SetScript("OnEvent", LKA.Initialize);

LKA:OnLoad();