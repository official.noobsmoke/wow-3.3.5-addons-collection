﻿LKA = {};

LKA.ZOOMED_ICON = ":16:16:0:0:64:64:4:60:4:60";
local ZOOMED_ICON = LKA.ZOOMED_ICON;
LKA.ZOOMED_ICON_SMALL = ":12:12:0:0:64:64:4:60:4:60";
local ZOOMED_ICON_SMALL = LKA.ZOOMED_ICON_SMALL;
LKA.RAID_TARGET = ":14:14:0:0:64:64";
local RAID_TARGET = LKA.RAID_TARGET;

LKA.ASSIGNMENT_HELP = [[|cFF8080FFVal'kyr Assignments|r

Some possible stuns/slows/infests will not be available until talents are loaded.

Backup and aoe stuns and slows, and infest cooldowns, are assigned in a priority - the first assignment is chosen unless he/she is disabled, in which case the second assignment is chosen, etc.

Single target stuns can be assigned in rotation - if the main assignment is longer than 40s cooldown, the second assignment will always be assigned to backup. Leaving the second slot blank will cause a backup to be picked from the regular backups list]]

LKA.channel_colors = {
  RAID = "FFFFFF",
  RAID_WARNING = "FFFFFF",
  YELL = "FFFFFF",
  PARTY = "FFFFFF",
  SAY = "FFFFFF",
  GUILD = "FFFFFF"
}
LKA.channels = {
  RAID = "Raid",
  RAID_WARNING = "RW",
  YELL = "Yell",
  PARTY = "Party",
  SAY = "Say",
  GUILD = "Guild"
}

LKA.class_colors = {
  ["DEATHKNIGHT"] = "|cFFC41F3B",
  ["DRUID"] = "|cFFFF7D0A",
  ["HUNTER"] = "|cFFABD473",
  ["MAGE"] = "|cFF69CCF0",
  ["PALADIN"] = "|cFFF58CBA",
  ["PRIEST"] = "|cFFFFFFFF",
  ["ROGUE"] = "|cFFFFF569",
  ["SHAMAN"] = "|cFF2459FF",
  ["WARLOCK"] = "|cFF9482C9",
  ["WARRIOR"] = "|cFFC79C6E",
  gray = "|cFFD0D0D0"
}
local class_colors = LKA.class_colors;

LKA.getNames = function(self, assignment, index)
  local ret = {};
  for i,v in pairs(self.content) do
    ret[i] = v.name;
  end
  
  --Mark all elements in use by others in the assignment as used
  for i,v in pairs(LKAnnounceSaved.valks[assignment]) do
    if(self.content[v] and i ~= index) then
      ret[v] = self.content[v].inUseName;
    end
  end
  
  --If the given index's assignment is not in the content list (that assignment is offline or incorrectly specced), insert it but mark it as disabled
  if (LKAnnounceSaved.valks[assignment][index] and not self.content[LKAnnounceSaved.valks[assignment][index]]) then
    ret[LKAnnounceSaved.valks[assignment][index]] = class_colors.gray..LKAnnounceSaved.valks[assignment][index];
  end
  
  return ret;
end

local NONE = " none";
local NONE_DISPLAY = "|cFFFF0000None";

LKA.stuns = {
  names = LKA.getNames,
  content = {
    [NONE] = {
      name = NONE_DISPLAY,
      inUseName = NONE_DISPLAY
    }
  }
};
LKA.rotation_stuns = {
  names = LKA.getNames,
  content = {
    [NONE] = {
      name = NONE_DISPLAY,
      inUseName = NONE_DISPLAY
    }
  }
};
LKA.slows = {
  names = LKA.getNames,
  content = {
    [NONE] = {
      name = NONE_DISPLAY,
      inUseName = NONE_DISPLAY
    }
  }
};
LKA.aoe_stuns = {
  names = LKA.getNames,
  content = {
    [NONE] = {
      name = NONE_DISPLAY,
      inUseName = NONE_DISPLAY
    }
  }
};
LKA.aoe_slows = {
  names = LKA.getNames,
  content = {
    [NONE] = {
      name = NONE_DISPLAY,
      inUseName = NONE_DISPLAY
    }
  }
};
LKA.infests = {
  names = LKA.getNames,
  content = {
    [NONE] = {
      name = NONE_DISPLAY,
      inUseName = NONE_DISPLAY
    }
  }
};

LKA.channel_colors = {
  RAID = "FFFFFF",
  RAID_WARNING = "FFFFFF",
  YELL = "FFFFFF",
  PARTY = "FFFFFF",
  SAY = "FFFFFF",
  GUILD = "FFFFFF"
};

LKA.channels = {
  RAID = "Raid",
  RAID_WARNING = "cRW",
  YELL = "Yell",
  PARTY = "Party",
  SAY = "Say",
  GUILD = "Guild"
};

LKA.class_colors = {
  ["DEATHKNIGHT"] = "|cFFC41F3B",
  ["DRUID"] = "|cFFFF7D0A",
  ["HUNTER"] = "|cFFABD473",
  ["MAGE"] = "|cFF69CCF0",
  ["PALADIN"] = "|cFFF58CBA",
  ["PRIEST"] = "|cFFFFFFFF",
  ["ROGUE"] = "|cFFFFF569",
  ["SHAMAN"] = "|cFF2459FF",
  ["WARLOCK"] = "|cFF9482C9",
  ["WARRIOR"] = "|cFFC79C6E",
  gray = "|cFFD0D0D0"
};

LKA.shapes = {
  names = function(self)
    local ret = {};
    for i,v in pairs(self.content) do
      ret[i] = v.name;
    end
    return ret;
  end,
  content = {
    [0] = {
      name = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t Unknown",
      texture = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
      chatName = "?"
    },
    {
      name = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_1"..RAID_TARGET.."|t Star",
      texture = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_1"..RAID_TARGET.."|t",
      chatName = "{star}"
    },
    {
      name = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_2"..RAID_TARGET.."|t Circle",
      texture = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_2"..RAID_TARGET.."|t",
      chatName = "{circle}"
    },
    {
      name = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_3"..RAID_TARGET.."|t Diamond",
      texture = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_3"..RAID_TARGET.."|t",
      chatName = "{diamond}"
    },
    {
      name = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_4"..RAID_TARGET.."|t Triangle",
      texture = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_4"..RAID_TARGET.."|t",
      chatName = "{triangle}"
    },
    {
      name = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_5"..RAID_TARGET.."|t Moon",
      texture = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_5"..RAID_TARGET.."|t",
      chatName = "{moon}"
    },
    {
      name = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_6"..RAID_TARGET.."|t Square",
      texture = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_6"..RAID_TARGET.."|t",
      chatName = "{square}"
    },
    {
      name = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_7"..RAID_TARGET.."|t Cross",
      texture = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_7"..RAID_TARGET.."|t",
      chatName = "{cross}"
    },
    {
      name = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_8"..RAID_TARGET.."|t Skull",
      texture = "|TInterface\\TARGETINGFRAME\\UI-RaidTargetingIcon_8"..RAID_TARGET.."|t",
      chatName = "{skull}"
    }
  }
};
local shapes = LKA.shapes;

LKA.shapes_sets = {
  names = function(self)
    local ret = {};
    for i,v in pairs(self.content) do
      ret[i] = v.name;
    end
    return ret;
  end,
  content = {
    --[[BigWigs = {
      modName = "Big Wigs",
      name = "Big Wigs:  "..shapes.content[0].texture.." "..shapes.content[0].texture.." "..shapes.content[0].texture,
      marks = {0, 0, 0}
    }, --BigWigs does not currently support marking Val'kyr]]
    DBM = {
      modName = "Deadly Boss Mods",
      name = "DBM:  "..shapes.content[4].texture.." "..shapes.content[3].texture.." "..shapes.content[2].texture,
      marks = {4, 3, 2}
    },
    DXE = {
      shortName = "Deus Vox Encounters",
      name = "DXE:  "..shapes.content[5].texture.." "..shapes.content[6].texture.." "..shapes.content[7].texture,
      marks = {5, 6, 7}
    },
    LKAnnounce = {
      name = "LKAnnounce:  "..shapes.content[1].texture.." "..shapes.content[2].texture.." "..shapes.content[3].texture,
      shortName = "LKAnnounce",
      marks = {1, 2, 3}
    },
    RaidWatch = {
      shortName = "Raid Watch 2",
      name = "Raid Watch 2:  "..shapes.content[4].texture.." "..shapes.content[5].texture.." "..shapes.content[3].texture,
      marks = {4, 5, 6}
    }
  }
}

LKA.possible_roles = {
  ["DEATHKNIGHT"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Frost"] = "|TInterface\\Icons\\Spell_DeathKnight_FrostPresence"..ZOOMED_ICON.."|t",
    ["Blood"] = "|TInterface\\Icons\\Spell_DeathKnight_BloodPresence"..ZOOMED_ICON.."|t",
    ["Unholy"] = "|TInterface\\Icons\\Spell_DeathKnight_UnholyPresence"..ZOOMED_ICON.."|t"
  },
  ["DRUID"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Balance"] = "|TInterface\\Icons\\Spell_Nature_StarFall"..ZOOMED_ICON.."|t",
    --["Feral Combat"] = "|TInterface\\Icons\\Ability_Racial_BearForm"..ZOOMED_ICON.."|t",
    ["Feral Combat"] = "|TInterface\\Icons\\Ability_Druid_CatForm"..ZOOMED_ICON.."|t",
    ["Restoration"] = "|TInterface\\Icons\\Spell_Nature_HealingTouch"..ZOOMED_ICON.."|t"
  },
  ["HUNTER"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Beast Mastery"] = "|TInterface\\Icons\\Ability_Hunter_BeastTaming"..ZOOMED_ICON.."|t",
    ["Survival"] = "|TInterface\\Icons\\Ability_Hunter_SwiftStrike"..ZOOMED_ICON.."|t",
    ["Marksmanship"] = "|TInterface\\Icons\\Ability_Marksmanship"..ZOOMED_ICON.."|t"
  },
  ["MAGE"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Frost"] = "|TInterface\\Icons\\spell_frost_frostbolt02"..ZOOMED_ICON.."|t",
    ["Fire"] = "|TInterface\\Icons\\spell_fire_flamebolt"..ZOOMED_ICON.."|t",
    ["Arcane"] = "|TInterface\\Icons\\spell_holy_magicalsentry"..ZOOMED_ICON.."|t"
  },
  ["PALADIN"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Holy"] = "|TInterface\\Icons\\spell_holy_holybolt"..ZOOMED_ICON.."|t",
    ["Protection"] = "|TInterface\\Icons\\spell_holy_devotionaura"..ZOOMED_ICON.."|t",
    ["Retribution"] = "|TInterface\\Icons\\spell_holy_auraoflight"..ZOOMED_ICON.."|t"
  },
  ["PRIEST"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Discipline"] = "|TInterface\\Icons\\spell_holy_wordfortitude"..ZOOMED_ICON.."|t",
    ["Holy"] = "|TInterface\\Icons\\spell_holy_guardianspirit"..ZOOMED_ICON.."|t",
    ["Shadow"] = "|TInterface\\Icons\\spell_shadow_shadowwordpain"..ZOOMED_ICON.."|t"
  },
  ["ROGUE"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Assassination"] = "|TInterface\\Icons\\ability_rogue_eviscerate"..ZOOMED_ICON.."|t",
    ["Combat"] = "|TInterface\\Icons\\ability_backstab"..ZOOMED_ICON.."|t",
    ["Subtlety"] = "|TInterface\\Icons\\ability_stealth"..ZOOMED_ICON.."|t"
  },
  ["SHAMAN"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Elemental"] = "|TInterface\\Icons\\spell_nature_lightning"..ZOOMED_ICON.."|t",
    ["Enhancement"] = "|TInterface\\Icons\\spell_nature_lightningshield"..ZOOMED_ICON.."|t",
    ["Restoration"] = "|TInterface\\Icons\\spell_nature_magicimmunity"..ZOOMED_ICON.."|t"
  },
  ["WARLOCK"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Affliction"] = "|TInterface\\Icons\\spell_shadow_deathcoil"..ZOOMED_ICON.."|t",
    ["Demonology"] = "|TInterface\\Icons\\spell_shadow_metamorphosis"..ZOOMED_ICON.."|t",
    ["Destruction"] = "|TInterface\\Icons\\spell_shadow_rainoffire"..ZOOMED_ICON.."|t"
  },
  ["WARRIOR"] = {
    ["Unknown"] = "|TInterface\\Icons\\INV_Misc_QuestionMark"..ZOOMED_ICON.."|t",
    ["Arms"] = "|TInterface\\Icons\\ability_rogue_eviscerate"..ZOOMED_ICON.."|t",
    ["Fury"] = "|TInterface\\Icons\\Ability_Warrior_InnerRage"..ZOOMED_ICON.."|t",
    ["Protection"] = "|TInterface\\Icons\\inv_shield_06"..ZOOMED_ICON.."|t"
  }
}

LKA.possible_stuns = {
  ["Improved HoJ - 6s"] = {
    require = {
      class = "PALADIN",
      talent = "Improved Hammer of Justice"
    },
    icon = "|TInterface\\Icons\\spell_holy_sealofmight"..ZOOMED_ICON_SMALL.."|t",
    priority = 2,
    cooldown = 40,
    spellName = "Improved HoJ",
    realSpellName = "Hammer of Justice"
  },
  ["HoJ - 6s"] = {
    require = {
      class = "PALADIN"
    },
    icon = "|TInterface\\Icons\\spell_holy_sealofmight"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    cooldown = 60,
    spellName = "Hammer of Justice"
  },
  ["Maim - 6s"] = {
    require = {
      class = "DRUID",
      spec = "Feral Combat"
    },
    icon = "|TInterface\\Icons\\ability_druid_mangle"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    cooldown = 0,
    spellName = "Maim"
  },
  ["Kidney Shot - 6s"] = {
    require = {
      class = "ROGUE"
    },
    icon = "|TInterface\\Icons\\ability_rogue_kidneyshot"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    cooldown = 0,
    spellName = "Kidney Shot"
  },
  ["Concussion Blow - 5s"] = {
    require = {
      class = "WARRIOR",
      talent = "Concussion Blow"
    },
    icon = "|TInterface\\Icons\\ability_thunderbolt"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    cooldown = 30,
    spellName = "Concussion Blow"
  },
  ["Deep Freeze - 5s"] = {
    require = {
      class = "MAGE",
      talent = "Deep Freeze"
    },
    icon = "|TInterface\\Icons\\ability_mage_deepfreeze"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    cooldown = 30,
    spellName = "Deep Freeze"
  },
  ["Bash - 5s"] = {
    require = {
      class = "DRUID",
      talent = "Brutal Impact"
    },
    icon = "|TInterface\\Icons\\ability_druid_bash"..ZOOMED_ICON_SMALL.."|t",
    priority = 2,
    cooldown = 30,
    spellName = "Bash"
  },
  --[[["Bash - 4s"] = {
    class = "DRUID",
    talent = {
      ["Feral(Tank)"] = true
    },
    icon = "|TInterface\\Icons\\ability_druid_bash"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    rotation = true
  },]]
  ["Gnaw - 3s"] = {
    require = {
      class = "DEATHKNIGHT",
      talent = "Master of Ghouls"
    },
    icon = "|TInterface\\Icons\\spell_deathknight_gnaw_ghoul"..ZOOMED_ICON_SMALL.."|t",
    rotation = true,
    priority = 1,
    cooldown = 60,
    spellName = "Gnaw"
  },
  ["Intimidation - 3s"] = {
    require = {
      class = "HUNTER",
      talent = "Intimidation"
    },
    icon = "|TInterface\\Icons\\ability_devour"..ZOOMED_ICON_SMALL.."|t",
    rotation = true,
    priority = 1,
    cooldown = 60,
    spellName = "Intimidation"
  }
}

LKA.possible_slows = {
  ["Chains of Ice"] = {
    require = {
      class = "DEATHKNIGHT"
    },
    icon = "|TInterface\\Icons\\spell_frost_chainsofice"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Chains of Ice"
  },
  ["Infected Wounds"] = {
    require = {
      class = "DRUID",
      talent = "Infected Wounds"
    },
    icon = "|TInterface\\Icons\\ability_druid_infectedwound"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Infected Wounds"
  },
  ["Wing Clip"] = {
    require = {
      class = "HUNTER"
    },
    icon = "|TInterface\\Icons\\ability_rogue_trip"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Wing Clip"
  },
  ["Frostbolt"] = {
    require = {
      class = "MAGE"
    },
    icon = "|TInterface\\Icons\\spell_frost_frostbolt02"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Frostbolt"
  },
  ["Frostfire Bolt"] = {
    require = {
      class = "MAGE",
      spec = "Fire"
    },
    icon = "|TInterface\\Icons\\ability_mage_frostfirebolt"..ZOOMED_ICON_SMALL.."|t",
    priority = 2,
    spellName = "Frostfire Bolt"
  },
  ["Slow"] = {
    require = {
      class = "MAGE",
      talent = "Slow"
    },
    icon = "|TInterface\\Icons\\spell_nature_slow"..ZOOMED_ICON_SMALL.."|t",
    priority = 3,
    spellName = "Slow"
  },
  ["Mind Flay"] = {
    require = {
      class = "PRIEST",
      spec = "Shadow"
    },
    icon = "|TInterface\\Icons\\spell_shadow_siphonmana"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Mind Flay"
  },
  ["Crippling Poison"] = {
    require = {
      class = "ROGUE"
    },
    icon = "|TInterface\\Icons\\ability_poisonsting"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Crippling Poison"
  },
  ["Frost Shock"] = {
    require = {
      class = "SHAMAN"
    },
    icon = "|TInterface\\Icons\\spell_frost_frostshock"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Frost Shock"
  },
  ["Curse of Exhaustion (30%)"] = {
    require = {
      class = "WARLOCK",
      talent = "Curse of Exhaustion"
    },
    icon = "|TInterface\\Icons\\spell_shadow_grimward"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Curse of Exhaustion"
  },
  ["Hamstring"] = {
    require = {
      class = "WARRIOR"
    },
    icon = "|TInterface\\Icons\\ability_shockwave"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Hamstring"
  }
}

LKA.possible_aoe_stuns = {
  ["Shockwave - 4s"] = {
    require = {
      class = "WARRIOR",
      talent = "Shockwave"
    },
    icon = "|TInterface\\Icons\\ability_warrior_shockwave"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    cooldown = 20,
    spellName = "Shockwave"
  },
  ["Holy Wrath - 3s"] = {
    require = {
      class = "PALADIN"
    },
    icon = "|TInterface\\Icons\\spell_holy_excorcism"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    cooldown = 30,
    spellName = "Holy Wrath"
  },
  ["Shadowfury - 3s"] = {
    require = {
      class = "WARLOCK",
      talent = "Shadowfury"
    },
    icon = "|TInterface\\Icons\\spell_shadow_shadowfury"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    cooldown = 20,
    spellName = "Shadowfury"
  }
}

LKA.possible_aoe_slows = {
  --[[["Piercing Howl"] = {
    require = {
      class = "WARRIOR",
      talent = "Piercing Howl"
    },
    icon = "|TInterface\\Icons\\spell_shadow_deathscream"..ZOOMED_ICON_SMALL.."|t",
    priority = 1
  }, --Does Piercing Howl work on Val'kyr?]]
  ["Frost Trap"] = {
    require = {
      class = "HUNTER"
    },
    icon = "|TInterface\\Icons\\spell_frost_freezingbreath"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Frost Trap"
  },
  ["Chilblains"] = {
    require = {
      class = "DEATHKNIGHT",
      talent = "Chilblains"
    },
    icon = "|TInterface\\Icons\\spell_frost_wisp"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Chilbains",
    realSpellName = "Pestilence"
  },
  ["Cone of Cold"] = {
    require = {
      class = "MAGE"
    },
    icon = "|TInterface\\Icons\\spell_frost_glacier"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Cone of Cold"
  },
  ["Earthbind Totem"] = {
    require = {
      class = "SHAMAN"
    },
    icon = "|TInterface\\Icons\\spell_nature_strengthofearthtotem02"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Earthbind Totem"
  },
  ["Desecration"] = {
    require = {
      class = "DEATHKNIGHT",
      talent = "Desecration"
    },
    icon = "|TInterface\\Icons\\spell_shadow_shadowfiend"..ZOOMED_ICON_SMALL.."|t",
    priority = 1,
    spellName = "Desecration",
    realSpellName = "Plague Strike"
  }
}

LKA.possible_infests = {
  ["Aura Mastery - 2m"] = {
    require = {
      class = "PALADIN",
      talent = "Aura Mastery"
    },
    icon = "|TInterface\\Icons\\spell_holy_auramastery"..ZOOMED_ICON_SMALL.."|t",
    cooldown = 120,
    vehicle = true,
    priority = 1,
    duration = 6,
    spellName = "Aura Mastery"
  },
  ["Divine Sacrifice - 2m"] = {
    require = {
      class = "PALADIN",
      talent = "Divine Sacrifice"
    },
    icon = "|TInterface\\Icons\\spell_holy_powerwordbarrier"..ZOOMED_ICON_SMALL.."|t",
    cooldown = 120,
    priority = 1,
    duration = 6,
    spellName = "Divine Sacrifice"
  },
  ["Divine Hymn - 6m"] = {
    require = {
      class = "PRIEST"
    },
    icon = "|TInterface\\Icons\\spell_holy_divinehymn"..ZOOMED_ICON_SMALL.."|t",
    cooldown = 360,
    priority = 1,
    duration = 8,
    spellName = "Divine Hymn"
  }
}