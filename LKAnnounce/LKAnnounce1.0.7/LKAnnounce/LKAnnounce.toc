## Interface: 30300
## Title: LK|cFFFF4040Announce
## Author: Mirrored
## Version: 1.0b7
## Notes: Handles the assignment, coordination, and announcement of many parts of the Lich King encounter in Icecrown Citadel.
## eMail: mirror@uga.edu
## X-Author-Server: Dragonmaw US
## X-Author-Faction: Horde
## X-Email: mirror@uga.edu
## X-Category: Raid
## DefaultState: Enabled
## LoadOnDemand: 0
## X-Curse-Packaged-Version: 1.0
## X-Curse-Project-Name: LKAnnounce
## X-Curse-Project-ID: lkannounce
## X-Curse-Repository-ID: wow/lkannounce/mainline

## OptionalDeps: Ace3, LibAboutPanel, LibDataBroker1.1, LibTalentQuery-1.0, LibGroupTalents-1.0
## SavedVariables: LKAnnounceSaved
embeds.xml
Data.lua
LKAnnounce.lua
