--[[
EV's Mod Library by EVmaker, v0.14:

After getting tired of copying often used functions (such as printing to chat) over and over after each mod I work on,
I decided to move all the most commonly used functions into a general library which I can have as a dependency for
each of my mods and simply have them all use it instead of duplicating the code for every mod.

Support for Levelator by SJones321
]]--

-- Declares the locals
local EML_Version = "0.15";
local debugMode = false;
local EML_supportedMods = {
	["Looter"] = true,
	["UsefulExtras"] = true,
	["XPGain"] = true,
	["SkillsPayBills"] = true,
	["MailWarning"] = true,
	["EasyShards"] = true,
	["PVPTimer"] = true,
	["LoreMaster"] = true,
	["SimpleViperWarning"] = true,
	["HealthPercents"] = true,
	["Inviter"] = true,
	["QuickMountGather"] = true,
	["QuickThreatDisplay"] = true,
	["Levelator"] = true,
	["EVTestMod"] = true
}
local EML_Colors = {
	["red"] = {143, 10, 13},
	["blue"] = {10, 12, 150},
	["cyan"] = {16, 211, 255},
	["teal"] = {0, 150, 89},
	["green"] = {20, 150, 10},
	["grassgreen"] = {50,150,50},
	["darkgreen"] = {5,107,0},
	["yellow"] = {255, 255, 0},
	["white"] = {255, 255, 255},
	["black"] = {0, 0, 0},
	["junk"] = {91, 91, 91},
	["common"] = {255, 255, 255},
	["uncommon"] = {10, 186, 17},
	["rare"] = {0, 88, 220},
	["epic"] = {93, 0, 204},
	["legendary"] = {225, 129, 0}
}
local EML_availColors = {
	[1] = "red",
	[2] = "blue",
	[3] = "cyan",
	[4] = "teal",
	[5] = "green",
	[6] = "grassgreen",
	[7] = "yellow",
	[8] = "white",
	[9] = "black",
	[10] = "junk",
	[11] = "common",
	[12] = "uncommon",
	[13] = "rare",
	[14] = "epic",
	[15] = "legendary"
}
local EML_HTMLColors = {
	["orange"] = "|cFFFF8000",
	["purple"] = "|cFFA335EE",
	["brightblue"] = "|cFF0070DE",
	["brightgreen"] = "|cFF1EFF00",
	["white"] = "|cFFFFFFFF",
	["close"] = "|r"
}

function EML_OnLoad()
	SLASH_EML1 = "/eml"
	SlashCmdList["EML"] = function(msg)
		EML_SlashHandler(msg);
	end
end

function EML_OnEvent(event)
	-- Nothing yet
end

-- Handles the slash commands
function EML_SlashHandler(theMsg)
	msg = strtrim(strlower(theMsg or ""))
	if msg == "version" then
		EMLChat("EML version: "..EML_Version,"chat","EML")
	elseif strmatch(msg, "list") then
		local theChoice = strmatch(msg, "list (.*)")
		if (theChoice) then
			if (theChoice == "supportedmods") then
				EML_Display(EML_supportedMods,"EML")
			end
		else
			EMLChat("List options: supportedmods","chat","EML")
		end
	elseif msg == "debug" then
		if (debugMode) then
			debugMode = false;
			EMLChat("Debug mode off","chat","EML")
		else
			debugMode = true;
			EMLChat("Debug mode on","chat","EML")
		end
	elseif msg == "build" then
		EML_buildInfo("info","EML")
	elseif msg == "zone" then
		EML_zoneInfo("info","EML")
	elseif msg == "test1" then
		EML_testFunction()
	else
		EMLChat("EV's Mod Library Menu","chat","EML")
		EMLChat("-------------------","chat","EML")
		EMLChat("version, (displays version information)","chat","EML")
		EMLChat("build, (displays WoW build information)","chat","EML")
		EMLChat("zone, (displays current zone information)","chat","EML")
		EMLChat("list, (choose what to list)","chat","EML")
	end
end

-- Prints to chat with the appropriate mods message
function EMLChat(msg,place,from,colors)
	local valR, valG, valB;
	if (type(colors) == "table") then
		valR, valG, valB = EML_splitTable(colors);
	elseif (EML_findTable(colors,EML_Colors)) then
		valR,valG,valB = EML_splitTable(EML_getTable(colors,EML_Colors));
	else
		valR, valG, valB = EML_splitTable(EML_Colors.yellow);
	end
	local isSupported = EML_findTable(from,EML_supportedMods);
	if (place == "chat") and (not isSupported) then
		if DEFAULT_CHAT_FRAME then
			DEFAULT_CHAT_FRAME:AddMessage("<EML> "..msg, valR, valG, valB)
		end
	end
	if (place == "error") and (not isSupported) then
		UIErrorsFrame:AddMessage("<EML> "..msg, valR, valG, valB)
	end
	if (place == "error") and (isSupported) then
		UIErrorsFrame:AddMessage("<"..from.."> "..msg, valR, valG, valB, 1, 10)
	end
	if (place == "chat") and (isSupported) then
		if DEFAULT_CHAT_FRAME then
			DEFAULT_CHAT_FRAME:AddMessage("<"..from.."> "..msg, valR, valG, valB)
		end
	end
	if (place == "raid") and (isSuppported) then
		RaidNotice_AddMessage(RaidWarningFrame, msg, {r=valR, g=valG, b=valB});
	end
	if place == "plainchat" then
		if DEFAULT_CHAT_FRAME then
			DEFAULT_CHAT_FRAME:AddMessage(msg, valR, valG, valB)
		end
	end
	if place == "plainerror" then
		UIErrorsFrame:AddMessage(msg, valR, valG, valB, 1, 10)
	end
	if (from == nil) and (place ~= "plainchat" and place ~= "plainerror") then
		if DEFAULT_CHAT_FRAME then
			DEFAULT_CHAT_FRAME:AddMessage("<EML> The command you just tried to use wasn't updated to use EV's Mod Library, please leave him a comment on the WoWI page of the mod this command was from.", 1, 1, 0)
		end
	end
end

-- Sends messages to different channels
function EML_sendChannel(what,where)
	local partyArray = EML_partyCheck();
	if (where == "party") then
		if (partyArray["Party"]) then
			SendChatMessage(what, "PARTY", nil);
		end
	elseif (where == "raid") then
		if (partyArray["Raid"]) then
			SendChatMessage(what, "RAID", nil);
		end
	elseif (where == "guild") then
		if (partyArray["Guild"]) then
			SendChatMessage(what, "GUILD", nil);
		end
	elseif (where == "all") then
		if (partyArray["Party"]) then
			SendChatMessage(what, "PARTY", nil);
		end
		if (partyArray["Guild"]) then
			SendChatMessage(what, "GUILD", nil);
		end
		if (partyArray["Raid"]) then
			SendChatMessage(what, "RAID", nil);
		end
	else
		SendChatMessage(what, "WHISPER", nil, where);
	end
end

--- Returns if the player is in a guild, party or raid
function EML_partyCheck()
	local hasParty = false;
	local hasGuild = false;
	local hasRaid = false;
	local hasPartyArray = {};
	if (GetNumPartyMembers > 0) then
		hasParty = true;
	end
	if (GetNumGuildMembers(true) > 0) then
		hasGuild = true;
	end
	if (GetNumRaidMembers > 0) then
		hasRaid = true;
	end
	hasPartyArray = {
		["Party"] = hasParty,
		["Guild"] = hasGuild,
		["Raid"] = hasRaid,
	}
	return hasPartyArray;
end

-- Grabs the name from a item link
function EML_Parser(link)
	local itemName,itemID;
	itemName = tostring(strmatch(link, "%[(.*)%]"));
	itemID = tonumber(strmatch(link, "item:(%d+)"));
	return itemName,itemID;
end

-- Gets the value of an item by name, id or link
function EML_getPrice(link)
	local itemPrice;
	if (link) then
		_, _, _, _, _, _, _, _, _, _, itemPrice = GetItemInfo(link)
	end
	return itemPrice;
end

-- Gets item tooltip information
function EML_toolInfo(arg,from)
	local itemLink;
	local itemArray;
	if (GameTooltip:IsShown()) then
		local name;
		name, itemLink = GameTooltip:GetItem();
	elseif (ItemRefTooltip:IsShown()) then
		local name;
		name, itemLink = ItemRefTooltip:GetItem();
	end
	if (itemLink) then
		itemArray = EML_getInfo(itemLink);
	end
	if (itemLink) then
		if (arg == "info") then
			EMLChat(tostring(itemLink)..", ID: "..itemArray["ID"]..", Stack Size: "..itemArray["StackCount"],"chat",from)
		else
			return itemArray;
		end
	else
		EMLChat("ID Failed.","chat",from)
		return nil;
	end
end

-- Returns GetItemInfo in a easily accessible table
function EML_getInfo(arg)
	local itemArray;
	local itemName, itemLink, itemRarity, itemLevel, itemMinLevel, itemType, itemSubType, itemStackCount, itemEquipLoc, itemTexture, itemPrice = GetItemInfo(arg)
	local itemCount = GetItemCount(arg);
	if (itemName) then
		local _,itemID = EML_Parser(itemLink);
		itemArray = {
		["Name"] = itemName,
		["ID"] = itemID,
		["Link"] = itemLink,
		["Rarity"] = itemRarity,
		["ILevel"] = itemLevel,
		["MinLevel"] = itemMinLevel,
		["Type"] = itemType,
		["SubType"] = itemSubType,
		["ItemCount"] = itemCount,
		["StackCount"] = itemStackCount,
		["EquipLoc"] = itemEquipLoc,
		["Texture"] = itemTexture,
		["Price"] = itemPrice
		}
	end
	return itemArray;
end

-- Adds a value to a table
function EML_addTable(what,where)
	local theValue = tostring(what);
	if (type(where) == "table") then
		for index,value in pairs(where) do
			if (type(index) == "number") then
				table.insert(where,what)
				return true;
			elseif (type(index) == "string") then
				where[theValue] = true;
				return true;
			end
		end
		table.insert(where,what)
		return true;
	else
		if (debugMode) then EMLChat("Not given a table.","chat","EML","red") end
		return nil;
	end
	if (theValue == "new") then
		EMLChat("Something hasn't been updated to use the new table add yet.","chat","EML","red")
	end
end

-- Removes a value from a table
function EML_remTable(what,where)
	local removedVal = false;
	if (type(where) == "table") then
		for index,value in pairs(where) do
			if (value == what) then
				table.remove(where,index)
				removedVal = true;
			end
		end
		for index,value in pairs(where) do
			if (index == what) then
				where[index]=nil;
				removedVal = true;
			end
		end
	else
		if (debugMode) then EMLChat("RemTable Not given a table.","chat","EML","red") end
	end
	return removedVal;
end

-- Clears an array (table)
function EML_clearTable(arg)
	arg = table.wipe(arg);
end

-- Copies a table for safe resetting or other uses
function EML_copyTable(arg)
	local newTable;
	if (type(arg) == "table") then
		newTable = {};
		for index,value in pairs(arg) do
			newTable[index] = value;
		end
	end
	return newTable;
end

-- Returns if a value is found in a table
function EML_findTable(what,where)
	local isFound = false;
	if (type(where) == "table") then
		for index,value in pairs(where) do
			if (value == what) then
				isFound=true;
			elseif (index == what) then
				isFound=true;
			end
		end
		for index,value in pairs(where) do
			if (type(value) == "table") then
				for index2,value2 in pairs(value) do
					if (value2 == what) then
						isFound = true;
					elseif (index2 == what) then
						isFound=true;
					end
				end
			end
		end
	else
		if (debugMode) then EMLChat("No table for FindTable","chat","EML") end
		return nil;
	end
	return isFound;
end

-- Returns the value in a table
function EML_getTable(what,where)
	if (type(where) == "table") then
		for index,value in pairs(where) do
			if (what == index) then
				return value;
			elseif (what == value) then
				return index;
			elseif (type(value) == "table") then
				for index2,value2 in pairs(value) do
					if (what == index2) then
						return value2;
					elseif (what == value2) then
						return index2;
					end
				end
			end
		end
	else
		if (debugMode) then EMLChat("No table for GetTable","chat","EML") end
		return nil;
	end
end

-- Makes sure settings are in one array from another array
function EML_mergeTable(arrayTo,arrayFrom)
	local newArray;
	if (type(arrayTo) ~= "table") or (type(arrayFrom) ~= "table") then
		return nil;
	else
		newArray = EML_copyTable(arrayTo);
	end
	for index,value in pairs(arrayFrom) do
		local isThere = EML_findTable(index,newArray);
		if (not isThere) then
			newArray[index]=value;
		end
	end
	return newArray;
end

-- Returns if a table is an empty table or not
function EML_isEmpty(arg)
	local isEmpty = true;
	if (type(arg) == "table") then
		if (#(arg) < 1) then
			for index,value in pairs(arg) do
				if (index ~= nil) then
					isEmpty = false;
				end
			end
		else
			isEmpty = false;
		end
	else
		if (arg) then
			isEmpty = false;
		end
	end
	return isEmpty;
end

-- Display arrays or variables
function EML_Display(arg,from)
    local displayVar;
    if (type(arg) == "table") then
		if (EML_isEmpty(arg)) then
			EMLChat("Table is empty","chat",from)
		end
        for index,value in pairs(arg) do
            if type(value) == "boolean" then
                if value then
                    value = "true"
                else
                    value = "false"
                end
            end
			if type(value) == "table" then
				for index2,value2 in pairs(value) do
					if type(value2) == "boolean" then
						if value2 then
							value2 = "true"
						else
							value2 = "false"
						end
					end
					if (value2 ~= nil and value2 ~= "" and value2 ~= " ") then
						value2 = tostring(value2)
					end
					if (value2 == nil or value2 == "" or value2 == " ") then
						value2 = "nil"
					end
					EMLChat("Item is: "..value2,"chat",from)
				end
			end
            if (value ~= "StartDontRemove") and (type(value) ~= "table") and (not debugMode) then
                EMLChat("Item "..index.." is: "..value,"chat",from)
            elseif (debugMode) then
                EMLChat("Item "..index.." is: "..value,"chat",from)
            end
        end
    else
--    if (type(arg) == "string" or type(arg) == "boolean" or type(arg) == "number") or (arg == nil) then
        if (type(arg) == "boolean") then
            if (arg) then
                displayVar = "true"
            else
                displayVar = "false"
            end
        elseif (type(arg) == "number") then
            displayVar = tostring(arg)
        elseif (arg == nil) then
            displayVar = "nil"
		else
            displayVar = arg
        end
        EMLChat("Item is: "..displayVar,"chat",from)
    end
end

-- Displays a normal table in exact order
function EML_displayNormal(arg,from)
	if (type(arg) == "table") then
		if (#(arg) > 0) then
			for i=1,#(arg) do
				local tempVar = tostring(arg[i]);
				EMLChat(i..": "..tempVar,"chat",from)
			end
		end
	end
end

-- Extracts an integrated array
function EML_splitTwo(what)
	local val1,val2;
	if (type(what) == "table") then
		for index,value in pairs(what) do
			if (index == 1) then
				val1 = value;
			elseif (index == 2) then
				val2 = value;
			end
		end
	else
		if (debugMode) then EMLChat("SplitTwo not given a table.","chat","EML","red") end
	end
	return val1,val2;
end

-- Splits a RGB table into WoW values
function EML_splitTable(arg)
	local val1, val2, val3;
	if (type(arg) == "table") then
		for index,value in pairs(arg) do
			if (index == 1) then
				val1 = tonumber(value/255);
			elseif (index == 2) then
				val2 = tonumber(value/255);
			elseif (index == 3) then
				val3 = tonumber(value/255);
			end
		end
	end
	return val1, val2, val3;
end

-- Splits a RGB table and returns the exact values
function EML_splitWoWTable(arg)
	local val1, val2, val3;
	if (type(arg) == "table") then
		for index,value in pairs(arg) do
			if (index == 1) then
				val1 = tonumber(value);
			elseif (index == 2) then
				val2 = tonumber(value);
			elseif (index == 3) then
				val3 = tonumber(value);
			end
		end
	end
	return val1, val2, val3;
end

-- Returns the value of the RGB color requested
function EML_getColor(arg)
	local theColor;
	if (arg) then
		if (EML_findTable(arg,EML_Colors)) then
			theColor=EML_getTable(arg,EML_Colors);
		end
	end
	return theColor;
end

-- Lists availible colors in EML
function EML_listColors(from)
	EMLChat("Availible Colors:","chat",from,"teal")
	EML_displayNormal(EML_availColors,from)
	EMLChat("----","plainchat",from,"teal")
end

-- EML frame toggler
function EMLFrame_Toggle(place,what)
	if (type(place) == "string") then
		local frame = getglobal(place)
		if (frame) then
			if (what == nil) then
				if (frame:IsVisible()) then
					frame:Hide()
					return "hide";
				else
					frame:Show()
					return "show";
				end
				return nil;
			else
				if (what == "open") then
					frame:Show()
					return "show";
				else
					frame:Hide()
					return "hide";
				end
			end
		end
	else
		if (debugMode) then EMLChat("Frame error","chat","EML","red") end
	end
end

-- Generates random numbers
function EML_randGen(minnum,maxnum)
	local randNum;
	local minNum = tonumber(minnum);
	local maxNum = tonumber(maxnum);
	if (minNum) then
		randNum=random(minNum,maxNum);
	else
		randNum=random(1,100);
	end
	return randNum;
end

-- Prints or returns the current WoW build information
function EML_buildInfo(what,where)
	local version, build, thedate, tocversion = GetBuildInfo()
	local buildInfo = {
		["Version"] = version,
		["Build"] = build,
		["Date"] = thedate,
		["TOC"] = tocversion,
	}
	if (debugMode) or (what == "info") then EMLChat("WoW Version: "..version..", Build: "..build..", TOC Version: "..tocversion,"chat",where) end
	return buildInfo;
end

-- Prints or returns the current WoW zone information
function EML_zoneInfo(what,where)
	local zoneName = tostring(GetZoneText());
	local subZoneName = tostring(GetSubZoneText());
	if (debugMode) or (what == "info") then EMLChat("Zone: "..zoneName..", SubZone: "..subZoneName,"chat",where) end
	return zoneName, subZoneName;
end

-- Gets bag information
function EML_bagInfo(where)
	local bagsName = {};
	local bagsType = {};
	local bagsSlots = {};
	local bagsFree = {};
	for i=0, 4 do
		local bagName = GetBagName(i);
		local bagFree, bagType = GetContainerNumFreeSlots(i);
		local numSlots = GetContainerNumSlots(i);
		if (bagName) then
			bagsName[i]=tostring(bagName);
			bagsType[i]=tonumber(bagType);
			bagsSlots[i]=tonumber(numSlots);
			bagsFree[i]=tonumber(bagFree);
		end
	end
	if (debugMode) then
		for i=0, #(bagsName) do
			EMLChat("Bag Name: "..tostring(bagsName[i])..", Bag Type: "..tostring(bagsType[i])..", Bag Slots: "..tostring(bagsSlots[i])..", Free Slots: "..tostring(bagsFree[i]),"chat",where)
		end
	end
	return bagsName,bagsType,bagsSlots,bagsFree;
end

function EML_itemInfo(bagID,slotID)
	local slotLink = GetContainerItemLink(bagID,slotID);
	local itemName, itemNum;
	if (slotLink) then itemName, itemNum = EML_Parser(slotLink); end
	return itemName,itemNum;
end

-- Provides easy handling of colors and returning of color values from the color picker
function EML_colorPicker(arg,from)
	local tempR,tempG,tempB;
	local r,g,b,shortR,shortG,shortB;
	local theColor = {};
	if arg == "reset" then
		ColorPickerFrame:SetColorRGB(1,0,0)
	end
	if arg == "open" then
		ColorPickerFrame.func = EML_colorPicker
		ColorPickerFrame:Show()
	else
		r, g, b = ColorPickerFrame:GetColorRGB();
		tempR=math.floor(r*255);
		tempG=math.floor(g*255);
		tempB=math.floor(b*255);
		shortR,shortG,shortB = (strsub(r,1,4)),(strsub(g,1,4)),(strsub(b,1,4));
		theColor={tempR,tempG,tempB};
	end
	if (arg == "get") and (tempR and tempG and tempB) then
		EMLChat("Red: "..tempR..", Green: "..tempG..", Blue: "..tempB,"chat",from,theColor)
	end
	if (arg == "getreal") and (r and g and b) then
		EMLChat("Red: "..shortR..", Green: "..shortG..", Blue: "..shortB,"chat",from,theColor)
	end
	return theColor;
end

-- Test function
function EML_testFunction()
	-- Nothing yet
end
-- End of code