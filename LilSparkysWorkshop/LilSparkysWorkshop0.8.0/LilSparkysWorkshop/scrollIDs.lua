
-- enchant scroll IDs

LSW.scrollData = {
[27951] = { scrollID = 37603, vellumID = 37602}, -- Enchant Boots - Dexterity
[7418] = { scrollID = 38679, vellumID = 38682}, -- Enchant Bracer - Minor Health
[7420] = { scrollID = 38766, vellumID = 38682}, -- Enchant Chest - Minor Health
[7426] = { scrollID = 38767, vellumID = 38682}, -- Enchant Chest - Minor Absorption
[7428] = { scrollID = 38768, vellumID = 38682}, -- Enchant Bracer - Minor Deflection
[7443] = { scrollID = 38769, vellumID = 38682}, -- Enchant Chest - Minor Mana
[7454] = { scrollID = 38770, vellumID = 38682}, -- Enchant Cloak - Minor Resistance
[7457] = { scrollID = 38771, vellumID = 38682}, -- Enchant Bracer - Minor Stamina
[7745] = { scrollID = 38772, vellumID = 39349}, -- Enchant 2H Weapon - Minor Impact
[7748] = { scrollID = 38773, vellumID = 38682}, -- Enchant Chest - Lesser Health
[7766] = { scrollID = 38774, vellumID = 38682}, -- Enchant Bracer - Minor Spirit
[7771] = { scrollID = 38775, vellumID = 38682}, -- Enchant Cloak - Minor Protection
[7776] = { scrollID = 38776, vellumID = 38682}, -- Enchant Chest - Lesser Mana
[7779] = { scrollID = 38777, vellumID = 38682}, -- Enchant Bracer - Minor Agility
[7782] = { scrollID = 38778, vellumID = 38682}, -- Enchant Bracer - Minor Strength
[7786] = { scrollID = 38779, vellumID = 39349}, -- Enchant Weapon - Minor Beastslayer
[7788] = { scrollID = 38780, vellumID = 39349}, -- Enchant Weapon - Minor Striking
[7793] = { scrollID = 38781, vellumID = 39349}, -- Enchant 2H Weapon - Lesser Intellect
[7857] = { scrollID = 38782, vellumID = 38682}, -- Enchant Chest - Health
[7859] = { scrollID = 38783, vellumID = 38682}, -- Enchant Bracer - Lesser Spirit
[7861] = { scrollID = 38784, vellumID = 38682}, -- Enchant Cloak - Lesser Fire Resistance
[7863] = { scrollID = 38785, vellumID = 38682}, -- Enchant Boots - Minor Stamina
[7867] = { scrollID = 38786, vellumID = 38682}, -- Enchant Boots - Minor Agility
[13378] = { scrollID = 38787, vellumID = 38682}, -- Enchant Shield - Minor Stamina
[13380] = { scrollID = 38788, vellumID = 39349}, -- Enchant 2H Weapon - Lesser Spirit
[13419] = { scrollID = 38789, vellumID = 38682}, -- Enchant Cloak - Minor Agility
[13421] = { scrollID = 38790, vellumID = 38682}, -- Enchant Cloak - Lesser Protection
[13464] = { scrollID = 38791, vellumID = 38682}, -- Enchant Shield - Lesser Protection
[13485] = { scrollID = 38792, vellumID = 38682}, -- Enchant Shield - Lesser Spirit
[13501] = { scrollID = 38793, vellumID = 38682}, -- Enchant Bracer - Lesser Stamina
[13503] = { scrollID = 38794, vellumID = 39349}, -- Enchant Weapon - Lesser Striking
[13522] = { scrollID = 38795, vellumID = 38682}, -- Enchant Cloak - Lesser Shadow Resistance
[13529] = { scrollID = 38796, vellumID = 39349}, -- Enchant 2H Weapon - Lesser Impact
[13536] = { scrollID = 38797, vellumID = 38682}, -- Enchant Bracer - Lesser Strength
[13538] = { scrollID = 38798, vellumID = 38682}, -- Enchant Chest - Lesser Absorption
[13607] = { scrollID = 38799, vellumID = 38682}, -- Enchant Chest - Mana
[13612] = { scrollID = 38800, vellumID = 38682}, -- Enchant Gloves - Mining
[13617] = { scrollID = 38801, vellumID = 38682}, -- Enchant Gloves - Herbalism
[13620] = { scrollID = 38802, vellumID = 38682}, -- Enchant Gloves - Fishing
[13622] = { scrollID = 38803, vellumID = 38682}, -- Enchant Bracer - Lesser Intellect
[13626] = { scrollID = 38804, vellumID = 38682}, -- Enchant Chest - Minor Stats
[13631] = { scrollID = 38805, vellumID = 38682}, -- Enchant Shield - Lesser Stamina
[13635] = { scrollID = 38806, vellumID = 38682}, -- Enchant Cloak - Defense
[13637] = { scrollID = 38807, vellumID = 38682}, -- Enchant Boots - Lesser Agility
[13640] = { scrollID = 38808, vellumID = 38682}, -- Enchant Chest - Greater Health
[13642] = { scrollID = 38809, vellumID = 38682}, -- Enchant Bracer - Spirit
[13644] = { scrollID = 38810, vellumID = 38682}, -- Enchant Boots - Lesser Stamina
[13646] = { scrollID = 38811, vellumID = 38682}, -- Enchant Bracer - Lesser Deflection
[13648] = { scrollID = 38812, vellumID = 38682}, -- Enchant Bracer - Stamina
[13653] = { scrollID = 38813, vellumID = 39349}, -- Enchant Weapon - Lesser Beastslayer
[13655] = { scrollID = 38814, vellumID = 39349}, -- Enchant Weapon - Lesser Elemental Slayer
[13657] = { scrollID = 38815, vellumID = 38682}, -- Enchant Cloak - Fire Resistance
[13659] = { scrollID = 38816, vellumID = 38682}, -- Enchant Shield - Spirit
[13661] = { scrollID = 38817, vellumID = 38682}, -- Enchant Bracer - Strength
[13663] = { scrollID = 38818, vellumID = 38682}, -- Enchant Chest - Greater Mana
[13687] = { scrollID = 38819, vellumID = 38682}, -- Enchant Boots - Lesser Spirit
[13689] = { scrollID = 38820, vellumID = 38682}, -- Enchant Shield - Lesser Block
[13693] = { scrollID = 38821, vellumID = 39349}, -- Enchant Weapon - Striking
[13695] = { scrollID = 38822, vellumID = 39349}, -- Enchant 2H Weapon - Impact
[13698] = { scrollID = 38823, vellumID = 38682}, -- Enchant Gloves - Skinning
[13700] = { scrollID = 38824, vellumID = 38682}, -- Enchant Chest - Lesser Stats
[13746] = { scrollID = 38825, vellumID = 38682}, -- Enchant Cloak - Greater Defense
[13794] = { scrollID = 38826, vellumID = 38682}, -- Enchant Cloak - Resistance
[13815] = { scrollID = 38827, vellumID = 38682}, -- Enchant Gloves - Agility
[13817] = { scrollID = 38828, vellumID = 38682}, -- Enchant Shield - Stamina
[13822] = { scrollID = 38829, vellumID = 38682}, -- Enchant Bracer - Intellect
[13836] = { scrollID = 38830, vellumID = 38682}, -- Enchant Boots - Stamina
[13841] = { scrollID = 38831, vellumID = 38682}, -- Enchant Gloves - Advanced Mining
[13846] = { scrollID = 38832, vellumID = 38682}, -- Enchant Bracer - Greater Spirit
[13858] = { scrollID = 38833, vellumID = 38682}, -- Enchant Chest - Superior Health
[13868] = { scrollID = 38834, vellumID = 38682}, -- Enchant Gloves - Advanced Herbalism
[13882] = { scrollID = 38835, vellumID = 38682}, -- Enchant Cloak - Lesser Agility
[13887] = { scrollID = 38836, vellumID = 38682}, -- Enchant Gloves - Strength
[13890] = { scrollID = 38837, vellumID = 38682}, -- Enchant Boots - Minor Speed
[13898] = { scrollID = 38838, vellumID = 39349}, -- Enchant Weapon - Fiery Weapon
[13905] = { scrollID = 38839, vellumID = 38682}, -- Enchant Shield - Greater Spirit
[13915] = { scrollID = 38840, vellumID = 39349}, -- Enchant Weapon - Demonslaying
[13917] = { scrollID = 38841, vellumID = 38682}, -- Enchant Chest - Superior Mana
[13931] = { scrollID = 38842, vellumID = 38682}, -- Enchant Bracer - Deflection
[13933] = { scrollID = 38843, vellumID = 38682}, -- Enchant Shield - Frost Resistance
[13935] = { scrollID = 38844, vellumID = 38682}, -- Enchant Boots - Agility
[13937] = { scrollID = 38845, vellumID = 39349}, -- Enchant 2H Weapon - Greater Impact
[13939] = { scrollID = 38846, vellumID = 38682}, -- Enchant Bracer - Greater Strength
[13941] = { scrollID = 38847, vellumID = 38682}, -- Enchant Chest - Stats
[13943] = { scrollID = 38848, vellumID = 39349}, -- Enchant Weapon - Greater Striking
[13945] = { scrollID = 38849, vellumID = 38682}, -- Enchant Bracer - Greater Stamina
[13947] = { scrollID = 38850, vellumID = 38682}, -- Enchant Gloves - Riding Skill
[13948] = { scrollID = 38851, vellumID = 38682}, -- Enchant Gloves - Minor Haste
[20008] = { scrollID = 38852, vellumID = 38682}, -- Enchant Bracer - Greater Intellect
[20009] = { scrollID = 38853, vellumID = 38682}, -- Enchant Bracer - Superior Spirit
[20010] = { scrollID = 38854, vellumID = 38682}, -- Enchant Bracer - Superior Strength
[20011] = { scrollID = 38855, vellumID = 38682}, -- Enchant Bracer - Superior Stamina
[20012] = { scrollID = 38856, vellumID = 38682}, -- Enchant Gloves - Greater Agility
[20013] = { scrollID = 38857, vellumID = 38682}, -- Enchant Gloves - Greater Strength
[20014] = { scrollID = 38858, vellumID = 38682}, -- Enchant Cloak - Greater Resistance
[20015] = { scrollID = 38859, vellumID = 38682}, -- Enchant Cloak - Superior Defense
[20016] = { scrollID = 38860, vellumID = 38682}, -- Enchant Shield - Vitality
[20017] = { scrollID = 38861, vellumID = 38682}, -- Enchant Shield - Greater Stamina
[20020] = { scrollID = 38862, vellumID = 38682}, -- Enchant Boots - Greater Stamina
[20023] = { scrollID = 38863, vellumID = 38682}, -- Enchant Boots - Greater Agility
[20024] = { scrollID = 38864, vellumID = 38682}, -- Enchant Boots - Spirit
[20025] = { scrollID = 38865, vellumID = 38682}, -- Enchant Chest - Greater Stats
[20026] = { scrollID = 38866, vellumID = 38682}, -- Enchant Chest - Major Health
[20028] = { scrollID = 38867, vellumID = 38682}, -- Enchant Chest - Major Mana
[20029] = { scrollID = 38868, vellumID = 39349}, -- Enchant Weapon - Icy Chill
[20030] = { scrollID = 38869, vellumID = 39349}, -- Enchant 2H Weapon - Superior Impact
[20031] = { scrollID = 38870, vellumID = 39349}, -- Enchant Weapon - Superior Striking
[20032] = { scrollID = 38871, vellumID = 39349}, -- Enchant Weapon - Lifestealing
[20033] = { scrollID = 38872, vellumID = 39349}, -- Enchant Weapon - Unholy Weapon
[20034] = { scrollID = 38873, vellumID = 39349}, -- Enchant Weapon - Crusader
[20035] = { scrollID = 38874, vellumID = 39349}, -- Enchant 2H Weapon - Major Spirit
[20036] = { scrollID = 38875, vellumID = 39349}, -- Enchant 2H Weapon - Major Intellect
[21931] = { scrollID = 38876, vellumID = 39349}, -- Enchant Weapon - Winter\'s Might
[22749] = { scrollID = 38877, vellumID = 39349}, -- Enchant Weapon - Spellpower
[22750] = { scrollID = 38878, vellumID = 39349}, -- Enchant Weapon - Healing Power
[23799] = { scrollID = 38879, vellumID = 39349}, -- Enchant Weapon - Strength
[23800] = { scrollID = 38880, vellumID = 39349}, -- Enchant Weapon - Agility
[23801] = { scrollID = 38881, vellumID = 38682}, -- Enchant Bracer - Mana Regeneration
[23802] = { scrollID = 38882, vellumID = 38682}, -- Enchant Bracer - Healing Power
[23803] = { scrollID = 38883, vellumID = 39349}, -- Enchant Weapon - Mighty Spirit
[23804] = { scrollID = 38884, vellumID = 39349}, -- Enchant Weapon - Mighty Intellect
[25072] = { scrollID = 38885, vellumID = 38682}, -- Enchant Gloves - Threat
[25073] = { scrollID = 38886, vellumID = 38682}, -- Enchant Gloves - Shadow Power
[25074] = { scrollID = 38887, vellumID = 38682}, -- Enchant Gloves - Frost Power
[25078] = { scrollID = 38888, vellumID = 38682}, -- Enchant Gloves - Fire Power
[25079] = { scrollID = 38889, vellumID = 38682}, -- Enchant Gloves - Healing Power
[25080] = { scrollID = 38890, vellumID = 38682}, -- Enchant Gloves - Superior Agility
[25081] = { scrollID = 38891, vellumID = 38682}, -- Enchant Cloak - Greater Fire Resistance
[25082] = { scrollID = 38892, vellumID = 38682}, -- Enchant Cloak - Greater Nature Resistance
[25083] = { scrollID = 38893, vellumID = 38682}, -- Enchant Cloak - Stealth
[25084] = { scrollID = 38894, vellumID = 38682}, -- Enchant Cloak - Subtlety
[25086] = { scrollID = 38895, vellumID = 38682}, -- Enchant Cloak - Dodge
[27837] = { scrollID = 38896, vellumID = 39349}, -- Enchant 2H Weapon - Agility
[27899] = { scrollID = 38897, vellumID = 37602}, -- Enchant Bracer - Brawn
[27905] = { scrollID = 38898, vellumID = 37602}, -- Enchant Bracer - Stats
[27906] = { scrollID = 38899, vellumID = 37602}, -- Enchant Bracer - Major Defense
[27911] = { scrollID = 38900, vellumID = 37602}, -- Enchant Bracer - Superior Healing
[27913] = { scrollID = 38901, vellumID = 37602}, -- Enchant Bracer - Restore Mana Prime
[27914] = { scrollID = 38902, vellumID = 37602}, -- Enchant Bracer - Fortitude
[27917] = { scrollID = 38903, vellumID = 37602}, -- Enchant Bracer - Spellpower
[27944] = { scrollID = 38904, vellumID = 37602}, -- Enchant Shield - Tough Shield
[27945] = { scrollID = 38905, vellumID = 37602}, -- Enchant Shield - Intellect
[27946] = { scrollID = 38906, vellumID = 37602}, -- Enchant Shield - Shield Block
[27947] = { scrollID = 38907, vellumID = 37602}, -- Enchant Shield - Resistance
[27948] = { scrollID = 38908, vellumID = 37602}, -- Enchant Boots - Vitality
[27950] = { scrollID = 38909, vellumID = 37602}, -- Enchant Boots - Fortitude
[27954] = { scrollID = 38910, vellumID = 37602}, -- Enchant Boots - Surefooted
[27957] = { scrollID = 38911, vellumID = 37602}, -- Enchant Chest - Exceptional Health
[27958] = { scrollID = 38912, vellumID = 43145}, -- Enchant Chest - Exceptional Mana
[27960] = { scrollID = 38913, vellumID = 37602}, -- Enchant Chest - Exceptional Stats
[27961] = { scrollID = 38914, vellumID = 37602}, -- Enchant Cloak - Major Armor
[27962] = { scrollID = 38915, vellumID = 37602}, -- Enchant Cloak - Major Resistance
[27967] = { scrollID = 38917, vellumID = 37602}, -- Enchant Weapon - Major Striking
[27968] = { scrollID = 38918, vellumID = 37602}, -- Enchant Weapon - Major Intellect
[27971] = { scrollID = 38919, vellumID = 37602}, -- Enchant 2H Weapon - Savagery
[27972] = { scrollID = 38920, vellumID = 37602}, -- Enchant Weapon - Potency
[27975] = { scrollID = 38921, vellumID = 37602}, -- Enchant Weapon - Major Spellpower
[27977] = { scrollID = 38922, vellumID = 37602}, -- Enchant 2H Weapon - Major Agility
[27981] = { scrollID = 38923, vellumID = 37602}, -- Enchant Weapon - Sunfire
[27982] = { scrollID = 38924, vellumID = 37602}, -- Enchant Weapon - Soulfrost
[27984] = { scrollID = 38925, vellumID = 37602}, -- Enchant Weapon - Mongoose
[28003] = { scrollID = 38926, vellumID = 37602}, -- Enchant Weapon - Spellsurge
[28004] = { scrollID = 38927, vellumID = 37602}, -- Enchant Weapon - Battlemaster
[33990] = { scrollID = 38928, vellumID = 37602}, -- Enchant Chest - Major Spirit
[33991] = { scrollID = 38929, vellumID = 37602}, -- Enchant Chest - Restore Mana Prime
[33992] = { scrollID = 38930, vellumID = 37602}, -- Enchant Chest - Major Resilience
[33993] = { scrollID = 38931, vellumID = 37602}, -- Enchant Gloves - Blasting
[33994] = { scrollID = 38932, vellumID = 37602}, -- Enchant Gloves - Precise Strikes
[33995] = { scrollID = 38933, vellumID = 37602}, -- Enchant Gloves - Major Strength
[33996] = { scrollID = 38934, vellumID = 37602}, -- Enchant Gloves - Assault
[33997] = { scrollID = 38935, vellumID = 37602}, -- Enchant Gloves - Major Spellpower
[33999] = { scrollID = 38936, vellumID = 37602}, -- Enchant Gloves - Major Healing
[34001] = { scrollID = 38937, vellumID = 37602}, -- Enchant Bracer - Major Intellect
[34002] = { scrollID = 38938, vellumID = 37602}, -- Enchant Bracer - Assault
[34003] = { scrollID = 38939, vellumID = 37602}, -- Enchant Cloak - Spell Penetration
[34004] = { scrollID = 38940, vellumID = 37602}, -- Enchant Cloak - Greater Agility
[34005] = { scrollID = 38941, vellumID = 37602}, -- Enchant Cloak - Greater Arcane Resistance
[34006] = { scrollID = 38942, vellumID = 37602}, -- Enchant Cloak - Greater Shadow Resistance
[34007] = { scrollID = 38943, vellumID = 37602}, -- Enchant Boots - Cat\'s Swiftness
[34008] = { scrollID = 38944, vellumID = 37602}, -- Enchant Boots - Boar\'s Speed
[34009] = { scrollID = 38945, vellumID = 37602}, -- Enchant Shield - Major Stamina
[34010] = { scrollID = 38946, vellumID = 37602}, -- Enchant Weapon - Major Healing
[42620] = { scrollID = 38947, vellumID = 37602}, -- Enchant Weapon - Greater Agility
[42974] = { scrollID = 38948, vellumID = 43146}, -- Enchant Weapon - Executioner
[44383] = { scrollID = 38949, vellumID = 37602}, -- Enchant Shield - Resilience
[44483] = { scrollID = 38950, vellumID = 43145}, -- Enchant Cloak - Superior Frost Resistance
[44484] = { scrollID = 38951, vellumID = 43145}, -- Enchant Gloves - Expertise
[44488] = { scrollID = 38953, vellumID = 43145}, -- Enchant Gloves - Precision
[44489] = { scrollID = 38954, vellumID = 43145}, -- Enchant Shield - Defense
[44492] = { scrollID = 38955, vellumID = 43145}, -- Enchant Chest - Mighty Health
[44494] = { scrollID = 38956, vellumID = 43145}, -- Enchant Cloak - Superior Nature Resistance
[44500] = { scrollID = 38959, vellumID = 43145}, -- Enchant Cloak - Superior Agility
[44506] = { scrollID = 38960, vellumID = 43145}, -- Enchant Gloves - Gatherer
[44508] = { scrollID = 38961, vellumID = 43145}, -- Enchant Boots - Greater Spirit
[44509] = { scrollID = 38962, vellumID = 43145}, -- Enchant Chest - Greater Mana Restoration
[44510] = { scrollID = 38963, vellumID = 43146}, -- Enchant Weapon - Exceptional Spirit
[44513] = { scrollID = 38964, vellumID = 43145}, -- Enchant Gloves - Greater Assault
[44524] = { scrollID = 38965, vellumID = 43146}, -- Enchant Weapon - Icebreaker
[44528] = { scrollID = 38966, vellumID = 43145}, -- Enchant Boots - Greater Fortitude
[44529] = { scrollID = 38967, vellumID = 43145}, -- Enchant Gloves - Major Agility
[44555] = { scrollID = 38968, vellumID = 43145}, -- Enchant Bracers - Exceptional Intellect
[44556] = { scrollID = 38969, vellumID = 43145}, -- Enchant Cloak - Superior Fire Resistance
[60616] = { scrollID = 38971, vellumID = 43145}, -- Enchant Bracers - Striking
[44576] = { scrollID = 38972, vellumID = 43146}, -- Enchant Weapon - Lifeward
[44582] = { scrollID = 38973, vellumID = 43145}, -- Enchant Cloak - Spell Piercing
[44584] = { scrollID = 38974, vellumID = 43145}, -- Enchant Boots - Greater Vitality
[44588] = { scrollID = 38975, vellumID = 43145}, -- Enchant Chest - Exceptional Resilience
[44589] = { scrollID = 38976, vellumID = 43145}, -- Enchant Boots - Superior Agility
[44590] = { scrollID = 38977, vellumID = 43145}, -- Enchant Cloak - Superior Shadow Resistance
[44591] = { scrollID = 38978, vellumID = 43145}, -- Enchant Cloak - Titanweave
[44592] = { scrollID = 38979, vellumID = 43145}, -- Enchant Gloves - Exceptional Spellpower
[44593] = { scrollID = 38980, vellumID = 43145}, -- Enchant Bracers - Major Spirit
[44595] = { scrollID = 38981, vellumID = 43146}, -- Enchant 2H Weapon - Scourgebane
[44596] = { scrollID = 38982, vellumID = 43145}, -- Enchant Cloak - Superior Arcane Resistance
[44598] = { scrollID = 38984, vellumID = 43145}, -- Enchant Bracers - Expertise
[60623] = { scrollID = 38986, vellumID = 43145}, -- Enchant Boots - Icewalker
[44616] = { scrollID = 38987, vellumID = 43145}, -- Enchant Bracers - Greater Stats
[44621] = { scrollID = 38988, vellumID = 43146}, -- Enchant Weapon - Giant Slayer
[44623] = { scrollID = 38989, vellumID = 43145}, -- Enchant Chest - Super Stats
[44625] = { scrollID = 38990, vellumID = 43145}, -- Enchant Gloves - Armsman
[44629] = { scrollID = 38991, vellumID = 43146}, -- Enchant Weapon - Exceptional Spellpower
[44630] = { scrollID = 38992, vellumID = 43146}, -- Enchant 2H Weapon - Greater Savagery
[44631] = { scrollID = 38993, vellumID = 43145}, -- Enchant Cloak - Shadow Armor
[44633] = { scrollID = 38995, vellumID = 43146}, -- Enchant Weapon - Exceptional Agility
[44635] = { scrollID = 38997, vellumID = 43145}, -- Enchant Bracers - Greater Spellpower
[46578] = { scrollID = 38998, vellumID = 43146}, -- Enchant Weapon - Deathfrost
[46594] = { scrollID = 38999, vellumID = 37602}, -- Enchant Chest - Defense
[47051] = { scrollID = 39000, vellumID = 37602}, -- Enchant Cloak - Steelweave
[47672] = { scrollID = 39001, vellumID = 43145}, -- Enchant Cloak - Mighty Armor
[47766] = { scrollID = 39002, vellumID = 43145}, -- Enchant Chest - Greater Defense
[47899] = { scrollID = 39004, vellumID = 43145}, -- Enchant Cloak - Wisdom
[47900] = { scrollID = 39005, vellumID = 43145}, -- Enchant Chest - Super Health
[47901] = { scrollID = 39006, vellumID = 43145}, -- Enchant Boots - Tuskarr\'s Vitality
[59625] = { scrollID = 43987, vellumID = 43146}, -- Enchant Weapon - Black Magic
[60606] = { scrollID = 44449, vellumID = 43145}, -- Enchant Boots - Assault
[60621] = { scrollID = 44453, vellumID = 43146}, -- Enchant Weapon - Greater Potency
[60653] = { scrollID = 44455, vellumID = 43145}, -- Enchant Shield - Greater Intellect
[60609] = { scrollID = 44456, vellumID = 43145}, -- Enchant Cloak - Speed
[60663] = { scrollID = 44457, vellumID = 43145}, -- Enchant Cloak - Major Agility
[60668] = { scrollID = 44458, vellumID = 43145}, -- Enchant Gloves - Crusher
[60691] = { scrollID = 44463, vellumID = 43146}, -- Enchant 2H Weapon - Massacre
[60692] = { scrollID = 44465, vellumID = 43145}, -- Enchant Chest - Powerful Stats
[60707] = { scrollID = 44466, vellumID = 43146}, -- Enchant Weapon - Superior Potency
[60714] = { scrollID = 44467, vellumID = 43146}, -- Enchant Weapon - Mighty Spellpower
[60763] = { scrollID = 44469, vellumID = 43145}, -- Enchant Boots - Greater Assault
[60767] = { scrollID = 44470, vellumID = 43145}, -- Enchant Bracers - Superior Spellpower
[59621] = { scrollID = 44493, vellumID = 43146}, -- Enchant Weapon - Berserking
[59619] = { scrollID = 44497, vellumID = 43146}, -- Enchant Weapon - Accuracy
[44575] = { scrollID = 44815, vellumID = 43145}, -- Enchant Bracers - Greater Assault
[62256] = { scrollID = 44947, vellumID = 43146}, -- Enchant Bracers - Major Stamina
[62948] = { scrollID = 45056, vellumID = 43146}, -- Enchant Staff - Greater Spellpower
[62959] = { scrollID = 45060, vellumID = 43146}, -- Enchant Staff - Spellpower
[63746] = { scrollID = 45628, vellumID = 38682}, -- Enchant Boots - Lesser Accuracy
[64441] = { scrollID = 46026, vellumID = 43146}, -- Enchant Weapon - Blade Ward
[64579] = { scrollID = 46098, vellumID = 43146}, -- Enchant Weapon - Blood Draining
[71692] = { scrollID = 50816, vellumID = 38682}, -- Enchant Gloves - Angler
}
