






do
	LSWConfig.glyphTypes = {}

	local glyphTypeColor = {
		["Major"] = "|cffff8000",
		["Minor"] = "|cff00ff80",
	}



	local function GlyphType(itemID)
		if not LSWConfig.glyphTypes then
			LSWConfig.glyphTypes = {}
		end


		if not LSWConfig.glyphTypes[itemID] then
			local tooltip = getglobal("LSWParsingTooltip")


			if tooltip == nil then
				tooltip = CreateFrame("GameTooltip", "LSWParsingTooltip", UIParent, "GameTooltipTemplate")
				tooltip:SetOwner(LSW.parentFrame, "ANCHOR_NONE")
			end

			tooltip:SetHyperlink("item:"..itemID)

			local tiplines = tooltip:NumLines()

			for i=2, tiplines, 1 do
				local lineText = getglobal("LSWParsingTooltipTextLeft"..i):GetText() or " "


				local g = string.match(lineText, "(%w+) Glyph")

				if g then
					LSWConfig.glyphTypes[itemID] = g
					break
				end
			end
		end

		return LSWConfig.glyphTypes[itemID]
	end


	function LSW:FilterButtonText(button, itemID, recipeID)
		if not button then
			return
		end

		local text = button:GetText()

		if not text then
		--	LSW:ChatMessage(button:GetName())
			return
		end


		if itemID and string.match(text, "Glyph of") then
			local glyphType = GlyphType(itemID)

			if glyphType then
				local newText = string.gsub(text, "Glyph of", (glyphTypeColor[glyphType] or "")..glyphType..":|r")

				button:SetText(newText)
				return
			end
		end

		if recipeID and string.match(text, "Enchant ") then
			local newText = string.gsub(text, "Enchant ", "")

			button:SetText(newText)
			return
		end
	end
end

