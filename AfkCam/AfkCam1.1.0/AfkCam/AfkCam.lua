-- CONFIG
local f_camSpeed = .05 -- Arbitrary number that looks good
local b_saveView = true;

-- GLOBALS --
local isAFK = false;

-- FUNCS --


-- Add AfkCam panel to the WoW Interface AddOn config window
--[[
function AfkCam_onLoad (ipanel)
	ipanel.name = "AfkCam";
	ipanel.okay = function (self) DEFAULT_CHAT_FRAME:AddMessage("AfkCam: Settings saved."); end;
	ipanel.cancel = function (self) DEFAULT_CHAT_FRAME:AddMessage("AfkCam: Config canceled."); end;
	ipanel.default = function (self) DEFAULT_CHAT_FRAME:AddMessage("AfkCam: Reset to default."); end;
	InterfaceOptions_AddCategory(ipanel);
end
--]]

-- Start the camera panning
local function StartCameraMovement (speed)
	if (b_saveView) then
		SaveView(5);
	end
	MoveViewLeftStart(speed);
end

-- Stop the camera movement and pan back to original view
local function StopCameraMovement ()
	MoveViewLeftStop();
	if (b_saveView) then
		SetView(5);
		ResetView(5);
	end
end

-- Check to see if the player (un)AFK'd and call the appropriate function
local function onFlagChange (self, event, ...)
	if (UnitIsAFK("player") and not isAFK) then
	-- Player has become AFK
		StartCameraMovement(f_camSpeed);
		DEFAULT_CHAT_FRAME:AddMessage("AfkCam activated.");
		isAFK = true;
	elseif (not UnitIsAFK("player") and isAFK) then
	-- Player has become un-AFK
		StopCameraMovement();
		DEFAULT_CHAT_FRAME:AddMessage("AfkCam deactivated.");
		isAFK = false;
	--else
	-- Player's flag change concerned DND, not becoming AFK or un-AFK
	end
end

local function main ()
	local frame = CreateFrame("FRAME", "AfkCamFrame");
	frame:RegisterEvent("PLAYER_FLAGS_CHANGED"); -- "PLAYER_FLAGS_CHANGED" triggers when a player becomes (un)AFK and (un)DND
	frame:SetScript("OnEvent", onFlagChange);
end

main();