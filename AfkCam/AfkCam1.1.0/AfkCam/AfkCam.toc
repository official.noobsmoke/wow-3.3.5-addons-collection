## Interface: 30300
## Title: AfkCam
## Notes: Rotates the game camera around your character when you go AFK.
## Version: 1.1
AfkCam.lua