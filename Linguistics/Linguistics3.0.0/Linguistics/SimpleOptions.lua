-- ---------------------------------------------------------------
-- ---------------------------------------------------------------
-- ---------- Addon was made by Bonefish on Ragnaros-EU ----------
-- ----------- Please report bugs or tips on Curse.com -----------
-- -------------- E-mail me at: lowerpug@gmail.com ---------------
-- ---------------------------------------------------------------

-- [[ START IMPORTANT ]]
-- Change version in Linguistics.lua and in .toc file
	local newVersion = "v3.0";
-- [[  END IMPORTANT  ]]

-- ling on / off / toggle slashcommand
SLASH_LING1 = '/ling';
function SlashCmdList.LING(msg, editbox)
	if (msg=="off") then
		LINGUISTICS_MASTOG = nil
		LinguisticsConfigOptionsWowLfr:Disable();
		LinguisticsConfigOptionsWowZon:Disable();
		LinguisticsConfigOptionsWowDun:Disable();
		LinguisticsConfigOptionsWowRai:Disable();
		LinguisticsConfigOptionsWowBat:Disable();
		LinguisticsConfigOptionsInt:Disable();
		LinguisticsConfigOptionsNum:Disable();
		LinguisticsConfigOptionsPun:Disable();
		LinguisticsConfigOptionsLol:Disable();
		LinguisticsConfigOptionsMrk:Disable();
		LinguisticsConfigOptionsCap:Disable();
		LinguisticsConfigChatFilter:Disable();
		LinguisticsConfigMasterButton:SetChecked(LINGUISTICS_MASTOG)
		DEFAULT_CHAT_FRAME:AddMessage("Linguistics " .. newVersion .. " |cff69ccf0[English]|r is now |cffff0000disabled|r.", 1.0, 0.82, 0.0)
	elseif (msg=="on") then	
		LINGUISTICS_MASTOG = true
		LinguisticsConfigOptionsWowLfr:Enable();
		LinguisticsConfigOptionsWowZon:Enable();
		LinguisticsConfigOptionsWowDun:Enable();
		LinguisticsConfigOptionsWowRai:Enable();
		LinguisticsConfigOptionsWowBat:Enable();
		LinguisticsConfigOptionsInt:Enable();
		LinguisticsConfigOptionsNum:Enable();
		LinguisticsConfigOptionsPun:Enable();
		LinguisticsConfigOptionsLol:Enable();
		LinguisticsConfigOptionsMrk:Enable();
		LinguisticsConfigOptionsCap:Enable();
		LinguisticsConfigChatFilter:Enable();
		LinguisticsConfigMasterButton:SetChecked(LINGUISTICS_MASTOG)
		DEFAULT_CHAT_FRAME:AddMessage("Linguistics " .. newVersion .. " |cff69ccf0[English]|r is now |cff00ff00enabled|r.", 1.0, 0.82, 0.0)
	elseif (msg=="toggle") then
		if (LINGUISTICS_MASTOG==nil) then
			LINGUISTICS_MASTOG = true
			DEFAULT_CHAT_FRAME:AddMessage("Linguistics " .. newVersion .. " |cff69ccf0[English]|r is now |cff00ff00enabled|r.", 1.0, 0.82, 0.0)
		else
			LINGUISTICS_MASTOG = nil
			DEFAULT_CHAT_FRAME:AddMessage("Linguistics " .. newVersion .. " |cff69ccf0[English]|r is now |cffff0000disabled|r.", 1.0, 0.82, 0.0)
		end
		LinguisticsConfigMasterButton:SetChecked(LINGUISTICS_MASTOG)
	else
		InterfaceOptionsFrame_OpenToCategory("Linguistics")
	end
end

-- Create UI
local name = ...
do
	--[[ Slash Handler ]]--
	SlashCmdList["LINGUISTICS"] = function() InterfaceOptionsFrame_OpenToCategory(name) end
	SLASH_LINGUISTICS1 = "/linguistics"
	SLASH_LINGUISTICS2 = "/ling"

	--[[ Localization ]]--
	local locMasTog = "Master switch. Turn Linguistics on or off."
	local locMasTxt = "Thank you for making use of Linguistics. Currently\nthere is only English language support. If you notice\nany bugs, errors or words that are incorrectly changed\nthen please report this on |cff69ccf0curse.com|r. Now, talk like a sir."
	local locOptWowLfr = "Write out LF abbreviations."
	local locOptWowZon = "Write out zone abbreviations."
	local locOptWowDun = "Write out dungeon abbreviations."
	local locOptWowRai = "Write out raid abbreviations."
	local locOptWowBat = "Write out battleground abbreviations."
	local locOptInt = "Correct common misspelled words."
	local locOptNum = "Write out numbers 1-9."
	local locOptPun = "Add missing and remove excessive punctuation."
	local locOptLol = "Change Internet abbreviations like 'lol', 'imo', 'afk', etcetera."
	local locOptMrk = "Allow colours to be used as chat commands when typing target markers."
	local locOptCap = "Do not filter words with more then one capital letter. Allows full caps lock sentences."
	local locOptMrk2 = "Example: Typing \"{green}\" in the chat will show the triangle icon ( |TInterface\\TargetingFrame\\UI-RaidTargetingIcon_4:0|t )."
	local locChaFil = "|cff69ccf0Apply the filters above to the chat from other people.|r"
	
	local locToDo = "Author: |cff00FF00Cryptonym / Bonefish (Ragnaros-EU)|r\nWebsite: |cff69ccf0http://www.curse.com/addons/wow/linguistics|r\nE-mail me at: |cff69ccf0mail@lowerpug.com|r"
	
	--[[ Main Panel ]]--
	local linguistics = CreateFrame("Frame", "LinguisticsConfig", InterfaceOptionsFramePanelContainer)
	--linguistics:SetScript("OnEvent", function(self, event, ...) self[event](self, event, ...) end)
	linguistics:Hide()
	linguistics.name = name
	linguistics:SetScript("OnShow", function()
	-- Check or uncheck boxes by loading from vars
		LinguisticsConfigMasterButton:SetChecked(LINGUISTICS_MASTOG)
		LinguisticsConfigOptionsWowLfr:SetChecked(LINGUISTICS_OPTWOWLFR)
		LinguisticsConfigOptionsWowZon:SetChecked(LINGUISTICS_OPTWOWZON)
		LinguisticsConfigOptionsWowDun:SetChecked(LINGUISTICS_OPTWOWDUN)
		LinguisticsConfigOptionsWowRai:SetChecked(LINGUISTICS_OPTWOWRAI)
		LinguisticsConfigOptionsWowBat:SetChecked(LINGUISTICS_OPTWOWBAT)
		LinguisticsConfigOptionsInt:SetChecked(LINGUISTICS_OPTINT)
		LinguisticsConfigOptionsNum:SetChecked(LINGUISTICS_OPTNUM)
		LinguisticsConfigOptionsPun:SetChecked(LINGUISTICS_OPTPUN)
		LinguisticsConfigOptionsLol:SetChecked(LINGUISTICS_OPTLOL)
		LinguisticsConfigOptionsMrk:SetChecked(LINGUISTICS_OPTMRK)
		LinguisticsConfigOptionsCap:SetChecked(LINGUISTICS_OPTCAP)		
		LinguisticsConfigChatFilter:SetChecked(LINGUISTICS_CHAFIL)
	-- Disable buttons if masterswich is off
		if ( LinguisticsConfigMasterButton:GetChecked() == nil ) then
			LinguisticsConfigOptionsWowLfr:Disable();
			LinguisticsConfigOptionsWowZon:Disable();
			LinguisticsConfigOptionsWowDun:Disable();
			LinguisticsConfigOptionsWowRai:Disable();
			LinguisticsConfigOptionsWowBat:Disable();
			LinguisticsConfigOptionsInt:Disable();
			LinguisticsConfigOptionsNum:Disable();
			LinguisticsConfigOptionsPun:Disable();
			LinguisticsConfigOptionsLol:Disable();
			LinguisticsConfigOptionsMrk:Disable();
			LinguisticsConfigOptionsCap:Disable();
			LinguisticsConfigChatFilter:Disable();
		end
	end)
	local title = linguistics:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
	title:SetPoint("TOPLEFT", 16, -16)
	title:SetText("Linguistics " .. newVersion .. " |cff69ccf0[English]|r") --wowace magic, replaced with tag version
	InterfaceOptions_AddCategory(linguistics)

	--[[ Master Checkbox ]]--
	local btnMasTog = CreateFrame("CheckButton", "LinguisticsConfigMasterButton", linguistics, "OptionsBaseCheckButtonTemplate")
	btnMasTog:SetPoint("TOPLEFT", 16, -35)
	btnMasTog:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_MASTOG = true
			LinguisticsConfigOptionsWowLfr:Enable();
			LinguisticsConfigOptionsWowZon:Enable();
			LinguisticsConfigOptionsWowDun:Enable();
			LinguisticsConfigOptionsWowRai:Enable();
			LinguisticsConfigOptionsWowBat:Enable();
			LinguisticsConfigOptionsInt:Enable();
			LinguisticsConfigOptionsNum:Enable();
			LinguisticsConfigOptionsPun:Enable();
			LinguisticsConfigOptionsLol:Enable();
			LinguisticsConfigOptionsMrk:Enable();
			LinguisticsConfigOptionsCap:Enable();
			LinguisticsConfigChatFilter:Enable();
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_MASTOG = nil
			LinguisticsConfigOptionsWowLfr:Disable();
			LinguisticsConfigOptionsWowZon:Disable();
			LinguisticsConfigOptionsWowDun:Disable();
			LinguisticsConfigOptionsWowRai:Disable();
			LinguisticsConfigOptionsWowBat:Disable();
			LinguisticsConfigOptionsInt:Disable();
			LinguisticsConfigOptionsNum:Disable();
			LinguisticsConfigOptionsPun:Disable();
			LinguisticsConfigOptionsLol:Disable();
			LinguisticsConfigOptionsMrk:Disable();
			LinguisticsConfigOptionsCap:Disable();
			LinguisticsConfigChatFilter:Disable();
		end
	end)
	
	local btnMasTogText = btnMasTog:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnMasTogText:SetPoint("LEFT", btnMasTog, "RIGHT", 0, 0)
	btnMasTogText:SetText(locMasTog)
	local btnMasTogDesc = btnMasTog:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnMasTogDesc:SetPoint("TOPLEFT", btnMasTogText, "BOTTOMLEFT", 0, -5)
	btnMasTogDesc:SetJustifyH("LEFT")
	btnMasTogDesc:SetWordWrap(true)
	btnMasTogDesc:SetText(locMasTxt)

	--[[ Options Header ]]--
	local optionsTitle = linguistics:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
	optionsTitle:SetPoint("TOPLEFT", btnMasTogDesc, "BOTTOMLEFT", 0, -10)
	optionsTitle:SetText("World of Warcraft filters")
	
	--[[ Options WoW LFR ]]--
	local btnOptWowLfr = CreateFrame("CheckButton", "LinguisticsConfigOptionsWowLfr", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptWowLfr:SetPoint("TOPLEFT", 43, -137)
	btnOptWowLfr:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTWOWLFR = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTWOWLFR = nil
		end
	end)
	
	local btnOptWowLfrText = btnOptWowLfr:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptWowLfrText:SetPoint("LEFT", btnOptWowLfr, "RIGHT", 0, 0)
	btnOptWowLfrText:SetText(locOptWowLfr)
	
	--[[ Options WoW ZON ]]--
	local btnOptWowZon = CreateFrame("CheckButton", "LinguisticsConfigOptionsWowZon", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptWowZon:SetPoint("TOPLEFT", 43, -157)
	btnOptWowZon:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTWOWZON = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTWOWZON = nil
		end
	end)
	
	local btnOptWowZonText = btnOptWowZon:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptWowZonText:SetPoint("LEFT", btnOptWowZon, "RIGHT", 0, 0)
	btnOptWowZonText:SetText(locOptWowZon)
	
	--[[ Options WoW DUN ]]--
	local btnOptWowDun = CreateFrame("CheckButton", "LinguisticsConfigOptionsWowDun", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptWowDun:SetPoint("TOPLEFT", 43, -177)
	btnOptWowDun:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTWOWDUN = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTWOWDUN = nil
		end
	end)
	
	local btnOptWowDunText = btnOptWowDun:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptWowDunText:SetPoint("LEFT", btnOptWowDun, "RIGHT", 0, 0)
	btnOptWowDunText:SetText(locOptWowDun)
	
	--[[ Options WoW RAI ]]--
	local btnOptWowRai = CreateFrame("CheckButton", "LinguisticsConfigOptionsWowRai", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptWowRai:SetPoint("TOPLEFT", 43, -197)
	btnOptWowRai:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTWOWRAI = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTWOWRAI = nil
		end
	end)
	
	local btnOptWowRaiText = btnOptWowRai:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptWowRaiText:SetPoint("LEFT", btnOptWowRai, "RIGHT", 0, 0)
	btnOptWowRaiText:SetText(locOptWowRai)
	
	--[[ Options WoW BAT ]]--
	local btnOptWowBat = CreateFrame("CheckButton", "LinguisticsConfigOptionsWowBat", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptWowBat:SetPoint("TOPLEFT", 43, -217)
	btnOptWowBat:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTWOWBAT = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTWOWBAT = nil
		end
	end)
	
	local btnOptWowBatText = btnOptWowBat:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptWowBatText:SetPoint("LEFT", btnOptWowBat, "RIGHT", 0, 0)
	btnOptWowBatText:SetText(locOptWowBat)

	--[[ Miscellaneous Header ]]--
	local optionsTitle = linguistics:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
	optionsTitle:SetPoint("TOPLEFT", btnOptWowBat, "BOTTOMLEFT", 0, -5)
	optionsTitle:SetText("Miscellaneous filters")
	
	--[[ Options Internet Slang ]]--
	local btnOptInt = CreateFrame("CheckButton", "LinguisticsConfigOptionsInt", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptInt:SetPoint("TOPLEFT", 43, -267)
	btnOptInt:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTINT = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTINT = nil
		end
	end)
	
	local btnOptIntText = btnOptInt:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptIntText:SetPoint("LEFT", btnOptInt, "RIGHT", 0, 0)
	btnOptIntText:SetText(locOptInt)

		--[[ Options Lol ]]--
	local btnOptLol = CreateFrame("CheckButton", "LinguisticsConfigOptionsLol", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptLol:SetPoint("TOPLEFT", 43, -287)
	btnOptLol:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTLOL = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTLOL = nil
		end
	end)
	
	local btnOptLolText = btnOptLol:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptLolText:SetPoint("LEFT", btnOptLol, "RIGHT", 0, 0)
	btnOptLolText:SetText(locOptLol)

		--[[ Options Numbers ]]--
	local btnOptNum = CreateFrame("CheckButton", "LinguisticsConfigOptionsNum", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptNum:SetPoint("TOPLEFT", 43, -307)
	btnOptNum:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTNUM = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTNUM = nil
		end
	end)
	
	local btnOptNumText = btnOptNum:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptNumText:SetPoint("LEFT", btnOptNum, "RIGHT", 0, 0)
	btnOptNumText:SetText(locOptNum)

	--[[ Options Caps ]]--
	local btnOptCap = CreateFrame("CheckButton", "LinguisticsConfigOptionsCap", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptCap:SetPoint("TOPLEFT", 43, -347)
	btnOptCap:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTCAP = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTCAP = false
		end
	end)
	
	local btnOptCapText = btnOptCap:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptCapText:SetPoint("LEFT", btnOptCap, "RIGHT", 0, 0)
	btnOptCapText:SetText(locOptCap)
	
	--[[ Options Raid markers ]]--
	local btnOptMrk = CreateFrame("CheckButton", "LinguisticsConfigOptionsMrk", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptMrk:SetPoint("TOPLEFT", 43, -367)
	btnOptMrk:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTMRK = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTMRK = nil
		end
	end)
	
	local btnOptMrkText = btnOptMrk:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptMrkText:SetPoint("LEFT", btnOptMrk, "RIGHT", 0, 0)
	btnOptMrkText:SetText(locOptMrk)

	local btnOptMrkText2 = btnOptMrk:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptMrkText2:SetPoint("LEFT", btnOptMrk, "RIGHT", 0, -20)
	btnOptMrkText2:SetText(locOptMrk2)
	
	--[[ Options Punctuation ]]--
	local btnOptPun = CreateFrame("CheckButton", "LinguisticsConfigOptionsPun", linguistics, "OptionsBaseCheckButtonTemplate")
	btnOptPun:SetPoint("TOPLEFT", 43, -327)
	btnOptPun:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_OPTPUN = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_OPTPUN = nil
		end
	end)
	
	local btnOptPunText = btnOptPun:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnOptPunText:SetPoint("LEFT", btnOptPun, "RIGHT", 0, 0)
	btnOptPunText:SetText(locOptPun)

	--[[ Chat Filter ]]--
	local btnChaFil = CreateFrame("CheckButton", "LinguisticsConfigChatFilter", linguistics, "OptionsBaseCheckButtonTemplate")
	btnChaFil:SetPoint("TOPLEFT", 43, -424)
	btnChaFil:SetScript("OnClick", function(frame)
		if frame:GetChecked() then
			PlaySound("igMainMenuOptionCheckBoxOn")
			LINGUISTICS_CHAFIL = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			LINGUISTICS_CHAFIL = nil
		end
	end)
	
	local btnChaFilText = btnChaFil:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnChaFilText:SetPoint("LEFT", btnChaFil, "RIGHT", 0, 0)
	btnChaFilText:SetText(locChaFil)
	
	local ZZtexture = linguistics:CreateTexture(nil, "ARTWORK")
	ZZtexture:SetPoint("TOPLEFT", 360, 2)
	ZZtexture:SetTexture("Interface\\AddOns\\Linguistics\\lingsir.blp")

	local btnMasTogDesc = btnMasTog:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnMasTogDesc:SetPoint("TOPLEFT", btnOptMrkText2, "BOTTOMLEFT", -43, -4)
	btnMasTogDesc:SetJustifyH("LEFT")
	btnMasTogDesc:SetWordWrap(true)
	btnMasTogDesc:SetText("|cff69ccf0-----------------------------------------------------------------------------------------|r")

	local btnMasTogDesc = btnMasTog:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnMasTogDesc:SetPoint("TOPLEFT", btnChaFilText, "BOTTOMLEFT", -43, -4)
	btnMasTogDesc:SetJustifyH("LEFT")
	btnMasTogDesc:SetWordWrap(true)
	btnMasTogDesc:SetText("|cff69ccf0-----------------------------------------------------------------------------------------|r")
	
	--[[ To-do Header ]]--
--	local todoTitle = linguistics:CreateFontString(nil, "ARTWORK", "GameFontRedLarge")
--	todoTitle:SetPoint("TOPLEFT", btnOptPunText, "BOTTOMLEFT", -43, -51)
--	todoTitle:SetText("Upcoming features and known bugs [Patch 5.4.7]")
	
	local btnMasTogDesc = btnMasTog:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
	btnMasTogDesc:SetPoint("TOPLEFT", btnOptMrkText2, "BOTTOMLEFT", -43, -76)
	btnMasTogDesc:SetJustifyH("LEFT")
	btnMasTogDesc:SetWordWrap(true)
	btnMasTogDesc:SetText(locToDo)
end

