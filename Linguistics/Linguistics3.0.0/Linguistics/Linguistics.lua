-- ---------------------------------------------------------------
-- ---------------------------------------------------------------
-- ---- Addon was made by Cryptonym / Bonefish on Ragnaros-EU ----
-- ----------- Please report bugs or tips on Curse.com -----------
-- --------------- E-mail me at: mail@lowerpug.com ---------------
-- ---------------------------------------------------------------

-- [[ START IMPORTANT ]]
-- Change version in SimpleOptions.lua and in .toc file
	local newVersion = "v3.0";
-- [[  END IMPORTANT  ]]

-- Hooking Blizzard function
local Linguistics_SendChatMessage = SendChatMessage;

-- If we detect Wintertime we'll note the channel and stay off of it
local wtID, wtName = GetChannelName("WinterTimeGlobal")

-- Nerf Caps Lock
function nerfCaps(x)
		x = strlower(x);
		x = runDatabase(x);
		x = gsub(x, "^%A*%a", strupper);
		return x;
end

-- Main hook
function SendChatMessage(msg, system, language, chatnumber) 
	-- Check for Master Switch
	if (LINGUISTICS_MASTOG == true) then
		if (msg ~= "" and (string.find(msg, "%[") == nil)) then 
			if ( string.find(msg, "^%/") == nil ) and (chatnumber ~= wtID) then

				msg = runFinalDatabase(msg,system,event);
			
			end
		end
	end
	Linguistics_SendChatMessage(msg, system, language, chatnumber);
end 

-- Run databases for external chat
local function lingChatFilter(self, event, msg, author, ...)

	local playerName = UnitName('player');

	if ((LINGUISTICS_CHAFIL == true) and (LINGUISTICS_MASTOG == true) and (author~=playerName)) then	
		-- Run database
		msg = runFinalDatabase(msg,system,event)	
	end
	-- Send it back
	return false, msg, author, ...
end	

-- Delay welcome message
local total = 0
local stop = false

local function onUpdate(self,elapsed)
    total = total + elapsed
    if ((total >= 4) and (stop==false)) then
		DEFAULT_CHAT_FRAME:AddMessage("Linguistics " .. newVersion .. " |cff69ccf0[English]|r is succesfully loaded! Use |cff69ccf0/ling|r or |cff69ccf0/linguistics|r to open the options menu. Use |cff69ccf0/ling|r |cff00ff00on|cffffffff | |cffff0000off|cffffffff | |cff69ccf0toggle|r to enable or disable the addon.", 1.0, 0.82, 0.0)
        total = 0
		stop = true
    end
end
 
local f = CreateFrame("frame")
f:SetScript("OnUpdate", onUpdate)

-- Addon loaded startup
local function RegisterEvents(self, ...)
	for i=1,select('#', ...) do
		self:RegisterEvent(select(i, ...))
	end
end

local LinguisticsFrame = CreateFrame("Frame")
LinguisticsFrame.RegisterEvents = RegisterEvents
LinguisticsFrame:RegisterEvents('ADDON_LOADED')

LinguisticsFrame:SetScript("OnEvent", function(frame, event, firstArg, ...)  
  if ((event=="ADDON_LOADED") and (firstArg=="Linguistics")) then

	local  newForce = "1";
  
	if ((LINGUISTICS_FORCE == nil) or (LINGUISTICS_FORCE < newForce)) then
		LINGUISTICS_MASTOG = true
		LINGUISTICS_FORCE = newForce;
		LINGUISTICS_OPTWOWLFR = true
		LINGUISTICS_OPTWOWZON = true
		LINGUISTICS_OPTWOWDUN = true
		LINGUISTICS_OPTWOWRAI = true
		LINGUISTICS_OPTWOWBAT = true
		LINGUISTICS_OPTINT = true
		LINGUISTICS_OPTNUM = true
		LINGUISTICS_OPTPUN = true
		LINGUISTICS_OPTLOL = true
		LINGUISTICS_OPTMRK = true
		LINGUISTICS_OPTCAP = true
		LINGUISTICS_CHAFIL = false
	end

	ChatFrame_AddMessageEventFilter("CHAT_MSG_AFK", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_BATTLEGROUND", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_BATTLEGROUND_LEADER", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_BN_CONVERSATION", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_BN_WHISPER", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_DND", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_EMOTE", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_GUILD", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_OFFICER", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_PARTY", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_PARTY_LEADER", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_RAID", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_RAID_LEADER", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_RAID_WARNING", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_SAY", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_WHISPER", lingChatFilter)
	ChatFrame_AddMessageEventFilter("CHAT_MSG_YELL", lingChatFilter)
end

end)