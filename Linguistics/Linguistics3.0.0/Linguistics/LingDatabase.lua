-- ---------------------------------------------------------------
-- ---------------------------------------------------------------
-- ---- Addon was made by Cryptonym / Bonefish on Ragnaros-EU ----
-- ----------- Please report bugs or tips on Curse.com -----------
-- --------------- E-mail me at: mail@lowerpug.com ---------------
-- ---------------------------------------------------------------

-- Database function
function runDatabase(msg)

	-- Checking for Wow LFR 
	if (LINGUISTICS_OPTWOWLFR == true) then
			msg = gsub(msg, "(%A+)lfr(%A+)", "%1looking for raid%2");
			msg = gsub(msg, "(%A+)lfd(%A+)", "%1looking for dungeon%2");
			msg = gsub(msg, "(%A+)lfg(%A+)", "%1looking for group%2");
			msg = gsub(msg, "(%A+)lfm(%A+)", "%1looking for more%2");
			msg = gsub(msg, "(%A+)lfw(%A+)", "%1looking for work%2");
			msg = gsub(msg, "(%A+)lf(%W+)", "%1looking for%2");
			
			msg = gsub(msg, "(%A+)LFR(%A+)", "%1looking for raid%2");
			msg = gsub(msg, "(%A+)LFD(%A+)", "%1looking for dungeon%2");
			msg = gsub(msg, "(%A+)LFG(%A+)", "%1looking for group%2");
			msg = gsub(msg, "(%A+)LFM(%A+)", "%1looking for more%2");
			msg = gsub(msg, "(%A+)LFW(%A+)", "%1looking for work%2");
			msg = gsub(msg, "(%A+)LF(%W+)", "%1looking for%2");
			
	end
	
	-- Raidmarkers
	if (LINGUISTICS_OPTMRK == true) then
			msg = gsub(msg, "(%A+){white}(%A+)", "%1{rt8}%2");
			msg = gsub(msg, "(%A+){red}(%A+)", "%1{rt7}%2");
			msg = gsub(msg, "(%A+){blue}(%A+)", "%1{rt6}%2");
			msg = gsub(msg, "(%A+){lblue}(%A+)", "%1{rt4}%2");
			msg = gsub(msg, "(%A+){green}(%A+)", "%1{rt4}%2");
			msg = gsub(msg, "(%A+){purple}(%A+)", "%1{rt3}%2");
			msg = gsub(msg, "(%A+){orange}(%A+)", "%1{rt2}%2");
			msg = gsub(msg, "(%A+){nipple}(%A+)", "%1{rt2}%2");
			msg = gsub(msg, "(%A+){yellow}(%A+)", "%1{rt1}%2");
			
			msg = gsub(msg, "(%A+){WHITE}(%A+)", "%1{rt8}%2");
			msg = gsub(msg, "(%A+){RED}(%A+)", "%1{rt7}%2");
			msg = gsub(msg, "(%A+){BLUE}(%A+)", "%1{rt6}%2");
			msg = gsub(msg, "(%A+){LBLUE}(%A+)", "%1{rt4}%2");
			msg = gsub(msg, "(%A+){GREEN}(%A+)", "%1{rt4}%2");
			msg = gsub(msg, "(%A+){PURPLE}(%A+)", "%1{rt3}%2");
			msg = gsub(msg, "(%A+){ORANGE}(%A+)", "%1{rt2}%2");
			msg = gsub(msg, "(%A+){NIPPLE}(%A+)", "%1{rt2}%2");
			msg = gsub(msg, "(%A+){YELLOW}(%A+)", "%1{rt1}%2");
			
	end
	
	-- Checking for WoW Zones
	if (LINGUISTICS_OPTWOWZON == true) then
			msg = gsub(msg, "(%A+)epl(%A+)", "%1Eastern Plaguelands%2");
			msg = gsub(msg, "(%A+)smc(%A+)", "%1Silvermoon City%2");
			msg = gsub(msg, "(%A+)sos(%A+)", "%1Swamp of Sorrows%2");
			msg = gsub(msg, "(%A+)stv(%A+)", "%1Stranglethorn Vale%2");
			msg = gsub(msg, "(%A+)sw(%A+)", "%1Stormwind%2");
			msg = gsub(msg, "(%A+)uc(%A+)", "%1Undercity%2");
			msg = gsub(msg, "(%A+)wpl(%A+)", "%1Western Plaguelands%2");
			msg = gsub(msg, "(%A+)exo(%A+)", "%1Exodar%2");
			msg = gsub(msg, "(%A+)gads(%A+)", "%1Gadgetzan%2");
			msg = gsub(msg, "(%A+)org(%A+)", "%1Orgrimmar%2");
			msg = gsub(msg, "(%A+)tb(%A+)", "%1Thunder Bluff%2");
			msg = gsub(msg, "(%A+)xr(%A+)", "%1Crossroads%2");
			msg = gsub(msg, "(%A+)hfp(%A+)", "%1Hellfire Peninsula%2");
			msg = gsub(msg, "(%A+)shat(%A+)", "%1Shattrath City%2");
			msg = gsub(msg, "(%A+)dala(%A+)", "%1Dalaran%2");						
			msg = gsub(msg, "(%A+)dalaa(%A+)", "%1Dalaran%2");						
			msg = gsub(msg, "(%A+)tbc(%A+)", "%1the Burning Crusade%2");		
			msg = gsub(msg, "(%A+)wotlk(%A+)", "%1Wrath of the Lich King%2");		
			msg = gsub(msg, "(%A+)cata(%A+)", "%1Cataclysm%2");		
			msg = gsub(msg, "(%A+)mop(%A+)", "%1Mists of Pandaria%2");
	end
	
	-- Checking for WoW Dungeons
	if (LINGUISTICS_OPTWOWDUN == true) then			
			msg = gsub(msg, "(%A+)rfc(%A+)", "%1Ragefire Chasm%2");	
			msg = gsub(msg, "(%A+)sfk(%A+)", "%1Shadowfang Keep%2");	
			msg = gsub(msg, "(%A+)bfd(%A+)", "%1Blackfathom Depths%2");	
			msg = gsub(msg, "(%A+)gnomer(%A+)", "%1Gnomeregan%2");	
			msg = gsub(msg, "(%A+)rfk(%A+)", "%1Razorfen Kraul%2");	
			msg = gsub(msg, "(%A+)rfd(%A+)", "%1Razorfen Downs%2");	
			msg = gsub(msg, "(%A+)ulda(%A+)", "%1Uldaman%2");	
			msg = gsub(msg, "(%A+)zf(%A+)", "%1Zul Farrak%2");	
			msg = gsub(msg, "(%A+)brd(%A+)", "%1Blackrock Depths%2");	
			msg = gsub(msg, "(%A+)brs(%A+)", "%1Blackrock Spire%2");	
			msg = gsub(msg, "(%A+)lbrs(%A+)", "%1Lower Blackrock Spire%2");	
			msg = gsub(msg, "(%A+)ubrs(%A+)", "%1Upper Blackrock Spire%2");	
			msg = gsub(msg, "(%A+)scholo(%A+)", "%1Scholomance%2");	
			msg = gsub(msg, "(%A+)za(%A+)", "%1Zul'Aman%2");
			msg = gsub(msg, "(%A+)zg(%A+)", "%1Zul'Gurub%2");				
	end
	
	-- Checking for WoW Raids
	if (LINGUISTICS_OPTWOWRAI == true) then	
			msg = gsub(msg, "(%A+)mc(%A+)", "%1Molten Core%2");	
			msg = gsub(msg, "(%A+)bwl(%A+)", "%1Blackwing Lair%2");	
			msg = gsub(msg, "(%A+)aq20(%A+)", "%1Ahn'Qiraj 20%2");	
			msg = gsub(msg, "(%A+)aq40(%A+)", "%1Ahn'Qiraj 40%2");	
			msg = gsub(msg, "(%A+)kara(%A+)", "%1Karazhan%2");	
			msg = gsub(msg, "(%A+)ssc(%A+)", "%1Serpentshrine Cavern%2");	
			msg = gsub(msg, "(%A+)tk(%A+)", "%1Tempest Keep%2");	
			msg = gsub(msg, "(%A+)bt(%A+)", "%1Black Temple%2");	
			msg = gsub(msg, "(%A+)nax(%A+)", "%1Naxxramas%2");	
			msg = gsub(msg, "(%A+)naxx(%A+)", "%1Naxxramas%2");	
			msg = gsub(msg, "(%A+)eoe(%A+)", "%1Eye of Eternity%2");	
			msg = gsub(msg, "(%A+)ony(%A+)", "%1Onyxia%2");	
			msg = gsub(msg, "(%A+)toc(%A+)", "%1Trial of the Crusader%2");	
			msg = gsub(msg, "(%A+)togc(%A+)", "%1Trial of the Grand Crusader%2");	
			msg = gsub(msg, "(%A+)icc(%A+)", "%1Icecrown Citadel%2");	
			msg = gsub(msg, "(%A+)bh(%A+)", "%1Baradin Hold%2");	
			msg = gsub(msg, "(%A+)bd(%A+)", "%1Blackwing Descent%2");
			msg = gsub(msg, "(%A+)bot(%A+)", "%1Bastion of Twilight%2");
			msg = gsub(msg, "(%A+)tfw(%A+)", "%1Throne of the Four Winds%2");
			msg = gsub(msg, "(%A+)fl(%A+)", "%1Firelands%2");
			msg = gsub(msg, "(%A+)ds(s?%A+)", "%1Dragon Soul%2");
			msg = gsub(msg, "(%A+)mv(%A+)", "%1Mogu'shan Vaults%2");
			msg = gsub(msg, "(%A+)hf(%A+)", "%1Heart of Fear%2");
			msg = gsub(msg, "(%A+)tes(%A+)", "%1Terrace of Endless Spring%2");
			msg = gsub(msg, "(%A+)tot(%A+)", "%1Throne of Thunder%2");
			msg = gsub(msg, "(%A+)soo(%A+)", "%1Siege of Orgrimmar%2");
	end
	
	-- Checking for WoW Battlegrounds
	if (LINGUISTICS_OPTWOWBAT == true) then	
			msg = gsub(msg, "(%A+)ab(%A+)", "%1Arathi Basin%2");
			msg = gsub(msg, "(%A+)av(%A+)", "%1Alterac Valley%2");
			msg = gsub(msg, "(%A+)eots(%A+)", "%1Eye of the Storm%2");
			msg = gsub(msg, "(%A+)wsg(%A+)", "%1Warsong Gulch%2");
			msg = gsub(msg, "(%A+)sota(%A+)", "%1Strand of the Ancients%2");
			msg = gsub(msg, "(%A+)ioc(%A+)", "%1Isle of Conquest%2");
			msg = gsub(msg, "(%A+)bfg(%A+)", "%1Battle for Gilneas%2");
			msg = gsub(msg, "(%A+)tp(%A+)", "%1Twin Peaks%2");
			msg = gsub(msg, "(%A+)ssm(%A+)", "%1Silvershard Mines%2");
			msg = gsub(msg, "(%A+)tok(%A+)", "%1Temple of Kotmogu%2");
			msg = gsub(msg, "(%A+)wg(%A+)", "%1Wintergrasp%2");
			msg = gsub(msg, "(%A+)tb(%A+)", "%1Tol Barad%2");
			msg = gsub(msg, "(%A+)rbg(%A+)", "%1Rated Battleground%2");
			msg = gsub(msg, "(%A+)rbgs(%A+)", "%1Rated Battlegrounds%2");
			msg = gsub(msg, "(%A+)ww(%A+)", "%1Waterworks%2");
			msg = gsub(msg, "(%A+)wv(%A+)", "%1Wardens Vigil%2");
			msg = gsub(msg, "(%A+)icg(%A+)", "%1Ironclad Garrison%2");
			msg = gsub(msg, "(%A+)bs(%A+)", "%1Blacksmith%2");
			msg = gsub(msg, "(%A+)lm(%A+)", "%1Lumber Mill%2");
	end
	
	-- Filter Internet
	if (LINGUISTICS_OPTLOL == true) then
                        msg = gsub(msg, "(%A+)BH(%A+)", "%1Battered Hilt%2");
                        msg = gsub(msg, "(%A+)pvp(%A+)", "%1Player versus Player%2");
                        msg = gsub(msg, "(%A+)pug(%A+)", "%1Pick up group%2");
                        msg = gsub(msg, "(%A+)jc(%A+)", "%1Jewelcrafter%2");
                        msg = gsub(msg, "(%A+)int(%A+)", "%1Intellect%2");
                        msg = gsub(msg, "(%A+)IF(%A+)", "%1Ironforge%2");
                        msg = gsub(msg, "(%A+)icd(%A+)", "%1Internal Cooldown%2");
                        msg = gsub(msg, "(%A+)hk(%A+)", "%1Honorable kills%2");
                        msg = gsub(msg, "(%A+)gcd(%A+)", "%1Global Cooldown%2");
                        msg = gsub(msg, "(%A+)fps(%A+)", "%1Frames per second%2");
                        msg = gsub(msg, "(%A+)FD(%A+)", "%1Feign Death%2");
                        msg = gsub(msg, "(%A+)dw(%A+)", "%1Don't worry%2");
                        msg = gsub(msg, "(%A+)AH(%A+)", "%1Auction House%2");
                        msg = gsub(msg, "(%A+)agi(%A+)", "%1Agility%2");
                        msg = gsub(msg, "(%A+)alch(%A+)", "%1Alchemy%2");
                        msg = gsub(msg, "(%A+)BF(%A+)", "%1Boyfriend%2");
                        msg = gsub(msg, "(%A+)GF(%A+)", "%1Girlfriend%2");
                        msg = gsub(msg, "(%A+)OT(%A+)", "%1Off Tank%2");
                        msg = gsub(msg, "(%A+)MT(%A+)", "%1Main Tank%2");
                        msg = gsub(msg, "(%A+)lk(%A+)", "%1The Lich King%2");
                        msg = gsub(msg, "(%A+)sindy(%A+)", "%1Sindragosa%2");
                        msg = gsub(msg, "(%A+)pp(%A+)", "%1Professor Putricide%2");
                        msg = gsub(msg, "(%A+)dbs(%A+)", "%1Deathbringer Saurfang%2");
                        msg = gsub(msg, "(%A+)rot(%A+)", "%1DRotface%2");
                        msg = gsub(msg, "(%A+)ret(%A+)", "%1Retribution%2");
                        msg = gsub(msg, "(%A+)resto(%A+)", "%1Restoration%2");
                        msg = gsub(msg, "(%A+)hpala(%A+)", "%1Holy Paladin%2");
                        msg = gsub(msg, "(%A+)rpala(%A+)", "%1Retribution Paladin%2");
                        msg = gsub(msg, "(%A+)rdudu(%A+)", "%1Restoartion Druid%2");
                        msg = gsub(msg, "(%A+)dk(%A+)", "%1Death Knight%2");
                        msg = gsub(msg, "(%A+)engi(%A+)", "%1Engineering%2");
			msg = gsub(msg, "(%A+)gn(%A+)", "%1Good Night%2");
                        msg = gsub(msg, "(%A+)grun(%A+)", "%1Guild run%2");
			msg = gsub(msg, "(%A+)lawl(%A+)", "%1haha%2");
			msg = gsub(msg, "(%A+)lma%o+(%A+)", "%1haha%2");
			msg = gsub(msg, "(%A+)lmfa%o+(%A+)", "%1haha%2");
			msg = gsub(msg, "(%A+)r%o+fl(%A+)", "%1haha%2");
			msg = gsub(msg, "(%A+)aoe(%A+)", "%1area of effect%2");
			msg = gsub(msg, "(%A+)w8st(%A+)", "%1waste%2");
			msg = gsub(msg, "(%A+)ap(%A+)", "%1attack power%2");
			msg = gsub(msg, "(%A+)bg(%A+)", "%1%battleground2");
			msg = gsub(msg, "(%A+)boe(%A+)", "%1Bind on Equip%2");
			msg = gsub(msg, "(%A+)bis(%A+)", "%1best in slot%2");
			msg = gsub(msg, "(%A+)bop(%A+)", "%1%Bind on Pickup2");
			msg = gsub(msg, "(%A+)bou(%A+)", "%1Bind on Use%2");
			msg = gsub(msg, "(%A+)boa(%A+)", "%1Bind on Account%2");
			msg = gsub(msg, "(%A+)cc(%A+)", "%1croud control%2");
                        msg = gsub(msg, "(%A+)fu(%A+)", "%1fuck you%2");
			msg = gsub(msg, "(%A+)fth(%A+)", "%1for the Horde%2");
                        msg = gsub(msg, "(%A+)fyi(%A+)", "%1for your information%2");
			msg = gsub(msg, "(%A+)fta(%A+)", "%1for the Alliance%2");
			msg = gsub(msg, "(%A+)irl(%A+)", "%1in real life%2");
                        msg = gsub(msg, "(%A+)RO(%A+)", "%1Raid Over%2");
			msg = gsub(msg, "(%A+)lom(%A+)", "%1low on mana%2");
			msg = gsub(msg, "(%A+)mats(%A+)", "%1materials%2");
			msg = gsub(msg, "(%A+)ml(%A+)", "%1master looter%2");
			msg = gsub(msg, "(%A+)mh(%A+)", "%1main-hand%2");
                        msg = gsub(msg, "(%A+)nc(%A+)", "%1no comment%2");
			msg = gsub(msg, "(%A+)oom(%A+)", "%1out of mana%2");
			msg = gsub(msg, "(%A+)pot(%A+)", "%1potion%2");
			msg = gsub(msg, "(%A+)pots(%A+)", "%1potions%2");
			msg = gsub(msg, "(%A+)lil(%A+)", "%1little%2");
			msg = gsub(msg, "(%A+)res(%A+)", "%1resurrect%2");
			msg = gsub(msg, "(%A+)ress(%A+)", "%1resurrect%2");
			msg = gsub(msg, "(%A+)rez(%A+)", "%1resurrect%2");
			msg = gsub(msg, "(%A+)rezzing(%A+)", "%1resurrecting%2");
			msg = gsub(msg, "(%A+)rezing(%A+)", "%1resurrecting%2");
			msg = gsub(msg, "(%A+)rp(%A+)", "%1roleplay%2");
			msg = gsub(msg, "(%A+)ui(%A+)", "%1user interface%2");
			msg = gsub(msg, "(%A+)afaik(%A+)", "%1%as far as i know2");
			msg = gsub(msg, "(%A+)afk(%A+)", "%1away from keyboard%2");
			msg = gsub(msg, "(%A+)atm(%A+)", "%1at the moment%2");
			msg = gsub(msg, "(%A+)bath(%A+)", "%1bathroom%2");
			msg = gsub(msg, "(%A+)brt(%A+)", "%1be right there%2");
			msg = gsub(msg, "(%A+)btw(%A+)", "%1by the way%2");
			msg = gsub(msg, "(%A+)cba(%A+)", "%1can't be arsed%2");
			msg = gsub(msg, "(%A+)diaf(%A+)", "%1die in a fire%2");
			msg = gsub(msg, "(%A+)ffs(%A+)", "%1for fucks sake%2");
			msg = gsub(msg, "(%A+)ftl(%A+)", "%1for the loss%2");
			msg = gsub(msg, "(%A+)ftw(%A+)", "%1for the win%2");
			msg = gsub(msg, "(%A+)gtfo(%A+)", "%1get the fuck out%2");
			msg = gsub(msg, "(%A+)garosh(%A+)", "%1garrosh%2");	
			msg = gsub(msg, "(%A+)garros(%A+)", "%1garrosh%2");	
			msg = gsub(msg, "(%A+)garoshe(%A+)", "%1garrosh%2");	
			msg = gsub(msg, "(%A+)idc(%A+)", "%1i dont care%2");
			msg = gsub(msg, "(%A+)idk(%A+)", "%1%i dont know%2");
			msg = gsub(msg, "(%A+)iirc(%A+)", "%1if i recall correct%2");
			msg = gsub(msg, "(%A+)imho(%A+)", "%1in my hunble opinion%2");
			msg = gsub(msg, "(%A+)imo(%A+)", "%1in my opinion%2");
			msg = gsub(msg, "(%A+)inc(%A+)", "%1incoming%2");
			msg = gsub(msg, "(%A+)omw(%A+)", "%1on my way%2");
			msg = gsub(msg, "(%A+)stfu(%A+)", "%1shut the fuck up%2");
			msg = gsub(msg, "(%A+)tbh(%A+)", "%1to be honest%2");
			msg = gsub(msg, "(%A+)wc(%A+)", "%1wrong channel%2");
			msg = gsub(msg, "(%A+)wtb(%A+)", "%1want to buy%2");
			msg = gsub(msg, "(%A+)wts(%A+)", "%1want to sell%2");
			msg = gsub(msg, "(%A+)wtf(%A+)", "%1what the fuck%2");
			msg = gsub(msg, "(%A+)wth(%A+)", "%1what the hell%2");
			msg = gsub(msg, "(%A+)wtt(%A+)", "%1want to trade%2");
			msg = gsub(msg, "(%A+)gg(%A+)", "%1good game%2");
			msg = gsub(msg, "(%A+)gj(%A+)", "%1good job%2");
			msg = gsub(msg, "(%A+)imba(%A+)", "%1imbalanced%2");
			msg = gsub(msg, "(%A+)l2p(%A+)", "%1learn to play%2");
			msg = gsub(msg, "(%A+)ea(%A+)", "%1each%2");
			msg = gsub(msg, "(%A+)5sr(%A+)", "%1five-second rule%2");
			msg = gsub(msg, "(%A+)jp(%A+)", "%1Justice Points%2");
			msg = gsub(msg, "(%A+)vp(%A+)", "%1Valor Points%2");
			msg = gsub(msg, "(%A+)cp(%A+)", "%1Conquest Points%2");
			msg = gsub(msg, "(%d+)g(%A+)", "%1 gold%2");
			msg = gsub(msg, "(%d+)k(%A+)", "%1,000%2");
			msg = gsub(msg, "(%A+)omg(%A+)", "%1oh my god%2");
			msg = gsub(msg, "(%A+)omfg(%A+)", "%1oh my fucking god%2");
			msg = gsub(msg, "(%A+)fos(%A+)", "%1Feat of Strength%2");
                        msg = gsub(msg, "(%A+)bb(%A+)", "%1Bye Bye%2");
			msg = gsub(msg, "(%A+)brb(%A+)", "%1be right back%2");
			msg = gsub(msg, "(%A+)ttyl(%A+)", "%1talk to you later%2");
			msg = gsub(msg, "(%A+)efc(%A+)", "%1enemy flag carrier%2");
			msg = gsub(msg, "(%A+)fc(%A+)", "%1flag carrier%2");
			msg = gsub(msg, "(%A+)ily(%A+)", "%1i love you%2");
			msg = gsub(msg, "(%A+)dis(%A+)", "%1this%2");
			msg = gsub(msg, "(%A+)gl(%A+)", "%1good luck%2");
			msg = gsub(msg, "(%A+)gtg(%A+)", "%1got to go%2");
			msg = gsub(msg, "(%A+)y(%A+)", "%1yes%2");
			msg = gsub(msg, "(%A+)ign(%A+)", "%1in-game name%2");
			msg = gsub(msg, "(%A+)jk(%A+)", "%1just kidding%2");
			msg = gsub(msg, "(%A+)j/k(%A+)", "%1just kidding%2");
			msg = gsub(msg, "(%A+)dg(%A+)", "%1dungeon%2");
			msg = gsub(msg, "(%A+)mt(%A+)", "%1mistell%2");
                        msg = gsub(msg, "(%A+)w/e(%A+)", "%1whatever%2");
	end
	
	-- Checking for Spelling Options
	if (LINGUISTICS_OPTINT == true) then
			msg = gsub(msg, "(%A+)4ce(%A+)", "%1force%2");
			msg = gsub(msg, "(%A+)ofc(%A+)", "%1of course%2");
			msg = gsub(msg, "(%A+)1h(%A+)", "%1one-handed%2");
			msg = gsub(msg, "(%A+)2h(%A+)", "%1two-handed%2");
			msg = gsub(msg, "(%A+)n1(%A+)", "%1nice one%2");
                        msg = gsub(msg, "(%A+)achiev(s?%A+)", "%1achievement%2");
                        msg = gsub(msg, "(%A+)achiv(s?%A+)", "%1achievement%2");
			msg = gsub(msg, "(%A+)achive(s?%A+)", "%1achievement%2");
			msg = gsub(msg, "(%A+)achieve(s?%A+)", "%1achievement%2");
			msg = gsub(msg, "(%A+)achivment(s?%A+)", "%1achievement%2");
			msg = gsub(msg, "(%A+)achivement(s?%A+)", "%1achievement%2");
			msg = gsub(msg, "(%A+)hc(%A+)", "%1Heroic%2");	
			msg = gsub(msg, "(%A+)hc%'?s(%A+)", "%1Heroics%2");				
			msg = gsub(msg, "(%A+)hs(%A+)", "%1hearthstone%2");				
			msg = gsub(msg, "(%A+)chu(%A+)", "%1you%2");
			msg = gsub(msg, "(%A+)its(%A+)", "%1it's%2");
			msg = gsub(msg, "(%A+)thx(%A+)", "%1thank you%2");
			msg = gsub(msg, "(%A+)panda(%A+)", "%1pandaren%2");
			msg = gsub(msg, "(%A+)ty(%A+)", "%1thank you%2");
			msg = gsub(msg, "(%A+)np(%A+)", "%1no problem%2");
                        msg = gsub(msg, "(%A+)rly(%A+)", "%1really%2");
			msg = gsub(msg, "(%A+)rlly(%A+)", "%1really%2");
                        msg = gsub(msg, "(%A+)sry(%A+)", "%1sorry%2");
			msg = gsub(msg, "(%A+)srry(%A+)", "%1sorry%2");
			msg = gsub(msg, "(%A+)esp(%A+)", "%1especially%2");
			msg = gsub(msg, "(%A+)i%s*c(%A+)", "%1i see%2");
			msg = gsub(msg, "(%A+)liek(%A+)", "%1like%2");
			msg = gsub(msg, "(%A+)waddup(%A+)", "%1what's up%2");
			msg = gsub(msg, "(%A+)cmon(%A+)", "%1c'mon%2");
			msg = gsub(msg, "(%A+)sowwy(%A+)", "%1sorry%2");
			msg = gsub(msg, "(%A+)dat(%A+)", "%1that%2");
                        msg = gsub(msg, "(%A+)prolly(%A+)", "%1probably%2");
			msg = gsub(msg, "(%A+)probly(%A+)", "%1probably%2");
			msg = gsub(msg, "(%A+)argueing(%A+)", "%1arguing%2");
			msg = gsub(msg, "(%A+)ppl(%A+)", "%1people%2");
			msg = gsub(msg, "(%A+)peeps(%A+)", "%1people%2");
			msg = gsub(msg, "(%A+)m8(%A+)", "%1mate%2");
			msg = gsub(msg, "(%A+)yh(%A+)", "%1yeah%2");
			msg = gsub(msg, "(%A+)ye(%A+)", "%1yeah%2");
			msg = gsub(msg, "(%A+)ya(%A+)", "%1yeah%2");
			msg = gsub(msg, "(%A+)yah(%A+)", "%1yeah%2");
			msg = gsub(msg, "(%A+)u(%A+)", "%1you%2");
			msg = gsub(msg, "(%A+)flx(%A+)", "%1flex%2");
			msg = gsub(msg, "(%A+)gimme(%A+)", "%1give me%2");
			msg = gsub(msg, "(%A+)gotta(%A+)", "%1got to%2");
			msg = gsub(msg, "(%A+)obv(%A+)", "%1obvious%2");
			msg = gsub(msg, "(%A+)pls(%A+)", "%1please%2");
			msg = gsub(msg, "(%A+)plz(%A+)", "%1please%2");
			msg = gsub(msg, "(%A+)plox(%A+)", "%1please%2");
			msg = gsub(msg, "(%A+)plesae(%A+)", "%1please%2");
			msg = gsub(msg, "(%A+)inv(%A+)", "%1invite%2");
			msg = gsub(msg, "(%A+)whos(%A+)", "%1who's%2");
			msg = gsub(msg, "(%A+)r(%A+)", "%1are%2");
			msg = gsub(msg, "(%A+)pty(%A+)", "%1party%2");
			msg = gsub(msg, "(%A+)prty(%A+)", "%1party%2");
			msg = gsub(msg, "(%A+)etc(%A+)", "%1etcetera%2");
			msg = gsub(msg, "(%A+)g(%A+)", "%1gold%2");
			msg = gsub(msg, "(%A+)gz(%A+)", "%1congratulations%2");
			msg = gsub(msg, "(%A+)gratz(%A+)", "%1congratulations%2");
			msg = gsub(msg, "(%A+)grats(%A+)", "%1congratulations%2");
			msg = gsub(msg, "(%A+)grtz(%A+)", "%1congratulations%2");
			msg = gsub(msg, "(%A+)gtz(%A+)", "%1congratulations%2");
			msg = gsub(msg, "(%A+)n%o+(%A+)", "%1no%2");
			msg = gsub(msg, "(%A+)gief(%A+)", "%1give%2");
			msg = gsub(msg, "(%A+)noes(%A+)", "%1no%2");
			msg = gsub(msg, "(%A+)nay(%A+)", "%1no%2");
			msg = gsub(msg, "(%A+)srs(%A+)", "%1serious%2");
			msg = gsub(msg, "(%A+)srsly(%A+)", "%1seriously%2");
			msg = gsub(msg, "(%A+)nop(%A+)", "%1no%2");
			msg = gsub(msg, "(%A+)ive(%A+)", "%1I've%2");
			msg = gsub(msg, "(%A+)im(%A+)", "%1I'm%2");
			msg = gsub(msg, "(%A+)horde(%A+)", "%1Horde%2");
			msg = gsub(msg, "(%A+)alliance(%A+)", "%1Alliance%2");
			msg = gsub(msg, "(%A+)lvl(%A+)", "%1level%2");
			msg = gsub(msg, "(%A+)lvls(%A+)", "%1levels%2");
			msg = gsub(msg, "(%A+)lvling(%A+)", "%1leveling%2");
			msg = gsub(msg, "(%A+)hes(%A+)", "%1he's%2");
			msg = gsub(msg, "(%A+)shes(%A+)", "%1she's%2");
			msg = gsub(msg, "(%A+)exp(%A+)", "%1experience%2");
			msg = gsub(msg, "(%A+)exp%Sd(%A+)", "%1experienced%2");
			msg = gsub(msg, "(%A+)expirienced(%A+)", "%1experienced%2");
			msg = gsub(msg, "(%A+)expirienced(%A+)", "%1experienced%2");
			msg = gsub(msg, "(%A+)experianced(%A+)", "%1experienced%2");
			msg = gsub(msg, "(%A+)expirianced(%A+)", "%1experienced%2");
			msg = gsub(msg, "(%A+)expirience(%A+)", "%1experience%2");
			msg = gsub(msg, "(%A+)experiance(%A+)", "%1experience%2");
			msg = gsub(msg, "(%A+)expiriance(%A+)", "%1experience%2");
			msg = gsub(msg, "(%A+)alot(%A+)", "%1a lot%2");
			msg = gsub(msg, "(%A+)ofcourse(%A+)", "%1of course%2");
			msg = gsub(msg, "(%A+)scen(%A+)", "%1scenario%2");
			msg = gsub(msg, "(%A+)scens(%A+)", "%1scenarios%2");
			msg = gsub(msg, "(%A+)hscen(%A+)", "%1Heroic scenario%2");
			msg = gsub(msg, "(%A+)hscens(%A+)", "%1Heroic scenarios%2");
			msg = gsub(msg, "(%A+)%/w(%A+)", "%1whisper%2");
			msg = gsub(msg, "(%A+)pm(%A+)", "%1whisper%2");
			msg = gsub(msg, "(%A+)gv(%A+)", "%1guild vault%2");
			msg = gsub(msg, "(%A+)gb(%A+)", "%1guild bank%2");
			msg = gsub(msg, "(%A+)guildbank(%A+)", "%1guild bank%2");
			msg = gsub(msg, "(%A+)guildvault(%A+)", "%1guild vault%2");
			msg = gsub(msg, "(%A+)norm(%A+)", "%1normal%2");
			msg = gsub(msg, "(%A+)tho(%A+)", "%1though%2");
			msg = gsub(msg, "(%A+)summ(%A+)", "%1summon%2");
			msg = gsub(msg, "(%A+)rdy(%A+)", "%1ready%2");
			msg = gsub(msg, "(%A+)rep(%A+)", "%1reputation%2");
			msg = gsub(msg, "(%A+)dailies(%A+)", "%1daily quests%2");
			msg = gsub(msg, "(%A+)dailys(%A+)", "%1daily quests%2");
			msg = gsub(msg, "(%A+)xfer(%A+)", "%1transfer%2");
			msg = gsub(msg, "(%A+)imma(%A+)", "%1I'll%2");
			msg = gsub(msg, "(%A+)xmog(%A+)", "%1transmog%2");
			msg = gsub(msg, "(%A+)mog(%A+)", "%1transmog%2");
			msg = gsub(msg, "(%A+)tmog(%A+)", "%1transmog%2");
			msg = gsub(msg, "(%A+)resrv(%A+)", "%1reserved%2");
			msg = gsub(msg, "(%A+)resv(%A+)", "%1reserved%2");
			msg = gsub(msg, "(%A+)belf(%A+)", "%1Blood Elf%2");
			msg = gsub(msg, "(%A+)nelf(%A+)", "%1Night Elf%2");
			msg = gsub(msg, "(%A+)nm(%A+)", "%1normal%2");
			msg = gsub(msg, "(%A+)whats(%A+)", "%1what's%2");
			msg = gsub(msg, "(%A+)thats(%A+)", "%1that's%2");
			msg = gsub(msg, "(%A+)bout(%A+)", "%1about%2");
			msg = gsub(msg, "(%A+)pwn(%A+)", "%1powned%2");
			msg = gsub(msg, "(%A+)pwned(%A+)", "%1powned%2");
			msg = gsub(msg, "(%A+)pwnd(%A+)", "%1powned%2");
			msg = gsub(msg, "(%A+)ownd(%A+)", "%1owned%2");
			msg = gsub(msg, "(%A+)ilvl(%A+)", "%1item level%2");
			msg = gsub(msg, "(%A+)ilev(%A+)", "%1item level%2");
			msg = gsub(msg, "(%A+)ilevel(%A+)", "%1item level%2");
			msg = gsub(msg, "(%A+)ze(%A+)", "%1the%2");
			msg = gsub(msg, "(%A+)teh(%A+)", "%1the%2");
			msg = gsub(msg, "(%A+)coudl(%A+)", "%1could%2");
			msg = gsub(msg, "(%A+)coz(%A+)", "%1because%2");
			msg = gsub(msg, "(%A+)woudl(%A+)", "%1would%2");
			msg = gsub(msg, "(%A+)sham(%A+)", "%1shaman%2");
			msg = gsub(msg, "(%A+)shammy(%A+)", "%1shaman%2");
			msg = gsub(msg, "(%A+)warr(%A+)", "%1warrior%2");
			msg = gsub(msg, "(%A+)gnna(%A+)", "%1gonna%2");
			msg = gsub(msg, "(%A+)stahp(%A+)", "%1stop%2");
			msg = gsub(msg, "(%A+)prot(%A+)", "%1protection%2");
			msg = gsub(msg, "(%A+)enha(%A+)", "%1enhancement%2");
			msg = gsub(msg, "(%A+)enhanc(%A+)", "%1enhancement%2");
			msg = gsub(msg, "(%A+)enhance(%A+)", "%1enhancement%2");
			msg = gsub(msg, "(%A+)disc(%A+)", "%1discipline%2");
			msg = gsub(msg, "(%A+)vent(%A+)", "%1Ventrilo%2");
			msg = gsub(msg, "(%A+)ts(%A+)", "%1Teamspeak%2");
			msg = gsub(msg, "(%A+)some%s*1(%A+)", "%1someone%2");
			msg = gsub(msg, "(%A+)wanna(%A+)", "%1want to%2");
			msg = gsub(msg, "(%A+)some%s*one(%A+)", "%1someone%2");
			msg = gsub(msg, "(%A+)ill(%A+)", "%1I'll%2");		
			msg = gsub(msg, "(%A+)gonna(%A+)", "%1going to%2");	
			msg = gsub(msg, "(%A+)undercity(%A+)", "%1Undercity%2");
			msg = gsub(msg, "(%A+)orgrimmar(%A+)", "%1Orgrimmar%2");
			msg = gsub(msg, "(%A+)ogrimar(%A+)", "%1Orgrimmar%2");
			msg = gsub(msg, "(%A+)ogrimmar(%A+)", "%1Orgrimmar%2");
			msg = gsub(msg, "(%A+)orgrimar(%A+)", "%1Orgrimmar%2");
			msg = gsub(msg, "(%A+)thunder%s*bluff(%A+)", "%1Thunder Bluff%2");
			msg = gsub(msg, "(%A+)silvermoon(%A+)", "%1Silvermoon%2");
			msg = gsub(msg, "(%A+)ironforge(%A+)", "%1Ironforge%2");
			msg = gsub(msg, "(%A+)stormwind(%A+)", "%1Stormwind%2");
			msg = gsub(msg, "(%A+)darnassus(%A+)", "%1Darnassus%2");
			msg = gsub(msg, "(%A+)exodar(%A+)", "%1Exodar%2");
			msg = gsub(msg, "(%A+)gilneas(%A+)", "%1Gilneas%2");
			msg = gsub(msg, "(%A+)dalaran(%A+)", "%1Dalaran%2");
			msg = gsub(msg, "(%A+)shattrath(%A+)", "%1Shattrath%2");
		--		msg = gsub(msg, "(%A+)(%A+)", "%1%2");	

		-- Partitial or one-letter words
			msg = gsub(msg, "(%a+)dnt(%A+)", "%1dn't%2"); -- Changes dnt to dn't if dnt is the end of a word
			msg = gsub(msg, "(%a+)yre(%A+)", "%1y're%2"); -- yre ~ y're
			msg = gsub(msg, "(%a+)snt(%A+)", "%1sn't%2"); -- snt ~ sn't				
			msg = gsub(msg, "(%A+%a?)ont(%A+)", "%1on't%2"); -- Changes ont to on't if ont has only one letter infront of it and is at the end of a word
			msg = gsub(msg, "(%A+%a?)ant(%A+)", "%1an't%2"); -- Changes ant ~ an't
			msg = gsub(msg, "(%A+)u(%A+)", "%1you%2"); -- u ~ you
			msg = gsub(msg, "(%A+)i(%A+)", "%1I%2"); -- i ~ I

		-- Changing back exceptions
			msg = gsub(msg, "wan't", " want");
			msg = gsub(msg, "fon't", " font");
	end
		
	-- Checking for Numbers Options
	if (LINGUISTICS_OPTNUM == true) then
		-- Numbers 1-9
			msg = gsub(msg, "(%s+)1(%s+)", "%1one%2"); -- Changes if the number is surrounded by spaces
			msg = gsub(msg, "(%s+)2(%s+)", "%1two%2");
			msg = gsub(msg, "(%s+)3(%s+)", "%1three%2");
			msg = gsub(msg, "(%s+)4(%s+)", "%1four%2");
			msg = gsub(msg, "(%s+)5(%s+)", "%1five%2");
			msg = gsub(msg, "(%s+)6(%s+)", "%1six%2");
			msg = gsub(msg, "(%s+)7(%s+)", "%1seven%2");
			msg = gsub(msg, "(%s+)8(%s+)", "%1eight%2");
			msg = gsub(msg, "(%s+)9(%s+)", "%1nine%2");
	end	

	-- Send it back
	return msg;
	
end

function runPuncDatabase(msg,system,event)
			-- Check for Punctuation Options
				if (LINGUISTICS_OPTPUN == true) then

					if (system==nil) then system = "notnil"; end
					if (event==nil) then event = "notnil"; end

					-- Adding Punctuation
					msg = gsub(msg, ",+(%S)", ", %1"); -- Add space after comma if letter is glued
					msg = gsub(msg, ",+%s+(%d+)", ",%1"); -- Remove space after comma if it's a number
					msg = gsub(msg, "%s+(,+)", "%1"); -- Remove space(s) before commas
					msg = gsub(msg, "%s+(%.+)", "%1"); -- Remove space(s) before dots
					msg = gsub(msg, "%s+(%?+)", "%1"); -- Remove space(s) before question marks
					msg = gsub(msg, "%s+(%!+)", "%1"); -- Remove space(s) before exclaimation marks
					msg = gsub(msg, "(%.+)(%a+)", "%1 %2"); -- After dot use space if letter is present and glued
					msg = gsub(msg, "(%P%P)$", "%1."); -- End string with dot if no punctuation in the last two characters(preventing smileys with a dot behind it).
					msg = gsub(msg, "%s+haha", ", haha");
					msg = gsub(msg, "haha%s+", "haha, ");
				
				-- Final capitalization
					if ((system~="EMOTE") and (event~="CHAT_MSG_EMOTE")) then msg = gsub(msg, "^%a", strupper); end -- Let the start of string start with a capital if letter but not when custom emote
					msg = gsub(msg, "%.%s+%a", strupper); -- After a dot use a capital if letter
					msg = gsub(msg, "%!%s+%a", strupper); -- After a question mark use a capital if letter
					msg = gsub(msg, "%?%s+%a", strupper); -- After a exclamation mark use a capital if letter
					msg = gsub(msg, "%.%s+Com(%A)", ".com%1"); -- change back .com for websites
					msg = gsub(msg, "%.%s+Co%.%s+Uk(%A)", ".co.uk%1"); -- change back .co.uk for websites
					msg = gsub(msg, "(%A+)Http(%A+)", "%1http%2"); -- Http to http
					msg = gsub(msg, "(%A+)Www(%A+)", "%1www%2"); -- Www to www
			end
		
	-- Send it back
	return msg;
	
end

function runFinalDatabase(msg,system,event)

		-- Checking for Caps
		if (LINGUISTICS_OPTCAP == false) then
			-- Words with one or more caps get nerfed to only first letter in caps
				msg = gsub(msg, "(%w*%u+%w*)", nerfCaps);	
		end

			-- Adding the Linguistics controlmessages, making the string start and end with our mark and a space.
				msg = "LINGUISTICSCODE " .. msg .. " LINGUISTICSCODE";

				-- Run database
					msg = runDatabase(msg);
							
			-- Removing the LinguisticsSpeak controlmessages
				msg = gsub(msg, "%s?LINGUISTICSCODE%s?", "");

				-- Run Punctuation database
					msg = runPuncDatabase(msg,system,event);		

			-- Adding the Linguistics controlmessages, making the string start and end with our mark and a space.
				msg = "LINGUISTICSCODE " .. msg .. " LINGUISTICSCODE";
			
				-- Correcting smileys
					msg = gsub(msg, "(%A+)Xd(%A+)", " xD "); -- xD				
					msg = gsub(msg, "(%A+)xd(%A+)", " xD "); -- xD				
					msg = gsub(msg, "(%A+)XD(%A+)", " xD "); -- xD				
			
			-- Removing the LinguisticsSpeak controlmessages
				msg = gsub(msg, "%s?LINGUISTICSCODE%s?", "");
				
	-- Send it back
	return msg;
				
end