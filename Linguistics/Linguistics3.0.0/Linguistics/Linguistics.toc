## Interface: 60100 
## Title:|cffffd100Linguistics v3.0|r |cff69ccf0[English]|r
## Author: Bonefish (Ragnaros-EU)
## Version: 3.0
## Notes: Removes abbreviations and adds punctuation.
## DefaultState: Enabled
## SavedVariables: LINGUISTICS_FORCE, LINGUISTICS_MASTOG, LINGUISTICS_OPTWOWLFR, LINGUISTICS_CHAFIL, LINGUISTICS_OPTWOWZON, LINGUISTICS_OPTWOWDUN, LINGUISTICS_OPTWOWRAI, LINGUISTICS_OPTWOWBAT, LINGUISTICS_OPTINT, LINGUISTICS_OPTLOL, LINGUISTICS_OPTCAP, LINGUISTICS_OPTMRK, LINGUISTICS_OPTNUM, LINGUISTICS_OPTPUN
## X-Category: Chat/Communication
## X-License: All Rights Reserved
## X-Curse-Packaged-Version: v3.0
## X-Curse-Project-Name: Linguistics
## X-Curse-Project-ID: linguistics
## X-Curse-Repository-ID: wow/linguistics/mainline

LingDatabase.lua
Linguistics.lua
SimpleOptions.lua
