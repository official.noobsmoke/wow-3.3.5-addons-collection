## Interface: 40300
## Title: LibRareIds-1.0
## Author: dlui
## Version: 1.0.5
## Notes: A table of all rare/rare-elites in game.
## X-Curse-Packaged-Version: 1.0.5
## X-Curse-Project-Name: LibRareIds-1.0
## X-Curse-Project-ID: librareids-1-0
## X-Curse-Repository-ID: wow/librareids-1-0/mainline

LibStub\LibStub.lua
LibRareIds-1.0.xml