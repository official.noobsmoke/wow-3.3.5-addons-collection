--Author: Eldrikt
--Created 23.11.2012

--init the addon as a Ace Addon
BossRotation = LibStub("AceAddon-3.0"):NewAddon("BossRotation", "AceConsole-3.0", "AceEvent-3.0", "AceHook-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale("BossRotation") --the table with the localized strings

--prepare for FuBar
local LDB = LibStub("LibDataBroker-1.1", true)
local LDBIcon = LibStub("LibDBIcon-1.0", true)

LL_DEBUG = false;

--the local options
local options = {
	name = "BossRotation",
	handler = BossRotation,
	type = "group",
	args = {
		General = {
			type = "group",
			name = L["General Settings"],
			desc = L["General Settings"],
			handler = BossRotation,
			args = {
				UseDebugLog = {
					type = "toggle", 									--check box
					name = L["Use debug log"], 					--title to be displayed
					desc = L["If selected logs addon communication in an extra debug log frame"],  --tooltip description
					get = "IsUseDebugLog",								--get method
					set = "ToggleUseDebugLog",							--set method
					order = 1,
				},
				ShowDebugLog = {
					type = "toggle", 									--check box
					name = L["Show debug log"], 					--title to be displayed
					desc = L["Show or hide the extra debug log frame"],  --tooltip description
					get = "IsShowDebugLog",								--get method
					set = "ToggleShowDebugLog",							--set method
					order = 1,
				},

				showMinimapIcon = {
					type = "toggle", 										--check box
					name = L["showMinimapIcon"], 							--title to be displayed
					desc = L["showMinimapIconDesc"],  						--tooltip description
					get = "IsShowMinimapIcon",								--get method
					set = "ToggleShowMinimapIcon",							--set method
					order = 5,
				},
			},
		},
	},
}

--the defaults
local defaults = {
	profile = {
		General = {
			UseDebugLog = false,
			ShowDebugLog = false,
			showMinimapIcon = true,
		},
		LDBIconStorage = {}, -- LibDBIcon storage
		Instances = {
			["Siege of Ogrimar"] = {
				{
					displayname = "Imm er seus",
					fullname = "Immerseus",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Pro tec tors",
					fullname = "The Fallen Protectors",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Noru shen",
					fullname = "Norushen",
					healers = 7,
					tanks = 3,
					ranged = 7,
					melee = 8,
				},
				{
					displayname = "Sha",
					fullname = "Sha of Pride",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Gala kras",
					fullname = "Galakras",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Jugger naut",
					fullname = "Iron Juggernaut",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Sha mans",
					fullname = "Kor'kron Dark Shaman",
					healers = 7,
					tanks = 3,
					ranged = 8,
					melee = 7,
				},
				{
					displayname = "Naz grim",
					fullname = "General Nazgrim",
					healers = 7,
					tanks = 3,
					ranged = 8,
					melee = 7,
				},
				{
					displayname = "Mal korok",
					fullname = "Malkorok",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Spoils",
					fullname = "Spoils of Pandaria",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Thok",
					fullname = "Thok the Bloodthirsty",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Black fuse",
					fullname = "Siegecrafter Blackfuse",
					healers = 6,
					tanks = 2,
					ranged = 10,
					melee = 8,
				},
				{
					displayname = "Klaxxi",
					fullname = "Pragons of Klaxxi",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
				{
					displayname = "Garr osh",
					fullname = "Garrosh Hellscream",
					healers = 7,
					tanks = 2,
					ranged = 8,
					melee = 8,
				},
			}
		},
		RaiderRoles = {
		},
		WishList = {
		},		
	},
}

local IconRegistered = false;
local LDBObj = nil

--Initialize stuff here
function BossRotation:OnInitialize()
    -- Called when the addon is loaded
	--                                 defined in TOC,             ,Profile Name
	self.db = LibStub("AceDB-3.0"):New("BossRotationDB", defaults, "Deafult");
	--add profiles support
	options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db);
	--Add to Blizzard options
	LibStub("AceConfig-3.0"):RegisterOptionsTable("BossRotation", options);
	--LibStub("AceConfigRegistry-3.0"):RegisterOptionsTable("BossRotation Blizz", options);
	LibStub("AceConfigRegistry-3.0"):RegisterOptionsTable("BossRotation Profile", options.args.profile);
	LibStub("AceConfigRegistry-3.0"):RegisterOptionsTable("BossRotation General", options.args.General);
	
	local AceConfigDialog = LibStub("AceConfigDialog-3.0");
	AceConfigDialog:AddToBlizOptions("BossRotation General", "BossRotation");
	AceConfigDialog:AddToBlizOptions("BossRotation Profile", "Profile", "BossRotation");
	
	--Register chat commands
	self:RegisterChatCommand("BossRotation", "ChatCommand");
	self:RegisterChatCommand("br", "ChatCommand");
	
	if LDB then
		local L_BMH_LEFT = L["|cffffff00Click|r to open the Roster Management"]
		local L_BMH_RIGHT = L["|cffffff00Right-click|r to open the options menu"]
		LDBObj = LDB:NewDataObject("BossRotation", {
			type = "launcher",
			icon = "Interface\\Icons\\ACHIEVEMENT_BOSS_GARROSH",
			label = "BossRotation",
			OnClick = function(_,msg)
				if msg == "LeftButton" then
					BossRotation:ChatCommand("show");
				elseif msg == "RightButton" then
					--BossRotation:ChatCommand("menu");
				end
			end,
			OnTooltipShow = function(tooltip)
				if not tooltip or not tooltip.AddLine then return end
				tooltip:AddLine("BossRotation")
				tooltip:AddLine(L_BMH_LEFT)
				--tooltip:AddLine(L_BMH_RIGHT)
			end,
		})
				
		if LDBIcon and BossRotation.db.profile.General.showMinimapIcon then
			LDBIcon:Register("BossRotation", LDBObj, BossRotation.db.profile.LDBIconStorage)
			IconRegistered = true;
			LDBIcon:Hide("BossRotation")
		end	
	end
	
	--CreateBossRotationFrame();
	BossMonitorRoster:AddLogMessage("Debug", "calling init from BossRotation:OnInitialize()")
	BossMonitorRoster:Init();
end

function BossRotation:updateIconState()	
	if LDBIcon then
		if BossRotation.db.profile.General.showMinimapIcon then
			if not IconRegistered then
				LDBIcon:Register("BossRotation", LDBObj, BossRotation.db.profile.LDBIconStorage)
				IconRegistered = true;
			end
			LDBIcon:Show("BossRotation")
		else
			LDBIcon:Hide("BossRotation")
		end
	end
end

--handles the OnEnable event and set up message hooks
function BossRotation:OnEnable()
    -- Called when the addon is enabled
	self.InRaid = false; -- is the player currently in a raid group
	RegisterAddonMessagePrefix("BossRotation");
	self:RegisterEvent("CHAT_MSG_ADDON");
	self:RegisterEvent("GROUP_ROSTER_UPDATE");
	
	self.myName = BossMonitorRoster:GetPlayerNameNoRealm( UnitName("player"));
	end


function BossRotation:GROUP_ROSTER_UPDATE(event,...)
	if event == "GROUP_ROSTER_UPDATE" then
		--BossMonitorRoster:AddLogMessage("Debug", "Raid changed, refreshing raiders")
		BossMonitorRoster:RefreshRaiders();
		if not self.InRaid then
			--just entered the raid. 
			self.InRaid = true;
			--ask for status refresh from other players
			SendAddonMessage("BossRotation",BossMonitorRoster.Messages.MSG_HELLO,"RAID")
		end
	end
end

function BossRotation:CHAT_MSG_ADDON(eventname, prefix, message, channel, sender)
	if prefix == "BossRotation" then
		BossMonitorRoster:AddLogMessage(sender, message);
		--get the message type first
		local msgtype, args = strsplit(":", message, 2);
		if msgtype == BossMonitorRoster.Messages.MSG_RAIDER_UPDATE then
			local raider, boss, attendance = strsplit(":",args);
			BossMonitorRoster:SetAttendance(raider, boss, attendance);
		elseif msgtype == BossMonitorRoster.Messages.MSG_HELLO and BossMonitorRoster:GetPlayerNameNoRealm(sender) ~= self.myName then
			-- sender joined the raid, say Hi
			SendAddonMessage("BossRotation",BossMonitorRoster.Messages.MSG_HI..":"..sender,"RAID")
		elseif msgtype == BossMonitorRoster.Messages.MSG_HI and 
				BossMonitorRoster:GetPlayerNameNoRealm(sender) ~= self.myName and 
				BossMonitorRoster:GetPlayerNameNoRealm(args) == self.myName then
			--raid members are replying to my Hello message. 
				BossMonitorRoster:AddLogMessage("Debug","I am waiting for update")
				BossMonitorRoster:StartStatusUpdate(sender);
		elseif msgtype == BossMonitorRoster.Messages.MSG_GET_CURRENT_STATUS then
			local target, playername = strsplit(":", args);
			if BossMonitorRoster:GetPlayerNameNoRealm(target) == self.myName then
				BossMonitorRoster:SendStatusUpdate(playername);
			end
		elseif msgtype == BossMonitorRoster.Messages.MSG_GET_WISHLIST then
			local target, playername = strsplit(":", args);
			if BossMonitorRoster:GetPlayerNameNoRealm(target) == self.myName then
				BossMonitorRoster:SendWishList(playername);
			end
		elseif msgtype == BossMonitorRoster.Messages.MSG_RAIDER_ROW_INFO then
			BossMonitorRoster:UpdateRaiderInfo(args)
		elseif msgtype == BossMonitorRoster.Messages.MSG_BOSS_NUMBERS_BREAKDOWN then
			BossMonitorRoster:UpdateBossesBreakdown(args)
		elseif msgtype == BossMonitorRoster.Messages.MSG_RAIDER_ROLE_UPDATE then
			BossMonitorRoster:UpdateRaiderRole(args)
		elseif msgtype == BossMonitorRoster.Messages.MSG_GET_BOSS_BREAKDOWN and BossMonitorRoster:GetPlayerNameNoRealm(sender) ~= self.myName then
			BossMonitorRoster:SendBossBreakdown(args)
		elseif msgtype == BossMonitorRoster.Messages.MSG_RAIDER_WISH_UPDATE then
			BossMonitorRoster:UpdateRaiderWish(args);
		elseif msgtype == BossMonitorRoster.Messages.MSG_RAIDER_WISH_LIST then
			BossMonitorRoster:UpdateWishlist(args)
		end
	end
end

--handle chat commands
function BossRotation:ChatCommand(input)
	local params = input:trim();
	local par1,par2,par3,par4 = strsplit(" ", params);
	if params == "menu" then
		InterfaceOptionsFrame_OpenToCategory("BossRotation");
	elseif params == "show" then
		Roster_Frame:Show();
		--BossRotation:ListItems();
	elseif params == "test" then
		BossRotation_Scroll:Show();
	elseif params == "statusupdate" then
		BossMonitorRoster:SendStatusUpdate("Eldrikt")
	elseif params == "showlog" then
		getglobal("Roster_Frame_LogFrame"):Show();
	elseif params == "hidelog" then
		getglobal("Roster_Frame_LogFrame"):Hide();
	else
		LibStub("AceConfigCmd-3.0").HandleCommand(BossRotation, "br","BossRotation", input);
	end
end





