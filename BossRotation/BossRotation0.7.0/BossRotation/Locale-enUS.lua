--Author: Eldrikt
--Created 23.11.2012

local L = LibStub("AceLocale-3.0"):NewLocale("BossRotation", "enUS", true)

--chat commands
L["bossrotation"] = true
L["br"] = true

--autoLinkWhenLootMaster
L["Use debug log"] = true --name
L["If selected logs addon communication in an extra debug log frame"] = true --description

--checkLootTreshold
L["Show debug log"] = true --name
L["Show or hide the extra debug log frame"] = true --description

--show Minimap Icon
L["showMinimapIcon"] = "Show Minimap Icon"
L["showMinimapIconDesc"] = "Enable to show the minimap icon"

--tooltips
L["|cffffff00Click|r to open the Roster Management"] = true
L["|cffffff00Right-click|r to open the options menu"] = true

L["General Settings"] = true
