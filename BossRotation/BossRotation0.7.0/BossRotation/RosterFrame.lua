--Author Eldrikt
--created 21.02.2014

--##########################################################################################################################
--########################### RosterFrame ##################################################################################
--##########################################################################################################################

local RolePlural = {
	Healer = "Healers",
	Tank = "Tanks",
	Melee = "Melee",
	Ranged = "Ranged",
}


local RosterFrame = {}
RosterFrame.LogMessages = {}
RosterFrame.Attendance = {}
RosterFrame.Bosses = {}
RosterFrame.Raiders = {}
RosterFrame.WishList = {}
RosterFrame.EnableExtendedFunctions = false;
--Addon message starting strings
RosterFrame.Messages = {
	MSG_HELLO = "Hello",
	MSG_HI = "Hi",
	MSG_RAIDER_UPDATE = "RU",
	MSG_BOSS_NUMBERS_BREAKDOWN = "BNB",
	MSG_RAIDER_ROW_INFO = "RRI",
	MSG_GET_CURRENT_STATUS = "GCS",
	MSG_GET_BOSS_BREAKDOWN = "GBB",
	MSG_GET_WISHLIST = "GWL",
	MSG_CURRENT_STATUS_UPDATE_DONE = "SUD",
	MSG_RAIDER_ROLE_UPDATE = "RRC",
	MSG_RAIDER_WISH_UPDATE = "RWU",
	MSG_RAIDER_WISH_LIST = "RWL",
}

RosterFrame.sorting = {
	role = "asc",
	name = "asc",
	class = "asc",
}

RosterFrame.myFistTime = true; --set to false after the frame is shown fully loaded for the first time
local TableRowCount = 25;
local LogFrameRowCount = 5;

--##########################################################################################################################
--########################### Frame Creation ###############################################################################
--##########################################################################################################################

function RosterFrame.OnLoad(self)
	--get a refference to the frame
	self.Frame = Roster_Frame;
	--popup menu for raider role selection
	self.SelectRoleMenu = {
		{ text = "Select player role" ,isTitle=true, notCheckable = true},
		{ text = "Tank", func = function() RosterFrame:SetUserRole("Tank") end },
		{ text = "Healer", func = function() RosterFrame:SetUserRole("Healer") end },
		{ text = "Ranged", func = function() RosterFrame:SetUserRole("Ranged") end },
		{ text = "Melee", func = function() RosterFrame:SetUserRole("Melee") end },
	}
	self.SelectRoleMenuFrame = CreateFrame("Frame", "SelectRoleMenuFrame", UIParent, "UIDropDownMenuTemplate")
	
	--popup menu for raider wish selection
	self.SelectWishMenu = {
		{ text = "Set your wish for this boss", isTitle=true, notCheckable = true},
		{ text = "Major upgrade", value = "Major", func = function() RosterFrame:SetUserWish("Major") end },
		{ text = "Minor upgrade", value = "Minor", func = function() RosterFrame:SetUserWish("Minor") end },
		{ text = "Not needed", value = "NotNeeded", func = function() RosterFrame:SetUserWish("NotNeeded") end },
		
	}
	
	self.SelectWishMenuFrame = CreateFrame("Frame", "SelectWishMenuFrame", UIParent, "UIDropDownMenuTemplate");
		
	-- create scroll frame buttons
	self:InitRaiderButtons() 
	--create boss name buttons
	self:InitBossNameButtons();
end

function RosterFrame:InitReportMenu()
	local reportMenuTitle = { text = "Report", isTitle = true, notCheckable = true }
	local ReportMenu = { text = "Report", value = "reportMenu", hasArrow = true, menuList = {}}
	
	tinsert(ReportMenu.menuList,{ text = "Select a role to export", isTitle = true, notCheckable = true });
	tinsert(ReportMenu.menuList,RosterFrame:CreateReportMenuEntry("Tank"));
	tinsert(ReportMenu.menuList,RosterFrame:CreateReportMenuEntry("Healer"));
	tinsert(ReportMenu.menuList,RosterFrame:CreateReportMenuEntry("Ranged"));
	tinsert(ReportMenu.menuList,RosterFrame:CreateReportMenuEntry("Melee"));
	
	
	tinsert(self.SelectWishMenu, reportMenuTitle);
	tinsert(self.SelectWishMenu, ReportMenu);
end

function RosterFrame:CreateReportMenuEntry(role)
	local channels = {"Raid", "Guild", "Party", "Say" }
	for i = 5,10 do
		local index, name = GetChannelName(i);
		if index >0 then
			tinsert(channels,name);
		end
	end
	local rolemenu = {text = role, hasArrow = true, menuList = {}}
	tinsert(rolemenu.menuList, {text = "Select target channel", isTitle = true, notCheckable = true});
	for k,v in pairs(channels) do
		local menuEntry = {text = v, func = function() RosterFrame:Report(role, v) end}
		tinsert(rolemenu.menuList, menuEntry);
	end
	return rolemenu;
end


--creates all buttons inside the scroll frame
--only call this method once in the OnLoad method!!!
--use other methods in this class to change the information inside the buttons later
--this method creates global buttons. There is no way to destroy them freeing the ressources and to recreate them
function RosterFrame:InitRaiderButtons()
	--the first raider name button is created outside the loop and is the main refference for the positioning of the rest
	local firstnametag = self:CreateRaiderNameButton(1,"Raider"..1);
	firstnametag:SetPoint("TOPLEFT",getglobal("Roster_Frame_ScrollFrame"),"TOPLEFT",4,-5) --dock it just inside the scroll frame
	--the last name button we created. Marks the position of the current row
	local lastnametag = firstnametag 
	for r = 1,TableRowCount do --for each row
		if r>1 then --do not do it for the first row
			--create an entry for the name tag and position it under the previous name tag
			local nametag = self:CreateRaiderNameButton(r,"Raider"..r);
			nametag:SetPoint("TOPLEFT",lastnametag,"BOTTOMLEFT")
			lastnametag = nametag --update the row refference object
		end
		for c=1,14 do -- for each boss column
			--create an attendance button
			local item = self:CreateRaiderAttendanceButton(r,c)
			--position it in the current row with an offset to the right
			item:SetPoint("TOPLEFT", lastnametag, "TOPRIGHT", item:GetWidth() * (c-1),0);
			item.column = c;
		end
	end
end

--create and return a button based on the ButtonRowRaiderTemplate
--this is a button showing the name of a raider
--index: the row index of the button relative to the scroll frame.
--name: the initial text to place on the button.
function RosterFrame:CreateRaiderNameButton(index, name)
	local item = CreateFrame("Button", "RaiderNameButton"..index, getglobal("Roster_Frame_ScrollFrame"), "ButtonRowRaiderTemplate");
	local textframe = getglobal("RaiderNameButton"..index.."_text");
	textframe:SetText(name);
	textframe:SetTextColor(1,1,0);
	return item;
end


--create and return a button based on the ButtonRowItemTemplate
--this is a button with a clickable text allowing to change the state of the raider
--name: the row index of the button relative to its position in the scroll frame. 
--boss: the column indes of the button relative to its position in the scroll frame. 
--The first boss button has the column indes of 1. Name buttons do not count in the indexing!
function RosterFrame:CreateRaiderAttendanceButton(name, boss)
	local item = CreateFrame("Button", "RaiderAttendanceButton_"..name.."_"..boss, getglobal("Roster_Frame_ScrollFrame"), "ButtonRowItemTemplate");
	return item;
end

--creates all boss name buttons at the top of the frame
--only call this method once in the OnLoad method!!!
function RosterFrame:InitBossNameButtons()
	local lastitem = nil;
	for i=1,14 do
		local item = self:CreateBossNameButton(i,"Evil Boss "..i);
		if not lastitem then
			--position the first button differently. It will create the anchor for the remaining buttons
			item:SetPoint("TOPLEFT", self.Frame, "TOPLEFT", 130,-5);
		else
			--position the item to the left of the previous one
			item:SetPoint("TOPLEFT", lastitem, "TOPRIGHT");
		end
		lastitem = item;
	end
end

--creates a Boss name button (a column header button)
--index: The column index
--name: The initial text to place on the button
--note: call this only during initialization from InitBossNameButtons
function RosterFrame:CreateBossNameButton(index, name)
	local item = CreateFrame("Button", "BossNameButton"..index, self.Frame, "BossNameTemplate");
	local textframe = getglobal("BossNameButton"..index.."_text");
	textframe:SetText(name);
	--textframe:SetTextColor(1,1,0);
	return item;
end

--Initializes Important RosterFrame variables after the addon has fully loaded
function RosterFrame:Init()
	if BossRotation.db and not self.IsInitialized then
		self.Bosses = BossRotation.db.profile.Instances["Siege of Ogrimar"]
		self.WishList = BossRotation.db.profile.WishList;
		RosterFrame:AddLogMessage("Debug", "Starting Init()")
		RosterFrame:AddLogMessage("Debug", #(self.Raiders).." raiders found");
		BossRotation:ToggleShowDebugLog(info, BossRotation.db.profile.General.ShowDebugLog)
		self:RefreshRaiders();
		self.IsInitialized = true;
	end
end

--##########################################################################################################################
--########################### Refresh Raiders ##############################################################################
--##########################################################################################################################

--check the current group for new raiders and add these to the roster if needed
function RosterFrame:RefreshRaiders()
		for index =1,GetNumGroupMembers() do
			local name, rank, _, _, class, fileName = GetRaidRosterInfo(index);
			if name then
				local playername = RosterFrame:GetPlayerNameNoRealm(name);
				if name == BossRotation.myName then
					--check if I am promoted, enable extended functions if that's the case
					--self:AddLogMessage("Debug","my Rank is "..rank,"RAID")
					self.EnableExtendedFunctions = (rank > 0);
					--if self.EnableExtendedFunctions then self:AddLogMessage("Debug","Extended Functions enabled!","RAID") end
				end
				local raider = RosterFrame:IsPlayerInRaiders(playername);
				if not raider then
					RosterFrame:AddLogMessage("Debug", "Adding "..name.." to the Roster");
					--add the player to the local raiders table
					raider = {}
					raider.name = playername;
					raider.class = class;
					raider.classFileName = fileName;
					raider.rank = ""
					raider.role = ""
					tinsert(self.Raiders, raider);
					self:AddAttendanceRow(raider);
					--try fetching information about the new raider
					if self.updater then
						self:AddLogMessage("Debug", "Asking about "..playername)
						local msg = self.Messages.MSG_GET_CURRENT_STATUS..":"..self.updater..":"..playername;
						SendAddonMessage("BossRotation",msg,"RAID")
					end
				else
					--RosterFrame:AddLogMessage("Debug", name.." is already in the roster");
				end
				self:TryGetRole(raider);
			end
		end
		self:ScrollBar_Update();
end

--returns the player name without the realm attached to it
--name: a player name with or without the realm
function RosterFrame:GetPlayerNameNoRealm(name)
	local strippedname, _ = strsplit("-",name);
	strippedname = strtrim(strippedname);
	return strtrim(strippedname);
end

--checks if the given playername is already in the Raiders table
function RosterFrame:IsPlayerInRaiders(playername)
	for k,v in pairs(self.Raiders) do
		if v.name == playername then
			return v
		end
	end
	return nil
end

--Tries to figure out the role of the given player
--the guildmember argument is a table value as defined in RefreshRaiders above
function RosterFrame:TryGetRole(guildmember)
	--handle pure dps classes first
	if guildmember.classFileName == "MAGE" or guildmember.classFileName == "WARLOCK" or guildmember.classFileName == "HUNTER" then
		guildmember.role = "Ranged"
	elseif guildmember.classFileName == "ROGUE" then
		guildmember.role = "Melee"
	elseif guildmember.role == "" then
		guildmember.role = "Unknown"
	end
	
	if guildmember.role == "Unknown" then
		--try to deduce the players role from his raid role
		local role = UnitGroupRolesAssigned(guildmember.name);
		if role == "HEALER" then
			guildmember.role = "Healer";
		elseif role == "TANK" then
			guildmember.role = "Tank";
		elseif role == "DAMAGER" then
			--either melee or ranged dps, crossreference with the class
			if guildmember.classFileName == "PALADIN" 
				or guildmember.classFileName == "WARRIOR" 
				or guildmember.classFileName == "DEATH KNIGHT"
				or guildmember.classFileName == "MONK" then
				--these can only be melee after we know that they are neither healer nor tank
				guildmember.role = "Melee";
			elseif guildmember.classFileName == "PRIEST" then
				--same as above, priests can only be ranged if they are not healers
				guildmember.role = "Ranged"
			end
		end
	end
	
	if guildmember.role == "Unknown" then
		--still no clue about the class? Check if we have previously stored that internaly
		if BossRotation.db.profile.RaiderRoles[guildmember.name] then
			--we have previously saved this raiders role
			guildmember.role = BossRotation.db.profile.RaiderRoles[guildmember.name];
		end
	end
end

--add a new entry into self.Attendance
--raider: a raider item as defined in the raiders table
function RosterFrame:AddAttendanceRow(raider)
	local raiderRow = {}
	raiderRow.Raider = raider 
	raiderRow.Bosses = {}
	--for each column containing the bosses
	for k,v in pairs(self.Bosses) do
		raiderRow.Bosses[v.fullname] = "out"; --init each raider to be out for the boss
	end
	tinsert(self.Attendance,raiderRow);
end

--##########################################################################################################################
--########################### Update Scroll Frame ##########################################################################
--##########################################################################################################################

--handle scroll bar updates
function RosterFrame.ScrollBar_Update()
	local raiderCount = RosterFrame:TableCount(RosterFrame.Attendance)
	local offsetbefore = FauxScrollFrame_GetOffset(RosterFrameScrollBar);
	--local offset = FauxScrollFrame_GetOffset(RosterFrameScrollBar);
	
	--BossRotation:Print("raider count: "..raiderCount.." offset before: "..offsetbefore.." offset after:"..FauxScrollFrame_GetOffset(RosterFrameScrollBar));
	local line;
	for line = 1, TableRowCount do
		local lineplusoffset = line + FauxScrollFrame_GetOffset(RosterFrameScrollBar);
		if lineplusoffset <= raiderCount then
			--update row
			RosterFrame:UpdateScrollRow(line, lineplusoffset)
			--show the row
			RosterFrame:SetRowVisible(line, true);
		else
			--hide the row
			RosterFrame:SetRowVisible(line, false)
		end
	end
	FauxScrollFrame_Update(RosterFrameScrollBar,raiderCount,TableRowCount,14);
end

--counts the number of key-value-pairs in a table
--time consuming but accurate for non-indexed tables
function RosterFrame:TableCount(tbl)
	local count = 0;
	if type(tbl) == "table" then
		for k,v in pairs(tbl) do
			count = count + 1;
		end
	end
	return count;
end

--update information inside a button row based on information from the attendance table
--rowindex: index of the button row to be updated
--dataindex: index of the row inside the attendance table containing the information to be placed in the button row
function RosterFrame:UpdateScrollRow(rowindex, dataindex)
	--the data in our attendance table does not seem to be indexed
	--we will loop trough the key-value pairs and count the current index

	local v = self.Attendance[dataindex]
	--update the row header button
	getglobal("RaiderNameButton"..rowindex).Raider = v.Raider;
	getglobal("RaiderNameButton"..rowindex.."_text"):SetText(v.Raider.name);
	local color = RAID_CLASS_COLORS[v.Raider.classFileName]
	getglobal("RaiderNameButton"..rowindex.."_text"):SetTextColor(color.r, color.g, color.b);
	local roleColor = RosterFrame.GetRoleColor(v.Raider.role)
	local texture = getglobal("RaiderNameButton"..rowindex.."_texture");
	if texture then
		getglobal("RaiderNameButton"..rowindex.."_texture"):SetTexture(roleColor.r, roleColor.g, roleColor.b, roleColor.a);
		getglobal("RaiderNameButton"..rowindex.."_texture"):SetGradientAlpha("VERTICAL", roleColor.r, roleColor.g, roleColor.b, 0.6, roleColor.r, roleColor.g, roleColor.b, 0);
	else
		BossRotation:Print("texture is nil")
	end
	--update the cells
	for c=1,14 do
		local attendanceButton = getglobal("RaiderAttendanceButton_"..rowindex.."_"..c)
		attendanceButton.item = v;
		RosterFrame:UpdateAttendanceButton(attendanceButton);
	end
end

--Set Text Color and Texture for a given cell in the table
function RosterFrame:UpdateAttendanceButton(button)
	if button and button.item then
		if button.column > #(self.Bosses) then
			button:Hide()
		else
			button:Show()
			local textframe = getglobal(button:GetName().."_text");
			local texture = getglobal(button:GetName().."_texture");

			local value = button.item.Bosses[self.Bosses[button.column].fullname];
			textframe:SetText(value);
			if value == "in" then
				textframe:SetTextColor("0","1","0");
			elseif value == "out" then
				textframe:SetTextColor("1","0","0");
			else
				textframe:SetTextColor("0","1","1");
			end
			
			local wishColor = self:GetWishColor(button.item.Raider.name, self.Bosses[button.column].fullname)
			texture:SetTexture(wishColor.r, wishColor.g, wishColor.b, 1);
			texture:SetGradientAlpha("VERTICAL", wishColor.r, wishColor.g, wishColor.b, wishColor.a, wishColor.r, wishColor.g, wishColor.b, 0);
		end
	end
end

--shows or hides a row in the scroll frame
--rowindex: the index of the rows
--visible: boolean - show or hide the rows
function RosterFrame:SetRowVisible(rowindex, visible)
	--show or hide the name tag
	local nametag = getglobal("RaiderNameButton"..rowindex)
	if nametag then
		if visible then
			nametag:Show();
		else
			nametag:Hide();
		end
	end
	--show or hide the boss tags
	for c=1,#(self.Bosses) do
		local bosstag = getglobal("RaiderAttendanceButton_"..rowindex.."_"..c)
		if bosstag then
			if visible then
				bosstag:Show();
			else
				bosstag:Hide();
			end
		end
	end
end

--Get the color for a given row
--role: a string defining the role. Possible values are "Tank", "Healer", "Ranged", "Melee"
--any other values would return black
function RosterFrame.GetRoleColor(role)
	local color = {r = "0", g = "0", b = "0", a = "1" }
	if role == "Tank" then
		color.r = "1"
	elseif role == "Healer" then
		color.r = "1"
		color.g = "1"
		color.b = "1"
	elseif role == "Melee" then
		color.r = "1"
		color.g = "1"
	elseif role == "Ranged" then
		color.g = "1"
		color.b = "1"
	end
	return color
end

--##########################################################################################################################
--########################### Frame Events #################################################################################
--##########################################################################################################################

function RosterFrame.OnShow(self)
	if self.IsInitialized then
		--just in case we missed a raider
		self:RefreshRaiders();
		--make sure that boss buttons get the correct values
		self:UpdateBossNameButtons()
		--force a scroll frame update
		self:ScrollBar_Update()
		
		if self.myFistTime then
			self.myFistTime = false;
			RosterFrame:InitReportMenu();
		end
	end
end

--updates all boss name buttons on the frame
function RosterFrame:UpdateBossNameButtons()
	--loop over all boss name buttons
	for i=1,#(self.Bosses) do
		local boss = self.Bosses[i];
		local widget = getglobal("BossNameButton"..i);
		widget.BossInfo = boss;
		RosterFrame:UpdateBossNameButton(widget);
	end
end

--updates the visual elements of a boss name button from the BossInfo stored within it
function RosterFrame:UpdateBossNameButton(button)
	local breakdown = self:GetBossBreakdown(button.BossInfo.fullname);
	getglobal(button:GetName().."_text"):SetText(button.BossInfo.displayname);
	getglobal(button:GetName().."_Healers"):SetText("H:"..breakdown.Healer.."/"..button.BossInfo.healers);
	getglobal(button:GetName().."_Tanks"):SetText("T:"..breakdown.Tank.."/"..button.BossInfo.tanks);
	getglobal(button:GetName().."_Ranged"):SetText("R:"..breakdown.Ranged.."/"..button.BossInfo.ranged);
	getglobal(button:GetName().."_Melee"):SetText("M:"..breakdown.Melee.."/"..button.BossInfo.melee);
	local color = self:GetWishColor(BossRotation.myName, button.BossInfo.fullname);
	button:SetBackdropBorderColor(color.r, color.g, color.b, color.a);
end

--returns the wish color for the raider and boss
function RosterFrame:GetWishColor(raider, boss)
	local color = {r = "0.5", g = "0.5", b = "0.5", a = "0.5" }
	if self.WishList[raider] then
		if self.WishList[raider][boss] == "Major" then
			color.r = 0.2
			color.g = 0.8
			color.b = 0.1
			color.a = 1.0
		elseif self.WishList[raider][boss] == "Minor" then
			color.r = 0.9
			color.g = 0.9
			color.b = 0.1
			color.a = 1.0
		else
			--color.g = 0
			--color.b = 0
		end
	end
	return color
end

--update the breakdown for a boss given the bosses full name
function RosterFrame:UpdateBossBreakdown(bossname)
	--loop over all boss name buttons
	for i=1,#(self.Bosses) do
		if self.Bosses[i].fullname == bossname then
			local boss = self.Bosses[i];
			local widget = getglobal("BossNameButton"..i);
			widget.BossInfo = boss;
			RosterFrame:UpdateBossNameButton(widget);
			break
		end
	end
end

--calculates the current breakdown numbers for a given boss
--bossname: the fullname of a boss
function RosterFrame:GetBossBreakdown(bossname)
	local breakdown = {
		Tank = 0,
		Healer = 0,
		Ranged = 0,
		Melee = 0,
		Unknown = 0
	}
	
	if self.Attendance then
		for k,v in pairs(self.Attendance) do
			if v.Bosses[bossname] == "in" then
				breakdown[v.Raider.role] = breakdown[v.Raider.role] + 1;
			end	
		end
	end
	
	return breakdown;
end

--closes the form
function RosterFrame.Close(self)
	self.Frame:Hide();
end

--event handler for the item click event 
--changes the text of a table cell
function RosterFrame.ClickEntry(self,widget,button)
	if widget and self.EnableExtendedFunctions then
		if button == "LeftButton" then
			--get the font string
			local msg = RosterFrame.Messages.MSG_RAIDER_UPDATE..":"..widget.item.Raider.name..":"..self.Bosses[widget.column].fullname;
			local textframe = getglobal(widget:GetName().."_text");
			--toggle text and color
			if textframe:GetText() == "out" then
				msg = msg..":in";
			else
				msg = msg..":out";
			end
			SendAddonMessage("BossRotation",msg,"RAID")
		elseif button == "RightButton" then
			RosterFrame:OpenSelectWishMenu(self.Bosses[widget.column].fullname, widget.item.Raider.name)
		end
	end	
end

function RosterFrame:OnCellEnter(self)
	if RosterFrame.EnableExtendedFunctions then
		GameTooltip:SetOwner(self, "ANCHOR_TOP");
		GameTooltip:SetText("|cffffff00Click|r to set if the raider will join for this boss or not\n|cffffff00Right-click|r to set the raider's wish for this boss");
	end
end

--opens a popupmenu at the cursors location for wishlist selection
--selectedBoss: fullname of the boss for which a wish is been selected
--selectedRaider: raider name of the player selecting the wish
function RosterFrame:OpenSelectWishMenu(selectedBoss, selectedRaider)
	self.SelectedBoss = selectedBoss;
	self.SelectedRaider = selectedRaider;
	
	EasyMenu(self.SelectWishMenu, self.SelectWishMenuFrame, "cursor", 0, 0, "MENU")
			
	--preapare the default selected value
	local myWish = "NotNeeded";
	if self.WishList[self.SelectedRaider] and self.WishList[self.SelectedRaider][self.SelectedBoss] then
		myWish = self.WishList[self.SelectedRaider][self.SelectedBoss];
	end
	
	--set the selected item
	UIDropDownMenu_SetSelectedValue(self.SelectWishMenuFrame, myWish, true);
end

--event handler for sort button click events
function RosterFrame:SortAttendance(sortvalue)
	if self.sorting[sortvalue] == "asc" then
		table.sort(self.Attendance, function(a,b) return a.Raider[sortvalue] < b.Raider[sortvalue] end)
		self.sorting[sortvalue] = "desc"
	else
		table.sort(self.Attendance, function(a,b) return a.Raider[sortvalue] > b.Raider[sortvalue] end)
		self.sorting[sortvalue] = "asc"	
	end
	self:ScrollBar_Update()
end

--event handler for row header click events
--opens the role selectoin popup
function RosterFrame.ClickRowHeader(self,widget,button)
	if widget and button and self.EnableExtendedFunctions then
		self.SelectedRaider = widget
		EasyMenu(self.SelectRoleMenu, self.SelectRoleMenuFrame, "cursor", 0, 0, "MENU")
		UIDropDownMenu_SetSelectedName(self.SelectRoleMenuFrame, widget.Raider.role, true);
	end
end

--event handler for role selection popup
--sets the role for the currently selected raider, which is the one the menu opened on
function RosterFrame:SetUserRole(role)
	if self.SelectedRaider then
		local msg = self.Messages.MSG_RAIDER_ROLE_UPDATE..":"..self.SelectedRaider.Raider.name..":"..role;
		SendAddonMessage("BossRotation",msg,"RAID")
	end
end

--event handler for the wish selection popup
function RosterFrame:SetUserWish(wish)
	if self.SelectedBoss and self.SelectedRaider then
		local msg = self.Messages.MSG_RAIDER_WISH_UPDATE..":"..self.SelectedRaider..":"..self.SelectedBoss..":"..wish;
		SendAddonMessage("BossRotation",msg,"RAID");
	end
end


function RosterFrame:Report(role, channel)
	--build the message
	local msg = RolePlural[role].." for "..self.SelectedBoss..": ";
	for k,v in pairs(self.Attendance) do
		if v.Raider.role == role and v.Bosses[self.SelectedBoss] == "in" then
			msg = msg..v.Raider.name..", ";
		end
	end
	
	msg = strtrim(msg, ", "); --remove trailing chars
	
	local messages = {}
	local startidx, endidx, firstWord, restOfString = string.find(msg,", ",250);
	if startidx then
		tinsert(messages, strsub(msg, 1, endidx));
		tinsert(messages, restOfString);
	else
		tinsert(messages, msg);
	end
	
	for k,v in pairs(messages) do
	--output the message
		if GetChannelName(channel) > 0 then
			SendChatMessage(v,"channel", "common",GetChannelName(channel));
		else
			SendChatMessage(v,channel);
		end
	end
end


--event handler for column header click
--show the boss properties frame with updated boss information
function RosterFrame.ClickBossName(self,widget,button)
	if widget and widget.BossInfo then
		if button == "LeftButton" then
			if self.EnableExtendedFunctions then --allow only promoted players to adjust the breakdown
				BossProperties:ShowWidget(widget)
			end
		elseif button == "RightButton" then
			RosterFrame:OpenSelectWishMenu(widget.BossInfo.fullname, BossRotation.myName)
		end
	end	
end

--debug function used to output the current state of the attendance table to the chat
function RosterFrame:OutputAttendance()
	BossRotation:Print("Printing Bosses "..#(self.Bosses))
	
	BossRotation:Print("Attendance contains "..self:TableCount(self.Attendance).." elements");
	for k,v in pairs(self.Attendance) do
		BossRotation:Print("["..k.."]->"..v.Raider.name.." "..v.Raider.role.." "..v.Raider.class);
		self:outputTable(v.Bosses,"-->");
	end
end

--a debug function used to output a table to the chat
--tbl: the table to output
--spacing: a string placed infront of each row used to output internal tables farther right
function RosterFrame:outputTable(tbl,spacing)
	if not spacing then spacing = "" end
	for k,v in pairs(tbl) do
		if type(v) == "table" then
			BossRotation:Print(spacing.."."..k.."==>"..self:TableCount(v).." elements")
			self:outputTable(v,spacing.."."..k)
		else
			if type(v) == "userdata" then
				BossRotation:Print(spacing.."["..k.."] - userdata");
			elseif type(v) == "function" then -- only output the key for not printable values
				BossRotation:Print(spacing..":"..k.."(...) - function");
			elseif type(v) == "boolean" then
				--handle booleans
				if v then
					BossRotation:Print(spacing.."["..k.."]:true");
				else
					BossRotation:Print(spacing.."["..k.."]:false");
				end
			else
				BossRotation:Print(spacing.."["..k.."]:"..v);
			end
		end
	end
end

--##########################################################################################################################
--########################### Addon Message Handling #######################################################################
--##########################################################################################################################

--handle log frame scroll updates
function RosterFrame.LogScrollBar_Update()
	for line = 1, LogFrameRowCount do
		local lineplusoffset = line + FauxScrollFrame_GetOffset(Roster_Frame_LogFrame_ScrollBar);
		if lineplusoffset <= #(RosterFrame.LogMessages) then
			--update row
			getglobal("Roster_Frame_LogFrame_LogEntry"..line):SetText(RosterFrame.LogMessages[lineplusoffset][1]);
			local fontstring = getglobal("Roster_Frame_LogFrame_LogEntry"..line):GetFontString();
			local color = RosterFrame.LogMessages[lineplusoffset][2];
			fontstring:SetTextColor(color.r, color.g, color.b);
			--show the row
			getglobal("Roster_Frame_LogFrame_LogEntry"..line):Show();
		else
			--hide the row
			getglobal("Roster_Frame_LogFrame_LogEntry"..line):Hide();
		end
	end
	FauxScrollFrame_Update(Roster_Frame_LogFrame_ScrollBar,#(RosterFrame.LogMessages),LogFrameRowCount,14);
end

--add a new message to the log frame
--sender: the player sending the message or "Debug"
--msg: the message as string
function RosterFrame:AddLogMessage(sender, msg)
	if BossRotation.db.profile.General.UseDebugLog then
		--translate the message
		for k,v in pairs(self.Messages) do
			msg = gsub(msg,v,gsub(k,"MSG_",""));
		end
		
		local msgColor ={}
		if sender == "Debug" then
			msgColor.r="0"
			msgColor.g="1"
			msgColor.b="1"
		elseif self:GetPlayerNameNoRealm(sender) == UnitName("player") then
			msgColor.r="0"
			msgColor.g="1"
			msgColor.b="0"
		else
			msgColor.r="1"
			msgColor.g="1"
			msgColor.b="0"
		end
		
		--add the message
		tinsert(self.LogMessages,{"["..sender.."]: "..msg, msgColor});
		--scroll the log frame to the bottom
		local offset = #(self.LogMessages) - LogFrameRowCount
		if offset<0 then offset = 0 end
		FauxScrollFrame_SetOffset(Roster_Frame_LogFrame_ScrollBar,offset);
		--update the log frame
		RosterFrame.LogScrollBar_Update();
	end
end

--initiate a full status update
--sender: a playername of a fellow raid member running the addon
--this method will send update request messages to sender for every raider currently in the raid
--the sender will also become the refference updater for future status requests
--he may of may not change during a session
function RosterFrame:StartStatusUpdate(sender)
	self.updater = sender;
	--self:AddLogMessage("Debug","Starting status update request. Asking "..sender)
	for i=1,#(self.Bosses) do
		--self:AddLogMessage("Debug", "Asking about "..self.Bosses[i].fullname)
		local msg = self.Messages.MSG_GET_BOSS_BREAKDOWN..":"..self.Bosses[i].fullname;
		SendAddonMessage("BossRotation",msg,"RAID");
	end
	for k,v in pairs(self.Raiders) do
		--self:AddLogMessage("Debug", "Asking about "..v.name)
		local msg = self.Messages.MSG_GET_CURRENT_STATUS..":"..self.updater..":"..v.name;
		SendAddonMessage("BossRotation",msg,"RAID")
	end
	for k,v in pairs(self.Raiders) do
		--self:AddLogMessage("Debug", "Asking about "..v.name)
		local msg = self.Messages.MSG_GET_WISHLIST..":"..self.updater..":"..v.name;
		SendAddonMessage("BossRotation",msg,"RAID")
	end
	
end

--Answer to a status update request about a given player name
function RosterFrame:SendStatusUpdate(playername)
	local status = ""
	for k,v in pairs(self.Attendance) do
		if v.Raider.name == playername then
			status = v.Raider.role;
			for i=1,#(self.Bosses) do
				status = status..":"..v.Bosses[self.Bosses[i].fullname]
			end
			break
		end
	end
	--status already starts with :
	local msg = self.Messages.MSG_RAIDER_ROW_INFO..":"..playername..":"..status;
	SendAddonMessage("BossRotation",msg,"RAID")
end

--handles MSG_RAIDER_ROW_INFO Addon Message
--updates the information about a single raider
--args: colon separated string containing in/out information about
--all bosses in the instance
--the first 2 items represent the raider name and his/her role
function RosterFrame:UpdateRaiderInfo(args)
	local raidername, status = strsplit(":",args,2);
	--find the raider row in the attendance table
	for k,v in pairs(self.Attendance) do
		if v.Raider.name == raidername then
			--get the attendance strings as a table
			--RosterFrame:AddLogMessage("Debug", "updating attendance information for "..raidername)
			if status then
				local att = {strsplit(":",status)} 
				v.Raider.role = att[1];
				BossRotation.db.profile.RaiderRoles[v.Raider.name] = v.Raider.role;
				local i = 1
				for i=1,#(self.Bosses) do
					--RosterFrame:AddLogMessage("Debug", "    Setting "..Bosses[i].fullname.." from "..v.Bosses[Bosses[i].fullname].." to "..att[i]);
					if att[i+1] then
						v.Bosses[self.Bosses[i].fullname] = att[i+1]
					end
				end
			end
		break;
		end
	end
	RosterFrame:ScrollBar_Update();
end

--handles the MSG_RAIDER_UPDATE Addon Message
--used to update the state of the attendance table
--attendance can be either "in" or "out"
function RosterFrame:SetAttendance(raider, boss, attendance)
	for k,v in pairs(self.Attendance) do
		if v.Raider.name == raider then
			v.Bosses[boss] = attendance;
			self:UpdateBossBreakdown(boss)
			break;
		end
	end
	self:ScrollBar_Update();
end

--handles the MSG_BOSS_NUMBERS_BREAKDOWN addon message
function RosterFrame:UpdateBossesBreakdown(args)
	local bossname, tanks, healers, ranged, melee = strsplit(":", args);
	for k,v in pairs(self.Bosses) do
		if v.fullname == bossname then
			v.tanks = tanks;
			v.healers = healers;
			v.ranged = ranged;
			v.melee = melee;
			self:UpdateBossBreakdown(bossname);
		end
	end
end

--handles the MSG_RAIDER_ROLE_UPDATE addon message
function RosterFrame:UpdateRaiderRole(args)
	local raider, role = strsplit(":", args);
	--find the raider in the attendance table
	for k,v in pairs(self.Attendance) do
		if v.Raider.name == raider then
			--set his role
			v.Raider.role = role;
			--update the settings so we know it for the next time
			BossRotation.db.profile.RaiderRoles[v.Raider.name] = role;
			--force scroll frame update
			self.ScrollBar_Update(self);
			--update the boss name buttons for the breakdown
			self:UpdateBossNameButtons();
			break;
		end
	end
end

--handles the MSG_GET_BOSS_BREAKDOWN addon message
function RosterFrame:SendBossBreakdown(args)
	for i=1,#(self.Bosses) do
		if self.Bosses[i].fullname == args then
			local msg = RosterFrame.Messages.MSG_BOSS_NUMBERS_BREAKDOWN..":"..args..":"..self.Bosses[i].tanks..":"..self.Bosses[i].healers..":"..self.Bosses[i].ranged..":"..self.Bosses[i].melee;
			SendAddonMessage("BossRotation",msg,"RAID");
			break;
		end
	end
end

-- handles the MSG_GET_WISHLIST addon message
function RosterFrame:SendWishList(args)
	local msg = self.Messages.MSG_RAIDER_WISH_LIST..":"..args;
	for i=1,#(self.Bosses) do
		if self.WishList[args] then
			local wishprio = self.WishList[args][self.Bosses[i].fullname];
			if not wishprio then
				msg = msg..":".."0";
			elseif wishprio == "NotNeeded" then
				msg = msg..":".."0";
			elseif wishprio == "Minor" then
				msg = msg..":".."1";
			elseif wishprio == "Major" then
				msg = msg..":".."2";
			end
		end
	end
	SendAddonMessage("BossRotation",msg,"RAID");
end

--handles the MSG_RAIDER_WISH_LIST addon message
function RosterFrame:UpdateWishlist(args)
	local raidername, status = strsplit(":",args,2);
	self.WishList[raidername] = {};
	if status then
		local wishes = {strsplit(":",status)};
		for i =1,#(wishes) do
			if wishes[i] == "1" then
				self.WishList[raidername][self.Bosses[i].fullname] = "Minor";
			elseif wishes[i] == "2" then
				self.WishList[raidername][self.Bosses[i].fullname] = "Major";	
			end
		end
	end
	--force scroll frame update
	self.ScrollBar_Update(self);
end

--handles the MSG_RAIDER_WISH_UPDATE addon message
function RosterFrame:UpdateRaiderWish(args)
	local raider, boss, wish = strsplit(":", args);
	if not self.WishList[raider] then self.WishList[raider] = {} end
	self.WishList[raider][boss] = wish;
	--force scroll frame update
	self.ScrollBar_Update(self);
	--update the boss name buttons for the breakdown
	self:UpdateBossNameButtons();
end

--##########################################################################################################################
--########################### BossPropertiesFrame ##########################################################################
--##########################################################################################################################
local BossPropertiesFrame = {}

function BossPropertiesFrame.OnLoad(self)
	self.Frame = BossProperties_Frame;
end

function BossPropertiesFrame.OnShow(self)
	
end

function BossPropertiesFrame.Close(self)
	self.Frame:Hide();
end

function BossPropertiesFrame.Apply(self)
	if self.Widget then
		--get the numbers breakdown from the frame and save them back into the widget
		local healers = getglobal("BossProperties_Frame_Healers"):GetText();
		local tanks = getglobal("BossProperties_Frame_Tanks"):GetText();
		local ranged = getglobal("BossProperties_Frame_Ranged"):GetText();
		local melee = getglobal("BossProperties_Frame_Melee"):GetText();
		local msg = RosterFrame.Messages.MSG_BOSS_NUMBERS_BREAKDOWN..":"..self.Widget.BossInfo.fullname..":"..tanks..":"..healers..":"..ranged..":"..melee;
		
		SendAddonMessage("BossRotation",msg,"RAID")
		--force widget update
		RosterFrame:UpdateBossNameButton(self.Widget);
	end
end

function BossPropertiesFrame:ShowWidget(widget)
	if widget then
		if widget.BossInfo then		
			getglobal("BossProperties_Frame_Text"):SetText(widget.BossInfo.fullname);
			getglobal("BossProperties_Frame_Healers"):SetText(widget.BossInfo.healers);
			getglobal("BossProperties_Frame_Tanks"):SetText(widget.BossInfo.tanks);
			getglobal("BossProperties_Frame_Ranged"):SetText(widget.BossInfo.ranged);
			getglobal("BossProperties_Frame_Melee"):SetText(widget.BossInfo.melee);
			-- set the widget refference to the frame
			self.Widget = widget;
			--finally show the frame
			self.Frame:Show()
		end
	end	

end

--##########################################################################################################################
--########################### Init variables ###############################################################################
--##########################################################################################################################

BossMonitorRoster = RosterFrame;
BossProperties = BossPropertiesFrame;