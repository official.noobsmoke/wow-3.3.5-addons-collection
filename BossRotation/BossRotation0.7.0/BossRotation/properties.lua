--This file contains the set/get functions for all the options
--Author: Eldrikt
--Created 23.11.2012

--get the UseDebugLog parameter
function BossRotation:IsUseDebugLog(info)
    return self.db.profile.General.UseDebugLog;
end

--set the UseDebugLog parameter
function BossRotation:ToggleUseDebugLog(info, newValue)
	self.db.profile.General.UseDebugLog = newValue;
end

--get the ShowDebugLog parameter
function BossRotation:IsShowDebugLog(info)
    return getglobal("Roster_Frame_LogFrame"):IsShown();
end

--set the ShowDebugLog parameter
function BossRotation:ToggleShowDebugLog(info, newValue)
	self.db.profile.General.ShowDebugLog = newValue;
	if newValue then
		getglobal("Roster_Frame_LogFrame"):Show();
	else
		getglobal("Roster_Frame_LogFrame"):Hide();
	end
end

--get the showMinimapIcon parameter
function BossRotation:IsShowMinimapIcon(info)
    return self.db.profile.General.showMinimapIcon;
end

--set the showMinimapIcon parameter
function BossRotation:ToggleShowMinimapIcon(info, newValue)
	self.db.profile.General.showMinimapIcon = newValue;
	BossRotation:updateIconState();
end
