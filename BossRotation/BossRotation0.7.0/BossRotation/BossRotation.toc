## Interface: 50400
## Title: BossRotation |cff7fff7f -Ace3-|r
## Notes: Allows raider rotation management for SoO
## Author: Eldrikt
## Version: 0.7
## SavedVariables: BossRotationDB
## OptionalDeps: Ace3
## X-Embeds: Ace3
## X-Category: Raid

embeds.xml

Locale-enUS.lua

BossRotation.lua
properties.lua
RosterFrame.lua
RosterFrame.xml