## Interface: 30300
## Title: GuildGreet |cFFFFA0A0Extended by Urbin|r
## Version: 30300.9
## Author: Urbin |cFFFFA0A0 (taken over from Greya, Mordikaiin/Rozak)|r
## X-Date: 2010-03-09
## X-Category: Guild
## X-Website: http://wow.curse-gaming.com/downloads/details/7529/
## URL: http://wow.curse-gaming.com/downloads/details/7529/
## Description: Keeps a log of guildies waiting for your greeting
## Notes: Keeps a log of guildies waiting for your greeting
## SavedVariables: GLDG_Data
## OptionalDeps: myAddOns
GuildGreet.xml
