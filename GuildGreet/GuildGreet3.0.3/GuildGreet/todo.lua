GLDG_Todo = ""..
"|cFFFFFF7F[will not implement]|r\r\n"..
"- prepend main name in chat in main-class-colour (suggested by nimisha)\r\n"..
"  [will not implement, as it does not make sense]\r\n"..
"- I will not add auto-greeting, even though it has been requested by many.\r\n"..
"  If you are too lazy to click the mouse to greet your friends, what is that\r\n"..
"  greeting worth...\r\n"..
"\r\n"..
"|cFFFFFF7F[unlikely to be implemented in the future]|r\r\n"..
"- add smart main name matching algorithm when automatically building main-\r\n"..
"  alt relations (suggested by Kortanis) \r\n"..
"- fubar/titan plugin instead of GreetList (suggested by Kobihunt)\r\n"..
"- implement MiniMap icon (from Greya's original todo list)\r\n"..
"- Synch between officers (suggested by Kobihunt)\r\n"..
"- add auto-assignment notes to guild/officer notes (suggested by Kobihunt)\r\n"..
"- add guild roster support (suggested by Kobihunt)\r\n"..
"- add an invisible character to the end of each message, so people can filter\r\n"..
"  them to stop clutter in their chat frame (' ' (alt-255) or ' ' (alt-0160))\r\n"..
"  (suggested by Natronx77)\r\n"..
"- implement optional [GuildGreet] after name in chat when sending messages\r\n"..
"  (from Greya's original todo list)\r\n"..
"- add a 'fader time' after which old entries are removed from the greet list\r\n"..
"  (suggested by ImmortalDragon2)\r\n"..
"- toggle addon on/off (suggested by ImmortalDragon2)\r\n"..
"  [already partially implemented]\r\n"..
"- add automatic main-alt detection based on special guild rank (suggested by\r\n"..
"  Kortanis)\r\n"..
"- add support to monitor multiple channels (suggested by Epophis)\r\n"..
"- auto-assignment without the need for the [main] tag (suggested by Kobihunt)\r\n"..
"- let the user decide whether to use the 'public' channel or whisper for each\r\n"..
"  event instead of globally (suggrested by nighttar)\r\n"..
"- support 'alt - <name>' as well (suggested by mdsandler)\r\n"..
"- allow for more than 500 guild members (reported by Dima_todd) [probably a\r\n"..
"  Blizzard limitation; not likely to be implemented in the near future]\r\n"..
"- I suggest adding the option of sending 'greetings' via /tells, and\r\n"..
"  'congratulations' via 'g'chat (suggested by techsgtchen)\r\n"..
"\r\n"..
"|cFFFFFF7F[under consideration]|r\r\n"..
"- people that are in GLDG_DataChannel from an old channel being monitored are\r\n"..
"  still detected coming online when a new channel has been chosen (reported\r\n"..
"  by Epophis)\r\n"..
"- add option to not congratulate alts (suggested by Thomas Richter)\r\n"..
"- detect player leaving guild, to avoid spamming 'you are not in a guild'\r\n"..
"  (reported by felorah)\r\n"..
"- filter by guild name (need to find out how to create multi-level drop\r\n"..
"  down boxes)\r\n"..
"- some people report that level up grats and welcome to guild messages no\r\n"..
"  longer work, I can't reproduce the problem (reported by randy47sm and\r\n"..
"  dumtesofva)\r\n"..
"- add keybinds to greet/discard the top entry in the list in order to be\r\n"..
"  greet people without taking your hands off the keyboard while in combat\r\n"..
"  (suggested by warderbrad)\r\n"..
"\r\n"..
"|cFFFFFF7F[planned]|r\r\n"..
"- remove colour sets\r\n"..
"- friends are still greeted even if friends support is disabled (reported by\r\n"..
"  cycronexium)\r\n"..
""
