-- Default (English)
--------------------

-- All text strings used in the configuration GUI
GLDG_TXT = {}
-- Names for the tabs
GLDG_TXT.TabGeneral	= "General"
GLDG_TXT.TabGreetings	= "Greetings"
GLDG_TXT.TabPlayers	= "Players"
-- Queue list
GLDG_TXT.new		= "NEW"
GLDG_TXT.lvl		= "LEVEL"
GLDG_TXT.rank		= "RANK"
-- Queue list tooltips
GLDG_TXT.tipdef		= "At %s, this guildmember came online for the first time during this session."
GLDG_TXT.tiprelog	= "At %s, this guildmember came back online after being offline for %s."
GLDG_TXT.tiprelogalt	= " Character used by player previously was %s."
GLDG_TXT.tipnew		= "At %s, this player joined the guild"
GLDG_TXT.tipnew2	= "Player joined the guild before you logged on."
GLDG_TXT.tiplvl		= "Guildmember reached level %s."
GLDG_TXT.tiprank	= "%s promoted the player to rank %s earlier."
GLDG_TXT.tiprank2	= "At %s, %s promoted this player to rank %s."
GLDG_TXT.tiprank3	= "Player was promoted to rank %s before you logged on."
GLDG_TXT.tipalt		= " Main character for this player is %s."
GLDG_TXT.hour		= "%d hour "
GLDG_TXT.minute		= "%d min"
-- General tab: greeting options
GLDG_TXT.optheader	= "Configuration options to determine who, when and how to greet"
GLDG_TXT.relogalways	= "Always show relogs"
GLDG_TXT.reloglimit	= "Only show relogs after more then %d min"
GLDG_TXT.greetasmain	= "Greet alts with the same name as main by default"
GLDG_TXT.whisper	= "Whisper greetings to players"
GLDG_TXT.listNames	= "List alt and main names when player logs in"
-- General tab: player list settings
GLDG_TXT.listheader	= "Configuration options for displaying the players waiting for a greeting"
GLDG_TXT.listdirect	= "List grows upwards instead of downwards"
GLDG_TXT.listvisible	= "List header is always visible"
GLDG_TXT.listsize	= "Display a maximum of %d queued players"
-- Greetings tab
GLDG_TXT.greetheader	= "Manage the messages you want to use for greeting"
GLDG_TXT.collectheader	= "Custom collections"
GLDG_TXT.cbnew		= "Create new collection"
GLDG_TXT.cbdel		= "Remove selection"
GLDG_TXT.cbrealm	= "Set realm collection"
GLDG_TXT.cbguild	= "Set guild collection"
GLDG_TXT.cbplayer	= "Set character collection"
GLDG_TXT.custheader	= "Current value"
GLDG_TXT.nodef		= "not defined"
GLDG_TXT.selection	= "Selected collection"
-- Greetins tab: selection header and buttons
GLDG_TXT.colglobal	= "Global defaults"
GLDG_TXT.colcustom	= "Collection \'%s\'"
GLDG_TXT.msgheader	= "%s : select the greeting category you want to edit"
GLDG_TXT.default	= "coming online"
GLDG_TXT.relog		= "relogging"
GLDG_TXT.joining	= "joining guild"
GLDG_TXT.newrank	= "promotion"
GLDG_TXT.newlevel	= "leveling"
-- Greetings tab: greetings editbox
GLDG_TXT.Greet		= "Edit default greeting message: first use of %s is replaced by player name"
GLDG_TXT.GreetBack	= "Edit relog greeting message: first use of %s is replaced by player name"
GLDG_TXT.Welcome	= "Edit welcome greeting message: first use of %s is replaced by player name"
GLDG_TXT.NewRank	= "Edit promotion congratulations: first %s is player, second %s is new rank"
GLDG_TXT.NewLevel	= "Edit new level congratulations: first %s is player, second %s is new level"
-- Players tab
GLDG_TXT.playersheader	= "Configure settings for your guildmembers: ignore them, set main/alt and enter alias"
GLDG_TXT.markIGN	= "IGNORE"
GLDG_TXT.markMAIN	= "MAIN"
GLDG_TXT.markALT	= "ALT"
GLDG_TXT.ignores	= "Include ignored players in the list"
GLDG_TXT.showalt	= "Always show alts"
GLDG_TXT.groupalt	= "Keep with main"
GLDG_TXT.pbsign		= "Ignore"
GLDG_TXT.pbrign		= "Unignore"
GLDG_TXT.pbalias	= "Set alias"
GLDG_TXT.pbsmain	= "Set as main"
GLDG_TXT.pbrmain	= "Unset as main"
GLDG_TXT.pbpmain	= "Promote to main"
GLDG_TXT.pbsalt		= "Set as alt"
GLDG_TXT.pbralt		= "Unset as alt"
GLDG_TXT.mainhead	= "Select main for character %s"
GLDG_TXT.aliashead	= "Set alias for character %s"
-- Common use
GLDG_TXT.set		= "set"
GLDG_TXT.cancel		= "cancel"
GLDG_TXT.update		= "update"
GLDG_TXT.delete		= "remove"
GLDG_TXT.add		= "add"
GLDG_TXT.clear		= "clear"


-- Deutsch (German)
-------------------
if (GetLocale() == "deDE") then
-- Names for the tabs
GLDG_TXT.TabGeneral	= "Allgemein"
GLDG_TXT.TabGreetings	= "Grü\195\159e"
GLDG_TXT.TabPlayers	= "Spieler"
-- Queue list
GLDG_TXT.new		= "NEU"
GLDG_TXT.lvl		= "LEVEL"
GLDG_TXT.rank		= "RANG"
-- Queue list tooltips
GLDG_TXT.tipdef		= "Um %s kam das Mitglied erstmals diese Sitzung online."
GLDG_TXT.tiprelog	= "Um %s kam das Mitglied wieder online, nachdem es %s offline war."
GLDG_TXT.tiprelogalt	= " Charakter des Spielers war vorher %s."
GLDG_TXT.tipnew		= "Um %s ist dieser Spieler der Gilde beigetreten."
GLDG_TXT.tipnew2	= "Spieler ist der Gilde beigetreten bevor Du eingeloggt hast."
GLDG_TXT.tiplvl		= "Gildenmitglied hat Level %s erreicht."
GLDG_TXT.tiprank	= "Spieler wurde von %s zum Rang %s befördert."
GLDG_TXT.tiprank2	= "Um %s wurde Spieler von %s zum Rang %s befördert."
GLDG_TXT.tiprank3	= "Der Spieler wurde zum Rang %s befördert, bevor Du eingeloggt hast."
GLDG_TXT.tipalt		= " Main dieses Spielers ist %s."
GLDG_TXT.hour		= "%d Stunde "
GLDG_TXT.minute		= "%d Minute"
-- General tab: greeting options
GLDG_TXT.optheader	= "Konfigurationsoptionen wen wann wie zu grü\195\159en"
GLDG_TXT.relogalways	= "Relogs immer zeigen"
GLDG_TXT.reloglimit	= "Zeige Relogs nur nach mehr als %d Minute"
GLDG_TXT.greetasmain	= "Standardmässig Alts mit dem Namen des Main grü\195\159en"
GLDG_TXT.whisper	= "Spieler anfl�stern"
GLDG_TXT.listNames	= "Alle Spielernamen auflisten beim Einloggen"
-- General tab: player list settings
GLDG_TXT.listheader	= "Konfigurationsoptionen für die Anzeige von Spielern, die auf eine Begrü\195\159ung warten"
GLDG_TXT.listdirect	= "Liste wächst nach oben statt unten"
GLDG_TXT.listvisible	= "Listen überschrift ist immer sichtbar"
GLDG_TXT.listsize	= "Maximal %d Spieler in Schlange anzeigen"
-- Greetings tab
GLDG_TXT.greetheader	= "Verwaltet die Nachrichten, die Du zur Begrü\195\159ung verwendst"
GLDG_TXT.collectheader	= "Persönliche Sammlung"
GLDG_TXT.cbnew		= "Neue Sammlung erzeugen"
GLDG_TXT.cbdel		= "Markierung entfernen"
GLDG_TXT.cbrealm	= "Server-Sammlung setzen"
GLDG_TXT.cbguild	= "Gilden-Sammlung setzen"
GLDG_TXT.cbplayer	= "Charakter-Sammlung setzen"
GLDG_TXT.custheader	= "Gegenwärtiger Wert"
GLDG_TXT.nodef		= "nicht definiert"
GLDG_TXT.selection	= "Markierte Sammlung"
-- Greetins tab: selection header and buttons
GLDG_TXT.colglobal	= "Standardeinstellungen"
GLDG_TXT.colcustom	= "Sammlung \'%s\'"
GLDG_TXT.msgheader	= "%s : wähle die Gru\195\159kategorie, welche Du bearbeiten willst"
GLDG_TXT.default	= "Einloggen"
GLDG_TXT.relog		= "Reloggen"
GLDG_TXT.joining	= "Gildenbeitritt"
GLDG_TXT.newrank	= "Beförderung"
GLDG_TXT.newlevel	= "Levelup"
-- Greetings tab: greetings editbox
GLDG_TXT.Greet		= "Gru\195\159 beim Einloggen editieren: erste Verwendung %s wird zum Spielernamen"
GLDG_TXT.GreetBack	= "Gru\195\159 beim Reloggen editieren: erste Verwendung %s wird zum Spielernamen"
GLDG_TXT.Welcome	= "Gru\195\159 beim Beitritt editieren: erste Verwendung %s wird zum Spielernamen"
GLDG_TXT.NewRank	= "Gratulation bei Beförderung editieren: erstes %s Spieler, zweites %s neuer Rang"
GLDG_TXT.NewLevel	= "Gratulation beim level-up editieren: erstes %s Spieler, zweites %s neuer Level"
-- Players tab
GLDG_TXT.playersheader	= "Spezielle Einstellungen für Gildenmitglieder verwalten"
GLDG_TXT.markIGN	= "IGNORIEREN"
GLDG_TXT.markMAIN	= "MAIN"
GLDG_TXT.markALT	= "ALT"
GLDG_TXT.ignores	= "Ignorierte Spieler in der Liste mit einschlie\195\159en"
GLDG_TXT.showalt	= "Zeigen Sie immer Alts"
GLDG_TXT.groupalt	= "Unterhalt mit main"
GLDG_TXT.pbsign		= "Ignorieren"
GLDG_TXT.pbrign		= "Gru\195\159en"
GLDG_TXT.pbalias	= "Alias setzen"
GLDG_TXT.pbsmain	= "Als Main setzen"
GLDG_TXT.pbrmain	= "Kein Main"
GLDG_TXT.pbpmain	= "Zum Main"
GLDG_TXT.pbsalt		= "Als Alt setzen"
GLDG_TXT.pbralt		= "Kein Alt"
GLDG_TXT.mainhead	= "Main für %s wählen"
GLDG_TXT.aliashead	= "Alias für %s setzen"
-- Common use
GLDG_TXT.set		= "Setzen"
GLDG_TXT.cancel		= "Abbrechen"
GLDG_TXT.update		= "Update"
GLDG_TXT.delete		= "Entfernen"
GLDG_TXT.add		= "Hinzufügen"
GLDG_TXT.clear		= "Löschen"

-- fran�ais (French)
--------------------
elseif (GetLocale() == "frFR") then
end


-- Some greetings to get started: those can be changed in the configuration GUI
-- Important note: this list will only be used when no configuration is found!
-------------------------------------------------------------------------------

-- Default
GLDG_GREET = {}
GLDG_GREET[1] = "Hi %s"
GLDG_GREET[2] = "Hello %s"
GLDG_GREET[3] = "Hey %s"
-- Relogs
GLDG_GREETBACK = {}
GLDG_GREETBACK[1] = "Welcome back %s"
GLDG_GREETBACK[2] = "Hi again %s"
GLDG_GREETBACK[3] = "Hi %s, welcome back"
-- Welcome to guild
GLDG_WELCOME = {}
GLDG_WELCOME[1] = "Welcome to the guild %s"
GLDG_WELCOME[2] = "Hi %s, nice to have you in the guild"
GLDG_WELCOME[3] = "Welcome %s, good you're joining us"
-- Promotion
GLDG_RANK = {}
GLDG_RANK[1] = "Congratulations %s with your promotion to %s"
GLDG_RANK[2] = "Grats for %s on becoming %s"
-- New level
GLDG_LEVEL = {}
GLDG_LEVEL[1] = "Congratulations %s on reaching level %s"
GLDG_LEVEL[2] = "%s, grats on getting level %s"

-- Deutsch (German)
-------------------
if (GetLocale() == "deDE") then
-- Default
GLDG_GREET[2] = "Hallo %s"
GLDG_GREET[3] = "Willkommen %s"
-- Relogs
GLDG_GREETBACK[1] = "Willkommen zurück %s"
GLDG_GREETBACK[2] = "Hi %s, willkommen zurück"
GLDG_GREETBACK[3] = nil
-- Welcome to guild
GLDG_WELCOME[1] = "Wilkommen in der Gilde, %s"
GLDG_WELCOME[2] = "Hallo %s, toll Dich in der Gilde zu haben"
GLDG_WELCOME[3] = "Willkommen %s, schön, dass Du uns beitrittst"
-- Promotion
GLDG_RANK[1] = "%s, gratuliere zur Beförderung zu %s"
GLDG_RANK[2] = "Gratuliere %s zu %s"
-- New level
GLDG_LEVEL[1] = "%s, gratulire Level %s erreicht zu haben"
GLDG_LEVEL[2] = "%s, grats zu Level %s"

-- fran�ais (French)
--------------------
elseif (GetLocale() == "frFR") then
end
