## Interface: 20003
## Title: GuildGreet
## Version: 2.0.3
## Author: Mordikaiin & Rozak
## eMail: michaelquickel@comcast.net (Mordikaiin) Rozak@theblackcitadel.net (Rozak)
## Description: Keeps a log of guildies waiting for your greeting
## Notes: Keeps a log of guildies waiting for your greeting
## SavedVariables: GLDG_Data
## OptionalDeps: myAddOns
GuildGreet.xml
