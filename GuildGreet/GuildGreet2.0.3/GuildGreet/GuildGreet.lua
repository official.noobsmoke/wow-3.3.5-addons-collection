--[[--------------------------------------------------------
-- GuildGreet, a World of Warcraft social guild assistant --
------------------------------------------------------------
CODE INDEX (search on index for fast access):
_01_ Addon Variables
_02_ Addon Startup
_03_ Event Handler
_04_ Addon Initialization
_05_ Guild Roster Import
_06_ Monitor System Messages
_07_ Display Greetlist
_08_ Display Tooltip
_09_ Greet Players
_10_ Slash Handler
_11_ Tab Changer
_12_ General Tab Update
_13_ Greetings Tab Update
_14_ Players Tab Update
_15_ General Helper Functions
]]----------------------------------------------------------

-- Addon constants
GLDG_NAME 	= "GuildGreet"
GLDG_VERSION 	= "v2.0.3"
GLDG_DATE	= "12 January 2007"
GLDG_AUTHOR	= "Mordikaiin & Rozak"
GLDG_EMAIL	= "michaelquickel@comcast.net (Mordikaiin) Rozak@theblackcitadel.net (Rozak)"
GLDG_WEBSITE	= "Coming Soon"
GLDG_GUI	= "GuildGreetFrame"	-- Name of GUI config window
GLDG_LIST	= "GuildGreetList"	-- Name of GUI player list
GDLG_VNMBR	= 52			-- Number code for this version

-- Table linking tabs to frames
GLDG_Tab2Frame = {}
GLDG_Tab2Frame.Tab1 = "General"
GLDG_Tab2Frame.Tab2 = "Greetings"
GLDG_Tab2Frame.Tab3 = "Players"

-- Strings we look for
GLDG_ONLINE 	= ".*%[(.+)%]%S*"..string.sub(ERR_FRIEND_ONLINE_SS, 20)
GLDG_OFFLINE	= string.format(ERR_FRIEND_OFFLINE_S, "(.+)")
GLDG_JOINED	= string.format(ERR_GUILD_JOIN_S, "(.+)")
GLDG_PROMO	= string.format(ERR_GUILD_PROMOTE_SSS, "(.+)", "(.+)", "(.+)")
GLDG_DEMOTE	= string.format(ERR_GUILD_DEMOTE_SSS, ".+", "(.+)", "(.+)")


--------------------------
-- _01_ Addon Variables --
--------------------------

-- Stored data
GLDG_Data = {}			-- Data saved between sessions
GLDG_DataGuild = nil		-- Pointer to relevant guild section in GLDG_Data
GLDG_DataGreet = nil		-- Pointer to relevant greeting section in GLDG_Data
-- Initialization
GLDG_Main = nil			-- Main program window
GLDG_Realm = nil		-- Name of the current realm
GLDG_GuildName = nil		-- Name of your guild
GLDG_NewGuild = nil		-- Set if initializing a new guild
GLDG_UpdateRequest = 0		-- If set with time, update will be performed
GLDG_InitComplete = nil		-- Set in initialization is done
-- Various
GLDG_Debug = false		-- Show debugging
-- Core variables
GLDG_Online = {}		-- Time of player going online
GLDG_Offline = {}		-- Time of player going offline
GLDG_RankUpdate = {}		-- Set with time for all players getting promoted during the session
GLDG_Queue = {}			-- List of players waiting to be greeted
-- Configuration: greetings tab
GLDG_SelColName = nil		-- Name of the currently selected collection
GLDG_NumColRows = 5		-- Maximum number of collections that can be displayed
GLDG_ChangeName = nil		-- Name of the setting to be changed
GLDG_Selection = "Greet"	-- Selected greeting category
GLDG_SelMsgNum = nil		-- Number of the currently selected message
GLDG_NumSelRows = 5		-- Maximum number of greetings that can be displayed
GLDG_GreetOffset = 0		-- Offset for displaying greetings
-- Configuration: players tab
GLDG_SelPlrName = nil		-- Name of the currently selected player
GLDG_NumPlrRows = 20		-- Maximum number of players that can be displayed
GLDG_SortedList = {}		-- Sorted list of members of your guild, excluding your own characters
GLDG_PlayerOffset = 0		-- Offset for displaying players
GLDG_NumMain = 0		-- Number of players defined as main
GLDG_NumAlts = 0		-- Number of players that are alts for current selected player
GLDG_NumSubRows = 9		-- Maximum number of mains that can be displayed on subframe


------------------------
-- _02_ Addon Startup --
------------------------

function GLDG_OnLoad()
	-- Events monitored by Event Handler
	GLDG_Main = this
	this:RegisterEvent("ADDON_LOADED")
	this:RegisterEvent("PLAYER_ENTERING_WORLD")
	-- Slash commands for CLI
	SLASH_GLDG1 = "/guildgreet"
	SLASH_GLDG2 = "/gg"
	SlashCmdList.GLDG = GLDG_SlashHandler
end

------------------------------------------------------------
function GLDG_myAddons()
	-- Register addon with myAddons
	if not (myAddOnsFrame_Register) then return end
	myAddOnsFrame_Register({
		name = GLDG_NAME,
		version = GLDG_VERSION,
		releaseDate = GLDG_DATE,
		author = GLDG_AUTHOR,
		email = GLDG_EMAIL,
		website = GLDG_WEBSITE,
		category = MYADDONS_CATEGORY_GUILD,
		optionsframe = GLDG_GUI,
	})
end


------------------------
-- _03_ Event Handler --
------------------------

function GLDG_OnEvent()
	-- Distribute events to appropriate functions
	if (event == "ADDON_LOADED") and (arg1 == GLDG_NAME) then
		GLDG_myAddons()
		GLDG_Init()
		GLDG_Main:UnregisterEvent("ADDON_LOADED")
	elseif (event == "PLAYER_ENTERING_WORLD") then
		if not GLDG_InitComplete then GLDG_Init() end
		GLDG_InitRoster()
		GLDG_Main:UnregisterEvent("PLAYER_ENTERING_WORLD")
		GLDG_Main:RegisterEvent("GUILD_ROSTER_UPDATE")
	elseif (event == "GUILD_ROSTER_UPDATE") and IsInGuild() then
		if (GLDG_GuildName and GLDG_Realm) then GLDG_RosterImport() else GLDG_InitRoster() end
	elseif (event == "CHAT_MSG_SYSTEM") then GLDG_SystemMsg(arg1) end
end


-------------------------------
-- _04_ Addon Initialization --
-------------------------------

function GLDG_Init()
	-- Set defaults for missing settings
	if not GLDG_Data.RelogTime then GLDG_Data.RelogTime = 15 end
	if not GLDG_Data.ListSize then GLDG_Data.ListSize = 5 end
	if not GLDG_Data.Greet then GLDG_Data.Greet = GLDG_GREET end
	if not GLDG_Data.GreetBack then GLDG_Data.GreetBack = GLDG_GREETBACK end
	if not GLDG_Data.Welcome then GLDG_Data.Welcome = GLDG_WELCOME end
	if not GLDG_Data.NewRank then GLDG_Data.NewRank = GLDG_RANK end
	if not GLDG_Data.NewLevel then GLDG_Data.NewLevel = GLDG_LEVEL end
	if not GLDG_Data.Collections then GLDG_Data.Collections = {} end
	if not GLDG_Data.Custom then GLDG_Data.Custom = {} end
	if not GLDG_Data.Ranks then GLDG_Data.Ranks = {} end
	-- Set initial pointers to avoid errors (hack!)
	GLDG_DataGuild = GLDG_Data
	GLDG_DataGreet = GLDG_Data
	-- Keep version in configuration file
	GLDG_Data.Version = GDLG_VNMBR
	-- Initialize the list GUI
	getglobal(GLDG_LIST.."TitleText"):SetText(GLDG_NAME.." "..GLDG_VERSION)
	-- Initialize the config GUI
	getglobal(GLDG_GUI.."Title"):SetText(GLDG_NAME.." "..GLDG_VERSION)
	-- Make GUI close on escape
	tinsert(UISpecialFrames, GLDG_GUI)
	-- Initialize tabs and set the first one active
	local frame = getglobal(GLDG_GUI)
	PanelTemplates_SetNumTabs(frame, GLDG_TableSize(GLDG_Tab2Frame))
	PanelTemplates_SetTab(frame, 1)
	-- Set tab names and initialize tabframes
	for tabNum = 1, GLDG_TableSize(GLDG_Tab2Frame) do
		local tab = getglobal(GLDG_GUI.."Tab"..tabNum)
		local frameName = GLDG_Tab2Frame["Tab"..tabNum] 
		if frameName then
			local label = GLDG_TXT["Tab"..frameName]
			if label then
				-- tab has label: initialize frame
				tab:SetText(label)
				tab:Show()
				GLDG_InitFrame(frameName) end end end
	GLDG_InitComplete = true
end

------------------------------------------------------------
function GLDG_InitFrame(frameName)
	-- Set full name for frame
	local name = GLDG_GUI..frameName
	-- Configure the frames
	if (frameName == "General") then
		-- Greeting options texts
		getglobal(name.."Header"):SetText(GLDG_TXT.optheader)
		getglobal(name.."GreetAsMainText"):SetText(GLDG_TXT.greetasmain)
		getglobal(name.."WhisperText"):SetText(GLDG_TXT.whisper)
		getglobal(name.."ListNamesText"):SetText(GLDG_TXT.listNames)
		-- Queued greetings list texts
		getglobal(name.."ListHeader"):SetText(GLDG_TXT.listheader)
		getglobal(name.."ListdirectText"):SetText(GLDG_TXT.listdirect)
		getglobal(name.."ListheaderText"):SetText(GLDG_TXT.listvisible)
		-- Set value for checkboxes
		getglobal(name.."GreetAsMainBox"):SetChecked(GLDG_Data.GreetAsMain)
		getglobal(name.."WhisperBox"):SetChecked(GLDG_Data.Whisper)
		getglobal(name.."ListNamesBox"):SetChecked(GLDG_Data.ListNames)
		getglobal(name.."ListdirectBox"):SetChecked(GLDG_Data.ListUp)
		getglobal(name.."ListheaderBox"):SetChecked(GLDG_Data.ListVisible)
		-- Set values for Relog and Listsize sliders
		getglobal(name.."RelogSlider"):SetValue(GLDG_Data.RelogTime)
		getglobal(name.."ListsizeSlider"):SetValue(GLDG_Data.ListSize)
	elseif (frameName == "Greetings") then
		getglobal(name.."Header"):SetText(GLDG_TXT.greetheader)
		getglobal(name.."CollectHeader"):SetText(GLDG_TXT.collectheader)
		getglobal(name.."ColRealm"):SetText(GLDG_TXT.cbrealm)
		getglobal(name.."ColGuild"):SetText(GLDG_TXT.cbguild)
		getglobal(name.."ColPlayer"):SetText(GLDG_TXT.cbplayer)
		getglobal(name.."SubCustomHeader"):SetText(GLDG_TXT.custheader)
		getglobal(name.."SubNewHeader"):SetText(GLDG_TXT.cbnew)
		getglobal(name.."SubNewAdd"):SetText(GLDG_TXT.add)
		getglobal(name.."SubNewCancel"):SetText(GLDG_TXT.cancel)
		getglobal(name.."SubChangeSelection"):SetText(GLDG_TXT.selection)
		getglobal(name.."SubChangeGlobal"):SetText(GLDG_TXT.colglobal)
		getglobal(name.."SubChangeClear"):SetText(GLDG_TXT.nodef)
		getglobal(name.."SubChangeCancel"):SetText(GLDG_TXT.cancel)
		getglobal(name.."SelDefault"):SetText(GLDG_TXT.default)
		getglobal(name.."SelRelog"):SetText(GLDG_TXT.relog)
		getglobal(name.."SelJoin"):SetText(GLDG_TXT.joining)
		getglobal(name.."SelRank"):SetText(GLDG_TXT.newrank)
		getglobal(name.."SelLevel"):SetText(GLDG_TXT.newlevel)
	elseif (frameName == "Players") then
		-- Header and option texts
		getglobal(name.."Header"):SetText(GLDG_TXT.playersheader)
		getglobal(name.."IgnoreText"):SetText(GLDG_TXT.ignores)
		getglobal(name.."AltText"):SetText(GLDG_TXT.showalt)
		getglobal(name.."Alt2Text"):SetText(GLDG_TXT.groupalt)
		-- Button text
		getglobal(name.."Alias"):SetText(GLDG_TXT.pbalias)
		-- Set value for option checkboxes
		getglobal(name.."IgnoreBox"):SetChecked(GLDG_Data.ShowIgnore)
		getglobal(name.."AltBox"):SetChecked(GLDG_Data.ShowAlt)
		getglobal(name.."Alt2Box"):SetChecked(GLDG_Data.GroupAlt)
	end
end

------------------------------------------------------------
function GLDG_InitGreet(section)
	-- Set greetings section pointer if section exists
	local t = GLDG_Data.Custom
	if not t[section] then return true
	elseif (t[section] == "") then GLGG_DataGreet = GLDG_Data
	else	GLDG_SelColName = t[section]
		GLDG_DataGreet = GLDG_Data.Collections[t[section]] end
end

------------------------------------------------------------
function GLDG_InitRoster()
	-- Retreive realm name and guild name if needed
	if not GLDG_Realm then GLDG_Realm = GetCVar("realmName") end
	if not GLDG_GuildName then GLDG_GuildName = GetGuildInfo("player") end
	if not (GLDG_Realm and GLDG_GuildName) then return end
	-- Create the guild store if needed 
	if not GLDG_Data[GLDG_Realm.." - "..GLDG_GuildName] then
		GLDG_NewGuild = true
		GLDG_Data[GLDG_Realm.." - "..GLDG_GuildName] = {} end
	-- Set guild section pointer
	GLDG_DataGuild = GLDG_Data[GLDG_Realm.." - "..GLDG_GuildName]
	-- Set greetings section pointer
	if GLDG_InitGreet(GLDG_Realm.."-"..UnitName("player")) and
	   GLDG_InitGreet(GLDG_Realm.."-"..GLDG_GuildName) and
	   GLDG_InitGreet(GLDG_Realm) then
		-- No custom collections are used
		GLDG_DataGreet = GLDG_Data end
	-- Set initial custom collection settings
	GLDG_ShowCustom(GLDG_GUI.."Greetings")
	-- Launch request for full guild roster
	local offline = GetGuildRosterShowOffline()
	SetGuildRosterShowOffline(true)
	if not offline then SetGuildRosterShowOffline(false) end
end

------------------------------------------------------------
function GLDG_OnUpdate()
	-- Used only to initialize
	if not GLDG_UpdateRequest or (GetTime() < GLDG_UpdateRequest) then return end
	-- Get names for guild and realm
	if not (GLDG_Realm and GLDG_GuildName) then
		GLDG_UpdateRequest = GetTime() + 1
		GLDG_InitRoster()
	else
		GLDG_UpdateRequest = GetTime() + 10
		local setting = GetGuildRosterShowOffline()
		SetGuildRosterShowOffline(true)
		GuildRoster()
		if not setting then SetGuildRosterShowOffline(false) end
	end
end


------------------------------
-- _05_ Guild Roster Import --
------------------------------

function GLDG_RosterImport()
	-- Update guildrank names
	GLDG_Data.Ranks[GLDG_Realm.."-"..GLDG_GuildName] = {}
	for i = 1, GuildControlGetNumRanks() do GLDG_Data.Ranks[GLDG_Realm.."-"..GLDG_GuildName][GuildControlGetRankName(i)] = i end
	-- Add info about all players of your guild
	local cnt = 0
	local complete = false
	for i = 1, GetNumGuildMembers() do
		local pl, rn, ri, lv, cl, zn, pn, on, ol, st = GetGuildRosterInfo(i)
		if pl then 
			cnt = cnt +1
			if not GLDG_DataGuild[pl] then
				-- New player
				GLDG_DataGuild[pl] = {}
				if not GLDG_NewGuild and (GLDG_TableSize(GLDG_DataGreet.Welcome) > 0) then
					GLDG_DataGuild[pl].new = true end end
			if (pl == UnitName("player")) then
				-- This player is our own: ignore completely
				GLDG_DataGuild[pl].own = true
				GLDG_DataGuild[pl].ignore = true end
			if not GLDG_DataGuild[pl].lvl then GLDG_DataGuild[pl].lvl = lv end
			if (type(GLDG_DataGuild[pl].newrank) ~= "number") then GLDG_DataGuild[pl].newrank = nil end
			if not GLDG_RankUpdate[pl] then
				if GLDG_DataGuild[pl].newrank and (ri > GLDG_DataGuild[pl].newrank) then
					-- Player got demoted again
					GLDG_DataGuild[pl].promotor = nil
					GLDG_DataGuild[pl].newrank = nil
				elseif GLDG_DataGuild[pl].newrank and (ri < GLDG_DataGuild[pl].newrank) then
					-- Player got promoted again
					GLDG_DataGuild[pl].promotor = nil
					GLDG_DataGuild[pl].newrank = ri
				elseif GLDG_DataGuild[pl].rank and (ri < GLDG_DataGuild[pl].rank) then
					-- Player got promoted
					GLDG_DataGuild[pl].newrank = ri end
				GLDG_DataGuild[pl].rank = ri
				GLDG_DataGuild[pl].rankname = rn end
			if ol then
				-- Remove rank info if we're not interested in it
				if GLDG_DataGuild[pl].newrank and ((GLDG_TableSize(GLDG_DataGreet.NewRank) == 0) or GLDG_DataGuild[pl].ignore) then
					GLDG_DataGuild[pl].promotor = nil
					GLDG_DataGuild[pl].newrank = nil end
				-- Add promoted player to queue if hour is known
				if GLDG_DataGuild[pl].newrank and GLDG_RankUpdate[pl] and (not GLDG_Queue[pl]) then
					GLDG_Queue[pl] = GLDG_RankUpdate[pl] end
				-- Update player level
				if (lv > GLDG_DataGuild[pl].lvl) and (GLDG_TableSize(GLDG_DataGreet.NewLevel) > 0) and not GLDG_DataGuild[pl].ignore then 
					GLDG_DataGuild[pl].newlvl = true end
				-- Update queue with all changes still missing
				if (GLDG_DataGuild[pl].new or GLDG_DataGuild[pl].newlvl or GLDG_DataGuild[pl].newrank) and (not GLDG_Queue[pl]) then
					GLDG_Queue[pl] = "[??:??] " end
				GLDG_DataGuild[pl].lvl = lv
			else complete = true end
			if not GLDG_Offline[pl] then GLDG_Offline[pl] = false end
			if (GLDG_Online[pl] == nil) then GLDG_Online[pl] = ol end end end
	-- If we got our info, switch to the next phase
	if (cnt > 0) and complete then
		GLDG_NewGuild = false
		GLDG_UpdateRequest = nil
		GLDG_RosterPurge()
		GLDG_Main:RegisterEvent("CHAT_MSG_SYSTEM")
		GLDG_ShowQueue() end
end

------------------------------------------------------------
function GLDG_RosterPurge()
	-- Don't purge if list is not complete
	if not GetGuildRosterShowOffline() then return end
	-- Set guildlist
	local purge = {}
	-- Purge old members from database
	for p in pairs(GLDG_DataGuild) do if (GLDG_Offline[p] == nil) then purge[p] = true end end
	for p in pairs(purge) do GLDG_DataGuild[p] = nil end
end


------------------------------------------
-- assemble list of alts of a main char --
------------------------------------------
-- Urbin
function GLDG_FindAlts(mainName, playerName)
	local altList = "["..mainName.."] "

	for player in pairs(GLDG_DataGuild) do
		if ( GLDG_DataGuild[player].alt == mainName ) then
			if (not (player == playerName)) then
				altList = altList..player.." "
			--else
				--altList = altList.."("..player..") "
			end
		end
	end

	return altList
end

------------------------------------------
-- list of alts and main of a char --
------------------------------------------
-- Urbin
function GLDG_ListForPlayer(player)
	if player ~= nil then
		if GLDG_DataGuild[player] ~= nil then
			if GLDG_DataGuild[player].alt then
				--
				-- Alt of Main
				--
				local altsList = GLDG_FindAlts(GLDG_DataGuild[player].alt, player)
				GLDG_Print(GLDG_NAME..": "..player.." "..altsList)
			else
				if GLDG_DataGuild[player].main then
					--
					-- Main
					--
					local altsList = GLDG_FindAlts(player, player)
					GLDG_Print(GLDG_NAME..": "..altsList)
				--else
					--
					-- Player has no alt
					--
					-- GLDG_Print(GLDG_NAME..": "..player.." has no alts")
				end
			end
		else
			GLDG_Print(player.." is not in guild")
		end
	end
end

------------------------------------------
-- list of alts and main of all players --
------------------------------------------
-- bee
function GLDG_ListAllPlayers(singles)
	for player in pairs(GLDG_DataGuild) do
		if (singles) then
			if ( not GLDG_DataGuild[player].alt ) then
				GLDG_ListForPlayer(player)
			end
		else
			if ( GLDG_DataGuild[player].main or not GLDG_DataGuild[player].alt ) then
				GLDG_ListForPlayer(player)
			end
		end
	end
end

----------------------------------
-- _06_ Monitor System Messages --
----------------------------------

function GLDG_SystemMsg(msg)
	-- Receiving system message
	GLDG_DebugPrint("incoming system message: "..msg)
	
	-- Check players coming online
	local _, _, player = string.find(msg, GLDG_ONLINE)
	if player then
		GLDG_DebugPrint("detected player coming online: "..player)
		if (not GLDG_DataGuild[player]) or GLDG_DataGuild[player].ignore then return end
		GLDG_DebugPrint("player "..player.." is a member of our guild")
		GLDG_Online[player] = GetTime()

		-- Urbin
		if GLDG_Data.ListNames then
			if GLDG_DataGuild[player].alt then
				--
				-- Alt of Main
				--
				local altsList = GLDG_FindAlts(GLDG_DataGuild[player].alt, player)
				GLDG_Print(GLDG_NAME..": "..player.." "..altsList)
			else
				if GLDG_DataGuild[player].main then
					--
					-- Main
					--
					local altsList = GLDG_FindAlts(player, player)
					GLDG_Print(GLDG_NAME..": "..altsList)
				else
					--
					-- Player has no alt
					--
					GLDG_Print(GLDG_NAME..": "..player.." has no Alts!")
				end
			end
		end

		if GLDG_DataGuild[player].alt then GLDG_Offline[player] = GLDG_Offline[GLDG_DataGuild[player].alt] end
		if GLDG_Offline[player] and (GLDG_Online[player] - GLDG_Offline[player] < GLDG_Data.RelogTime * 60) then return end
		GLDG_DebugPrint("player "..player.." is not been online in the last "..GLDG_Data.RelogTime.." minutes.")
		if GLDG_Offline[player] and (GLDG_TableSize(GLDG_DataGreet.GreetBack) == 0) then return end
		GLDG_DebugPrint("player "..player.." is not been online before")
		if not (GLDG_Offline[player] or GLDG_DataGuild[player].new or GLDG_DataGuild[player].newlvl or GLDG_DataGuild[player].newrank) and (GLDG_TableSize(GLDG_DataGreet.Greet) == 0) then return end
		GLDG_DebugPrint("player "..player.." should be greeted")
		GLDG_Queue[player] = GLDG_GetLogtime(player)
		GLDG_ShowQueue()
		return end

	-- Check players going offline
	local _, _, player = string.find(msg, GLDG_OFFLINE)
	if player then
		GLDG_DebugPrint("detected player going offline: "..player)
		if (not GLDG_DataGuild[player]) or GLDG_DataGuild[player].ignore then return end
		GLDG_DebugPrint("player "..player.." is a member of our guild")
		GLDG_Online[player] = nil
		GLDG_RankUpdate[player] = nil
		if GLDG_DataGuild[player].alt then
			local main = GLDG_DataGuild[player].alt
			GLDG_DataGuild[main].last = player
			GLDG_Offline[main] = GetTime()
		elseif GLDG_DataGuild[player].main then
			GLDG_DataGuild[player].last = player
			GLDG_Offline[player] = GetTime()
		else GLDG_Offline[player] = GetTime() end
		if GLDG_Queue[player] then
			GLDG_Queue[player] = nil
			GLDG_ShowQueue() end
		return end

	-- Check players joining the guild
	local _, _, player = string.find(msg, GLDG_JOINED)
	if player and (GLDG_TableSize(GLDG_DataGreet.Welcome) > 0) then
		GLDG_DebugPrint("detected player joining guild: "..player)
		GLDG_DataGuild[player] = {}
		GLDG_Online[player] = GetTime()
		GLDG_Offline[player] = false
		GLDG_Queue[player] = GLDG_GetLogtime(player)
		GLDG_DataGuild[player].new = true
		GLDG_ShowQueue()
		return end
		
	-- Check for promotions
	local _, _, promo, player, rank = string.find(msg, GLDG_PROMO)
	if player and (GLDG_TableSize(GLDG_DataGreet.NewRank) > 0) and not GLDG_DataGuild[player].ignore then
		GLDG_DebugPrint("detected player getting promotion: "..player.." -> "..rank)
		GLDG_DataGuild[player].promoter = promo
		GLDG_DataGuild[player].rankname = rank
		if GLDG_Data.Ranks[GLDG_Realm.."-"..GLDG_GuildName] and GLDG_Data.Ranks[GLDG_Realm.."-"..GLDG_GuildName][rank] then
			GLDG_DataGuild[player].rank = GLDG_Data.Ranks[GLDG_Realm.."-"..GLDG_GuildName][rank] - 1 end
		GLDG_RankUpdate[player] = GLDG_GetLogtime(player)
		GLDG_DataGuild[player].newrank = GLDG_DataGuild[player].rank
		if GLDG_Online[player] then
			GLDG_Queue[player] = GLDG_GetLogtime(player)
			GLDG_ShowQueue() end
		return end
		
	-- Check for demotions
	local _, _, player, rank = string.find(msg, GLDG_DEMOTE)
	if player then
		GLDG_DebugPrint("detected player getting demotion: "..player.." -> "..rank)
		GLDG_DataGuild[player].promoter = nil
		GLDG_DataGuild[player].rankname = rank
		if GLDG_Data.Ranks[GLDG_Realm.."-"..GLDG_GuildName] and GLDG_Data.Ranks[GLDG_Realm.."-"..GLDG_GuildName][rank] then
			GLDG_DataGuild[player].rank = GLDG_Data.Ranks[GLDG_Realm.."-"..GLDG_GuildName][rank] - 1 end
		GLDG_RankUpdate[player] = GLDG_GetLogtime(player)
		if GLDG_DataGuild[player].newrank then
			GLDG_DataGuild[player].newrank = nil
			GLDG_Queue[player] = nil
			GLDG_ShowQueue() end
		return end
end

------------------------------------------------------------
function GLDG_GetLogtime(player)
	-- Helper function: mark playing coming online
	local hour, minute = GetGameTime()
	if (hour < 10) then hour = "[0"..hour else hour = "["..hour end
	if (minute < 10) then minute = ":0"..minute.."] " else minute = ":"..minute.."] " end
	return hour..minute
end


----------------------------
-- _07_ Display Greetlist --
----------------------------

function GLDG_ShowQueue()
	-- Sort queue according logon time
	local queue = {}
	local total = 0
	for p in pairs(GLDG_Queue) do
		-- Look for the position in the list we need
		local loc = 1
		while queue[loc] and (GLDG_Online[queue[loc]] <= GLDG_Online[p]) do loc = loc + 1 end
		-- We found the position: move everything beyond it
		for cnt = total, loc, -1 do queue[cnt+1] = queue[cnt] end
		-- Insert the player
		total = total + 1
		queue[loc] = p end
	-- Display the list as needed
	if (total == 0) and not (GLDG_Debug or GLDG_Data.ListVisible) then
		-- No lines and not debugging: hide main window
		getglobal(GLDG_LIST):Hide()
		return end
	-- Hide the unused direction
	local dir = "ULine"
	if GLDG_Data.ListUp then dir = "Line" end
	cnt = 1
	while getglobal(GLDG_LIST..dir..cnt) do
		getglobal(GLDG_LIST..dir..cnt):Hide()
		cnt = cnt + 1 end
	if GLDG_Data.ListUp then dir = "ULine" else dir = "Line" end
	-- Show the used direction
	for cnt = 1, math.min(total, GLDG_Data.ListSize) do
		local line = GLDG_LIST..dir..cnt
		local colorName = "default"
		getglobal(line.."Text"):SetText(GLDG_Queue[queue[cnt]]..queue[cnt])
		getglobal(line.."Text2"):SetText("")
		if GLDG_DataGuild[queue[cnt]].new then
			colorName = "new"
			getglobal(line.."Text2"):SetText(GLDG_TXT.new)
		elseif GLDG_DataGuild[queue[cnt]].newlvl then
			colorName = "lvl"
			getglobal(line.."Text2"):SetText(GLDG_TXT.lvl)
		elseif GLDG_DataGuild[queue[cnt]].newrank then
			colorName = "rank"
			getglobal(line.."Text2"):SetText(GLDG_TXT.rank)
		elseif GLDG_Offline[queue[cnt]] then
			colorName = "relog" end
		GLDG_SetTextColor(line.."Text", colorName)
		GLDG_SetTextColor(line.."Text2", colorName)
		getglobal(line):Show() end
	-- Hide lines we don't need
	cnt = total + 1
	while getglobal(GLDG_LIST..dir..cnt) do
		getglobal(GLDG_LIST..dir..cnt):Hide()
		cnt = cnt + 1 end
	-- Show main window
	getglobal(GLDG_LIST):Show()
end


--------------------------
-- _08_ Display Tooltip --
--------------------------

function GLDG_ShowToolTip(buttonName)
	-- Tooltip title: use player name and color
	local name = string.sub(getglobal(buttonName.."Text"):GetText(), 9)
	local logtime = string.sub(getglobal(buttonName.."Text"):GetText(), 2, 6)
	local r, g, b = getglobal(buttonName.."Text"):GetTextColor()
	-- Construct tip
	local tip = string.format(GLDG_TXT.tipdef, logtime)
	if GLDG_DataGuild[name].new then
		if (logtime == "??:??") then tip = GLDG_TXT.tipnew2
		else tip = string.format(GLDG_TXT.tipnew, logtime) end
	elseif GLDG_DataGuild[name].newlvl then tip = string.format(GLDG_TXT.tiplvl, GLDG_DataGuild[name].lvl)
	elseif GLDG_DataGuild[name].newrank then
		if GLDG_DataGuild[name].promoter then
			 if (logtime == "??:??") then tip = string.format(GLDG_TXT.tiprank, GLDG_DataGuild[name].promoter, GLDG_DataGuild[name].rankname)
			 else tip = string.format(GLDG_TXT.tiprank2, logtime, GLDG_DataGuild[name].promoter, GLDG_DataGuild[name].rankname) end
		else tip = string.format(GLDG_TXT.tiprank3, GLDG_DataGuild[name].rankname) end
	elseif GLDG_Offline[name] then
		local minoff = math.ceil((GLDG_Online[name] - GLDG_Offline[name]) / 60)
		local hrsoff = math.floor(minoff / 60)
		minoff = math.fmod(minoff, 60)
		local timestr = ""
		if (hrsoff > 0) then timestr = string.format(GLDG_TXT.hour, hrsoff) end
		if (minoff > 0) then timestr = timestr..string.format(GLDG_TXT.minute, minoff) end
		tip = string.format(GLDG_TXT.tiprelog, logtime, timestr) end
	-- If this is a main, add last used character to tip
	if GLDG_DataGuild[name].main and GLDG_DataGuild[name].last then tip = tip..string.format(GLDG_TXT.tiprelogalt, GLDG_DataGuild[name].last)
	-- If this is not the main, add last used character and main information to tip
	elseif GLDG_DataGuild[name].alt then
		local main = GLDG_DataGuild[name].alt
		if GLDG_DataGuild[main].last then tip = tip..string.format(GLDG_TXT.tiprelogalt, GLDG_DataGuild[main].last) end
		tip = tip..string.format(GLDG_TXT.tipalt, GLDG_DataGuild[name].alt) end
	-- If player has alias, add to name
	if GLDG_DataGuild[name].alias then name = name.." ("..GLDG_DataGuild[name].alias..")" end
	-- Show tooltip
	GameTooltip_SetDefaultAnchor(GameTooltip, this)
	GameTooltip:SetText(name, r, g, b, 1.0, 1)
	GameTooltip:AddLine(tip, 1, 1, 1, 1.0, 1)
	GameTooltip:Show()
end


------------------------
-- _09_ Greet Players --
------------------------

function GLDG_ClickName(button, name)
	-- Strip timestamp from name
	name = string.sub(name, 9)
	-- Greet the player if left click
	if (button == "LeftButton") then GLDG_SendGreet(name) end
	-- Cleanup where needed
	local player = GLDG_DataGuild[name]
	player.new = nil
	player.newlvl = nil
	player.newrank = nil
	player.promoter = nil
	-- Remove name from queue and refresh
	GLDG_Queue[name] = nil
	GLDG_ShowQueue()
end

------------------------------------------------------------
function GLDG_SendGreet(name)
	-- Choose appropriate greeting list
	local option = ""
	local list = GLDG_DataGreet.Greet
	local player = GLDG_DataGuild[name]
	local cname = name
	if player.new then list = GLDG_DataGreet.Welcome
	elseif player.newrank then
		list = GLDG_DataGreet.NewRank
		option = player.rankname
	elseif player.newlvl then
		list = GLDG_DataGreet.NewLevel
		option = player.lvl
	elseif GLDG_Offline[name] then list = GLDG_DataGreet.GreetBack end
	-- Switch to alias if defined
	if player.alias then name = player.alias
	-- Use name of main if alt and option is set
	elseif player.alt and GLDG_Data.GreetAsMain then
		if GLDG_DataGuild[player.alt].alias then name = GLDG_DataGuild[player.alt].alias else name = player.alt end end
	-- Select a random greeting
	local greet = list[math.random(GLDG_TableSize(list))]
	-- Send greeting
	if GLDG_Data.Whisper then
		SendChatMessage(string.format(greet, name, option), "WHISPER", nil, cname)
	else
		SendChatMessage(string.format(greet, name, option), "GUILD")
	end
end


------------------------
-- _10_ Slash Handler --
------------------------

function GLDG_SlashHandler(msg)
	-- Urbin
	local msgorig = ""
	if msg then msgorig = msg end

	if msg then msg = string.lower(msg) end
	if msg and string.sub(msg, 1, 5) == "debug" then
		-- Debug option
		if string.sub(msg, 7, 9) == "off" then GLDG_Debug = false
		elseif string.sub(msg, 7, 8) == "on" then GLDG_Debug = true
		elseif string.sub(msg, 7, 12) == "toggle" then GLDG_Debug = not GLDG_Debug end
		local state = "OFF"
		if GLDG_Debug then state = "ON" end
		DEFAULT_CHAT_FRAME:AddMessage("GUILDGREET DEBUG IS NOW "..state)
		GLDG_ShowQueue()

	-- Urbin
	elseif msg ~= nil and msg == "*" then GLDG_ListAllPlayers(false)
	elseif msg ~= nil and msg ~= "" then GLDG_ListForPlayer(msgorig)

	-- Default behaviour: start configuration GUI
	else getglobal(GLDG_GUI):Show() end
end


----------------------
-- _11_ Tab Changer --
----------------------

function GLDG_ClickTab(tabName)
	-- Actions to perform when a tab is clicked
	PanelTemplates_Tab_OnClick(getglobal(GLDG_GUI))
	-- Show frame linked to tab, hide all others
	local tabNum = 1
	while getglobal(GLDG_GUI.."Tab"..tabNum) do
		local frame = nil
		if GLDG_Tab2Frame["Tab"..tabNum] then frame = getglobal (GLDG_GUI..GLDG_Tab2Frame["Tab"..tabNum]) end
		if frame then if (tabName == GLDG_GUI.."Tab"..tabNum) then frame:Show() else frame:Hide() end end
		tabNum = tabNum + 1 end
end


-----------------------------
-- _12_ General Tab Update --
-----------------------------

function GLDG_UpdateRelog()
	-- Store the new value
	GLDG_Data.RelogTime = this:GetValue()
	-- Update display
	local text = getglobal(this:GetParent():GetName().."RelogText")
	if (GLDG_Data.RelogTime == 0) then text:SetText(GLDG_TXT.relogalways)
	else text:SetText(string.format(GLDG_TXT.reloglimit, GLDG_Data.RelogTime)) end
end

------------------------------------------------------------
function GLDG_UpdateListsize()
	-- Store the new value
	GLDG_Data.ListSize = this:GetValue()
	-- Update display
	local text = getglobal(this:GetParent():GetName().."ListsizeText")
	text:SetText(string.format(GLDG_TXT.listsize, GLDG_Data.ListSize))
	-- Update queue
	GLDG_ShowQueue()
end


-------------------------------
-- _13_ Greetings Tab Update --
-------------------------------

function GLDG_ShowCollections(frame)
	-- Set frame
	if not frame then frame = this:GetParent():GetName() end
	-- Update scrollbar
	local bar = getglobal(frame.."Collectbar")
	FauxScrollFrame_Update(bar, GLDG_TableSize(GLDG_Data.Collections), GLDG_NumColRows, 15)
	local offset = FauxScrollFrame_GetOffset(bar)
	-- Sort the list of existing custom collections
	local sorted = {}
	local total = 0
	for col in pairs(GLDG_Data.Collections) do
		-- Look for the position in the list we need
		local loc = 1
		while sorted[loc] and (sorted[loc] < col) do loc = loc + 1 end
		-- We found the position: move everything beyond it
		for cnt = total, loc, -1 do sorted[cnt+1] = sorted[cnt] end
		-- Insert the collection
		total = total + 1
		sorted[loc] = col end
	-- Show the collections visible
	for i = 1, GLDG_NumColRows do
		getglobal(frame.."Collect"..i):UnlockHighlight()
		if sorted[i + offset] then
			getglobal(frame.."Collect"..i.."Text"):SetText(sorted[i + offset])
			getglobal(frame.."Collect"..i):Enable()
			if GLDG_SelColName and (GLDG_SelColName == sorted[i + offset]) then getglobal(frame.."Collect"..i):LockHighlight() end
		else	getglobal(frame.."Collect"..i.."Text"):SetText("")
			getglobal(frame.."Collect"..i):Disable() end end
	-- Set buttons and text depending the selected collection
	local colhead = GLDG_TXT.colglobal
	if GLDG_SelColName then
		getglobal(frame.."ColNewDel"):SetText(GLDG_TXT.cbdel)
		colhead = string.format(GLDG_TXT.colcustom, GLDG_SelColName)
	else 	getglobal(frame.."ColNewDel"):SetText(GLDG_TXT.cbnew) end
	getglobal(frame.."GreetHeader"):SetText(string.format(GLDG_TXT.msgheader, colhead))
end

------------------------------------------------------------
function GLDG_ShowGreetings(frame)
	-- Show messages from selected category
	local list = GLDG_Data[GLDG_Selection]
	if GLDG_SelColName then list = GLDG_Data.Collections[GLDG_SelColName][GLDG_Selection] end
	local cnt = 1
	local line = frame.."Line"
	while list[cnt + GLDG_GreetOffset] and getglobal(line..cnt) do
		getglobal(line..cnt.."Text"):SetText(list[cnt + GLDG_GreetOffset])
		getglobal(line..cnt):Enable()
		cnt = cnt + 1 end
	while getglobal(line..cnt) do
		getglobal(line..cnt.."Text"):SetText("")
		getglobal(line..cnt):Disable()
		cnt = cnt + 1 end
	-- Set highlight
	for cnt = 1, GLDG_NumSelRows do
		if GLDG_SelMsgNum and (GLDG_SelMsgNum == GLDG_GreetOffset + cnt) then getglobal(line..cnt):LockHighlight()
		else getglobal(line..cnt):UnlockHighlight() end end
	-- Set editbox
	if GLDG_SelMsgNum then getglobal(frame.."Editbox"):SetText(list[GLDG_SelMsgNum])
	else getglobal(frame.."Editbox"):SetText("") end
	-- Set editbox buttons
	if GLDG_SelMsgNum then
		getglobal(frame.."MsgAdd"):SetText(GLDG_TXT.update)
		getglobal(frame.."MsgDel"):SetText(GLDG_TXT.delete)
	else	getglobal(frame.."MsgAdd"):SetText(GLDG_TXT.add)
		getglobal(frame.."MsgDel"):SetText(GLDG_TXT.clear) end
end

------------------------------------------------------------
function GLDG_ShowCustom(frame)
	-- Display the current values
	local d = GLDG_Data.Custom[GLDG_Realm]
	if d and (d ~= "") and not GLDG_Data.Collections[d] then
		-- Collection no longer exists
		GLDG_Data.Custom[GLDG_Realm] = nil
		d = nil end
	local f = getglobal(frame.."SubCustomRealm")
	if not d then f:SetText(GLDG_TXT.nodef)
	elseif (d == "") then f:SetText(GLDG_TXT.colglobal)
	else f:SetText(d) end
	d = GLDG_Data.Custom[GLDG_Realm.."-"..GLDG_GuildName]
	if d and (d ~= "") and not GLDG_Data.Collections[d] then
		-- Collection no longer exists
		GLDG_Data.Custom[GLDG_Realm.."-"..GLDG_GuildName] = nil
		d = nil end
	f = getglobal(frame.."SubCustomGuild")
	if not d then f:SetText(GLDG_TXT.nodef)
	elseif (d == "") then f:SetText(GLDG_TXT.colglobal)
	else f:SetText(d) end
	d = GLDG_Data.Custom[GLDG_Realm.."-"..UnitName("player")]
	if d and (d ~= "") and not GLDG_Data.Collections[d] then
		-- Collection no longer exists
		GLDG_Data.Custom[GLDG_Realm.."-"..UnitName("player")] = nil
		d = nil end
	f = getglobal(frame.."SubCustomChar")
	if not d then f:SetText(GLDG_TXT.nodef)
	elseif (d == "") then f:SetText(GLDG_TXT.colglobal)
	else f:SetText(d) end
	-- Set greetings section pointer at is possibly changed
	if GLDG_InitGreet(GLDG_Realm.."-"..UnitName("player")) and
	   GLDG_InitGreet(GLDG_Realm.."-"..GLDG_GuildName) and
	   GLDG_InitGreet(GLDG_Realm) then
		-- No custom collections are used
		GLDG_DataGreet = GLDG_Data end
	-- Show the frame
	GLDG_ColButtonPressed(frame)
	getglobal(frame.."SubCustom"):Show()
end

------------------------------------------------------------
function GLDG_ClickCollection(col)
	-- Set new selected collection
	if GLDG_SelColName and (GLDG_SelColName == col) then GLDG_SelColName = nil
	else GLDG_SelColName = col end
	-- Update change selection button
	local button = getglobal(this:GetParent():GetName().."SubChangeSelection")
	if GLDG_SelColName then button:Enable() else button:Disable() end
	-- Refresh display
	GLDG_ShowCollections()
	GLDG_ShowGreetings(this:GetParent():GetName())
end

------------------------------------------------------------
function GLDG_ColButtonPressed(frame)
	-- Enable all collections buttons
	getglobal(frame.."ColNewDel"):Enable()
	getglobal(frame.."ColRealm"):Enable()
	getglobal(frame.."ColGuild"):Enable()
	getglobal(frame.."ColPlayer"):Enable()
	-- Hide all subframes
	getglobal(frame.."SubCustom"):Hide()
	getglobal(frame.."SubNew"):Hide()
	getglobal(frame.."SubChange"):Hide()
end

------------------------------------------------------------
function GLDG_ClickNewCol(frame)
	-- If collection is selected, remove it
	if GLDG_SelColName then
		GLDG_Data.Collections[GLDG_SelColName] = nil
		GLDG_SelColName = nil
		GLDG_ShowCustom(frame)
		GLDG_ShowCollections(frame)
		return end
	-- Disable this button and show the subframe
	GLDG_ColButtonPressed(frame)
	this:Disable()
	getglobal(frame.."SubNew"):Show()
end

------------------------------------------------------------
function GLDG_ClickChangeCustom(setting, frame)
	-- Set the value to be updated
	GLDG_ChangeName = GLDG_Realm
	if (setting == "guild") then
		getglobal(frame.."SubChangeHeader"):SetText(GLDG_TXT.cbguild)
		GLDG_ChangeName = GLDG_ChangeName.."-"..GLDG_GuildName
	elseif (setting == "char") then
		getglobal(frame.."SubChangeHeader"):SetText(GLDG_TXT.cbplayer)
		GLDG_ChangeName = GLDG_ChangeName.."-"..UnitName("player")
	else	getglobal(frame.."SubChangeHeader"):SetText(GLDG_TXT.cbrealm) end
	-- Disable this button and show the subframe
	GLDG_ColButtonPressed(frame)
	this:Disable()
	getglobal(frame.."SubChange"):Show()
end

------------------------------------------------------------
function GLDG_ClickNewColAdd(frame)
	-- Don't do anything if no name is given or name already exists
	local box = getglobal(this:GetParent():GetName().."Editbox")
	local name = box:GetText()
	if (name == "") or GLDG_Data.Collections[name] then return end
	box:SetText("")
	-- Create the new collection
	GLDG_Data.Collections[name] = {}
	local n = GLDG_Data.Collections[name]
	n.Greet = {}
	n.GreetBack = {}
	n.Welcome = {}
	n.NewRank = {}
	n.NewLevel = {}
	-- Refresh list and hide new collection frame
	GLDG_ShowCustom(frame)
	GLDG_ShowCollections(frame)
end

------------------------------------------------------------
function GLDG_ChangeCollect(setting, frame)
	-- Set new setting
	if (setting == "clear") then GLDG_Data.Custom[GLDG_ChangeName] = nil
	elseif (setting == "global") then GLDG_Data.Custom[GLDG_ChangeName] = ""
	elseif (setting == "selection") then GLDG_Data.Custom[GLDG_ChangeName] = GLDG_SelColName end
	-- Hide frame
	GLDG_ShowCustom(frame)
end

------------------------------------------------------------
function GLDG_ClickGreetButton(id)
	-- Set frame
	local frame = this:GetName()
	-- Change selection if id is given
	if id then
		frame = this:GetParent():GetName()
		GLDG_SelMsgNum = nil
		getglobal(frame.."Editbox"):SetText("")
		local map = {}
		map[1] = "Greet"
		map[2] = "GreetBack"
		map[3] = "Welcome"
		map[4] = "NewRank"
		map[5] = "NewLevel"
		GLDG_Selection = map[id] end
	-- Enable all buttons except active
	if (GLDG_Selection == "Greet") then getglobal(frame.."SelDefault"):Disable() else getglobal(frame.."SelDefault"):Enable() end
	if (GLDG_Selection == "GreetBack") then getglobal(frame.."SelRelog"):Disable() else getglobal(frame.."SelRelog"):Enable() end
	if (GLDG_Selection == "Welcome") then getglobal(frame.."SelJoin"):Disable() else getglobal(frame.."SelJoin"):Enable() end
	if (GLDG_Selection == "NewRank") then getglobal(frame.."SelRank"):Disable() else getglobal(frame.."SelRank"):Enable() end
	if (GLDG_Selection == "NewLevel") then getglobal(frame.."SelLevel"):Disable() else getglobal(frame.."SelLevel"):Enable() end
	-- Update editbox header
	getglobal(frame.."EditboxHeader"):SetText(GLDG_TXT[GLDG_Selection])
	-- Refresh scrollbar
	GLDG_ClickGreetScrollBar(getglobal(frame.."Scrollbar"))
end

------------------------------------------------------------
function GLDG_ClickGreetScrollBar(frame)
	if not frame then frame = this end
	GLDG_GreetOffset = FauxScrollFrame_GetOffset(frame)
	local list = GLDG_Data[GLDG_Selection]
	if GLDG_SelColName then list = GLDG_Data.Collections[GLDG_SelColName][GLDG_Selection] end
	FauxScrollFrame_Update(frame, GLDG_TableSize(list), GLDG_NumSelRows, 15)
	GLDG_ShowGreetings(frame:GetParent():GetName())
end

------------------------------------------------------------
function GLDG_ClickGreeting(id)
	-- Set new selected message
	if GLDG_SelMsgNum and (GLDG_SelMsgNum == GLDG_GreetOffset + id) then GLDG_SelMsgNum = nil
	else GLDG_SelMsgNum = GLDG_GreetOffset + id end
	-- Refresh greetings list
	GLDG_ShowGreetings(this:GetParent():GetName())
end

------------------------------------------------------------
function GLDG_ClickGreetAdd(frame)
	-- Clear focus
	getglobal(frame.."Editbox"):ClearFocus()
	-- Refuse if no input in editbox
	local text = getglobal(frame.."Editbox"):GetText()
	if (text == "") then return end
	-- Update message if one is selected
	local list = GLDG_Data[GLDG_Selection]
	if GLDG_SelColName then list = GLDG_Data.Collections[GLDG_SelColName][GLDG_Selection] end
	if GLDG_SelMsgNum then list[GLDG_SelMsgNum] = text
	-- Add message if none is selected
	else 	getglobal(frame.."Editbox"):SetText("")
		list[GLDG_TableSize(list) + 1] = text end
	-- Update display
	GLDG_ClickGreetScrollBar(getglobal(frame.."Scrollbar"))
end

------------------------------------------------------------
function GLDG_ClickGreetRemove(frame)
	-- Clear focus
	getglobal(frame.."Editbox"):ClearFocus()
	-- Remove message if one is selected
	local list = GLDG_Data[GLDG_Selection]
	if GLDG_SelColName then list = GLDG_Data.Collections[GLDG_SelColName][GLDG_Selection] end
	if GLDG_SelMsgNum then
		local cnt = GLDG_SelMsgNum
		while list[cnt + 1] do
			list[cnt] = list[cnt + 1]
			cnt = cnt + 1 end
		GLDG_SelMsgNum = nil
		list[cnt] = nil end
	-- Update display
	GLDG_ClickGreetScrollBar(getglobal(frame.."Scrollbar"))
end


-----------------------------
-- _14_ Players Tab Update --
-----------------------------

function GLDG_SortString(player)
	-- Helper function: returns string that should be used for sorting
	local result = player
	if (GLDG_DataGuild[player].alt and GLDG_Data.GroupAlt) then result = GLDG_DataGuild[player].alt..player end
	return string.lower(result)
end

------------------------------------------------------------
function GLDG_ListPlayers()
	-- Abort if addon is not initialized
	if not GLDG_DataGuild then return end
	-- Check if there is a reason to clear the selected player
	if GLDG_SelPlrName then
		if (not GLDG_DataGuild[GLDG_SelPlrName]) or
		   (GLDG_DataGuild[GLDG_SelPlrName].ignore and not GLDG_Data.ShowIgnore) then
			GLDG_SelPlrName = nil end end
	-- Prepare for list creation
	GLDG_SortedList = {}
	local total = 0
	GLDG_NumMain = 0
	GLDG_NumAlts = 0
	local s = {}
	if GLDG_SelPlrName then s = GLDG_DataGuild[GLDG_SelPlrName] end
	-- Create the list of players to display
	for player in pairs(GLDG_DataGuild) do
		local p = GLDG_DataGuild[player]
		if not p.own then
			-- Update counters for main and alt
			if p.main then GLDG_NumMain = GLDG_NumMain + 1 end
			if p.alt and (p.alt == GLDG_SelPlrName) then GLDG_NumAlts = GLDG_NumAlts + 1 end
			-- See if we are supposed to add this player in our list
			local show = GLDG_Data.ShowIgnore or not p.ignore
			show = show and ((p.alt == GLDG_SelPlrName) or (p.alt == s.alt) or not p.alt or GLDG_Data.ShowAlt)
			if show then
				-- Look for the position in the list we need
				local loc = 1
				while GLDG_SortedList[loc] and (GLDG_SortString(player) > GLDG_SortString(GLDG_SortedList[loc])) do loc = loc + 1 end
				-- We found our position: move everything beyond it
				for cnt = total, loc, -1 do GLDG_SortedList[cnt + 1] = GLDG_SortedList[cnt] end
				-- Insert our player
				total = total + 1
				GLDG_SortedList[loc] = player end end end
	-- Update the scrollbar
	GLDG_ClickPlayerBar()
end

------------------------------------------------------------
function GLDG_ShowPlayers()
	-- Display the players visible in the frame
	local cnt = 1
	local line = GLDG_GUI.."PlayersLine"
	while GLDG_SortedList[cnt + GLDG_PlayerOffset] and getglobal(line..cnt) do
		local text = getglobal(line..cnt.."Text")
		local text2 = getglobal(line..cnt.."Text2")
		local p = GLDG_DataGuild[GLDG_SortedList[cnt + GLDG_PlayerOffset]]
		if p.alias then text:SetText(GLDG_SortedList[cnt + GLDG_PlayerOffset].."*")
		else text:SetText(GLDG_SortedList[cnt + GLDG_PlayerOffset]) end
		text2:SetText("")
		if p.ignore then
			text:SetTextColor(1, 0.25, 0.25)
			text2:SetText(GLDG_TXT.markIGN)
		elseif p.main then
			text:SetTextColor(0.25, 1, 0.25)
			text2:SetText(GLDG_TXT.markMAIN)
		elseif p.alt then
			text:SetTextColor(0.25, 0.25, 1)
			text2:SetText(GLDG_TXT.markALT)
		elseif p.alias then
			text:SetTextColor(0.68, 0.8, 1)
		else text:SetTextColor(1, 1, 1) end
		text2:SetTextColor(text:GetTextColor())
		getglobal(line..cnt):Enable()
		cnt = cnt +1 end
	-- Disable any rows left unused
	for cnt2 = cnt, GLDG_NumPlrRows do
		getglobal(line..cnt2.."Text"):SetText("")
		getglobal(line..cnt2.."Text2"):SetText("")
		getglobal(line..cnt2):Disable() end
	-- Set/clear highlight
	for cnt = 1, GLDG_NumPlrRows do
		if GLDG_SelPlrName and (GLDG_SortedList[cnt + GLDG_PlayerOffset] == GLDG_SelPlrName) then getglobal(line..cnt):LockHighlight()
		else getglobal(line..cnt):UnlockHighlight() end end
	-- Update buttons
	GLDG_ShowPlayerButtons()
end

------------------------------------------------------------
function GLDG_ClickPlayer(playerName)
	-- Prepare for scrollbar adjustment if needed
	local p = {}
	if GLDG_SelPlrName then p = GLDG_DataGuild[GLDG_SelPlrName] end
	local old = nil
	if (not GLDG_Data.ShowAlt) and (p.main or p.alt) then old = GLDG_GetPlayerOffset(playerName) end
	-- Set new selected player
	if (playerName == GLDG_SelPlrName) then GLDG_SelPlrName = nil
	else GLDG_SelPlrName = playerName end
	-- Refresh list
	GLDG_ListPlayers()
	-- Adjust scrollbar if needed
	GLDG_CorrectPlayerOffset(old, playerName)
end

------------------------------------------------------------
function GLDG_ClickPlayerBar(frame)
	-- Update offset
	if not frame then frame = getglobal(GLDG_GUI.."PlayersPlayerbar") end
	FauxScrollFrame_Update(frame, GLDG_TableSize(GLDG_SortedList), GLDG_NumPlrRows, 15)
	GLDG_PlayerOffset = FauxScrollFrame_GetOffset(frame)
	GLDG_ShowPlayers()
end

------------------------------------------------------------
function GLDG_ShowPlayerButtons()
	-- Set frame
	local frame = GLDG_GUI.."Players"
	-- Hide subframes
	getglobal(frame.."SubAlias"):Hide()
	getglobal(frame.."SubMainAlt"):Hide()
	-- If no player is selected: hide all buttons
	if not GLDG_SelPlrName then
		getglobal(frame.."Ignore"):Hide()
		getglobal(frame.."Alias"):Hide()
		getglobal(frame.."Main"):Hide()
		getglobal(frame.."Alt"):Hide()
		return end
	-- Set selected player
	local p = GLDG_DataGuild[GLDG_SelPlrName]
	-- Ignore button
	local button = getglobal(frame.."Ignore")
	if p.ignore then button:SetText(GLDG_TXT.pbrign) else button:SetText(GLDG_TXT.pbsign) end
	button:Show()
	-- Alias button
	button = getglobal(frame.."Alias")
	button:Show()
	button:Enable()
	-- Main button
	button = getglobal(frame.."Main")
	button:Show()
	if p.alt then button:SetText(GLDG_TXT.pbpmain)
	elseif not p.main then button:SetText(GLDG_TXT.pbsmain)
	elseif (GLDG_NumAlts == 0) then button:SetText(GLDG_TXT.pbrmain)
	else button:Hide() end
	-- Alt button
	button = getglobal(frame.."Alt")
	button:Show()
	button:Enable()
	if p.alt then button:SetText(GLDG_TXT.pbralt)
	elseif p.main then button:Hide()
	else button:SetText(GLDG_TXT.pbsalt) end
end

------------------------------------------------------------
function GLDG_ClickIgnore()
	-- Toggle ignore state for selected player
	local p = GLDG_DataGuild[GLDG_SelPlrName]
	local main = p.alt
	if p.main then main = GLDG_SelPlrName end
	local newval = nil
	if not p.ignore then newval = true end
	-- If main or alt, toggle ignore for all characters of this player
	if main then for q in pairs(GLDG_DataGuild) do if (q == main) or (GLDG_DataGuild[q].alt == main) then GLDG_DataGuild[q].ignore = newval end end
	-- Otherwise, simply toggle ignore for this one character
	else p.ignore = newval end
	-- Show updated list
	GLDG_ListPlayers()
end

------------------------------------------------------------
function GLDG_ClickAlias()
	-- Activate alias subframe
	GLDG_ShowPlayerButtons()
	this:Disable()
	getglobal(this:GetParent():GetName().."SubAlias"):Show()
end

------------------------------------------------------------
function GLDG_ClickMain()
	-- Toggle main status for selected character or promote an alt
	local p = GLDG_DataGuild[GLDG_SelPlrName]
	if p.main then p.main = nil
	elseif p.alt then
		local oldmain = p.alt
		p.alt = nil
		p.main = true
		for q in pairs(GLDG_DataGuild) do
			if (q == oldmain) then
				GLDG_DataGuild[q].main = nil
				GLDG_DataGuild[q].alt = GLDG_SelPlrName
			elseif (GLDG_DataGuild[q].alt == oldmain) then
				GLDG_DataGuild[q].alt = GLDG_SelPlrName end end
	else p.main = true end
	-- Show updated list
	GLDG_ListPlayers()
end

------------------------------------------------------------
function GLDG_ClickAlt()
	-- Prepare for scrollbar adjustment if needed
	local p = {}
	if GLDG_SelPlrName then p = GLDG_DataGuild[GLDG_SelPlrName] end
	local old = nil
	if GLDG_Data.GroupAlt then old = GLDG_GetPlayerOffset(GLDG_SelPlrName) end
	-- Toggle alt status for selected character
	local p = GLDG_DataGuild[GLDG_SelPlrName]
	if p.alt then p.alt = nil
	elseif (GLDG_NumMain == 1) then
		for q in pairs(GLDG_DataGuild) do
			if GLDG_DataGuild[q].main then
				p.alt = q
				p.ignore = GLDG_DataGuild[q].ignore
				break end end	
	else	GLDG_ShowPlayerButtons()
		this:Disable()
		getglobal(this:GetParent():GetName().."SubMainAlt"):Show()
		return end
	-- Show updated list
	GLDG_ListPlayers()
	-- Adjust scrollbar if needed
	GLDG_CorrectPlayerOffset(old, GLDG_SelPlrName)
end

------------------------------------------------------------
function GLDG_ShowPlayerAlias(frame)
	-- Set title
	getglobal(frame.."Header"):SetText(string.format(GLDG_TXT.aliashead, GLDG_SelPlrName))
	-- Set editbox and buttons text
	local p = GLDG_DataGuild[GLDG_SelPlrName]
	if p.alias then
		getglobal(frame.."Set"):SetText(GLDG_TXT.update)
		getglobal(frame.."Del"):SetText(GLDG_TXT.delete)
		getglobal(frame.."Editbox"):SetText(p.alias)
	else	getglobal(frame.."Set"):SetText(GLDG_TXT.set)
		getglobal(frame.."Del"):SetText(GLDG_TXT.cancel)
		getglobal(frame.."Editbox"):SetText("") end
end

------------------------------------------------------------
function GLDG_ClickAliasSet(alias)
	if (alias == "") then return end
	GLDG_DataGuild[GLDG_SelPlrName].alias = alias
	GLDG_ListPlayers()
end

------------------------------------------------------------
function GLDG_ClickAliasRemove()
	GLDG_DataGuild[GLDG_SelPlrName].alias = nil
	GLDG_ListPlayers()
end

------------------------------------------------------------
function GLDG_ShowMainAlt(frame)
	-- Set frame and linename
	if not frame then frame = this end
	local name = frame:GetParent():GetName()
	-- Set title
	getglobal(name.."Header"):SetText(string.format(GLDG_TXT.mainhead, GLDG_SelPlrName))
	-- Create a sorted list of all mains
	local mainlist = {}
	local total = 0
	for p in pairs(GLDG_DataGuild) do
		if GLDG_DataGuild[p].main then
			local loc = 1
			while mainlist[loc] and (mainlist[loc] < p) do loc = loc + 1 end
			for cnt = total, loc, -1 do mainlist[cnt + 1] = mainlist[cnt] end
			total = total + 1
			mainlist[loc] = p end end
	-- Configure scrollbar
	FauxScrollFrame_Update(frame, GLDG_TableSize(mainlist), GLDG_NumSubRows, 15)
	local offset = FauxScrollFrame_GetOffset(frame)
	-- Set all rows
	name = name.."Line"
	for line = 1, GLDG_NumSubRows do	
		if mainlist[line + offset] then
			getglobal(name..line.."Text"):SetText(mainlist[line + offset])
			getglobal(name..line):Enable()
		else	getglobal(name..line.."Text"):SetText("")
			getglobal(name..line):Disable() end end
end

------------------------------------------------------------
function GLDG_ClickMainAlt(player)
	-- Mark position of current selected player
	local old = GLDG_GetPlayerOffset(GLDG_SelPlrName)
	-- Make this the main for the currently selected character
	GLDG_DataGuild[GLDG_SelPlrName].alt = player
	GLDG_DataGuild[GLDG_SelPlrName].ignore = GLDG_DataGuild[player].ignore
	-- Hide the subframe window
	this:GetParent():Hide()		
	-- Refresh the playerlist
	GLDG_ListPlayers()
	-- Adjust scrollbar if needed
	GLDG_CorrectPlayerOffset(old, GLDG_SelPlrName)
end

------------------------------------------------------------
function GLDG_GetPlayerOffset(playerName)
	-- Find current position number on list
	local old = nil
	for i = 1, GLDG_TableSize(GLDG_SortedList) do
		if (GLDG_SortedList[i] == playerName) then
			old = i
			break end end
	return old
end

------------------------------------------------------------
function GLDG_CorrectPlayerOffset(old, playerName)
	-- Abort if no value given
	if not old then return end
	local new = nil
	-- Find new position number on list for this selection
	for i = 1, GLDG_TableSize(GLDG_SortedList) do
		if (GLDG_SortedList[i] == playerName) then
			new = i
			break end end
	-- Abort if no longer on list or position didn't change
	if (not new) or (old == new) then return end
	-- Calculate new offset and set it
	GLDG_PlayerOffset = math.max(0, math.min(GLDG_PlayerOffset + new - old, GLDG_TableSize(GLDG_SortedList) - GLDG_NumPlrRows))
	FauxScrollFrame_SetOffset(getglobal(GLDG_GUI.."PlayersPlayerbar"), GLDG_PlayerOffset)
	GLDG_ListPlayers()
end


-----------------------------------
-- _15_ General Helper Functions --
-----------------------------------

function GLDG_SetTextColor(textName, colorName)
	-- Define color names
	local Color = {}
	Color.default = {}
	Color.default.r = 1
	Color.default.g = 0.5
	Color.default.b = 0
	Color.new = {}
	Color.new.r = 1
	Color.new.g = 0.25
	Color.new.b = 0.25
	Color.lvl = {}
	Color.lvl.r = 0.5
	Color.lvl.g = 0.5
	Color.lvl.b = 1
	Color.rank = {}
	Color.rank.r = 0.8
	Color.rank.g = 0
	Color.rank.b = 0.8
	Color.relog = {}
	Color.relog.r = 0.25
	Color.relog.g = 1
	Color.relog.b = 0.25
	local color = Color[colorName]
	local text = getglobal(textName)
	if text then text:SetTextColor(color.r, color.g, color.b) end
end

------------------------------------------------------------
function GLDG_DebugPrint(msg)
	if not (GLDG_Debug and msg) then return end
	DEFAULT_CHAT_FRAME:AddMessage("GUILDGREET DEBUG: "..msg)
end

------------------------------------------------------------
-- Urbin
function GLDG_Print(msg)
	if not (msg) then return end
	DEFAULT_CHAT_FRAME:AddMessage(msg)
end

------------------------------------------------------------
function GLDG_TableSize(info)
	local result = 0
	for item in pairs(info) do result = result + 1 end
	return result
end
