GuildGreet - A World of Warcraft guild tool
Readme file for v2.0.4
----------------------------------------------------------------


INSTALLATION
------------
Extract the folder in GuildGreet.zip to the directory 
..\World of Warcraft\Interface\AddOns


DESCRIPTION
-----------
This is a guild tool: it's useless if you are not in a guild

As soon as you go online, the addon loads the list of players in your guild and starts to
monitor all changes. As soon as a player comes online or joins the guild, the name is 
added to a small list onscreen. If you leftclick on the name, one of the possible greetings 
(at random) is written in the guild channel and the name is removed from the list. if you
rightclick on the name, it's removed from the list without sending a greeting.
These color codes are used for the list:
- red: players joining the guild both during your session and earlier. These players will also 
       be indicated by the word NEW at the end of the line.
- orange: players logging in for the first time during your current session
- green: players relogging. They logged on before during your current session
- blue: players gaining a higher level both during your session and then earlier. These players
        will also be indicated by the word LEVEL at the end of the line.
- purple: players getting a higher guildrank then previous session and earlier. These players
          will also be indicated by the word RANK at the end of the line.

Notes:
- the list shown on the screen can be dragged anywhere you want by using the red titlebar to drag.
- the onscreen list shows a maximum of 5 players by default, but GuildGreet will store the names of any
  further players needing a greeting.
- mousing over the names in the list will display a tooltip with usefull information regarding the player

For detailed information on changing the configuration (like greeting texts), see the file manual.txt


DEBUGGING
---------
CLI commands: /gg debug [ on | off | toggle ]
If you encounter problems with GuildGreet, you might want to turn on debugging to see what's going wrong.
When debugging is enabled, two effects will happen:
1) the list of players to be greeted is forced visible, even when no players are on the list
2) incoming system messages are followed: debug messages are written to DEFAULT_CHAT_FRAME
If you find some problem, please report the issue to me, the author, and provide these details:
- version of GuildGreet you are using
- language of your WoW client
- debug messages displayed (if any)
- your description of the problem


SUPPORTED
---------
- WoW clients for all languages (GUI is currently available in English and German)
- myAddOns 2.0
        
KNOWN BUGS
----------
- playerlist manipulations near end of list can result in temporary hiding the last
  entries of the list from the GUI


TODO LIST
---------
- Implement MiniMap Icon
- Implement Optional [GuildGreet] After name in Chat when sending messages
- Implement OPtional Auto Send Greet
- Implement Friends Support
- Implement Optional Whisper FUnction
-


VERSION HISTORY
---------------
v0.52c
- Update to Interface: 11100

v0.52b
- Update to Interface: 11000
- Taken over by Mordikaiin

v0.52
- Current I Face Version

v0.51
- initial guild information retrieval is now more reliable

v0.50
- new separate tabs for managing messages and players
- more control over displaying queued greetings list
- more configurable playerlist for configuration
- reworked entire rank detection part as the old one didn't work correctly
- added option to greet alts with the same name as their main
- implemented collections to support using multiple greetings lists: see manual.txt for details
- default relog timeout is now set to 15 min
- faster initial playerlist loading
- fixed minor bug in displaying relogs
- fixed minor bug in displaying playerlist
- updated configuration documentation and moved to manual.txt
- translation for German GUI completed thanks to Urbin
- for NEW installations, the initial greetings will be in German if you have a German WoW client

v0.43
- yet another bug (detecting players going offline)

v0.42
- fixed another stupid bug (typo)

v0.41
- fixed bug with tooltip for main/alt characters when they log on for the first time.

v0.40
- small changes in existing tooltip texts and some additions too
- fixed scrollbar issues introducted by patch 1.9
- fixed another bug in offline counter system
- support for ignore, alias and main/alt

v0.31
- fixed bug in offline counter system
- fixed bug in joining members part
- now compatible with all languages thanks to a brilliant idea of Lunox
- updated for WoW patch 1.9

v0.30
- added support for detecting players joining the guild while you are offline
- added support for player leveling
- added support for player guildrank promotions
- fixed bug with removing messages
- corrected German localisation (thanks to sdw and Lunox)
- corrected French localisation (thanks to Feu)
- made sure offline members are also included in guild info retrieval
- added debug support

v0.21
- corrected French localisation (thanks to mymycracra)

v0.20
- now separate greetings for reloggers and joining players
- configuration GUI: open with /gg or /guildgreet
- fixed bug with tooltip for new players

v0.11beta
- a right click on the name now removes it from the list without sending a greeting
- removed some forgotten debug messages
- importing guild members should be more reliable now
- readme.txt added

v0.10beta
- initial release


CREDITS
-------
This addon was started based on an idea from Skint on Bloodhoof (EU) by Greya (greya@synema.be)
and is now being taken over by Mordikaiin of Dragonblight (US) (Michaelquickel@comcast.net).


German translation is available thanks to Urbin