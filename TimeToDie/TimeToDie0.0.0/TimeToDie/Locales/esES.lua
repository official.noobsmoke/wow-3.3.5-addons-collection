local L = LibStub('AceLocale-3.0'):NewLocale('TimeToDie', 'esES', true)
if not L then return end

-- L["Algorithm"] = ""
L["Center"] = "Centrado"
-- L["Display Frame"] = ""
L["Enemy only"] = "Sólo enemigo"
L["Estimated time until current target will die."] = "Tiempo estimado hasta que el objetivo actual muera."
L["Font"] = "Fuente"
-- L["Initial Midpoints (Original)"] = ""
L["Justify"] = "Justificación"
-- L["Least Squares"] = ""
L["Left"] = "Izquierda"
L["Only track enemy targets."] = "Sólo hacer seguimiento de objetivos enemigos."
L["Outline"] = "Contorno"
L["Right"] = "Derecha"
-- L["Set font outline."] = ""
-- L["Set frame strata."] = ""
-- L["Strata"] = ""
-- L["Thick"] = ""
-- L["Use focus instead of target."] = ""
-- L["Use target of target when your target is friendly."] = ""
-- L["Weighted Least Squares"] = ""

