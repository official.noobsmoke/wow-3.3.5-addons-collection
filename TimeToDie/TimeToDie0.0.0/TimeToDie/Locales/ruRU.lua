local L = LibStub('AceLocale-3.0'):NewLocale('TimeToDie', 'ruRU', true)
if not L then return end

-- L["Algorithm"] = ""
L["Center"] = "Середина"
L["Display Frame"] = "Показывать окно"
L["Enemy only"] = "Òîëüêî âðàãè" -- Needs review
L["Estimated time until current target will die."] = "Расчётное время до смерти текущей цели"
L["Font"] = "Шрифт"
-- L["Initial Midpoints (Original)"] = ""
L["Justify"] = "Выравнивание"
-- L["Least Squares"] = ""
L["Left"] = "Лево"
L["Only track enemy targets."] = "Îòñëåæèâàòü òîëüêî öåëè ïðîòèâíèêà" -- Needs review
L["Outline"] = "Êîíòóð" -- Needs review
L["Right"] = "Право"
L["Set font outline."] = "Óñòàíîâêà êîíòóðà øðèôòà" -- Needs review
L["Set frame strata."] = "Óñòàíîâêà ñòðàòû ôðåéìà" -- Needs review
L["Strata"] = "Ñòðàòà" -- Needs review
L["Thick"] = "Òîëñòûé" -- Needs review
L["Use focus instead of target."] = "Èñïîëüçîâàòü ôîêóñ âìåñòî öåëè" -- Needs review
-- L["Use target of target when your target is friendly."] = ""
-- L["Weighted Least Squares"] = ""

