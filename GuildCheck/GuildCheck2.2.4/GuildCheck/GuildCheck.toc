## Interface: 30100
## X-Curse-Packaged-Version: v2.24
## X-Curse-Project-Name: GuildCheck
## X-Curse-Project-ID: project-3843
## X-Curse-Repository-ID: wow/project-3843/mainline

## Title: GuildCheck
## Version: v2.24
## Notes: Shows changes in your guild since your last time online.
## Notes-deDE: Zeigt Veränderungen in der Gilde seit dem letzten Mal online sein.
## Notes-ruRU: Показать изменения в вашей гильдии с прошлого вашего визита.

## Author: Tunhadil
## X-Credits: Drizzd, Eritnull aka Шептун

## SavedVariables: GCDB

## OptionalDeps: Ace3, LibGuild-1.0, LibStub, LibBabble-Class-3.0
## X-Embeds: Ace3, LibGuild-1.0, LibStub, LibBabble-Class-3.0

## X-Category: Guild

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

Locale-enUS.lua
Locale-deDE.lua
Locale-ruRU.lua
GuildCheck.lua