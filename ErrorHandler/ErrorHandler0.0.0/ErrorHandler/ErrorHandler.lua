UIErrorsFrame:UnregisterEvent("UI_ERROR_MESSAGE")

local function HexToRGBPerc(hex)
	local rhex, ghex, bhex = string.sub(hex, 1, 2), string.sub(hex, 3, 4), string.sub(hex, 5, 6)
	return tonumber(rhex, 16)/255, tonumber(ghex, 16)/255, tonumber(bhex, 16)/255
end

local f = CreateFrame("Frame")

f:RegisterEvent("UI_ERROR_MESSAGE")
f:RegisterEvent("ADDON_LOADED")

f:SetScript("OnEvent", function(_, g, e)
	if (g == "ADDON_LOADED" and e == "ErrorHandler") then
		if (ErrDB == nil) then
			ErrDB = { 
				ErrName = { SPELL_FAILED_AURA_BOUNCED, ERR_ABILITY_COOLDOWN, SPELL_FAILED_NEED_AMMO, SPELL_FAILED_SPELL_IN_PROGRESS, SPELL_FAILED_MOVING, SPELL_FAILED_STUNNED, SPELL_FAILED_BAD_TARGETS, ERR_ITEM_COOLDOWN, ERR_OUT_OF_ENERGY, ERR_OUT_OF_MANA, ERR_OUT_OF_RAGE, ERR_OUT_OF_RUNIC_POWER, SPELL_FAILED_NOTHING_TO_DISPEL, ERR_LFG_DESERTER_PARTY, ERR_OUT_OF_RANGE, ERR_PARTY_LFG_BOOT_LOOT_ROLLS, ERR_PARTY_LFG_BOOT_IN_COMBAT, "Requires <professions> <skill>", ERR_SPELL_COOLDOWN, SPELL_FAILED_NO_COMBO_POINTS, "That player can not be kicked for another <time>", ERR_NO_ATTACK_TARGET, ERR_BADATTACKPOS, ERR_LFG_DESERTER_PLAYER, SPELL_FAILED_NOT_IN_BATTLEGROUND, SPELL_FAILED_NOT_IN_ARENA, SPELL_FAILED_CASTER_AURASTATE, ERR_INVALID_ATTACK_TARGET, ERR_GROUP_JOIN_BATTLEGROUND_DESERTERS, ERR_LFG_CANT_USE_BATTLEGROUND, ERR_LFG_CANT_USE_DUNGEONS, ERR_NOT_ENOUGH_ARENA_POINTS, ERR_NOT_ENOUGH_HONOR_POINTS, ERR_NOT_ENOUGH_MONEY, ERR_GENERIC_NO_TARGET, SPELL_FAILED_NOT_BEHIND, ERR_LFG_NO_ROLES_SELECTED, SPELL_FAILED_TARGETS_DEAD },
				ErrBool = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
				ErrColor = { "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000" },
				ErrRdr = { "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default" }
			}
		elseif (#ErrDB.ErrName < 38) then
			ErrDB = { 
				ErrName = { SPELL_FAILED_AURA_BOUNCED, ERR_ABILITY_COOLDOWN, SPELL_FAILED_NEED_AMMO, SPELL_FAILED_SPELL_IN_PROGRESS, SPELL_FAILED_MOVING, SPELL_FAILED_STUNNED, SPELL_FAILED_BAD_TARGETS, ERR_ITEM_COOLDOWN, ERR_OUT_OF_ENERGY, ERR_OUT_OF_MANA, ERR_OUT_OF_RAGE, ERR_OUT_OF_RUNIC_POWER, SPELL_FAILED_NOTHING_TO_DISPEL, ERR_LFG_DESERTER_PARTY, ERR_OUT_OF_RANGE, ERR_PARTY_LFG_BOOT_LOOT_ROLLS, ERR_PARTY_LFG_BOOT_IN_COMBAT, "Requires <professions> <skill>", ERR_SPELL_COOLDOWN, SPELL_FAILED_NO_COMBO_POINTS, "That player can not be kicked for another <time>", ERR_NO_ATTACK_TARGET, ERR_BADATTACKPOS, ERR_LFG_DESERTER_PLAYER, SPELL_FAILED_NOT_IN_BATTLEGROUND, SPELL_FAILED_NOT_IN_ARENA, SPELL_FAILED_CASTER_AURASTATE, ERR_INVALID_ATTACK_TARGET, ERR_GROUP_JOIN_BATTLEGROUND_DESERTERS, ERR_LFG_CANT_USE_BATTLEGROUND, ERR_LFG_CANT_USE_DUNGEONS, ERR_NOT_ENOUGH_ARENA_POINTS, ERR_NOT_ENOUGH_HONOR_POINTS, ERR_NOT_ENOUGH_MONEY, ERR_GENERIC_NO_TARGET, SPELL_FAILED_NOT_BEHIND, ERR_LFG_NO_ROLES_SELECTED, SPELL_FAILED_TARGETS_DEAD },
				ErrBool = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
				ErrColor = { "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000", "AA0000" },
				ErrRdr = { "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default", "Default" }
			}
		else
			ErrDB = ErrDB
		end

	elseif (g == "UI_ERROR_MESSAGE") then
		if (string.sub(e, 1, 8) == "Requires") then
			if (ErrDB.ErrBool[18]) then
				RedirectOutput(ErrDB.ErrRdr[18], e, HexToRGBPerc(ErrDB.ErrColor[18]))
			end
	
		elseif (string.sub(e, 1, 11) == "That player") then
			if (ErrDB.ErrBool[21]) then
				RedirectOutput(ErrDB.ErrRdr[21], e, HexToRGBPerc(ErrDB.ErrColor[21]))
			end
		end
		
		for i = 1, #ErrDB.ErrName do
		
			if (e == ErrDB.ErrName[i]) then
				if (ErrDB.ErrBool[i]) then
					RedirectOutput(ErrDB.ErrRdr[i], e, HexToRGBPerc(ErrDB.ErrColor[i]))
				end
			end
		end

	end
	
end)