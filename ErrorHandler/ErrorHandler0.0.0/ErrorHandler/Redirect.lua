--Contains functions to handle the different redirects

local function RGBPercToHex(r, g, b)
	r = r <= 1 and r >= 0 and r or 0
	g = g <= 1 and g >= 0 and g or 0
	b = b <= 1 and b >= 0 and b or 0
	return string.format("%02x%02x%02x", r*255, g*255, b*255)
end

function RedirectOutput(channel, text, rR, gG, bB)
	if (channel == "Chatframe") then
		print("|cff"..RGBPercToHex(rR, gG, bB)..text)

	elseif (channel == "Raidwarning") then
		RaidNotice_AddMessage(RaidWarningFrame, text, { r = rR, g = gG, b = bB })
	elseif (channel == "Bossemote") then
		RaidNotice_AddMessage(RaidBossEmoteFrame, text, { r = rR, g = gG, b = bB })
	elseif (channel == "MSBT") then 
		if (IsAddOnLoaded("MikScrollingBattleText") == 1) then
			MikSBT.DisplayMessage(text, MikSBT.DISPLAYTYPE_NOTIFICATION, true, rR * 255, gG * 255, bB * 255)
		else
			print("<Error Handler>|cffFF0000MikScrollingBattleText not enabled!|r")
		end
	elseif (channel == "SCT") then
		if (IsAddOnLoaded("sct") == 1) then
			SCT:DisplayText(text, {r=rR, g=gG, b=bB}, nil, "damage", SCT.FRAME3)
		else
			print("<Error Handler>|cffFF0000ScrollingCombatText not enabled!|r")
		end
	elseif (channel == "Parrot") then
		if (IsAddOnLoaded("parrot") == 1) then
			Parrot:ShowMessage(text, "Notification", true, rR, gG, bB)
		else
			print("<Error Handler>|cffFF0000Parrot not enabled!|r")
		end
	elseif (channel == "BlizzardCT") then
		if (IsAddOnLoaded("Blizzard_CombatText") and SHOW_COMBAT_TEXT) then
			CombatText_AddMessage(text, CombatText_StandardScroll, rR, gG, bB);
		else
			print("<Error Handler>|cffFF0000BlizzardCombatText not enabled!|r")
		end
	else
			UIErrorsFrame:AddMessage(text, rR, gG, bB)
	end
end