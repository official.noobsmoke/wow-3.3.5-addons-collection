local options = CreateFrame("Frame", "ErrorHandlerConfig")
options:Hide()
options.name = "Error Handler"
InterfaceOptions_AddCategory(options)

InterfaceOptionsFrame:SetWidth(900)

local title = options:CreateFontString("ErrorHandlerConfigTitle", "ARTWORK", "GameFontNormalLarge")
title:SetPoint("TOPLEFT", 16, -16)
title:SetText("Error Handler")

local txtEnabled = options:CreateFontString("ErrorHandlerEnabledText", "ARTWORK", "GameFontHighlight")
txtEnabled:SetPoint("TOPLEFT", 300, -38)
txtEnabled:SetText("Enable")

local txtRedirect = options:CreateFontString("ErrorHandlerRedirectText", "ARTWORK", "GameFontHighlight")
txtRedirect:SetPoint("TOPLEFT", 384, -38)
txtRedirect:SetText("Redirect")

local txtColor = options:CreateFontString("ErrorHandlerColorText", "ARTWORK", "GameFontHighlight")
txtColor:SetPoint("TOPLEFT", 510, -38)
txtColor:SetText("Color")

options:RegisterEvent("ADDON_LOADED")

local optionsScrollFrame = optionsScrollFrame or CreateFrame("ScrollFrame", "ErrorHandlerScrollFrame", options)
local optionsFrame = optionsFrame or CreateFrame("Frame", "ErrorHandlerConfigFrame", optionsScrollFrame)
local scrollbar = scrollbar or CreateFrame("Slider", "ErrorHandlerScrollBar", optionsScrollFrame)

optionsScrollFrame:SetWidth(616)
optionsScrollFrame:SetHeight(364)
optionsScrollFrame:SetPoint("TOPLEFT", 16, -58)
optionsScrollFrame:Show();

optionsFrame:SetPoint("TOPLEFT", 0, 0)

optionsScrollFrame:SetScrollChild(optionsFrame)

if not scrollbar.bg then
	scrollbar.bg = scrollbar:CreateTexture(nil, "BACKGROUND")
	scrollbar.bg:SetAllPoints(true)
	scrollbar.bg:SetTexture(0, 0, 0, 0.5)
end

if not scrollbar.thumb then
	scrollbar.thumb = scrollbar:CreateTexture(nil, "OVERLAY")
	scrollbar.thumb:SetTexture("Interface\\Buttons\\UI-ScrollBar-Knob")
	scrollbar.thumb:SetSize(25, 25)
	scrollbar:SetThumbTexture(scrollbar.thumb)
end

optionsFrame:SetSize(616, 1000)

local ScrollMax = optionsFrame:GetHeight() - 200
scrollbar:SetOrientation("VERTICAL");
scrollbar:SetSize(14, 354)
scrollbar:SetPoint("TOPLEFT", optionsScrollFrame, "TOPRIGHT", 0, 0)
scrollbar:SetMinMaxValues(0, ScrollMax)
scrollbar:SetValue(0)
scrollbar:SetScript("OnValueChanged", function(self)
	optionsScrollFrame:SetVerticalScroll(self:GetValue())
end)

optionsScrollFrame:EnableMouseWheel(true)
optionsScrollFrame:SetScript("OnMouseWheel", function(self, delta)
	local current = scrollbar:GetValue()
      
	if IsShiftKeyDown() and (delta > 0) then
		scrollbar:SetValue(0)
	elseif IsShiftKeyDown() and (delta < 0) then
		scrollbar:SetValue(ScrollMax)
	elseif (delta < 0) and (current < ScrollMax) then
		scrollbar:SetValue(current + 20)
	elseif (delta > 0) and (current > 1) then
		scrollbar:SetValue(current - 20)
	end
end)

local backdrop = {
  -- path to the background texture
  bgFile = "",  
  -- path to the border texture
  edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border",
  -- true to repeat the background texture to fill the frame, false to scale it
  tile = true,
  -- size (width or height) of the square repeating background tiles (in pixels)
  tileSize = 16,
  -- thickness of edge segments and square size of edge corners (in pixels)
  edgeSize = 16,
  -- distance from the edges of the frame to those of the background texture (in pixels)
  insets = {
    left = 5,
    right = 5,
    top = 5,
    bottom = 5
  }
}

local RedirectOptions = {
	"Default",
	"Chatframe",
	"Raidwarning",
	"Bossemote",
	"MSBT",
	"SCT",
	"Parrot",
	"BlizzardCT"
}

local function RGBPercToHex(r, g, b)
	r = r <= 1 and r >= 0 and r or 0
	g = g <= 1 and g >= 0 and g or 0
	b = b <= 1 and b >= 0 and b or 0
	return string.format("%02x%02x%02x", r*255, g*255, b*255)
end

local function HexToRGBPerc(hex)
	local rhex, ghex, bhex = string.sub(hex, 1, 2), string.sub(hex, 3, 4), string.sub(hex, 5, 6)
	return tonumber(rhex, 16)/255, tonumber(ghex, 16)/255, tonumber(bhex, 16)/255
end

local function ShowColorPicker(hex, CPa, which, table)
	local rhex, ghex, bhex = string.sub(hex, 1, 2), string.sub(hex, 3, 4), string.sub(hex, 5, 6)

	local name = string.gsub(which, "Button", "")

	ColorPickerFrame:SetColorRGB(tonumber(rhex, 16)/255,tonumber(ghex, 16)/255,tonumber(bhex, 16)/255);
	ColorPickerFrame.hasOpacity, ColorPickerFrame.opacity = (CPa ~= nil), CPa;
	ColorPickerFrame.previousValues = {tonumber(rhex, 16)/255,tonumber(ghex, 16)/255,tonumber(bhex, 16)/255,CPa};
	ColorPickerFrame.func =
		function()
			
		end
	ColorPickerFrame.opacityFunc =
		function()
			_G[name]:SetTexture(ColorPickerFrame:GetColorRGB())
			ErrDB.ErrColor[table] = RGBPercToHex(ColorPickerFrame:GetColorRGB())
		end
 	ColorPickerFrame:Hide(); -- Need to run the OnShow handler.
	ColorPickerFrame:Show();
end

local NumCategories = 0
local NumOptions = 0

local function CreateCategoryFrame(title, height, distanceTop)
	NumCategories = NumCategories + 1

	CategoryFrame = CreateFrame("Frame", "CategoryFrame"..NumCategories, optionsFrame)
	CategoryFrame:SetWidth(602)
	CategoryFrame:SetHeight(height)
	CategoryFrame:SetPoint("TOPLEFT", 0, distanceTop - 16)
	CategoryFrame:SetBackdrop(backdrop)
	CategoryFrame:SetBackdropBorderColor(.6, .6, .6, 1);

	local CategoryTitle = optionsFrame:CreateFontString("CategoryTitle"..NumCategories, "ARTWORK", "GameFontNormal")
	CategoryTitle:SetPoint("TOPLEFT", 16, distanceTop)
	CategoryTitle:SetText(title)
end

local function CreateOption(CategoryFrame, i, distanceTop)
	NumOptions = NumOptions + 1
	
	optionTitle = CategoryFrame:CreateFontString("optionTitle"..NumOptions, "ARTWORK", "GameFontHighlight")
	optionTitle:SetPoint("TOPLEFT", CategoryFrame, 16, distanceTop)
	if (string.len(ErrDB.ErrName[i]) > 35) then
		optionTitle:SetText(string.sub(ErrDB.ErrName[i], 1, 35).."...")
	else
		optionTitle:SetText(ErrDB.ErrName[i])
	end

	optionCheckboxEnable = CreateFrame("CheckButton", "optionCheckboxEnable"..NumOptions, CategoryFrame)
	optionCheckboxEnable:SetWidth(26)
	optionCheckboxEnable:SetHeight(26)
	optionCheckboxEnable:SetPoint("TOPLEFT", 292, distanceTop + 2)
	optionCheckboxEnable:SetScript("OnShow", function(frame)
		if (ErrDB.ErrBool[i]) then
			frame:SetChecked(true)
		else
			frame:SetChecked(false)
		end
	end)
	optionCheckboxEnable:SetScript("OnClick", function(frame)
		local tick = frame:GetChecked()
		if tick then
			PlaySound("igMainMenuOptionCheckBoxOn")
			ErrDB.ErrBool[i] = true
		else
			PlaySound("igMainMenuOptionCheckBoxOff")
			ErrDB.ErrBool[i] = false
		end
	end)

	optionCheckboxEnable:SetNormalTexture("Interface\\Buttons\\UI-CheckBox-Up")
	optionCheckboxEnable:SetPushedTexture("Interface\\Buttons\\UI-CheckBox-Down")
	optionCheckboxEnable:SetHighlightTexture("Interface\\Buttons\\UI-CheckBox-Highlight")
	optionCheckboxEnable:SetCheckedTexture("Interface\\Buttons\\UI-CheckBox-Check")

	optionDropdownRedirect = CreateFrame("Frame", "optionDropdownRedirect"..NumOptions, CategoryFrame, "UIDropDownMenuTemplate")
	optionDropdownRedirect:ClearAllPoints()
	optionDropdownRedirect:SetPoint("TOPLEFT", 320, distanceTop + 4)

	local function OnClick(self, parentDropdownFrame)   
		UIDropDownMenu_SetSelectedID(parentDropdownFrame, self:GetID())
		ErrDB.ErrRdr[i] = self:GetText()  
	end

	local function initialize(self, level)
		local info = UIDropDownMenu_CreateInfo()

		for k,v in pairs(RedirectOptions) do
			info = UIDropDownMenu_CreateInfo()
			info.text = v
			info.value = v
			info.arg1 = self
			info.func = OnClick
			UIDropDownMenu_AddButton(info, level)
		end
	end

	UIDropDownMenu_Initialize(optionDropdownRedirect, initialize)
	UIDropDownMenu_SetSelectedName(optionDropdownRedirect, ErrDB.ErrRdr[i])
	
	optionColorSwatchButton = CreateFrame("Button", "optionColorSwatchButton"..NumOptions, CategoryFrame)
	optionColorSwatchButton:SetWidth(22)
	optionColorSwatchButton:SetHeight(22)
	optionColorSwatchButton:SetPoint("TOPLEFT", 500, distanceTop + 2)
	optionColorSwatchButton:SetNormalTexture("Interface\\ChatFrame\\ChatFrameColorSwatch")

	optionColorSwatchTexture = optionColorSwatchButton:CreateTexture("optionColorSwatch"..NumOptions, "OVERLAY")
	optionColorSwatchTexture:SetWidth(12)
	optionColorSwatchTexture:SetHeight(12)
	optionColorSwatchTexture:SetPoint("CENTER", optionColorSwatchButton, 0, 0)
	optionColorSwatchTexture:SetTexture(HexToRGBPerc(ErrDB.ErrColor[i]))

	optionColorSwatchButton:SetScript("OnClick", function(self)
		ShowColorPicker(ErrDB.ErrColor[i], nil, self:GetName(), i)
	end)
		
end


options:SetScript("OnEvent", function(_,event, ...)
	if (event == "ADDON_LOADED" and arg1 == "ErrorHandler") then
		CreateCategoryFrame("Errors", 1100, 0)
		topSpacing = -16
		for i = 1, #ErrDB.ErrName do
			CreateOption(CategoryFrame1, i, topSpacing)
			topSpacing = topSpacing -28
		end
	end
end)
