﻿## Interface: 30300
## Title: Tidy Plates: |cff89F559Threat Plates|r |cffff00ffBeta|r
## Notes: Revolutionary |cff89F559threat reactive nameplates|r by |cff3079ffSyronius|r.
## Author: Syronius of Black Dragonflight
## RequiredDeps: TidyPlates
## SavedVariables: ThreatPlates3BetaDB
## Version: 3.2
## OptionalDeps: Ace3, LibSharedMedia-3.0

Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
embeds.xml

TidyPlatesThreat.lua

Widgets\Widgets.lua
Widgets\EliteArtWidget.lua
Widgets\EPEEN.lua
Widgets\ClassIconWidget.lua
Widgets\ComboPointWidget.lua
Widgets\TargetArtWidget.lua
Widgets\ThreatWidget.lua
Widgets\TotemIconWidget.lua
Widgets\UniqueIconWidget.lua

options.lua

Styles\empty.lua
Styles\normal.lua
Styles\etotem.lua
Styles\totem.lua
Styles\unique.lua
Styles\tank.lua
Styles\dps.lua

