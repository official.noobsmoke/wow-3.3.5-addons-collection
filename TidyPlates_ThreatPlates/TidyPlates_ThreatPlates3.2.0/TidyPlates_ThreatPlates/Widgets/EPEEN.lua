------------------------------
-- EPEEN Widget
------------------------------
do
	local artpath = "Interface\\Addons\\TidyPlates_ThreatPlates\\Widgets\\EPEEN\\"
	local function UpdateEpeenWidget(frame, unit)
	if GS_Data then
		if GS_Data[GetRealmName()]["Players"][unit.name] and GS_Data[GetRealmName()]["Players"][UnitName("player")]["GearScore"] and TidyPlatesThreat.db.profile.epeenWidget.ON and not InCombatLockdown() then
			local size = GS_Data[GetRealmName()]["Players"][unit.name]["GearScore"]
			local mine = GS_Data[GetRealmName()]["Players"][UnitName("player")]["GearScore"]
			local height = 0
			local extra = 0
			local topPoint
			local topTexture
			local midTexture
			if mine >= size then
				if size == 0 then
					height = 1
				elseif size >= 500 and size < 5000 then
					height = (size * 0.002)
				elseif size >= 5000 and size < 5500 then
					height = (((size - 5000 )* 0.13) + 10)
				elseif size >= 5500 and size < 6300 then
					height = (((size - 5500 )* 0.18) + 75)
				elseif size >=6300 then
					height = (((size - 6300)* 0.1) + 220)
				end
				extra = 0
				topPoint = frame.EPEEN
				topTexture = "top"
				midTexture = nil
			else
				if mine < 500 then
					height = 1
				elseif mine >= 500 and mine < 5000 then
					height = (mine * 0.002)
				elseif mine >= 5000 and mine < 5500 then
					height = (((mine - 5000 )* 0.13) + 10)
				elseif mine >= 5500 and mine < 6300 then
					height = (((mine - 5500 )* 0.18) + 75)
				elseif mine >=6300 then
					height = (((mine - 6300)* 0.1) + 220)
				end
				if size < 5000 then
					extra = ((size * 0.002) - height)
				elseif size >= 5000 and size < 5500 then
					extra = ((((size - 5000 )* 0.13) + 10) - height)
				elseif size >= 5500 and size < 6300 then
					extra = ((((size - 5500 )* 0.18) + 75) - height)
				elseif size >=6300 then
					extra = ((((size - 6300)* 0.1) + 220) - height)
				end
				topPoint = frame.EXTRA
				topTexture = "greentop"
				midTexture = artpath.."greenmiddle"
			end
			-- Set Size
			frame.EPEEN:ClearAllPoints()
			frame.EPEEN:SetPoint("BOTTOM", frame.BOTTOM, "TOP")
			frame.EPEEN:SetHeight(height)
			frame.EXTRA:ClearAllPoints()
			frame.EXTRA:SetPoint("BOTTOM", frame.EPEEN, "TOP")
			frame.EXTRA:SetHeight(extra)
			frame.EXTRA:SetTexture(midTexture)
			frame.TOP:ClearAllPoints()
			frame.TOP:SetPoint("BOTTOM", topPoint, "TOP")
			frame.ICON:ClearAllPoints()
			frame.ICON:SetPoint("TOP", frame.BOTTOM, "BOTTOM", -1, 10)			
			frame.BOTTOM:SetTexture(artpath.."bottom")
			frame.TOP:SetTexture(artpath..topTexture)
			if size > mine then
				frame.MyScore:SetText(mine)
				frame.GearScore:SetText(size.." (+"..(size - mine)..")")
				frame.GearScore:SetPoint("BOTTOMLEFT", topPoint, "BOTTOMRIGHT", -1, 1)
			else
				frame.MyScore:SetText("")
				frame.GearScore:SetText(size)
				frame.GearScore:SetPoint("BOTTOMLEFT", topPoint, "BOTTOMRIGHT", -1, -6)
			end
			frame.MyScore:SetTextColor(GearScore_GetQuality(mine))
			frame.MyScore:SetPoint("BOTTOMLEFT", topPoint, "BOTTOMRIGHT", -1, -9)
			frame.GearScore:SetTextColor(GearScore_GetQuality(size))
			
			-- Set Fading
			frame:Show()
		else 
			frame:Hide() 
		end
	end
	end

	local function CreateEpeenWidget(parent, showname, hangtime, art)
		local frame = CreateFrame("Frame", nil, parent)
		frame:SetWidth(30)
		frame:SetHeight(300)
		-- Set Bottom Texture
		frame.BOTTOM = frame:CreateTexture(nil, "OVERLAY")
		frame.BOTTOM:SetPoint("CENTER")
		frame.BOTTOM:SetWidth(16)
		frame.BOTTOM:SetHeight(20)
		-- Set Top Texture
		frame.TOP = frame:CreateTexture(nil, "OVERLAY")
		frame.TOP:SetWidth(16)
		frame.TOP:SetHeight(20)
		-- Set EPEEN
		frame.EPEEN = frame:CreateTexture(nil, "OVERLAY")
		frame.EPEEN:SetTexture(artpath.."middle")
		frame.EPEEN:SetWidth(16)
		frame.EPEEN:SetHeight(100)
		-- Set EXTRA
		frame.EXTRA = frame:CreateTexture(nil, "OVERLAY")
		frame.EXTRA:SetTexture(artpath.."greenmiddle")
		frame.EXTRA:SetWidth(16)
		frame.EXTRA:SetHeight(0)
		-- Set Epeen Icon
		frame.ICON = frame:CreateTexture(nil, "OVERLAY")
		frame.ICON:SetTexture(artpath.."icon")
		frame.ICON:SetWidth(24)
		frame.ICON:SetHeight(24)
		-- My GearScore Text
		frame.MyScore = frame:CreateFontString(nil, "OVERLAY")
		frame.MyScore:SetFont("Interface\\AddOns\\TidyPlates_ThreatPlates\\Fonts\\Accidental Presidency.ttf", 9, "OUTLINE")
		frame.MyScore:SetJustifyH("LEFT")
		frame.MyScore:SetPoint("BOTTOMLEFT",frame.BOTTOM,"TOPRIGHT", -4, -6)
		frame.MyScore:SetShadowOffset(1, -1)
		frame.MyScore:SetShadowColor(0,0,0,1)
		frame.MyScore:SetWidth(30)
		frame.MyScore:SetHeight(10)
		-- GearScore Text
		frame.GearScore = frame:CreateFontString(nil, "OVERLAY")
		frame.GearScore:SetFont("Interface\\AddOns\\TidyPlates_ThreatPlates\\Fonts\\Accidental Presidency.ttf", 9, "OUTLINE")
		frame.GearScore:SetJustifyH("LEFT")
		frame.GearScore:SetPoint("BOTTOMLEFT",frame.BOTTOM,"TOPRIGHT", -4, -6)
		frame.GearScore:SetShadowOffset(1, -1)
		frame.GearScore:SetShadowColor(0,0,0,1)
		frame.GearScore:SetWidth(60)
		frame.GearScore:SetHeight(10)
		
		-- Mechanics/Setup
		frame:Hide()
		frame.Update = UpdateEpeenWidget
		return frame
	end
	
	ThreatPlatesWidgets.CreateEpeenWidget = CreateEpeenWidget
end