-- Copyright (c) 2008 kydori <kydori@gmail.com>
local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local L = LibStub("AceLocale-3.0"):NewLocale(Aesa.appName, "enUS", true, debug)

if not L then return end
L["Aesa"] = true
L["Alpha"] = true
L["Alpha 1"] = true
L["Alpha 2"] = true
L["Appearance"] = true
L["Bar Color"] = true
L["Bar Height"] = true
L["Bar Texture"] = true
L["Bar Width"] = true
L["Cooldowns"] = true
L["Display a group of tabs stick on timers frame allow you to review cooldowns of units you've recently targeted."] = true
L["Drag to move"] = true
L["Enabled"] = true
L["Focus"] = true
L["Font Size"] = true
L["Grow Up"] = true
L["Highlight Color"] = true
L["Highlight New"] = true
L["Hostile Units Only"] = true
L["Icon Position"] = true
L["Left"] = true
L["Lock (Uncheck to move)"] = true
L["Maximum cooldown duration to show (in seconds)."] = true
L["Maximum number of tabs to be shown."] = true
L["Maximum number of timer bars to be shown on screen."] = true
L["Profiles"] = true
L["Right"] = true
L["Scale"] = true
L["Set the spacing between timer bars."] = true
L["Shown Timers"] = true
L["Spacing"] = true
L["Tab Height"] = true
L["Tab Texture"] = true
L["Tabbed"] = true
L["Tabs"] = true
L["Target"] = true
L["The last cooldown ability triggered by your target will be highlighted."] = true
L["Time Limit"] = true
L["Track enemy cooldowns only."] = true
L["Tracking"] = true
L["Type %s for options."] = true
L["Uncheck [LOCK] checkbox in setup menu to move bars."] = true
L["Unit Tabs"] = true
