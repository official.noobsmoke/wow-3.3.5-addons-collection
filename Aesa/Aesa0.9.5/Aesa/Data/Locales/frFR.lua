-- Copyright (c) 2008 kydori <kydori@gmail.com>
local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local L = LibStub("AceLocale-3.0"):NewLocale(Aesa.appName, "frFR", false)

if not L then return end
