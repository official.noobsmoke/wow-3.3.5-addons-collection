-- Copyright (c) 2008 kydori <kydori@gmail.com>
local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local L = LibStub("AceLocale-3.0"):NewLocale(Aesa.appName, "koKR", false)

if not L then return end
L["Aesa"] = true
L["Alpha"] = "투명도"
L["Alpha 1"] = "투명도 1"
L["Alpha 2"] = "투명도 2"
L["Appearance"] = "외형"
L["Bar Color"] = "바 색상"
L["Bar Height"] = "바 높이"
L["Bar Texture"] = "바 무늬"
L["Bar Width"] = "바 너비"
L["Cooldowns"] = "재사용 대기시간"
L["Display a group of tabs stick on timers frame allow you to review cooldowns of units you've recently targeted."] = "타이머 창 위의 탭에 당신이 최근 대상으로 선택한 유닛의 재사용 대기시간을 보여줍니다."
L["Drag to move"] = "드래그 이동"
L["Enabled"] = "사용"
L["Focus"] = "주시"
L["Font Size"] = "글꼴 크기"
L["Grow Up"] = "위로 확장"
L["Highlight Color"] = "강조 색상"
L["Highlight New"] = "새로운 강조"
L["Hostile Units Only"] = "적대적 유닛만"
L["Icon Position"] = "아이콘 위치"
L["Left"] = "왼쪽"
L["Lock (Uncheck to move)"] = "잠금 (이동하려면 해제)"
L["Maximum cooldown duration to show (in seconds)."] = "보여줄 최대 재사용 지속시간(초)를 설정합니다."
L["Maximum number of tabs to be shown."] = "보여줄 탭의 최대 갯수를 설정합니다."
L["Maximum number of timer bars to be shown on screen."] = "화면에 보여줄 타이머 바의 최대 갯수를 설정합니다."
L["Profiles"] = "프로필"
L["Right"] = "오른쪽"
L["Scale"] = "크기"
L["Set the spacing between timer bars."] = "타이머 바 사이의 간격을 설정합니다."
L["Shown Timers"] = "타이머 표시"
L["Spacing"] = "간격"
L["Tab Height"] = "탭 높이"
L["Tab Texture"] = "탭 무늬"
L["Tabbed"] = "라벨"
L["Tabs"] = "탭"
L["Target"] = "대상"
L["The last cooldown ability triggered by your target will be highlighted."] = "당신의 대상이 시전한 마지막 재사용 대기시간 주문을 강조합니다."
L["Time Limit"] = "시간 제한"
L["Track enemy cooldowns only."] = "적대적 대상의 재사용 대기시간만 추적합니다."
L["Tracking"] = "추적"
L["Type %s for options."] = "옵션을 위해 %s을 입력합니다."
L["Uncheck [LOCK] checkbox in setup menu to move bars."] = "바를 움직이기 위해 설정 메뉴의 잠금 체크를 해제합니다."
L["Unit Tabs"] = "유닛 탭"
