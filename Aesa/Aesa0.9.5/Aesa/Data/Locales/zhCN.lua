-- Copyright (c) 2008 kydori <kydori@gmail.com>
local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local L = LibStub("AceLocale-3.0"):NewLocale(Aesa.appName, "zhCN", false)

if not L then return end
L["Aesa"] = true
L["Alpha"] = "透明度"
L["Alpha 1"] = "透明度1"
L["Alpha 2"] = "透明度2"
L["Appearance"] = "外观"
L["Bar Color"] = "计时条颜色"
L["Bar Height"] = "计时条高度"
L["Bar Texture"] = "计时条材质"
L["Bar Width"] = "计时条宽度"
L["Cooldowns"] = "冷却"
L["Display a group of tabs stick on timers frame allow you to review cooldowns of units you've recently targeted."] = "显示一组记录你最近数个目标的标签使之你可以在这些目标间切换显示其冷却。"
L["Drag to move"] = "拖曳移动"
L["Enabled"] = "启用"
L["Focus"] = "焦点"
L["Font Size"] = "字体大小"
L["Grow Up"] = "向上增长"
L["Highlight Color"] = "高亮颜色"
L["Highlight New"] = "高亮新冷却"
L["Hostile Units Only"] = "仅用于敌对单位"
L["Icon Position"] = "图标位置"
L["Left"] = "左侧"
L["Lock (Uncheck to move)"] = "锁定（禁止移动）"
L["Maximum cooldown duration to show (in seconds)."] = "显示最大多少冷却时间持续（秒）"
L["Maximum number of tabs to be shown."] = "显示标签最大显示个数。"
L["Maximum number of timer bars to be shown on screen."] = "在屏幕上显示最多多少个计时条。"
L["Profiles"] = "配置文件"
L["Right"] = "右侧"
L["Scale"] = "缩放"
L["Set the spacing between timer bars."] = "设置相邻计时条间的间隔。"
L["Shown Timers"] = "显示计时器"
L["Spacing"] = "间隔"
L["Tab Height"] = "标签高度"
L["Tab Texture"] = "标签材质"
L["Tabbed"] = "已启用标签"
L["Tabs"] = "标签"
L["Target"] = "目标"
L["The last cooldown ability triggered by your target will be highlighted."] = "目标最后使用的一个冷却技能将高亮显示。"
L["Time Limit"] = "时间限制"
L["Track enemy cooldowns only."] = "仅追踪敌对玩家冷却。"
L["Tracking"] = "追踪"
L["Type %s for options."] = "输入 %s 打开选项。"
L["Uncheck [LOCK] checkbox in setup menu to move bars."] = "禁用设置中[锁定]复选框可移动计时条。"
L["Unit Tabs"] = "单位标签"
