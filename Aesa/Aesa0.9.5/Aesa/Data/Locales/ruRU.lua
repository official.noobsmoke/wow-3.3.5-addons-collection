-- Copyright (c) 2008 kydori <kydori@gmail.com>
local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local L = LibStub("AceLocale-3.0"):NewLocale(Aesa.appName, "ruRU", false)

if not L then return end
L["Aesa"] = true
L["Alpha"] = "Альфа"
L["Alpha 1"] = "Альфа 1"
L["Alpha 2"] = "Альфа 2"
L["Appearance"] = "Внешний вид"
L["Bar Color"] = "Цвет колонки"
L["Bar Height"] = "Высота колонки"
L["Bar Texture"] = "Шрифт"
L["Bar Width"] = "Ширина"
L["Cooldowns"] = "Кулдауны"
L["Drag to move"] = "Переместить"
L["Enabled"] = "Включено"
L["Focus"] = "Фокус"
L["Font Size"] = "Размер шрифта"
L["Highlight Color"] = "Цвет выделения"
L["Hostile Units Only"] = "Только враждебные цели"
L["Icon Position"] = "Позиция иконки"
L["Left"] = "Влево"
L["Lock (Uncheck to move)"] = "Замок (Снимите, чтобы передвинуть)"
L["Maximum cooldown duration to show (in seconds)."] = "Максимальная продолжительность кулдауна для показа (в секундах)."
L["Maximum number of tabs to be shown."] = "Максимальное число вкладок для показа."
L["Maximum number of timer bars to be shown on screen."] = "Максимальное число баров таймера, которые будут показаны на экране."
L["Profiles"] = "Профили"
L["Right"] = "Вправо"
L["Scale"] = "Шкала"
L["Set the spacing between timer bars."] = "Установить расстояние между таймером баров."
L["Shown Timers"] = "Показывать таймеры"
L["Spacing"] = "Расстояние"
L["Tab Height"] = "Высота вкладки"
L["Tab Texture"] = "Шрифт вкладки"
L["Target"] = "Цель"
L["The last cooldown ability triggered by your target will be highlighted."] = [=[Кулдаун последней способности вашей цели будет выдвинута на первый план.
 ]=]
L["Time Limit"] = "Ограничение времени"
L["Track enemy cooldowns only."] = "Отслеживание только вражеских кулдаунов"
L["Type %s for options."] = "Нажмите %s для опций."
L["Uncheck [LOCK] checkbox in setup menu to move bars."] = "Снимите флажок [Замок] в меню настройки для перемещения баров."
