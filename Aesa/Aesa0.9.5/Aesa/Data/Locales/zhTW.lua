-- Copyright (c) 2008 kydori <kydori@gmail.com>
local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local L = LibStub("AceLocale-3.0"):NewLocale(Aesa.appName, "zhTW", false)

if not L then return end
L["Aesa"] = true
L["Alpha"] = "透明度"
L["Alpha 1"] = "透明度1"
L["Alpha 2"] = "透明度2"
L["Appearance"] = "外觀"
L["Bar Color"] = "計時條顏色"
L["Bar Height"] = "計時條高度"
L["Bar Texture"] = "計時條材質"
L["Bar Width"] = "計時條寬度"
L["Cooldowns"] = "冷卻"
L["Display a group of tabs stick on timers frame allow you to review cooldowns of units you've recently targeted."] = "顯示一組記錄你最近數個目標的標簽使之你可以在這些目標間切換顯示其冷卻。"
L["Drag to move"] = "拖拽移動"
L["Enabled"] = "啟用"
L["Focus"] = "焦點"
L["Font Size"] = "字型大小"
L["Grow Up"] = "向上增長"
L["Highlight Color"] = "高亮顏色"
L["Highlight New"] = "高亮新冷卻"
L["Hostile Units Only"] = "只用於敵對單位"
L["Icon Position"] = "圖示位置"
L["Left"] = "左側"
L["Lock (Uncheck to move)"] = "鎖定（取消選定后可移動）"
L["Maximum cooldown duration to show (in seconds)."] = "顯示最大多少冷卻時間持續（秒）"
L["Maximum number of tabs to be shown."] = "顯示標簽最大顯示個數。"
L["Maximum number of timer bars to be shown on screen."] = "在屏幕上顯示最多多少個計時條。"
L["Profiles"] = "配置檔"
L["Right"] = "右側"
L["Scale"] = "縮放"
L["Set the spacing between timer bars."] = "設定相鄰計時條間的間隔。"
L["Shown Timers"] = "顯示計時器"
L["Spacing"] = "間隔"
L["Tab Height"] = "標簽高度"
L["Tab Texture"] = "標簽材質"
L["Tabbed"] = "已啟用標簽"
L["Tabs"] = "標簽"
L["Target"] = "目標"
L["The last cooldown ability triggered by your target will be highlighted."] = "目標最後使用的一個冷卻技能將高亮顯示。"
L["Time Limit"] = "時間限制"
L["Track enemy cooldowns only."] = "僅追蹤敵對玩家冷卻。"
L["Tracking"] = "追蹤"
L["Type %s for options."] = "輸入 %s 打開選項。"
L["Uncheck [LOCK] checkbox in setup menu to move bars."] = "禁用設定中[鎖定]複選框可移動計時條。"
L["Unit Tabs"] = "單位標簽"
