## Interface: 30300
## Author: Kydori,Ananhaid
## Name: Aesa
## Title: Aesa |cff7fff7f0.9.5|r
## Notes: Enemy cooldown tracker.
## Notes-zhCN: 敌对冷却计时器。
## Notes-zhTW: 敵對冷卻計時器。
## DefaultState: Enabled
## OptionalDeps: Ace3, LibSharedMedia-3.0
## SavedVariables: AesaDB
## Version: 0.9.5
## X-Embeds: Ace3, LibSharedMedia-3.0
## X-Category: Combat
## X-eMail: kydori@gmail.com
## X-Curse-Packaged-Version: 0.9.5 Beta1
## X-Curse-Project-Name: Aesa (Enemy Cooldown Tracker)
## X-Curse-Project-ID: project-8061
## X-Curse-Repository-ID: wow/project-8061/mainline

embeds.xml
Global.lua
Data\Locales\enUS.lua
Data\Locales\zhCN.lua
Data\Locales\zhTW.lua
Data\Locales\koKR.lua
Data\Locales\ruRU.lua
Aesa.lua
Options.lua
Data\Spells.lua
