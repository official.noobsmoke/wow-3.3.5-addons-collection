-- Copyright (c) 2008 kydori <kydori@gmail.com>

local L = LibStub("AceLocale-3.0"):GetLocale("Aesa")
local adbo = LibStub("AceDBOptions-3.0")

function Aesa:GetOptions()
	local db = self.db.profile
	local options = {
		type = "group", name = self.appName, childGroups = "tree",
		args = {
			enabled = {
				type = "toggle", name = L["Enabled"], order = 1,
				get = function() return Aesa:IsEnabled() end,
				set = function(_, v)
					db.enabled = v
					if v then Aesa:Enable() else Aesa:Disable() end
				end
			},
			lock = {
				type = "toggle", name = L["Lock (Uncheck to move)"], order = 2,
				get = function() return db.locked end,
				set = function(_, v)
					db.locked = v
					self:C_ApplySettings()
				end
			},
			cd = {
				type = "group", name = L["Cooldowns"], order = 3,
				args = {
					main = {
						type = "group", name = L["Target"], childGroups = "tab", order = 1,
						args = {}
					},
					aux = {
						type = "group", name = L["Focus"], childGroups = "tab", order = 2,
						args = {}
					}
				}
			},
			profiles = adbo:GetOptionsTable(self.db)
		}
	}
	self:AddTrackerOptions(options.args.cd.args.main.args, ".cd.target", "target", L["Target"])
	self:AddTrackerOptions(options.args.cd.args.aux.args, ".cd.focus", "focus", L["Focus"])
	return options
end

function Aesa:AddTrackerOptions(args, path, dbk, header)
	if not args then return end
	local db = self.db.profile.cd[dbk]
	local n = self:StrWordCount(path, ".")
	local to = {
		header = {type = "header", order = 1, name = header},
		enabled = {
			type = "toggle", name = L["Enabled"], order = 2,
			get = function() return db.enabled end,
			set = function(_, v)
				db.enabled = v
				self:C_ApplySettings()
			end
		},
		appearance = {
			type = "group", name = L["Appearance"], order = 3,
			get = function(info)
				if info.type == "color" and self:IsTable(db[info[n + 2]]) then
					do return unpack(db[info[n + 2]]) end
				else
					return db[info[n + 2]]
				end
			end,
			set = function(info, ...)
				if info.type == "color" then
					db[info[n + 2]] = {...}
				else
					db[info[n + 2]] = ...
				end
				self:C_ApplySettings()
			end,
			args = {
				width = {
					type = "range", name = L["Bar Width"], order = 1,
					min = 80, max = 600, step = 1
				},
				height = {
					type = "range", name = L["Bar Height"], order = 2,
					min = 15, max = 60, step = 1
				},
				scale = {
					type = "range", name = L["Scale"], order = 3,
					desc = L["Set the spacing between timer bars."],
					min = 0.1, max = 2, step = 0.01
				},
				spacing = {
					type = "range", name = L["Spacing"], order = 4,
					min = 0, max = 20, step = 1
				},
				barColor = {type = "color", name = L["Bar Color"], order = 5},
				highlightColor = {type = "color", name = L["Highlight Color"], order = 6},
				alpha = {
					type = "range", name = L["Alpha"], order = 7,
					min = 0, max = 1, step = 0.1
				},
				fontSize = {
					type = "range", name = L["Font Size"], order = 8,
					min = 5, max = 25, step = 1
				},
				iconSide = {
					type = "select", name = L["Icon Position"], order = 9,
					values = {LEFT = L["Left"], RIGHT = L["Right"]}
				},
				texture = {
					type = "select", name = L["Bar Texture"], order = 10,
					values = function() return self:SMList() end
				},
				growUp = {type = "toggle", name = L["Grow Up"], order = 11}
			}
		},
		tracking = {
			type = "group", name = L["Tracking"], order = 4,
			get = function(info) return db[info[n + 2]] end,
			set = function(info, value)
				db[info[n + 2]] = value
				self:C_ApplySettings()
				end,
			args = {
				rows = {
					type = "range", name = L["Shown Timers"], order = 1,
					desc = L["Maximum number of timer bars to be shown on screen."],
					min = 3, max = 16, step = 1
				},
				timeLimit = {
					type = "range", name = L["Time Limit"], order = 2,
					desc = L["Maximum cooldown duration to show (in seconds)."],
					min = 3, max = 3600, step = 1
				},
				highlightNew = {
					type = "toggle", name = L["Highlight New"], order = 3,
					desc = L["The last cooldown ability triggered by your target will be highlighted."],
				},
				hostileOnly = {
					type = "toggle", name = L["Hostile Units Only"], order = 4,
					desc = L["Track enemy cooldowns only."],
				}
			}
		},
		unitTabs = {
			type = "group", name = L["Unit Tabs"], order = 5,
			get = function(info) return db[info[n + 2]] end,
			set = function(info, value)
				db[info[n + 2]] = value
				self:C_ApplySettings()
			end,
			args = {
				tabbed = {
					type = "toggle", name = L["Tabbed"], order = 1,
					desc = L["Display a group of tabs stick on timers frame allow you to review cooldowns of units you've recently targeted."]
				},
				tabs = {
					type = "range", name = L["Tabs"], order = 2,
					desc = L["Maximum number of tabs to be shown."],
					min = 3, max = 6, step = 1
				},
				tabHeight = {
					type = "range", name = L["Tab Height"], order = 3,
					min = 15, max = 40, step = 1
				},
				tabSpacing = {
					type = "range", name = L["Spacing"], order = 4,
					min = 0, max = 20, step = 1
				},
				tabAlpha1 = {
					type = "range", name = L["Alpha 1"], order = 5,
					min = 0, max = 1, step = 0.1
				},
				tabAlpha2 = {
					type = "range", name = L["Alpha 2"], order = 6,
					min = 0, max = 1, step = 0.1
				},
				tabFontSize = {
					type = "range", name = L["Font Size"], order = 7,
					min = 5, max = 25, step = 1
				},
				tabTexture = {
					type = "select", name = L["Tab Texture"], order = 8,
					values = function() return self:SMList() end
				}
			}
		}
	}
	for k, v in pairs(to) do
		args[k] = v
	end
end
