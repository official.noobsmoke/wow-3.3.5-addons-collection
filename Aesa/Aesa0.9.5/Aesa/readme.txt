

The word "AESA" which stands for "Active Electronically Scanned Array", is known as a type of radar built on phased array technology. While here, it's a lightweight WOW addon that tracks player's cooldowns for you and help you control the combat.

The addon processes only crucial abilities such as Kidney Shot (rogue), Will of the Forsaken (racial) and ignores worthless cooldowns such as Powerword:Shield (priest) to avoid explosive increasing of timer bars that makes you confused.

Features:

    * Unit cooldowns tracking (either from friendly or hostile units, at your option) within your combatlog range and displays countdown timers according to your present target selection.
    * Support tabbed browsing of unit cooldowns. The addon keeps a record of your target history and allows you to reinspect their cooldowns by switching between tabs without changing (and losing) the present target.
    * Support cooldowns tracking of focus.
    * Correctly handles chained cooldowns, e.g. cold snap (mage) resets CD of all frost spells and preparation (rogue) resets sprint, vanish, evasion, etc...
    * The last cooldown ability triggered by your target will be highlighted in order to prevent you from getting lost into dozens of cooldown timers.
    * Minor UI customizations: size, font, texture, etc...

Known Issues:

    * As some talents/equipments will reduce cooldowns of certain abilities, for example, intercept (warrior ability), which originally has a 30 seconds cooldown is reduced to 15 for a 33/28 PvP/Arena warrior. The addon is unable to analyse these talents infomation due to protections of BLIZZARD core API. As a result I have to define and use the minimal cooldown value of spells in data context as far as I can.

Brilliant ideas/suggestions are always welcomed, and thank you for your concern.

- Maintained by kydori
