-- Copyright (c) 2008 kydori <kydori@gmail.com>

Aesa = LibStub("AceAddon-3.0"):NewAddon("Aesa", "AceConsole-3.0", "AceEvent-3.0")
Aesa.appName = "Aesa"
Aesa.dbName = "AesaDB"
Aesa.version = "0.9.4"

function Aesa:ToNumber(v)
	local r = tonumber(v)
	if not r then return 0 end
	return r
end

function Aesa:ToString(v)
	local r = tostring(v)
	if not r then return "" end
	return r
end

function Aesa:IsTable(o)
	return type(o) == "table"
end

function Aesa:IsString(o)
	return type(o) == "number" or type(o) == "string"
end

function Aesa:StrWordCount(str, word)
	if not self:IsString(str) or not self:IsString(word) or string.len(str) < string.len(word) then
		return 0
	end
	local len = string.len(str) - string.len(word) + 1
	local i, n = 1, 0
	while i < len do
		i = string.find(str, word, i, true)
		if not i then return n end
		i = i + 1
		n = n + 1
	end
	return n
end

function Aesa:PrintTable(t, prefix, n)
	if not prefix then prefix = "." end
	if not n then n = 1 end
	if n > 6 then return "..." end
	if self:IsTable(t) then
		for k, v in pairs(t) do
			if (self:IsTable(v)) then
				self:Print(prefix .. " / " .. k .. " : ")
				self:PrintTable(v, prefix .. " / " .. k, n + 1)
			elseif self:IsString(v) then
				self:Print(prefix .. " / " .. k .. " = " .. v)
			end
		end
	end
end
