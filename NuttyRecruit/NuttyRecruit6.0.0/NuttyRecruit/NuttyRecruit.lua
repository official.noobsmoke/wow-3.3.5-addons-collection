--Big thank you to zaphon helping with zone changed issue---

---few notes i have localised the channel types tbh I am not shure if they need localising or not (this is the first addon i have localised an addon)---

NuttyRecruit = LibStub("AceAddon-3.0"):NewAddon("NuttyRecruit", "AceTimer-3.0", "AceConsole-3.0", "AceEvent-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale("NuttyRecruit")
local df = LibStub("LibDeformat-3.0")


-- the options--
local options = { 
    name = L["Nutty Recruit"], 
    handler = NuttyRecruit,
	type = "group",
	childGroups = "select",
    args = {
			intro = {
			type = "description",
			name = L["Guild Recruitment and Officer addon."],
			order = 0,
		},
		arecruitmentspamoptions ={
			type = "group",
			order = 0,
			name = L["Recruitment Spam"], --localise
			args = {
		recruitmentspamheader = {
			type = "header",
			name = L["Recruitment Spam"], 
			order = 0,
		},
			--enable the spam--
		enable = {
			type = "toggle",
			name = L["Recruitment Spam"], 
			order = 1,
			desc = L["Enables recruitment spam."],
			get = function(info) return NuttyRecruit.db.profile.enable end,
			set = function(info, value) NuttyRecruit.db.profile.enable = value end,
		},
			--time between spams--											
		interval = {
			type = 'range',
			name = L["Interval"], 
			desc = L["The amount of SECONDS between spammings in a particular location"], 
			min = 300,
			max = 3000,
			step = 1,			
			get = function(info)
						return NuttyRecruit.db.profile.between
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.between = newValue
					end,
			order = 2,
		},
		spamwhenafkenable = {
			type = "toggle",
			name = L["Spam when afk or busy"], --loc
			order = 3,
			desc = L["Lets Nutty Recruit spam when afk or busy. Nutty Recruit will not spam when you are afk or busy unless you enable this option"], --loc
			get = function(info) return NuttyRecruit.db.profile.afkspam end,
			set = function(info, value) NuttyRecruit.db.profile.afkspam = value end,
		},
			--the message--
		recruitmentmsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message text to be displayed"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.message
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.message = newValue
					end,
			order = 4,
		},
			--the channels--
		incityselect = {
			type = "description",
			name = L["Select a channel to use when in a city."],
			order = 5,
		},
		trade = {
			type = "toggle",
			name = L["Trade"],
			order = 6,
			desc = L["Enables trade advertisement."],
			get = function(info) return NuttyRecruit.db.profile.trade end,
			set = function(info, value) NuttyRecruit.db.profile.trade = value end,
			disabled = function(info) return NuttyRecruit.db.profile.guildrecruitment end,
		},
		guildrecruitment = {
			type = "toggle",
			name = L["Guild Recruitment"],
			order = 7,
			desc = L["Enables guild recruitment advertisement."], 
			get = function(info) return NuttyRecruit.db.profile.guildrecruitment end,
			set = function(info, value) NuttyRecruit.db.profile.guildrecruitment = value end,
			disabled = function(info) return NuttyRecruit.db.profile.trade end,
		},
		outofcityselect = {
			type = "description",
			name = L["Select a channel to use when NOT in a city."],
			order = 8,
		},
		general  = {
			type = "toggle",
			name = L["General"],
			order = 9,
			desc = L["Enables general advertisement."],
			get = function(info) return NuttyRecruit.db.profile.general  end,
			set = function(info, value) NuttyRecruit.db.profile.general  = value end,
			disabled = function(info) return NuttyRecruit.db.profile.localdefense end,
		},
		localdefense = {
			type = "toggle",
			name = L["Local Defense"], 
			order = 10,
			desc = L["Enables local defense advertisement."], 
			get = function(info) return NuttyRecruit.db.profile.localdefense end,
			set = function(info, value) NuttyRecruit.db.profile.localdefense = value end,
			disabled = function(info) return NuttyRecruit.db.profile.general end,
		},

		spaminfo = {
			type = "description",
			name = L["You can type %s and it will be replaced with <Guild Name>(Lvl: GuildLvl) changing the Name and level to your guilds. e.g. <Next Episode> (Lvl: 9)"], --localise
			order = 11,
		},
		},
		},
		bMOTDoptions ={
			type = "group",
			name = L["MOTD Options."],
			order = 1,
			args = {
		MOTDselect = {
			type = "header",
			name = L["MOTD Options."],
			order = 0,
		},
		enableMOTD = {
			type = "toggle",
			name = L["MOTD spam"], 
			order = 1,
			desc = L["Enables guild message of the day spam."], 
			get = function(info) return NuttyRecruit.db.profile.MOTD end,
			set = function(info, value) NuttyRecruit.db.profile.MOTD = value end,
		},
		motdinterval = {
			type = 'range',
			name = L["MOTD interval"], 
			desc = L["The amount of SECONDS between spamming guild message of the day"],
			min = 900,
			max = 18000,
			step = 1,			
			get = function(info)
						return NuttyRecruit.db.profile.MOTDbetween
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.MOTDbetween = newValue
					end,
			order = 2,
		},
		gmotdmsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message text to be displayed"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.gmotdmessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.gmotdmessage = newValue
					end,
			order = 3,
		},
		},
		},
		cNewRecruitoptions ={
			type = "group",
			name = L["New Recruit Spam Options."],
			order = 3,
			args = {
		NRselect = {
			type = "header",
			name = L["New Recruit Spam Options."], 
			order = 0,
		},
		newrecruitenable = {
			type = "toggle",
			name = L["New recruit welcome"], 
			order = 2,
			desc = L["Enables new recruit spam."], 
			get = function(info) return NuttyRecruit.db.profile.NREnable end,
			set = function(info, value) NuttyRecruit.db.profile.NREnable = value end,
		},
		time = {
			type = 'range',
			name = L["New recruit time"],
			desc = L["The average amount of time (in seconds) between noticing a new member and your welcome message"],
			min = 1,
			max = 20,
			order = 3,
			get =   function()
                        return tonumber(NuttyRecruit.db.profile.NRbetween)
                    end,
			set =   function(info, NewValue)
                        NuttyRecruit.db.profile.NRbetween = tonumber(NewValue)
                    end,
		},
		msg = {
            type = 'input',
            multiline = true,
            name = L["Message"],
            desc = L["The message text to be displayed.  Note the string \"%s\" will be replaced with the new recruit name"],
            usage = L["<Your message here>"],
            width = "full",
			order = 4,
            get =   function() 
                        return tostring(NuttyRecruit.db.profile.NRmessage)
                    end,
            set =   function(info, NewValue)
                        NuttyRecruit.db.profile.NRmessage = tostring(NewValue)
                    end,
        },
		},
		},
		dGUILDINFOoptions ={
			type = "group",
			name = L["Guild Info"],
			order = 4,
			args = {
		guildinfoheader = {
			type = "header",
			name = L["Guild Info"], 
			order = 0,
		},
		GuildInfoenable = {
			type = "toggle",
			width = "full",
			name = L["/w Guild info enable"], 
			order = 1,
			desc = L["Enables guild info reply. When someone /w you the a keyword you auto /w them back with the selected replys below. You can select multiple replys."], 
			get = function(info) return NuttyRecruit.db.profile.GIEnable end,
			set = function(info, value) NuttyRecruit.db.profile.GIEnable = value end,
		},
		guildinfoword = {
			name = L["Add a guild info keyword"],
			desc = L["Add a guild info keyword to the list of keywords."],
			type = "input",
			order = 2,
			get = function() return "" end,
			set = function(_,value)
						local name =(value)
						if name then
							NuttyRecruit.db.profile.guidinfoWord[name] = value
						end
					end,
		},
		editGiWord = {
			name = L["Edit a guild info keyword."],
			desc = L["Select a word from the list to edit it."],
			type = "select",
			order = 3,
			values = function()
						guidinfoWord = {}
						for k,v in pairs(NuttyRecruit.db.profile.guidinfoWord) do
							tinsert(guidinfoWord, k)
						end
						return guidinfoWord
					end,
					
			get = function()
						for k, v in pairs(guidinfoWord) do
							if name == v then
								return k
							end
						end
					end,
			set = function(_,name)
						local name = guidinfoWord[name]
						deletevalue = NuttyRecruit.db.profile.guidinfoWord[name]
					end,
			},
			deleteword = {
					name = function() if deletevalue then return L["|cffFF0000Delete the word:|r "]..deletevalue.."|cffFF0000?|r" else return " " end end,
					type = "group",
					inline = true,
					order = 4,
					args = {
						delete = {
							name = L["Delete Word"], -- loc
							desc = L["Click to delete this word from the keyword list."], --loc
							type = "execute",
							hidden = function() if deletevalue then return false else return true end end,
							func = function() NuttyRecruit.db.profile.guidinfoWord[deletevalue] = nil deletevalue = nil end,
						},
					},
				},
				
				
		guildinfotime = {
			name = L["Time between messages"], --loc
			desc = L["The time between message spams in seconds."],  --loc
			type = 'range',
			order = 5,
			min = 1,
			max = 60,
			step = 1,			
			get = function(info)
						return NuttyRecruit.db.profile.delaytime
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.delaytime = newValue
					end,
		},
		space = {
		type = "description",
			order = 6,
			name = " ",
		},
		replyone = {
			type = "toggle",
			name = L["Reply 1"], 
			order = 7,
			desc = L["Enables Guild Info Reply 1"], 
			get = function(info) return NuttyRecruit.db.profile.GIRONEEnable end,
			set = function(info, value) NuttyRecruit.db.profile.GIRONEEnable = value end,
		},
		
		replyonemsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Reply 1"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.GIRONEmessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.GIRONEmessage = newValue
					end,
			order = 8,
		},
		replytwo = {
			type = "toggle",
			name = L["Reply 2"], 
			order = 9,
			desc = L["Enables Guild Info Reply 2"], 
			get = function(info) return NuttyRecruit.db.profile.GIRTWOEnable end,
			set = function(info, value) NuttyRecruit.db.profile.GIRTWOEnable = value end,
		},
		replytwomsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Reply 2"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.GIRTWOmessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.GIRTWOmessage = newValue
					end,
			order = 10,
		},
		replythree = {
			type = "toggle",
			name = L["Reply 3"], 
			order = 11,
			desc = L["Enables Guild Info Reply 3"], 
			get = function(info) return NuttyRecruit.db.profile.GIRTHREEEnable end,
			set = function(info, value) NuttyRecruit.db.profile.GIRTHREEEnable = value end,
		},
		replythreemsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Reply 3"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.GIRTHREEmessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.GIRTHREEmessage = newValue
					end,
			order = 12,
		},
		replyfour = {
			type = "toggle",
			name = L["Reply 4"], 
			order = 13,
			desc = L["Enables Guild Info Reply 4"], 
			get = function(info) return NuttyRecruit.db.profile.GIRFOUREnable end,
			set = function(info, value) NuttyRecruit.db.profile.GIRFOUREnable = value end,
		},
		replyfourmsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Reply 4"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.GIRFOURmessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.GIRFOURmessage = newValue
					end,
			order = 14,
		},
		replyfive = {
			type = "toggle",
			name = L["Reply 5"], 
			order = 15,
			desc = L["Enables Guild Info Reply 5"], 
			get = function(info) return NuttyRecruit.db.profile.GIRFIVEEnable end,
			set = function(info, value) NuttyRecruit.db.profile.GIRFIVEEnable = value end,
		},
		replyfivemsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Reply 5"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.GIRFIVEmessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.GIRFIVEmessage = newValue
					end,
			order = 16,
		},
		},
		},
		eRaidOptionsoptions ={
			type = "group",
			order = 5,
			name = L["Raid Options"],
			args = {
		raidoptionsheader = {
			type = "header",
			name = L["Raid Options"], 
			order = 0,
		},
		raidinviteenable = {
			type = "toggle",
			name = L["/w Invite"], 
			order = 1,
			desc = L["Enables /w invite. So when people /w you a keyword you auto send them a group invite"], 
			get = function(info) return NuttyRecruit.db.profile.INVITEEnable end,
			set = function(info, value) NuttyRecruit.db.profile.INVITEEnable = value end,
		},
		raidinviteword = {
			name = L["Add a /w invite keyword"],
			desc = L["Add a /w invite keyword to the list of keywords."],
			type = "input",
			order = 2,
			get = function() return "" end,
			set = function(_,value)
						local name =(value)
						if name then
							NuttyRecruit.db.profile.RaidInviteWord[name] = value
						end
					end,
		},
		editRIWords = {
			name = L["Edit a /w invite keyword."],
			desc = L["Select a word from the list to edit it."],
			type = "select",
			order = 3,
			values = function()
						RaidInviteWord = {}
						for k,v in pairs(NuttyRecruit.db.profile.RaidInviteWord) do
							tinsert(RaidInviteWord, k)
						end
						return RaidInviteWord
					end,
						get = function()
						for k, v in pairs(RaidInviteWord) do
							if name == v then
								return k
							end
						end
					end,
						set = function(_,name)
						local name = RaidInviteWord[name]
						deletevalueri = NuttyRecruit.db.profile.RaidInviteWord[name]
					end,
			},
		deleteRIword = {
				name = function() if deletevalueri then return L["|cffFF0000Delete the word:|r "]..deletevalueri.."|cffFF0000?|r" else return " " end end,
				type = "group",
				inline = true,
				order = 4,
				args = {
					delete = {
						name = L["Delete Word"], -- loc
						desc = L["Click to delete this word from the keyword list."], --loc
						type = "execute",
						hidden = function() if deletevalueri then return false else return true end end,
						func = function() NuttyRecruit.db.profile.RaidInviteWord[deletevalueri] = nil deletevalueri = nil end,
					},
				},
			},
		raidcaonvertenable = {
			type = "toggle",
			name = L["Auto convert to raid"], 
			order = 5,
			desc = L["Enables auto convert to raid when you are the party leader and have more than two members in the party. Useful for /w invite if forming a raid"], 
			get = function(info) return NuttyRecruit.db.profile.RCONVERTEnable end,
			set = function(info, value) NuttyRecruit.db.profile.RCONVERTEnable = value end,
		},		
		shortreadycheckenable = {
			type = "toggle",
			name = L["Short ready check"], 
			order = 6,
			desc = L["Enables /rc to do a ready check"], 
			get = function(info) return NuttyRecruit.db.profile.readycheckoptions end,
			set = function(info, value) NuttyRecruit.db.profile.readycheckoptions = value end,
		},
		raidrollenable = {
			type = "toggle",
			name = L["Raid roll"], 
			order = 7,
			desc = L["Enables /rr /raidroll /rroll to do a raid roll. You can add the itemlink to the raid roll command"], 
			get = function(info) return NuttyRecruit.db.profile.raidrolloptions end,
			set = function(info, value) NuttyRecruit.db.profile.raidrolloptions = value end,
		},
		rolltrackerenable = {
			type = "toggle",
			name = L["Enable roll tracker"], 
			order = 8,
			desc = L["Enables roll tracker. Once enabled you can use /rt or /rolltracker to bring up the roll tracking window. The window will auto open when someone does a /roll with this option enabled "], 
			get = function(info) return NuttyRecruit.db.profile.rolltrackeroptions end,
			set = function(info, value)
			NuttyRecruit.db.profile.rolltrackeroptions = value
			NuttyRecruit:RollTrackerShowHide()end,
		},
		rolltrackerclear = {
			type = "toggle",
			name = L["Auto roll tracker clear"], 
			order = 9,
			desc = L["Enables auto clearing the roll tracker data when you close the window."], 
			get = function(info) return NuttyRecruit.db.profile.rolltrackerclearoptions end,
			set = function(info, value) NuttyRecruit.db.profile.rolltrackerclearoptions = value end,
		},
		lock = {
			type = "toggle",
			name = L["Unlock roll tracker"], 
			order = 10,
			desc = L["Unlock roll tracker so you can move its location. NOTE: Enable roll tracker first"], 
			get = function(info) return NuttyRecruit.db.profile.lock end,
			set = function(info, value) 
			NuttyRecruit.db.profile.lock = value 
			NuttyRecruit:ChangeRollTrackerText()end,
		},
		disbandraidenable = {
			type = "toggle",
			name = L["Enable raid disband"], 
			order = 11,
			desc = L["Enables raid disband. Type /draid, /dr or /disband to disband the raid."], 
			get = function(info) return NuttyRecruit.db.profile.disbandraidenable end,
			set = function(info, value)	NuttyRecruit.db.profile.disbandraidenable = value end,
		},
		},
		},
		fspamoptions ={
			type = "group",
			order = 6,
			name = L["Spam Options"],
			args = {
		spamheader = {
			type = "header",
			name = L["Spams"], 
			order = 0,
		},
		spamoneenable = {
			type = "toggle",
			name = L["Spam 1"], 
			order = 1,
			width = "full",
			desc = L["Enables /spamone or /s1 command"], 
			get = function(info) return NuttyRecruit.db.profile.enablespamone end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamone = value end,
		},
		spamonepartraidenable = {
			type = "toggle",
			name = L["Raid/Party spam"], 
			order = 2,
			desc = L["Enables party or raid spam depending on if you are in a party or raid for Spam 1"], 
			get = function(info) return NuttyRecruit.db.profile.enablespamonepartyraid end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamonepartyraid = value end,
		},
		spamoneguildenable = {
			type = "toggle",
			name = L["Guild spam"], 
			order = 3,
			desc = L["Enables guild spam for Spam 1"], 
			get = function(info) return NuttyRecruit.db.profile.enablespamoneguild end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamoneguild = value end,
		},
		spamonemsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Spam 1"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.spamonemessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.spamonemessage = newValue
					end,
			order = 4,
		},
		spamtwoenable = {
			type = "toggle",
			name = L["Spam 2"],
			order = 5,
			width = "full",
			desc = L["Enables /spamtwo or /s2 command"],
			get = function(info) return NuttyRecruit.db.profile.enablespamtwo end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamtwo = value end,
		},
		spamtwopartraidenable = {
			type = "toggle",
			name = L["Raid/Party spam"], 
			order = 6,
			desc = L["Enables party or raid spam depending on if you are in a party or raid for Spam 2"],
			get = function(info) return NuttyRecruit.db.profile.enablespamtwopartyraid end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamtwopartyraid = value end,
		},
		spamtwoguildenable = {
			type = "toggle",
			name = L["Guild spam"], 
			order = 7,
			desc = L["Enables guild spam for Spam 2"],
			get = function(info) return NuttyRecruit.db.profile.enablespamtwoguild end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamtwoguild = value end,
		},
		spamtwomsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Spam 2"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.spamtwomessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.spamtwomessage = newValue
					end,
			order = 8,
		},
		spamthreeenable = {
			type = "toggle",
			name = L["Spam 3"],
			order = 9,
			width = "full",
			desc = L["Enables /spamthree or /s3 command"],
			get = function(info) return NuttyRecruit.db.profile.enablespamthree end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamthree = value end,
		},
		spamthreepartraidenable = {
			type = "toggle",
			name = L["Raid/Party spam"], 
			order = 10,
			desc = L["Enables party or raid spam depending on if you are in a party or raid for Spam 3"],
			get = function(info) return NuttyRecruit.db.profile.enablespamthreepartyraid end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamthreepartyraid = value end,
		},
		spamthreeguildenable = {
			type = "toggle",
			name = L["Guild spam"], 
			order = 11,
			desc = L["Enables guild spam for Spam 3"],
			get = function(info) return NuttyRecruit.db.profile.enablespamthreeguild end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamthreeguild = value end,
		},
		spamthreemsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Spam 3"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.spamthreemessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.spamthreemessage = newValue
					end,
			order = 12,
		},
		spamfourenable = {
			type = "toggle",
			name = L["Spam 4"],
			order = 13,
			width = "full",
			desc = L["Enables /spamfour or /s4 command"],
			get = function(info) return NuttyRecruit.db.profile.enablespamfour end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamfour = value end,
		},
		spamfourpartraidenable = {
			type = "toggle",
			name = L["Raid/Party spam"], 
			order = 14,
			desc = L["Enables party or raid spam depending on if you are in a party or raid for Spam 4"],
			get = function(info) return NuttyRecruit.db.profile.enablespamfourpartyraid end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamfourpartyraid = value end,
		},
		spamfourguildenable = {
			type = "toggle",
			name = L["Guild spam"], 
			order = 15,
			desc = L["Enables guild spam for Spam 4"],
			get = function(info) return NuttyRecruit.db.profile.enablespamfourguild end,
			set = function(info, value) NuttyRecruit.db.profile.enablespamfourguild = value end,
		},
		spamfourmsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Message"], 
			desc = L["The message for Spam 4"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.spamfourmessage
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.spamfourmessage = newValue
					end,
			order = 16,
		},
		
		},
	},
		ginviteoptions ={
			type = "group",
			order = 7,
			name = L["Guild Invite"],
			args = {
		giheader = {
			type = "header",
			name = L["Guild Invite"], 
			order = 1,
		},
		gikenable = {
			type = "toggle",
			name = L["/w Guild invite"], 
			order = 2,
			desc = L["Enables /w guild invite. So when people /w you a keyword you auto send them a guild invite"],
			get = function(info) return NuttyRecruit.db.profile.gikenable end,
			set = function(info, value) NuttyRecruit.db.profile.gikenable = value end,
		},
		gikword = {
			name = L["Add a guild invite keyword"],
			desc = L["Add a /w guild invite keyword to the list of keywords."],
			type = "input",
			order = 3,
			get = function() return "" end,
			set = function(_,value)
						local name =(value)
						if name then
							NuttyRecruit.db.profile.gikWord[name] = value
						end
					end,
		},
		editGIKWords = {
			name = L["Edit a guild invite keyword."], -- loc
			desc = L["Select a word from the list to edit it."],--loc
			type = "select",
			order = 3,
			values = function()
						gikWord = {}
						for k,v in pairs(NuttyRecruit.db.profile.gikWord) do
							tinsert(gikWord, k)
						end
						return gikWord
					end,
						get = function()
						for k, v in pairs(gikWord) do
							if name == v then
								return k
							end
						end
					end,
						set = function(_,name)
						local name = gikWord[name]
						deletevaluegik = NuttyRecruit.db.profile.gikWord[name]
					end,
			},
			deleteRIword = {
					name = function() if deletevaluegik then return L["|cffFF0000Delete the word:|r "]..deletevaluegik.."|cffFF0000?|r" else return " " end end,
					type = "group",
					inline = true,
					order = 4,
					args = {
						delete = {
							name = L["Delete Word"], -- loc
							desc = L["Click to delete this word from the keyword list."], --loc
							type = "execute",
							hidden = function() if deletevaluegik then return false else return true end end,
							func = function() NuttyRecruit.db.profile.gikWord[deletevaluegik] = nil deletevaluegik = nil end,
						},
					},
				},

		notesinfo = {
			type = "header",
			name = L["Notes"], 
			order = 5,
		},
		commandsinfo = {
			type = "description",
			name = L["You can type /gi or /ginvite to invite people to join your guild. If they are already in a guild you will auto /w them to tell them that they need to /gquit"], --localise
			order = 6,
		},
		},
		},
		autoreplyoptions ={
			type = "group",
			order = 8,
			name = L["Auto Reply"],
			args = {
		giheader = {
			type = "header",
			name = L["Auto Reply"], 
			order = 1,
		},
		timebeforereply = {
			name = L["Time before auto reply."], --loc
			desc = L["The time to wait before you auto reply in seconds."],  --loc
			type = 'range',
			order = 2,
			min = 1,
			max = 30,
			step = 1,			
			get = function(info)
						return NuttyRecruit.db.profile.replytime
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.replytime = newValue
					end,
		},
		spaceaftertime = {
			type = "description",
			name = "",
			order = 3,
		},
		Autoreplyoneenable = {
			type = "toggle",
			name = L["Auto reply 1"], 
			order = 4,
			desc = L["Enables Auto reply 1. So when people /w you a keyword you auto send them a /w reply."], 
			get = function(info) return NuttyRecruit.db.profile.AutoROEnable end,
			set = function(info, value) NuttyRecruit.db.profile.AutoROEnable = value end,
		},
		spacearo = {
			type = "description",
			name = "",
			order = 5,
		},
		AutoReplyOneword = {
			name = L["Add a Auto reply 1 keyword"],
			desc = L["Add a Auto reply 1 keyword to the list of keywords."],
			type = "input",
			order = 6,
			get = function() return "" end,
			set = function(_,value)
						local name =(value)
						if name then
							NuttyRecruit.db.profile.AutoReplyOneWord[name] = value
						end
					end,
		},
		editAROWords = {
			name = L["Edit a Auto reply 1 keyword."],
			desc = L["Select a word from the list to edit it."],
			type = "select",
			order = 7,
			values = function()
						AutoReplyOneWord = {}
						for k,v in pairs(NuttyRecruit.db.profile.AutoReplyOneWord) do
							tinsert(AutoReplyOneWord, k)
						end
						return AutoReplyOneWord
					end,
						get = function()
						for k, v in pairs(AutoReplyOneWord) do
							if name == v then
								return k
							end
						end
					end,
						set = function(_,name)
						local name = AutoReplyOneWord[name]
						deletevaluearo = NuttyRecruit.db.profile.AutoReplyOneWord[name]
					end,
			},
		deleteAROword = {
				name = function() if deletevaluearo then return L["|cffFF0000Delete the word:|r "]..deletevaluearo.."|cffFF0000?|r" else return " " end end,
				type = "group",
				inline = true,
				order = 8,
				args = {
					delete = {
						name = L["Delete Word"],
						desc = L["Click to delete this word from the keyword list."],
						type = "execute",
						hidden = function() if deletevaluearo then return false else return true end end,
						func = function() NuttyRecruit.db.profile.AutoReplyOneWord[deletevaluearo] = nil deletevaluearo = nil end,
					},
				},
			},
		aronemsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Auto reply 1 message"], 
			desc = L["The message text to be displayed"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.autoreplyonemsg
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.autoreplyonemsg = newValue
					end,
			order = 9,
		},
		Autoreplytwoenable = {
			type = "toggle",
			name = L["Auto reply 2"], 
			order = 10,
			desc = L["Enables Auto reply 2. So when people /w you a keyword you auto send them a /w reply."], 
			get = function(info) return NuttyRecruit.db.profile.AutoRTEnable end,
			set = function(info, value) NuttyRecruit.db.profile.AutoRTEnable = value end,
		},
		spaceartwo = {
			type = "description",
			name = "",
			order = 11,
		},
		AutoReplyTWOword = {
			name = L["Add a Auto reply 2 keyword"],
			desc = L["Add a Auto reply 2 keyword to the list of keywords."],
			type = "input",
			order = 12,
			get = function() return "" end,
			set = function(_,value)
						local name =(value)
						if name then
							NuttyRecruit.db.profile.AutoReplyTwoWord[name] = value
						end
					end,
		},
		editARTWords = {
			name = L["Edit a Auto reply 2 keyword."],
			desc = L["Select a word from the list to edit it."],
			type = "select",
			order = 13,
			values = function()
						AutoReplyTwoWord = {}
						for k,v in pairs(NuttyRecruit.db.profile.AutoReplyTwoWord) do
							tinsert(AutoReplyTwoWord, k)
						end
						return AutoReplyTwoWord
					end,
						get = function()
						for k, v in pairs(AutoReplyTwoWord) do
							if name == v then
								return k
							end
						end
					end,
						set = function(_,name)
						local name = AutoReplyTwoWord[name]
						deletevalueart = NuttyRecruit.db.profile.AutoReplyTwoWord[name]
					end,
			},
		deleteARTword = {
				name = function() if deletevalueart then return L["|cffFF0000Delete the word:|r "]..deletevalueart.."|cffFF0000?|r" else return " " end end,
				type = "group",
				inline = true,
				order = 14,
				args = {
					delete = {
						name = L["Delete Word"],
						desc = L["Click to delete this word from the keyword list."],
						type = "execute",
						hidden = function() if deletevalueart then return false else return true end end,
						func = function() NuttyRecruit.db.profile.AutoReplyTwoWord[deletevalueart] = nil deletevalueart = nil end,
					},
				},
			},
		artwomsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Auto reply 2 message"], 
			desc = L["The message text to be displayed"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.autoreplytwomsg
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.autoreplytwomsg = newValue
					end,
			order = 15,
		},
		Autoreplythreeenable = {
			type = "toggle",
			name = L["Auto reply 3"], 
			order = 16,
			desc = L["Enables Auto reply 3. So when people /w you a keyword you auto send them a /w reply."], 
			get = function(info) return NuttyRecruit.db.profile.AutoRThreeEnable end,
			set = function(info, value) NuttyRecruit.db.profile.AutoRThreeEnable = value end,
		},
		spacearthree = {
			type = "description",
			name = "",
			order = 17,
		},
		AutoReplyTHREEword = {
			name = L["Add a Auto reply 3 keyword"],
			desc = L["Add a Auto reply 3 keyword to the list of keywords."],
			type = "input",
			order = 18,
			get = function() return "" end,
			set = function(_,value)
						local name =(value)
						if name then
							NuttyRecruit.db.profile.AutoRThreeWord[name] = value
						end
					end,
		},
		editARThreeWords = {
			name = L["Edit a Auto reply 3 keyword."],
			desc = L["Select a word from the list to edit it."],
			type = "select",
			order = 19,
			values = function()
						AutoRThreeWord = {}
						for k,v in pairs(NuttyRecruit.db.profile.AutoRThreeWord) do
							tinsert(AutoRThreeWord, k)
						end
						return AutoRThreeWord
					end,
						get = function()
						for k, v in pairs(AutoRThreeWord) do
							if name == v then
								return k
							end
						end
					end,
						set = function(_,name)
						local name = AutoRThreeWord[name]
						deletevaluearthree = NuttyRecruit.db.profile.AutoRThreeWord[name]
					end,
			},
		deleteARThreeword = {
				name = function() if deletevaluearthree then return L["|cffFF0000Delete the word:|r "]..deletevaluearthree.."|cffFF0000?|r" else return " " end end,
				type = "group",
				inline = true,
				order = 20,
				args = {
					delete = {
						name = L["Delete Word"],
						desc = L["Click to delete this word from the keyword list."],
						type = "execute",
						hidden = function() if deletevaluearthree then return false else return true end end,
						func = function() NuttyRecruit.db.profile.AutoRThreeWord[deletevaluearthree] = nil deletevaluearthree = nil end,
					},
				},
			},
		arthreemsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Auto reply 3 message"], 
			desc = L["The message text to be displayed"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.autoreplythreemsg
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.autoreplythreemsg = newValue
					end,
			order = 21,
		},
		Autoreplyfourenable = {
			type = "toggle",
			name = L["Auto reply 4"], 
			order = 22,
			desc = L["Enables Auto reply 4. So when people /w you a keyword you auto send them a /w reply."], 
			get = function(info) return NuttyRecruit.db.profile.AutoRFourEnable end,
			set = function(info, value) NuttyRecruit.db.profile.AutoRFourEnable = value end,
		},
		spacearfour = {
			type = "description",
			name = "",
			order = 23,
		},
		AutoReplyFourword = {
			name = L["Add a Auto reply 4 keyword"],
			desc = L["Add a Auto reply 4 keyword to the list of keywords."],
			type = "input",
			order = 24,
			get = function() return "" end,
			set = function(_,value)
						local name =(value)
						if name then
							NuttyRecruit.db.profile.AutoRFourWord[name] = value
						end
					end,
		},
		editARFourWords = {
			name = L["Edit a Auto reply 4 keyword."],
			desc = L["Select a word from the list to edit it."],
			type = "select",
			order = 25,
			values = function()
						AutoRFourWord = {}
						for k,v in pairs(NuttyRecruit.db.profile.AutoRFourWord) do
							tinsert(AutoRFourWord, k)
						end
						return AutoRFourWord
					end,
						get = function()
						for k, v in pairs(AutoRFourWord) do
							if name == v then
								return k
							end
						end
					end,
						set = function(_,name)
						local name = AutoRFourWord[name]
						deletevaluearfour = NuttyRecruit.db.profile.AutoRFourWord[name]
					end,
			},
		deleteARFourword = {
				name = function() if deletevaluearfour then return L["|cffFF0000Delete the word:|r "]..deletevaluearfour.."|cffFF0000?|r" else return " " end end,
				type = "group",
				inline = true,
				order = 26,
				args = {
					delete = {
						name = L["Delete Word"],
						desc = L["Click to delete this word from the keyword list."],
						type = "execute",
						hidden = function() if deletevaluearfour then return false else return true end end,
						func = function() NuttyRecruit.db.profile.AutoRFourWord[deletevaluearfour] = nil deletevaluearfour = nil end,
					},
				},
			},
		arfourmsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Auto reply 4 message"], 
			desc = L["The message text to be displayed"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.autoreplyfourmsg
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.autoreplyfourmsg = newValue
					end,
			order = 27,
		},
		Autoreplyfiveenable = {
			type = "toggle",
			name = L["Auto reply 5"], 
			order = 28,
			desc = L["Enables Auto reply 5. So when people /w you a keyword you auto send them a /w reply."], 
			get = function(info) return NuttyRecruit.db.profile.AutoRFiveEnable end,
			set = function(info, value) NuttyRecruit.db.profile.AutoRFiveEnable = value end,
		},
		spacearfive = {
			type = "description",
			name = "",
			order = 29,
		},
		AutoReplyFIVEword = {
			name = L["Add a Auto reply 5 keyword"],
			desc = L["Add a Auto reply 5 keyword to the list of keywords."],
			type = "input",
			order = 30,
			get = function() return "" end,
			set = function(_,value)
						local name =(value)
						if name then
							NuttyRecruit.db.profile.AutoRFiveWord[name] = value
						end
					end,
		},
		editARFiveWords = {
			name = L["Edit a Auto reply 5 keyword."],
			desc = L["Select a word from the list to edit it."],
			type = "select",
			order = 31,
			values = function()
						AutoRFiveWord = {}
						for k,v in pairs(NuttyRecruit.db.profile.AutoRFiveWord) do
							tinsert(AutoRFiveWord, k)
						end
						return AutoRFiveWord
					end,
						get = function()
						for k, v in pairs(AutoRFiveWord) do
							if name == v then
								return k
							end
						end
					end,
						set = function(_,name)
						local name = AutoRFiveWord[name]
						deletevaluearfive = NuttyRecruit.db.profile.AutoRFiveWord[name]
					end,
			},
		deleteARFiveword = {
				name = function() if deletevaluearfive then return L["|cffFF0000Delete the word:|r "]..deletevaluearfive.."|cffFF0000?|r" else return " " end end,
				type = "group",
				inline = true,
				order = 32,
				args = {
					delete = {
						name = L["Delete Word"],
						desc = L["Click to delete this word from the keyword list."],
						type = "execute",
						hidden = function() if deletevaluearfive then return false else return true end end,
						func = function() NuttyRecruit.db.profile.AutoRFiveWord[deletevaluearfive] = nil deletevaluearfive = nil end,
					},
				},
			},
		arfivemsg = {
			type = 'input',
			multiline = true,
			width = "full",
			name = L["Auto reply 5 message"], 
			desc = L["The message text to be displayed"],
			usage = L["<Your message here>"],
			get = function(info)
						return NuttyRecruit.db.profile.autoreplyfivemsg
					end,
			set = function(info, newValue)
						NuttyRecruit.db.profile.autoreplyfivemsg = newValue
					end,
			order = 33,
		},
		},
		},
	},
}

local defaults = {
    profile =  {
	enable = false,
	message = "Put your add here it is a good idea to have /w the keyword guildinfo for more information in your advert if you use the guildinfo part of the addon",
	between = 300,
	localdefense = false,
	general = false,
	trade = false,
	guildrecruitment = false,
	afkspam = false,
	MOTDbetween = 1800,
	MOTD = false,
	NREnable = false,
	NRbetween = 5,
	NRmessage = "Welcome %s I hope you enjoy your new Guild",
	GIEnable = false,
	delaytime = 1,
	GIRONEEnable = false,
	GIRONEmessage = "We can offer a friendly, mature and relaxing atmosphere. As long you respect the other guild members, you will be respected. We do not tolerate whiners, slackers, unreliable people or guild jumpers.",
	GIRTWOEnable = false,
	GIRTWOmessage = "Currently recruiting all classes atm",
	GIRTHREEEnable = false,
	GIRTHREEmessage = "Raid Days and Times: Wed,Thur and Mondays. 20:30-00:00",
	GIRFOUREnable = false,
	GIRFOURmessage = "We have a Social Rank for people not wanting to raid or want to raid at a later date. You do not need to post an application to join as social member but if you want to raid you must post an application before you will be allowed to.",
	GIRFIVEEnable = false,
	GIRFIVEmessage = "Raider applicants must post an application at http://nextepisodewow.tk//",
	INVITEEnable = false,
	RCONVERTEnable = false,
	readycheckoptions = false,
	raidrolloptions = false,
	enablespamone = false,
	spamonemessage = "<<<Raid invites starting /w Nuttyprot the keyword !invite for an invite>>>",
	enablespamoneguild = false,
	enablespamonepartyraid = false,
	enablespamtwo = false,
	enablespamtwopartyraid = false,
	enablespamtwoguild  = false,
	spamtwomessage = "<<Main spec rolls now please>>",
	enablespamthree = false,
	enablespamthreepartyraid = false,
	enablespamthreeguild  = false,
	spamthreemessage = "<<Offspec Rolls Now Please>>",
	enablespamfour = false,
	enablespamfourpartyraid = false,
	enablespamfourguild  = false,
	lock = false,
	spamfourmessage = "<<Thanks for the raid guys well done>>",
	rolltrackeroptions = false,
	rolltrackerclearoptions = false,
	x = 0,
	y = 0,
	disbandraidenable = false,
	guidinfoWord = {},
	RaidInviteWord = {},
	gmotdmessage = "",
	gikenable = false,
	gikWord = {},
	replytime = 2,
	AutoROEnable = false,
	AutoReplyOneWord = {},
	autoreplyonemsg = "Auto reply 1 message",
	AutoRTEnable = false,
	AutoReplyTwoWord = {},
	autoreplytwomsg = "Auto reply 2 message",
	AutoRThreeEnable = false,
	AutoRThreeWord = {},
	autoreplythreemsg = "Auto reply 3 message",
	AutoRFourEnable = false,
	AutoRFourWord = {},
	autoreplyfourmsg = "Auto reply 4 message",
	AutoRFiveEnable = false,
	AutoRFiveWord = {},
	autoreplyfivemsg = "Auto reply 5 message",
	},
}

--when addon starts--
function NuttyRecruit:OnInitialize()
		NuttyRecruit.db = LibStub("AceDB-3.0"):New("NuttyRecruitDB", defaults, "Default")
		LibStub("AceConfig-3.0"):RegisterOptionsTable("NuttyRecruit", options)
		LibStub("AceConfig-3.0"):RegisterOptionsTable("NuttyRecruit Profiles", LibStub("AceDBOptions-3.0"):GetOptionsTable(NuttyRecruit.db))
		NuttyRecruit.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("NuttyRecruit", "NuttyRecruit")
		LibStub("AceConfigDialog-3.0"):AddToBlizOptions("NuttyRecruit Profiles", "Profiles", "NuttyRecruit")
		self.db.RegisterCallback(self, "OnProfileChanged", "OnProfileChanged")
		self.db.RegisterCallback(self, "OnProfileCopied", "OnProfileChanged")
		self.db.RegisterCallback(self, "OnProfileReset", "OnProfileChanged")
		NuttyRecruit:LoadFramePosition()
		RTMainFrame:Hide()
		NuttyRecruit:EventResisters()
		NuttyRecruit:ChatCommands()
		rollArray = {}
		rollNames = {}
end

function NuttyRecruit:EventResisters()
		NuttyRecruit:RegisterEvent("CHAT_MSG_WHISPER", "AutoReplie")
		NuttyRecruit:RegisterEvent("ZONE_CHANGED_NEW_AREA", "ZoneChangedEvent")
		NuttyRecruit:RegisterEvent("PARTY_MEMBERS_CHANGED", "ConvertToRaidCheck")
		NuttyRecruit:RegisterEvent("CHAT_MSG_SYSTEM", "RollCatcher")
end

function NuttyRecruit:ChatCommands()
		NuttyRecruit:RegisterChatCommand("NuttyRecruit", "OpenOptions")
		NuttyRecruit:RegisterChatCommand("NR", "OpenOptions")
		NuttyRecruit:RegisterChatCommand("gi", "Invite")
		NuttyRecruit:RegisterChatCommand("ginvite", "Invite")
		NuttyRecruit:RegisterChatCommand("rc", "DoReadyCheck")
		NuttyRecruit:RegisterChatCommand("raidroll", "RaidRoll")
		NuttyRecruit:RegisterChatCommand("rr", "RaidRoll")
		NuttyRecruit:RegisterChatCommand("rroll", "RaidRoll")
		NuttyRecruit:RegisterChatCommand("spamone", "SpamOne")
		NuttyRecruit:RegisterChatCommand("s1", "SpamOne")
		NuttyRecruit:RegisterChatCommand("spamtwo", "SpamTwo")
		NuttyRecruit:RegisterChatCommand("s2", "SpamTwo")
		NuttyRecruit:RegisterChatCommand("spamthree", "SpamThree")
		NuttyRecruit:RegisterChatCommand("s3", "SpamThree")
		NuttyRecruit:RegisterChatCommand("spamfour", "SpamFour")
		NuttyRecruit:RegisterChatCommand("s4", "SpamFour")
		NuttyRecruit:RegisterChatCommand("rt", "ShowRollTracker")
		NuttyRecruit:RegisterChatCommand("rolltracker", "ShowRollTracker")
		NuttyRecruit:RegisterChatCommand("draid", "Disband")
		NuttyRecruit:RegisterChatCommand("disband", "Disband")
		NuttyRecruit:RegisterChatCommand("dr", "Disband")
		NuttyRecruit:RegisterChatCommand("gispam", "GInfo")
		NuttyRecruit:RegisterChatCommand("ar1", "SendAutoReplyOne")
		NuttyRecruit:RegisterChatCommand("ar2", "SendAutoReplyTwo")
		NuttyRecruit:RegisterChatCommand("ar3", "SendAutoReplyThree")
		NuttyRecruit:RegisterChatCommand("ar4", "SendAutoReplyFour")
		NuttyRecruit:RegisterChatCommand("ar5", "SendAutoReplyFive")
		
end

--slash command handler--
function NuttyRecruit:OpenOptions(input)
    if not input or input:trim() == "" then
        InterfaceOptionsFrame_OpenToCategory(NuttyRecruit.optionsFrame)
    else
        LibStub("AceConfigCmd-3.0").HandleCommand(NuttyRecruit, "NR", "NuttyRecruit", input)
    end
end

--locals--
local city = {}
local TimeSinceLastUpdate = 0;
local TimeSinceLastUpdateMOTD =0;
local rollArray ={}
local rollNames ={}

---- inviter---------------------------------------------
-----------------------------------------------------------

--called when a person tryes to invite someone via /gi or /ginvite----
function NuttyRecruit:Invite(name)
		 	local bytes, pos = {string.byte(name,1,-1)}, 1
	while(bytes[pos+1] and bit.band(bytes[pos+1], 0xc0) == 0x80)do
		pos = pos + 1
	end
	name = string.upper(string.sub(name, 1, pos)) .. string.lower(string.sub(name, pos+1))
	     NuttyRecruit:Print(L["Ok sit tight doing a who to make sure no guild for: "] .. name)

  SendWho("n-\"" .. name .. "\"")
 NuttyRecruit:ScheduleTimer("Invitenext", 8, name)
		  	     
  end
--called after doing the Who check---
  function NuttyRecruit:Invitenext(name)
      local count = GetNumWhoResults();
      charname, guildname, level, race, class, zone, unknown = GetWhoInfo(1)
    	addname = name
		if guildname ~= "" then NuttyRecruit:inguild(name) --if person is in a guild this function is called
		else NuttyRecruit:Sendinvite(name) --if person is not in a guild tis function is called
	end
end
	
---if a person is in a guild /w them asking them to leave current guild then try again every 30 seconds 3 times---
t = 1
function NuttyRecruit:inguild(name)
if t==1 then
			SendChatMessage(L["You are in a guild already do a /gquit so I can invite you"], L["WHISPER"], nil, name); --localize
			end
t = (t + 1)
		if t<4 then
		NuttyRecruit:Print(L["User in a guild waiting 30 seconds and re-inviting: "] .. name) --localize
		 NuttyRecruit:ScheduleTimer("Invite", 30, name)

		else
	      NuttyRecruit:Print(L["Tryed to invite 3 times Nutty Recruit aborting invite for: "] .. name) --localize
		  NuttyRecruit:Done(name)
		end

end
--sends the invite---
function NuttyRecruit:Sendinvite(name)
	GuildInvite(name)
end
--announce that we have finished trying to invite some1---
function NuttyRecruit:Done(name)
NuttyRecruit:Print(L["Ok I am done trying for: "] .. name)

end
---- Guild recruitment spamer------------
-----------------------------------------------
--announcer--
function NuttyRecruit:test(zone)
	if NuttyRecruit.db.profile.enable then --checks if addon is enabled--
	NuttyRecruit:AFKCheck()
		if IsInGuild() and not InCombatLockdown() and not IsInInstance() then --checks if in a guild and if in combat or if in instance
			if not NuttyRecruit.afk then
				NuttyRecruit:SpamRecruitmentMessage(zone)
			else if NuttyRecruit.afk and NuttyRecruit.db.profile.afkspam then
				NuttyRecruit:SpamRecruitmentMessage(zone)
			end
		end
	end
end
end
function NuttyRecruit:SpamRecruitmentMessage(zone)
			local guildLevel = GetGuildLevel()
			local guildName = GetGuildInfo("PLAYER")
			local info = string.format("%s", "<"..guildName.."> (Lvl: "..guildLevel..")")
			local message = string.format(NuttyRecruit.db.profile.message, info)
			if NuttyRecruit:IsCity() then -- checks if we are in city
				NuttyRecruit:InCityAdvertise(message)
			else -- we are not in a city
				NuttyRecruit:OutCityAdvertise(message)
			end
end
function NuttyRecruit:AFKCheck()
if UnitIsAFK("player") or UnitIsDND("player") then
	NuttyRecruit.afk = true
else
	NuttyRecruit.afk = false
	end
end

--called when zone is changed----
function NuttyRecruit:ZoneChangedEvent()
	TimeSinceLastUpdate = 0
end

--the timer--
local function OnUpdate(self, elapsed)
	local UpdateInterval = tonumber(NuttyRecruit.db.profile.between)
    TimeSinceLastUpdate = TimeSinceLastUpdate + elapsed
    if TimeSinceLastUpdate and TimeSinceLastUpdate >= 15 then
		-- only check every 15 seconds --
		TimeSinceLastUpdate = 0
	    local zone = GetZoneText()
		if NuttyRecruit:IsCity() then
			zone = L["city"] -- Make all cities one global entry, so we don't spam when going from Dalaran to Orgrimmar for example
		end
		if zone then
			if type(NuttyRecruit.db.profile.lasttime) ~= "table" then
				NuttyRecruit.db.profile.lasttime = {}
			end
			if NuttyRecruit.db.profile.lasttime[zone] == nil then
				NuttyRecruit.db.profile.lasttime[zone] = 0
			end
			local diff = time() - tonumber(NuttyRecruit.db.profile.lasttime[zone])
			if (diff >= UpdateInterval) then
				NuttyRecruit:test(zone)
				NuttyRecruit.db.profile.lasttime[zone] = time()
			end
		end
	end
end

--returns true or false if in a city or not--
function NuttyRecruit:IsCity()
    zone=GetZoneText()
    if city[zone] == 1 then 
        return true
    elseif city[zone] == 0 then 
        return false
    else
        local channels = { EnumerateServerChannels() }
        for i, v in ipairs(channels) do
            if v == L["Trade"] then
                city[zone] = 1
                return true
            end
        end 
        city[zone] = 0
        return false
    end
end
--channel functions simple yet effective way of filtering the selected channels--
function NuttyRecruit:InCityAdvertise(message)
		NuttyRecruit:trade(message) 
		NuttyRecruit:guildrecruitment(message) 
end
function NuttyRecruit:OutCityAdvertise(message)
		NuttyRecruit:general(message) 
		NuttyRecruit:localdefense(message) 
end
function NuttyRecruit:trade(message)
	if NuttyRecruit.db.profile.trade then
		SendChatMessage(message, "CHANNEL",nil,GetChannelName(L["Trade - City"]));
	end
end
function NuttyRecruit:guildrecruitment(message) 
	if NuttyRecruit.db.profile.guildrecruitment then
		SendChatMessage(message, "CHANNEL",nil,GetChannelName(L["GuildRecruitment - City"])); 
	end 
end
function NuttyRecruit:general(message)
	if NuttyRecruit.db.profile.general then
		SendChatMessage(message, "CHANNEL",nil,GetChannelName(L["General - "]..GetZoneText()));
	end
end
function NuttyRecruit:localdefense(message)
	if NuttyRecruit.db.profile.localdefense then
		SendChatMessage(message, "CHANNEL",nil,GetChannelName(L["LocalDefense - "]..GetZoneText()));
	end
end
local RecruitSpam = CreateFrame("frame")
RecruitSpam:SetScript("OnUpdate", OnUpdate)

--MOTD----------------------

---timer for MOTD very basic all that is needed rearly--
local function OnUpdateMOTD(self, elapsed)
	local UpdateInterval = tonumber(NuttyRecruit.db.profile.MOTDbetween)
		TimeSinceLastUpdateMOTD = TimeSinceLastUpdateMOTD + elapsed
	if TimeSinceLastUpdateMOTD and TimeSinceLastUpdateMOTD >= UpdateInterval then
		NuttyRecruit:motd()
		TimeSinceLastUpdateMOTD = 0
  end
end
--MOTD spam--
function NuttyRecruit:motd()
	if CanEditMOTD() and NuttyRecruit.db.profile.MOTD then
	MOTD = NuttyRecruit.db.profile.gmotdmessage
		GuildSetMOTD(MOTD)
	end
end
local MOTDSpam = CreateFrame("frame")
MOTDSpam:SetScript("OnUpdate", OnUpdateMOTD)


--- New Member joined welcomer----------------

--catch new member joined text--
local NewRecruitFrame = CreateFrame("Frame", nil, UIParent)
NewRecruitFrame:SetScript("OnEvent", function(_, _, msg)
    local name = string.match(msg, L["(.+) has joined the guild"])
	if NuttyRecruit.db.profile.NREnable then
		if name then
			NuttyRecruit:MemberAdded(name)
		end
	end
end)
--timer before welcome--
NewRecruitFrame:SetScript("OnUpdate", function(self, elapsed)
    if self.timing then
        self.TimeSinceLastUpdate = self.TimeSinceLastUpdate + elapsed
        if (self.TimeSinceLastUpdate > self.timetowait) then
            SendChatMessage(self.message,L["GUILD"]);
            self.timing = false
            self.TimeSinceLastUpdate = 0
        end
    end
end)
NewRecruitFrame:RegisterEvent("CHAT_MSG_SYSTEM")
NewRecruitFrame.TimeSinceLastUpdate = 0
function NuttyRecruit:MemberAdded(name)
	local newmessage = string.format(NuttyRecruit.db.profile.NRmessage, name)
	local newtime = math.random()
		newtime = newtime * 2 * NuttyRecruit.db.profile.NRbetween
		NewRecruitFrame.timing = true
		NewRecruitFrame.timetowait = newtime
		NewRecruitFrame.message = newmessage
end

-------Auto Replie and raid inviter-----------
function NuttyRecruit:findMatch(str, patterns, caseSensitive)
    local ret
    for _, pattern in pairs(patterns) do
        if caseSensitive then
            ret = {string.find(str, pattern)}
        else
            ret = {string.find(string.lower(str), pattern)}
        end
        
        if ret[1] then
            return unpack(ret)
        end
    end
    
    return nil
end
--text catcher for raid invite,guild info and auto reply--
function NuttyRecruit:AutoReplie(event, msg, sender)
if sender ~= UnitName("player") then
	msg = msg:lower()
	if self:findMatch(msg,NuttyRecruit.db.profile.guidinfoWord, false) then
		NuttyRecruit:GInfo(sender)
	elseif self:findMatch(msg,NuttyRecruit.db.profile.RaidInviteWord, false) then
		NuttyRecruit:RaidInvite(sender)
	elseif self:findMatch(msg,NuttyRecruit.db.profile.gikWord, false) then
		NuttyRecruit:Ginvite(sender)
	elseif self:findMatch(msg,NuttyRecruit.db.profile.AutoReplyOneWord, false) then
		NuttyRecruit:AutoReplyOneSend(sender)
	elseif self:findMatch(msg,NuttyRecruit.db.profile.AutoReplyTwoWord, false) then
		NuttyRecruit:AutoReplyTwoSend(sender)
	elseif self:findMatch(msg,NuttyRecruit.db.profile.AutoRThreeWord, false) then
		NuttyRecruit:AutoReplyThreeSend(sender)
	elseif self:findMatch(msg,NuttyRecruit.db.profile.AutoRFourWord, false) then
		NuttyRecruit:AutoReplyFourSend(sender)
	elseif self:findMatch(msg,NuttyRecruit.db.profile.AutoRFiveWord, false) then
		NuttyRecruit:AutoReplyFiveSend(sender)
	end
end
end
--autoreply output--
function NuttyRecruit:AutoReplyOneSend(sender)
	if NuttyRecruit.db.profile.AutoROEnable then
		timetowait = NuttyRecruit.db.profile.replytime
		NuttyRecruit:ScheduleTimer("SendAutoReplyOne", timetowait, sender)
	end
end
function NuttyRecruit:SendAutoReplyOne(sender)
	SendChatMessage(NuttyRecruit.db.profile.autoreplyonemsg, L["WHISPER"], nil, sender)
end

function NuttyRecruit:AutoReplyTwoSend(sender)
	if NuttyRecruit.db.profile.AutoRTEnable then
		timetowait = NuttyRecruit.db.profile.replytime
		NuttyRecruit:ScheduleTimer("SendAutoReplyTwo", timetowait, sender)
	end
end
function NuttyRecruit:SendAutoReplyTwo(sender)
	SendChatMessage(NuttyRecruit.db.profile.autoreplytwomsg, L["WHISPER"], nil, sender)
end

function NuttyRecruit:AutoReplyThreeSend(sender)
	if NuttyRecruit.db.profile.AutoRThreeEnable then
		timetowait = NuttyRecruit.db.profile.replytime
		NuttyRecruit:ScheduleTimer("SendAutoReplyThree", timetowait, sender)
	end
end
function NuttyRecruit:SendAutoReplyThree(sender)
	SendChatMessage( NuttyRecruit.db.profile.autoreplythreemsg, L["WHISPER"], nil, sender)
end

function NuttyRecruit:AutoReplyFourSend(sender)
	if NuttyRecruit.db.profile.AutoRFourEnable then
		timetowait = NuttyRecruit.db.profile.replytime
		NuttyRecruit:ScheduleTimer("SendAutoReplyFour", timetowait, sender)
	end
end
function NuttyRecruit:SendAutoReplyFour(sender)
	SendChatMessage( NuttyRecruit.db.profile.autoreplyfourmsg, L["WHISPER"], nil, sender)
end

function NuttyRecruit:AutoReplyFiveSend(sender)
	if NuttyRecruit.db.profile.AutoRFiveEnable then
		timetowait = NuttyRecruit.db.profile.replytime
		NuttyRecruit:ScheduleTimer("SendAutoReplyFive", timetowait, sender)
	end
end
function NuttyRecruit:SendAutoReplyFive(sender)
	SendChatMessage( NuttyRecruit.db.profile.autoreplyfivemsg, L["WHISPER"], nil, sender)
end



--invite via keyword--
function NuttyRecruit:Ginvite(sender)
	if NuttyRecruit.db.profile.gikenable then
		GuildInvite(sender)
	end
end
--auto reply to guildinfo--
function NuttyRecruit:GInfo(sender)
if NuttyRecruit.db.profile.GIEnable then
delaytime = NuttyRecruit.db.profile.delaytime
	NuttyRecruit:ScheduleTimer("SendMessageOne", delaytime, sender)
local delaytimetwo = delaytime + delaytime
	NuttyRecruit:ScheduleTimer("SendMessageTwo", delaytimetwo, sender)
local delaytimethree = delaytimetwo + delaytime
	NuttyRecruit:ScheduleTimer("SendMessageThree", delaytimethree, sender)
local delaytimefour = delaytimethree + delaytime
	NuttyRecruit:ScheduleTimer("SendMessageFour", delaytimefour, sender)
local delaytimefive = delaytimefour + delaytime
	NuttyRecruit:ScheduleTimer("SendMessageFive", delaytimefive, sender)
	end
end
--the messages to send gave them all indevidual outputs and enabling commands---
function NuttyRecruit:SendMessageOne(sender)
if NuttyRecruit.db.profile.GIRONEEnable then
	SendChatMessage(NuttyRecruit.db.profile.GIRONEmessage, L["WHISPER"], nil, sender)
	end
end
function NuttyRecruit:SendMessageTwo(sender)
if NuttyRecruit.db.profile.GIRTWOEnable then
	SendChatMessage(NuttyRecruit.db.profile.GIRTWOmessage, L["WHISPER"], nil, sender)
	end
end
function NuttyRecruit:SendMessageThree(sender)
if NuttyRecruit.db.profile.GIRTHREEEnable then
	SendChatMessage(NuttyRecruit.db.profile.GIRTHREEmessage, L["WHISPER"], nil, sender)
	end
end
function NuttyRecruit:SendMessageFour(sender)
if NuttyRecruit.db.profile.GIRFOUREnable then
	SendChatMessage(NuttyRecruit.db.profile.GIRFOURmessage, L["WHISPER"], nil, sender)
	end
end
function NuttyRecruit:SendMessageFive(sender)
if NuttyRecruit.db.profile.GIRFIVEEnable then
	SendChatMessage(NuttyRecruit.db.profile.GIRFIVEmessage, L["WHISPER"], nil, sender)
	end
end
---------raid mods---------

--raid inviter-----
function NuttyRecruit:RaidInvite(sender)
if NuttyRecruit.db.profile.INVITEEnable then
	InviteUnit(sender)
	end
end
--auto convert to raid option----
function NuttyRecruit:ConvertToRaidCheck()
 if not UnitInRaid("player") then
	if(IsPartyLeader()) and (GetNumPartyMembers() > 2) then
		if NuttyRecruit.db.profile.RCONVERTEnable then
			NuttyRecruit:Print(L["Converting to a Raid"])
			ConvertToRaid();
		end
	end
 end
end
--/rc ready check-----------------
function NuttyRecruit:DoReadyCheck()
	if NuttyRecruit.db.profile.readycheckoptions then
	if (UnitInRaid("player") ~= nil) and (GetRealNumRaidMembers() > 0) then

		if IsRaidLeader() or IsRaidOfficer() then
			DoReadyCheck()
		else
			NuttyRecruit:Print(L["You are not a raid leader or have raid assist, and cannot perform a ready check."])
		end
	elseif (UnitInParty("player") ~= nil) and (GetRealNumPartyMembers() > 0) then
		if UnitIsPartyLeader("player") then
			DoReadyCheck()		
		else
			NuttyRecruit:Print(L["You are not the party leader and cannot perform a ready check."])
		end
	else
		NuttyRecruit:Print(L["Not in a group"])
	end
	end
end
---raid roll--------------
function NuttyRecruit:RaidRoll(msg)
if NuttyRecruit.db.profile.raidrolloptions then
	ItemLink = string.match(strtrim(msg), "^\124c%x+\124Hitem[:%-%d]+\124h%[[^\124]+%]\124h\124r")
	ItemLink = (ItemLink == nil) and "" or (" " .. ItemLink)
	NameArray = { }
	NameCount = 0

	if (GetNumRaidMembers() > 0) then
	
		for i = 1, 40 do
			local name = GetRaidRosterInfo(i);
			if (name ~= nil) then
			NameCount = NameCount + 1
				NameArray[NameCount] = name
			end
		end
		
	elseif (GetNumPartyMembers() > 0) then

		NameCount = 1
		NameArray[1] = UnitName("player")
		for i = 1, GetNumPartyMembers() do
			NameCount = NameCount + 1
			NameArray[NameCount] = UnitName("party" .. i)
		end

	else
		NuttyRecruit:Print(L["Not in a group"])
	end
	
	if (NameCount > 0) then
	
		ChatType = GetNumRaidMembers() > 0 and "RAID" or "PARTY"
		
		SendChatMessage(L["RAID ROLLING"] .. ItemLink, ChatType, nil, nil)
	
		local namesPerLine = 5
	
		for line = 1, math.floor((NameCount + namesPerLine - 1) / namesPerLine) do
			local message = ""
			local index = 1 + (line - 1) * namesPerLine
			for i = index, math.min(NameCount, index + namesPerLine - 1) do
				message = message .. " " .. i .. "-" .. NameArray[i]
			end
			message = strtrim(message)
			
			SendChatMessage(message, ChatType, nil, nil)
		end
		
		NuttyRecruit:ScheduleTimer("RaidRolling", 0.5, NameCount)
	
	end
end
end
function NuttyRecruit:RaidRolling(NameCount)
RandomRoll(1, NameCount)
end

--disband raid---
 function NuttyRecruit:Disband()
	if NuttyRecruit.db.profile.disbandraidenable then
		if GetRealNumRaidMembers() > 0 and IsRaidLeader() then --check in a raid and am raid leader
                for n=1,GetNumRaidMembers() do
                    local name , rank, subgroup, level, class, fileName, zone = GetRaidRosterInfo(n);
                    if name ~= UnitName("Player") then -- make shure i dont try and remove myself
                        UninviteUnit(name);
                    end
				end
		end
	end
end


--roll tracker----------------------------------

--show tracker--
function NuttyRecruit:ShowRollTracker()
	if NuttyRecruit.db.profile.rolltrackeroptions then
	RTMainFrame:Show()
	end
end
--catch rolls--
function NuttyRecruit:RollCatcher(event, msg)
	if NuttyRecruit.db.profile.rolltrackeroptions then
		local player,rolled,low,high = df(msg, RANDOM_ROLL_RESULT)
		if player then
			table.insert(rollArray, {
				player = player,
				rolled = tonumber(rolled),
				low = tonumber(low),
				high = tonumber(high),
				})
			NuttyRecruit:UpdateList(player,rolled,low,high,msg)
		end
	end
end
-- Sort the list
local function frametest_Sort(a, b)
	return a.rolled < b.rolled
end
function NuttyRecruit:frameupdate()
RTMainFrame:SetPoint(NuttyRecruit.db.profile.rtframe.position.p, UIParent, NuttyRecruit.db.profile.rtframe.position.r, NuttyRecruit.db.profile.rtframe.position.x, NuttyRecruit.db.profile.rtframe.position.y)
end
--update the list and send it to the roll tracker--
function NuttyRecruit:UpdateList(player,rolled,low,high,msg)
	local rollText = ""
	
	table.sort(rollArray, frametest_Sort)
	
	for i, rolled in pairs(rollArray) do
	local tied = (rollArray[i + 1] and rolled.rolled == rollArray[i + 1].rolled) or (rollArray[i - 1] and rolled.rolled == rollArray[i - 1].rolled)
		rollText = string.format("|c%s%d|r: |c%s%s|r\n",
				tied and "ff00FF00" or "ffffffff",
				rolled.rolled,
				((rolled.low ~= 1 or rolled.high ~= 100)) and  "fffF0000" or "ffffffff",
				rolled.player,
				(rolled.low ~= 1 or rolled.high ~= 100) and format(" (%d-%d)", rolled.low, rolled.high) or "") .. rollText
	end
	NuttyRecruit:ShowRollTracker()
	RTeditBox:SetText(rollText)
end
---load frame posistion--
function NuttyRecruit:LoadFramePosition()
	RTMainFrame:ClearAllPoints()
	if (NuttyRecruit.db.profile.x ~= 0) or (NuttyRecruit.db.profile.y ~= 0) then
		RTMainFrame:SetPoint("TOPLEFT", UIParent,"BOTTOMLEFT", NuttyRecruit.db.profile.x, NuttyRecruit.db.profile.y)
	else
		RTMainFrame:SetPoint("CENTER", UIParent, "CENTER", 0, 0)
	end
end
--load frame posistion when changing profile--	
function NuttyRecruit:OnProfileChanged()
	NuttyRecruit:LoadFramePosition()
end
--save frame location--
function NuttyRecruit:SaveFramePosition()
	NuttyRecruit.db.profile.x = RTMainFrame:GetLeft()
	NuttyRecruit.db.profile.y = RTMainFrame:GetTop()
end
--change text on roll tracker frame and show the frame when player selects unlock frame in options--
function NuttyRecruit:ChangeRollTrackerText()
NuttyRecruit:ShowRollTracker()
if NuttyRecruit.db.profile.lock then
	Fontstring1:SetText(L["Roll Tracker UNLOCKED"])
else 
	Fontstring1:SetText(L["Roll Tracker"])
	end
end
--clear rolls--
function NuttyRecruit:ClearRolls()
	if NuttyRecruit.db.profile.rolltrackerclearoptions then
		rollArray = {} 
		rollNames = {}
		NuttyRecruit:UpdateList()
	end
end
--show hide tracker--
function NuttyRecruit:RollTrackerShowHide()
	if NuttyRecruit.db.profile.rolltrackeroptions then
		RTMainFrame:Show()
	else
		RTMainFrame:Hide()
	end
end

------------- Frames for roll tracker ------

-----Main frame-----------
local    RTMainFrame = CreateFrame("Frame", "RTMainFrame", UIParent)
	RTMainFrame:RegisterEvent("ADDON_LOADED")
	RTMainFrame:SetScript("OnEvent", function(self, event, addon)
	if addon == "NuttyRecruit" then
    RTMainFrame.BackDrop = {bgFile = "Interface\\TutorialFrame\\TutorialFrameBackground", 
        edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Border", 
        insets = { 
                left = 4,
                right = 4,
                top = 4,
                bottom = 4
            }
        }
    RTMainFrame:SetBackdrop(RTMainFrame.BackDrop)
    RTMainFrame:SetBackdropColor(0, 0, 0, 1)
    RTMainFrame:SetHeight(216)
    RTMainFrame:SetWidth(180)
	RTMainFrame:SetFrameStrata("HIGH")
    RTMainFrame:SetClampedToScreen(true)
    RTMainFrame:SetMovable(true)
	RTMainFrame:ClearAllPoints()
	RTMainFrame:SetPoint("CENTER", UIParent, "CENTER", 0, 0)
	RTMainFrame:SetScript("OnMouseDown",function(self, button)
		if ( button == "LeftButton" ) then
			if NuttyRecruit.db.profile.lock then
						self:StartMoving()
				end
		end
	end)
	RTMainFrame:SetScript("OnMouseUp",function(self, button)
		if ( button == "LeftButton" ) then
			self:StopMovingOrSizing()
			NuttyRecruit:SaveFramePosition()
		end
	end)
	RTMainFrame:SetScript("OnHide",function(self) self:StopMovingOrSizing() end)
	RTMainFrame:SetClampedToScreen(true)
	NuttyRecruit:LoadFramePosition()
	end
	end)
	--ROLL TRACKER TEXT AT TOP OF BOX----
	Fontstring1 = RTMainFrame:CreateFontString("Fontstring1","OVERLAY",RTMainFrame)
    Fontstring1:SetFont("Fonts\\FRIZQT__.TTF",10)
    Fontstring1:SetPoint("TOPLEFT", 15, -15)
    Fontstring1:SetText(L["Roll Tracker"])
    Fontstring1:SetJustifyH("LEFT")
	--CLEAR BUTTON--  
    RTMainFrame.Frame1Button1 = CreateFrame("Button", "Frame1Frame1Button1", RTMainFrame)
    RTMainFrame.Frame1Button1:SetHeight(25)
    RTMainFrame.Frame1Button1:SetWidth(100)
    RTMainFrame.Frame1Button1:SetPoint("BOTTOMLEFT", 15,15)
    RTMainFrame.Frame1Button1:SetNormalTexture("Interface\\Buttons\\UI-Panel-Button-Up")
    RTMainFrame.Frame1Button1:SetPushedTexture("Interface\\Buttons\\UI-Panel-Button-Down")
    RTMainFrame.Frame1Button1:SetHighlightTexture("Interface\\Buttons\\UI-Panel-Button-Highlight")
    RTMainFrame.Frame1Button1:SetDisabledTexture("Interface\\Buttons\\UI-Panel-Button-Disabled")
	RTMainFrame.Frame1Button1:SetScript("OnClick",function() rollArray = {} rollNames = {} NuttyRecruit:UpdateList() end) -- clears the table and updates it
	--CLEAR TEXT--
	Fontstring2 = RTMainFrame.Frame1Button1:CreateFontString("Fontstring2","OVERLAY",RTMainFrame.Frame1Button1)
    Fontstring2:SetFont("Fonts\\FRIZQT__.TTF",10)
    Fontstring2:SetPoint("BOTTOMLEFT", RTMainFrame.Frame1Button1, "BOTTOMLEFT", 15,10)
    Fontstring2:SetText(L["Clear"]) --loc
	--close button--
    RTMainFrame.Frame1Button2 = CreateFrame("Button", "Frame1Frame1Button2", RTMainFrame)
    RTMainFrame.Frame1Button2:SetHeight(25)
    RTMainFrame.Frame1Button2:SetWidth(25)
    RTMainFrame.Frame1Button2:SetPoint("TOPRIGHT",RTMainFrame,"TOPRIGHT",0,0)
    RTMainFrame.Frame1Button2:SetNormalTexture("Interface\\Buttons\\UI-Panel-MinimizeButton-Up.blp")
    RTMainFrame.Frame1Button2:SetPushedTexture("Interface\\Buttons\\UI-Panel-MinimizeButton-Down.blp")
    RTMainFrame.Frame1Button2:SetHighlightTexture("Interface\\Buttons\\UI-Panel-MinimizeButton-Highlight.blp")
	RTMainFrame.Frame1Button2:SetScript("OnClick",function(this)  NuttyRecruit:ClearRolls() this:GetParent():Hide()end) -- closes the frame
	--scroll frame---
 	RTScroll = CreateFrame("ScrollFrame", "RTScroll", RTMainFrame, "UIPanelScrollFrameTemplate")
	RTScroll:SetPoint("TOPLEFT", RTMainFrame, "TOPLEFT", 15, -30)
	RTScroll:SetPoint("BOTTOMRIGHT", RTMainFrame, "BOTTOMRIGHT", -35, 45)
	--data--
	RTeditBox = CreateFrame("EditBox", "RTeditBox", RTMainFrame)
	RTeditBox:SetMultiLine(true)
	RTeditBox:SetMaxLetters(99999)
	RTeditBox:EnableMouse(true)
	RTeditBox:SetAutoFocus(false)
	RTeditBox:SetFontObject(ChatFontNormal)
	RTeditBox:SetWidth(130)
	RTeditBox:SetHeight(800)
	RTScroll:SetScrollChild(RTeditBox)
---spammer all spams have individual options gives user more flexability---

--spam one---
function NuttyRecruit:SpamOne()
	if NuttyRecruit.db.profile.enablespamone then
		NuttyRecruit:SpamOneRaidParty()
		NuttyRecruit:SpamOneGuild()
	end
end
function NuttyRecruit:SpamOneRaidParty()
	if NuttyRecruit.db.profile.enablespamonepartyraid then
		if GetRealNumRaidMembers() > 0 then
		SendChatMessage(NuttyRecruit.db.profile.spamonemessage, L["RAID"])
		elseif GetRealNumPartyMembers() > 0 then
		SendChatMessage(NuttyRecruit.db.profile.spamonemessage, L["PARTY"])
		end
	end
end
function NuttyRecruit:SpamOneGuild()
	if NuttyRecruit.db.profile.enablespamoneguild then
		SendChatMessage(NuttyRecruit.db.profile.spamonemessage, L["GUILD"])
	end
end
---spam two---
function NuttyRecruit:SpamTwo()
	if NuttyRecruit.db.profile.enablespamtwo then
		NuttyRecruit:SpamTwoRaidParty()
		NuttyRecruit:SpamTwoGuild()
	end
end
function NuttyRecruit:SpamTwoRaidParty()
if NuttyRecruit.db.profile.enablespamtwopartyraid then
	if GetRealNumRaidMembers() > 0 then
		SendChatMessage(NuttyRecruit.db.profile.spamtwomessage, L["RAID"])
		elseif GetRealNumPartyMembers() > 0 then
		SendChatMessage(NuttyRecruit.db.profile.spamtwomessage, L["PARTY"])
		end
	end
end
function NuttyRecruit:SpamTwoGuild()
	if NuttyRecruit.db.profile.enablespamtwoguild then
		SendChatMessage(NuttyRecruit.db.profile.spamtwomessage, L["GUILD"])
	end
end
--spam three---
function NuttyRecruit:SpamThree()
	if NuttyRecruit.db.profile.enablespamthree then
		NuttyRecruit:SpamThreeRaidParty()
		NuttyRecruit:SpamThreeGuild()
	end
end
function NuttyRecruit:SpamThreeRaidParty()
if NuttyRecruit.db.profile.enablespamthreepartyraid then
	if GetRealNumRaidMembers() > 0 then
		SendChatMessage(NuttyRecruit.db.profile.spamthreemessage, L["RAID"])
		elseif GetRealNumPartyMembers() > 0 then
		SendChatMessage(NuttyRecruit.db.profile.spamthreemessage, L["PARTY"])
		end
	end
end
function NuttyRecruit:SpamThreeGuild()
	if NuttyRecruit.db.profile.enablespamthreeguild then
		SendChatMessage(NuttyRecruit.db.profile.spamthreemessage, L["GUILD"])
	end
end
---spam four---
function NuttyRecruit:SpamFour()
	if NuttyRecruit.db.profile.enablespamfour then
		NuttyRecruit:SpamFourRaidParty()
		NuttyRecruit:SpamFourGuild()
	end
end
function NuttyRecruit:SpamFourRaidParty()
if NuttyRecruit.db.profile.enablespamfourpartyraid then
	if GetRealNumRaidMembers() > 0 then
		SendChatMessage(NuttyRecruit.db.profile.spamfourmessage, L["RAID"])
		elseif GetRealNumPartyMembers() > 0 then
		SendChatMessage(NuttyRecruit.db.profile.spamfourmessage, L["PARTY"])
		end
	end
end
function NuttyRecruit:SpamFourGuild()
	if NuttyRecruit.db.profile.enablespamfourguild then
		SendChatMessage(NuttyRecruit.db.profile.spamfourmessage, L["GUILD"])
	end
end