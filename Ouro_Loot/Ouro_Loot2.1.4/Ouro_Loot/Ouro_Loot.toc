## Interface: 30300
## Title: Ouro Loot
## Version: 2.14
## Notes: Raid loot tracking
# oldDependencies: lib-st
## OptionalDeps: DBM-Core
## SavedVariables: sv_OLoot, sv_OLoot_opts

# Ace3 r958
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
Libs\AceAddon-3.0\AceAddon-3.0.xml
Libs\AceEvent-3.0\AceEvent-3.0.xml
Libs\AceTimer-3.0\AceTimer-3.0.xml
Libs\AceConsole-3.0\AceConsole-3.0.xml
Libs\AceGUI-3.0\AceGUI-3.0.xml
Libs\AceComm-3.0\AceComm-3.0.xml

#Libs\lib-st-Core-r118.lua
Libs\lib-st-Core-r137v38.lua

# sigh
AceGUIWidget-MultiLineEditBoxPreviousAPI.lua

# extra bits
AceGUIWidget-Spacer.lua
AceGUIWidget-lib-st.lua
AceGUIWidget-DoTimerEditBoxDropDown.lua

lootgui.lua
verbage.lua
text_tabs.lua
abbreviations.lua

mleqdkp.lua

