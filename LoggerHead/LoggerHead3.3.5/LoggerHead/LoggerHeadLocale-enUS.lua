local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local L = LibStub("AceLocale-3.0"):NewLocale("LoggerHead", "enUS", true, debug)

L["Click to toggle combat logging"] = true
L["Disabled"] = true
L["Enable Chat Logging"] = true
L["Enable Chat Logging whenever the Combat Log is enabled"] = true
L["Enabled"] = true
L["Enable Transcriptor Logging whenever the Combat Log is enabled"] = true
L["Enable Transcriptor Support"] = true
L["Instances"] = true
L["Output"] = true
L["Profiles"] = true
L["Prompt on new zone?"] = true
L["Prompt when entering a new zone?"] = true
L["Right-click to open the options menu"] = true
L["Show minimap icon"] = true
L["Toggle showing or hiding the minimap icon."] = true
L["Unclassified"] = true
L["You have entered |cffd9d919%s|r. Enable logging for this area?"] = true
L["Zones"] = true


