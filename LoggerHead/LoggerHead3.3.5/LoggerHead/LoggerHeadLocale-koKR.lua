local L = LibStub("AceLocale-3.0"):NewLocale("LoggerHead", "koKR")
if not L then return end

L["Click to toggle combat logging"] = "클릭: 전투 로그 토글"
L["Disabled"] = "|cffff0000불가능|r"
L["Enable Chat Logging"] = "채팅 로그 활성화"
L["Enable Chat Logging whenever the Combat Log is enabled"] = "전투 로그가 활성화 될때 채팅로그 활성화"
L["Enabled"] = "|cff00ff00가능|r"
L["Enable Transcriptor Logging whenever the Combat Log is enabled"] = "전투 로그가 활성화 될때 Transcriptor 로깅 활성화"
L["Enable Transcriptor Support"] = "Transcriptor 지원 활성화"
L["Instances"] = "인스턴스"
L["Output"] = "출력"
L["Profiles"] = "프로파일"
L["Prompt on new zone?"] = "새로운 지역 바로 기록"
L["Prompt when entering a new zone?"] = "새로운 지역에 들어서면 로그 기록을 바로 시작하시겠습니까?"
L["Right-click to open the options menu"] = "옵션메뉴를 열려면 우클릭"
L["Show minimap icon"] = "미니맵 아이콘 보기"
L["Toggle showing or hiding the minimap icon."] = "미니맵 아이콘 토글"
L["Unclassified"] = "미분류"
L["You have entered |cffd9d919%s|r. Enable logging for this area?"] = "\"|cffd9d919%s.|r\"에 들어섰습니다. 이 지역(인스턴스던전)에 대한 로그를 파일로 기록하시겠습니까?"
L["Zones"] = "지역"

