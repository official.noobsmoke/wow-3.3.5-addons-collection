﻿local L = LibStub("AceLocale-3.0"):NewLocale("LoggerHead", "frFR")
if not L then return end

L["Click to toggle combat logging"] = "Cliquer pour activer/désactiver le log de combat"
L["Disabled"] = "Désactivé"
L["Enable Chat Logging"] = "Activer le log de chat"
L["Enable Chat Logging whenever the Combat Log is enabled"] = "Activer le log de chat dès que le log de combat est activé"
L["Enabled"] = "Activé"
L["Enable Transcriptor Logging whenever the Combat Log is enabled"] = "Activer la prise en charge de Transcriptor chaque fois que le journal de combat est activé"
L["Enable Transcriptor Support"] = "Activer la prise en charge de Transcriptor"
L["Instances"] = true
L["Output"] = "Sortie"
L["Profiles"] = "Profils"
L["Prompt on new zone?"] = "Rappeler dans une nouvelle zone ?"
L["Prompt when entering a new zone?"] = "Rappeler en entrant dans une nouvelle zone ?"
L["Right-click to open the options menu"] = "Clic droit pour ouvrir le menu des options"
L["Show minimap icon"] = "Montrer l'icône sur la mini-carte"
L["Toggle showing or hiding the minimap icon."] = "Activer/désactiver l'affichage de l'icône sur la mini-carte"
L["Unclassified"] = "Non classifié"
L["You have entered |cffd9d919%s|r. Enable logging for this area?"] = "Vous êtes entré dans |cffd9d919%s.|r Voulez-vous activer les logs pour cette zone/instance ?"
L["Zones"] = true


