﻿## Interface: 30300
## Title: LoggerHead
## Notes: Automatically turns on the combat log for selected instances
## Notes-ruRU: Автоматически включает запись лога боя в выбранных подземельях
## Author: Sano
## X-eMail: rsmozang@gmail.com
## X-Category: Miscellaneous
## X-Embeds: Ace3, LibTourist-3.0, LibSink-2.0, LibBabble-Zone-3.0
## Version: 3.3.5.106
## OptionalDeps: Ace3, LibTourist-3.0, LibBabble-Zone-3.0, LibSink-2.0
## SavedVariables: LoggerHeadDB
## LoadManagers: AddonLoader
## X-LoadOn-Always: delayed
## X-Curse-Packaged-Version: Release 3.3.107
## X-Curse-Project-Name: LoggerHead
## X-Curse-Project-ID: loggerhead
## X-Curse-Repository-ID: wow/loggerhead/mainline

Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
Libs\LibDataBroker-1.1\libDataBroker-1.1.lua

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

LoggerHeadLocale-enUS.lua
LoggerHeadLocale-frFR.lua
LoggerHeadLocale-zhTW.lua
LoggerHeadLocale-zhCN.lua
LoggerHeadLocale-koKR.lua
LoggerHeadLocale-ruRU.lua

Loggerhead.lua
