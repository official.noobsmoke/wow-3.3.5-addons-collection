﻿local L = LibStub("AceLocale-3.0"):NewLocale("LoggerHead", "ruRU")
if not L then return end

L["Click to toggle combat logging"] = "Кликните для включения/отключения записи журнала боя"
L["Disabled"] = "Выключено"
L["Enable Chat Logging"] = "Включить запись чата"
L["Enable Chat Logging whenever the Combat Log is enabled"] = "Включать запись чата всякий раз, когда включен журнал боя"
L["Enabled"] = "Включено"
L["Enable Transcriptor Logging whenever the Combat Log is enabled"] = "Включить запись логов в Transcriptor'е, когда включен журнал боя"
L["Enable Transcriptor Support"] = "Включить поддержку Transcriptor'а"
L["Instances"] = "Подземелья"
L["Output"] = "Вывод"
L["Profiles"] = "Профили"
L["Prompt on new zone?"] = "Уведомлять о новой локации?"
L["Prompt when entering a new zone?"] = "Уведомлять при входе в новую локацию?"
L["Right-click to open the options menu"] = "Клик правой кнопкой для открытия меню опций"
L["Show minimap icon"] = "Отображать иконку у мини-карты"
L["Toggle showing or hiding the minimap icon."] = "Вкл/Откл отображение иконки у мини-карты."
L["Unclassified"] = "Не классифицировано"
L["You have entered |cffd9d919%s|r. Enable logging for this area?"] = "Вы вошли в |cffd9d919%s.|r Хотите включить запись для этой локации?"
L["Zones"] = "Локации"

