local L = LibStub("AceLocale-3.0"):NewLocale("LoggerHead", "zhTW")
if not L then return end

L["Click to toggle combat logging"] = "點擊開啟/關閉記錄戰斗日志"
L["Disabled"] = "未記錄"
L["Enable Chat Logging"] = "啟用聊天紀錄"
L["Enable Chat Logging whenever the Combat Log is enabled"] = "無論戰斗紀錄是否啟用都啟用聊天紀錄"
L["Enabled"] = "記錄中"
L["Enable Transcriptor Logging whenever the Combat Log is enabled"] = "無論戰斗記錄是否啟用都開啟Transcriptor記錄"
L["Enable Transcriptor Support"] = "啟用 Transcriptor 支持"
L["Instances"] = "副本"
L["Output"] = "輸出提示"
L["Profiles"] = "配置文件"
L["Prompt on new zone?"] = "切換地區時詢問?"
L["Prompt when entering a new zone?"] = "切換地區時詢問是否記錄戰斗日志?"
L["Right-click to open the options menu"] = "右鍵點擊打開選項菜單"
L["Show minimap icon"] = "顯示小地圖圖標"
L["Toggle showing or hiding the minimap icon."] = "顯示或隱藏小地圖圖標."
L["Unclassified"] = "未知區域"
L["You have entered |cffd9d919%s|r. Enable logging for this area?"] = "你已經進入 |cffd9d919%s.|r 你想要為此地區/副本記錄戰斗日志嗎？"
L["Zones"] = "區域"

