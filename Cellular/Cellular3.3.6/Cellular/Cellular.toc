## Interface: 30300
## Title: Cellular
## Notes: Instant messenger for whispers
## Author: TotalPackage
## Version: 3.3.006

## SavedVariables: CellularDB
## SavedVariablesPerCharacter: CellularCharDB, Cellular_History

## OptionalDeps: LibSharedMedia-3.0
## X-Category: Chat/Communication

## LoadManagers: AddonLoader
## X-LoadOn-Always: true

embeds.xml
core.lua
