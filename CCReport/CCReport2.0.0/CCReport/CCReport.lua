local CCReport = CreateFrame("Frame")
CCReport.playername = UnitName("player")
local enabled = 'true';
SLASH_CCR1 = '/CCR';
SLASH_CCR2 = '/CCReport';
local function SlashCmd(cmd,self)
	if (cmd == 'enable') then
		enabled = 'true';
		DEFAULT_CHAT_FRAME:AddMessage("CCReport Enabled",0,1,0);
        elseif (cmd == 'disable') then
                enabled = 'false';
		DEFAULT_CHAT_FRAME:AddMessage("CCReport Disabled",1,0,0);
        else
                DEFAULT_CHAT_FRAME:AddMessage("Unknown command. Enter either '/ccr enable' to activate CCReport, or '/ccr disable' to deactivate it.",0,0,1);
        end
end
SlashCmdList["CCR"] = SlashCmd;

CCReport:SetScript("OnEvent",function()
------------------------------------------
--					--
--		Crowd Controls		--
--					--
------------------------------------------
if (enabled == 'true') then
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 51724 or arg9 == 11297 or arg9 == 2070 or arg9 ==  6770))
	then
		SendChatMessage("{skull}SAPPED{skull}", "PARTY")
	end
--Sap^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 51724 or arg9 == 11297 or arg9 == 2070 or arg9 ==  6770))
	then
		SendChatMessage("{skull}SAP REFRESHED{skull}", "PARTY")
	end
--Sap (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 5782 or arg9 == 6213 or arg9 == 6215))
	then
		SendChatMessage("{skull}FEARED{skull}", "PARTY")
	end
--Fear^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 5782 or arg9 == 6213 or arg9 == 6215))
	then
		SendChatMessage("{skull}FEAR REFRESHED{skull}", "PARTY")
	end
--Fear (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 188 or arg9 == 12824 or arg9 == 12825 or arg9 ==  12826 or arg9 == 61305 or arg9 == 28272 or arg9 == 61721 or arg9 == 61780 or arg9 == 28271))
	then
		SendChatMessage("{skull}POLY'D{skull}", "PARTY")
	end
--Polymorph^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 188 or arg9 == 12824 or arg9 == 12825 or arg9 ==  12826 or arg9 == 61305 or arg9 == 28272 or arg9 == 61721 or arg9 == 61780 or arg9 == 28271))
	then
		SendChatMessage("{skull}POLY REFRESHED{skull}", "PARTY")
	end
--Polymorph (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 51514))
	then
		SendChatMessage("{skull}HEXED{skull}", "PARTY")
	end
--Hex^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 2094))
	then
		SendChatMessage("{skull}BLINDED{skull}", "PARTY")
	end
--Blind^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 6358))
	then
		SendChatMessage("{skull}SEDUCED{skull}", "PARTY")
	end
--Seduction (Succubus)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 6358))
	then
		SendChatMessage("{skull}SEDUCE REFRESHED{skull}", "PARTY")
	end
--Seduction (Succubus) (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 33786))
	then
		SendChatMessage("{skull}CYCLONED{skull}", "PARTY")
	end
--Cyclone^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 605))
	then
		SendChatMessage("{skull}MIND CONTROLLED{skull}", "PARTY")
	end
--Mind Control^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 8122 or arg9 == 8124 or arg9 == 10888 or arg9 == 10890))
	then
		SendChatMessage("{skull}FEARED{skull}", "PARTY")
	end
--Psychic Scream^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 20511))
	then
		SendChatMessage("{skull}FEARED{skull}", "PARTY")
	end
--Intimidating Shout^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 20066))
	then
		SendChatMessage("{skull}REPENTANCED{skull}", "PARTY")
	end
--Repentance^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 14309 or arg9 == 60210))
	then
		SendChatMessage("{skull}FROZEN{skull}", "PARTY")
	end
--Freezing Trap / Arrow^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 710 or arg9 == 18647))
	then
		SendChatMessage("{skull}BANISHED{skull}", "PARTY")
	end
--Banish^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 710 or arg9 == 18647))
	then
		SendChatMessage("{skull}BANISH REFRESHED{skull}", "PARTY")
	end
--Banish (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 2637 or arg9 == 18657 or arg9 == 18658))
	then
		SendChatMessage("{skull}HIBERNATED{skull}", "PARTY")
	end
--Hibernate^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 2637 or arg9 == 18657 or arg9 == 18658))
	then
		SendChatMessage("{skull}HIBERNATE REFRESHED{skull}", "PARTY")
	end
--Hibernate (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 51209))
	then
		SendChatMessage("{skull}FROZEN{skull}", "PARTY")
	end
--Hungering Cold^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 47476))
	then
		SendChatMessage("{skull}SILENCED{skull}", "PARTY")
	end
--Strangulate^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 5211))
	then
		SendChatMessage("{skull}BASHED{skull}", "PARTY")
	end
--Bash^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 53308))
	then
		SendChatMessage("{skull}ROOTED{skull}", "PARTY")
	end
--Entangling Roots^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 1513))
	then
		SendChatMessage("{skull}FEARED{skull}", "PARTY")
	end
--Scare Beast^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 1513))
	then
		SendChatMessage("{skull}FEAR REFRESHED{skull}", "PARTY")
	end
--Scare Beast (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 49012))
	then
		SendChatMessage("{skull}STUNNED{skull}", "PARTY")
	end
--Wyvern Sting^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 10308))
	then
		SendChatMessage("{skull}HoJ'd{skull}", "PARTY")
	end
--Hammer Of Justice^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 10326))
	then
		SendChatMessage("{skull}FEARED{skull}", "PARTY")
	end
--Turn Evil^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 10326))
	then
		SendChatMessage("{skull}FEAR REFRESHED{skull}", "PARTY")
	end
--Turn Evil (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 10955))
	then
		SendChatMessage("{skull}SHACKLED{skull}", "PARTY")
	end
--Shackle Undead^
	if ((arg7 == CCReport.playername)
	and ( arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 10955))
	then
		SendChatMessage("{skull}SHACKLE REFRESHED{skull}", "PARTY")
	end
--Shackle Undead (Refreshed)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 58582))
	then
		SendChatMessage("{skull}I JUST HIT STONECLAW TOTEM /PALM{skull}", "PARTY")
	end
--Stoneclaw Stun^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 17928))
	then
		SendChatMessage("{skull}FEARED{skull}", "PARTY")
	end
--Howl Of Terror^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 42917 or arg9 == 33395))
	then
		SendChatMessage("{skull}FROZEN{skull}", "PARTY")
	end
--Frost Nova / Water Elemental Freeze^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 44572))
	then
		SendChatMessage("{skull}FROZEN{skull}", "PARTY")
	end
--Deep Freeze^
------------------------------------------
--					--
--		Silences		--
--					--
------------------------------------------
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 34490))
	then
		SendChatMessage("{skull}SILENCED{skull}", "PARTY")
	end
--Silencing Shot^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 55021 or arg9 == 18469))
	then
		SendChatMessage("{skull}SILENCED{skull}", "PARTY")
	end
--Counterspell^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 18425))
	then
		SendChatMessage("{skull}SILENCED{skull}", "PARTY")
	end
--Kick^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 1330))
	then
		SendChatMessage("{skull}SILENCED{skull}", "PARTY")
	end
--Garrote^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 63529))
	then
		SendChatMessage("{skull}SILENCED{skull}", "PARTY")
	end
--Silence (Priest)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 24259))
	then
		SendChatMessage("{skull}SILENCED{skull}", "PARTY")
	end
--Spell Lock (Fel Hunter)^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 18498))
	then
		SendChatMessage("{skull}SILENCED{skull}", "PARTY")
	end
--Silence (Warrior)^
------------------------------------------
--					--
--		Warlock			--
--					--
------------------------------------------
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED")
	and (arg9 == 30108 or arg9 == 30404 or arg9 == 30405 or arg9 == 47841 or arg9 == 47843))
	then
		SendChatMessage("{cross}UA APPLIED{cross}", "PARTY")
	end
--Unstable Affliction Applied^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 30108 or arg9 == 30404 or arg9 == 30405 or arg9 == 47841 or arg9 == 47843))
	then
		SendChatMessage("{cross}UA REMOVED{cross}", "PARTY")
	end
--Unstable Affliction Removed^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 30108 or arg9 == 30404 or arg9 == 30405 or arg9 == 47841 or arg9 == 47843))
	then
		SendChatMessage("{cross}UA REFRESHED{cross}", "PARTY")
	end
--Unstable Affliction Refreshed^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 31117 or arg9 == 43523 or arg9 == 65813))
	then
		SendChatMessage("{cross}I JUST DISPELLED UNSTABLE AFFLICTION /PALM{cross}", "PARTY")
	end
--Unstable Affliction Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_APPLIED" or arg2 == "SPELL_AURA_REFRESH")
	and (arg9 == 47811))
	then
		SendChatMessage("{circle}IMMOLATED{circle}", "PARTY")
	end
--Immolate^
------------------------------------------
--					--
--		Dispels			--
--					--
------------------------------------------
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 20217))
	then
		SendChatMessage("{triangle}KINGS - REMOVED{triangle}", "PARTY")
	end
--Blessing of Kings Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 48936))
	then
		SendChatMessage("{triangle}WISDOM - REMOVED{triangle}", "PARTY")
	end
--Blessing of Wisdom Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 48932))
	then
		SendChatMessage("{triangle}MIGHT - REMOVED{triangle}", "PARTY")
	end
--Blessing of Might Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 53307))
	then
		SendChatMessage("{triangle}THORNS - REMOVED{triangle}", "PARTY")
	end
--Thorns Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 48469))
	then
		SendChatMessage("{triangle}MotW - REMOVED{triangle}", "PARTY")
	end
--Mark of the Wild Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 48043))
	then
		SendChatMessage("{triangle}SPIRIT - REMOVED{triangle}", "PARTY")
	end
--Prayer of Spirit Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 48169))
	then
		SendChatMessage("{triangle}SHADOW PROTECTION - REMOVED{triangle}", "PARTY")
	end
--Shadow Protection Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 42995 or arg9 == 61024))
	then
		SendChatMessage("{triangle}INTELLECT - REMOVED{triangle}", "PARTY")
	end
--Arcane Intellect / Dalaran Intellect Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 48161))
	then
		SendChatMessage("{triangle}FORTITUDE - REMOVED{triangle}", "PARTY")
	end
--Power Word: Fortitude Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 54646))
	then
		SendChatMessage("{triangle}FOCUS MAGIC - REMOVED{triangle}", "PARTY")
	end
--Focus Magic Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 20911))
	then
		SendChatMessage("{triangle}SANCTUARY - REMOVED{triangle}", "PARTY")
	end
--Blessing of Sanctuary Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 53563))
	then
		SendChatMessage("{triangle}BEACON - REMOVED{triangle}", "PARTY")
	end
--Beacon of Light Dispelled^
	if ((arg7 == CCReport.playername)
	and (arg2 == "SPELL_AURA_REMOVED")
	and (arg9 == 25780))
	then
		SendChatMessage("{triangle}RIGHTEOUS FURY - REMOVED{triangle}", "PARTY")
	end
--Righteous Fury Dispelled^
end
end)


CCReport:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
DEFAULT_CHAT_FRAME:AddMessage("CCReport loaded - Sup bro.",1,0,0)