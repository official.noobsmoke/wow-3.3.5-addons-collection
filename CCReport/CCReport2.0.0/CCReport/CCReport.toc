## Interface: 30300
## Title: C|cFFFF8080C|cFFFFFF80R|cFF80FF80e|cFF8080FFp|cFFFF8080o|cFFFFFF80r|cFF80FF80t
## Version: 2.0
## Notes: A small simple and compact addon that prints a message in party chat letting your party know you've been CC'd and in what way, such as Polymorph, Fear, Hex, Mind Control, Silence, Roots etc.
## Author: Teliko

CCReport.lua
