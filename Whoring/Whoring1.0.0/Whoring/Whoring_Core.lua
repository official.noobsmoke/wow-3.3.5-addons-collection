

function Whoring_OnLoad(self)
	self:RegisterEvent("PLAYER_REGEN_ENABLED");
	self:RegisterEvent("PLAYER_REGEN_DISABLED");
	--self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
end

function IsPlayer(guid)
	return tonumber(guid:sub(5,5), 16)==0;
end

function Whoring_OnEvent(self, event, ...)
    if event == "INSTANCE_ENCOUNTER_ENGAGE_UNIT" then
        print("INSTANCE_ENCOUNTER_ENGAGE_UNIT")
    elseif event == "PLAYER_REGEN_DISABLED" then
		self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
    elseif event == "PLAYER_REGEN_ENABLED" then
		self:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
    elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then	
      local timestamp, type, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags = select(1, ...)
      if type == "SPELL_AURA_APPLIED" then --dot
         local spellId, spellName, spellSchool = select(9, ...)
         if (destName == "Little Ooze" or destName == "Big Ooze" or spellName == "Corruption") then
			 if spellName == "Vampiric Touch" or spellName == "Living Bomb" then
				 Print(sourceName, spellId, destName)
			 end
		-- elseif (destName == "Prince Valanar" or destName == "Prince Keleseth" or destName == "Prince Taldaram") then
		--	 if (spellName == "Vampiric Touch" or spellName == "Living Bomb") and UnitHealth(destName) == 1 then
		--		 Print(sourceName, spellId, destName)
		--	 end
		 elseif (destName == "Vile Spirit") then
			 if (spellName == "Mind Sear" or spellName == "Seed of Corruption" or spellName == "Corruption" or spellName == "Living Bomb") then
				 Print(sourceName, spellId, destName)
			 end
		 elseif (destName == "Drudge Ghoul") then
			 if ((spellName == "Vampiric Touch" or spellName == "Seed of Corruption" or spellName == "Corruption" or spellName == "Living Bomb") and UnitHealth("boss1") / UnitHealthMax("boss1") > 0.7) then
				 Print(sourceName, spellId, destName)
			 end
		 elseif (destName == "Shambling Horror") then
			 if ((spellName == "Vampiric Touch" or spellName == "Corruption" or spellName == "Living Bomb") and UnitHealth("boss1") / UnitHealthMax("boss1") > 0.7) then
				 Print(sourceName, spellId, destName)
			 end			 
		 end
	  elseif type == "SPELL_CAST_SUCCESS" then -- instant
		 local spellId, spellName, spellSchool = select(9, ...)
		 if (destName == "Little Ooze" or destName == "Big Ooze") then
			 if (spellName == "Cleave" and IsPlayer(sourceGUID) or spellName == "Swipe (Cat)") then
				 Print(sourceName, spellId, destName)
			 end
		 elseif (destName == "Prince Valanar" or destName == "Prince Keleseth" or destName == "Prince Taldaram") then
			 if (spellName == "Cleave" and IsPlayer(sourceGUID) or spellName == "Swipe (Cat)") then
				 Print(sourceName, spellId, destName)
			 end
		 elseif (destName == "Master's Training Dummy") then
			 if (spellName == "Cleave" and IsPlayer(sourceGUID) or spellName == "Swipe (Cat)") then
				 Print(sourceName, spellId, destName)
			 end
--		 elseif (destName == "Vile Spirit") then
--			 if (spellName == "Living Bomb") then
--				 Print(sourceName, spellId, destName)
--			 end
--		 elseif (destName == "Drudge Ghoul" and UnitHealth("boss1") / UnitHealthMax("boss1") > 0.7) then
--			 if (spellName == "Living Bomb") then
--				 Print(sourceName, spellId, destName)
--			 end
		 end
	  elseif type == "SPELL_DAMAGE" then -- channeling
		  local spellId, spellName, spellSchool = select(9, ...)
		  if (destName == "Master's Training Dummy" or destName == "Little Ooze" or destName == "Big Ooze") then
			 if (spellName == "Cleave" and IsPlayer(sourceGUID) or spellName == "Swipe (Cat)" or spellName == "Seal of Command") then
				 Print(sourceName, spellId, destName)
			 end
		  elseif (destName == "Drudge Ghoul") then
			 if (spellName == "Fan of Knives" and UnitHealth("boss1") / UnitHealthMax("boss1") > 0.7) then
				 Print(sourceName, spellId, destName)
			 end
		  end
	  end
   end
end

function Print(sourceName, spellId, destName)
local index = GetChannelName("Whoring") -- It finds General is a channel at index 1
if (index~=nil) then 
    SendChatMessage(string.format("*Whoring* %s cast %s on %s", sourceName, GetSpellLink(spellId), destName) , "RAID"); 
else
	DEFAULT_CHAT_FRAME:AddMessage(string.format("*Whoring* %s cast %s on %s", sourceName, GetSpellLink(spellId), destName));
end
end
