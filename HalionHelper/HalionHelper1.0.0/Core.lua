HalionHelperMod = LibStub("AceAddon-3.0"):NewAddon("HalionHelperMod", "AceEvent-3.0", "AceTimer-3.0")
HalionHelperMod.MINOR_VERSION = tonumber(("$Revision: 9 $"):match("%d+"))
local mod = HalionHelperMod
local L

L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "enUS", true)
L["Stop All Damage!"] = true
L["Slow Down!"] = true
L["Harder! Faster!"] = true
L["OMG MOAR DAMAGE!"] = true
L["There"] = true
L["Here"] = true

L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "deDE")
if L then
L["Harder! Faster!"] = "Härter! Schneller!"
L["Here"] = "Hier"
L["OMG MOAR DAMAGE!"] = "MEHR SCHADEN!"
L["Slow Down!"] = "Langsamer!"
L["Stop All Damage!"] = "KEIN SCHADEN MEHR!!"
L["There"] = "Drüben"

end
L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "esES")
if L then
-- L["Harder! Faster!"] = ""
-- L["Here"] = ""
-- L["OMG MOAR DAMAGE!"] = ""
-- L["Slow Down!"] = ""
-- L["Stop All Damage!"] = ""
-- L["There"] = ""

end
L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "esMX")
if L then
-- L["Harder! Faster!"] = ""
-- L["Here"] = ""
-- L["OMG MOAR DAMAGE!"] = ""
-- L["Slow Down!"] = ""
-- L["Stop All Damage!"] = ""
-- L["There"] = ""

end
L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "frFR")
if L then
L["Harder! Faster!"] = "Plus de DPS !"
L["Here"] = "Ici"
-- L["OMG MOAR DAMAGE!"] = ""
L["Slow Down!"] = "Doucement !"
L["Stop All Damage!"] = "Arrêtez tout DPS !"
L["There"] = "Autre monde"

end
L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "koKR")
if L then
L["Harder! Faster!"] = "공격! 빠르게!"
L["Here"] = "낮음"
L["OMG MOAR DAMAGE!"] = "최대 극딜 하세요!"
L["Slow Down!"] = "공격! 천천히!"
L["Stop All Damage!"] = "모두 공격 중지!"
L["There"] = "높음"

end
L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "ruRU")
if L then
L["Harder! Faster!"] = "Сильней! Быстрей!"
L["Here"] = "Тут"
L["OMG MOAR DAMAGE!"] = "БОЛЬШЕ УРОНА!"
L["Slow Down!"] = "Притормози!"
L["Stop All Damage!"] = "Прекратить весь урон!"
L["There"] = "Там"

end
L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "zhCN")
if L then
L["Harder! Faster!"] = "这边快用全力打王阿! 不然这边坦克快死啦!"
L["Here"] = "100%" -- Needs review
L["OMG MOAR DAMAGE!"] = "这边打王稍微 快一点!"
L["Slow Down!"] = "这边打王稍微 慢一点!"
L["Stop All Damage!"] = "这边停止所有对王伤害! 另一边坦克快死啦!"
L["There"] = "0%" -- Needs review

end
L = LibStub("AceLocale-3.0"):NewLocale("HalionHelper", "zhTW")
if L then
L["Harder! Faster!"] = "大力打! 大力打!"
L["Here"] = "100%"
L["OMG MOAR DAMAGE!"] = "稍微大力些!"
L["Slow Down!"] = "稍微慢一些!"
L["Stop All Damage!"] = "停手! 停手!"
L["There"] = "0%"

end
L = LibStub("AceLocale-3.0"):GetLocale("HalionHelper")


local pullNPCs = { 40142, 39863 }
local TEXTURE = "Interface\\BUTTONS\\WHITE8X8"
local CORPOREALITY = GetSpellInfo(74826)

local buffs = {
	74836, --  70% less dealt, 100% less taken
	74835, --  50% less dealt,  80% less taken
	74834, --  30% less dealt,  50% less taken
	74833, --  20% less dealt,  30% less taken
	74832, --  10% less dealt,  15% less taken
	74826, --  normal
	74827, --  15% more dealt,  20% more taken
	74828, --  30% more dealt,  50% more taken
	74829, --  60% more dealt, 100% more taken
	74830, -- 100% more dealt, 200% more taken
	74831, -- 200% more dealt, 400% more taken
}

for i, id in ipairs(buffs) do
	buffs[id] = i
end

local HalionHelper = CreateFrame("Frame", "HalionHelper", UIParent)
HalionHelper:SetPoint("CENTER")
HalionHelper:SetHeight(20)
HalionHelper:SetWidth(210)

HalionHelper:SetBackdrop({
	bgFile = "Interface\\BUTTONS\\WHITE8X8", tile = false, tileSize = 16,
	edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 16,
	insets = { left = 4, right = 4, top = 4, bottom = 4 },
})
HalionHelper:SetBackdropColor(0, 0, 0, 1)
HalionHelper:SetBackdropBorderColor(0.6, 0.6, 0.6, 1)

HalionHelper.segments = { }

HalionHelper.segments[1] = HalionHelper:CreateTexture(nil, "ARTWORK")
HalionHelper.segments[1]:SetWidth(1)
HalionHelper.segments[1]:SetHeight(HalionHelper:GetHeight() - 10)
HalionHelper.segments[1]:SetPoint("RIGHT", HalionHelper, "LEFT", 5, 0)

for i = 2, 11 do
	local t = HalionHelper:CreateTexture(nil, "ARTWORK")
	t:SetPoint("LEFT", HalionHelper.segments[i - 1], "RIGHT", 0, 0)
	t:SetWidth((HalionHelper:GetWidth() - 10) / 10)
	t:SetHeight(HalionHelper:GetHeight() - 10)
	t:SetTexture(TEXTURE)
	HalionHelper.segments[i] = t
end

HalionHelper.segments[2]:SetVertexColor(1, 0, 0)
HalionHelper.segments[3]:SetVertexColor(1, 0, 0)
HalionHelper.segments[4]:SetVertexColor(1, .5, 0)
HalionHelper.segments[5]:SetVertexColor(1, 1, 0)
HalionHelper.segments[6]:SetVertexColor(0, 1, 0)
HalionHelper.segments[7]:SetVertexColor(0, 1, 0)
HalionHelper.segments[8]:SetVertexColor(.8, .8, 0)
HalionHelper.segments[9]:SetVertexColor(1, .5, 0)
HalionHelper.segments[10]:SetVertexColor(1, 0, 0)
HalionHelper.segments[11]:SetVertexColor(1, 0, 0)

for i = 2, 5 do
	HalionHelper.segments[i]:SetAlpha(.5)
end
for i = 6, 7 do
	HalionHelper.segments[i]:SetAlpha(.8)
end

HalionHelper.message = HalionHelper:CreateFontString(nil, "OVERLAY", "GameFontNormalLarge")
HalionHelper.message:SetPoint("BOTTOM", HalionHelper, "TOP", 0, 5)
HalionHelper.message:SetText("Message Here")

HalionHelper.outLabel = HalionHelper:CreateFontString(nil, "OVERLAY", "GameFontNormal")
HalionHelper.outLabel:SetPoint("TOPLEFT", HalionHelper, "BOTTOMLEFT", 5, -5)
HalionHelper.outLabel:SetText(L["There"])

HalionHelper.inLabel = HalionHelper:CreateFontString(nil, "OVERLAY", "GameFontNormal")
HalionHelper.inLabel:SetPoint("TOPRIGHT", HalionHelper, "BOTTOMRIGHT", -5, -5)
HalionHelper.inLabel:SetText(L["Here"])

HalionHelper.indicator = HalionHelper:CreateTexture(nil, "OVERLAY")
HalionHelper.indicator:SetPoint("CENTER", HalionHelper.segments[6], "RIGHT")
HalionHelper.indicator:SetWidth(10)
HalionHelper.indicator:SetHeight(HalionHelper:GetHeight() + 10)
HalionHelper.indicator:SetTexture("Interface\\WorldStateFrame\\WorldState-CaptureBar")
HalionHelper.indicator:SetTexCoord(0.77734375, 0.796875, 0, 0.28125)
HalionHelper.indicator:SetDesaturated(true)

HalionHelper:EnableMouse(true)
HalionHelper:SetMovable(true)
HalionHelper:RegisterForDrag("LeftButton")
HalionHelper:SetUserPlaced(false)
HalionHelper:SetScript("OnDragStart", HalionHelper.StartMoving)
HalionHelper:SetScript("OnDragStop", function(self)
	self:StopMovingOrSizing()
	self:SetUserPlaced(false)
	local point, _, _, x, y = self:GetPoint(1)
	if not HalionHelperDB then HalionHelperDB = {} end
	HalionHelperDB.point = point
	HalionHelperDB.x = x
	HalionHelperDB.y = y
end)

local counter = 0
HalionHelper:Hide()
HalionHelper:SetScript("OnUpdate", function(self, elapsed)
	counter = counter + elapsed
	if counter > 0.2 then
		local spellID = select(11, UnitBuff("boss1", CORPOREALITY))
		if not spellID or not buffs[spellID] then
			spellID = select(11, UnitBuff("boss2", CORPOREALITY))
		end
		local i = spellID and buffs[spellID]
		if i then
			self.indicator:SetPoint("CENTER", self.segments[i], "RIGHT")
			if i < 5 then
				self.message:SetText(L["Stop All Damage!"])
				self.message:SetTextColor(1, 0.5, 0)
			elseif i == 5 then
				self.message:SetText(L["Slow Down!"])
				self.message:SetTextColor(1, 1, 0)
			elseif i == 6 then
				self.message:SetText()
			elseif i == 7 then
				self.message:SetText(L["Harder! Faster!"])
				self.message:SetTextColor(1, 1, 0)
			elseif i > 6 then
				self.message:SetText(L["OMG MOAR DAMAGE!"])
				self.message:SetTextColor(1, 0.5, 0)
			end
		else
			self.message:SetText("")
		end
		counter = 0
	end
end)

function mod:UNIT_HEALTH(event, unit)
	if unit == "boss1" or unit == "boss2" then
		local max = UnitHealthMax(unit)
		if max then
			if UnitHealth(unit) / max <= 0.5 then
				if not HalionHelper:IsShown() then HalionHelper:Show() end
			end
		end
	end
end

local function slashHandler()
	if HalionHelper:IsShown() then HalionHelper:Hide() else HalionHelper:Show() end
end

function mod:OnEnable()
	self:RegisterEvent("INSTANCE_ENCOUNTER_ENGAGE_UNIT", "CheckForPull")
	if not HalionHelperDB then HalionHelperDB = {} end
	HalionHelper:SetPoint(HalionHelperDB.point or "CENTER", HalionHelperDB.x or 0, HalionHelperDB.y or 250)
	SlashCmdList["HALIONHELPERCOMMAND"] = slashHandler
	SLASH_HALIONHELPERCOMMAND1 = "/halionhelper"
end

function mod:StartFight()
	self:RegisterEvent("PLAYER_REGEN_ENABLED", "EndFight")
	self:RegisterEvent("UNIT_HEALTH")
	self:RegisterEvent("PLAYER_REGEN_DISABLED", "CheckForPull")
end

function mod:EndFight()
	self:UnregisterEvent("PLAYER_REGEN_ENABLED")
	self:UnregisterEvent("UNIT_HEALTH")
	HalionHelper:Hide()
	HalionHelper.message:SetText("Message Here")
end

do -- Pull detection
	local t = { "boss1", "target", "targettarget", "focus", "focustarget", "mouseover", "mouseovertarget" }
	for i = 1, 4 do t[#t+1] = format("party%dtarget", i) end
	for i = 1, 40 do t[#t+1] = format("raid%dtarget", i) end
	
	local function doCheckForPull(id, num)
		for _, unit in pairs(t) do
			if UnitExists(unit) and not UnitIsPlayer(unit) and tonumber(UnitGUID(unit):sub(-12, -7), 16) == id then
				if UnitAffectingCombat(unit) then
					mod:StartFight()
					return true
				elseif num < 30 then
					mod:ScheduleTimer(function() doCheckForPull(id, num + 1) end, 1)
				end
				return
			end
		end
	end

	function mod:CheckForPull()
		if UnitInRaid("player") then for _, id in pairs(pullNPCs) do if doCheckForPull(id, 0) then return end end end
	end
end
