## Author: Allara, Phanx
## Interface: 30300
## Notes: Helps you see the Halion's Corporeality during phase 3
## Title: Halion Helper
## Version: 1.0
## SavedVariables: HalionHelperDB
## OptionalDeps: Ace3
## X-Embeds: Ace3
## X-Category: Boss Encounters
## LoadManagers: AddonLoader
## X-LoadOn-Zone: The Ruby Sanctum
## X-LoadOn-Slash: /halionhelper
## X-Curse-Packaged-Version: r9.2-release
## X-Curse-Project-Name: Halion Helper
## X-Curse-Project-ID: halionhelper
## X-Curse-Repository-ID: wow/halionhelper/mainline

#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
Libs\AceAddon-3.0\AceAddon-3.0.xml
Libs\AceLocale-3.0\AceLocale-3.0.xml
Libs\AceTimer-3.0\AceTimer-3.0.xml
Libs\AceEvent-3.0\AceEvent-3.0.xml
#@end-no-lib-strip@

Core.lua
