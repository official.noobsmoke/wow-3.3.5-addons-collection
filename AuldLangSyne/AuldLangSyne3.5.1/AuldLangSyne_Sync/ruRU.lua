﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Sync")
L:RegisterTranslations("ruRU", function()
    return {
        ["Sync"] = "Синхронизация",
        ["Synchronize the friends lists of all your characters on this realm."] = "Синхронизирует список друзей между вашими персонажами в этом мире.",
    }
end)
