﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Sync")
L:RegisterTranslations("deDE", function()
    return {
        ["Synchronize the friends lists of all your characters on this realm."] = "Synchronisiert die Freundesliste aller Charaktere auf diesem Realm.",
    }
end)
