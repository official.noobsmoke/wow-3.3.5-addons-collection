local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Sync")
L:RegisterTranslations("enUS", function()
	return {
		["Sync"] = true,
		["Synchronize the friends lists of all your characters on this realm."] = true,
	}
end)
