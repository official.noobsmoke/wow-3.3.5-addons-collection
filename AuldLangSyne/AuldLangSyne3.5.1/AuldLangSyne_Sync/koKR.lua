﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Sync")
L:RegisterTranslations("koKR", function()
	return {
		["Sync"] = "동기화",
		["Synchronize the friends lists of all your characters on this realm."] = "현재 서버 내 당신의 모든 케릭터들의 친구 목록을 동기화 합니다.",
	}
end)
