﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Sync")
L:RegisterTranslations("zhTW", function()
	return {
		["Sync"] = "同步",
		["Synchronize the friends lists of all your characters on this realm."] = "同步你在這一個伺服器所有角色的好友資料。",
	}
end)
