## Interface: 30300
## Title: AuldLangSyne |r[|cffeda55fSync|r]
## Notes: Synchronize your friends list
## Version: 2.0
## Author: Kemayo
## X-Website: http://kemayo.wowinterface.com
## X-eMail: kemayo@gmail.com
## X-RelSite-WoWI: 5320
## X-WoWIPortal: kemayo
## X-Category: Interface Enhancements
## X-AuldLangSyne-DefaultState: 1
## SavedVariables: AuldLangSyneSyncDB
## OptionalDeps: AuldLangSyne, Ace2, Deformat, FuBar, TabletLib, DewdropLib, LibFriends-1.0
## X-Curse-Packaged-Version: v3.3.5.1
## X-Curse-Project-Name: AuldLangSyne
## X-Curse-Project-ID: auldlangsyne
## X-Curse-Repository-ID: wow/auldlangsyne/mainline

enUS.lua
deDE.lua
koKR.lua
zhTW.lua
ruRU.lua
Sync.lua
