## Interface: 30300
## Title: AuldLangSyne |r[|cffeda55fGuildList|r]
## Notes: Make your guild list all purdy, like.
## Version: 2.0
## Author: Kemayo
## X-Website: http://kemayo.wowinterface.com
## X-eMail: kemayo@gmail.com
## X-RelSite-WoWI: 5320
## X-WoWIPortal: kemayo
## X-Category: Interface Enhancements
## X-AuldLangSyne-DefaultState: 1
## SavedVariables: AuldLangSyneGuildListDB
## OptionalDeps: AuldLangSyne, AuldLangSyne_Info, AuldLangSyne_FriendList, Ace2, Deformat, FuBar, TabletLib, DewdropLib, LibGuild-1.0
## X-Curse-Packaged-Version: v3.3.5.1
## X-Curse-Project-Name: AuldLangSyne
## X-Curse-Project-ID: auldlangsyne
## X-Curse-Repository-ID: wow/auldlangsyne/mainline

enUS.lua
deDE.lua
koKR.lua
zhTW.lua
ruRU.lua
GuildList.lua
