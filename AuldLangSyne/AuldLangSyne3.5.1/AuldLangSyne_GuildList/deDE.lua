﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_GuildList")
L:RegisterTranslations("deDE", function()
    return {
        ["Guild List"] = "Gildenliste",
        ["Choose what information to display"] = "W\195\164hle aus, welche Infos angezeigt werden sollen.",
        ["Columns"] = "Spalten",
        ["Settings for each column"] = "Einstellungen f\195\188r jede Spalte.",
        ["Area"] = "Gebiet",
        ["Class"] = "Klasse",
        ["Info to display on this line."] = "Infos, die in dieser Zeile angezeigt werden.",
        ["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = "Dokumentation unter http://www.wowace.com/wiki/AuldLangSyne",
        ["Color"] = "Farbe",
        ["Color online friends, according to their tags."] = "Freunde einf\195\164rben, die online sind.",
        ["Color offline friends, according to their tags."] = "Freunde einf\195\164rben, die offline sind.",
        ["Fade offline"] = "Offline: Transparenz",
        ["Fade colors for offline friends to this %age of their base value."] = "Stellt die Transparenz der Farben f\195\188r Offline-Freunde ein.",
    }
end)
