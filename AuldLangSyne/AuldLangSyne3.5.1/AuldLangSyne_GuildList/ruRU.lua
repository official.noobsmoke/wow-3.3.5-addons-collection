﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_GuildList")
L:RegisterTranslations("ruRU", function()
    return {
        ["Guild List"] = "Список гильдии",
        ["Choose what information to display"] = "Выбирите какую показывать информацию.",
        ["Columns"] = "Столбцы",
        ["Settings for each column"] = "Настройки для каждого столбца.",
		["Name"] = "Имя",
		["Level"] = "Уровень",
        ["Area"] = "Зона",
        ["Class"] = "Класс",
        ["Info to display on this line."] = "Информация, отображаемая в этой строке.",
        ["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = "http://www.wowace.com/wiki/AuldLangSyne",
        ["Color"] = "Цвет",
        ["Online"] = "Онлайн",
        ["Color online friends, according to their tags."] = "Раскрашивать согильдийцев, которые находятся в игре в соответствии с их тэгами.",
        ["Offline"] = "Оффлайн",
        ["Color offline friends, according to their tags."] = "Раскрашивать согильдийцев, которых нет в игре в соответствии с их тэгами.",
        ["Fade offline"] = "Интенсивность цвета",
        ["Fade colors for offline friends to this %age of their base value."] = "Приглушать цвет для согильдийцев, которых нет в игре, на этот % от базового значения.",
    }
end)
