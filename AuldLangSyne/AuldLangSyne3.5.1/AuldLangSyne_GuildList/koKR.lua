﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_GuildList")
L:RegisterTranslations("koKR", function()
	return {
		["Guild List"] = "길드원 목록",
		["Choose what information to display"] = "표시할 정보를 선택합니다.",
		["Columns"] = "컬럼",
		["Settings for each column"] = "각 컬럼의 설정",
		["Name"] = "이름",
		["Level"] = "레벨",
		["Area"] = "지역",
		["Class"] = "직업",
		["Info to display on this line."] = "현재 줄에 표시할 정보",
		["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = "http://www.wowace.com/wiki/AuldLangSyne의 정보를 참조하세요.",
		["Color"] = "색상",
		["Online"] = "접속 중",
		["Color online friends, according to their tags."] = "접속 중인 친구목록의 색상을 설정합니다.",
		["Offline"] = "오프라인",
		["Color offline friends, according to their tags."] = "접속하지 않은 친구목록의 색상을 설정합니다.",
		["Fade offline"] = "오프라인 사라짐",
		["Fade colors for offline friends to this %age of their base value."] = "접속하지 않은 기간에 따라 친구목록의 색상을 설정합니다.",
	}
end)