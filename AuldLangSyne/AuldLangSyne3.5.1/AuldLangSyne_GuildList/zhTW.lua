﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_GuildList")
L:RegisterTranslations("zhTW", function()
	return {
		["Guild List"] = "公會成員列表",
		["Choose what information to display"] = "選擇要顯示何種資訊。",
		["Columns"] = "欄位",
		["Settings for each column"] = "欄位個別設定",
		["Name"] = "名字",
		["Level"] = "等級",
		["Area"] = "區域",
		["Class"] = "職業",
		["Info to display on this line."] = "本行要顯示的資訊種類。",
		["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = "請參考網站 http://www.wowace.com/wiki/AuldLangSyne",
		["Color"] = "顏色",
		["Online"] = "在線上",
		["Color online friends, according to their tags."] = "基於好友的標記，顯示正在線上的好友",
		["Offline"] = "已離線",
		["Color offline friends, according to their tags."] = "基於好友的標記，顯示已離線的好友",
		["Fade offline"] = "已離線者淡出顏色",
		["Fade colors for offline friends to this %age of their base value."] = "基於顏色的比例，顯示離線上好友的淡出顏色",
	}
end)