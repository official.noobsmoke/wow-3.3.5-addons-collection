local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_GuildList")
L:RegisterTranslations("enUS", function()
	return {
		["Guild List"] = true,
		["Choose what information to display"] = true,
		["Columns"] = true,
		["Settings for each column"] = true,
		["Name"] = true,
		["Level"] = true,
		["Area"] = true,
		["Class"] = true,
		["Info to display on this line."] = true,
		["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = true,
		["Color"] = true,
		["Online"] = true,
		["Color online friends, according to their tags."] = true,
		["Offline"] = true,
		["Color offline friends, according to their tags."] = true,
		["Fade offline"] = true,
		["Fade colors for offline friends to this %age of their base value."] = true,
	}
end)