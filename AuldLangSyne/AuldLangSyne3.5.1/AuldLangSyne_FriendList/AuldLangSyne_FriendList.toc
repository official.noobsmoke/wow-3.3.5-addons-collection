## Interface: 30300
## Title: AuldLangSyne |r[|cffeda55fFriendList|r]
## Notes: Make your friends list all purdy, like.
## Version: 2.0
## Author: Kemayo
## X-Website: http://kemayo.wowinterface.com
## X-eMail: kemayo@gmail.com
## X-RelSite-WoWI: 5320
## X-WoWIPortal: kemayo
## X-Category: Interface Enhancements
## X-AuldLangSyne-DefaultState: 1
## SavedVariables: AuldLangSyneFriendListDB
## OptionalDeps: AuldLangSyne, AuldLangSyne_Info, Ace2, LibBabble-Class-3.0, Deformat, FuBar, TabletLib, DewdropLib, LibFriends-1.0
## X-Curse-Packaged-Version: v3.3.5.1
## X-Curse-Project-Name: AuldLangSyne
## X-Curse-Project-ID: auldlangsyne
## X-Curse-Repository-ID: wow/auldlangsyne/mainline

enUS.lua
deDE.lua
koKR.lua
zhTW.lua
ruRU.lua
FriendList.lua
