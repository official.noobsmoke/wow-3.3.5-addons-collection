local VERSION = tonumber(("$Revision: 45259 $"):match("%d+"))

local _G = getfenv(0)

local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_FriendList")
local friends = LibStub("LibFriends-1.0")
local BC

local AuldLangSyne = AuldLangSyne
local FriendList, Info, Note
if AuldLangSyne then
	FriendList = AuldLangSyne:NewModule("FriendList", "AceEvent-2.0", "AceHook-2.1", "AceConsole-2.0")
	if AuldLangSyne.revision < VERSION then
		AuldLangSyne.version = "r" .. VERSION
		AuldLangSyne.revision = VERSION
		AuldLangSyne.date = ("$Date: 2007-07-29 22:02:53 -0700 (Sun, 29 Jul 2007) $"):match("%d%d%d%d%-%d%d%-%d%d")
	end
	if AuldLangSyne:HasModule("Info") then
		Info = AuldLangSyne:GetModule("Info")
	end
	if AuldLangSyne:HasModule("Note") then
		Note = AuldLangSyne:GetModule("Note")
	end
else
	FriendList = AceLibrary("AceAddon-2.0"):new("AceEvent-2.0", "AceHook-2.1", "AceConsole-2.0", "AceDB-2.0")
	AuldLangSyne_FriendList = FriendList -- since this isn't a module in this branch, global for access
	Info = AuldLangSyne_Info
end

-- 3.1 backwards comapt
local GetQuestDifficultyColor = _G.GetQuestDifficultyColor or _G.GetDifficultyColor

function FriendList:OnInitialize(name)
	local profiledefaults = {
		lineone = "[pvprank:class] [name:class] - [area] - [status]",
		linetwo = "[level:level] [race:sex] [class:class] [guild:green]",
		coloronline = true,
		coloroffline = true,
		fadeoffline = 0.5,
	}
	if AuldLangSyne then
		self.db = AuldLangSyne:AcquireDBNamespace("FriendList")
		AuldLangSyne:RegisterDefaults("FriendList", "profile", profiledefaults)
	else
		self:RegisterDB("AuldLangSyneFriendListDB")
		self:RegisterDefaults("profile", profiledefaults)
	end
	
	self.menu = {
		handler = FriendList,
		type = "group",
		name = L["Friend List"], desc = L["Choose what information to display"],
		args = {
			lineone = {
				name = L["Line 1"], type = "text",
				desc = L["Info to display on this line."],
				usage = L["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"],
				get = function() return self.db.profile.lineone end,
				set = function(t) self.db.profile.lineone = t; FriendsList_Update() end,
			},
			linetwo = {
				name = L["Line 2"], type = "text",
				desc = L["Info to display on this line."],
				usage = L["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"],
				get = function() return self.db.profile.linetwo end,
				set = function(t) self.db.profile.linetwo = t; FriendsList_Update() end,
			},
			color = {
				type = "group",
				name = L["Color"],
				desc = L["Color"],
				args = {
					online = {
						name = L["Online"], type = "toggle",
						desc = L["Color online friends, according to their tags."],
						get = function() return self.db.profile.coloronline end,
						set = function(t) self.db.profile.coloronline = t; FriendsList_Update() end,
					},
					offline = {
						name = L["Offline"], type = "toggle",
						desc = L["Color offline friends, according to their tags."],
						get = function() return self.db.profile.coloroffline end,
						set = function(t) self.db.profile.coloroffline = t; FriendsList_Update() end,
					},
					fade = {
						name = L["Fade offline"], type = "range",
						desc = L["Fade colors for offline friends to this %age of their base value."],
						min = 0, max = 1, step = 0.05, isPercent = true,
						get = function() return self.db.profile.fadeoffline end,
						set = function(t) self.db.profile.fadeoffline = t; FriendsList_Update() end,
					},
				},
			},
		},
	}
	
	self:RegisterChatCommand({'/friendlist', '/alsfriendlist',}, self.menu)
	
	--Stolen from AceDB.
	local _,race = UnitRace("player")
	if race == "Orc" or race == "Scourge" or race == "Troll" or race == "Tauren" or race == "BloodElf" then
		self.faction = "0"
	else
		self.faction = "1"
	end
	
	if Info and Info.db.profile.lineone then
		local idb, sdb = Info.db.profile, self.db.profile
		sdb.lineone = idb.lineone or sdb.lineone
		sdb.linetwo = idb.linetwo or sdb.linetwo
		sdb.coloronline = idb.coloronline or sdb.coloronline
		sdb.coloroffline = idb.coloroffline or idb.coloroffline
		sdb.fadeoffline = idb.fadeoffline or sdb.fadeoffline
		
		if AuldLangSyne_Info then
			Info:ResetDB("profile")
		else
			AuldLangSyne:ResetDB("Info", "profile")
		end
	end
end

function FriendList:OnEnable(first)
	self:SecureHook(FriendsFrameFriendsScrollFrame,"buttonFunc","FriendsFrame_SetButton")
	FriendsList_Update()
end

function FriendList:OnDisable()
	FriendsList_Update()
end

------------------------------------------------------------------------
-- Friend info display
------------------------------------------------------------------------

do
	local colors = {
		["class"] = function(name)
				if friends:GetEnglishClass(name) then
					return friends:GetClassColor(name)
				elseif Info and Info.db.realm[name] and Info.db.realm[name].class then
					if not BC then BC = LibStub("LibBabble-Class-3.0"):GetReverseLookupTable() end
					local c = RAID_CLASS_COLORS[BC[Info.db.realm[name].class]:upper()]
					return c.r, c.g, c.b
				else
					return 0.8, 0.8, 0.8
				end
			end,
		["level"] = function(name)
				local level = friends:GetLevel(name)
				if not level then
					level = Info and Info.db.realm[name] and Info.db.realm[name].level
				end
				if level then
					local c = GetQuestDifficultyColor(level)
					return c.r, c.g, c.b
				end
				return 1,1,1
			end,
		["sex"] = function(name)
				local sex = Info and Info.db.realm[name] and Info.db.realm[name].sex
				if sex == MALE then
					return 0.56, 0.69, 0.94
				elseif sex == FEMALE then
					return 0.94, 0.56, 0.76
				end
				return 1, 1, 1
			end,
		-- And some static colors
		["gold"] = function(name) return 1, 0.78, 0 end, -- this is the "default" blizzard-gold color
		["red"] = function(name) return 1, 0, 0 end,
		["green"] = function(name) return 0, 1, 0 end,
		["blue"] = function(name) return 0, 0, 1 end,
		["white"] = function(name) return 1, 1, 1 end,
		["black"] = function(name) return 0, 0, 0 end,
		["grey"] = function(name) return 0.5, 0.5, 0.5 end,
	}
	local colorformat = "|cff%.2x%.2x%.2x%s|r"
	local function colorify(name, colortype, s)
		if (not s) or s=='' then return '' end
		local online = friends:IsFriendOnline(name)
		if (online and FriendList.db.profile.coloronline) or ((not online) and FriendList.db.profile.coloroffline) then
			local r, g, b = 1, 1, 1
			if colortype then
				if colors[colortype] then
					r, g, b = colors[colortype](name)
				else
					local hexr, hexg, hexb = colortype:match("c(%x%x)(%x%x)(%x%x)")
					if hexr then
						r, g, b = tonumber(hexr, 16)/255, tonumber(hexg, 16)/255, tonumber(hexb, 16)/255
					end
				end
			end
			if not online then
				local fade = FriendList.db.profile.fadeoffline
				r, g, b = r*fade, g*fade, b*fade
			end
			return colorformat:format(r*255, g*255, b*255, s)
		else
			if online then
				return s
			else
				local fade = FriendList.db.profile.fadeoffline
				return colorformat:format(255*fade, 255*fade, 255*fade, s)
			end
		end
	end
	local subs = {
		["name"] = function(name, modifier) return colorify(name, modifier, name) end,
		["class"] = function(name, modifier) local class = friends:GetClass(name) if class == UNKNOWN and Info and Info.db.realm[name] and Info.db.realm[name].class then class = Info.db.realm[name].class end return colorify(name, modifier, class) end,
		["level"] = function(name, modifier) local level = friends:GetLevel(name) if level == 0 and Info and Info.db.realm[name] and Info.db.realm[name].level then level = Info.db.realm[name].level end return colorify(name, modifier, level) end,
		["race"] = function(name, modifier) return colorify(name, modifier, Info and Info.db.realm[name] and Info.db.realm[name].race) end,
		["sex"] = function(name, modifier) return colorify(name, modifier, Info and Info.db.realm[name] and Info.db.realm[name].sex) end,
		["pvprank"] = function(name, modifier) return colorify(name, modifier, Info and Info.db.realm[name] and Info.db.realm[name].rank and FriendList:GetRankName(Info.db.realm[name].rank, Info.db.realm[name].sex)) end,
		["pvpranknum"] = function(name, modifier) return colorify(name, modifier, Info and Info.db.realm[name] and Info.db.realm[name].rank and tostring(Info.db.realm[name].rank - 4)) end,
		["guild"] = function(name, modifier) return colorify(name, modifier, Info and Info.db.realm[name] and Info.db.realm[name].guild and ('<'..Info.db.realm[name].guild..'>')) end,
		["guildrank"] = function(name, modifier) return colorify(name, modifier, Info and Info.db.realm[name] and Info.db.realm[name].guildRank) end,
		["area"] = function(name, modifier) return colorify(name, modifier, friends:GetZone(name)~=UNKNOWN and friends:GetZone(name) or '') end,
		["status"] = function(name, modifier) return colorify(name, modifier, friends:IsFriendOnline(name) and (friends:GetStatus(name) or '') or (Info and Info.db.realm[name] and FriendList:HowLongAgo(Info.db.realm[name].updated))) end,
		["note"] = function(name, modifier) if Note and AuldLangSyne:IsModuleActive("Note") then return colorify(name, modifier, Note.db.realm["friend"][name]) else return "" end end,
	}
	local tagpattern = "%[([^:%]]+):?([^%]]*)%]"
	local current_friend
	local function tagreplace(tag, modifier)
		if subs[tag] then
			return subs[tag](current_friend, modifier)
		else
			return colorify(current_friend, modifier, tag)
		end
	end
	function FriendList:FriendsFrame_SetButton(button, index, firstButton)
		if button and button.buttonType == FRIENDS_BUTTON_TYPE_WOW then
			local name = GetFriendInfo(button.id)
			current_friend = name

			local nameLocationText = button.name
			local infoText = button.info

			nameLocationText:SetText(strtrim(((self.db.profile.lineone):gsub(tagpattern, tagreplace)):gsub("%-%s*%-", ''), "- \t"))
			infoText:SetText(strtrim(((self.db.profile.linetwo):gsub(tagpattern, tagreplace)):gsub("%-%s*%-", ''), "- \t"))
			-- Make sure we're not overflowing
			if nameLocationText:GetWidth() > 275 then
				nameLocationText:SetJustifyH("LEFT")
				nameLocationText:SetWidth(275)
				nameLocationText:SetHeight(14) -- If we don't set the height, the line wraps.
			end
			if infoText:GetWidth() > 275 then
				infoText:SetJustifyH("LEFT")
				infoText:SetWidth(275)
				infoText:SetHeight(14) -- If we don't set the height, the line wraps.
			end
		end
	end
end

------------------------------------------------------------------------
-- Utility functions
------------------------------------------------------------------------

function FriendList:HowLongAgo(when)
	if when then
		local ago = (time() - when)
		local minutes = math.ceil(ago / 60)
		if minutes > 59 then
			local hours = math.ceil(ago / 3600)
			if hours > 23 then
				local days = math.ceil(ago / 86400)
				lastseen = ((days > 1) and INT_GENERAL_DURATION_DAYS_P1 or INT_GENERAL_DURATION_DAYS):format(days)
			else
				lastseen = ((hours > 1) and INT_GENERAL_DURATION_HOURS_P1 or INT_GENERAL_DURATION_HOURS):format(hours)
			end
		else
			lastseen = ((minutes > 1) and INT_GENERAL_DURATION_MIN_P1 or INT_GENERAL_DURATION_MIN):format(minutes)
		end
		return lastseen
	else
		return ""
	end
end

function FriendList:GetRankName(rank, sex)
	--"rank" should be the number of the PVP rank (starts at 5, goes to 19).
	--"sex" should be either MALE or FEMALE.
	return rank > 0 and _G["PVP_RANK_"..rank.."_"..self.faction..((sex == FEMALE) and "_FEMALE" or "")] or ''
end
