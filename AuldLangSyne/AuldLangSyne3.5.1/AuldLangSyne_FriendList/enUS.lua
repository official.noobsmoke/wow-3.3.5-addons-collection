local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_FriendList")
L:RegisterTranslations("enUS", function()
	return {
		["Friend List"] = true,
		["Choose what information to display"] = true,
		["Line 1"] = true,
		["Line 2"] = true,
		["Info to display on this line."] = true,
		["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = true,
		["Color"] = true,
		["Online"] = true,
		["Color online friends, according to their tags."] = true,
		["Offline"] = true,
		["Color offline friends, according to their tags."] = true,
		["Fade offline"] = true,
		["Fade colors for offline friends to this %age of their base value."] = true,
	}
end)