﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_FriendList")
L:RegisterTranslations("deDE", function()
    return {
        ["Friend List"] = "Freundesliste",
        ["Choose what information to display"] = "Die ausgew\195\164hlten Informationen anzeigen.",
        ["Line 1"] = "Zeile 1",
        ["Line 2"] = "Zeile 2",
        ["Info to display on this line."] = "Infos, die auf dieser Zeile angezeigt werden.",
        ["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = "Dokumentation unter http://www.wowace.com/wiki/AuldLangSyne",
        ["Color"] = "Farbe",
        ["Color online friends, according to their tags."] = "Freunde einf\195\164rben, die online sind.",
        ["Color offline friends, according to their tags."] = "Freunde einf\195\164rben, die offline sind.",
        ["Fade offline"] = "Offline: Transparenz",
        ["Fade colors for offline friends to this %age of their base value."] = "Stellt die Transparenz der Farben f\195\188r Offline-Freunde ein.",
    }
end)
