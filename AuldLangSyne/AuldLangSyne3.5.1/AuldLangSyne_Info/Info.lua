local VERSION = tonumber(("$Revision: 249 $"):match("%d+"))

local _G = getfenv(0)

local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Info")
local friends = AceLibrary("LibFriends-1.0")
local deformat = AceLibrary("Deformat-2.0")

local AuldLangSyne = AuldLangSyne
local Info
if AuldLangSyne then
	Info = AuldLangSyne:NewModule("Info", "AceEvent-2.0", "AceHook-2.1", "AceConsole-2.0")
	if AuldLangSyne.revision < VERSION then
		AuldLangSyne.version = "r" .. VERSION
		AuldLangSyne.revision = VERSION
		AuldLangSyne.date = ("$Date: 2010-06-27 22:11:28 +0000 (Sun, 27 Jun 2010) $"):match("%d%d%d%d%-%d%d%-%d%d")
	end
else
	Info = AceLibrary("AceAddon-2.0"):new("AceEvent-2.0", "AceHook-2.1", "AceConsole-2.0", "AceDB-2.0")
	AuldLangSyne_Info = Info -- since this isn't a module in this branch, global for access
end

function Info:OnInitialize(name)
	if AuldLangSyne then
		self.db = AuldLangSyne:AcquireDBNamespace("Info")
	else
		self:RegisterDB("AuldLangSyneInfoDB")
	end
	
	if AuldLangSyne and AuldLangSyne.db.realm.chars then
		for name, data in pairs(AuldLangSyne.db.realm.chars) do
			self.db.realm[name] = data
		end
		AuldLangSyne.db.realm.chars = nil
	end
end

function Info:OnEnable(first)
	friends.RegisterCallback(self, "Update")
	self:SecureHook(GameTooltip, "SetUnit")
	self:SecureHook("GetWhoInfo")
	self:SecureHook("SendWho")
end

------------------------------------------------------------------------
-- Friend info display
------------------------------------------------------------------------

function Info:Update()
	for name in friends:GetIterator() do -- sorts by name, no offline friends
		if self.db.realm[name] == nil then self.db.realm[name] = {} end
		self.db.realm[name].level = friends:GetLevel(name)
		self.db.realm[name].class = friends:GetClass(name)
		self.db.realm[name].updated = time()
	end
end

function Info:SetUnit(object, unitid)
	if UnitIsPlayer(unitid) then
		local name = UnitName(unitid)
		if self.db.realm[name] ~= nil then
			self.db.realm[name].rank = UnitPVPRank(unitid)
			self.db.realm[name].race = UnitRace(unitid)

			local sex = UnitSex(unitid)
			-- 1: neuter/unknown, 2: male, 3: female
			-- We're not storing the output of UnitSex directly, for Blizzard has been known to change it.
			if sex == 2 then
				self.db.realm[name].sex = MALE
			elseif sex == 3 then
				self.db.realm[name].sex = FEMALE
			end

			local guildName, guildRankName, guildRankIndex = GetGuildInfo(unitid)
			self.db.realm[name].guild = guildName
			self.db.realm[name].guildRank = guildRankName

			self.db.realm[name].updated = time()

			if FriendsFrame:IsVisible() and FriendsFrame.selectedTab == 1 then
				FriendsList_Update()
			end
		end
	end
end

local in_get_who_info = false
function Info:GetWhoInfo(i)
	if in_get_who_info then return end
	in_get_who_info = true
	local name, guild, level, race, class = GetWhoInfo(i)
	if self.db.realm[name] ~= nil then
		if guild == "" then guild = nil end
		self.db.realm[name].guild = guild
		self.db.realm[name].level = level
		self.db.realm[name].race = race
		self.db.realm[name].class = class
		self.db.realm[name].updated = time()

		if FriendsFrame:IsVisible() and FriendsFrame.selectedTab == 1 then
			FriendsList_Update()
		end
	end
	in_get_who_info = false
end

do
	local function stopListening()
		Info:UnregisterEvent("CHAT_MSG_SYSTEM")
	end
	function Info:SendWho(filter)
		self:RegisterEvent("CHAT_MSG_SYSTEM")
		self:ScheduleEvent("ALSInfo_stoplistening", stopListening, 0.5)
	end
end

function Info:CHAT_MSG_SYSTEM(message)
	local _, name, level, race, class, guild, location = deformat(message, WHO_LIST_GUILD_FORMAT)
	if not name then _, name, level, race, class, location = deformat(message, WHO_LIST_FORMAT) end
	if name and self.db.realm[name] then
		if guild then self.db.realm[name].guild = guild end
		self.db.realm[name].level = level
		self.db.realm[name].race = race
		self.db.realm[name].class = class
		self.db.realm[name].updated = time()
		if FriendsFrame:IsVisible() and FriendsFrame.selectedTab == 1 then
			FriendsList_Update()
		end
	end
end
