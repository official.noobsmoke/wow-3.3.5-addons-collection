local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Info")
L:RegisterTranslations("deDE", function()
    return {
        ["Show"] = "Anzeigen",
        ["Choose what information to display"] = "W\195\164hle aus, welche Infos angezeigt werden sollen.",
        ["Line 1"] = "Zeile 1",
        ["Line 2"] = "Zeile 2",
        ["Info to display on this line."] = "Info, die in dieser Zeile angezeigt werden soll.",
        ["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = "Dokumentation unter http://www.wowace.com/wiki/AuldLangSyne",
        ["Color"] = "Farbe",

        ["Color online friends, according to their tags."] = "Freunde einf\195\164rben, die Online sind.",

        ["Color offline friends, according to their tags."] = "Freunde einf\195\164rben, die Offline sind.",
        ["Fade offline"] = "Transparenz Offlines",
        ["Fade colors for offline friends to this %age of their base value."] = "Stellt die Transparenz der Farben f�r Offline-Freunde ein.",
        
        [" day"] = " Tag",
        [" hour"] = " Stunde",
        [" minute"] = " Minute",
        ["s"] = "n",
        [" ago"] = "weg",
    }
end)
