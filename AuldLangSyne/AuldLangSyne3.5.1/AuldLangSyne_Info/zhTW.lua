﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Info")
L:RegisterTranslations("zhTW", function()
	return {
		["Show"] = "顯示",
		["Choose what information to display"] = "選擇要顯示何種資訊。",
		["Line 1"] = "第一行",
		["Line 2"] = "第二行",
		["Info to display on this line."] = "本行要顯示的資訊種類。",
		["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = "請參考網站 http://www.wowace.com/wiki/AuldLangSyne",
		["Color"] = "顏色",
		["Online"] = "在線上",
		["Color online friends, according to their tags."] = "基於好友的標記，顯示正在線上的好友",
		["Offline"] = "已離線",
		["Color offline friends, according to their tags."] = "基於好友的標記，顯示已離線的好友",
		["Fade offline"] = "已離線者淡出顏色",
		["Fade colors for offline friends to this %age of their base value."] = "基於顏色的比例，顯示離線上好友的淡出顏色",

		[" day"] = "天",
		[" hour"] = "小時",
		[" minute"] = "分",
		["s"] = "",
		[" ago"] = "以前",
	}
end)