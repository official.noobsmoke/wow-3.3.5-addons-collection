﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Info")
L:RegisterTranslations("ruRU", function()
    return {
        ["Show"] = "Показать",
        ["Choose what information to display"] = "Выбирите какую показывать информацию.",
        ["Line 1"] = "Строка 1",
        ["Line 2"] = "Строка 2",
        ["Info to display on this line."] = "Информация, отображаемая в этой строке.",
        ["See documentation at: http://www.wowace.com/wiki/AuldLangSyne"] = "http://www.wowace.com/wiki/AuldLangSyne",
        ["Color"] = "Farbe",

        ["Color online friends, according to their tags."] = "Раскрашивать друзей, которые находятся в игре в соответствии с их тэгами.",

        ["Color offline friends, according to their tags."] = "Раскрашивать друзей, которых нет в игре в соответствии с их тэгами.",
        ["Fade offline"] = "Интенсивность цвета",
        ["Fade colors for offline friends to this %age of their base value."] = "Приглушать цвет для друзей, которых нет в игре, на этот % от базового значения.",
        
        [" day"] = " день",
        [" hour"] = " час",
        [" minute"] = " минута",
        ["s"] = "с",
        [" ago"] = "прошло",
    }
end)
