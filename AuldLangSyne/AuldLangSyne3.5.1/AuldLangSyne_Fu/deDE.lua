﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Fu")
L:RegisterTranslations("deDE", function()
    return {
        ["Columns"] = "Spalten",
        ["Colors"] = "Farben",
        ["FuBar plugin options"] = "FuBar Plugin Optionen",
        ["Friends"] = "Freunde",
        ["Show"] = "Anzeigen",
        ["Show name"] = "Zeigt Gildennamem an",
        ["Show Total"] = "Zeigt Gesamtanzahl an",
        ["Show the total in the fubar plugin"] = "Zeigt die Gesamtanzahl im FuBar Plugin an.",
        ["Sort"] = "Sortieren",
        ["Guild"] = "Gilde",
        ["MotD"] = "MotD",
        ["Message of the Day"] = "Nachricht des Tages anzeigen.",
        ["FuBar options"] = "FuBar Optionen",
        ["|cffeda55fClick|r to open the friends panel.  |cffeda55fAlt-Click|r to open the guild panel.  |cffeda55fClick|r a line to whisper a player. |cffeda55fControl-Click|r a line to edit a note. |cffeda55fAlt-Click|r a line to invite to a group."] = "|cffeda55fKlicken|r, um die Freundesliste aufzurufen.  |cffeda55fAlt-Klicken|r, um die Gilden\195\188bersicht aufzurufen.  Auf |cffeda55fZeile klicken|r, um zu fl\195\188stern. Auf |cffeda55fZeile Strg-Klicken|r, um eine Notiz zu erstellen. Eine |cffeda55fZeile Alt-Klicken|r, um in Gruppe einzuladen.",
        ["Name"] = "Name",
        ["Level"] = "Level",
        ["Class"] = "Klasse",
        ["Zone"] = "Zone",
        ["Status"] = "Status",
        ["Note"] = "Notiz",
        ["Rank"] = "Rang",
    }
end)
