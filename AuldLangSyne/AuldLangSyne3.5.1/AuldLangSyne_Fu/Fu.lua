local _G = getfenv(0)
local VERSION = tonumber(("$Revision: 264 $"):match("%d+"))
local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Fu")

local tablet = AceLibrary("Tablet-2.0")
local dewdrop = AceLibrary("Dewdrop-2.0")
local friends = AceLibrary("LibFriends-1.0")
local guild = AceLibrary("LibGuild-1.0")
local roster = AceLibrary("Roster-2.1")
local BC

local AuldLangSyne = AuldLangSyne
local Fu
local new, del, newHash, newSet, deepCopy, deepDel, clear
if AuldLangSyne then
	Fu = AuldLangSyne:NewModule("Fu", "AceHook-2.1", "AceEvent-2.0", "FuBarPlugin-2.0")
	if AuldLangSyne.revision < VERSION then
		AuldLangSyne.version = "r" .. VERSION
		AuldLangSyne.revision = VERSION
		AuldLangSyne.date = ("$Date: 2010-07-02 23:53:12 +0000 (Fri, 02 Jul 2010) $"):match("%d%d%d%d%-%d%d%-%d%d")
	end
	new, del, newHash, newSet, deepCopy, deepDel, clear = AuldLangSyne.new, AuldLangSyne.del, AuldLangSyne.newHash, AuldLangSyne.newSet, AuldLangSyne.deepCopy, AuldLangSyne.deepDel, AuldLangSyne.clear
else
	Fu = AceLibrary("AceAddon-2.0"):new("AceEvent-2.0", "AceHook-2.1", "AceConsole-2.0", "FuBarPlugin-2.0", "AceDB-2.0")
	AuldLangSyne_Fu = Fu -- since this isn't a module in this branch, global for access
	do
		local list = setmetatable({}, {__mode='k'})
		function new(...)
			local t = next(list)
			if t then
				list[t] = nil
				for i = 1, select('#', ...) do
					t[i] = select(i, ...)
				end
				return t
			else
				return { ... }
			end
		end
		function del(t)
			for k in pairs(t) do
				t[k] = nil
			end
			t[''] = true
			t[''] = nil
			list[t] = true
			return nil
		end
	end
end

-- 3.1 backwards comapt
local GetQuestDifficultyColor = _G.GetQuestDifficultyColor or _G.GetDifficultyColor

local faction_name = { [0] = 'Horde', [1] = 'Alliance' }

Fu.name = "AuldLangSyneFu"
Fu.title = "AuldLangSyneFu"
Fu.clickableTooltip = true
Fu.hasIcon = "Interface\\FriendsFrame\\FriendsFrameScrollIcon"
Fu.hideWithoutStandby = true
Fu.independentProfile = true

function Fu:OnInitialize(name)
	local profiledefaults = {
		friends = {
			show = true,
			showtotal = true,
			sort = 'NAME',
			offline = false,
			columns = {
				name = true,
				level = true,
				class = false,
				zone = true,
				status = true,
				note = true,
			},
			colors = {
				name = 'class',
				level = 'level',
				class = 'class',
				zone = 'none',
			},
		},
		guild = {
			show = true,
			showname = true,
			showtotal = true,
			sort = 'NAME',
			offline = false,
			motd = true,
			columns = {
				name = true,
				level = true,
				class = false,
				zone = true,
				status = true,
				rank = true,
				note = true,
			},
			colors = {
				name = 'class',
				level = 'level',
				class = 'class',
				zone = 'none',
				rank = 'none',
			},
		},
		realid = {
			show = true,
			showtotal = true,
			sort = 'NAME',
			offline = false,
			columns = {
				name = true,
				toon = true,
				level = true,
				class = false,
				zone = true,
				realm = true,
				game = true,
				status = true,
				broadcast = false,
			},
			colors = {
				name = 'none',
				toon = 'class',
				level = 'level',
				class = 'class',
				zone = 'none',
				realm = 'none',
				game = 'none',
				broadcast = 'none',
			},
		},
	}
	
	if AuldLangSyne then
		self.db = AuldLangSyne:AcquireDBNamespace("Fu")
		AuldLangSyne:RegisterDefaults("Fu", "profile", profiledefaults)
	else
		self:RegisterDB("AuldLangSyneFuDB")
		self:RegisterDefaults("profile", profiledefaults)
	end
	
	local function makeGetter(kind, option)
		return function(key) return self.db.profile[kind][option][key] end
	end
	local function makeSetter(kind, option)
		return function(key, t) self.db.profile[kind][option][key] = t; self:Update() end
	end
	local function makeColumnsGroup(kind)
		local group = {
			type = "group",
			name = L["Columns"], desc = L["Columns"],
			pass = true,
			get = makeGetter(kind, 'columns'),
			set = makeSetter(kind, 'columns'),
			args = {},
		}
		for column in pairs(self.db.profile[kind].columns) do
			group.args[column] = {type="toggle", name=column, desc=column,}
		end
		return group
	end
	local colorChoices = {'class', 'level', 'none'}
	local function makeColorsGroup(kind)
		local group = {
			type = "group",
			name = L["Colors"], desc = L["Colors"],
			pass = true,
			get = makeGetter(kind, 'colors'),
			set = makeSetter(kind, 'colors'),
			args = {},
		}
		for column in pairs(self.db.profile[kind].colors) do
			group.args[column] = {type='text', name=column, desc=column, validate=colorChoices,}
		end
		return group
	end
	self.menu = {
		handler = Fu,
		name = "FuBar", type = "group",
		desc = L["FuBar plugin options"],
		args = {
			friends = {
				type = "group",
				name = L["Friends"], desc = L["Friends"],
				args = {
					show = {
						type = 'toggle',
						name = L["Show"], desc = L["Show"],
						get = function() return self.db.profile.friends.show end,
						set = function(t) self.db.profile.friends.show = t; self:Update() end,
						order = 1,
					},
					showtotal = {
						type = 'toggle',
						name = L["Show Total"], desc = L["Show the total in the fubar plugin"],
						get = function() return self.db.profile.friends.showtotal end,
						set = function(t) self.db.profile.friends.showtotal = t; self:Update() end,
						order = 2,
					},
					offline = {
						type = 'toggle',
						name = L["Offline"], desc = L["Offline"],
						get = function() return self.db.profile.friends.offline end,
						set = function(t) self.db.profile.friends.offline = t; self:UpdateTooltip() end,
					},
					sort = {
						type = 'text',
						name = L["Sort"], desc = L["Sort"],
						get = function() return self.db.profile.friends.sort end,
						set = function(t) self.db.profile.friends.sort = t; self:Update() end,
						validate = {'NAME', 'LEVEL', 'ONLINE', 'ZONE', 'CLASS'},
					},
					columns = makeColumnsGroup('friends'),
					colors = makeColorsGroup('friends'),
				},
			},
			guild = {
				type = "group",
				name = L["Guild"], desc = L["Guild"],
				args = {
					show = {
						type = 'toggle',
						name = L["Show"], desc = L["Show"],
						get = function() return self.db.profile.guild.show end,
						set = function(t) self.db.profile.guild.show = t; self:Update() end,
						order = 1,
					},
					showname = {
						type = 'toggle',
						name = L["Show name"], desc = L["Show name"],
						get = function() return self.db.profile.guild.showname end,
						set = function(t) self.db.profile.guild.showname = t; self:Update() end,
						order = 2,
					},
					showtotal = {
						type = 'toggle',
						name = L["Show Total"], desc = L["Show the total in the fubar plugin"],
						get = function() return self.db.profile.guild.showtotal end,
						set = function(t) self.db.profile.guild.showtotal = t; self:Update() end,
						order = 2,
					},
					offline = {
						type = 'toggle',
						name = L["Offline"], desc = L["Offline"],
						get = function() return self.db.profile.guild.offline end,
						set = function(t) self.db.profile.guild.offline = t; self:UpdateTooltip() end,
					},
					motd = {
						type = 'toggle',
						name = L["MotD"], desc = L["Message of the Day"],
						get = function() return self.db.profile.guild.motd end,
						set = function(t) self.db.profile.guild.motd = t; self:Update() end,
					},
					sort = {
						type = 'text',
						name = L["Sort"], desc = L["Sort"],
						get = function() return self.db.profile.guild.sort end,
						set = function(t) self.db.profile.guild.sort = t; self:Update() end,
						validate = {'NAME', 'LEVEL', 'ZONE', 'CLASS', 'RANK'},
					},
					columns = makeColumnsGroup('guild'),
					colors = makeColorsGroup('guild'),
				},
			},
			realid = {
				type = "group",
				name = L["Real ID"], desc = L["Real ID"],
				args = {
					show = {
						type = 'toggle',
						name = L['Show'], desc = L["Show"],
						get = function() return self.db.profile.realid.show end,
						set = function(t) self.db.profile.realid.show = t; self:Update() end,
						order = 1,
					},
					showtotal = {
						type = 'toggle',
						name = L["Show Total"], desc = L["Show the total in the fubar plugin"],
						get = function() return self.db.profile.realid.showtotal end,
						set = function(t) self.db.profile.realid.showtotal = t; self:Update() end,
						order = 2,
					},
					offline = {
						type = 'toggle',
						name = L['Offline'], desc = L['Offline'],
						get = function() return self.db.profile.realid.offline end,
						set = function(t) self.db.profile.realid.offline = t; self:UpdateTooltip() end,
						order = 3,
					},
					sort = {
						type = 'text',
						name = L["Sort"], desc = L["Sort"],
						get = function() return self.db.profile.realid.sort end,
						set = function(t) self.db.profile.realid.sort = t; self:Update() end,
						validate = {'NAME', 'LEVEL', 'ONLINE', 'ZONE', 'CLASS'},
					},
					columns = makeColumnsGroup('realid'),
					colors = makeColorsGroup('realid'),
				},
			},
			fubar = {
				type = "group",
				name = "FuBar",
				desc = L["FuBar options"],
				args = AceLibrary("FuBarPlugin-2.0"):GetAceOptionsDataTable(self),
				order = 300,
			},
		},
	}
	
	if AuldLangSyne then
		self.OnMenuRequest = AuldLangSyne.menu
	else
		self.OnMenuRequest = self.menu
	end
	
	self.db.profile.friends.sort = (self.db.profile.friends.sort):upper()
	self.db.profile.guild.sort = (self.db.profile.guild.sort):upper()
end

function Fu:OnEnable(first)
	friends.RegisterCallback(self, "Update")
	guild.RegisterCallback(self, "Update")
	self:RegisterBucketEvent("RosterLib_RosterChanged", 1, "Update")
	self:RegisterEvent("BN_CONNECTED","Update")
	self:RegisterEvent("BN_DISCONNECTED","Update")
	self:RegisterEvent("BN_SELF_ONLINE","Update")
	self:RegisterEvent("BN_FRIEND_LIST_SIZE_CHANGED","Update")
	self:RegisterEvent("BN_FRIEND_INFO_CHANGED","Update")
end

function Fu:HowLongAgo(when)
	if when then
		local ago = (time() - when)
		local minutes = math.ceil(ago / 60)
		if minutes > 59 then
			local hours = math.ceil(ago / 3600)
			if hours > 23 then
				local days = math.ceil(ago / 86400)
				lastseen = ((days > 1) and INT_GENERAL_DURATION_DAYS_P1 or INT_GENERAL_DURATION_DAYS):format(days)
			else
				lastseen = ((hours > 1) and INT_GENERAL_DURATION_HOURS_P1 or INT_GENERAL_DURATION_HOURS):format(hours)
			end
		else
			lastseen = ((minutes > 1) and INT_GENERAL_DURATION_MIN_P1 or INT_GENERAL_DURATION_MIN):format(minutes)
		end
		return lastseen
	else
		return ""
	end
end

------------------------------------------------------------------------
-- FuBar methods
------------------------------------------------------------------------

function Fu:OnClick()
	if IsAltKeyDown() then
		ToggleFriendsFrame(3) --guild
	else
		ToggleFriendsFrame(1) --friends
	end
end

function Fu:OnTextUpdate()
	local t
	if self.db.profile.friends.show or self.db.profile.guild.show or self.db.profile.realid.show then
		t = ''
		if BNFeaturesEnabled() and BNConnected() and self.db.profile.realid.show then
			local total, online = BNGetNumFriends()
			t = t.."|cff57a9dd"..online..(self.db.profile.realid.showtotal and ('/'..total) or '').."|r"
			if self.db.profile.friends.show or self.db.profile.guild.show then
				t = t..' '
			end
		end
		if self.db.profile.friends.show then
			t = t..'|cffff8800'..friends:GetNumOnline()..(self.db.profile.friends.showtotal and ('/'..friends:GetNumTotal()) or '')..'|r'
		end
		if IsInGuild() then
			t = t..'|cff00ff00'
			if self.db.profile.friends.show and self.db.profile.guild.show then
				t = t..' '
			end
			if self.db.profile.guild.showname then
				t = t..(guild:GetGuildName() or UNKNOWN)..' '
			end
			if self.db.profile.guild.show then
				t = t..guild:GetNumOnline()..(self.db.profile.guild.showtotal and ('/'..guild:GetNumTotal()) or '')
			end
			t = t..'|r'
		end
	else
		t = 'Auld'
	end
	self:SetText(t)
end

do
	local function editNote(name, type)
		local notes = AuldLangSyne and (AuldLangSyne:HasModule("Note") and AuldLangSyne:GetModule("Note")) or AuldLangSyne_Note
		if notes then
			if notes.editFrame:IsVisible() and notes.editFrame.name == name then
				notes.editFrame.name = ""
				notes.editFrame.type = ""
				notes.editFrame:Hide()
			else
				notes.editFrame.name = name
				notes.editFrame.type = type
				
				notes.editFrame.editbox:SetText(notes.db.realm[type][name] or '')
				notes.editFrame.edit_text:SetText(format(AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Note")["Editing note for %s"], name))
				notes.editFrame:Show()
			end
		end
	end
	local function click(name, type, presenceID)
		if IsAltKeyDown() then
			if presenceID then
				if CanCooperateWithToon(presenceID) then
					local _,_,_,toonName = BNGetFriendInfoByID(presenceID)
					if toonName then
						InviteUnit(toonName)
					end
				end
			else
				InviteUnit(name)
			end
		elseif IsControlKeyDown() then
			if not presenceID then -- no Real ID notes yet
				editNote(name, type)
			end
		else
			SetItemRef("player:"..name, "|Hplayer:"..name.."|h["..name.."|h", "LeftButton")
		end
	end
	function Fu:OnTooltipUpdate()
		tablet:SetHint(L["|cffeda55fClick|r to open the friends panel.  |cffeda55fAlt-Click|r to open the guild panel.  |cffeda55fClick|r a line to whisper a player. |cffeda55fControl-Click|r a line to edit a note. |cffeda55fAlt-Click|r a line to invite to a group."])
		
		local notes = AuldLangSyne and (AuldLangSyne:HasModule("Note") and AuldLangSyne:GetModule("Note").db.realm) or (AuldLangSyne_Note and AuldLangSyne_Note.db.realm)
		local info = AuldLangSyne and (AuldLangSyne:HasModule("Info") and AuldLangSyne:GetModule("Info").db.realm) or (AuldLangSyne_Info and AuldLangSyne_Info.db.realm)
		local realopts = self.db.profile.realid
		if realopts.show then
			local realcols = new()
			if realopts.columns.name then table.insert(realcols, 'Name') end
			if realopts.columns.toon then table.insert(realcols, 'Toon') end
			if realopts.columns.level then table.insert(realcols, 'Level') end
			if realopts.columns.class then table.insert(realcols, 'Class') end
			if realopts.columns.zone then table.insert(realcols, 'Zone') end
			if realopts.columns.realm then table.insert(realcols, 'Realm') end
			if realopts.columns.game then table.insert(realcols, 'Game') end
			if realopts.columns.status then table.insert(realcols, 'Status') end
			if realopts.columns.broadcast then table.insert(realcols, 'Broadcast') end

			local header = new()
			for i,col in ipairs(realcols) do
				if i == 1 then
					header['text'] = L[col]
				else
					header['text'..i] = L[col]
				end
			end

			local cat = tablet:AddCategory('id', 'realid', 'columns', #realcols, 'text', L['Real ID'])
			cat:AddLine(header)
			local tmp = new()
			local realids = new()
			for i = 1, BNGetNumFriends() do
				local presenceID, givenName, surname, toonName, toonID, client, isOnline, lastOnline, isAFK, isDND, broadcastText, noteText, isFriend, broadcastTime = BNGetFriendInfo(i)
				if realopts.offline or isOnline then
					local toonInfo	
					if BNGetNumFriendToons(i) >= 1 then
						toonInfo = new(BNGetFriendToonInfo(i, 1)) -- Hardcoded to first toon for now.
					end
					realids[i] = new(toonInfo, presenceID, givenName, surname, toonName, toonID, client, isOnline, lastOnline, isAFK, isDND, broadcastText, noteText, isFriend, broadcastTime)
				end
			end
		
			sort(realids, function(a, b) 
				if realopts.sort == "NAME" then
					if a[4] == b[4] then
						return a[3] < b[3]
					else
						return a[4] < b[4]
					end
				elseif realopts.sort == "LEVEL" then
					if a[1] and b[1] then
						if a[1][10] and b[1][10] then
							if a[1][10] < b[1][10] then
								return true
							end
						end
					end
					return false
				elseif realopts.sort == "ONLINE" then
					if not b[8] and a[8] then
						return true
					end
					return false
				elseif realopts.sort == "ZONE" then
					if a[1] and b[1] then
						if a[1][9] and b[1][9] then
							if a[1][9] < b[1][9] then
								return true
							end
						end
					end
					return false
				elseif realopts.sort == "CLASS" then
					if a[1] and b[1] then
						if a[1][7] and b[1][7] then
							if a[1][7] < b[1][7] then
								return true
							end
						end
					end
					return false
				end
			end)

			for i = 1, #realids do
				local toonInfo, presenceID, givenName, surname, toonName, toonID, client, isOnline, lastOnline, isAFK, isDND, broadcastText, noteText, isFriend, broadcastTime = unpack(realids[i], 1, 15)
				local hasFocus, _, _, realmName, faction, race, class, guild, zoneName, level
				if toonInfo then
					hasFocus, _, _, realmName, faction, race, class, guild, zoneName, level = unpack(toonInfo, 1, 10) 
				end
				tmp.name = string.format(BATTLENET_NAME_FORMAT, givenName, surname)
				if faction and client == BNET_CLIENT_WOW then
					tmp.toon = string.format("|TInterface\\TargetingFrame\\UI-PVP-%s:0:0:0:0:64:64:5:37:3:37|t %s", faction_name[faction], toonName)
				else
					tmp.toon = toonName
				end
				tmp.level = level
				tmp.class = class
				tmp.zone = zoneName
				tmp.realm = realmName
				tmp.game = client
				if isAFK then
					tmp.status = AFK
				elseif isDND then
					tmp.status = DND
				elseif not isOnline then
					tmp.status = self:HowLongAgo(lastOnline)	
				else
					tmp.status = nil
				end
				tmp.broadcast = broadcastText
				local line = new()
				for i,col in ipairs(realcols) do
					col = string.lower(col)
					local key
					local r, g, b = 1, 1, 1
					if i==1 then key = 'text' else key = 'text'..i end
					if class and realopts.colors[col] == 'class' then
						if not BC then BC = LibStub("LibBabble-Class-3.0"):GetReverseLookupTable() end
						local c = RAID_CLASS_COLORS[BC[class]:upper()]
						r,g, b = c.r, c.g, c.b
					elseif level and realopts.colors[col] == 'level' then
						local c = GetQuestDifficultyColor(level)
						r,g,b = c.r, c.g, c.b
					end
					if not isOnline then
						r,g,b = r*0.5, g*0.5, b*0.5
					end
					line[key] = tmp[col]
					line[key..'R'], line[key..'G'], line[key..'B'] = r,g,b
				end
				line['func'] = click; line['arg1'] = tmp.name; line['arg2'] = 'realid'; line['arg3'] = presenceID
				line['hasCheck'] = true
				if CanCooperateWithToon(presenceID) and roster:GetUnitIDFromName(toonName) then
					line['checked'] = true
				end
				cat:AddLine(line)
				del(line)
			end
			del(tmp)
			del(header)
			del(realcols)
			for i = 1, #realids do
				local toonInfo = realids[i][1]
				if toonInfo then
					del(toonInfo)
				end
				del(realids[i])
			end
			del(realids)
		end
		local fopts = self.db.profile.friends
		if fopts.show then
			local friendcols = new()
			if fopts.columns.name then table.insert(friendcols, 'Name') end
			if fopts.columns.level then table.insert(friendcols, 'Level') end
			if fopts.columns.class then table.insert(friendcols, 'Class') end
			if fopts.columns.zone then table.insert(friendcols, 'Zone') end
			if fopts.columns.status then table.insert(friendcols, 'Status') end
			if notes and fopts.columns.note then table.insert(friendcols, 'Note') end
			local header = new()
			for i,col in ipairs(friendcols) do
				if i==1 then
					header['text'] = L[col]
				else
					header['text'..i] = L[col]
				end
			end
			
			local cat = tablet:AddCategory('id', 'friends', 'columns', #friendcols, 'text', L["Friends"])
			cat:AddLine(header)
			local tmp = new()
			for name in friends:GetIterator(fopts.sort, fopts.offline) do
				local online = friends:IsFriendOnline(name)
				if fopts.offline or online then
					tmp.name = name
					tmp.level = friends:GetLevel(name)
					if tmp.level == 0 and info and info[name] and info[name].level then
						tmp.level = info[name].level
					end
					tmp.class = friends:GetClass(name)
					if tmp.class == UNKNOWN and info and info[name] and info[name].class then
						tmp.class = info[name].class
					end
					tmp.zone = friends:GetZone(name)
					tmp.status = friends:GetStatus(name)
					if notes and notes.friend[name] and notes.friend[name] ~= '' then
						local note = notes.friend[name]
						if strlen(note) > 50 then
							note = strsub(note, 0, 30)..'...'
						end
						tmp.note = "|cffff8800{"..note.."}|r "
					else
						tmp.note = ''
					end
					local line = new()
					for i,col in ipairs(friendcols) do
						col = string.lower(col)
						local key
						local r, g, b = 1, 1, 1
						if i==1 then key = 'text' else key = 'text'..i end
						if fopts.colors[col] == 'class' then
							if friends:GetEnglishClass(name) then
								r,g,b = friends:GetClassColor(name)
							elseif info and info[name] and info[name].class then
								if not BC then BC = LibStub("LibBabble-Class-3.0"):GetReverseLookupTable() end
								local c = RAID_CLASS_COLORS[BC[info[name].class]:upper()]
								r,g,b = c.r, c.g, c.b
							else
								r,g,b = 0.8, 0.8, 0.8
							end
						elseif fopts.colors[col] == 'level' then
							local c = GetQuestDifficultyColor(tmp.level)
							r,g,b = c.r, c.g, c.b
						end
						if not online then
							r, g, b = r * 0.5, g * 0.5, b * 0.5
						end
						line[key] = tmp[col]
						line[key..'R'], line[key..'G'], line[key..'B'] = r, g, b
					end
					line['func'] = click; line['arg1'] = name; line['arg2'] = 'friend'
					line['hasCheck'] = true
					if roster:GetUnitIDFromName(name) then
						line['checked'] = true
					end
					cat:AddLine(line)
					del(line)
				end
			end
			del(tmp)
			del(header)
			del(friendcols)
		end
		
		local gopts = self.db.profile.guild
		if IsInGuild() and gopts.show then
			local guildcols = new()
			if gopts.columns.name then table.insert(guildcols, 'Name') end
			if gopts.columns.level then table.insert(guildcols, 'Level') end
			if gopts.columns.class then table.insert(guildcols, 'Class') end
			if gopts.columns.zone then table.insert(guildcols, 'Zone') end
			if gopts.columns.rank then table.insert(guildcols, 'Rank') end
			if gopts.columns.status then table.insert(guildcols, 'Status') end
			if gopts.columns.note then table.insert(guildcols, 'Note') end
			local header = new()
			for i,col in ipairs(guildcols) do
				if i==1 then
					header['text'] = L[col]
				else
					header['text'..i] = L[col]
				end
			end
			
			if gopts.motd then
				local cat = tablet:AddCategory('id', 'motd', 'text', L["MotD"])
				cat:AddLine('text', GetGuildRosterMOTD(), 'wrap', true)
			end
			
			local cat = tablet:AddCategory('id', 'guild', 'columns', #guildcols, 'text', L["Guild"])
			cat:AddLine(header)
			local tmp = new()
			for name in guild:GetIterator(gopts.sort, gopts.offline) do
				local online = guild:IsMemberOnline(name)
				if gopts.offline or online then
					tmp.name = name
					tmp.level = guild:GetLevel(name)
					tmp.class = guild:GetClass(name)
					tmp.zone = guild:GetZone(name)
					tmp.status = guild:GetStatus(name)
					tmp.rank = guild:GetRank(name)
					local n = ''
					if notes and notes.guild[name] and notes.guild[name] ~= '' then
						local note = notes.guild[name]
						if strlen(note) > 30 then
							note = strsub(note, 0, 30)..'...'
						end
						n = n .. "|cff00aa00{"..note.."}|r "
					end
					local gnote = guild:GetNote(name)
					if gnote and gnote ~= '' then
						n = n .. "|cff00ff00["..gnote.."]|r "
					end
					local gonote = guild:GetOfficerNote(name)
					if gonote and gonote ~= '' then
						n = n .. "|cff00ffff["..gonote.."]|r "
					end
					tmp.note = n
					
					local line = new()
					for i,col in ipairs(guildcols) do
						col = string.lower(col)
						local key
						local r, g, b = 1, 1, 1
						if i==1 then key = 'text' else key = 'text'..i end
						if gopts.colors[col] == 'class' then
							r,g,b = guild:GetClassColor(name)
						elseif gopts.colors[col] == 'level' then
							local c = GetQuestDifficultyColor(tmp.level)
							r,g,b = c.r, c.g, c.b
						end
						if not online then
							r, g, b = r * 0.5, g * 0.5, b * 0.5
						end
						line[key] = tmp[col]
						line[key..'R'], line[key..'G'], line[key..'B'] = r, g, b
					end
					line['func'] = click; line['arg1'] = name; line['arg2'] = 'guild'
					line['hasCheck'] = true
					if roster:GetUnitIDFromName(name) then
						line['checked'] = true
					end
					cat:AddLine(line)
					del(line)
				end
			end
			del(tmp)
			del(header)
			del(guildcols)
		end
	end
end
