﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Fu")
L:RegisterTranslations("koKR", function()
	return {
		["Columns"] = "항목",
		["Colors"] = "색상",
		["FuBar plugin options"] = "FuBar 플러그인 설정",
		["Friends"] = "친구",
		["Show"] = "표시",
		["Show name"] = "이름 표시",
		["Show Total"] = "전체 표시",
		["Show the total in the fubar plugin"] = "FuBar 플러그인에 전체를 표시합니다.",
		["Offline"] = "오프라인",
		["Sort"] = "정렬",
		["Guild"] = "길드",
		["MotD"] = "길드 메세지",
		["Message of the Day"] = "오늘의 길드 메세지",
		["FuBar options"] = "FuBar 설정",

		["|cffeda55fClick|r to open the friends panel.  |cffeda55fAlt-Click|r to open the guild panel.  |cffeda55fClick|r a line to whisper a player. |cffeda55fControl-Click|r a line to edit a note. |cffeda55fAlt-Click|r a line to invite to a group."] = "친구창을 열려면 |cffeda55f클릭|r하세요.  길드창을 열려면 |cffeda55fALT-클릭|r하세요.  플레이어에게 귓속말을 보내려면 해당 줄을 |cffeda55f클릭|r하세요. 메모를 편집하려면 해당 줄을 |cffeda55fCTRL-클릭|r하세요. 파티에 초대하려면 해당 줄을 |cffeda55fALT-클릭|r하세요.",

		["Name"] = "이름",
		["Level"] = "레벨",
		["Class"] = "직업",
		["Zone"] = "지역",
		["Status"] = "상태",
		["Note"] = "메모",
		["Rank"] = "등급",
	}
end)