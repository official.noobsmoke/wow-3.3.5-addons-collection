local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Fu")
L:RegisterTranslations("enUS", function()
	return {
		["Columns"] = true,
		["Colors"] = true,
		["FuBar plugin options"] = true,
		["Friends"] = true,
		["Show"] = true,
		["Show name"] = true,
		["Show Total"] = true,
		["Show the total in the fubar plugin"] = true,
		["Offline"] = true,
		["Sort"] = true,
		["Guild"] = true,
		["MotD"] = true,
		["Message of the Day"] = true,
		["FuBar options"] = true,

		["|cffeda55fClick|r to open the friends panel.  |cffeda55fAlt-Click|r to open the guild panel.  |cffeda55fClick|r a line to whisper a player. |cffeda55fControl-Click|r a line to edit a note. |cffeda55fAlt-Click|r a line to invite to a group."] = true,

		["Name"] = true,
		["Level"] = true,
		["Class"] = true,
		["Zone"] = true,
		["Status"] = true,
		["Note"] = true,
		["Rank"] = true,
		["Real ID"] = true,
		["Toon"] = true,
		["Realm"] = true,
		["Game"] = true,
		["Broadcast"] = true,
	}
end)
