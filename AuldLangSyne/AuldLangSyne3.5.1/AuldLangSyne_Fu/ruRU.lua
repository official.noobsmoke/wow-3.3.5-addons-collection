﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Fu")
L:RegisterTranslations("ruRU", function()
    return {
        ["Columns"] = "Столбцы",
        ["Colors"] = "Цвета",
        ["FuBar plugin options"] = "Опции модуля FuBar",
        ["Friends"] = "Друзья",
        ["Show"] = "Показать",
        ["Show name"] = "Показать имя",
        ["Show Total"] = "Показать Итоги",
        ["Show the total in the fubar plugin"] = "Показать Итоги в модуле FuBar.",
        ["Offline"] = "Оффлайн",
        ["Sort"] = "Сортировка",
        ["Guild"] = "Гильдия",
        ["MotD"] = "Сообщение дня",
        ["Message of the Day"] = "Сообщение дня гильдии.",
        ["FuBar options"] = "Оции FuBar",

        ["|cffeda55fClick|r to open the friends panel.  |cffeda55fAlt-Click|r to open the guild panel.  |cffeda55fClick|r a line to whisper a player. |cffeda55fControl-Click|r a line to edit a note. |cffeda55fAlt-Click|r a line to invite to a group."] = "|cffeda55fКлик|r открыть спискок друзей.  |cffeda55fAlt-Клик|r открыть панель гильдии.  |cffeda55fКлик|r по строке для шопота. |cffeda55fControl-Клик|r по строке для редактирования примечания. |cffeda55fAlt-Клик|r по строке для приглашения в группу.",

        ["Name"] = "Имя",
        ["Level"] = "Уровень",
        ["Class"] = "Класс",
        ["Zone"] = "Зона",
        ["Status"] = "Статус",
        ["Note"] = "Примечание",
        ["Rank"] = "Ранг",
    }
end)
