﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Fu")
L:RegisterTranslations("zhTW", function()
	return {
		["Columns"] = "欄位",
		["Colors"] = "顏色",
		["FuBar plugin options"] = "FuBar 插件選項",
		["Friends"] = "好友",
		["Show"] = "顯示",
		["Show name"] = "顯示名字",
		["Show Total"] = "顯示總數",
		["Show the total in the fubar plugin"] = "FuBar 上顯示總數",
		["Offline"] = "離線",
		["Sort"] = "排序",
		["Guild"] = "公會",
		["MotD"] = "本日訊息",
		["Message of the Day"] = "本日公會訊息",
		["FuBar options"] = "FuBar 選項",

		["|cffeda55fClick|r to open the friends panel.  |cffeda55fAlt-Click|r to open the guild panel.  |cffeda55fClick|r a line to whisper a player. |cffeda55fControl-Click|r a line to edit a note. |cffeda55fAlt-Click|r a line to invite to a group."] = "|cffeda55f點擊|r開啟好友列表。|cffeda55fAlt-點擊|r開啟公會成員列表。|cffeda55f點擊|r列表上的玩家進行密語。|cffeda55fCtrl-點擊|r可修改註解。|cffeda55fAlt-點擊|r列表上的玩家可邀請對方。",

		["Name"] = "名字",
		["Level"] = "等級",
		["Class"] = "職業",
		["Zone"] = "區域",
		["Status"] = "狀態",
		["Note"] = "註記",
		["Rank"] = "公會等級",
	}
end)