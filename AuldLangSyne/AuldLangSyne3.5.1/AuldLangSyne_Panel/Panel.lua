local VERSION = tonumber(("$Revision: 50335 $"):match("%d+"))

local _G = getfenv(0)

local AuldLangSyne = AuldLangSyne
local Panel, Note
if AuldLangSyne then
	Panel = AuldLangSyne:NewModule("Panel", "AceHook-2.1", "AceConsole-2.0")
	AuldLangSyne:SetModuleDefaultState("Panel", false)
	if AuldLangSyne.revision < VERSION then
		AuldLangSyne.version = "r" .. VERSION
		AuldLangSyne.revision = VERSION
		AuldLangSyne.date = ("$Date: 2007-09-29 17:46:09 -0700 (Sat, 29 Sep 2007) $"):match("%d%d%d%d%-%d%d%-%d%d")
	end
else
	Panel = AceLibrary("AceAddon-2.0"):new("AceEvent-2.0", "AceHook-2.1", "AceConsole-2.0", "AceDB-2.0")
	AuldLangSyne_Panel = Panel -- since this isn't a module in this branch, global for access
end

function Panel:OnInitialize(name)
	if AuldLangSyne:HasModule("Note") then
		Note = AuldLangSyne:GetModule("Note")
	else
		self:Print("Note module not found; disabling self")
		AuldLangSyne:ToggleModuleActive(self, false)
	end
end

function Panel:OnEnable(first)
	if (not Note) or (not AuldLangSyne:IsModuleActive("Note")) then
		self:Print("Note module not found; disabling self")
		AuldLangSyne:ToggleModuleActive(self, false)
		return
	end
	if first then
		self:MakeNoteTabs()
	end
	self:SecureHook(Note, "SaveNote")
end

function Panel:OnDisable()
	--self:HideNoteTabs()
end

-- Window:
local function clear(t)
	for k in pairs(t) do
		t[k] = nil
	end
	t[''] = true
	t[''] = nil
	return t
end

local selectedNote = 0
local selectedName
local noteCache = {}
local NoteList_Update = function()
	local notes = Note.db.realm.generic
	local numNotes = 0
	--Build the note cache
	clear(noteCache)
	for name, note in pairs(notes) do
		numNotes = numNotes + 1
		table.insert(noteCache, name)
	end
	table.sort(noteCache)
	
	--[[if numNotes > 0 then
		if selectedNote == 0 then
			selectedNote = 1
		end
	end--]]
	
	local offset = FauxScrollFrame_GetOffset(ALSNoteScrollFrame)
	for i=1, IGNORES_TO_DISPLAY, 1 do
		local index = i + offset
		local nameText = _G["ALSNoteFrameButton"..i.."Name"]
		local button = _G["ALSNoteFrameButton"..i]
		if noteCache[index] then
			button.name = noteCache[index]
			nameText:SetText(noteCache[index] .. ': ' .. notes[noteCache[index]])
			nameText:SetJustifyH("LEFT")
			nameText:SetWidth(button:GetWidth())
			nameText:SetHeight(button:GetHeight())
		end
		button:SetID(index)
		-- Update the highlight
		if index == selectedNote then
			selectedName = noteCache[index]
			button:LockHighlight()
		else
			button:UnlockHighlight()
		end
		
		if index > numNotes then
			button:Hide()
		else
			button:Show()
		end
	end
	
	-- ScrollFrame stuff
	FauxScrollFrame_Update(ALSNoteScrollFrame, numNotes, IGNORES_TO_DISPLAY, FRIENDS_FRAME_IGNORE_HEIGHT)
end
function Panel:MakeNoteTabs()
	local tabID = FriendsFrame.numTabs + 1
	local noteframe = CreateFrame("Frame", "ALSNoteFrame", FriendsFrame)
	noteframe:SetAllPoints(FriendsFrame)
	noteframe:Hide()
	
	local buttonclick = function()
		selectedNote = this:GetID()
		NoteList_Update()
		PlaySound("igMainMenuOptionCheckBoxOn")
	end
	
	local anchor
	for i=1, IGNORES_TO_DISPLAY, 1 do
		local line = CreateFrame("Button", "ALSNoteFrameButton"..i, noteframe, "FriendsFrameIgnoreButtonTemplate")
		line:SetScript("OnClick", buttonclick)
		if anchor then
			line:SetPoint("TOP", anchor, "BOTTOM", 0, 0)
		else
			line:SetPoint("TOPLEFT", FriendsFrame, "TOPLEFT", 23, -80)
		end
		line.type = "generic"
		line:SetScript("OnEnter", Note.ButtonEnter)
		line:SetScript("OnLeave", Note.ButtonLeave)
		line:SetScript("OnClick", Note.ButtonClick)
		anchor = line
	end
	local scroll = CreateFrame("ScrollFrame", "ALSNoteScrollFrame", noteframe, "FauxScrollFrameTemplate")
	scroll:SetWidth(296)
	scroll:SetHeight(332)
	scroll:SetPoint("TOPRIGHT", FriendsFrame, "TOPRIGHT", -67, -75)
	local makeTexture = function(path, w, h, point, anchorframe, relativepoint, x, y, l, r, t, b)
		local tex = scroll:CreateTexture()
		tex:SetDrawLayer("ARTWORK")
		tex:SetTexture(path)
		tex:SetWidth(w)
		tex:SetHeight(h)
		tex:SetPoint(point, anchorframe, relativepoint, x, y)
		tex:SetTexCoord(l, r, t, b)
	end
	makeTexture("Interface\PaperDollInfoFrame\UI-Character-ScrollBar", 31, 256,
		"TOPLEFT", scroll, "TOPRIGHT", -2, 5, 0, 0.484375, 0, 1)
	makeTexture("Interface\PaperDollInfoFrame\UI-Character-ScrollBar", 31, 106,
		"BOTTOMLEFT", scroll, "BOTTOMRIGHT", -2, -2, 0.515625, 1, 0, 0.4140625)
	scroll:SetScript("OnVerticalScroll", function()
		FauxScrollFrame_OnVerticalScroll(FRIENDS_FRAME_IGNORE_HEIGHT, NoteList_Update)
	end)
	
	local makeButton = function(name, text, point, anchorframe, relativepoint, x, y)
		local button = CreateFrame("Button", name, noteframe, "UIPanelButtonTemplate")
		button:SetText(text)
		button:SetWidth(131)
		button:SetHeight(21)
		button:SetPoint(point, anchorframe, relativepoint, x, y)
		button:SetFrameLevel(button:GetFrameLevel() + 3)
		return button
	end
	local addButton = makeButton("AWSNoteFrameAddButton", "Add Note", "BOTTOMLEFT", FriendsFrame, "BOTTOMLEFT", 17, 81)
	addButton:SetScript("OnClick", function()
		if UnitExists('target') then
			this.name = UnitName('target')
			this.type = 'generic'
			Note.ButtonClick()
		end
	end)
	--[[local removeButton = makeButton("AWSNoteFrameRemoveButton", "Remove Note", "LEFT", addButton, "RIGHT", 62, 0)
	removeButton:SetScript("OnClick", function()
		Note.db.realm.generic[selectedName] = nil
		NoteList_Update()
	end)--]]
	
	local tab = CreateFrame("Button", "FriendsFrameTab"..tabID, _G['FriendsFrame'], "FriendsFrameTabTemplate")
	tab:SetPoint("LEFT", _G["FriendsFrameTab"..(tabID-1)], "RIGHT", -14, 0)
	tab:SetText("Notes")
	tab:SetID(tabID)
	
	PanelTemplates_SetNumTabs(FriendsFrame, tabID)
	PanelTemplates_UpdateTabs(FriendsFrame)
	
	table.insert(FRIENDSFRAME_SUBFRAMES, "ALSNoteFrame")
	local oFFU = FriendsFrame_Update
	FriendsFrame_Update = function()
		if FriendsFrame.selectedTab == tabID then
			FriendsTabHeader:Hide()
			FriendsFrameTopLeft:SetTexture("Interface\\PaperDollInfoFrame\\UI-Character-General-TopLeft")
			FriendsFrameTopRight:SetTexture("Interface\\PaperDollInfoFrame\\UI-Character-General-TopRight")
			FriendsFrameBottomLeft:SetTexture("Interface\\FriendsFrame\\UI-IgnoreFrame-BotLeft")
			FriendsFrameBottomRight:SetTexture("Interface\\FriendsFrame\\UI-IgnoreFrame-BotRight")
			FriendsFrameTitleText:SetText("Notes")
			FriendsFrame_ShowSubFrame("ALSNoteFrame")
			NoteList_Update()
		else
			return oFFU()
		end
	end
end

function Panel:SaveNote(obj, name, note, ntype)
	if ntype == 'generic' then
		NoteList_Update()
	end
end
