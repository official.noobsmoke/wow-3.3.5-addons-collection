## Interface: 30300
## Title: AuldLangSyne |r[|cffeda55fPanel|r]
## Notes: Add a notes panel to the social frame
## Version: 2.0
## Author: Kemayo
## X-Website: http://kemayo.wowinterface.com
## X-eMail: kemayo@gmail.com
## X-RelSite-WoWI: 5320
## X-WoWIPortal: kemayo
## X-Category: Interface Enhancements
## X-AuldLangSyne-DefaultState: 0
## SavedVariables: AuldLangSyneNoteDB
## OptionalDeps: AuldLangSyne, AuldLangSyne_Note, Ace2, Deformat, FuBar, TabletLib, DewdropLib
## X-Curse-Packaged-Version: v3.3.5.1
## X-Curse-Project-Name: AuldLangSyne
## X-Curse-Project-ID: auldlangsyne
## X-Curse-Repository-ID: wow/auldlangsyne/mainline


Panel.lua
