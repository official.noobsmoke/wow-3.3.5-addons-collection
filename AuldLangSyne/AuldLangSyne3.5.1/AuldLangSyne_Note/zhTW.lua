﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Note")
L:RegisterTranslations("zhTW", function()
	return {
		["ctimport"] = "由 CT 匯入",
		["Import notes for this realm from CT_PlayerNotes"] = "由 CT_PlayerNotes 匯入本伺服器的資料。",
		["Note in player tooltip"] = "玩家提示視窗的註記",
		["Turns display of player notes in tooltips on and off."] = "在玩家的提示視窗中顯示示註記與否。",
		["Note on logon"] = "登入時顯示註記",
		["Turns display of player notes on logon on and off."] = "玩家登入登出時，是否顯示登入時顯示註記。",
		["Note on /who"] = "/who 時顯示註記",
		["Turns display of player notes when you /who them on and off."] = "輸入 /who 查詢時顯示註記與否。",

		["Friend: "] = "好友: ",
		["Guild: "] = "公會: ",
		["Ignore: "] = "忽略: ",
		["Note: "] = "註記: ",
		["Set note for %s: %s"] = "設定 %s 的註記: %s",
		["Removed note from %s.  (Was: %s)"] = "移除 %s 的註解。（目前為: %s ）",
		["Editing note for %s"] = "修改 %s 的註記",
		["Click to edit"] = "點擊開始修改",
		["Edit note"] = "修改註記",
		["Confirm"] = "確認",
		["Cancel"] = "取消",
		["Imported %d player notes from CT_Core"] = "已由 CT_Core 匯入 %d 筆角色的註記。",
		["Imported %d guild notes from CT_Core"] = "已由 CT_Core 匯入 %d 筆公會註解註記。",
		["Imported %d ignore notes from CT_Core"] = "已由 CT_Core 匯入 %d 筆忽略列表的註記。",
		["CT_Core is not loaded"] = "CT_Core 並沒有載入",

	}
end)