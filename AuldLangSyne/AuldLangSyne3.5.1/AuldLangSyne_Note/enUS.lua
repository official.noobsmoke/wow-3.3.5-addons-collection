local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Note")
L:RegisterTranslations("enUS", function()
	return {
		["ctimport"] = true,
		["Import notes for this realm from CT_PlayerNotes"] = true,
		["Note in player tooltip"] = true,
		["Turns display of player notes in tooltips on and off."] = true,
		["Note on logon"] = true,
		["Turns display of player notes on logon on and off."] = true,
		["Note on /who"] = true,
		["Turns display of player notes when you /who them on and off."] = true,
		["Add notes to player menus"] = true,
		["Adds an 'edit note' entry to the right-click menu for players"] = true,

		["Friend: "] = true,
		["Guild: "] = true,
		["Ignore: "] = true,
		["Note: "] = true,
		["Set note for %s: %s"] = true,
		["Removed note from %s.  (Was: %s)"] = true,
		["Editing note for %s"] = true,
		["Click to edit"] = true,
		["Edit note"] = true,
		["Confirm"] = true,
		["Cancel"] = true,
		["Imported %d player notes from CT_Core"] = true,
		["Imported %d guild notes from CT_Core"] = true,
		["Imported %d ignore notes from CT_Core"] = true,
		["CT_Core is not loaded"] = true,

	}
end)