local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Note")
L:RegisterTranslations("deDE", function()
    return {
        ["Import notes for this realm from CT_PlayerNotes"] = "Importiere Notizen f\195\188r diesen Realm von CT_PlayerNotes.",
        ["Note in player tooltip"] = "Notiz im Tooltip",
        ["Turns display of player notes in tooltips on and off."] = "Schaltet die Anzeige von Spielernotizen in Tooltips ein und aus.",
        ["Note on logon"] = "Notiz beim Einloggen",
        ["Turns display of player notes on logon on and off."] = "Schaltet die Anzeige von Spielernotizen beim Einloggen ein und aus.",
        ["Note on /who"] = "Notiz beim /who",
        ["Turns display of player notes when you /who them on and off."] = "Schaltet die Anzeige von Spielernotizen beim /who ein und aus.",
        ["Add notes to player menus"] = "Notizfunktion im Spielermen\195\188",
        ["Adds an 'edit note' entry to the right-click menu for players"] = "F\195\188gt einen 'Notiz bearbeiten' Eintrag in das Rechtsklick-Men\195\188 f\195\188r Spieler hinzu.",

        ["Friend: "] = "Freund: ",
        ["Guild: "] = "Gilde: ",
        ["Ignore: "] = "Ignorieren: ",
        ["Note: "] = "Notiz: ",
        ["Set note for %s: %s"] = "Notiz f�r %s bearbeiten: %s",
        ["Removed note from %s.  (Was: %s)"] = "Notiz f�r %s gel\195\182scht.  (War: %s)",
        ["Editing note for %s"] = "Bearbeitete Notiz f�r %s",
        ["Click to edit"] = "Zum Bearbeiten klicken",
        ["Edit note"] = "Notiz bearbeiten",
        ["Confirm"] = "Best\195\164tigen",
        ["Cancel"] = "Abbrechen",
        ["Imported %d player notes from CT_Core"] = "%d Spielernotizen von CT_Core importiert",
        ["Imported %d guild notes from CT_Core"] = "%d Gildennotizen von CT_Core importiert",
        ["Imported %d ignore notes from CT_Core"] = "%d Ignorenotizen von CT_Core importiert",
        ["CT_Core is not loaded"] = "CT_Core ist nicht geladen",

    }
end)
