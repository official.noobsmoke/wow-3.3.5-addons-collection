﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Note")
L:RegisterTranslations("koKR", function()
	return {
		["ctimport"] = "CT 불러오기",
		["Import notes for this realm from CT_PlayerNotes"] = "CT_PlayerNotes 에서 이 서버에 대한 메모를 불러옵니다.",
		["Note in player tooltip"] = "플레이어 툴팁 메모",
		["Turns display of player notes in tooltips on and off."] = "툴팁에 플레이어 메모를 표시를 켜거나 끕니다.",
		["Note on logon"] = "접속 메모",
		["Turns display of player notes on logon on and off."] = "접속시 플레이어 메모를 표시를 켜거나 끕니다.",
		["Note on /who"] = "/누구 메모",
		["Turns display of player notes when you /who them on and off."] = "/누구 실행 시 플레이어 메모 표시를 켜거나 끕니다.",
		["Add notes to player menus"] = "플레이어 메뉴에 노트 추가",
		["Adds an 'edit note' entry to the right-click menu for players"] = "플레이어 프레임을 오른쪽 클릭하여 노트를 추가합니다.",

		["Friend: "] = "친구: ",
		["Guild: "] = "길드: ",
		["Ignore: "] = "제외: ",
		["Note: "] = "노트: ",
		["Set note for %s: %s"] = "%s의 노트 설정: %s",
		["Removed note from %s.  (Was: %s)"] = "%s의 노트가 삭제되었습니다. (Was: %s)",
		["Editing note for %s"] = "%s 메모 수정",
		["Click to edit"] = "클릭 : 수정",
		["Edit note"] = "메모 편집",
		["Confirm"] = "확인",
		["Cancel"] = "취소",
		["Imported %d player notes from CT_Core"] = "CT_Core에서 %d 개의 친구 목록을 불러옵니다.",
		["Imported %d guild notes from CT_Core"] = "CT_Core에서 %d 개의 길드원 목록을 불러옵니다.",
		["Imported %d ignore notes from CT_Core"] = "CT_Core에서 %d 개의 차단 목록을 불러옵니다.",
		["CT_Core is not loaded"] = "CT_Core 애드온이 실행되지 않았습니다.",

	}
end)