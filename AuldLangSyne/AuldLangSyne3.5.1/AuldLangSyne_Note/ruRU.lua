﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Note")
L:RegisterTranslations("ruRU", function()
    return {
        ["ctimport"] = "CT_PlayerNotes",
        ["Import notes for this realm from CT_PlayerNotes"] = "Импортировать примечания персонажей для этого мира из модуля CT_PlayerNotes.",
        ["Note in player tooltip"] = "Примечание в подсказке",
        ["Turns display of player notes in tooltips on and off."] = "Показывать примечания персонажа в подсказке.",
        ["Note on logon"] = "Примечание при входе в игру",
        ["Turns display of player notes on logon on and off."] = "Показывать примечание в чате при входе игрока в игру.",
        ["Note on /who"] = "Примечание для /who",
        ["Turns display of player notes when you /who them on and off."] = "Показывать примечание персонажа для команды /who.",
        ["Add notes to player menus"] = "Добавить примечание к меню персонажа",
        ["Adds an 'edit note' entry to the right-click menu for players"] = "Добавляет команду 'Изменить примечание' к меню персонажа, которое вызывается по правой кнопке мыши.",

        ["Friend: "] = "Друзья: ",
        ["Guild: "] = "Гильдия: ",
        ["Ignore: "] = "Игнорируемые: ",
        ["Note: "] = "Примечание: ",
        ["Set note for %s: %s"] = "Установить примечание для %s: %s",
        ["Removed note from %s.  (Was: %s)"] = "Удалить примечание %s (было: %s).",
        ["Editing note for %s"] = "Изменить примечание для %s",
        ["Click to edit"] = "Клик для изменения",
        ["Edit note"] = "Изменить примечание",
        ["Confirm"] = "Подтвердить",
        ["Cancel"] = "Отменить",
        ["Imported %d player notes from CT_Core"] = "Импортировано %d примечаний игроков из CT_Core",
        ["Imported %d guild notes from CT_Core"] = "Импортировано %d примечаний согильдийцев из CT_Core",
        ["Imported %d ignore notes from CT_Core"] = "Импортировано %d примечаний игнорируемых CT_Core",
        ["CT_Core is not loaded"] = "CT_Core не загружен",

    }
end)
