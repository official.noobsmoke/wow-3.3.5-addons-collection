﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Backup")
L:RegisterTranslations("zhTW", function()
	return {
		["Backup"] = "備份",
		["Options for backing up and restoring your friends list"] = "可用來備份或是還原你的好友列表。",
		["Save"] = "儲存",
		["Take a snapshot of your current friends list"] = "將你目前的好友列表存起來。",
		["Load"] = "載入",
		["Restore a snapshot of your friends list, wiping out your current friends list"] = "從之前儲存的好友列表中還原，目前的好友列表將會清除。",

		["This slot is currently empty."] = "目前是空的。",
		["Backed up %d friends to slot %d"] = "備份 %d 名好友到位置 %d 。",
	}
end)