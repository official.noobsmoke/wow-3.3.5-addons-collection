local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Backup")
L:RegisterTranslations("enUS", function()
	return {
		["Backup"] = true,
		["Options for backing up and restoring your friends list"] = true,
		["Save"] = true,
		["Take a snapshot of your current friends list"] = true,
		["Load"] = true,
		["Restore a snapshot of your friends list, wiping out your current friends list"] = true,

		["This slot is currently empty."] = true,
		["Backed up %d friends to slot %d"] = true,
	}
end)