﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Backup")
L:RegisterTranslations("koKR", function()
	return {
		["Backup"] = "백업",
		["Options for backing up and restoring your friends list"] = "백업과 친구 목록 복원에 대한 설정입니다.",
		["Save"] = "저장",
		["Take a snapshot of your current friends list"] = "현재 친구 목록을 저장합니다.",
		["Load"] = "불러오기",
		["Restore a snapshot of your friends list, wiping out your current friends list"] = "현재 친구 목록을 삭제하고, 저장된 친구 목록을 복원합니다.",

		["This slot is currently empty."] = "해당 슬롯은 현재 비어있습니다.",
		["Backed up %d friends to slot %d"] = "%d명의 친구가 %d 슬롯으로 백업되었습니다.",
	}
end)