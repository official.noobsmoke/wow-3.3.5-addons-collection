﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Backup")
L:RegisterTranslations("deDE", function()
    return {
        ["Backup"] = "Backup",
        ["Options for backing up and restoring your friends list"] = "Optionen, um deine Freundesliste zu sichern und wiederherzustellen.",
        ["Save"] = "Sichern",
        ["Take a snapshot of your current friends list"] = "Kopie deiner aktuellen Freundesliste sichern.",
        ["Load"] = "Laden",
        ["Restore a snapshot of your friends list, wiping out your current friends list"] = "Stellt eine Kopie deiner Freundesliste wieder her, aber l\195\182scht deine Aktuelle.",
        
        ["This slot is currently empty."] = "Dieser Slot ist momentan leer.",
        ["Backed up %d friends to slot %d"] = "%d Freunde in Slot %d gesichert.",
    }
end)
