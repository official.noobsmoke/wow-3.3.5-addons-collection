﻿local L = AceLibrary("AceLocale-2.2"):new("AuldLangSyne_Backup")
L:RegisterTranslations("ruRU", function()
    return {
        ["Backup"] = "Бэкап",
        ["Options for backing up and restoring your friends list"] = "Настройки для копирования и восстановления списка друзей.",
        ["Save"] = "Сохранить",
        ["Take a snapshot of your current friends list"] = "Сделать копию текущего списка друзей.",
        ["Load"] = "Восстановить",
        ["Restore a snapshot of your friends list, wiping out your current friends list"] = "Восстановить копию списка друзей в текущий список друзей (текущий список перезаписывается).",
        
        ["This slot is currently empty."] = "Этот слот пуст.",
        ["Backed up %d friends to slot %d"] = "Сохраняю список из %d друзей(га) в слот %d.",
    }
end)
