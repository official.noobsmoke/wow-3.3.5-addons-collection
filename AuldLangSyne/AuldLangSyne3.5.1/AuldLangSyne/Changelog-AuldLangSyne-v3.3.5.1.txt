------------------------------------------------------------------------
r265 | shefki | 2010-07-02 23:56:17 +0000 (Fri, 02 Jul 2010) | 1 line
Changed paths:
   A /tags/v3.3.5.1 (from /trunk:264)

Tagging as v3.3.5.1
------------------------------------------------------------------------
r264 | shefki | 2010-07-02 23:53:12 +0000 (Fri, 02 Jul 2010) | 2 lines
Changed paths:
   M /trunk/Fu/Fu.lua

Add configurable sorting for Real ID friends.

------------------------------------------------------------------------
r263 | shefki | 2010-07-02 21:33:39 +0000 (Fri, 02 Jul 2010) | 5 lines
Changed paths:
   M /trunk/Sync/Sync.lua

Ticket #32: is already your friend bug.

The name of the friends weren't available when we tried to sync so the Sync
module tried to readd friends you already had.

------------------------------------------------------------------------
r262 | shefki | 2010-07-02 21:01:54 +0000 (Fri, 02 Jul 2010) | 2 lines
Changed paths:
   M /trunk/Fu/enUS.lua

Add the "Broadcast" localization I forgot to add to the Fu Module.

------------------------------------------------------------------------
r261 | shefki | 2010-07-01 23:45:41 +0000 (Thu, 01 Jul 2010) | 2 lines
Changed paths:
   M /trunk/Note/Note.lua

Workaround for the attempt to index local 'fb' error.

------------------------------------------------------------------------
r259 | shefki | 2010-07-01 19:51:38 +0000 (Thu, 01 Jul 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Put the status tag back in the defaults, need it for last online time for now.

------------------------------------------------------------------------
r258 | shefki | 2010-06-28 05:51:48 +0000 (Mon, 28 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Fu/Fu.lua

Add a faction icon in front of Real ID Toon Names 

------------------------------------------------------------------------
r257 | shefki | 2010-06-28 05:25:01 +0000 (Mon, 28 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Fu/Fu.lua

Fix an error when the tooltip is up and a Real ID friend logs out. 

------------------------------------------------------------------------
r256 | shefki | 2010-06-28 05:16:24 +0000 (Mon, 28 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Fu/enUS.lua

Oops forgot to commit the localization file.

------------------------------------------------------------------------
r255 | shefki | 2010-06-28 04:48:25 +0000 (Mon, 28 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Fu/Fu.lua

First stab at adding Real ID support Fu module.

------------------------------------------------------------------------
r254 | shefki | 2010-06-28 03:44:22 +0000 (Mon, 28 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Remove status tag from default first line since Blizzard now has the icon to show.

------------------------------------------------------------------------
r253 | shefki | 2010-06-28 02:44:10 +0000 (Mon, 28 Jun 2010) | 2 lines
Changed paths:
   M /trunk/AuldLangSyne.toc
   M /trunk/Backup/AuldLangSyne_Backup.toc
   M /trunk/FriendList/AuldLangSyne_FriendList.toc
   M /trunk/Fu/AuldLangSyne_Fu.toc
   M /trunk/GuildList/AuldLangSyne_GuildList.toc
   M /trunk/Info/AuldLangSyne_Info.toc
   M /trunk/Note/AuldLangSyne_Note.toc
   M /trunk/Panel/AuldLangSyne_Panel.toc
   M /trunk/Sync/AuldLangSyne_Sync.toc

TOC bumpage.

------------------------------------------------------------------------
r252 | shefki | 2010-06-28 02:35:54 +0000 (Mon, 28 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua
   M /trunk/Fu/Fu.lua

Fix errors for players with unknown classes, forgot to put an else fall through in.

------------------------------------------------------------------------
r251 | shefki | 2010-06-27 22:27:20 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Fu/Fu.lua

Fix a bunch of variables leaking to global namespace.

------------------------------------------------------------------------
r250 | shefki | 2010-06-27 22:15:08 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Note/Note.lua

Avoid an error with the Note module.

------------------------------------------------------------------------
r249 | shefki | 2010-06-27 22:11:28 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Info/Info.lua

Remove the cached guild properly from /who info if the player has no guild.

------------------------------------------------------------------------
r248 | shefki | 2010-06-27 22:03:24 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Ticket #3: Add [note] support to FriendList module.

------------------------------------------------------------------------
r247 | shefki | 2010-06-27 21:47:29 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Fu/Fu.lua

Fix Fu module to use cached data from Info module now that GetFriendInfo() is not hooked.

------------------------------------------------------------------------
r246 | shefki | 2010-06-27 21:43:35 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Fix a leaked variable.

------------------------------------------------------------------------
r245 | shefki | 2010-06-27 21:34:31 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Fix FriendList module to show cached info from Info module now that GetFriendInfo is no longer hooked.

------------------------------------------------------------------------
r244 | shefki | 2010-06-27 20:44:42 +0000 (Sun, 27 Jun 2010) | 5 lines
Changed paths:
   M /trunk/Info/Info.lua

Fix taint caused by Info module.  No longer hook GetWhoInfo and GetFriendInfo.

This fixes the Info module causing problems with the Raid UI and the Blizzard
Note Interface.

------------------------------------------------------------------------
r243 | shefki | 2010-06-27 08:03:56 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Panel/Panel.lua

Fix Panel module to properly update notes after editing them.

------------------------------------------------------------------------
r242 | shefki | 2010-06-27 07:42:45 +0000 (Sun, 27 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Panel/Panel.lua

Fix the Panel module for 3.3.5.

------------------------------------------------------------------------
r241 | shefki | 2010-06-26 21:50:18 +0000 (Sat, 26 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Note/Note.lua

Fix the Note module to work properly with the 3.3.5 ignore list.

------------------------------------------------------------------------
r240 | shefki | 2010-06-26 07:23:46 +0000 (Sat, 26 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Note/Note.lua

Adjust note button for Summon Friend button.

------------------------------------------------------------------------
r239 | shefki | 2010-06-25 22:56:02 +0000 (Fri, 25 Jun 2010) | 2 lines
Changed paths:
   M /trunk/Note/Note.lua

Fix Note module for 3.3.5.

------------------------------------------------------------------------
r238 | shefki | 2010-06-24 23:31:57 +0000 (Thu, 24 Jun 2010) | 4 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Really fix FriendList module.

Previous attempts didn't account for the change from FauxScroll to DynamicScroll.

------------------------------------------------------------------------
r237 | shefki | 2010-06-23 23:23:14 +0000 (Wed, 23 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Fix FriendList to know to skip offline RealID and properly deal with the online/offline separator.

------------------------------------------------------------------------
r236 | shefki | 2010-06-23 22:12:39 +0000 (Wed, 23 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Have to account for the offline bar in FriendList module.

------------------------------------------------------------------------
r235 | shefki | 2010-06-23 21:30:41 +0000 (Wed, 23 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Make the FriendList module skip over the RealID fields.

------------------------------------------------------------------------
r234 | shefki | 2010-06-23 21:11:31 +0000 (Wed, 23 Jun 2010) | 2 lines
Changed paths:
   M /trunk/FriendList/FriendList.lua

Fix FriendList module for 3.3.5

------------------------------------------------------------------------
r233 | zuz666 | 2010-01-17 11:11:12 +0000 (Sun, 17 Jan 2010) | 1 line
Changed paths:
   M /trunk/FriendList/ruRU.lua
   M /trunk/Fu/ruRU.lua
   M /trunk/GuildList/ruRU.lua
   M /trunk/Info/ruRU.lua
   M /trunk/Note/ruRU.lua
   M /trunk/Sync/ruRU.lua

Fix ruRU locale
------------------------------------------------------------------------
