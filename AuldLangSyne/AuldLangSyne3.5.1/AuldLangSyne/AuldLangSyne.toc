## Interface: 30300
## Title: AuldLangSyne
## Title-zhTW: |cFF00FF00[介面]|r AuldLangSyne 公會/好友名單 
## Notes: Friends list improvements: notes, remembering the information of offline friends, friend list syncing and backups, and a fubar plugin for it all
## Notes-esES: Te permite anyadir notas personalizadas en tu lista de amigos.
## Notes-zhTW: 幫助你記住好友的詳細資料
## Notes-koKR: 친구 목록을 저장하는 애드온입니다.
## Version: 2.0
## Author: Kemayo
## X-Website: http://kemayo.wowinterface.com
## X-eMail: kemayo@gmail.com
## X-RelSite-WoWI: 5320
## X-WoWIPortal: kemayo
## X-Embeds: Ace2, LibStub, CallbackHandler-1.0, LibBabble-Class-3.0, Deformat, FuBarPlugin-2.0, TabletLib, DewdropLib, LibFriends-1.0, LibGuild-1.0, RosterLib
## X-Category: Interface Enhancements
## X-Credits: Addon idea derived from FriendsFacts and CT_PlayerNotes.  Module system is pretty much what ckknight did in PitBull.
## X-Donate: PayPal:kemayo AT gmail DOT com
## OptionalDeps: Ace2, Ace3, LibBabble-Class-3.0, Deformat, FuBar, TabletLib, DewdropLib, LibFriends-1.0, LibGuild-1.0, RosterLib
## SavedVariables: AuldLangSyneDB
## X-Curse-Packaged-Version: v3.3.5.1
## X-Curse-Project-Name: AuldLangSyne
## X-Curse-Project-ID: auldlangsyne
## X-Curse-Repository-ID: wow/auldlangsyne/mainline

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

AuldLangSyne.lua


