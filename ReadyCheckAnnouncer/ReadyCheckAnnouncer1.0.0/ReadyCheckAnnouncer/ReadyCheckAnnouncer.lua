--
-- ReadyCheckAnnoucer by Mikk of Bloodhoof-EU
--
-- 1. Announces "Not Ready" and "AFK" people to the raid/party
-- 2. Displays a small timer bar at the bottom of the chat frame so you know how long is left of the check
-- 3. Installs an "/rc" alias for "/readycheck"
--
-- This work is placed in the public domain by the author.
--


local patAFK = "^" .. RAID_MEMBERS_AFK:gsub("%%s", "(.*)") .. "$";

local patNotReady = "^" .. RAID_MEMBER_NOT_READY:gsub("%%s", "(.*)") .. "$";

local patStart = "^" .. READY_CHECK_START .. "$";

local patReady = "^" .. READY_CHECK_ALL_READY .. "$";

local patOtherStart = "^" .. ERR_RAID_LEADER_READY_CHECK_START_S:gsub("%%s", "(.*)") .. "$";


if(not RCAnn) then
  RCAnn = {}
  RCAnn.frame = CreateFrame("Frame");
  RCAnn.timer = CreateFrame("StatusBar", "RCAnnTimer", UIParent);
  RCAnn.timer.txt = RCAnn.timer:CreateTexture();
end


function RCAnn:Say(str)
  if(str:len()>255) then
    str = str:sub(1,255);  -- to avoid raid of 39 saying "no" and kicking off the RL :>  (though conceivably he had it coming? =))
  end
  SendChatMessage(str, (GetNumRaidMembers() > 0) and "RAID" or "PARTY");
end


function RCAnn:StartTimer()
	self.notready = nil;
  self.timer:ClearAllPoints();
  self.timer:SetPoint("TOPLEFT", CURRENT_CHAT_FRAME or DEFAULT_CHAT_FRAME, "BOTTOMLEFT", 3, -3);
  self.timer:SetPoint("BOTTOMRIGHT", CURRENT_CHAT_FRAME or DEFAULT_CHAT_FRAME, "BOTTOMLEFT", 100, -6);
  
  self.timer.txt:SetAllPoints(self.timer);
  self.timer.txt:SetTexture(0.7, 0.7, 0, 0.8);
  self.timer:SetStatusBarTexture(self.timer.txt);
  
  self.timer:SetMinMaxValues(0,30);
  self.timer:Show();
  self.timer:SetValue(30);
  self.timer:Show();
  
  self.timer:SetScript("OnUpdate", function()
      local n=RCAnn.timer:GetValue() - arg1;
      RCAnn.timer:SetValue(n);
      if(n<=0) then
        RCAnn:StopTimer();
      end
    end
  );
end

function RCAnn:StopTimer()
  self.timer:Hide();
  if(self.notready) then
    self:Say("Ready check: " .. string.format(RAID_MEMBER_NOT_READY, RCAnn.notready));
    self.notready = nil;
  end
end


function RCAnn.OnEvent()
  local self=RCAnn;
  if(event=="CHAT_MSG_SYSTEM") then
    if(arg1:find(patAFK)) then
      self:Say("Ready check: " .. arg1);
      self:StopTimer();
    elseif(arg1:find(patNotReady)) then
      local _,_,person = arg1:find(patNotReady);
      RCAnn.notready = ((RCAnn.notready and RCAnn.notready .. ", ") or "") .. person;
      -- RCAnn:Say("Ready check: " .. arg1);
    elseif(arg1:find(patStart)) then
      RCAnn:StartTimer();
    elseif(arg1:find(patOtherStart)) then	-- if someone else starts a readycheck
      RCAnn:StartTimer();	-- this just displays the timer so we know how long we have until we have to click
    elseif(arg1:find(patReady)) then
    	RCAnn:Say("Ready check: " .. arg1);
      RCAnn:StopTimer();
    end
  end
end




RCAnn.frame:RegisterEvent("CHAT_MSG_SYSTEM");

RCAnn.frame:SetScript("OnEvent", RCAnn.OnEvent);


SLASH_RCANN1 = "/rc";
-- You'd think you could just alias it straight off but that causes weird tainting errors in 3.0. See http://forums.worldofwarcraft.com/thread.html?topicId=11676470259&postId=118283238776&sid=1#6
SlashCmdList["RCANN"] = function(...) SlashCmdList["READYCHECK"](...) end

