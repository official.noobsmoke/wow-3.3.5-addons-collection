## Interface: 30100
## Title: ReadyCheckAnnouncer
## Notes: Announces results of readychecks to the raid/party, installs "/rc" alias, shows rc timer bar
## Author: Mikk
## Version: 1.00-30100
## eMail: dpsgnome@mail.com
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: 
## SavedVariablesPerCharacter: 
## X-Curse-Packaged-Version: 1.00-30100
## X-Curse-Project-Name: ReadyCheckAnnouncer
## X-Curse-Project-ID: ready-check-announcer
## X-Curse-Repository-ID: wow/ready-check-announcer/mainline

## LoadManagers: AddonLoader
## X-LoadOn-Always: delayed

ReadyCheckAnnouncer.lua

