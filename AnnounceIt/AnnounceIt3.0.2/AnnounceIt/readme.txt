//////////////////////////////////////////////
//            * AnnounceIt *                //
//     - A World of Warcrft Addon -         //
//                giant                     //
//////////////////////////////////////////////

AnnounceIt is a simple addon that lets you create messages to publish on desired channels, such as loot rules, VoIP connection info, guild messages, and the many other possibilities. You can set up up to 5 lines to be messaged at once and select the channel it will be published to.

As of 3.3.5:

    * Fixed whispers for 3.3.5 breakage


As of 3.0:

    * This addon is now an LDB addon and should work with any LDB display. If you do not have an LDB display you will be able to use the addon through the Minimap button. If you wish to use this addon with fubar you will need an addon such as Broker2FuBar. 

    * Now supports links! For items and spells, drag the item/spell icon to the text box and click. For profession links, simply open the profession window and click in the text box 

    * /announceit will now open the options menu. Not much there now though... 

    * Custom channels are now supported 

    * Bug with messages being limited fixed 

    * A left click on the control will attempt to send last message/channel 

    * New version control so messages will never be reset again during an update 

Please note, upgrading from a version pre 3.0 will wipe all messages current messages. You will need to add them again. This should not happen in future releases.
