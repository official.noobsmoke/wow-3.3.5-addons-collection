ai = LibStub("AceAddon-3.0"):NewAddon("AnnounceIt", "AceConsole-3.0")
ai:RegisterChatCommand("announceit", "OpenOptions")
local aiLDB = LibStub("LibDataBroker-1.1"):NewDataObject("AnnounceIt", {
	type = "launcher",
	text = "AnnounceIt",
	icon = "Interface\\Addons\\AnnounceIt\\icon",
	OnClick = function(_, msg)
		if msg == "LeftButton" then
			ai:LastMessage()
		end
		if msg == "RightButton" then
			ai:InitMenu()
		end
	end,
	OnTooltipShow = function(tooltip)
		if not tooltip or not tooltip.AddLine then return end
		tooltip:AddLine("AnnounceIt |cffffff003.0|r")
		tooltip:AddLine("|cffffff00" .. "Right-click|r for menu")
	end,
})
local icon = LibStub("LibDBIcon-1.0")
ai.version = GetAddOnMetadata("AnnounceIt", "Version")
ai.versionstring = "AnnounceIt "..GetAddOnMetadata("AnnounceIt", "Version")

-- declare variables and constants
local channelid
local aimenuitems = { }
local info = { }
local aiconstants = {
	["messagelimit"] = 20,
	["fieldlimit"] = 5,
	["chattypes"] = {
		["TEST"] = true,
		["GROUP"] = false,
		["GUILD"] = false,
		["WHISPER"] = false,
		["RECRUIT"] = false,
		["SAY"] = false,
		["YELL"] = false,
		["BATTLEGROUND"] = false,
		["TRADE"] = false,
		["LFG"] = false,
	},
}

-- defaults
local defaults = {
	profile = {
		messages = { },
		last = {
			enable = false,
		},
		minimap = {
			hide = false,
			minimapPos = 230,
			radius = 80,
		},
	},
}

function ai:LastMessage()
	if (self.db.profile.lastmessage) then
		messageid = self.db.profile.lastmessage["message"]
		channelid = self.db.profile.lastmessage["chan"]
		channeltype = self.db.profile.lastmessage["type"]
		self:SendMessage(messageid, channelid, channeltype)
	else
		DEFAULT_CHAT_FRAME:AddMessage("You have not previously sent any messages!", 1.0, 0.0, 0.0)
	end
end

function ai:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New("aiDB", defaults)
	icon:Register("AnnounceItButton", aiLDB, self.db.profile.minimap)
	
	local aioptions = {
		name = "AnnounceIt",
		type = "group",
		args = {
			enable = {
				name = "Hide Minimap Button",
				desc = "Toggles the AnnounceIt minimap button",
				type = "toggle",
				set = function(info,val) ai:togglemmb() end,
				get = function(info) return self.db.profile.minimap.hide end,
			},
		},
	}
	LibStub("AceConfig-3.0"):RegisterOptionsTable("AnnounceIt", aioptions)
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("AnnounceIt")
	
	if (self.db.profile.minimap.hide) then
		icon:Hide("AnnounceItButton")
	else
		icon:Show("AnnounceItButton")
	end
	
	--	version check
	if (self.db.profile.version == ai.version) then
		return
	else
		self.db.profile.version = ai.version
	end
	
end

StaticPopupDialogs["AISaveError"] = {
	text = "Your message title was not unique, please enter a unique title.",
	button1 = "OK",
	OnAccept = function()
		return
	end,
	timeout = 0,
	whileDead = true,
	hideOnEscape = true,
}

function ai:InitMenu()
	for z,value in pairs(self.db.profile.messages) do
		aimenuitems[z] = { }
		aimenuitems[z].channel = { }
		for sendchannel,senditemvalue in pairs(self.db.profile.messages[z].chan) do
			aimenuitems[z].channel[sendchannel] = "regular"
		end
		for sendcchannel,sendcitemvalue in pairs(self.db.profile.messages[z].custom) do
			if (self.db.profile.messages[z].custom[sendcchannel] ~= "") then
				aimenuitems[z].channel[sendcitemvalue] = "custom"
			end
		end
	end
	if not self.aimenu then
		self.aimenu = CreateFrame("Frame", "AnnounceItMenu")
	end
	local aimenu = self.aimenu
	aimenu.displayMode = "MENU"
	aimenu.initialize = function(self, level)

	if not (level) then
		return
	end

	wipe(info)

	level = level or 1
	if (level == 1) then
		info.isTitle = true
		info.text = ai.versionstring
		info.notCheckable = true
		UIDropDownMenu_AddButton(info, level)

		wipe(info)

		for message, value in pairs(aimenuitems) do
			info.hasArrow = true
			info.notCheckable = true
			info.text = message
			info.value = {
				["messagekey"] = message
			}
			UIDropDownMenu_AddButton(info, level)
		end

		wipe(info)
		info.disabled = 1
		UIDropDownMenu_AddButton(info, level)
		info.disabled = nil

		info.value = nil
		info.hasArrow = false
		info.notCheckable = true
		info.text = "New Message"
		info.func = function() ai:NewMessage() end
		UIDropDownMenu_AddButton(info, level)

		info.notCheckable = true
		info.value = nil
		info.hasArrow = false
		info.text = "Options"
		info.func = function() InterfaceOptionsFrame_OpenToCategory("AnnounceIt") end
		UIDropDownMenu_AddButton(info, level)


	end
	if (level == 2) then
		-- getting values of first menu
		local messageid = UIDROPDOWNMENU_MENU_VALUE["messagekey"]
		channellist = aimenuitems[messageid].channel

		info.isTitle = true
		info.text = messageid
		info.notCheckable = true
		UIDropDownMenu_AddButton(info, level)

		info.disabled = nil
		info.isTitle = nil

		for channelid, channeltype in pairs(channellist) do
			info.hasArrow = false
			info.notCheckable = true
			info.text = channelid
			info.func = function() ai:SendMessage(messageid,channelid,channeltype) end
			info.value = {
				["messagekey"] = messageid,
				["channelkey"] = channelid,
			}
			UIDropDownMenu_AddButton(info, level)
		end

		wipe(info)
		info.disabled = 1
		UIDropDownMenu_AddButton(info, level)
		info.disabled = nil

		info.hasArrow = nil
		info.notCheckable = true
		info.text = "Edit"
		info.func = function() ai:EditMessage(messageid) end
		UIDropDownMenu_AddButton(info, level)

		info.hasArrow = false
		info.notCheckable = true
		info.text = "Delete"
		info.func = function() ai:DeleteMessage(messageid) end
		UIDropDownMenu_AddButton(info, level)
	end
	end
	local x,y = GetCursorPosition(UIParent);
	ToggleDropDownMenu(1, nil, aimenu, "UIParent", x / UIParent:GetEffectiveScale() , y / UIParent:GetEffectiveScale())
end

-- create a new message and edit it
function ai:NewMessage()
	for index,value in pairs(aiconstants.chattypes) do
		getglobal("AnnounceItSetMessage_Chan"..index):SetChecked(false)
	end
	for i=1,aiconstants.fieldlimit,1 do
		getglobal("AnnounceItSetMessage_Text"..i):SetText("")
	end
	for i=1,4,1 do
		getglobal("AnnounceItSetMessage_CustomChan"..i):SetText("")
	end
	AnnounceItSetMessage_Label:SetText("New Message")
	active_message_id = "New"
	AnnounceItSetMessage_Header4:SetText(ai.versionstring)
	AnnounceItSetMessage:Show()
end

-- populate and open the edit box with the selected message
function ai:EditMessage(messageID)
	for index,value in pairs(aiconstants.chattypes) do
		getglobal("AnnounceItSetMessage_Chan"..index):SetChecked(false)
	end
	for index,value in pairs(self.db.profile.messages[messageID].chan) do
		getglobal("AnnounceItSetMessage_Chan"..index):SetChecked(true)
	end
	
	for i=1,aiconstants.fieldlimit,1 do
		getglobal("AnnounceItSetMessage_Text"..i):SetText(self.db.profile.messages[messageID].text[i])
	end
	for i,v in pairs(self.db.profile.messages[messageID].custom) do
		getglobal("AnnounceItSetMessage_CustomChan"..i):SetText(v)
	end
	AnnounceItSetMessage_Label:SetText(messageID)
	active_message_id = messageID
	AnnounceItSetMessage_Header4:SetText(ai.versionstring)
	AnnounceItSetMessage:Show()
end

-- Take the message and send it
function ai:SendMessage(selectedmessage, selectedchannel, customchannel)	
	if (selectedchannel == "TEST") then
		DEFAULT_CHAT_FRAME:AddMessage("Testing: "..selectedmessage, 1.0, 0.0, 0.0)
		for index,value in pairs(self.db.profile.messages[selectedmessage].text) do
			if (self.db.profile.messages[selectedmessage].text[index] ~= "") then
				DEFAULT_CHAT_FRAME:AddMessage(self.db.profile.messages[selectedmessage].text[index], 1.0, 0.0, 0.0)
			end
			self.db.profile.lastmessage = { }
			self.db.profile.lastmessage["message"] = selectedmessage
			self.db.profile.lastmessage["chan"] = selectedchannel
			self.db.profile.lastmessage["type"] = customchannel
		end
	else
		channelid = nil
		local errormessage = nil
		local messagechannel = selectedchannel
		if (selectedchannel == "RECRUIT") then
			channelid = GetChannelName("GuildRecruitment - City")
			messagechannel = "CHANNEL"
		end
               if (selectedchannel == "WHISPER") then
			local chatwindow = ChatEdit_GetActiveWindow();
			if (chatwindow) then
				channelType = chatwindow:GetAttribute("chatType")
				if (channelType == "REPLY") then
					channelid = ChatEdit_GetLastTellTarget(activeWindow)
				elseif (channelType == "WHISPER") then
					channelid = chatwindow:GetAttribute("tellTarget")
				else
					errormessage = "You are not in whisper or reply mode."
				end
			else
				errormessage = "You are not in whisper or reply mode."
			end
		end
		if (selectedchannel == "GROUP") then
			if (GetNumRaidMembers() > 0) then
				messagechannel = "RAID"
			elseif (GetNumPartyMembers() > 0) then
				messagechannel = "PARTY"
			else
				errormessage = "You are not in a party or raid."
			end
		end
		if (selectedchannel == "TRADE") then
			-- ToDo: Really need to add check here for availability of channel
			channelid = GetChannelName("Trade - City")
			if not (channelid) then
				errormessage = "You are not in a party or raid."
			else
				messagechannel = "CHANNEL"
			end
		end
		if (selectedchannel == "LFG") then
			channelid = GetChannelName("LookingForGroup")
			if not (channelid) then
				errormessage = "You do not belong to the LFG channel."
			else
				messagechannel = "CHANNEL"
			end
		end
		if (customchannel == "custom") then
			channelid = GetChannelName(selectedchannel)
			if (channelid == 0) then
				errormessage = "You do not seem to belong the "..selectedchannel.." channel right now."
			else
				messagechannel = "CHANNEL"
			end
		end
		if (errormessage) then
			DEFAULT_CHAT_FRAME:AddMessage("AnnounceIt Error: "..errormessage, 1.0, 0.0, 0.0)
			errormessage = nil
		else
			for index,value in pairs(self.db.profile.messages[selectedmessage].text) do
				SendChatMessage(self.db.profile.messages[selectedmessage].text[index], messagechannel, nil, channelid)
			end
			self.db.profile.lastmessage = { }
			self.db.profile.lastmessage["message"] = selectedmessage
			self.db.profile.lastmessage["chan"] = selectedchannel
			self.db.profile.lastmessage["type"] = customchannel
		end
	end
end

-- save the edit box values to the addon variables
function ai:Save()
	if (active_message_id == "New") then
		if (AnnounceItSetMessage_Label:GetText() == "New Message") then
			StaticPopup_Show ("AISaveError")
			return
		end
		for index,value in pairs (self.db.profile.messages) do
			if (AnnounceItSetMessage_Label:GetText() == index) then
				StaticPopup_Show ("AISaveError")
				return
			end
		end
		active_message_id = AnnounceItSetMessage_Label:GetText()
		self.db.profile.messages[active_message_id] = { }
	end
	self.db.profile.messages[active_message_id].chan = {}
	self.db.profile.messages[active_message_id].custom = {}
	for index,value in pairs(aiconstants.chattypes) do
		if (getglobal("AnnounceItSetMessage_Chan"..index):GetChecked()) then	
			self.db.profile.messages[active_message_id].chan[index] = true
		end
	end
	for cust=1, 4, 1 do
		customname = getglobal("AnnounceItSetMessage_CustomChan"..cust):GetText()
		self.db.profile.messages[active_message_id].custom[cust] = customname
	end
	self.db.profile.messages[active_message_id].text = {}
	for i=1,aiconstants.fieldlimit,1 do
		self.db.profile.messages[active_message_id].text[i] = getglobal("AnnounceItSetMessage_Text"..i):GetText()
	end 
	active_message_id = nil
	AnnounceItSetMessage:Hide();
end

-- simple deletion of a message
function ai:DeleteMessage(deleteid)
	self.db.profile.messages[deleteid] = nil
	aimenuitems[deleteid] = nil
	ai:InitMenu()
end

-- simply open the Blizzard options frame to this addons options
function ai:OpenOptions()
	InterfaceOptionsFrame_OpenToCategory("AnnounceIt")
end

-- toggle minimap button
function ai:togglemmb()
	if (self.db.profile.minimap.hide) then
		self.db.profile.minimap.hide = false
		icon:Show("AnnounceItButton")
	else
		self.db.profile.minimap.hide = true
		icon:Hide("AnnounceItButton")
	end
end

-- thanks to oming101 for this code for links
function ai:AddLink(index)
	local itemType, itemID, itemLink = GetCursorInfo()
	if itemType == "item" then
		getglobal("AnnounceItSetMessage_Text"..index):Insert(itemLink)
	elseif itemType == "spell" then
		local spellLink = GetSpellLink(itemID,"")
		getglobal("AnnounceItSetMessage_Text"..index):Insert(spellLink)
	end
	ClearCursor()
	tradeLink = GetTradeSkillListLink()
	getglobal("AnnounceItSetMessage_Text"..index):Insert(tradeLink)
end
