## Interface: 30300
## Title: AnnounceIt |cFF0000FF3.0
## Notes: Simple storage and retrieval access to broadcast your stored mesages to Party, Raid and other chat channels.
## Version: 3.0.2
## Author: giant
## SavedVariables: aiDB AnnounceItDB
## X-Category: Chat/Communication

libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\AceAddon-3.0\AceAddon-3.0.xml
libs\AceConsole-3.0\AceConsole-3.0.xml
libs\AceGUI-3.0\AceGUI-3.0.xml
libs\AceConfig-3.0\AceConfig-3.0.xml
libs\AceDB-3.0\AceDB-3.0.xml
libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
libs\LibDBIcon-1.0\LibDBIcon-1.0.lua

AnnounceIt.xml