## Interface: 30300
## Title: Chaos Bane Buddy
## Notes: Removal of the Chaos Bane buff without macros.
## Author: Lemon King
## Version: 1000
## DefaultState: enabled
## X-Category: Combat Assit
## X-Date: 6/29/2010

#Core
CBB_XML.xml
CBB_Core.lua