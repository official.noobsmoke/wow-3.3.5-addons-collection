

function CBB_Onload(self)
	self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
end


function CBB_OnEvent(self, event, ...)
   if event == "COMBAT_LOG_EVENT_UNFILTERED" then
      local timestamp, type, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags = select(1, ...)
      if type == "SPELL_AURA_APPLIED" then
         local spellId, spellName, spellSchool = select(9, ...)
         if spellId == 73422 then
            CancelUnitBuff("player", "Chaos Bane");           
         end
      end
   end
end