--[[
	Copyright (c) 2009, dr_AllCOM3
    All rights reserved.

    You're allowed to use this addon, free of monetary charge,
    but you are not allowed to modify, alter, or redistribute
    this addon without express, written permission of the author.
]]

local L = LibStub( "AceLocale-3.0" ):GetLocale( "DocsUI_Nameplates" )
local LSM = LibStub( "LibSharedMedia-3.0" )
local temp,temp2
local addon = DocsUI_Nameplates

local function p( text ) -- Print
	ChatFrame3:AddMessage( tostring( text ) )
end

local UnitGUID = UnitGUID
local UnitAura = UnitAura
local UnitBuff = UnitBuff
local UnitDebuff = UnitDebuff
local UnitExists = UnitExists
local UnitIsPlayer = UnitIsPlayer
local UnitClassification = UnitClassification
local unpack = unpack
local tonumber = tonumber
local tostring = tostring
local ipairs = ipairs
local next = next
local pairs	= pairs
local sort = sort
local strfind = strfind
local strlower = strlower
local tremove = tremove
local tinsert = tinsert
local format = format
local floor = floor
local wipe = wipe

local db, frames, realm, global, playerList, guidBuffs, guidCasts, scanUnitAuras, bossplateUpdate, party, raid, guild, bnet, friend, arena, battleground, isTank
local isInInstance, isInArena, isInBattleground, isInSanctuary, hasGroup, playerTargetGUID, unitIsDifferent, tankMode, configMode

local update = {}
local resources = DocsUI_Nameplates.resources
local round = resources.round
local playerName = UnitName( "player" )
local _, playerClass = UnitClass( "player" )
local playerGuild = GetGuildInfo( "player" )
local playerLevel = UnitLevel( "player" )
local playerGUID = UnitGUID( "player" )
local playerTarget = false
local realmName = GetRealmName()
DocsUI_Nameplates.inCombat = false
local inCombat = DocsUI_Nameplates.inCombat
DocsUI_Nameplates.isInPve = true
local isInPve = DocsUI_Nameplates.isInPve
DocsUI_Nameplates.isInPvp = false
local isInPvp = DocsUI_Nameplates.isInPvp
local tankMode = false
local tankModeCheckFrequency = 10
local temporaryList = {}
local framesShown = {}
DocsUI_Nameplates.framesShown = framesShown
local lfgList = {}
local whisperList = {}

local doUpdate = 0
local function setDoUpdate( number ) doUpdate = max( doUpdate, number ) end
local doUpdateThreat = 0
local function setDoUpdateThreat( number ) doUpdateThreat = max( doUpdateThreat, number ) end
local doUpdateNext = { frames = {}, name = {}, GUID = {} }
local function setDoUpdateMe( object, type, option )
    if type=="frame" then
        doUpdateNext.frames[object] = option or true
    elseif type=="name" then
        doUpdateNext.name[object] = option or true
    elseif type=="GUID" then
        doUpdateNext.GUID[object] = option or true
    end
end
DocsUI_Nameplates.setDoUpdateMe = setDoUpdateMe

local tooltipScan = CreateFrame( "GameTooltip", "DocsScanningTooltip", UIParent, "GameTooltipTemplate" )
DocsScanningTooltip:SetOwner( WorldFrame, "ANCHOR_NONE" )

local function setVisibility() -- Set nameplate visibility
    -- isInPve, isInPvp, inCombat
    
    if db.threatEnabled then
        SetCVar( "threatWarning", 3)
    else
        SetCVar( "threatWarning", 0)
    end
    
    if db.allowOverlap then
        SetCVar( "nameplateAllowOverlap", 1)
    else
        SetCVar( "nameplateAllowOverlap", 0)
    end
    
    if isInPve and not inCombat then
        -- Friend
        if db.visibilityPveFriendUnit then
            SetCVar( "nameplateShowFriends", 1)
        else
            SetCVar( "nameplateShowFriends", 0)
        end
        if db.visibilityPveFriendPet then
            SetCVar( "nameplateShowFriendlyPets", 1)
        else
            SetCVar( "nameplateShowFriendlyPets", 0)
        end
        if db.visibilityPveFriendGuardian then
            SetCVar( "nameplateShowFriendlyGuardians", 1)
        else
            SetCVar( "nameplateShowFriendlyGuardians", 0)
        end
        if db.visibilityPveFriendTotem then
            SetCVar( "nameplateShowFriendlyTotems", 1)
        else
            SetCVar( "nameplateShowFriendlyTotems", 0)
        end
        
        -- Enemy
        if db.visibilityPveEnemyUnit then
            SetCVar( "nameplateShowEnemies", 1)
        else
            SetCVar( "nameplateShowEnemies", 0)
        end
        if db.visibilityPveEnemyPet then
            SetCVar( "nameplateShowEnemyPets", 1)
        else
            SetCVar( "nameplateShowEnemyPets", 0)
        end
        if db.visibilityPveEnemyGuardian then
            SetCVar( "nameplateShowEnemyGuardians", 1)
        else
            SetCVar( "nameplateShowEnemyGuardians", 0)
        end
        if db.visibilityPveEnemyTotem then
            SetCVar( "nameplateShowEnemyTotems", 1)
        else
            SetCVar( "nameplateShowEnemyTotems", 0)
        end
    elseif isInPve then
        -- Friend
        if db.visibilityPvecombatFriendUnit then
            SetCVar( "nameplateShowFriends", 1)
        else
            SetCVar( "nameplateShowFriends", 0)
        end
        if db.visibilityPvecombatFriendPet then
            SetCVar( "nameplateShowFriendlyPets", 1)
        else
            SetCVar( "nameplateShowFriendlyPets", 0)
        end
        if db.visibilityPvecombatFriendGuardian then
            SetCVar( "nameplateShowFriendlyGuardians", 1)
        else
            SetCVar( "nameplateShowFriendlyGuardians", 0)
        end
        if db.visibilityPvecombatFriendTotem then
            SetCVar( "nameplateShowFriendlyTotems", 1)
        else
            SetCVar( "nameplateShowFriendlyTotems", 0)
        end
        
        -- Enemy
        if db.visibilityPvecombatEnemyUnit then
            SetCVar( "nameplateShowEnemies", 1)
        else
            SetCVar( "nameplateShowEnemies", 0)
        end
        if db.visibilityPvecombatEnemyPet then
            SetCVar( "nameplateShowEnemyPets", 1)
        else
            SetCVar( "nameplateShowEnemyPets", 0)
        end
        if db.visibilityPvecombatEnemyGuardian then
            SetCVar( "nameplateShowEnemyGuardians", 1)
        else
            SetCVar( "nameplateShowEnemyGuardians", 0)
        end
        if db.visibilityPvecombatEnemyTotem then
            SetCVar( "nameplateShowEnemyTotems", 1)
        else
            SetCVar( "nameplateShowEnemyTotems", 0)
        end
    elseif isInPvp then
        -- Friend
        if db.visibilityPvpFriendUnit then
            SetCVar( "nameplateShowFriends", 1)
        else
            SetCVar( "nameplateShowFriends", 0)
        end
        if db.visibilityPvpFriendPet then
            SetCVar( "nameplateShowFriendlyPets", 1)
        else
            SetCVar( "nameplateShowFriendlyPets", 0)
        end
        if db.visibilityPvpFriendGuardian then
            SetCVar( "nameplateShowFriendlyGuardians", 1)
        else
            SetCVar( "nameplateShowFriendlyGuardians", 0)
        end
        if db.visibilityPvpFriendTotem then
            SetCVar( "nameplateShowFriendlyTotems", 1)
        else
            SetCVar( "nameplateShowFriendlyTotems", 0)
        end
        
        -- Enemy
        if db.visibilityPvpEnemyUnit then
            SetCVar( "nameplateShowEnemies", 1)
        else
            SetCVar( "nameplateShowEnemies", 0)
        end
        if db.visibilityPvpEnemyPet then
            SetCVar( "nameplateShowEnemyPets", 1)
        else
            SetCVar( "nameplateShowEnemyPets", 0)
        end
        if db.visibilityPvpEnemyGuardian then
            SetCVar( "nameplateShowEnemyGuardians", 1)
        else
            SetCVar( "nameplateShowEnemyGuardians", 0)
        end
        if db.visibilityPvpEnemyTotem then
            SetCVar( "nameplateShowEnemyTotems", 1)
        else
            SetCVar( "nameplateShowEnemyTotems", 0)
        end
    end
end
DocsUI_Nameplates.setVisibility = setVisibility

local function softReset() -- Just deletes some savedData values to cause a refresh when setDoUpdate
    for i=1,#frames do
        frames[i].nameplate.savedData.maxHealthIs = nil
        frames[i].nameplate.savedData.levelIs = nil
    end
    
    setDoUpdate( 1 )
end

--[[ Updates ]]
local time
function DocsUI_Nameplates:updateNameplate( option ) -- onUpdate. Options: "onlyAura"
    local nameplate = self.nameplate
    local data = nameplate.data
    local savedData = nameplate.savedData
    time = GetTime()
    
    if option=="onlyHp" then
        update.hp( nameplate, "onlyHp" )
        update.hpNumber( nameplate, "onlyHp" )
        
        if nameplate.updateCounterHp then nameplate.updateCounterHp = nameplate.updateCounterHp+1 end
        update.debug( nameplate )
    elseif option=="onlyThreat" then
        update.threat( nameplate, "onlyThreat" )
        
        if nameplate.updateCounterThreat then nameplate.updateCounterThreat = nameplate.updateCounterThreat+1 end
        update.debug( nameplate )
    elseif option=="onlyCast" then
        update.cast( nameplate, "onlyCast" )
        
        if nameplate.updateCounterCast then nameplate.updateCounterCast = nameplate.updateCounterCast+1 end
        update.debug( nameplate )
    elseif option=="onlyAura" then
        update.auras( nameplate )
        
        if nameplate.updateCounterAura then nameplate.updateCounterAura = nameplate.updateCounterAura+1 end
        update.debug( nameplate )
    else
        update.collectDynamicData( nameplate )
        
        if global.nameplateHider[data.nameIs] then
            nameplate:Hide()
            
            return
        else
            nameplate:Show()
        end
        
        data.alphaIs = nameplate.alpha
        data.isTarget = playerTarget and data.alphaIs==1
        if data.isTarget then data.GUID = playerTargetGUID end
        
        unitIsDifferent = false
        -- Check if the unit has changed
        for index,value in pairs( data ) do
            if index~="healthIs" and savedData[index]~=value then--
                unitIsDifferent = true
                
                break
            end
        end
        
        update.highlight( nameplate )
        update.alpha( nameplate )
        update.size( self )
        update.mouseover( nameplate )
        update.unknownIcon( nameplate )
        update.threat( nameplate )
        update.name( nameplate )
        update.tot( nameplate )
        update.guild( nameplate )
        update.hp( nameplate )
        update.level( nameplate )
        update.hpNumber( nameplate )
        update.cast( nameplate )
        update.socialIcon( nameplate )
        update.raidIcon( nameplate )
        update.classIcon( nameplate )
        update.roleIcon( nameplate )
        update.totemIcon( nameplate )
        update.lfgIcon( nameplate )
        update.chatIcon( nameplate )
        update.threat( nameplate )
        update.auras( nameplate )
        update.bossplates( nameplate )
        
        -- Save data
        for index,value in pairs( data ) do
            savedData[index] = value
        end
        
        if nameplate.updateCounter then nameplate.updateCounter = nameplate.updateCounter+1 end
        update.debug( nameplate )
    end
end
local updateNameplate = DocsUI_Nameplates.updateNameplate

function DocsUI_Nameplates.updateNameplateByIndex( index )
    updateNameplate( frames[i] )
end
local updateNameplateByIndex = DocsUI_Nameplates.updateNameplateByIndex

function DocsUI_Nameplates.updateNameplateByName( name )
    for index,value in pairs( framesShown ) do
        if name==value.nameplate.data.nameIs then
            updateNameplate( value )
        end
    end
end
local updateNameplateByName = DocsUI_Nameplates.updateNameplateByName
update.auras = function() end

function DocsUI_Nameplates.updateNameplateByRaidIcon( index, GUID )
    if not index or not GUID then return end
    
    for i,v in pairs( framesShown ) do
        if index==v.nameplate.data.hasRaidIcon then
            v.nameplate.data.GUID = GUID
            updateNameplate( v )
            
            return
        end
    end
end
local updateNameplateByRaidIcon = DocsUI_Nameplates.updateNameplateByRaidIcon

function DocsUI_Nameplates.updateNameplateByGUID( GUID, option ) -- Options: "onlyAura"
    for index,value in pairs( framesShown ) do
        if GUID and value.nameplate.data.GUID and GUID==value.nameplate.data.GUID then
            updateNameplate( value, option )
                
            return
        end
    end
end
local updateNameplateByGUID = DocsUI_Nameplates.updateNameplateByGUID

function DocsUI_Nameplates:onShowNameplate() -- onShow
    local nameplate = self.nameplate
    local data = nameplate.data
    local savedData = nameplate.savedData
    time = GetTime()
    
    update.collectFixedData( nameplate )
    update.collectDynamicData( nameplate )
    
    if global.nameplateHider[data.nameIs] then
        nameplate:Hide()
        
        return
    else
        nameplate:Show()
    end
    
    data.isCasting = false
    nameplate.cast:Hide()
    if playerTarget then
        nameplate.alpha = db.alpha -- To prevent flashing from 1 to 0.5.
    else
        nameplate.alpha = self:GetAlpha()
    end
    data.alphaIs = nameplate.alpha
    data.isTarget = false
    
    --self.highlight:SetAllPoints( self.hp )
    nameplate:SetFrameLevel( 11 )
    
    nameplate.updateCounter = 0
    nameplate.updateCounterAura = 0
    nameplate.updateCounterCast = 0
    nameplate.updateCounterHp = 0
    nameplate.updateCounterThreat = 0
    
    framesShown[self] = self
    
    unitIsDifferent = true
    
    update.highlight( nameplate )
    update.alpha( nameplate )
    update.size( self )
    update.mouseover( nameplate )
    update.unknownIcon( nameplate )
    update.threat( nameplate )
    update.name( nameplate )
    update.tot( nameplate )
    update.guild( nameplate )
    update.hp( nameplate )
    update.level( nameplate )
    update.hpNumber( nameplate )
    update.cast( nameplate )
    update.socialIcon( nameplate )
    update.raidIcon( nameplate )
    update.classIcon( nameplate )
    update.roleIcon( nameplate )
    update.totemIcon( nameplate )
    update.threat( nameplate )
    update.auras( nameplate )
    update.bossplates( nameplate )
    update.debug( nameplate )
    
    --updateNameplate( self ) -- Update right now or you can see the frame change.
    setDoUpdateMe( self, "frame" )
end
local onHideNameplate = DocsUI_Nameplates.onHideNameplate

function DocsUI_Nameplates:onHideNameplate() -- onHide
    local nameplate = self.nameplate
    
    wipe( nameplate.data )
    wipe( nameplate.savedData )
    
    framesShown[self] = nil
    
    nameplate.highlight:Hide()
    nameplate.cast:Hide()
end
local onHideNameplate = DocsUI_Nameplates.onHideNameplate
--[[ Helper functions ]]
local function formatTime( time )
    if time>60 then
        time = floor( time/60 ).."m"
    elseif time>=3 then
        time = floor( time )
    else
        time = round( time, 1 )
    end
    
    return time
end

local function siValue( val ) -- Short numbers
    if val >= 10000000 then 
        return format( "%.1fm", val / 1000000 ) 
    elseif val >= 1000000 then
        return format( "%.2fm", val / 1000000 ) 
    elseif val >= 100000 then
        return format( "%.0fk", val / 1000 ) 
    elseif val >= 1000 then
        return format( "%.1fk", val / 1000 ) 
    else
        return val
    end
end

local function lightenColor( r, g, b )
    local modifier = 1.5
    
    return r+( 1-r )/modifier, g+( 1-g )/modifier, b+( 1-b )/modifier
end

local function getLevelColor( level )
    if level>playerLevel+4 then -- Red
        return { 0.75,0,0,1 }
    elseif level>playerLevel+2 then -- Orange
        return { 0.75,0.75/2,0,1 }
    elseif playerLevel+2>=level and level>=playerLevel-2 then -- Yellow
        return { 0.75,0.75,0,1 }
    else -- Green
        if playerLevel<=9 and level>=playerLevel-4 then
            return { 0,0.75,0,1 }
        elseif playerLevel<=19 and level>=playerLevel-5 then
            return { 0,0.75,0,1 }
        elseif playerLevel<=29 and level>=playerLevel-6 then
            return { 0,0.75,0,1 }
        elseif playerLevel<=39 and level>=playerLevel-7 then
            return { 0,0.75,0,1 }
        elseif playerLevel<=70 and level>=playerLevel-8 then
            return { 0,0.75,0,1 }
        elseif playerLevel<=80 and level>=playerLevel-8 then
            return { 0,0.75,0,1 }
        else -- Grey
            return { 0.5,0.5,0.5,1 }
        end
    end
end

local function getRaidIconIndex( raidIcon )
    -- ULx, ULy, LLx, LLy, URx, URy, LRx, LRy
    -- "Interface\\TargetingFrame\\UI-RaidTargetingIcons"
    if not raidIcon:IsShown() then return nil end
    
    local ULx,ULy,LLx,LLy,URx,URy,LRx,LRy = raidIcon:GetTexCoord()
    
    for i=1,8 do
        if resources.raidIconTextureCoords[i][1]==ULx and
        resources.raidIconTextureCoords[i][2]==ULy and
        resources.raidIconTextureCoords[i][3]==LLx and
        resources.raidIconTextureCoords[i][4]==LLy and
        resources.raidIconTextureCoords[i][5]==URx and
        resources.raidIconTextureCoords[i][6]==URy and
        resources.raidIconTextureCoords[i][7]==LRx and
        resources.raidIconTextureCoords[i][8]==LRy then
            return i
        end
    end
    
    return nil
end

local function reactionAndType( red, green, blue, data )
    if red < .01 and blue < .01 and green > .99 then
        return "FRIENDLY", "NPC" 
    elseif red < .01 and blue > .99 and green < .01 then
        return "FRIENDLY", "PLAYER"
    elseif red > .99 and blue < .01 and green > .99 then
        return "NEUTRAL", "NPC"
    elseif red > .99 and blue < .01 and green < .01 then
        return "HOSTILE", "NPC"
    else
        return "HOSTILE", "PLAYER"
    end
end

local function getUnitCombatStatus( r, g, b )
    return r>.5 and g<.5
end

local function threatByColor( threatGlow )
	local redCan, greenCan, blueCan, alphaCan = threatGlow:GetVertexColor()
	
	if not threatGlow:IsShown() then return "LOW" end
	if greenCan > .7 then return "MEDIUM" end
	if redCan > .7 then return "HIGH" end
end

local function getRole( data )
    -- "TANK" "HEALER" "DAMAGER"
    
    if not data.nameIs then return nil end
    
    local name = strlower( data.nameIs )
    
    if data.reaction=="HOSTILE" and data.type~="PLAYER" and data.isCasting and data.castingName and ( not global.casterList[name] or not global.healerList[name] ) then
        local castName = strlower( data.castingName )
        
        for i=1,#db.healingSpells do
            if strfind( castName,db.healingSpells[i] ) then
                global.healerList[name] = true
                break
            end
        end
        
        global.casterList[name] = true
    end
    
    if global.healerList[name] then
        return "HEALER"
    elseif global.casterList[name] then
        return "DAMAGER"
    elseif false then
        return "TANK"
    else
        return nil
    end
end

local function factionChampClassAndRole( name ) -- ToC: Faction Champs
    
    --[[Shaman]]--
		if name==L["Saamul"] or name==L["Thrakgar"] then
            return "SHAMAN", "DAMAGER"
        elseif name==L["Broln Stouthorn"] or name==L["Shaabad"] then
            return "SHAMAN", "HEALER"
    --[[Paladins]]--
        elseif name==L["Baelnor Lightbearer"] or name==L["Malithas Brightblade"] then
            return "PALADIN", "TANK"
        elseif name==L["Velanaa"] or name==L["Liandra Suncaller"] then
            return "PALADIN", "HEALER"
	--[[Druids]]--
        elseif name==L["Kavina Grovesong"] or name==L["Birana Stormhoof"] then
            return "DRUID", "DAMAGER"
        elseif name==L["Melador Valestrider"] or name==L["Erin Misthoof"] then
            return "DRUID", "HEALER"
	--[[Priests]]--
        elseif name==L["Brienna Nightfell"] or name==L["Vivienne Blackwhisper"] then
            return "PRIEST", "DAMAGER"
        elseif name==L["Anthar Forgemender"] or name==L["Caiphus the Stern"] then
            return "PRIEST", "HEALER"
	--[[Warlocks]]--
        elseif name==L["Serissa Grimdabbler"] or name==L["Harkzog"] then
            return "WARLOCK", "DAMAGER"
	--[[Warriors]]--
        elseif name==L["Shocuul"] or name==L["Narrhok Steelbreaker"] then
            return "WARRIOR", "TANK"
	--[[Rogues]]--
        elseif name==L["Irieth Shadowstep"] or name==L["Maz'dinah"] then
            return "ROGUE", "DAMAGER"
	--[[Death Knights]]--
        elseif name==L["Tyrius Duskblade"] or name==L["Gorgrim Shadowcleave"] then
            return "DEATHKNIGHT", "TANK"
	--[[Hunters]]--
        elseif name==L["Alyssia Moonstalker"] or name==L["Ruj'kah"] then
            return "HUNTER", "DAMAGER"
	--[[Mages]]--
        elseif name==L["Noozle Whizzlestick"] or name==L["Ginselle Blightslinger"] then
            return "MAGE", "DAMAGER"
        
        else
            return nil
        end
end

local function colorToString(r,g,b)
    return "C"..math.floor((100*r) + 0.5)..math.floor((100*g) + 0.5)..math.floor((100*b) + 0.5)
end
local classByColor = {}
for classname,color in pairs( RAID_CLASS_COLORS ) do 
    classByColor[colorToString(color.r, color.g, color.b)] = classname
end
local function getClassAndRole( data )
    local c, r
    
    if isInInstance~="none" and data.type~="PLAYER" and data.reaction=="HOSTILE" then c, r = factionChampClassAndRole( data.nameIs ) end
    
    if data.type=="PLAYER" and data.reaction=="HOSTILE" then
        c = classByColor[colorToString( data.red, data.green, data.blue )] or nil
    elseif data.type=="PLAYER" and data.class then
        c = data.class
    end
    
    if not r and data.reaction=="HOSTILE" then
        if data.isBoss then
            r = "BOSS"
        else
            r = getRole( data ) or nil
        end
    end
    
    return c, r
end

local function getTotem( name )
    for i=1,#resources.totems do
        if strfind(name, resources.totems[i].name) then
            return resources.totems[i].texture
        end
    end
    
    return nil
end

local function filterName( name )
    name = strlower( name )
    for i=1,#db.nameplateHider do
        if name==db.nameplateHider[i] then
            return true
        end
    end
    
    return false
end

local function collectUnitInfo( unit )
    if not UnitExists( unit ) then return end
    
    local GUID = UnitGUID( unit )
    local name, rlm = UnitName( unit )
    local _, class = UnitClass( unit )
    local guildName = GetGuildInfo( unit )
    local isPlayer = UnitIsPlayer( unit )
    
    if not name or name==UNKNOWN then return end
    
    if isPlayer and not rlm then
        playerList[name] = GUID
        playerList[GUID] = DocsUI_Nameplates:Serialize( name, GUID, class, guildName or "NONE", rlm or realmName )
    end
    
    if isPlayer then
        temporaryList[name] = GUID
        temporaryList[GUID] = DocsUI_Nameplates:Serialize( name, GUID, class or nil, guildName or "NONE", rlm or realmName or "NONE" )
    end
    
    if UnitClassification( unit )=="worldboss" then
        temporaryList[name] = GUID
        temporaryList[GUID] = DocsUI_Nameplates:Serialize( name, GUID, class or nil, guildName or "NONE", rlm or realmName or "NONE" )
    end
end

local function getSocialColor( social )
    if social=="raid" then
        return 255/255, 127/255, 0/255
    elseif social=="party" then
        return 166/255, 166/255, 255/255
    elseif social=="guild" then
        return 64/255, 255/255, 64/255
    elseif social=="bnet" then
        return 115/255, 199/255, 255/255
    elseif social=="friend" then
        return 254/255, 222/255, 41/255
    else
        return 255/255, 255/255, 255/255
    end
end

local function checkHealthbarNotVisible( data )
    if isInSanctuary or data.isTotem then
        return true
    elseif isInPve then
        if inCombat then
            if ( not db.healthBarFriendPveCombat and data.layout=="FRIEND" ) or ( not db.healthBarEnemyPveCombat and data.layout=="ENEMY" ) then
                return true
            end
        else
            if ( not db.healthBarFriendPve and data.layout=="FRIEND" ) or ( not db.healthBarEnemyPve and data.layout=="ENEMY" ) then
                return true
            end
        end
    elseif isInPvp then
        if ( not db.healthBarFriendPvp and data.layout=="FRIEND" ) or ( not db.healthBarEnemyPvp and data.layout=="ENEMY" ) then
            return true
        end
    end
    
    return false
end

local function checkCastbarNotVisible( data )
    if isInSanctuary or data.isTotem then
        return true
    elseif isInPve then
        if inCombat then
            if ( not db.castBarFriendPveCombat and data.layout=="FRIEND" ) or ( not db.castBarEnemyPveCombat and data.layout=="ENEMY" ) then
                return true
            end
        else
            if ( not db.castBarFriendPve and data.layout=="FRIEND" ) or ( not db.castBarEnemyPve and data.layout=="ENEMY" ) then
                return true
            end
        end
    elseif isInPvp then
        if ( not db.castBarFriendPvp and data.layout=="FRIEND" ) or ( not db.castBarEnemyPvp and data.layout=="ENEMY" ) then
            return true
        end
    end
    
    return false
end

local function checkBuffsNotVisible( data )
    if data.isTotem then
        return true
    elseif isInPve then
        if inCombat then
            if ( not db.buffsFriendPveCombat and data.layout=="FRIEND" ) or ( not db.buffsEnemyPveCombat and data.layout=="ENEMY" ) then
                return true
            end
        else
            if ( not db.buffsFriendPve and data.layout=="FRIEND" ) or ( not db.buffsEnemyPve and data.layout=="ENEMY" ) then
                return true
            end
        end
    elseif isInPvp then
        if ( not db.buffsFriendPvp and data.layout=="FRIEND" ) or ( not db.buffsEnemyPvp and data.layout=="ENEMY" ) then
            return true
        end
    else
        return false
    end
end

local function isNPCGrey( data )
    local r, g, b = unpack( getLevelColor( data.levelIs ) )
    
    return r==0.5 and g==0.5 and b==0.5
end

local function checkTankMode()
    if playerClass=="DEATHKNIGHT" then
        tankMode = UnitBuff( "player", GetSpellInfo( 48263 ) ) or false -- Frost Presence
    elseif playerClass=="PALADIN" then
        tankMode = UnitBuff( "player", GetSpellInfo( 25780 ) ) or false -- Righteous Fury
    elseif playerClass=="WARRIOR" then
        tankMode = GetShapeshiftForm()==2 or false -- Defensive Stance 
    elseif playerClass=="DRUID" then
        tankMode = GetShapeshiftForm()==1 or false or false -- Bear/Dire Bear Form 
    else
        tankMode = false
    end
end

--[[ Modify and update elements ]]
function update:collectFixedData()
    local data = self.data
    local savedData = self.savedData
    
    -- Fixed data
    data.nameIs = self.regions.nameText:GetText()
    data.nameIsReplacement = global.nameReplacer[data.nameIs] or nil
    data.isBoss = self.regions.dangerSkull:IsShown()
    data.isElite = self.regions.eliteIcon:IsShown()
    data.isTotem = getTotem( data.nameIs )
end

function update:collectDynamicData()
    local data = self.data
    local savedData = self.savedData
    
    -- Dynamic data
    _, data.maxHealthIs = self.children.healthBar:GetMinMaxValues()
    data.healthIs = self.children.healthBar:GetValue()
    data.levelIs = tonumber( self.regions.levelText:GetText() ) or 999
    data.isInCombat = getUnitCombatStatus( self.regions.nameText:GetTextColor() )
    data.hasRaidIcon = getRaidIconIndex( self.regions.raidIcon )
    data.raidIcon = self.regions.raidIcon
    if inCombat then data.threatState = threatByColor( self.regions.threatGlow ) else data.threatState = "LOW" end
    
    if temporaryList[data.nameIs] then
        data.GUID = temporaryList[data.nameIs]
        
        local success, _, _, class, guild, _ = DocsUI_Nameplates:Deserialize( temporaryList[data.GUID] )
        if success then
            data.class = class
            data.guild = guild
        end
    elseif playerList[data.nameIs] then
        data.GUID = playerList[data.nameIs]
        
        local success, _, _, class, guild, _ = DocsUI_Nameplates:Deserialize( playerList[data.GUID] )
        if success then
            data.class = class
            data.guild = guild
        end
    else
        if not inCombat and data.type~="PLAYER" and global.npcList[data.nameIs] then
            data.guild = global.npcList[data.nameIs]
        else
            data.guild = nil
        end
    end
    
    data.red, data.green, data.blue = self.children.healthBar:GetStatusBarColor()
    data.reaction, data.type = reactionAndType( data.red, data.green, data.blue, data )
    if not inCombat and isInSanctuary and data.reaction=="HOSTILE" then data.reaction = "FRIENDLY" end
    data.class, data.role = getClassAndRole( data )
    
    if data.reaction=="FRIENDLY" then
        data.layout = "FRIEND"
    elseif data.reaction=="NEUTRAL" then
        if global.agressiveList[data.nameIs] then
            data.layout = "ENEMY"
        else
            data.layout = "FRIEND"
        end
    else
        data.layout = "ENEMY"
    end
    
    data.healthNotVisible = checkHealthbarNotVisible( data )
    
    data.isCasting = self.children.castBar:IsShown()
    if data.isCasting then
        data.iscastingUninterruptable = self.regions.castNostop:IsShown()
        data.castIconTexture = self.regions.spellIcon:GetTexture()
        _, data.castingMax = self.children.castBar:GetMinMaxValues()
        data.castingTime = self.children.castBar:GetValue()
        data.castingName = UnitCastingInfo( "target" ) or UnitChannelInfo( "target" )
    end
    
    if inCombat and data.reaction=="NEUTRAL" and data.isInCombat and data.healthIs<data.maxHealthIs then -- Turn neutral target into agressive
        if not global.agressiveList[data.nameIs] then global.agressiveList[data.nameIs] = true end
    end
    
    data.isMouseover = self.highlight:IsShown()
    if data.isMouseover then data.GUID = UnitGUID( "mouseover" ) end
end

function update:name()
    -- self = nameplate
    
    if not unitIsDifferent then return end
    
    if self.data.isTotem then
        self.name:SetText( nil )
        
        return
    end
    
    self.name:SetText( self.data.nameIsReplacement or self.data.nameIs )
    
    if self.data.type=="PLAYER" and self.data.class then
        self.name:SetTextColor( RAID_CLASS_COLORS[self.data.class].r, RAID_CLASS_COLORS[self.data.class].g, RAID_CLASS_COLORS[self.data.class].b, db.fontColor.a )
    elseif raid[self.data.nameIs] and self.data.type=="PLAYER" then
        self.name:SetTextColor( getSocialColor( "raid" ), db.fontColor.a )
    elseif party[self.data.nameIs] and self.data.type=="PLAYER" then
        self.name:SetTextColor( getSocialColor( "party" ), db.fontColor.a )
    elseif guild[self.data.nameIs] and self.data.type=="PLAYER" then
        self.name:SetTextColor( getSocialColor( "guild" ), db.fontColor.a )
    elseif bnet[self.data.nameIs] and self.data.type=="PLAYER" then
        self.name:SetTextColor( getSocialColor( "bnet" ), db.fontColor.a )
    elseif friend[self.data.nameIs] and self.data.type=="PLAYER" then
        self.name:SetTextColor( getSocialColor( "friend" ), db.fontColor.a )
    elseif self.data.type~="PLAYER" and self.data.reaction~="HOSTILE" then
        self.name:SetTextColor( self.data.red, self.data.green, self.data.blue, 0.75 )
    else
        self.name:SetTextColor( lightenColor( self.data.red, self.data.green, self.data.blue, db.fontColor.a ) )
    end
end

function update:tot()
    -- self = nameplate
    
    if not unitIsDifferent or not db.tot then return end
    
    if self.data.isTotem or not isInArena then
        self.tot:SetText( nil )
        
        return
    end
    
    self.tot:SetText( "["..self.data.tot.."]" )
    
    if self.data.totClass then
        self.tot:SetTextColor( RAID_CLASS_COLORS[self.data.totClass].r, RAID_CLASS_COLORS[self.data.totClass].g, RAID_CLASS_COLORS[self.data.totClass].b, db.fontColor.a )
    else
        self.tot:SetTextColor( 0.5, 0.5, 0.5, db.fontColor.a )
    end
end

function update:guild()
    -- self = nameplate
    
    if not unitIsDifferent then return end
    
    if not db.guildName or self.data.isTotem or inCombat or isInPvp or self.data.layout=="ENEMY" or not self.data.guild or self.data.guild=="NONE" or ( isInInstance~="none" and self.data.type=="PLAYER" ) then
        self.guild:SetText( nil )
        
        return
    end
    
    self.guild:SetText( "<"..self.data.guild..">" )
    
    if self.data.type=="PLAYER" and raid[self.data.nameIs] then
        local r, g, b = getSocialColor( "raid" )
        self.guild:SetTextColor( r, g, b, db.fontColor.a )
    elseif self.data.type=="PLAYER" and party[self.data.nameIs] then
        local r, g, b = getSocialColor( "party" )
        self.guild:SetTextColor( r, g, b, db.fontColor.a )
    elseif self.data.type=="PLAYER" and guild[self.data.nameIs] then
        local r, g, b = getSocialColor( "guild" )
        self.guild:SetTextColor( r, g, b, db.fontColor.a )
    elseif self.data.type=="PLAYER" and bnet[self.data.nameIs] then
        local r, g, b = getSocialColor( "bnet" )
        self.guild:SetTextColor( r, g, b, db.fontColor.a )
    elseif self.data.type=="PLAYER" and friend[self.data.nameIs] then
        local r, g, b = getSocialColor( "friend" )
        self.guild:SetTextColor( r, g, b, db.fontColor.a )
    elseif self.data.type~="PLAYER" and self.data.reaction~="HOSTILE" then
        self.guild:SetTextColor( self.data.red, self.data.green, self.data.blue, 0.75 )
    else
        self.guild:SetTextColor( lightenColor( self.data.red, self.data.green, self.data.blue, db.fontColor.a ) )
    end
end

function update:socialIcon()
    -- self = nameplate
    
    if not unitIsDifferent then return end
    
    if not db.socialIcon or self.data.hasRaidIcon or self.data.isTotem or inCombat or isInPvp or isInInstance~="none" or self.data.layout=="ENEMY" or not self.data.class then
        self.socialIcon:Hide()
        
        return
    end
    
    if raid[self.data.nameIs] then
        local r, g, b = getSocialColor( "raid" )
        self.socialIcon:SetVertexColor( r, g, b, 1 )
        
        self.socialIcon:Show()
    elseif party[self.data.nameIs] then
        local r, g, b = getSocialColor( "party" )
        self.socialIcon:SetVertexColor( r, g, b, 1 )
        
        self.socialIcon:Show()
    elseif guild[self.data.nameIs] then
        local r, g, b = getSocialColor( "guild" )
        self.socialIcon:SetVertexColor( r, g, b, 1 )
        
        self.socialIcon:Show()
    elseif bnet[self.data.nameIs] then
        local r, g, b = getSocialColor( "bnet" )
        self.socialIcon:SetVertexColor( r, g, b, 1 )
        
        self.socialIcon:Show()
    elseif friend[self.data.nameIs] then
        local r, g, b = getSocialColor( "friend" )
        self.socialIcon:SetVertexColor( r, g, b, 1 )
        
        self.socialIcon:Show()
    else
        self.socialIcon:Hide()
    end
end

function update:hp( option )
    -- self = nameplate
    
    if option=="onlyHp" and not self.data.healthNotVisible then
        local healthIs = self.children.healthBar:GetValue()
        
        self.hp:SetValue( healthIs )
        
        return
    end
    
    if addon.configMode then
        self.data.healthIs = random( 1, 99 )
        self.data.maxHealthIs = 100
    end
    
    if self.data.healthNotVisible then
        self.hp:Hide()
        
        return
    end
    
    self.hp:SetValue( self.data.healthIs )
    
    if not unitIsDifferent then return end
    
    self.hp:Show()
    
    self.hp:SetMinMaxValues( 0, self.data.maxHealthIs )
    
    if self.data.class then
        self.hp:SetStatusBarColor( RAID_CLASS_COLORS[self.data.class].r,RAID_CLASS_COLORS[self.data.class].g,RAID_CLASS_COLORS[self.data.class].b )
    elseif not inCombat or not db.threatEnabled or not db.threatColor then --Normal color
        if isNPCGrey( self.data ) then
            self.hp:SetStatusBarColor( unpack( getLevelColor( self.data.levelIs ) ) )
        else
            self.hp:SetStatusBarColor( self.data.red, self.data.green, self.data.blue )
        end
    end
end

function update:level()
    -- self = nameplate
    
    if not unitIsDifferent then return end
    
    if not db.showLevel or self.data.type=="PLAYER" or self.data.isBoss or self.data.isTotem or self.data.layout=="FRIEND" then
        self.level:SetText( nil )
        
        return
    end
    
    local text = self.data.levelIs
    if text and self.data.isElite then text = text.."+" end
    
    self.level:SetText( text )
    
    self.level:SetTextColor( unpack( getLevelColor( self.data.levelIs ) ) )
end

function update:hpNumber( option )
    -- self = nameplate
    
    if option=="onlyHp" and ( db.showHpPercent or db.showHpNumber ) and not self.data.healthNotVisible then
        local healthIs = self.children.healthBar:GetValue()
        
        local text = ""
        if self.data.maxHealthIs and db.showHpPercent then
            local perc = min( ( healthIs/self.data.maxHealthIs )*100, 100 )
            
            if perc<1 then
                text = text..round( ( healthIs/self.data.maxHealthIs )*100, 1 ).."%"
            else
                text = text..floor( ( healthIs/self.data.maxHealthIs )*100 ).."%"
            end
        end
        if db.showHpNumber then text = text.." "..siValue( healthIs ) end
        self.hpNumber:SetText( text )
        
        return
    end
    
    if ( not db.showHpPercent and not db.showHpNumber ) or self.data.healthNotVisible then
        self.hpNumber:SetText( nil )
        
        return
    end
    
    local text = ""
    if self.data.maxHealthIs and db.showHpPercent then
        local perc = min( ( self.data.healthIs/self.data.maxHealthIs )*100, 100 )
        
        if perc<1 then
            text = text..round( ( self.data.healthIs/self.data.maxHealthIs )*100, 1 ).."%"
        else
            text = text..floor( ( self.data.healthIs/self.data.maxHealthIs )*100 ).."%"
        end
    end
    if db.showHpNumber then text = text.." "..siValue( self.data.healthIs ) end
    self.hpNumber:SetText( text )
    
    if not unitIsDifferent then return end
    
    if self.data.type=="PLAYER" and self.data.class then
        local r, g, b = lightenColor( RAID_CLASS_COLORS[self.data.class].r, RAID_CLASS_COLORS[self.data.class].g, RAID_CLASS_COLORS[self.data.class].b )
        self.hpNumber:SetTextColor( r, g, b, db.fontColor.a )
    elseif raid[self.data.nameIs] and self.data.type=="PLAYER" then
        self.hpNumber:SetTextColor( getSocialColor( "raid" ), db.fontColor.a )
    elseif party[self.data.nameIs] and self.data.type=="PLAYER" then
        self.hpNumber:SetTextColor( getSocialColor( "party" ), db.fontColor.a )
    elseif guild[self.data.nameIs] and self.data.type=="PLAYER" then
        self.hpNumber:SetTextColor( getSocialColor( "guild" ), db.fontColor.a )
    elseif bnet[self.data.nameIs] and self.data.type=="PLAYER" then
        self.hpNumber:SetTextColor( getSocialColor( "bnet" ), db.fontColor.a )
    elseif friend[self.data.nameIs] and self.data.type=="PLAYER" then
        self.hpNumber:SetTextColor( getSocialColor( "friend" ), db.fontColor.a )
    elseif not db.threatColor then --Normal color
        if not inCombat and isNPCGrey( self.data ) then
            local r, g, b = unpack( getLevelColor( self.data.levelIs ) )
            r, g, b = lightenColor( r, g, b )
            self.hpNumber:SetTextColor( r, g, b, db.fontColor.a )
        else
            local r, g, b = lightenColor( self.data.red, self.data.green, self.data.blue )
            self.hpNumber:SetTextColor( r, g, b, db.fontColor.a )
        end
    end
end

function update:cast( option )
    -- self = nameplate
    
    if addon.configMode and self.data.layout~="FRIEND" then
        self.data.isCasting = true
        self.data.castingMax = 2
        self.data.castingTime = 1.1
        self.data.castingName = "Config Spell"
    end
    
    if self.data.GUID and guidCasts[self.data.GUID] then
        if guidCasts[self.data.GUID].isCasting and not checkCastbarNotVisible( self.data ) then
            if guidCasts[self.data.GUID].alwaysFilled then
                self.cast:SetMinMaxValues( 0, 1 )
                self.cast:SetValue( 1 )
                self.cast.time:SetText( nil )
                self.cast.text:SetText( guidCasts[self.data.GUID].name )
            else
                self.cast:SetMinMaxValues( 0, guidCasts[self.data.GUID].max )
                self.cast:SetValue( guidCasts[self.data.GUID].value )
                self.cast.time:SetText( guidCasts[self.data.GUID].value )
                self.cast.text:SetText( guidCasts[self.data.GUID].name )
            end
            
            self.cast.icon.texture2:SetTexture( guidCasts[self.data.GUID].icon or "Interface\\Icons\\Inv_misc_questionmark" )
            
            if guidCasts[self.data.GUID].notInterruptible then
                self.cast:SetStatusBarColor( 0.5,0.5,0.5,0.75 )
            else
                self.cast:SetStatusBarColor( db.castColor.r,db.castColor.g,db.castColor.b,db.castColor.a )
            end
            
            self.cast.GUID = guidCasts[self.data.GUID].GUID
            --self.cast.unit = guidCasts[self.data.GUID].unit
            
            self.cast:Show()
        else
            guidCasts[self.data.GUID].isCasting = false
            guidCasts[self.data.GUID].realUnit = false
            
            self.cast:Hide()
        end
    end
end

function update:raidIcon()
    -- self = nameplate
    
    if self.data.hasRaidIcon then
        self.raidIcon:Show()
        self.raidIcon:SetTexCoord( self.data.raidIcon:GetTexCoord() )
    else
        self.raidIcon:Hide()
    end
end

function update:classIcon()
    -- self = nameplate
    
    if not unitIsDifferent then return end
    
    if self.data.isTotem or self.data.layout=="FRIEND" then
        self.classIcon:Hide()
        
        return
    end
    
    if not db.classIcon then
        self.classIcon:Hide()
    elseif self.data.class then
        local coords = resources.classIcon[self.data.class]
        self.classIcon:SetTexCoord( coords[1], coords[2], coords[3], coords[4] )
        self.classIcon:Show()
    else
        self.classIcon:Hide()
    end
end

function update:roleIcon()
    -- self = nameplate
    
    if not unitIsDifferent then return end
    
    if self.data.isTotem or self.data.layout=="FRIEND" then
        self.roleIcon:Hide()
        
        return
    end
    
    if not db.roleIcon then
        self.roleIcon:Hide()
    elseif self.data.type~="PLAYER" and self.data.role then
        local role = resources.roleTextureCoords[self.data.role]
        if type( role )=="table" then
            self.roleIcon:SetTexCoord( unpack( resources.roleTextureCoords[self.data.role] ) )
            self.roleIcon:Show()
        else
            self.roleIcon:Hide()
        end
    else
        self.roleIcon:Hide()
    end
end

function update:totemIcon()
    -- self = nameplate
    
    if not unitIsDifferent then return end
    
    if self.data.isTotem then
        self.totemIcon:Show()
        self.totemIcon:SetTexture( self.data.isTotem )
    else
        self.totemIcon:Hide()
    end
end

function update:unknownIcon()
    -- self = nameplate
    
    if not unitIsDifferent and not addon.configMode then return end
    
    if not db.unknownIcon or self.data.isTotem then
        self.unknownIcon:Hide()
    elseif self.data.GUID or ( self.data.type~="PLAYER" and self.data.layout=="FRIEND" and global.npcList[self.data.nameIs] ) then
        self.unknownIcon:Hide()
    else
        self.unknownIcon:Show()
    end
end

function update:lfgIcon()
    -- self = nameplate
    
    if not unitIsDifferent and not addon.configMode then return end
    
    if db.lfgIcon and lfgList[self.data.nameIs] then
        self.lfgIconText:SetText( lfgList[self.data.nameIs] )
            
        self.lfgIcon:Show()
    else
        self.lfgIconText:SetText( nil )
        
        self.lfgIcon:Hide()
    end
end

function update:chatIcon()
    -- self = nameplate
    
    if not unitIsDifferent and not addon.configMode then return end
    
    local w = whisperList[self.data.nameIs]
    
    if not inCombat and w and w+120>time then
        self.chatIcon:Show()
    else
        w = nil
        
        self.chatIcon:Hide()
    end
end

function update:threat( option ) -- (and Hp color)
    -- self = nameplate
    
    if option=="onlyThreat" and db.threatEnabled then
        self.data.threatState = threatByColor( self.regions.threatGlow )
    end
    
    if addon.configMode and db.threatEnabled and self.data.layout~="FRIEND" then
        self.data.threatState = "HIGH"
    elseif not db.threatEnabled or not inCombat or self.data.class or self.data.isTotem or self.data.layout=="FRIEND" or ( playerClass~="HUNTER" and not hasGroup ) then
        if self.aggroAlertIcon.visible then self.aggroAlertIcon:Hide() end
        self.aggroAlertIcon.visible = false
        
        return
    end
    
    if tankMode then
        if not self.data.threatState or self.data.threatState=="HIGH" then
            self.data.threatState = 1
        elseif self.data.threatState=="MEDIUM" then
            self.data.threatState = 2
        elseif self.data.threatState=="LOW" then
            self.data.threatState = 3
        end
    else
        if not self.data.threatState or self.data.threatState=="LOW" then
            self.data.threatState = 1
        elseif self.data.threatState=="MEDIUM" then
            self.data.threatState = 2
        elseif self.data.threatState=="HIGH" then
            self.data.threatState = 3
        end
    end
    
    -- Threat icon
    if db.threatIcon then
        if self.data.threatState==1 then
            if not self.aggroAlertIcon.visible then self.aggroAlertIcon:Show() end
            self.aggroAlertIcon.visible = true
            
            self.aggroAlertIcon:SetVertexColor( db.colorThreatSafe.r,db.colorThreatSafe.g,db.colorThreatSafe.b,db.colorThreatSafe.a )
        elseif self.data.threatState==2 then
            if not self.aggroAlertIcon.visible then self.aggroAlertIcon:Show() end
            self.aggroAlertIcon.visible = true
            
            self.aggroAlertIcon:SetVertexColor( db.colorThreatUnsafe.r,db.colorThreatUnsafe.g,db.colorThreatUnsafe.b,db.colorThreatUnsafe.a )
        elseif self.data.threatState==3 then
            if not self.aggroAlertIcon.visible then self.aggroAlertIcon:Show() end
            self.aggroAlertIcon.visible = true
            
            self.aggroAlertIcon:SetVertexColor( db.colorThreatAlert.r,db.colorThreatAlert.g,db.colorThreatAlert.b,db.colorThreatAlert.a )
        end
    else
        if self.aggroAlertIcon.visible then self.aggroAlertIcon:Hide() end
        self.aggroAlertIcon.visible = false
    end
    
    -- Threat bar
    --[[if db.threatBar then
        if self.data.threatState==1 then
            self.threatBar:Hide()
        elseif self.data.threatState==2 then
            self.threatBar:Show()
            self.threatBar:SetValue( 80 )
            
            self.threatBar:SetStatusBarColor( db.colorThreatUnsafe.r,db.colorThreatUnsafe.g,db.colorThreatUnsafe.b,db.colorThreatUnsafe.a )
        elseif self.data.threatState==3 then
            self.threatBar:Show()
            self.threatBar:SetValue( 100 )
            
            self.threatBar:SetStatusBarColor( db.colorThreatAlert.r,db.colorThreatAlert.g,db.colorThreatAlert.b,db.colorThreatAlert.a )
        end
    else
        if self.threatBar:IsShown() then self.threatBar:Hide() end
    end]]
    
    -- Threat color
    if db.threatColor and not self.data.healthNotVisible then
        if self.data.threatState==1 then
            self.hp:SetStatusBarColor( db.colorThreatSafe.r,db.colorThreatSafe.g,db.colorThreatSafe.b,db.colorThreatSafe.a )
        elseif self.data.threatState==2 then
            self.hp:SetStatusBarColor( db.colorThreatUnsafe.r,db.colorThreatUnsafe.g,db.colorThreatUnsafe.b,db.colorThreatUnsafe.a )
        elseif self.data.threatState==3 then
            self.hp:SetStatusBarColor( db.colorThreatAlert.r,db.colorThreatAlert.g,db.colorThreatAlert.b,db.colorThreatAlert.a )
        end
    else
        self.hp:SetStatusBarColor( self.data.red, self.data.green, self.data.blue )
        
        r, g, b = lightenColor( self.data.red, self.data.green, self.data.blue )
        self.hpNumber:SetTextColor( r, g, b, db.fontColor.a )
    end
    
    -- isTank
    --[[ Threat colored hp bar
    if true or self.data.class or not db.threatColor then
        return
    end
    if not db.threatBar or checkHealthbarNotVisible( self.data ) then
        if self.threatBar:IsShown() then self.threatBar:Hide() end
        
        return
    end
    
    if tankMode and self.data.threatIsTanked then
        local r,g,b = self.hp:GetStatusBarColor()
        if r~=db.colorThreatSafe.r or g~=db.colorThreatSafe.g or b~=db.colorThreatSafe.b then
            self.hp:SetStatusBarColor( db.colorThreatSafe.r,db.colorThreatSafe.g,db.colorThreatSafe.b,db.colorThreatSafe.a )
        end
    elseif self.data.threatStatePercent and self.data.threatStatePercent<=100 then -- Color gradient bar, when we have the exakt threat value
        if self.data.threatStatePercent<50 then
            local percent = self.data.threatStatePercent*2/100
            
            self.hp:SetStatusBarColor( resources.colorGradient( percent, db.colorThreatSafe, db.colorThreatUnsafe ) )
        elseif self.data.threatStatePercent>=50 then
            local percent = ( self.data.threatStatePercent-50 )*2/100
            
            self.hp:SetStatusBarColor( resources.colorGradient( percent, db.colorThreatUnsafe, db.colorThreatAlert ) )
        end
    elseif self.data.threatState==1 then -- Fall back to basic threat coloring
        
        
        
    elseif self.data.threatState==2 then
        
    else
        
    end]]
end

function update:size()
    -- self = frame
    
    if not unitIsDifferent then return end
    
    if inCombat then
        return
    elseif self.nameplate.data.layout=="FRIEND" then
        self:SetWidth( self.nameplate.originalWidth/3.8 )
        self:SetHeight( self.nameplate.originalHeight/2 )
    elseif self.nameplate.data.layout=="ENEMY" then
        self:SetWidth( self.nameplate.originalWidth )
        self:SetHeight( self.nameplate.originalHeight )
    end
end

function update:highlight()
    -- self = nameplate
    
    if not unitIsDifferent then return end
    
    -- Highlight over name
    if checkHealthbarNotVisible( self.data ) then
        self.highlight:ClearAllPoints()
        self.highlight:SetPoint( "CENTER", self.name, "CENTER", 0, -1 )
        self.highlight:SetWidth( db.width*2 )
        self.highlight:SetHeight( ( db.fontsize-2 )*3 )
        
        self.highlight:SetTexture( "Interface\\Addons\\DocsUI_Nameplates\\highlightName" )
        
        self.highlight:SetVertexColor( 1,1,1,0.25 )
    -- Highlight on hp bar
    else
        self.highlight:ClearAllPoints()
        self.highlight:SetAllPoints( self.hp )
        
        self.highlight:SetTexture( "Interface\\Addons\\DocsUI_Nameplates\\highlight" )
        
        self.highlight:SetVertexColor( 1,1,1,1 )
    end
end

function update:alpha()
    -- self = nameplate
    
    if self.data.isTarget then
        self:SetAlpha( 1 )
    else
        if self.data.isTotem then
            self:SetAlpha( 1 )
        elseif self.data.hasRaidIcon then
            self:SetAlpha( 1 )
        elseif self.data.alphaIs<1 then
            self:SetAlpha( db.alpha )
        else
            self:SetAlpha( 1 )
        end
    end
    
end

function update:mouseover()
    -- self = nameplate
    
    if self.data.isTarget then
        self.targetHighlight:Show()
        
        --[[if inCombat then
            temp = UnitName( "targettarget" )
            
            if name~=playerName and isTank[name] then
                self.data.threatIsTanked = true
            else
                self.data.threatIsTanked = false
            end
            _, _, self.data.threatStatePercent = UnitDetailedThreatSituation( "player", "target" )
            self.data.threatStateTimestamp = time
        end]]
        
        self:SetFrameLevel( 21 )
    elseif self.data.isMouseover then
        self.targetHighlight:Hide()
        
        self.data.GUID = UnitGUID( "mouseover" )
        
        if UnitCastingInfo( "mouseover" ) then addon:UNIT_SPELLCAST_START( "UNIT_SPELLCAST_START", "mouseover" ) end
        
        --[[if inCombat and UnitExists( "mouseover" ) then
            local name = UnitName( "mouseovertarget" )
            
            if name~=playerName and isTank[name] then
                self.data.threatIsTanked = true
            else
                self.data.threatIsTanked = false
            end
            _, _, self.data.threatStatePercent = UnitDetailedThreatSituation( "player", "mouseover" )
            self.data.threatStateTimestamp = time
        end]]
        
        self:SetFrameLevel( 22 )
    else
        self.targetHighlight:Hide()
        
        self:SetFrameLevel( 11 )
    end
end

function update:auras()
    -- self = nameplate
    
    if checkBuffsNotVisible( self.data ) then
        for i=1,db.buffsNumber do
            if self.buffs[i].visible then
                self.buffs[i].expirationTime = 0
                self.buffs[i]:Hide()
                self.buffs[i].visible = false
            end
            
            if self.debuffs[i].visible then
                self.debuffs[i].expirationTime = 0
                self.debuffs[i]:Hide()
                self.debuffs[i].visible = false
            end
        end
        
        return
    end
    
    if addon.configMode then return end
    
    local GUID = self.data.GUID
    
    if GUID and guidBuffs[GUID] then
        for i=1,#guidBuffs[GUID] do
            if guidBuffs[GUID][i] then
                if guidBuffs[GUID][i].expirationTime and guidBuffs[GUID][i].expirationTime>0 and time>guidBuffs[GUID][i].expirationTime then
                    tremove( guidBuffs[GUID], i )
                end
            end
        end
        
        sort( guidBuffs[GUID], function( a, b ) 
            if a and b then 
                if a.priority==b.priority then
                    if a.expirationTime==b.expirationTime then
                        return a.name<b.name
                    else
                        return ( a.expirationTime or 0 )<( b.expirationTime or 0 )
                    end
                else
                    return a.priority>b.priority
                end
            end 
        end )
        
        local counterBuffs = 1
        local counterDebuffs = 1
        for i=1,#guidBuffs[GUID] do
            if counterBuffs<=db.buffsNumber and guidBuffs[GUID][i].filter=="HELPFUL" then
                self.buffs[counterBuffs].spellName = guidBuffs[GUID][i].name or ""
                self.buffs[counterBuffs].expirationTime = guidBuffs[GUID][i].expirationTime or 0
                self.buffs[counterBuffs].duration = guidBuffs[GUID][i].duration or 1
                self.buffs[counterBuffs].count = guidBuffs[GUID][i].count or 0
                self.buffs[counterBuffs].filter = guidBuffs[GUID][i].filter
                
                self.buffs[counterBuffs].icon:SetTexture( "Interface\\Icons\\"..guidBuffs[GUID][i].icon )
                
                self.buffs[counterBuffs]:Show()
                self.buffs[counterBuffs].visible = true
                local c = self.buffs[counterBuffs].count
                if c>1 then
                    self.buffs[counterBuffs].counter:SetText( c )
                else
                    self.buffs[counterBuffs].counter:SetText( nil )
                end
                local e = self.buffs[counterBuffs].expirationTime
                if e>0 then
                    self.buffs[counterBuffs].time:SetText( formatTime( e-time ) )
                else
                    self.buffs[counterBuffs].time:SetText( nil )
                end
                
                counterBuffs = counterBuffs+1
            elseif counterDebuffs<=db.buffsNumber and guidBuffs[GUID][i].filter=="HARMFUL" then
                self.debuffs[counterDebuffs].spellName = guidBuffs[GUID][i].name or ""
                self.debuffs[counterDebuffs].expirationTime = guidBuffs[GUID][i].expirationTime or 0
                self.debuffs[counterDebuffs].duration = guidBuffs[GUID][i].duration or 1
                self.debuffs[counterDebuffs].count = guidBuffs[GUID][i].count or 0
                self.debuffs[counterDebuffs].filter = guidBuffs[GUID][i].filter
                
                self.debuffs[counterDebuffs].icon:SetTexture( "Interface\\Icons\\"..guidBuffs[GUID][i].icon )
                
                self.debuffs[counterDebuffs]:Show()
                self.debuffs[counterDebuffs].visible = true
                local c = self.debuffs[counterDebuffs].count
                if c>1 then
                    self.debuffs[counterDebuffs].counter:SetText( c )
                else
                    self.debuffs[counterDebuffs].counter:SetText( nil )
                end
                self.debuffs[counterDebuffs].time:SetText( formatTime( self.debuffs[counterDebuffs].expirationTime-time ) )
                
                counterDebuffs = counterDebuffs+1
            else
                break
            end
        end
        
        for i=counterBuffs,db.buffsNumber do
            if self.buffs[i].visible then
                self.buffs[i].expirationTime = 0
                self.buffs[i]:Hide()
                self.buffs[i].visible = false
            end
        end
        
        for i=counterDebuffs,db.buffsNumber do
            if self.debuffs[i].visible then
                self.debuffs[i].expirationTime = 0
                self.debuffs[i]:Hide()
                self.debuffs[i].visible = false
            end
        end
    else
        for i=1,db.buffsNumber do
            if self.buffs[i].visible then
                self.buffs[i].expirationTime = 0
                self.buffs[i]:Hide()
                self.buffs[i].visible = false
            end
            
            if self.debuffs[i].visible then
                self.debuffs[i].expirationTime = 0
                self.debuffs[i]:Hide()
                self.debuffs[i].visible = false
            end
        end
    end
end

function update:bossplates()
    -- self = nameplate
    
    bossplateUpdate( self )
end

function update:debug()
    -- self = nameplate
    
    if not db.debugMode then --TODO debug option
        self.debug.bg:Hide()
        self.debug.text:SetText( nil )
        
        return
    else
        self.debug.bg:Show()
    end
    
    local text = ""
    
    text = text.."Frame: "
    text = text..tostring( self.number )
    text = text.."\n"
    
    text = text.."Updates: "
    text = text..tostring( self.updateCounter )
    text = text.."\n"
    
    text = text.."Updates (hp): "
    text = text..tostring( self.updateCounterHp )
    text = text.."\n"
    
    text = text.."Updates (threat): "
    text = text..tostring( self.updateCounterThreat )
    text = text.."\n"
    
    text = text.."Updates (aura): "
    text = text..tostring( self.updateCounterAura )
    text = text.."\n"
    
    text = text.."Updates (cast): "
    text = text..tostring( self.updateCounterCast )
    text = text.."\n"
    
    text = text.."GUID: "
    text = text..tostring( self.data.GUID )
    text = text.."\n"
    --[[
    if self.data.list then
        text = text.."List: "
        if temporaryList[self.data.nameIs] then
            text = text..tostring( "temporaryList" )
        elseif playerList[self.data.nameIs] then
            text = text..tostring( "playerList" )
        end
        text = text.."\n"
    end
    ]]
    --[[
    text = text.."Layout: "
    text = text..tostring( tostring( self.data.layout ) )
    text = text.."\n"
    ]]
    text = text.."Guild: "
    text = text..tostring( self.data.guild )
    text = text.."\n"
    
    text = text.."hasRaidIcon: "
    text = text..tostring( self.data.hasRaidIcon )
    text = text.."\n"
    --[[
    if self.data.GUID and guidBuffs[self.data.GUID] then
        text = text.."Buffs: "
        for i=1,#guidBuffs[self.data.GUID] do
            if guidBuffs[self.data.GUID][i].filter=="HELPFUL" then
                text = text..tostring( guidBuffs[self.data.GUID][i].name )..", "
            end
        end
        text = text.."\n"
        
        text = text.."Debuffs: "
        for i=1,#guidBuffs[self.data.GUID] do
            if guidBuffs[self.data.GUID][i].filter=="HARMFUL" then
                text = text..tostring( guidBuffs[self.data.GUID][i].name )..", "
            end
        end
        text = text.."\n"
    end
    ]]
    self.debug.text:SetText( text )
end

--[[ Set up the nameplate ]]
local overlayType = "Texture"
local overlayBorderTexture = "Interface\\Tooltips\\Nameplate-Border"
local function isValidFrame( frame )
    if frame:GetName() then
        return false
    end

    overlayRegion = select( 2, frame:GetRegions() )
    
    if overlayRegion and overlayRegion:GetObjectType()==overlayType and overlayRegion:GetTexture()==overlayBorderTexture then
        frame.isNameplate = true
        return true
    end
    
    return false
end

local function onCreateChildFrames( ... )
    for i=1, select( "#", ... ) do 
		local frame = select( i, ... )
		if isValidFrame( frame ) and not frame.nameplate then DocsUI_Nameplates.createNewNameplate( frame ) end
    end
end

--[[ OnUpdate nameplate ]]
local nameplatesUpdater = CreateFrame( "Frame", nil, WorldFrame )
nameplatesUpdater:SetFrameStrata( "TOOLTIP" )  -- When parented to WorldFrame, causes OnUpdate to run close to last.
local lastUpdateNameplates = 0
local lastUpdateTanks = 0
local lastUpdateTankMode = 0
local numChildren = -1
local function onUpdate( self, elapsed ) -- onUpdate
	lastUpdateNameplates = lastUpdateNameplates + elapsed
    lastUpdateTanks = lastUpdateTanks + elapsed
    lastUpdateTankMode = lastUpdateTankMode + elapsed
    
    if true or lastUpdateNameplates>0 then -- No delay allowed
		local curChildren = WorldFrame:GetNumChildren()
        if curChildren~=numChildren then
            numChildren = curChildren
            onCreateChildFrames( WorldFrame:GetChildren() )
        end
        
        if playerTarget then
            for index,value in pairs( framesShown ) do
                local alpha = value:GetAlpha()
                
                value.nameplate.alpha = alpha or 1 -- Set nameplate alpha.
                if alpha~=1 then value:SetAlpha( 1 ) end -- Set to 1 to allow child frames to use their full alpha range.
            end
        end
        
        for index,value in pairs( doUpdateNext.frames ) do
            updateNameplate( index, value )
            
            doUpdateNext.frames[index] = nil
        end
        
        for index,value in pairs( doUpdateNext.name ) do
            updateNameplateByName( index, value )
            
            doUpdateNext.name[index] = nil
        end
        
        for index,value in pairs( doUpdateNext.GUID ) do
            updateNameplateByGUID( index, value )
            
            doUpdateNext.GUID[index] = nil
        end
        
        if doUpdateThreat>0 then
            doUpdateThreat = doUpdateThreat-1
            
            for index,value in pairs( framesShown ) do
                updateNameplate( value, "onlyThreat" )
            end
        end
        
        if doUpdate>0 then
            doUpdate = doUpdate-1
            
            for index,value in pairs( framesShown ) do
                updateNameplate( value )
            end
        end
        
        lastUpdateNameplates = 0
	end
    
    if not inCombat and lastUpdateTanks>20 then
        resources.searchForTanks()
        
        lastUpdateTanks = 0
    end
    
    if lastUpdateTankMode>10 then
        checkTankMode()
        
        lastUpdateTanks = 0
    end
    
end

--[[ Events ]]
function DocsUI_Nameplates:PLAYER_LEVEL_UP()
    playerLevel = UnitLevel( "player" )
end

function DocsUI_Nameplates:UPDATE_MOUSEOVER_UNIT()
    if UnitExists( "mouseover" ) then
        scanUnitAuras( "mouseover", playerTargetGUID )
        
        local GUID = UnitGUID( "mouseover" )
        local name, rlm = UnitName( "mouseover" )
        
        if not inCombat then
            local isPlayer = UnitIsPlayer( "mouseover" )
            local reaction = UnitReaction( "mouseover", "player" ) -- 4 is neutral
            
            if not isPlayer then
                DocsScanningTooltip:ClearLines()
                DocsScanningTooltip:SetUnit( "mouseover" )
                local text = DocsScanningTooltipTextLeft3:GetText()
                
                if text then
                    _, text = strsplit( " ", text )
                    
                    if text=="??" or tonumber( text ) then
                        global.npcList[name] = DocsScanningTooltipTextLeft2:GetText()
                    else
                        global.npcList[name] = "NONE"
                    end
                else
                    global.npcList[name] = "NONE"
                end
                
                if UnitClassification( "mouseover" )=="worldboss" then collectUnitInfo( "mouseover" ) end
            else
                collectUnitInfo( "mouseover" )
            end
        else
            collectUnitInfo( "mouseover" )
        end
        
        --setDoUpdateMe( name, "name" ) -- We have to delay it or every plate with that name will be the target.
    end
    
    setDoUpdate( 1 )
end

function DocsUI_Nameplates:PLAYER_REGEN_ENABLED()
    inCombat = false
    
    wipe( whisperList )
    wipe( guidCasts )
    
    DocsUI_Nameplates.bossDisable()
    
    setVisibility()
    softReset()
end

function DocsUI_Nameplates:PLAYER_REGEN_DISABLED()
    inCombat = true
    
    wipe( whisperList )
    wipe( guidCasts )
    
    if isInPve and ( isInInstance~="none" or db.debugMode ) then
        DocsUI_Nameplates:bossCheck()
    end
    
    setVisibility()
    softReset()
end

function DocsUI_Nameplates:PLAYER_TARGET_CHANGED()
    if UnitExists( "target" )==1 then
        playerTarget = UnitName( "target" ) or "UNKNOWN"
        playerTargetGUID = UnitGUID( "target" )
        
        collectUnitInfo( "target" )
        
        scanUnitAuras( "target", playerTargetGUID )
        
        if UnitCastingInfo( "target" ) then addon:UNIT_SPELLCAST_START( "UNIT_SPELLCAST_START", "target" ) end
    else
        playerTarget = false
        playerTargetGUID = nil
        
        for index,value in pairs( framesShown ) do
            value.nameplate.alpha = 1 -- Reset nameplate alpha
        end
    end
    
    softReset()
end

function DocsUI_Nameplates:RAID_TARGET_UPDATE()
    for i=1,#frames do
        frames[i].nameplate.data.hasRaidIcon = false
        
        frames[i].nameplate.raidIcon:Hide()
    end
    
    setDoUpdate( 1 )
end

function DocsUI_Nameplates:UNIT_TARGET( self, unit ) -- 0.5s delay
    if not UnitExists( unit ) or UnitIsUnit( "player", unit ) then return end
    
    local unitTarget = unit.."target"
    local raidIconIndex = GetRaidTargetIndex( unitTarget )
    
    if raidIconIndex and raidIconIndex>0 then
        updateNameplateByRaidIcon( raidIconIndex, UnitGUID( unitTarget ) )
    end
    
    if UnitIsPlayer( unitTarget ) then
        collectUnitInfo( unitTarget )
    end
    
    if isInArena and strfind( unit, "arena" ) then
        local GUID = UnitGUID( unit )
        
        for index,value in pairs( frames ) do
            if GUID and value.nameplate.data.GUID and GUID==value.nameplate.data.GUID then
                if unitTarget then
                    value.nameplate.data.tot = UnitName( unitTarget )
                    _, value.nameplate.data.totClass = UnitClass( unitTarget )
                else
                    value.nameplate.data.tot = nil
                end
                
                updateNameplate( value, option )
                    
                return
            end
        end
    end
end

function DocsUI_Nameplates:UNIT_THREAT_LIST_UPDATE()
    setDoUpdateThreat( 1 )
end

local zoneCheckFrame = CreateFrame( "Frame", nil, WorldFrame )
zoneCheckFrame:SetFrameStrata( "TOOLTIP" )
zoneCheckFrame:Hide()
zoneCheckFrame.lastUpdate = 0
local function zoneCheckFrameOnUpdate( self, elapsed ) -- Checks what type of instance we are in
	if self.lastUpdate+elapsed>self.lastUpdate+1 then
        self.lastUpdate = 0
    else
        self.lastUpdate = self.lastUpdate+elapsed
    end
	
    if self.lastUpdate>5 then
        self:Hide()
        
        --[[p( ">>>  "..GetTime() )
        p("isInPve: "..tostring(isInPve))
        p("isInPvp: "..tostring(isInPvp))
        p("isInInstance: "..tostring(isInInstance))
        p("isInSanctuary: "..tostring(isInSanctuary))]]
        
        --setDoUpdate( 1 )
        setVisibility() --TODO force refresh instead
        softReset()
        
        return
    elseif self.lastUpdate>1 then
        local isIn, type = IsInInstance()
        
        isInInstance = type or "none"
        isInSanctuary = UnitIsPVPSanctuary( "player" )
        hasGroup = GetNumPartyMembers()>1 or false
        
        if type=="pvp" then
            isInBattleground = true
            isInPvp = true
            
            isInPve = false
        elseif type=="arena" then
            isInArena = true
            isInPvp = true
            
            isInPve = false
        else
            isInPve = true
            isInPvp = false
            
            isInArena = false
            isInBattleground = false
        end
    end
end
zoneCheckFrame:SetScript( "OnUpdate", zoneCheckFrameOnUpdate )
local function zoneCheck()
    if GetGuildInfo( "player" ) then GuildRoster() end
    
    zoneCheckFrame.lastUpdate = 0
    zoneCheckFrame:Show()
end

function DocsUI_Nameplates:ZONE_CHANGED()
    zoneCheck()
end

function DocsUI_Nameplates:ZONE_CHANGED_INDOORS()
    zoneCheck()
end

function DocsUI_Nameplates:ZONE_CHANGED_NEW_AREA()
    zoneCheck()
    
    wipe( temporaryList )
    wipe( guidCasts )
    
    if db.combatLogFix then CombatLogClearEntries() end
end

DocsUI_Nameplates.raid = {}
local function checkRaidMembers()
    local raid = DocsUI_Nameplates.raid
    
    wipe( raid )
    
    for i=1,GetNumRaidMembers() do
        local unit = "raid"..i
        
        collectUnitInfo( unit )
        
        local GUID = UnitGUID( unit )
        local name, rlm = UnitName( unit )
        local _, class = UnitClass( unit )
        local guildName = GetGuildInfo( unit )
        
        raid[GUID] = raid[GUID] or {}
        
        raid[GUID].name = name
        raid[GUID].class = class
        raid[GUID].realm = rlm or realmName
        raid[GUID].guild = guildName or "NONE"
        raid[GUID].unit = unit
        
        raid[name] = GUID
        raid[unit] = name
    end
    
    setDoUpdate( 1 )
end

DocsUI_Nameplates.party = {}
local function checkPartyMembers()
    local party = DocsUI_Nameplates.party
    
    wipe( party )
    
    for i=1,GetNumPartyMembers() do
        local unit = "party"..i
        
        collectUnitInfo( unit )
        
        local GUID = UnitGUID( unit )
        local name, rlm = UnitName( unit )
        local _, class = UnitClass( unit )
        local guildName = GetGuildInfo( unit )
        
        party[GUID] = party[GUID] or {}
        
        party[GUID].name = name
        party[GUID].class = class
        party[GUID].realm = rlm or realmName
        party[GUID].guild = guildName or "NONE"
        party[GUID].unit = unit
        
        party[name] = GUID
        party[unit] = name
    end
    
    hasGroup = GetNumPartyMembers()>1 or false
    
    setDoUpdate( 1 )
end

DocsUI_Nameplates.arena = {}
local function checkArenaMembers()
    local arena = DocsUI_Nameplates.arena
    
    wipe( arena )
    
    local i = 1
    while UnitExists( "arena"..i ) do
        local unit = "arena"..i
        
        collectUnitInfo( unit )
        
        local GUID = UnitGUID( unit )
        local name, rlm = UnitName( unit )
        local _, class = UnitClass( unit )
        local guildName = GetGuildInfo( unit )
        
        arena[GUID] = arena[GUID] or {}
        
        arena[GUID].name = name
        arena[GUID].class = class
        arena[GUID].realm = rlm or realmName
        arena[GUID].guild = guildName or "NONE"
        arena[GUID].unit = unit
        
        arena[name] = GUID
        arena[unit] = name
        
        i = i+1
    end
    
    setDoUpdate( 1 )
end

DocsUI_Nameplates.battleground = {}

DocsUI_Nameplates.guild = {}
local function checkGuildMembers()
    wipe( DocsUI_Nameplates.guild )
    
    for i=1,GetNumGuildMembers() do
        local name = GetGuildRosterInfo(i)
        
        if name then DocsUI_Nameplates.guild[name] = true end
    end
end

DocsUI_Nameplates.bnet = {}
local function checkBnetFriends()
    wipe( DocsUI_Nameplates.bnet )
    
    for i=1,BNGetNumFriends() do
        local presenceID, givenName, surname, toonName = BNGetFriendInfo( i )
        
        if toonName then DocsUI_Nameplates.bnet[toonName] = true end
    end
end

DocsUI_Nameplates.friend = {}
local function checkFriendMembers()
    wipe( DocsUI_Nameplates.friend )
    
    for i=1,GetNumFriends() do
        local name = GetFriendInfo(i)
        
        if name then DocsUI_Nameplates.friend[name] = true end
    end
end

function DocsUI_Nameplates:RAID_ROSTER_UPDATE()
    checkRaidMembers()
    checkPartyMembers()
    checkGuildMembers()
    checkFriendMembers()
    checkBnetFriends()
    checkArenaMembers()
    DocsUI_Nameplates.resources.searchForTanks()
    
    setDoUpdate( 1 )
end
function DocsUI_Nameplates:PARTY_MEMBERS_CHANGED()
    checkRaidMembers()
    checkPartyMembers()
    checkGuildMembers()
    checkFriendMembers()
    checkBnetFriends()
    checkArenaMembers()
    
    setDoUpdate( 1 )
end
function DocsUI_Nameplates:GUILD_ROSTER_UPDATE()
    checkRaidMembers()
    checkPartyMembers()
    checkGuildMembers()
    checkFriendMembers()
    checkBnetFriends()
    checkArenaMembers()
    
    setDoUpdate( 1 )
end
function DocsUI_Nameplates:FRIENDLIST_UPDATE()
    checkRaidMembers()
    checkPartyMembers()
    checkGuildMembers()
    checkFriendMembers()
    checkBnetFriends()
    checkArenaMembers()
    
    setDoUpdate( 1 )
end
function DocsUI_Nameplates:BN_FRIEND_TOON_ONLINE()
    checkRaidMembers()
    checkPartyMembers()
    checkGuildMembers()
    checkFriendMembers()
    checkBnetFriends()
    checkArenaMembers()
    
    setDoUpdate( 1 )
end

function DocsUI_Nameplates:PLAYER_ENTERING_WORLD()
    checkRaidMembers()
    checkPartyMembers()
    checkGuildMembers()
    checkFriendMembers()
    checkBnetFriends()
    checkArenaMembers()
    
    zoneCheck()
    
    if db.combatLogFix then CombatLogClearEntries() end
    
    wipe( lfgList )
    wipe( whisperList )
    
    setDoUpdate( 1 )
end

function DocsUI_Nameplates:CHAT_MSG_CHANNEL( event, msg, sender, language, channelName, target, chatFlag, zoneID, channelNumber )
    if zoneID==2 or zoneID==26 then -- 2: Trade, 26: LFG
        local text = strlower( msg )
        
        for i=1,#global.LFGList do
            if strfind( strlower( text ), strlower( global.LFGList[i] ) ) then
                lfgList[sender] = msg
                
                setDoUpdate( 1 )
                
                break
            end
        end
    end
end

function DocsUI_Nameplates:CHAT_MSG_WHISPER( event, msg, sender, language, chatFlag )
    if msg then
        whisperList[sender] = GetTime()
        
        if not inCombat then softReset() end
    end
end

--[[ Initialize ]]
function DocsUI_Nameplates:initializeUpdater()
    -- Variables
    db = DocsUI_Nameplates.db.profile
    realm = DocsUI_Nameplates.db.realm
    global = DocsUI_Nameplates.db.global
    playerList = realm.playerList
    raid = DocsUI_Nameplates.raid
    party = DocsUI_Nameplates.party
    guild = DocsUI_Nameplates.guild
    bnet = DocsUI_Nameplates.bnet
    friend = DocsUI_Nameplates.friend
    arena = DocsUI_Nameplates.arena
    battleground = DocsUI_Nameplates.battleground
    isTank = DocsUI_Nameplates.isTank
    guidBuffs = DocsUI_Nameplates.GUIDBuffs
    guidCasts = DocsUI_Nameplates.GUIDCasts
    scanUnitAuras = DocsUI_Nameplates.scanUnitAuras
    bossplateUpdate = DocsUI_Nameplates.updateBossplate
    frames = DocsUI_Nameplates.frames
    
    isInPvp = false
    isInPve = true
    local _, type = IsInInstance()
    isInInstance = type or "none"
    isInBattleground = false
    isInArena = false
    isInSanctuary = false
    hasGroup = false
    
    -- Events
    self:RegisterEvent("PLAYER_REGEN_ENABLED")
    self:RegisterEvent("PLAYER_REGEN_DISABLED")
    self:RegisterEvent("PLAYER_TARGET_CHANGED")
    self:RegisterEvent("RAID_TARGET_UPDATE")
    self:RegisterEvent("UNIT_THREAT_LIST_UPDATE")
    self:RegisterEvent("UPDATE_MOUSEOVER_UNIT")
    --self:RegisterEvent("CURSOR_UPDATE")
    --GameTooltip:HookScript("OnTooltipSetUnit", function(self) setDoUpdate( 1 ) end)
    self:RegisterEvent("ZONE_CHANGED")
    self:RegisterEvent("ZONE_CHANGED_INDOORS")
    self:RegisterEvent("ZONE_CHANGED_NEW_AREA")
    self:RegisterEvent("PLAYER_ENTERING_WORLD")
    self:RegisterEvent("PLAYER_LEVEL_UP")
    self:RegisterEvent("UNIT_TARGET")
    self:RegisterEvent("RAID_ROSTER_UPDATE")
    self:RegisterEvent("PARTY_MEMBERS_CHANGED")
    self:RegisterEvent("GUILD_ROSTER_UPDATE")
    self:RegisterEvent("FRIENDLIST_UPDATE")
    self:RegisterEvent("PLAYER_LOGIN")
    self:RegisterEvent("BN_FRIEND_TOON_ONLINE")
    self:RegisterEvent("CHAT_MSG_CHANNEL")
    self:RegisterEvent("CHAT_MSG_WHISPER")
    
    -- Other files
    DocsUI_Nameplates:initializeAuras()
    DocsUI_Nameplates:initializeCast()
    DocsUI_Nameplates:initializeBossplateUpdater()
    
    DocsUI_Nameplates.initializeBossplateUpdater()
    bossplateUpdate = DocsUI_Nameplates.updateBossplate
    
    -- OnUpdate
    nameplatesUpdater:SetScript("OnUpdate", onUpdate)
    setDoUpdate( 1 )
end