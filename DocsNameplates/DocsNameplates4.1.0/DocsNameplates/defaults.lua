﻿--[[
	Copyright (c) 2009, dr_AllCOM3
    All rights reserved.

    You're allowed to use this addon, free of monetary charge,
    but you are not allowed to modify, alter, or redistribute
    this addon without express, written permission of the author.
]]

local L = LibStub("AceLocale-3.0"):GetLocale("DocsUI_Nameplates")
local LSM = LibStub("LibSharedMedia-3.0")
local db
local temp,temp2 = nil

--[[ Print ]]
local function p( text )
	ChatFrame3:AddMessage( tostring( text ) )
end

local _, playerClass = UnitClass( "player" )

DocsUI_Nameplates.version = 3
DocsUI_Nameplates.defaults = { 
    profile = {
        version = -1,
        logo = "Interface\\Addons\\DocsUI_Nameplates\\media\\logo",
        import = 0,
        debugMode = false,
        
        -- Options
        socialIcon = true,
        classIcon = false,
        roleIcon = true,
        showLevel = true,
        unknownIcon = true,
        guildName = true,
        lfgIcon = true,
        whisperIcon = true,
        tot = true,
        
        width = 60,
        height = 9,
        scale = 1,
        scaleBuffs = 1,
        
        combatLogFix = true,
        combatLogFixPrint = false,
        
        fontsize = 13,
        fontsizeName = 11,
        fontsizeGuild = 8,
        fontsizeBuffs = 11,
        
        outline = "NONE",
        font = "_DocsFont",
        fonti = "_DocsFonti",
        fontb = "_DocsFontb",
        
        texture = "_DocsStatusbar",
        bg = "Interface\\Addons\\DocsUI_Nameplates\\media\\bg",
        border = "Interface\\Addons\\DocsUI_Nameplates\\media\\borderGradient",
        borderIcon = "Interface\\Addons\\DocsUI_Nameplates\\media\\borderGradientIcon",
        borderSize = 4,
        
        threatEnabled = true,
        threatIcon = true,
        threatBar = true,
        threatColor = false,
        
        fontColor = {r=0.75,g=0.75,b=0.75,a=1},
        castColor = { r=255/255, g=201/255, b=14/255, a=1 },
        bgColor = {r=0,g=0,b=0,a=0.5},
        fgColor = {r=0.3,g=0.3,b=0.3,a=1},
        borderColor = {r=0,g=0,b=0,a=1},
        colorThreatSafe = {r=0,g=0.875,b=0,a=1},
        colorThreatUnsafe = {r=0.875,g=0.875,b=0,a=1},
        colorThreatAlert = {r=0.875,g=0,b=0,a=1},
        
        healthBarFriendPve = false,
        healthBarEnemyPve = true,
        healthBarFriendPveCombat = false,
        healthBarEnemyPveCombat = true,
        healthBarFriendPvp = false,
        healthBarEnemyPvp = true,
        
        castBarFriendPve = false,
        castBarEnemyPve = true,
        castBarFriendPveCombat = false,
        castBarEnemyPveCombat = true,
        castBarFriendPvp = false,
        castBarEnemyPvp = true,
        castBarNonTargets = true,
        
        buffsNumber = 6,
        
        allowOverlap = true,
        
        buffsFriendPve = false,
        buffsEnemyPve = true,
        buffsFriendPveCombat = false,
        buffsEnemyPveCombat = true,
        buffsFriendPvp = false,
        buffsEnemyPvp = true,
        
        visibilityPveFriendUnit = true,
        visibilityPveFriendPet = false,
        visibilityPveFriendGuardian = false,
        visibilityPveFriendTotem = false,
        visibilityPveEnemyUnit = true,
        visibilityPveEnemyPet = false,
        visibilityPveEnemyGuardian = true,
        visibilityPveEnemyTotem = true,
        
        visibilityPvecombatFriendUnit = false,
        visibilityPvecombatFriendPet = false,
        visibilityPvecombatFriendGuardian = false,
        visibilityPvecombatFriendTotem = false,
        visibilityPvecombatEnemyUnit = true,
        visibilityPvecombatEnemyPet = false,
        visibilityPvecombatEnemyGuardian = true,
        visibilityPvecombatEnemyTotem = true,
        
        visibilityPvpFriendUnit = true,
        visibilityPvpFriendPet = false,
        visibilityPvpFriendGuardian = false,
        visibilityPvpFriendTotem = false,
        visibilityPvpEnemyUnit = true,
        visibilityPvpEnemyPet = false,
        visibilityPvpEnemyGuardian = false,
        visibilityPvpEnemyTotem = true,
        
        buffFilter = {
            buffs = {
                my = {},
                any = {},
                never = {},
                allMy = false,
                allAny = false,
            },
            debuffs = {
                my = {},
                any = {},
                never = {},
                allMy = false,
                allAny = false,
            },
            filterLow = true,
        },
        
        -- unsortiertes zeug
        -- General Options
        
        
        heightCast = 5,
        heightThreat = 3,
        sizeBuffs = 14,
        sizeTotem = 16,
        
        spacing = 2,
        spacingFixed = 2,
        
        fontoffset = 0,
        
        textureComboPoint1 = "Interface\\Addons\\DocsUI_Nameplates\\media\\combo1",
        textureComboPoint2 = "Interface\\Addons\\DocsUI_Nameplates\\media\\combo2",
        textureComboPoint3 = "Interface\\Addons\\DocsUI_Nameplates\\media\\combo3",
        textureComboPoint4 = "Interface\\Addons\\DocsUI_Nameplates\\media\\combo4",
        textureComboPoint5 = "Interface\\Addons\\DocsUI_Nameplates\\media\\combo5",
        
        alpha = 0.6,
        
        showNameplates = true,
        UPDATE_FREQUENCY = 0.1,
        showNameplateName = true,
        showNameplatePvpName = true,
        
        showHpNumber = false,
        showHpPercent = true,
        swapBuffPosition = false,
        
        nameplateHider = {},
        
        -- Bossplates
        professorPutricide = true,
        
        healingSpells = {
            strlower( GetSpellInfo( 31739 ) ),--Heal
            strlower( GetSpellInfo( 2147 ) ),--Mending
            
            strlower( GetSpellInfo( 50464 ) ),--Nourish
            --strlower( GetSpellInfo( 48378 ) ),--Healing Touch
            strlower( GetSpellInfo( 48443 ) ),--Regrowth
            strlower( GetSpellInfo( 48447 ) ),--Tranquility
            --strlower( GetSpellInfo( 55459 ) ),--Chain Heal
            --strlower( GetSpellInfo( 49273 ) ),--Healing Wave
            --strlower( GetSpellInfo( 49276 ) ),--Lesser Healing Wave
            strlower( GetSpellInfo( 64843 ) ),--Divine Hymn
            strlower( GetSpellInfo( 53007 ) ),--Penance
            --strlower( GetSpellInfo( 48071 ) ),--Flash Heal
            --strlower( GetSpellInfo( 48120 ) ),--Binding Heal
            --strlower( GetSpellInfo( 48063 ) ),--Greater Heal
            strlower( GetSpellInfo( 48782 ) ),--Holy Light
            strlower( GetSpellInfo( 48785 ) ),--Flash of Light
        },
    },
    
    realm = {
        playerList = {},
    },
    
    global = {
        healerList = {},
        casterList = {},
        agressiveList = {},
        npcList = {},
        spellList = {},
        nameplateHider = {},
        nameReplacer = {},
        LFGList = {},
    },
    
}

local db = DocsUI_Nameplates.defaults.profile

local agressiveList = {
    ["Initiate's Training Dummy"] = true,
    ["Disciple's Training Dummy"] = true,
    ["Veteran's Training Dummy"] = true,
    ["Ebon Knight's Training Dummy"] = true,
    ["Highlord's Nemesis Trainer"] = true,
}

local nameplateHider = {
    "Dark Rune Commoner", --small non-elites in Thorim arena
    "Dunkler Runenb\195\188rger", --small non-elites in Thorim arena
    "Onyxian Whelp",
    "Welpe von Onyxia",
    "Forest Swarmer",
    "Fanged Pit Viper",
    "Bissige Grubenotter",
}

local nameReplacer = {
    ["Val'kyr Shadowguard"] = "Valk",
    ["Heroic Training Dummy"] = "Boss",
}

local LFGList = {
    "lfm",
}

local myBuffs = {
    ["WARRIOR"] = {
        
    },
    ["MAGE"] = {
        
    },
    ["ROGUE"] = {
        
    },
    ["DRUID"] = {
        
    },
    ["HUNTER"] = {
        
    },
    ["SHAMAN"] = {
        
    },
    ["PRIEST"] = {
        
    },
    ["WARLOCK"] = {
        
    },
    ["PALADIN"] = {
        
    },
    ["DEATHKNIGHT"] = {
        
    },
}

local anyBuffs = {
    {642,3},--Divine Shield
    {10278,3},--Hand of Protection
    {45438,3},--Ice Block
    {31224,3},--Cloak
    {26669,3},--Evasion
    {781,3},--Disengage
    {22812,3},--Barkskin
    {47585,3},--Dispersion
    {46924,3},--Bladestorm
    
    {69558,3},--Unstable Ooze
    {70768,3},--Shroud of the Occult
    {70842,3},--Mana Barrier
    {71730,3},--Lay Waste
    
    {61573,1},--Banner of the Alliance
}

local myDebuffs = {
    ["WARRIOR"] = {
        {47437,3},--Demoralizing Shout
        {1715,3},--Hamstring
    },
    ["MAGE"] = {
        {31589,3},--Slow
        {55360,3},--Living Bomb
    },
    ["ROGUE"] = {
        
    },
    ["DRUID"] = {
        {48468,3},--Insect Swarm
        {48463,3},--Moonfire
        {770,3},--Faerie Fire
    },
    ["HUNTER"] = {
        {49001,3},--Serpent Sting
    },
    ["SHAMAN"] = {
        
    },
    ["PRIEST"] = {
        {48125,3},--Shadow Word: Pain
        {48160,3},--Vampiric Touch
        {48300,3},--Devouring Plague
        {53023,3},--Mind Sear
    },
    ["WARLOCK"] = {
        {47813,3},--Corruption
        {47864,3},--Curse of Agony
        {47867,3},--Curse of Doom
        {11719,3},--Curse of Tongues
        {50511,3},--Curse of Weakness
        {47865,3},--Curse of the Elements
        {59164,3},--Haunt
        {47836,3},--Seed of Corruption
        {47843,3},--Unstable Affliction
    },
    ["PALADIN"] = {
        {53407,3},--Judgement of Justice
        {20271,3},--Judgement of Light
        {53408,3},--Judgement of Wisdom
    },
    ["DEATHKNIGHT"] = {
        {55095,3},--Frost Fever
        {55078,3},--Blood Plague
    },
}

local anyDebuffs = {
    {49012,3},--Wyvern Sting
    {14311,3},--Freezing Trap
    {12826,3},--Polymorph
    {51724,3},--Sap
    {51514,3},--Hex
    {18647,3},--Banish
    {2094,3},--Blind
    
    {73912,3},--Necrotic Plague
}

for i=1,#nameplateHider do
    DocsUI_Nameplates.defaults.global.nameplateHider[nameplateHider[i]] = true
end

for i,v in pairs( nameReplacer ) do
    DocsUI_Nameplates.defaults.global.nameReplacer[i] = v
end

for i=1,#LFGList do
    tinsert( DocsUI_Nameplates.defaults.global.LFGList, LFGList[i] )
end

for i=1,#agressiveList do
    local name = strlower( agressiveList[i] )
    
    tinsert( DocsUI_Nameplates.defaults.global.agressiveList, name )
end

local list = anyBuffs
for i=1,#list do
    local name = GetSpellInfo( list[i][1] )
    
    if name then db.buffFilter.buffs.any[name] = list[i][2] end
end

local list = anyDebuffs
for i=1,#list do
    local name = GetSpellInfo( list[i][1] )
    
    if name then db.buffFilter.debuffs.any[name] = list[i][2] end
end

local list = myBuffs[playerClass]
for i=1,#list do
    local name = GetSpellInfo( list[i][1] )
    
    if name then db.buffFilter.buffs.my[name] = list[i][2] end
end

local list = myDebuffs[playerClass]
for i=1,#list do
    local name = GetSpellInfo( list[i][1] )
    
    if name then db.buffFilter.debuffs.my[name] = list[i][2] end
end