
local	KarmaObj = KarmaAvEnK;
local	KCfg = KarmaObj.Cfg;
local	KarmaConfigLocal = nil;

KCfg.ColorSpaces =
	{
		Default1 = {
			Min = { r = 1.0, g = 0.0, b = 0.0 },
			Avg = { r = 0.5, g = 0.5, b = 1.0 },
			Max = { r = 0.0, g = 1.0, b = 0.0 },
		},

		Default2 = {
			Min = { r = 0.7, g = 0.7, b = 0.7 },
			Avg = { r = 0.5, g = 0.5, b = 1.0 },
			Max = { r = 0.3, g = 1.0, b = 0.3 },
		},
	};

-- CONFIG FIELDS (all local)
local	KARMA_CONFIG = {
			-- Options: "Sort/Color"
			SORTFUNCTION = "SORTFUNCTION",
			SORTFUNCTION_TYPE_KARMA = "KARMASORT",
			SORTFUNCTION_TYPE_NAME = "NAMESORT",
			SORTFUNCTION_TYPE_XP	= "XPSORT",
			SORTFUNCTION_TYPE_PLAYED = "PLAYEDSORT",
			SORTFUNCTION_TYPE_CLASS = "CLASSSORT",
			SORTFUNCTION_TYPE_JOINED = "JOINEDSORT",
			SORTFUNCTION_TYPE_JOINEDTHIS = "SRT_JOINEDTHIS",
			SORTFUNCTION_TYPE_TALENT = "TALENTSORT",
			SORTFUNCTION_TYPE_XPALL	= "XPALLSORT",
			SORTFUNCTION_TYPE_PLAYEDALL = "PLAYEDALLSORT",
			SORTFUNCTION_TYPE_GUILD_TOP = "BYGUILD_TOP",
			SORTFUNCTION_TYPE_GUILD_BOTTOM = "BYGUILD_BTM",

			COLORFUNCTION = "COLORFUNCTION",
			COLORFUNCTION_TYPE_XP	= "XPCOLOR",
			COLORFUNCTION_TYPE_PLAYED = "PLAYEDCOLOR",
			COLORFUNCTION_TYPE_KARMA = "KARMACOLOR",
			COLORFUNCTION_TYPE_CLASS = "CLASSCOLOR",
			COLORFUNCTION_TYPE_XPALL = "XPALLCOLOR",
			COLORFUNCTION_TYPE_PLAYEDALL = "PLAYEDALLCOLOR",
			COLORFUNCTION_TYPE_XPLVL = "XPLVLCOLOR";
			COLORFUNCTION_TYPE_XPLVLALL = "XPLVLALLCOLOR";

			COLORSPACE_ENABLE = "COLORSPACE_ENABLE",
			COLORSPACE_KARMA = "COLORSPACE_KARMA",
			COLORSPACE_TIME = "COLORSPACE_TIME",
			COLORSPACE_XP = "COLORSPACE_XP",

			-- Options: "Tooltip"
			TOOLTIP_SHIFTREQ = "TT_SHREQ",
			TOOLTIP_KARMA = "TT_KARMA",
			TOOLTIP_PLAYEDTOTAL = "TT_PLAYEDTOTAL",
			TOOLTIP_PLAYEDTHIS = "TT_PLAYEDTHIS",
			TOOLTIP_NOTES = "TT_NOTES",
			TOOLTIP_SKILL = "TT_SKILL",
			TOOLTIP_TALENTS = "TT_TALENTS",
			TOOLTIP_TERROR = "TT_TERROR",
			TOOLTIP_ALTS = "TT_ALTS",
			TOOLTIP_HELP = "TT_HELP",
			TOOLTIP_LFMADDKARMA = "TT_LFMADDKARMA",

			-- Options: "auto.Ign/Warn"
			AUTOIGNORE_THRESHOLD = "AUTOIGNORE_THRESHOLD",
			AUTOIGNORE = "AUTOIGNORE",
			AUTOIGNORE_INVITES = "AUTOIGNORE_INVITES",
			JOINWARN_THRESHOLD = "JOINWARN_THRESHOLD",
			JOINWARN_ENABLED = "JOINWARN_ENABLED",

			-- Options: "Chat windows"
			CHAT_DEFAULT = "CHAT_DEFAULT",
			CHAT_SECONDARY = "CHAT_SECONDARY",
			CHAT_DEBUG = "CHAT_DEBUG",

			MARKUP_ENABLED = "MARKUP",
			MARKUP_VERSION = "MARKUP_VERSION",
			MARKUP_COLOUR_NAME = "COLOUR_NAME",				-- missing in UI
			MARKUP_COLOUR_KARMA = "COLOUR_KARMA",				-- missing in UI
			MARKUP_SHOW_KARMA = "SHOW_KARMA",				-- missing in UI
			MARKUP_WHISPERS = "MARKUP_WHISPERS",
			MARKUP_CHANNELS = "MARKUP_CHANNELS",
			MARKUP_GUILD = "MARKUP_GUILD",
			MARKUP_RAID = "MARKUP_RAID",
			MARKUP_BG = "MARKUP_BG",
			MARKUP_YELLSAYEMOTE = "MARKUP_YSE",

			-- Options: "virtual Karma"
			TIME_KARMA_DEFAULT = "K_TIME_DEFAULT",
			TIME_KARMA_MINVAL = "K_TIME_MINVAL",
			TIME_KARMA_FACTOR = "K_TIME_FACTOR",
			TIME_KARMA_SKIPBGTIME = "K_TIME_NOTBG",

			-- Options: "Other"
			TARGET_COLORED = "TARGET_COLORED",
			QUESTWARNCOLLAPSED = "QUESTWARN",
			MAINWND_INITIALTAB = "MAINWND_INITTAB",
			TALENTS_AUTOFETCH = "TALENTS_AUTOFETCH",
			MINIMAP_HIDE = "MINIMAP_HIDE",
			QUESTSIGNOREDAILIES = "Q_IGN_DAILY",
			UPDATEWHILEAFK = "UPD_ON_AFK",
			DB_SPARSE = "SPARSEDB",

			-- Options: "DB cleaning"
			CLEAN_AUTO = "AUTOCLEAN",
			CLEAN_AUTOPVP = "AUTOCLEANPVP",
			CLEAN_KEEPIFNOTE = "CLEAN_KEEPIFNOTE",
			CLEAN_REMOVEPVPJOINS = "CLEAN_REM_PVP",
			CLEAN_REMOVEXSERVER = "CLEAN_REMOVEXSERVER",
			CLEAN_KEEPIFKARMA = "CLEAN_KEEPIFKARMA",
			CLEAN_KEEPIFREGIONCOUNT = "CLEAN_KEEPIFREGIONCOUNT",
			CLEAN_KEEPIFZONECOUNT = "CLEAN_KEEPIFZONECOUNT",
			CLEAN_KEEPIFQUESTCOUNT = "CLEAN_KEEPIFQUESTCOUNT",
			CLEAN_IGNOREPVPZONES = "CLEAN_IGNOREPVPZONES",

			-- Options: "Sharing"
			SHARE_ONREQ_KARMA = "SHARE_ONREQ_KARMA",
			SHARE_ONREQ_PUBLICNOTE = "SHARE_ONREQ_PUBNOTE",
			SHARE_CHANNEL_NAME = "SHARE_CHAN_NAME",
			SHARE_CHANNEL_AUTO = "SHARE_CHAN_AUTO",

			-- Options: "Tracking" (UI part TODO)
			RAID_TRACKALL = "RAID_TRACKALL",
			RAID_NOGROUP  = "RAID_NOGROUP",
			TRACK_DISABLEACHIEVEMENT = "TRACK_NOACHIEVEMENT",
			TRACK_DISABLEACHIEV_TERROR = "TRACK_NOACH_TERROR",
			TRACK_DISABLEQUEST = "TRACK_NOQUEST",
			TRACK_DISABLEREGION = "TRACK_NOREGION",
			TRACK_DISABLEZONE = "TRACK_NOZONE",
			TRACK_DISABLEPVPAREAS = "TRACK_NOPVPAREA",
			TRACK_DISABLEWARNING_VERSION = "TRACK_WARNVERSION",

			MENU_DEACTIVATE = "MENU_OFF",
			MENU_WARN = "MENU_WARN",

			-- not in Option window:
			MINIMAPPOS = "MINIMAP_ANGLE",
			SKILL_MODEL = "SKILL_MODEL",

			MAINWND_LIST_ = "MWND_L_",
			MAINWND_LIST_R = "MWND_L_R",
			MAINWND_LIST_Z = "MWND_L_Z",
			MAINWND_LIST_Q = "MWND_L_Q",
			MAINWND_LIST_A = "MWND_L_A",

			-- internal
			DEBUG_ENABLED = "DEBUG",

			-- == last entry no ",", add to previous == --
			BETA = "BETA"
		};

function	KCfg.Init()
	if (KarmaConfig == nil) then
		KarmaConfig = {};
	end
	KarmaConfigLocal = KarmaConfig;

	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.AUTOIGNORE_THRESHOLD, 10);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.AUTOIGNORE, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.AUTOIGNORE_INVITES, 1);

	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.JOINWARN_ENABLED, 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.JOINWARN_THRESHOLD, 35);

	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.SORTFUNCTION, KARMA_CONFIG.SORTFUNCTION_TYPE_KARMA);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.COLORFUNCTION, KARMA_CONFIG.COLORFUNCTION_TYPE_KARMA);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.COLORSPACE_ENABLE, false);
	if (not KCfg.Get("COLORSPACE_ENABLE")) then
		KarmaConfig[KARMA_CONFIG.COLORSPACE_KARMA] = nil;
		KarmaConfig[KARMA_CONFIG.COLORSPACE_TIME] = nil;
		KarmaConfig[KARMA_CONFIG.COLORSPACE_XP] = nil;
	end
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.COLORSPACE_KARMA, KCfg.ColorSpaces.Default1, nil, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.COLORSPACE_TIME, KCfg.ColorSpaces.Default2, nil, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.COLORSPACE_XP, KCfg.ColorSpaces.Default2, nil, true);

	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_ENABLED, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_VERSION, 2);

	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_WHISPERS, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_CHANNELS, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_GUILD, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_RAID, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_BG, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_YELLSAYEMOTE, true);

	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_COLOUR_NAME, true);	-- TODO: !UI
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_COLOUR_KARMA, true);	-- TODO: !UI
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MARKUP_SHOW_KARMA, true);	-- TODO: !UI

	--
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_AUTO, false);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_KEEPIFNOTE, 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_REMOVEPVPJOINS, 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_REMOVEXSERVER, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_KEEPIFKARMA, 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_KEEPIFQUESTCOUNT, 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_KEEPIFREGIONCOUNT, 2);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_KEEPIFZONECOUNT, 4);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.CLEAN_IGNOREPVPZONES, 1);

	-- 0: never, 1: always, 2: via GUILD, 3: with trusted people only
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.SHARE_ONREQ_KARMA, 3);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.SHARE_ONREQ_PUBLICNOTE, 2);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.SHARE_CHANNEL_NAME, "CommGlobal");
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.SHARE_CHANNEL_AUTO, false);

	-- Tracking:
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.RAID_TRACKALL, 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.RAID_NOGROUP, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TRACK_DISABLEACHIEVEMENT, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TRACK_DISABLEACHIEV_TERROR, 1);		-- default off since 30300.09
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TRACK_DISABLEQUEST, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TRACK_DISABLEREGION, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TRACK_DISABLEZONE, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TRACK_DISABLEPVPAREAS, 1);

	--
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TARGET_COLORED, 1);

	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_SHIFTREQ, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_KARMA, true);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_SKILL, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_TALENTS, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_TERROR, 0);			-- default off since 30300.09
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_ALTS, 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_NOTES, false);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_HELP, 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TOOLTIP_LFMADDKARMA, 1);

	--
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MINIMAP_HIDE, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.QUESTSIGNOREDAILIES, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.UPDATEWHILEAFK, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.DB_SPARSE, 0);

	--
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TIME_KARMA_DEFAULT, 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TIME_KARMA_MINVAL, 50);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TIME_KARMA_FACTOR, 0.4);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.TIME_KARMA_SKIPBGTIME, 1);

	--
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MENU_DEACTIVATE, 1);

	--
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MAINWND_LIST_ .. "R", 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MAINWND_LIST_ .. "Z", 0);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MAINWND_LIST_ .. "Q", 1);
	Karma_FieldInitialize(KarmaConfig, KARMA_CONFIG.MAINWND_LIST_ .. "A", 1);
end

function	KCfg.Get(sCfgName)
	if (KarmaConfigLocal == nil) then
		return nil;
	end

	if (sCfgName == nil) then
		KarmaChatDebug("Oops? Requested to get <nil> config. Doh! >> " .. debugstack());
		return nil;
	end

	local	sCfgKey = KARMA_CONFIG[sCfgName];
	if (sCfgKey == nil) then
		KarmaChatDebug("Oops? Requested to get unknown (misspelled?) config: <" .. tostring(sCfgName) .. "> Doh! >> " .. debugstack());
		return nil;
	end

	return KarmaConfigLocal[sCfgKey];
end

function	KCfg.Set(sCfgName, value)
	if (KarmaConfigLocal == nil) then
		return;
	end

	if (sCfgName == nil) then
		KarmaChatDebug("Oops? Requested to set <nil> config. Doh! >> " .. debugstack());
		return nil;
	end

	local	sCfgKey = KARMA_CONFIG[sCfgName];
	if (sCfgKey == nil) then
		KarmaChatDebug("Oops? Requested to set unknown (misspelled?) config: <" .. tostring(sCfgName) .. "> Doh! >> " .. debugstack());
		return nil;
	end

	KarmaChatDebug("Setting " .. sCfgKey .. " to value " .. tostring(value));
	KarmaConfigLocal[sCfgKey] = value;
end

function	KCfg.GetPerChar(sCfgName)
	if (KarmaConfigLocal == nil) then
		return nil;
	end

	if (sCfgName == nil) then
		KarmaChatDebug("Oops? Requested to get <nil> config. Doh! >> " .. debugstack());
		return nil;
	end

	local	sCfgKey = KARMA_CONFIG[sCfgName];
	if (sCfgKey == nil) then
		KarmaChatDebug("Oops? Requested to get unknown (misspelled?) config: <" .. tostring(sCfgName) .. "> Doh! >> " .. debugstack());
		return nil;
	end

	if (KARMA_LOADED == 1) then
		if Karma_EverythingLoaded() then
			local	CharConfig = KarmaObj.DB.CharacterConfigObjectGet();
			local	CharConfigValue = CharConfig[sCfgKey];
			if (CharConfigValue ~= nil) then
				KarmaChatDebug("Using per-char value of " .. sCfgKey);
				return CharConfigValue;
			end

			return KarmaConfigLocal[sCfgKey];
		end
	else
		return nil;
	end
end

function	KCfg.SetPerChar(sCfgName, value)
	if (KarmaConfigLocal == nil) then
		return;
	end

	if (sCfgName == nil) then
		KarmaChatDebug("Oops? Requested to set <nil> config. Doh! >> " .. debugstack());
		return nil;
	end

	local	sCfgKey = KARMA_CONFIG[sCfgName];
	if (sCfgKey == nil) then
		KarmaChatDebug("Oops? Requested to set unknown (misspelled?) config: <" .. tostring(sCfgName) .. "> Doh! >> " .. debugstack());
		return nil;
	end

	KarmaChatDebug("Setting " .. sCfgKey .. " to value " .. tostring(value));
	KarmaConfigLocal[sCfgKey] = value;

	if (KARMA_LOADED == 1) then
		if Karma_EverythingLoaded() then
			local	CharConfig = KarmaObj.DB.CharacterConfigObjectGet();
			KarmaChatDebug("Storing per-char value for " .. sCfgKey);
			CharConfig[sCfgKey] = value;
		end
	end
end

-- values for "Sort/Color"
local	KARMA_VALUES = {
			SORTFUNCTION_TYPE_KARMA = "KARMASORT",
			SORTFUNCTION_TYPE_NAME = "NAMESORT",
			SORTFUNCTION_TYPE_XP	= "XPSORT",
			SORTFUNCTION_TYPE_PLAYED = "PLAYEDSORT",
			SORTFUNCTION_TYPE_CLASS = "CLASSSORT",
			SORTFUNCTION_TYPE_JOINED = "JOINEDSORT",
			SORTFUNCTION_TYPE_JOINEDTHIS = "SRT_JOINEDTHIS",
			SORTFUNCTION_TYPE_TALENT = "TALENTSORT",
			SORTFUNCTION_TYPE_XPALL	= "XPALLSORT",
			SORTFUNCTION_TYPE_PLAYEDALL = "PLAYEDALLSORT",
			SORTFUNCTION_TYPE_GUILD_TOP = "BYGUILD_TOP",
			SORTFUNCTION_TYPE_GUILD_BOTTOM = "BYGUILD_BTM",

			COLORFUNCTION_TYPE_XP	= "XPCOLOR",
			COLORFUNCTION_TYPE_PLAYED = "PLAYEDCOLOR",
			COLORFUNCTION_TYPE_KARMA = "KARMACOLOR",
			COLORFUNCTION_TYPE_CLASS = "CLASSCOLOR",
			COLORFUNCTION_TYPE_XPALL = "XPALLCOLOR",
			COLORFUNCTION_TYPE_PLAYEDALL = "PLAYEDALLCOLOR",
			COLORFUNCTION_TYPE_XPLVL = "XPLVLCOLOR";
			COLORFUNCTION_TYPE_XPLVLALL = "XPLVLALLCOLOR";
		};

function	KCfg.EqualsIndirect(sValueA, sValueBName)
	local	sValueB = KARMA_VALUES[sValueBName];
	if (sValueB) then
		return sValueA == sValueB;
	end
end

function	KCfg.SetIndirect(sCfgName, sValueName)
	if (type(sValueName) ~= "string") then
		KarmaChatDebug("Oops? Requested to set <nil> indirect value. Doh! >> " .. debugstack());
		return
	end

	local	sValue = KARMA_VALUES[sValueName];
	if (sValue) then
		KCfg.Set(sCfgName, sValue);
	else
		KarmaChatDebug("Oops? Requested to set unknown indirect value. Doh! >> " .. debugstack());
	end
end

