## Interface: 30300
## Title: Karma
## Version: 30300.09
## Notes: Rating system for players of your faction
## Notes-deDE: Bewertungssystem für Spieler/-innen der eigenen Fraktion
## SavedVariables: KarmaConfig, KarmaData
## Author: Kärbär, EU-Proudmoore (original author Aveasau, revived by Endareth)
## X-Date: 2010-06-28
## X-Email: n/a
## X-Website: http://www.wowinterface.com/downloads/info8712.html
## X-Category: MYADDONS_CATEGORY_OTHERS
## X-Help: KARMA_MYADDONS_HELP
## DefaultState: enabled
Karma.xml

