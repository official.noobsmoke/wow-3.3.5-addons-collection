
-- localization.final.lua: complete localization

if (KARMAAVENK_LANG_LOC.PREF == "EN") then
	KARMAAVENK_LANG = KARMAAVENK_LANG_LOC[KARMAAVENK_LANG_LOC.PREF];
else
	-- define language to use
	KARMAAVENK_LANG = KARMAAVENK_LANG_LOC[KARMAAVENK_LANG_LOC.PREF];
	local	oCurr = KARMAAVENK_LANG;
	local	oEng = KARMAAVENK_LANG_LOC.EN;

	-- add missing strings
	KARMAAVENK_LANG_DUPLICATES = {};
	local	oDupes = KARMAAVENK_LANG_DUPLICATES;

	local	k, v;
	for k, v in pairs(oEng) do
		if (oCurr[k] == nil) then
			oCurr[k] = v;
		elseif (oCurr[k] == v) then
			oDupes[k] = v;
		end
	end
end

