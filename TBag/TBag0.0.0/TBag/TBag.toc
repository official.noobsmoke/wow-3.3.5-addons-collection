﻿## Interface: 30300
## Title: TBag
## Notes: The last bag addon you'll ever need.  Autosorting, viewable anywhere, easy to use yet amazingly configurable.
## Notes-deDE: Das einzige Taschenaddon was du jemals brauchen wirst. Automatisches Sortieren, überall einsehbar, Benutzerfreundlich, dabei aber unglaublich gut Konfigurierbar.
## Notes-ruRU: Модификация сумок. Возможности - авто-сортировка, просмотр сумок, банка в лубом месте, легок в использовании, гибкие настройки. 
## X-Category: Inventory
## Author: Talos, Modified by Shefki
## X-Email: shefki@shefki.org
## X-Website: http://wow.curse.com/downloads/details/8064/
## X-Feedback: http://wow.curseforge.com/projects/tbag-shefki/tickets/
## RequiredDeps:
## OptionalDeps: Auctioneer
## SavedVariables: TBagCfg, TBagInfo, TBnkItm, TInvItm, TContItm, TBodyItm, TMailItm, TTknItm
## X-Localizations: enUS, deDE, ruRU
## X-CompatibleLocales: enUS, deDE, ruRU
##
## X-Curse-Packaged-Version: 20100905-372
## X-Curse-Project-Name: TBag-Shefki
## X-Curse-Project-ID: tbag-shefki
## X-Curse-Repository-ID: wow/tbag-shefki/mainline
stub.lua
localization.enUS.lua
localization.deDE.lua
localization.ruRU.lua
defaults.lua
Tokens.xml
TBag.xml
Buttons.xml
MainFrame.xml
TInv.xml
TInvOpts.xml
TBnk.xml
TBnkOpts.xml
