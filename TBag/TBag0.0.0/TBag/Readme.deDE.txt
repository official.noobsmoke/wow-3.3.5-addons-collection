Dies ist das lang erwartete Update für TBag das eine Reihe von Fehlern behebt.

TBag ist ein WoW Addon das ein alternatives Interface für Taschen und Bank bereitstellt
Es ist aus einer Modifikation von Engbags entstanden welches praktisch die selben
Funktionen hat, dabei aber neue Funktionen bereitstellt.

Talos, der eigentliche Autor von TBag sagte dies als er das Addon herausgab:

"Zusätzlich zu der Automatischen Sortierung, welche du sicher schon kennen und lieben gelernt hast
habe ich viele neue Funktionen hinzugefügt wie zum Beispiel eine Suche nach Items
(im Briefkasten, etc.) Du kannst auch das Standardinterface für Taschen benutzen das Freie
Taschenplätze oben anordnet.
Es gibt auch eine Menge Funktionen die, die Nutzung vereinfachen, wie zum Beispiel Hervorheben neuer
Items, Farbliche Hervorhebungen und Kaufbare Banktaschenplätze ohne das Addon zu deaktivieren.
Ein Visueller Bearbeitungsmodus gibt dir die Möglichkeit, die Kategorien so anzuordnen wie du es magst.
Zusätzlich kannst du mit der Erweiterten Konfiguration jeden Erdenklichen Aspekt von TBag Konfigurieren."

Dies ist Eine Modifikation seines 070123 Releases für WoW 2.0.3.
Die Änderungen wurden von Shefki durchgeführt.
Schaue dir die Changelog Datei an falls du genauer Wissen willst welche Änderungen ich gemacht habe.
Die ToDo.txt Datei besteht aus ToDos, die im originalen TBag Release zu finden sind.
Manche sind, meiner Meinung nach, nicht wirklich Relevant, bei manchen bin ich mir unsicher was er Meinte,
andere Wiederum sind bereits Behoben, dazu lohnt es sich einen Blick auf meine ToDo Liste weiter unten zu werfen.

Getting Started

TBag kann nur das "sehen" was du bereits gesehen hast, daher ist für jeden Charakter folgendes zu tun:

1) Öffne deine Taschen
2) Gehe zur Bank
3) Schaue nach Post
4) Sehe deinen Körper an
5) Offne alle deine Handelsfenster

Dies Erlaubt dir den Inhalt deiner Taschen und der Bank deiner Charaktere durch Klicken auf das Namensmenü
oben Links zu jeder Zeit anzusehen (Sortiert nach ihren Berufen)
Dies gibt dir auch die Möglichkeit eine vollständige Itemsuche über die Such-Textbox unten links durchzuführen

Todo/Bekannte Fehler

* Konfigurationsverbesserung. Implementierung der Konfiguration sodass sie Teil des
  Blizzard Konfigurationsframe ist

* Unterstützung der Gildenbank. Ich werde das fertig machen, aber erst wenn ich es "richtig" machen kann

* Suche. Die Hervorhebung ist fertig, die Textergebnisse sollten noch aus dem Chatfenster raus.

* Zeige Itemanzahl in den Tooltips

* Skins.  Ich dachte da an etwas was der Blizzard UI ähnelt.

* Ansichtsmodi für Körper und Postinformationen die bereits zwischengespeichert und von
  der Suche verfügbar ist

* Unsauberer / Toter Code. Das Addon ist voll von unsauberem und Totem Code.
  Aufräumen und durchsehen jeder einzelnen Zeile wäre hier notwendig.
  Es ist auch Viel Code Duplikation vorhanden.
  Das ist zwar etwas was ich gerne tun würde, aber keine hohe Priorität für mich hat.
  Meine Prioritäten liegen bei der Behebung von nervigen Fehlen oder Veränderung von Funktionen
  um sie nützlicher zu machen.

* Lokalisierung.  Das Aufräumen der Übersetzung ist fertig, ich brauche nur Übersetzer.
  Falls du interessiert bist eine Übersetzung anzufertigen dann Kontaktiere mich auf Curse.

Kontakt

Ich werde sofern es mir möglich ist das die Addon Foren von Zeit zu Zeit durchsehen
Öfter bei Größeren Patches, seltener dazwischen.
Ich werde aber mit Sicherheit nicht jeden Tag dort hineinschauen
Curse Gaming und WowInterface werden aktuell gehalten mit dem letzten
Update und ich lese auch die Foren dort.  Keine Garantie irgendwo anders.

Der Kontakt ist generell in Englisch zu halten, sollte dies nicht möglich sein könnt ihr auch Dessa anschreiben.

Bug-Reports und Feature-Requests sollten in das Ticket-System von Curse eingetragen werden:
http://wow.curseforge.com/projects/tbag-shefki/tickets/
