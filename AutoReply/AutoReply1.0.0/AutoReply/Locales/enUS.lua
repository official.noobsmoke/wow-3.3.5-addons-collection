----------------------------------------
-- Addon name	: Animor's AutoReply
-- Author      	: Animor
-- Create Date 	: 12/1/2011
-- File			: enUS.lua
----------------------------------------

----------------------------------------
-- Libs
----------------------------------------
local L = LibStub("AceLocale-3.0"):NewLocale("AutoReply", "enUS", true)

----------------------------------------
-- Strings
----------------------------------------
-- Header
L["Auto reply to whispers in combat related situations."]		= true

-- General
L["General"] 													= true
L["Global"]														= true
L["Enable AutoReply"] 											= true
L["Global enable for AutoReply addon"]							= true
L["Auto reply only once"]										= true
L["Auto reply same whisperer only once per busy session"]		= true
L["Also to party/raid members"]									= true
L["Auto reply also to party/raid members"]						= true
L["Notify missed whisperers when available"]					= true
L["Notify missed whisperers when becoming available (except for AFK)."]= true
L["Hide minimap icon"]											= true
L["Hide auto reply from chat"]									= true
L["Hide auto reply from the player's chat frame"]				= true

-- AFK
L["AFK"]														= true
L["Save missed whispers also when AFK"]							= true
L["Save missed whispers also when away/AFK"]					= true
L["Notify missed whisperers when no longer AFK"]				= true
L[" is no longer AFK."]											= true

-- Death
L["Death"]														= true
L["Enable when dead"]											= true
L["Enable auto reply when running to your corpse"]				= true
L["Reply message during death:"]								= true
L["%PN is dead now, and the dead can't talk."]					= true

-- History
L["History"]													= true
L["AutoReply History"]											= true
L["Max number of history entries"]								= true
L["Set the maximum number of whisper history entries to save"]	= true
L["Show History"]												= true
L["Show missed whispers history\nAlso type \"/aar his\""]		= true
L["Clear History"]												= true
L["Clear missed whispers history"]								= true
L["Missed whispers history was cleared."]						= true
L["Are you sure you want to clear the history?"]				= true

-- Outdoor
L["Outdoor"]													= true
L["Outdoor combat"]												= true
L["Enable during outdoor combat"]								= true
L["Enable auto reply during outdoor combat"]					= true
L["%PN (%PHP) is busy in combat in %LOC. Target: %TN (%THP)."]	= true
L["Reply message during outdoor combat:"]						= true
L["General options"]											= true

-- Raids
L["Raids"]														= true
L["Enable in raids"]											= true
L["Enable auto reply while in raids"]							= true
L["Message"]													= true
L["Reply message during raids:"]								= true
L["%PN (%PHP) is busy raiding %LOC. Progress: %PRG."]			= true
L["Enable only in combat"]										= true
L["Enable auto reply only in raid combat situations"]			= true
L["Also to raid members"]										= true
L["Auto reply also to raid members"]							= true
L["Boss Fights"]												= true
L["Auto reply on raid boss fights"]								= true
L["Uncheck this if you run Boss mods and want to avoid double auto replies"]= true
L["Reply message during raid boss fights:"]						= true
L["%PN (%PHP) is busy raiding %LOC, now fighting boss %BN (%BHP)."]= true
L["Unknown"]													= true
L["Dragon Soul"]												= true -- raid name
L["Warmaster Blackhorn"]										= true
L["Spine of Deathwing"]											= true
L["Madness of Deathwing"]										= true

-- 5men Dungeons
L["Dungeons"]													= true
L["Enable in dungeons"]											= true
L["Enable auto reply while in 5men dungeons"]					= true
L["%PN (%PHP) is busy in %LOC dungeon. Progress: %PRG."]		= true
L["Reply message when in dungeons:"]							= true
L["Enable only in combat"]										= true
L["Enable auto reply only in 5men dungeon combat situations"]	= true
L["Also to party members"]										= true
L["Auto reply also to party members"]							= true							
L["Auto reply on dungeon boss fights"]							= true
L["Uncheck this if you run Boss mods and want to avoid double auto replies"]= true
L["Reply message during dungeon boss fights:"]					= true
L["%PN (%PHP) is busy in %LOC dungeon, now fighting boss %BN (%BHP)."]= true

-- Battleground
L["Battlegrounds"]												= true
L["Enable in battlegrounds"]									= true
L["Enable auto reply while in battlegrounds"]					= true
L["%PN (%PHP) is busy in %LOC BG for %TIM now. %SCR."]			= true
L["Reply message when in BGs:"]									= true
L["Enable auto reply only in battleground combat situations"]	= true
L["Enable also during preparation"]								= true
L["Enable auto reply also in battleground preparation"]			= true
L["<1 min"]														= true
L[" min"]														= true
L["Score: draw"]												= true
L["Leading"]													= true
L["Losing"]														= true
L["%s %d - %d"]													= true
L["resulted draw on"]											= true
L["has won"]													= true
L["has lost"]													= true
L["%s%s %s %s BG!"]												= true

-- Arena
L["Arenas"]														= true
L["Enable in arenas"]											= true
L["Enable auto reply while in arenas"]							= true
L["%PN (%PHP) is busy in %LOC for %TIM now. %SCR."]				= true
L["Reply message when in arenas:"]								= true
L["Enable auto reply also in arena preparation"]				= true
L["Auto reply also to arena party members"]						= true
L["Status: on preparation"]										= true
L["Status: team: %d/%d, enemy: %d/%d"]							= true
L["%s%s %s the Arena match!"]									= true
L["Gold"]														= true	-- arend team color
L["Green"]														= true	-- arena team color

-- Done messages
L[" has been defeated by "]										= true
L[" has defeated "]												= true
L[" is available now."]											= true

-- Help
L["Help"]														= true
L["Message template"]											= true
L["Player's name"]												= true
L["Player's HP(%)"]												= true
L["Target's name"]												= true
L["Target's HP(%)"]												= true
L["Location name (raid/dungeon/BG/arena/world)"]				= true
L["BG score or Arena status"]									= true
L["Instance progress"]											= true
L["Boss name"]													= true
L["Boss HP(%)"]													= true
L["BG/Arena run time"]											= true

-- LDB
L["Click to show missed whispers history"]						= true
L["Right-click to open configuration panel"]					= true