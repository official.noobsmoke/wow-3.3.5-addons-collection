----------------------------------------
-- Addon name	: Animor's AutoReply
-- Author      	: Animor
-- Create Date 	: 24/1/2012
-- File			: Options.lua
----------------------------------------

----------------------------------------
-- Libs
----------------------------------------
local AutoReply	= LibStub("AceAddon-3.0"):GetAddon("AutoReply")
local L 		= LibStub("AceLocale-3.0"):GetLocale("AutoReply")

----------------------------------------
-- Configuration panel options
----------------------------------------
local options = { 
    name = "Animor's AutoReply ".. GetAddOnMetadata("AutoReply", "Version"),	
    handler = AutoReply,
    type = "group",
	get = function(i) return AutoReply.db.profile[i[#i]] end,
    set = function(i, v) AutoReply.db.profile[i[#i]] = v end,
	childGroups = "tab",
    args = {
		desc0 = {
			order = 0,
            type = "description",
            name = "|cffffff00"..L["Auto reply to whispers in combat related situations."].."|r",
			fontSize = "medium",
        },
		desc1 = {
			order = 1,
            type = "description",
            name = " ",
        },
		
		global = {
			order = 2,
			type = "group",
			name = L["General"],
			args = {
				
				descGlobal = {
					order 	= 1,
					type 	= "description",
					name 	= " ",
				},
				
				headerGlobal = {
					order	= 1.1,
					type	= "header",
					name	= L["Global"]
				},
				-- General
				globalEn = {
					order	= 2,
					type 	= "toggle",
					name 	= L["Enable AutoReply"],
					desc 	= L["Global enable for AutoReply addon"],
					width 	= "double",
					get 	= function() return AutoReply.db.profile.globalEn end, 
					set 	= function(info, value)
								AutoReply.db.profile.globalEn = value 
								if value == true then
									AutoReply:Enable()
									AutoReply:Print("addon enabled")
								else
									AutoReply:Disable()
									AutoReply:Print("addon disabled")
								end
							end,
				},				
				
				replyOnce = {
					order	= 3,
					type 	= "toggle",
					name 	= L["Auto reply only once"],
					desc 	= L["Auto reply same whisperer only once per busy session"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,				
				},
				
				replyWhisperersEn = {
					order	= 3,
					type 	= "toggle",
					name 	= L["Notify missed whisperers when available"],
					desc 	= L["Notify missed whisperers when becoming available (except for AFK)."],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},
				
				hide = {
					order	= 4,
					type 	= "toggle",
					name 	= L["Hide minimap icon"],					
					width 	= "double",
					get 	= function() return AutoReply.db.char.minimapIcon.hide end, 
					set 	= function(info, value)
								AutoReply.db.char.minimapIcon.hide = value 
								AutoReply:ToggleMinimapIcon() 
							end,
					-- disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},				
				
				hideReply = {
					order	= 5,
					type 	= "toggle",
					name 	= L["Hide auto reply from chat"],
					desc 	= L["Hide auto reply from the player's chat frame"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,					
				},				
				
				-- AFK
				descAFK = {
					order 	= 6,
					type 	= "description",
					name 	= " ",
				},
				
				headerAFK = {
					order	= 7,
					type	= "header",
					name	= L["AFK"]
				},
				
				awayHistoryEn = {
					order	= 7.3,
					type 	= "toggle",
					name 	= L["Save missed whispers also when AFK"],
					desc 	= L["Save missed whispers also when away/AFK"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},
				
				replyAwayWhisperersEn = {
					order	= 7.4,
					type 	= "toggle",
					name 	= L["Notify missed whisperers when no longer AFK"],
					desc 	= L["Notify missed whisperers when no longer AFK"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.awayHistoryEn and AutoReply.db.profile.globalEn) end,
				},				
				
				-- History				
				descHistory = {
					order 	= 11,
					type 	= "description",
					name 	= " ",
				},
				
				headerHistory = {
					order	= 11.1,
					type	= "header",
					name	= L["History"]
				},
				
				showHistory = {
					order	= 12,
					type	= "execute",
					name	= L["Show History"],
					desc	= L["Show missed whispers history\nAlso type \"/aar his\""],
					width 	= "normal",
					handler = AutoReply,				
					func 	= "ToggleHistoryWindow",
				},
				
				clearHistory = {
					order	= 13,
					type	= "execute",
					name	= L["Clear History"],
					desc	= L["Clear missed whispers history"],					
					width 	= "normal",
					handler = AutoReply,
					confirm = true,
					confirmText = L["Are you sure you want to clear the history?"],
					func 	= "ClearHistory",					
				},
								
				numHistoryEntries = {
					order	= 15,
					type	= "range",
					name	= L["Max number of history entries"],
					desc 	= L["Set the maximum number of whisper history entries to save"],
					min 	= 10,
					max		= 300,
					step 	= 5,
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},				
				
			},
		}, -- global
		
		outdoor = {
			order = 2.5,
			type = "group",
			name = L["Outdoor"],			
			args = {				
				
				-- Outdoor combat
				combatEn = {
					order	= 1,
					type 	= "toggle",
					name 	= L["Enable during outdoor combat"],
					desc 	= L["Enable auto reply during outdoor combat"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},
				
				groupMembersEn = {
					order	= 2,
					type 	= "toggle",
					name 	= L["Also to party/raid members"],
					desc 	= L["Auto reply also to party/raid members"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.combatEn and AutoReply.db.profile.globalEn) end,
				},				
				
				combatText = {
					order	= 3,
					type 	= "input",
					name 	= L["Reply message during outdoor combat:"],					
					width 	= "full",					
					disabled = function(info) return not (AutoReply.db.profile.combatEn and AutoReply.db.profile.globalEn) end,
				},
				
				-- Death
				descDeath = {
					order 	= 4,
					type 	= "description",
					name 	= " ",
				},
								
				headerDeath = {
					order	= 5,
					type	= "header",
					name	= L["Death"]
				},
				
				deadEn = {
					order	= 6,
					type 	= "toggle",
					name 	= L["Enable when dead"],
					desc 	= L["Enable auto reply when running to your corpse"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},
				deadText = {
					order	= 7,
					type 	= "input",
					name 	= L["Reply message during death:"],					
					width 	= "full",					
					disabled = function(info) return not (AutoReply.db.profile.deadEn and AutoReply.db.profile.globalEn) end,
				},
				
				
			},
		}, -- outdoor
		
		
		raid = {
			order = 3,
			type = "group",
			name = L["Raids"],			
			args = {
				raidEn = {
					order	= 1,
					type 	= "toggle",
					name 	= L["Enable in raids"],
					desc 	= L["Enable auto reply while in raids"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},
								
				raidCombatEn = {
					order	= 2,
					type 	= "toggle",
					name 	= L["Enable only in combat"],
					desc 	= L["Enable auto reply only in raid combat situations"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.raidEn and AutoReply.db.profile.globalEn) end,
				},
				
				raidMembersEn = {
					order	= 3,
					type 	= "toggle",
					name 	= L["Also to raid members"],
					desc 	= L["Auto reply also to raid members"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.raidEn and AutoReply.db.profile.globalEn) end,
				},				
				
				descRaid = {
					order 	= 4,
					type 	= "description",
					name 	= " ",
				},
				
				headerMsg = {
					order	= 5,
					type	= "header",
					name	= L["Message"]
				},
								
				raidText = {
					order	= 6,
					type 	= "input",
					name 	= L["Reply message during raids:"],					
					width 	= "full",
					disabled = function(info) return not (AutoReply.db.profile.raidEn and AutoReply.db.profile.globalEn) end,
				},
				
				descRaidBoss = {
					order 	= 7,
					type 	= "description",
					name 	= " ",
				},
				
				headerBoss = {
					order	= 7.1,
					type	= "header",
					name	= L["Boss Fights"]
				},
				
				raidBossEn = {
					order	= 8,
					type 	= "toggle",
					name 	= L["Auto reply on raid boss fights"],
					desc 	= L["Uncheck this if you run Boss mods and want to avoid double auto replies"],
					width 	= "double",					
					disabled = function(info) return not (AutoReply.db.profile.raidEn and AutoReply.db.profile.globalEn) end,
				},				
								
				raidBossText = {
					order	= 9,
					type 	= "input",
					name 	= L["Reply message during raid boss fights:"],					
					width 	= "full",
					disabled = function(info) return not (AutoReply.db.profile.raidBossEn and AutoReply.db.profile.raidEn and AutoReply.db.profile.globalEn) end,
				},		
				
			},
		}, -- raid
		
		dungeon = {
			order = 4,
			type = "group",
			name = L["Dungeons"],			
			args = {
				dungeonEn = {
					order	= 1,
					type 	= "toggle",
					name 	= L["Enable in dungeons"],
					desc 	= L["Enable auto reply while in 5men dungeons"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},
										
				dungeonCombatEn = {
					order	= 2,
					type 	= "toggle",
					name 	= L["Enable only in combat"],
					desc 	= L["Enable auto reply only in 5men dungeon combat situations"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.dungeonEn and AutoReply.db.profile.globalEn) end,
				},
				
				dungeonPartyMembersEn = {
					order	= 3,
					type 	= "toggle",
					name 	= L["Also to party members"],
					desc 	= L["Auto reply also to party members"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.dungeonEn and AutoReply.db.profile.globalEn) end,
				},				
				
				descDungeon = {
					order 	= 4,
					type 	= "description",
					name 	= " ",
				},
					
				headerMsg = {
					order	= 5,
					type	= "header",
					name	= L["Message"]
				},
					
				dungeonText = {
					order	= 6,
					type 	= "input",
					name 	= L["Reply message when in dungeons:"],					
					width 	= "full",
					disabled = function(info) return not (AutoReply.db.profile.dungeonEn and AutoReply.db.profile.globalEn) end,
				},

				descDungeonBoss = {
					order 	= 7,
					type 	= "description",
					name 	= " ",
				},
				
				headerBoss = {
					order	= 7.1,
					type	= "header",
					name	= L["Boss Fights"]
				},
				
				dungeonBossEn = {
					order	= 8,
					type 	= "toggle",
					name 	= L["Auto reply on dungeon boss fights"],
					desc 	= L["Uncheck this if you run Boss mods and want to avoid double auto replies"],
					width 	= "double",					
					disabled = function(info) return not (AutoReply.db.profile.dungeonEn and AutoReply.db.profile.globalEn) end,
				},				
								
				dungeonBossText = {
					order	= 9,
					type 	= "input",
					name 	= L["Reply message during dungeon boss fights:"],					
					width 	= "full",
					disabled = function(info) return not (AutoReply.db.profile.dungeonBossEn and AutoReply.db.profile.dungeonEn and AutoReply.db.profile.globalEn) end,
				},						
				
			},
		}, -- dungeon
		
		battleground = {
			order = 5,
			type = "group",
			name = L["Battlegrounds"],			
			args = {
				bgEn = {
					order	= 1,
					type 	= "toggle",
					name 	= L["Enable in battlegrounds"],
					desc 	= L["Enable auto reply while in battlegrounds"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},
								
				bgCombatEn = {
					order	= 2,
					type 	= "toggle",
					name 	= L["Enable only in combat"],
					desc 	= L["Enable auto reply only in battleground combat situations"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.bgEn and AutoReply.db.profile.globalEn) end,
				},
								
				bgRaidMembersEn = {
					order	= 3,
					type 	= "toggle",
					name 	= L["Also to raid members"],
					desc 	= L["Auto reply also to raid members"],
					width 	= "double",					
					disabled = function(info) return not (AutoReply.db.profile.bgEn and AutoReply.db.profile.globalEn) end,
				},
				
				bgPrepareEn = {
					order	= 4,
					type 	= "toggle",
					name 	= L["Enable also during preparation"],
					desc 	= L["Enable auto reply also in battleground preparation"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.bgEn and AutoReply.db.profile.globalEn) end,
				},
				
				descBG = {
					order 	= 5,
					type 	= "description",
					name 	= " ",
				},
				
				headerMsg = {
					order	= 5.1,
					type	= "header",
					name	= L["Message"]
				},
					
				bgText = {
					order	= 6,
					type 	= "input",
					name 	= L["Reply message when in BGs:"],					
					width 	= "full",
					disabled = function(info) return not (AutoReply.db.profile.bgEn and AutoReply.db.profile.globalEn) end,
				},							
								
			},
		}, -- bg
		
		arena = {
			order = 6,
			type = "group",
			name = L["Arenas"],			
			args = {
				arenaEn = {
					order	= 1,
					type 	= "toggle",
					name 	= L["Enable in arenas"],
					desc 	= L["Enable auto reply while in arenas"],
					width 	= "double",
					disabled = function(info) return not AutoReply.db.profile.globalEn end,
				},
				
				arenaPartyMembersEn = {
					order	= 3,
					type 	= "toggle",
					name 	= L["Also to party members"],
					desc 	= L["Auto reply also to arena party members"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.arenaEn and AutoReply.db.profile.globalEn) end,
				},
				
				arenaPrepareEn = {
					order	= 4,
					type 	= "toggle",
					name 	= L["Enable also during preparation"],
					desc 	= L["Enable auto reply also in arena preparation"],
					width 	= "double",
					disabled = function(info) return not (AutoReply.db.profile.arenaEn and AutoReply.db.profile.globalEn) end,
				},
				
				descArena = {
					order 	= 5,
					type 	= "description",
					name 	= " ",
				},
				
				headerMsg = {
					order	= 5.1,
					type	= "header",
					name	= L["Message"]
				},
					
				arenaText = {
					order	= 6,
					type 	= "input",
					name 	= L["Reply message when in arenas:"],					
					width 	= "full",
					disabled = function(info) return not (AutoReply.db.profile.arenaEn and AutoReply.db.profile.globalEn) end,
				},								
			},
		}, -- arena				
		
		help = {
			order = 8,
			type = "group",
			name = L["Help"],
			args = {				
				desc05 = {
					order = 0.5,
					type = "description",
					name = " ",
					fontSize = "large",
				},
				
				desc1 = {
					order = 1,
					type = "description",
					name = "|cffffff00"..L["Message template"].."|r",
					fontSize = "large",
				},
				desc15 = {
					order = 1.5,
					type = "description",
					name = " ",
					fontSize = "large",
				},
				
				desc2 = {
					order = 2,
					type = "description",
					name = "|cff00ff00%PN|r   - "..L["Player's name"],
					fontSize = "medium",
				},
				
				desc25 = {
					order = 2.5,
					type = "description",
					name = " ",				
				},
				
				desc3 = {
					order = 3,
					type = "description",
					name = "|cff00ff00%PHP|r - "..L["Player's HP(%)"],
					fontSize = "medium",
				},
				
				desc35 = {
					order = 3.5,
					type = "description",
					name = " ",				
				},
				
				desc4 = {
					order = 4,
					type = "description",
					name = "|cff00ff00%TN|r   - "..L["Target's name"],
					fontSize = "medium",
				},
				
				desc45 = {
					order = 4.5,
					type = "description",
					name = " ",				
				},
				
				desc5 = {
					order = 5,
					type = "description",
					name = "|cff00ff00%THP|r - "..L["Target's HP(%)"],
					fontSize = "medium",
				},
				
				desc55 = {
					order = 5.5,
					type = "description",
					name = " ",				
				},
				
				desc6 = {
					order = 6,
					type = "description",
					name = "|cff00ff00%LOC|r - "..L["Location name (raid/dungeon/BG/arena/world)"],
					fontSize = "medium",
				},
				
				desc65 = {
					order = 6.5,
					type = "description",
					name = " ",				
				},
				
				desc7 = {
					order = 7,
					type = "description",
					name = "|cff00ff00%SCR|r - "..L["BG score or Arena status"],
					fontSize = "medium",
				},

				desc105 = {
					order = 7.5,
					type = "description",
					name = " ",				
				},
				
				desc11 = {
					order = 7.7,
					type = "description",
					name = "|cff00ff00%TIM|r - "..L["BG/Arena run time"],
					fontSize = "medium",
				},		
											
				
				desc75 = {
					order = 7.8,
					type = "description",
					name = " ",				
				},
				
				desc8 = {
					order = 8,
					type = "description",
					name = "|cff00ff00%PRG|r - "..L["Instance progress"],
					fontSize = "medium",
				},		
								
								
				desc85 = {
					order = 8.5,
					type = "description",
					name = " ",				
				},
				
				desc9 = {
					order = 9,
					type = "description",
					name = "|cff00ff00%BN|r   - "..L["Boss name"],
					fontSize = "medium",
				},		
				
				desc95 = {
					order = 9.5,
					type = "description",
					name = " ",				
				},
				
				desc10 = {
					order = 10,
					type = "description",
					name = "|cff00ff00%BHP|r - "..L["Boss HP(%)"],
					fontSize = "medium",
				},				
				
			},
		},		
    }, -- main args
} -- options

AutoReply.options = options