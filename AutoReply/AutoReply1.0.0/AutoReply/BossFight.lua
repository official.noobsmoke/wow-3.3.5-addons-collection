----------------------------------------
-- Addon name	: Animor's AutoReply
-- Author      	: Animor
-- Create Date 	: 4/3/2012
-- File			: BossFight.lua
----------------------------------------

----------------------------------------
-- Libs
----------------------------------------
local AutoReply	= LibStub("AceAddon-3.0"):GetAddon("AutoReply")
local L 		= LibStub("AceLocale-3.0"):GetLocale("AutoReply")

----------------------------------------
-- Local variables
----------------------------------------

-- Debug messages
local debug	= nil

-- Boss IDs (for encounters without INSTANCE_ENCOUNTER_ENGAGE_UNIT)	
local bossIDList	= {
	54431, -- Echo of Baine
	54445, -- Echo of Jaina
	54123, -- Echo of Sylvanas
	54544, -- Echo of Tyrande
	54432, -- Murozond 
	54853, -- Azshara
	55085, -- Perotharn
	54969, -- Mannoroth
	54590, -- Arcurion
	54968, -- Asira Dawnslayer
	54938, -- Benedictus
	46383, -- Randolph Moloch
	46264, -- Lord Overheat
	46254, -- Hogger
}

-- Extract encounter name from mobs names that appear in boss frame
local bossMobs	= {		
	-- Warmaster Blackhorn
	[56598]	= L["Warmaster Blackhorn"],		-- The Skyfire
	
	-- Spine of Deathwing
	[53879] = L["Spine of Deathwing"],		-- Deathwing
	
	-- Madness of Deathwing
	[56846] = L["Madness of Deathwing"], 	-- Arm Tentacle
	[56167] = L["Madness of Deathwing"], 	-- Arm Tentacle
	[56471] = L["Madness of Deathwing"], 	-- Mutated Corruption
	[56168] = L["Madness of Deathwing"], 	-- Wing Tentacle
}

--======================================
--======================================
-- BOSS FIGHT FUNCTIONS
--======================================
--======================================

----------------------------------------
-- Unit died or distroyed
----------------------------------------
function AutoReply:COMBAT_LOG_EVENT_UNFILTERED(_, _, subevent, ...)
	if subevent == "UNIT_DIED" or subevent == "UNIT_DESTROYED" then 
		self:CheckBossKill(subevent)
	end
end

----------------------------------------
-- Player enters combat
----------------------------------------
function AutoReply:PLAYER_REGEN_DISABLED()
	if self.db.profile.raidEn or self.db.profile.dungeonEn then
		local _, instanceType = IsInInstance()
		if instanceType == "raid" or instanceType == "party" and not self.bossEngaged then
			self:CheckBossFightStart()
		end
	end
end

-------------------------------------------------
-- Boss engage event - check for start/kill/wipe
-------------------------------------------------
function AutoReply:INSTANCE_ENCOUNTER_ENGAGE_UNIT()
	if self.db.profile.raidEn or self.db.profile.dungeonEn then
		if not self.bossEngaged then
			self:CheckBossFightStart()
		else
			self:CheckBossKill("INSTANCE_ENCOUNTER_ENGAGE_UNIT")
		end
	end
end

----------------------------------------
-- Check if boss fight started
----------------------------------------
function AutoReply:CheckBossFightStart()	
	if not self.bossEngaged then
		local bossEngaged, targetInCombat = self:BossEngaged()
		if bossEngaged then
			-- Boss fight start
			if debug then self:Print("boss fight start") end
			self.bossEngaged = true
			self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
			self:RegisterEvent("CHAT_MSG_MONSTER_YELL", "CheckBossKill")
			self:RegisterEvent("CHAT_MSG_MONSTER_EMOTE", "CheckBossKill")
			self:RegisterEvent("RAID_BOSS_EMOTE", "CheckBossKill")
			self:RegisterEvent("CHAT_MSG_MONSTER_SAY", "CheckBossKill")			
			-- self:Print("ScheduleTimer CheckBossWipe - first")
			self:ScheduleTimer("CheckBossWipe", 3)
		elseif (not targetInCombat) and UnitAffectingCombat("player") then
			-- If boss encounter starts before the boss was targetted or before the boss is in combat, schedule boss fight check again.
			-- self:Print("ScheduleTimer CheckBossFightStart")
			self:ScheduleTimer("CheckBossFightStart", 3)
		end
	end
end

----------------------------------------
-- Check for boss fight wipe
----------------------------------------
function AutoReply:CheckBossWipe(confirm)	
	if self.bossEngaged then
		local allBossesDead, bossFound = self:AllBossesDead()
		if self:GroupInCombat() or (bossFound and not allBossesDead) then
			-- No wipe (schedule wipe check again) if:
			-- 1) Still in combat.
			-- 2) Encountered started and Boss appeared, but out of combat yet.
			-- self:Print("ScheduleTimer CheckBossWipe")
			self:ScheduleTimer("CheckBossWipe", 3)			
		elseif confirm then
			-- Boss fight wipe
			if debug then self:Print("boss fight: wipe") end
			self:ReplyAllMissedBoss("wipe")
			self:HandleBossFightEnd()
		else
			-- Schedule for boss fight wipe in 3 seconds (unless boss is killed before)
			-- self:Print("ScheduleTimer CheckBossWipe confirm")
			self:ScheduleTimer("CheckBossWipe", 3, true)		
		end			
	end	
end

--------------------------------------------
-- Perform when boss fight ends (wipe/kill)
--------------------------------------------
function AutoReply:HandleBossFightEnd()
	self.bossEngaged		= false
	self.bossNum			= nil
	self.bossName			= L["Unknown"]
	self:CancelAllTimers()
	self.checkBossKillTimer = nil
	self:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	self:UnregisterEvent("CHAT_MSG_MONSTER_YELL")
	self:UnregisterEvent("CHAT_MSG_MONSTER_EMOTE")
	self:UnregisterEvent("RAID_BOSS_EMOTE")
	self:UnregisterEvent("CHAT_MSG_MONSTER_SAY")
end

----------------------------------------
-- Check if boss is alive
----------------------------------------
function AutoReply:BossEngaged()
	local bossEngaged 		= false
	local targetInCombat 	= false
	local bossNum			= ""
	local bossID			= ""
	
	-- Check for boss frame
	for i = 1, MAX_BOSS_FRAMES do
		bossNum	= "boss"..i
		if UnitExists(bossNum) and not UnitIsDead(bossNum) then -- no need to check UnitAffectingCombat for boss frames (some of them may be friendly).
			bossEngaged		= true
			if UnitAffectingCombat(bossNum) then
				targetInCombat	= true
			end
			self.bossNum	= bossNum
			
			-- Try to match mob name to real boss name
			bossID 			= self:GetUnitId(bossNum)
			for mobID, bossName in pairs(bossMobs) do
				if mobID == bossID then
					self.bossName = bossName
					break
				end
			end
			
			-- If mob name wasn't matched, use unit name.
			if self.bossName == L["Unknown"] then self.bossName = UnitName(bossNum)	end
			break
		end		
	end
	
	-- Manual check targets, for bosses without boss frame
	if not bossEngaged then
		bossEngaged, targetInCombat = self:CheckTargetBoss()
	end
	
	return bossEngaged, targetInCombat
end

----------------------------------------
-- Check if boss is dead
----------------------------------------
function AutoReply:AllBossesDead()
	local allBossesDead = true
	local bossFound		= false
	local bossAlive 	= false
		
	-- Check for boss frame
	for i = 1, MAX_BOSS_FRAMES do
		if UnitExists("boss"..i) then
			bossFound = true
			if not UnitIsDead("boss"..i) then
				allBossesDead	= false
			end
		end		
	end
	
	-- Manual check targets, for bosses without boss frame
	if not bossFound then
		_, _, bossFound, bossAlive = self:CheckTargetBoss()
		if bossAlive or not bossFound then
			allBossesDead	= false
		end
	end
	
	return allBossesDead, bossFound
end

---------------------------------------------
-- Manual check targets for boss
-- Returns bossEngaged, targetInCombat, bossFound, bossAlive
---------------------------------------------
function AutoReply:CheckTargetBoss()
	local targetInCombat	= false	
	local bossFound 		= false	
	local bossAlive 		= false	
	local unitId 			= ((GetNumRaidMembers() == 0) and "party") or "raid"
	local target 			= nil
	local targetUnitID 		= nil
	
	for i = 0, math.max(GetNumRaidMembers(), GetNumPartyMembers()) do
		target = (i == 0 and "playertarget") or unitId..i.."target"
		if UnitExists(target) and not UnitIsPlayer(target) then
			if UnitAffectingCombat(target) then			
				targetInCombat = true
			end
			if UnitClassification(target) == "worldboss" then
				-- worldboss
				bossFound = true -- boss found (dead/alive/in combat)
				if not UnitIsDead(target) then
					bossAlive = true -- boss is alive
				end
				if UnitAffectingCombat(target) then -- boss is in combat
					if self.bossName == L["Unknown"] then self.bossName = UnitName(target)	end
					return true, targetInCombat, bossFound, bossAlive
				end
			else
				-- check against boss ID list
				targetUnitID = self:GetUnitId(target)
				-- for _, bossID in pairs(bossIDList) do
				for i = 1, #bossIDList do
					if targetUnitID == bossIDList[i] then
						bossFound = true -- boss found (dead/alive/in combat)
						if not UnitIsDead(target) then
							bossAlive = true -- boss is alive
						end
						if UnitAffectingCombat(target) then -- boss is in combat
							if self.bossName == L["Unknown"] then self.bossName = UnitName(target)	end
							return true, targetInCombat, bossFound, bossAlive
						end
					end
				end
			end
		end
	end
	
	return false, targetInCombat, bossFound, bossAlive
end

----------------------------------------
-- Check for boss fight end
----------------------------------------
function AutoReply:CheckBossKill(eventname)
	if debug then self:Print(eventname) end
	if self.bossEngaged and (not UnitAffectingCombat("player")) and (self.db.profile.raidEn or self.db.profile.dungeonEn) then
		if debug then self:Print("CheckBossKill") end -- debug
		local allBossesDead, bossFound = self:AllBossesDead()
		if debug then self:Print(allBossesDead) end -- debug
		if allBossesDead or ((not bossFound) and self:GroupSurvived()) then
			if debug then self:Print("boss fight: kill") end	-- debug
			self.numKilledBosses = self.numKilledBosses + 1
			self:ReplyAllMissedBoss("kill")
			self:HandleBossFightEnd()
		-- Reschedule kill check in case:
		-- 1) Player is dead (so no PLAYER_REGEN_DISABLED will trigger when out of combat).
		-- 2) Boss is down and its frame/body vanish
		-- 3) Other players are still in combat for some time until the encounter ends.
		elseif (not bossFound) and self:GroupInCombat() and 
			  ((not self:TimeLeft(self.checkBossKillTimer)) or self:TimeLeft(self.checkBossKillTimer) <= 0.1) then -- Do not schedule more than one at a time.
			self.checkBossKillTimer = self:ScheduleTimer("CheckBossKill", 0.5)
		end
	end	
end

--------------------------------------
-- Check if group is in combat
--------------------------------------
function AutoReply:GroupInCombat()
	local unitId = ((GetNumRaidMembers() == 0) and "party") or "raid"
	for i = 0, math.max(GetNumRaidMembers(), GetNumPartyMembers()) do
		local id = (i == 0 and "player") or unitId..i
		if UnitAffectingCombat(id) and UnitIsVisible(id) and not UnitIsDeadOrGhost(id) then
			return true
		end
	end
	return false
end

----------------------------------------
-- Check if group is alive
----------------------------------------
function AutoReply:GroupSurvived()
	if not IsInInstance() then return false end -- Do not check if player is not in instance (return false).
	local unitId = ((GetNumRaidMembers() == 0) and "party") or "raid"
	for i = 0, math.max(GetNumRaidMembers(), GetNumPartyMembers()) do
		local id = (i == 0 and "player") or unitId..i
		-- Check if player is out of combat and didn't use any feign death ability to reset the boss.
		-- Check in addition if player is in 100 yard range.
		if UnitIsVisible(id) and not (UnitAffectingCombat(id) or UnitIsDeadOrGhost(id) or UnitIsFeignDeath(id) or 
		UnitAura(id, "Vanish") or UnitAura(id, "Invisibility") or UnitAura(id, "Shadowmeld") or UnitAura(id, "Spirit of Redemption")) then
			if debug then self:Print(GetUnitName(id)) end
			return true
		end
	end
	return false
end

----------------------------------------
-- Get unit ID
----------------------------------------
function AutoReply:GetUnitId(unit)
	local guid = UnitGUID(unit)
	return (guid and (tonumber(guid:sub(7, 10), 16))) or 0
end

------------------------------------------------
-- Reply to all boss fight missed on kill/wipe
------------------------------------------------
function AutoReply:ReplyAllMissedBoss(status)
	-- Check if enabled	
	if next(self.missedBossSenders) ~= nil and self.db.profile.replyWhisperersEn then
		-- Compose done/available message
		local replyMsg = ""	
		if status == "kill" then
			-- Boss kill
			replyMsg = self.msgPrefix..UnitName("player")..L[" has defeated "]..self.bossName.." :-)"
		elseif 	status == "wipe" then
			-- Boss wipe
			replyMsg = self.msgPrefix..UnitName("player")..L[" has been defeated by "]..self.bossName.." :-("			
		else
			self:Print("wrong boss fight status!")
		end
		-- Send to missed boss fight senders list
		for sender in pairs(self.missedBossSenders) do
			self:SendReply(replyMsg, sender)
		end
	end
	-- Always clear missed boss fight whisperers table
	wipe(self.missedBossSenders)
end