## Interface: 40300
## Author: Animor
## Title: Animor's AutoReply
## Version: 1.0
## Notes: Auto reply whispers in specific combat related situations.
## OptionalDeps: Ace3, Recount, LibDataBroker-1.1
## X-Embeds: Ace3, LibDataBroker-1.1
## X-Category: Chat/Communication
## SavedVariables: AutoReplyDB

embed.xml
Locales\Locales.xml
Core.lua
BossFight.lua
Options.lua
HistoryFrame.lua