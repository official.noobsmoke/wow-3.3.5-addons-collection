﻿----------------------------------------
-- Addon name	: Animor's AutoReply
-- Author      	: Animor
-- Create Date 	: 20/1/2012
-- File			: HistoryFrame.lua
----------------------------------------

----------------------------------------
-- Libs
----------------------------------------
local AutoReply	= LibStub("AceAddon-3.0"):GetAddon("AutoReply")
local L 		= LibStub("AceLocale-3.0"):GetLocale("AutoReply")

----------------------------------------
-- Backdrop templates
----------------------------------------
local OuterBackdrop = {
	bgFile		= "Interface\\DialogFrame\\UI-DialogBox-Background",
	edgeFile	= "Interface\\DialogFrame\\UI-DialogBox-Border",
	tile 		= true, 
	tileSize 	= 32, 
	edgeSize 	= 32,
	insets 		= { left = 8, right = 8, top = 8, bottom = 8 }
}

local InnerBackdrop  = {
	bgFile 		= "Interface\\ChatFrame\\ChatFrameBackground",
	edgeFile 	= "Interface\\Tooltips\\UI-Tooltip-Border",
	tile 		= true, 
	tileSize 	= 16, 
	edgeSize 	= 16,
	insets 		= { left = 3, right = 3, top = 5, bottom = 3 }
}

local SliderBackdrop  = {
	bgFile 		= "Interface\\ChatFrame\\ChatFrameBackground",	
	tile 		= true, 
	tileSize 	= 10, 
}

----------------------------------------
-- Main frame
----------------------------------------
local frame  = CreateFrame("Frame", "AutoReplyHistory", UIParent)
frame.width  = 600
frame.height = 450
frame:SetFrameStrata("FULLSCREEN_DIALOG")
frame:SetToplevel(true)
frame:SetSize(frame.width, frame.height)
frame:SetPoint("CENTER", UIParent, "CENTER", 0, 0)
frame:SetBackdrop(OuterBackdrop)
frame:SetBackdropColor(0, 0, 0, 1)
frame:EnableMouse(true)
frame:EnableMouseWheel(true)

-- Make movable/resizable
frame:SetMovable(true)
frame:SetResizable(enable)
frame:SetMinResize(200, 100)
table.insert(UISpecialFrames, "AutoReplyHistory")

----------------------------------------
-- Nain frame title
----------------------------------------
local titlebg = frame:CreateTexture(nil, "OVERLAY")
titlebg:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
titlebg:SetTexCoord(0.31, 0.67, 0, 0.63)
titlebg:SetPoint("TOP", 0, 12)
titlebg:SetWidth(170)
titlebg:SetHeight(40)

local titlebg_l = frame:CreateTexture(nil, "OVERLAY")
titlebg_l:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
titlebg_l:SetTexCoord(0.21, 0.31, 0, 0.63)
titlebg_l:SetPoint("RIGHT", titlebg, "LEFT")
titlebg_l:SetWidth(30)
titlebg_l:SetHeight(40)

local titlebg_r = frame:CreateTexture(nil, "OVERLAY")
titlebg_r:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
titlebg_r:SetTexCoord(0.67, 0.77, 0, 0.63)
titlebg_r:SetPoint("LEFT", titlebg, "RIGHT")
titlebg_r:SetWidth(30)
titlebg_r:SetHeight(40)

local title = CreateFrame("Frame", nil, frame)
title:EnableMouse(true)
title:RegisterForDrag("LeftButton")
title:SetScript("OnDragStart", function(self) self:GetParent():StartMoving() end)
title:SetScript("OnDragStop",  function(self) self:GetParent():StopMovingOrSizing() end)
title:SetAllPoints(titlebg)

local titletext = title:CreateFontString(nil, "OVERLAY", "GameFontNormal")
titletext:SetPoint("TOP", titlebg, "TOP", 0, -14)
titletext:SetText("Animor's AutoReply "..L["History"])

----------------------------------------
-- Close button
----------------------------------------
local closeButton = CreateFrame("Button", nil, frame, "UIPanelButtonTemplate")
closeButton:SetPoint("BOTTOM", 0, 10)
closeButton:SetHeight(30)
closeButton:SetWidth(85)
closeButton:SetText(CLOSE)
closeButton:SetScript("OnClick", function(self)	self:GetParent():Hide() end)
frame.closeButton = closeButton

----------------------------------------
-- Clear history button
----------------------------------------
local clearButton = CreateFrame("Button", nil, frame, "UIPanelButtonTemplate")

clearButton.buttonUpTexture = clearButton:CreateTexture(nil, nil, "UIPanelButtonUpTexture")
clearButton.buttonUpTexture:SetAlpha(0.7)
clearButton:SetNormalTexture(clearButton.buttonUpTexture)

clearButton.buttonDownTexture = clearButton:CreateTexture(nil, nil, "UIPanelButtonDownTexture")
clearButton.buttonDownTexture:SetAlpha(0.7)
clearButton:SetPushedTexture(clearButton.buttonDownTexture)

clearButton:SetNormalFontObject("GameFontDisable")
-- clearButton:SetHighlightFontObject("GameFontHighlight")

clearButton:SetPoint("BOTTOMLEFT", 15, 15)
clearButton:SetHeight(22)
clearButton:SetWidth(95)
clearButton:SetText(L["Clear History"])
clearButton:SetScript("OnClick", function(self)
		StaticPopupDialogs["AutoReply clear history"] = {
			text = L["Are you sure you want to clear the history?"],
			button1 	= ACCEPT,
			button2 	= CANCEL,			
			timeout 	= 0,
			whileDead 	= true,
			hideOnEscape= true,
			OnAccept  	= function()
				AutoReply:ClearHistory()
			end,			
		}
		AutoReplyHistoryDialog = StaticPopup_Show("AutoReply clear history")
		AutoReplyHistoryDialog:SetFrameStrata("TOOLTIP")
		end)
		
frame.clearButton = clearButton

----------------------------------------
-- Inner frame
----------------------------------------
local innerFRame = CreateFrame("Frame", nil, frame)
innerFRame:SetPoint("CENTER", 0, 5)
innerFRame:SetSize(frame.width - 35, frame.height - 75)
innerFRame:SetBackdrop(InnerBackdrop)
innerFRame:SetBackdropColor(0.1,0.1,0.1, 0.6)
innerFRame:SetBackdropBorderColor(0.4, 0.4, 0.4)
frame.innerFRame = innerFRame

----------------------------------------
-- ScrollingMessageFrame
----------------------------------------
local messageFrame = CreateFrame("ScrollingMessageFrame", nil, frame)
messageFrame:SetPoint("LEFT", innerFRame, "LEFT", 10, 0)
messageFrame:SetSize(frame.width - 70, frame.height - 90)
messageFrame:SetFontObject(GameFontNormal)
messageFrame:SetTextColor(1, 1, 1, 1) -- default color
messageFrame:SetJustifyH("LEFT")
messageFrame:SetHyperlinksEnabled(true)
messageFrame:SetFading(false)
messageFrame:SetMaxLines(300)
messageFrame:SetScript("OnHyperlinkClick", function(self, ...) SetItemRef(...); end)
frame.messageFrame = messageFrame

----------------------------------------
-- Scroll bar
----------------------------------------
local scrollBar = CreateFrame("Slider", nil, frame, "UIPanelScrollBarTemplate")
scrollBar:SetPoint("RIGHT", innerFRame, "RIGHT", -5, 0)
scrollBar:SetSize(20, frame.height - 125)
scrollBar:SetMinMaxValues(0, 300)
scrollBar:SetValueStep(1)
scrollBar.scrollStep = 2	
scrollBar:SetBackdrop(SliderBackdrop)
scrollBar:SetBackdropColor(0, 0, 0, 1)
frame.scrollBar = scrollBar

scrollBar:SetScript("OnValueChanged", function(self, value)	
	if (value == 0) then		
		-- workaround for wrapping lines that may corrupt first messages.
		messageFrame:SetScrollOffset(messageFrame:GetNumMessages() - messageFrame:GetNumLinesDisplayed())
	else
		-- Normal scrolling
		messageFrame:SetScrollOffset(select(2, scrollBar:GetMinMaxValues()) - value)	
	end
end)

scrollBar:SetValue(select(2, scrollBar:GetMinMaxValues()))

----------------------------------------
-- Main frame scrolling script
----------------------------------------
frame:SetScript("OnMouseWheel", function(self, delta)	

	local cur_val = scrollBar:GetValue()
	local min_val, max_val = scrollBar:GetMinMaxValues()

	if delta < 0 and cur_val < max_val then
		cur_val = math.min(max_val, cur_val + 2)
		scrollBar:SetValue(cur_val)
	elseif delta > 0 and cur_val > min_val then
		cur_val = math.max(min_val, cur_val - 2)
		scrollBar:SetValue(cur_val)
	end	
end)


