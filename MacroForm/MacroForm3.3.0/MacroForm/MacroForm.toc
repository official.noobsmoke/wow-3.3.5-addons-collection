﻿## Interface: 30300
## Title: MacroForm
## Author: Zaman
## Version: 3.3.0.3
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: MacroFormSettings
## X-Curse-Packaged-Version: v3.3.0.3
## X-Curse-Project-Name: MacroForm
## X-Curse-Project-ID: macroform
## X-Curse-Repository-ID: wow/macroform/mainline
locale\localization.lua
MacroForm.xml
