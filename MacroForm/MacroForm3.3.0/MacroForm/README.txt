﻿Usage:

/mf - Opens the MacroForm window
/mf hide - Hides the MacroForm window

Drag a spell/ability from your spellbook to the spell area in the window.
Check the conditions and the target you want to use and press generate.
Copy the script from the bottom of the window and paste it in to a macro.

For castsequence hit the Sequence button at the top (OneLine to go back).
Set the conditions and reset conditions for your macro, drag in a spell,
press add, drag in a new spell, press add and so on until you're done.
Copy the script from the bottom of the window and paste it in to the macro.

----
v3.3.0.1 Update:
Added castsequence functionality.
----
v3.3.0.0 Update:
Version update and removed [] when condition is empty.
----
v3.2.0.0 Update:
Version update
----
v3.1.0.0 Update:
Updated version and Interface number to match WoW