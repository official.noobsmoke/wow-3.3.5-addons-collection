﻿local MACROFORM_STEALTH = {
  "none",
  "stealth",
  "nostealth"
}

local MACROFORM_COMBAT = {
  "none",
  "combat",
  "nocombat"
}

local MACROFORM_STANCE = {
  "none",
  "0",
  "1",
  "2",
  "3",
  "4",
  "5"
}

local MACROFORM_AGRESSION = {
  "none",
  "harm",
  "help"
}

local MACROFORM_TARGET = {
  "notarget",
  "target",
  "targetoftarget",
  "player",
  "pet",
  "focus",
  "none",
  "mouseover"
}

local MACROFORM_DEAD = {
  "none",
  "dead",
  "nodead"
}

local MacroStealthSelected,MacroTargetSelected,MacroDeadSelected,MacroAgressionSelected,MacroStanceSelected,MacroCombatSelected,spellname,channelingname,MacroSequenceTargetSelected,MacroSequenceAgressionSelected,MacroSequenceDeadSelected
local MacroFormSpellList = {}

function MacroForm_toggle_visible()
  if(MacroForm:IsShown()) then
    MacroForm:Hide()
  else
    MacroForm:Show()
  end
end

function MacroForm_OnLoad(self)
  local version = GetAddOnMetadata("MacroForm", "Version")
  MacroForm_SetVariables()
  self:RegisterEvent("VARIABLES_LOADED")

  SlashCmdList["MACROFORM"] = function(msg)
    MacroForm_SlashCommand(msg)
  end
  SLASH_MACROFORM1 = "/mf"
  
  if( DEFAULT_CHAT_FRAME ) then
    DEFAULT_CHAT_FRAME:AddMessage("|cffffff00MacroForm v"..version.." loaded")
  end
  UIErrorsFrame:AddMessage("MacroForm v"..version.." AddOn loaded", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME)
  MacroTargetSelected = "notarget"
  MacroDeadSelected = "none"
  MacroAgressionSelected = "none"
  MacroStanceSelected = "none"
  MacroCombatSelected = "none"
  MacroStealthSelected = "none"
  MacroForm:Hide()
end

function MacroForm_OnEvent()
  if (event=="VARIABLES_LOADED") then
    MacroForm:ClearAllPoints()
    MacroForm:SetPoint("BOTTOMLEFT","UIParent", "BOTTOMLEFT", MacroFormSettings.dispx, MacroFormSettings.dispy)
  end
end

function MacroForm_SetVariables()
  if not(MacroFormSettings) then
    MacroFormSettings = {}
    MacroFormSettings.dispx = GetScreenWidth()/2 - 200
    MacroFormSettings.dispy = GetScreenHeight()/2 - 200
  end
end


function MacroForm_SlashCommand(msg)
  if(msg) then
    local command = strlower(msg)
    if(command == "hide") then
      MacroForm:Hide()
    else
      MacroForm:Show()
    end
  end
end

function MacroForm_SavePosition(self)
  MacroFormSettings.dispx = self:GetLeft()
  MacroFormSettings.dispy = self:GetBottom()
end

function Macro_OnAdd()
	if(MacroFormOneLine:IsShown()) then
		Macro_AddOneLine()
	else
		Macro_AddSequence()
	end
end

function Macro_AddSequence()
  local macro = _G["MacroFormMacroEditBoxEdit"]
  local alt = _G["MacroFormSequenceResetAlt"]:GetChecked()
  local shift = _G["MacroFormSequenceResetShift"]:GetChecked()
  local ctrl = _G["MacroFormSequenceResetCtrl"]:GetChecked()
  local target = _G["MacroFormSequenceResetTarget"]:GetChecked()
  local combat = _G["MacroFormSequenceResetCombat"]:GetChecked()
  local secondsBox = _G["MacroFormSequenceResetSeconds"]
  local text = macro:GetText()
  local seconds = secondsBox:GetText()
  local filter, reset
  if(not(text) or (text == "")) then
  	wipe(MacroFormSpellList)
  end
  if(MacroSequenceTargetSelected ~= nil and MacroSequenceTargetSelected ~= "notarget") then
    filter = "@"..MacroSequenceTargetSelected
  end
  if(MacroSequenceAgressionSelected ~= nil and MacroSequenceAgressionSelected ~= "none") then
    if(filter) then
      filter = filter..","..MacroSequenceAgressionSelected
    else
      filter = MacroSequenceAgressionSelected
    end
  end
  if(MacroSequenceDeadSelected ~= nil and MacroSequenceDeadSelected ~= "none") then
    if(filter) then
      filter = filter..","..MacroSequenceDeadSelected
    else
      filter = MacroSequenceDeadSelected
    end
  end
  tinsert(MacroFormSpellList,getn(MacroFormSpellList)+1,spellname)
  text = "#showtooltip\n/castsequence "
  if(seconds and not(seconds == "")) then
  	reset = seconds
  end
  if(filter) then
    text = text.."["..filter.."] "
  end
  if(target) then
  	if(reset) then
  	  reset = reset.."/target"
  	else
  	  reset = "target"
  	end
  end
  if(combat) then
  	if(reset) then
  	  reset = reset.."/combat"
  	else
  	  reset = "combat"
  	end
  end
  if(alt) then
  	if(reset) then
  	  reset = reset.."/alt"
  	else
  	  reset = "alt"
  	end
  end
  if(shift) then
  	if(reset) then
  	  reset = reset.."/shift"
  	else
  	  reset = "shift"
  	end
  end
  if(ctrl) then
  	if(reset) then
  	  reset = reset.."/ctrl"
  	else
  	  reset = "ctrl"
  	end
  end
  if(reset) then
    text = text.."reset="..reset.." "
  end
  for i=1,getn(MacroFormSpellList) do
  	text = text .. MacroFormSpellList[i]
  	if i < getn(MacroFormSpellList) then
  	  text = text..", "
  	end
  end
  macro:SetText(text)
end

function Macro_AddOneLine()
  local exists = _G["MacroFormOneLineTargetPanelCheckExists"]:GetChecked()
  local alt = _G["MacroFormOneLineCheckALT"]:GetChecked()
  local shift = _G["MacroFormOneLineCheckShift"]:GetChecked()
  local ctrl = _G["MacroFormOneLineCheckCTRL"]:GetChecked()
  local macro = _G["MacroFormMacroEditBoxEdit"]
  local channelingchecked = _G["MacroFormOneLineSelfPanelCheckChanneling"]:GetChecked()
  local text = macro:GetText()
  local filter
  
  local obj = _G["MacroFormMacroEditBoxEdit"]
  if(not(text) or (text == "")) then
    text = "/cast "
  else
    text = text.."; "
  end
  if(MacroTargetSelected ~= nil and MacroTargetSelected ~= "notarget") then
    filter = "@"..MacroTargetSelected
  end
  if(MacroAgressionSelected and MacroAgressionSelected ~= "none") then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter..MacroAgressionSelected
  end
  if(MacroDeadSelected and MacroDeadSelected ~= "none") then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter..MacroDeadSelected
  end
  if(MacroStanceSelected and MacroStanceSelected ~= "none") then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter.."stance:"..MacroStanceSelected
  end
  if(MacroCombatSelected and MacroCombatSelected ~= "none") then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter..MacroCombatSelected
  end
  if(channelingchecked) then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    if(channelingname) then
      filter = filter.."channeling:"..channelingname
    else
      filter = filter.."channeling"
    end
  end
  if(MacroStealthSelected and MacroStealthSelected ~= "none") then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter..MacroStealthSelected
  end
  if(exists) then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter.."exists"
  end
  if(alt) then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter.."mod:alt"
  end
  if(shift) then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter.."mod:shift"
  end
  if(ctrl) then
    if(filter) then
      filter = filter..","
    else
      filter = ""
    end
    filter = filter.."mod:ctrl"
  end
  if(filter) then
    text = text.."["..filter.."] "
  end
  if(spellname) then
    text = text..spellname
  else
    text = text.."<spellname>"
  end
  macro:SetText(text)
end

function Macro_OnClear()
  local obj = _G["MacroFormMacroEditBoxEdit"]
  obj:SetText("")
end

function Macro_OnClose()
  MacroForm:Hide()
end

--
--	** TARGET DROP DOWN **
--
function MacroForm_TargetDropdown_OnClick(self)
  local i = self:GetID()
  MacroTargetSelected = MACROFORM_TARGET[i]
  UIDropDownMenu_SetSelectedID(MacroFormOneLineTargetPanelComboTarget, i)
end

function MacroForm_TargetDropDown_OnShow()
	UIDropDownMenu_Initialize(MacroFormOneLineTargetPanelComboTarget, MacroForm_TargetDropDown_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormOneLineTargetPanelComboTarget, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_TargetDropDown_Initialize()
	local i
	if(getn(MACROFORM_TARGET) > 0) then
		for i = 1, getn(MACROFORM_TARGET), 1 do
			info = {
				text = MACROFORM_TARGET[i],
				func = MacroForm_TargetDropdown_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

function MacroForm_SequenceTargetDropdown_OnClick(self)
  local i = self:GetID()
  MacroSequenceTargetSelected = MACROFORM_TARGET[i]
  UIDropDownMenu_SetSelectedID(MacroFormSequenceConditionComboTarget, i)
end

function MacroForm_SequenceTargetDropDown_OnShow()
	UIDropDownMenu_Initialize(MacroFormSequenceConditionComboTarget, MacroForm_SequenceTargetDropDown_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormSequenceConditionComboTarget, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_SequenceTargetDropDown_Initialize()
	local i
	if(getn(MACROFORM_TARGET) > 0) then
		for i = 1, getn(MACROFORM_TARGET), 1 do
			info = {
				text = MACROFORM_TARGET[i],
				func = MacroForm_SequenceTargetDropdown_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

--
--	** AGRESSION DROP DOWN **
--
function MacroForm_AgressionDropDown_OnClick(self)
  local i = self:GetID()
  MacroAgressionSelected = MACROFORM_AGRESSION[i]
  UIDropDownMenu_SetSelectedID(MacroFormOneLineTargetPanelComboAgression, i)
end

function MacroForm_AgressionDropDown_OnShow()
	UIDropDownMenu_Initialize(MacroFormOneLineTargetPanelComboAgression, MacroForm_AgressionDropDown_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormOneLineTargetPanelComboAgression, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_AgressionDropDown_Initialize()
	local i
	if(getn(MACROFORM_AGRESSION) > 0) then
		for i = 1, getn(MACROFORM_AGRESSION), 1 do
			info = {
				text = MACROFORM_AGRESSION[i],
				func = MacroForm_AgressionDropDown_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

function MacroForm_SequenceAgressionDropDown_OnClick(self)
  local i = self:GetID()
  MacroSequenceAgressionSelected = MACROFORM_AGRESSION[i]
  UIDropDownMenu_SetSelectedID(MacroFormSequenceConditionComboAgression, i)
end

function MacroForm_SequenceAgressionDropDown_OnShow()
	UIDropDownMenu_Initialize(MacroFormSequenceConditionComboAgression, MacroForm_SequenceAgressionDropDown_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormSequenceConditionComboAgression, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_SequenceAgressionDropDown_Initialize()
	local i
	if(getn(MACROFORM_AGRESSION) > 0) then
		for i = 1, getn(MACROFORM_AGRESSION), 1 do
			info = {
				text = MACROFORM_AGRESSION[i],
				func = MacroForm_SequenceAgressionDropDown_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

--
--	** DEAD DROP DOWN **
--
function MacroForm_Dead_OnClick(self)
  local i = self:GetID()
  MacroDeadSelected = MACROFORM_DEAD[i]
  UIDropDownMenu_SetSelectedID(MacroFormOneLineTargetPanelComboDead, i)
end

function MacroForm_Dead_OnShow()
	UIDropDownMenu_Initialize(MacroFormOneLineTargetPanelComboDead, MacroForm_Dead_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormOneLineTargetPanelComboDead, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_Dead_Initialize()
	local i
	if(getn(MACROFORM_DEAD) > 0) then
		for i = 1, getn(MACROFORM_DEAD), 1 do
			info = {
				text = MACROFORM_DEAD[i],
				func = MacroForm_Dead_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

function MacroForm_SequenceDead_OnClick(self)
  local i = self:GetID()
  MacroSequenceDeadSelected = MACROFORM_DEAD[i]
  UIDropDownMenu_SetSelectedID(MacroFormSequenceConditionComboDead, i)
end

function MacroForm_SequenceDead_OnShow()
	UIDropDownMenu_Initialize(MacroFormSequenceConditionComboDead, MacroForm_SequenceDead_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormSequenceConditionComboDead, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_SequenceDead_Initialize()
	local i
	if(getn(MACROFORM_DEAD) > 0) then
		for i = 1, getn(MACROFORM_DEAD), 1 do
			info = {
				text = MACROFORM_DEAD[i],
				func = MacroForm_SequenceDead_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

--
--	** STANCE DROP DOWN **
--
function MacroForm_StanceDropdown_OnClick(self)
  local i = self:GetID()
  MacroStanceSelected = MACROFORM_STANCE[i]
  UIDropDownMenu_SetSelectedID(MacroFormOneLineSelfPanelComboStance, i)
end

function MacroForm_StanceDropdown_OnShow()
	UIDropDownMenu_Initialize(MacroFormOneLineSelfPanelComboStance, MacroForm_StanceDropdown_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormOneLineSelfPanelComboStance, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_StanceDropdown_Initialize()
	local i
	if(getn(MACROFORM_STANCE) > 0) then
		for i = 1, getn(MACROFORM_STANCE), 1 do
			info = {
				text = MACROFORM_STANCE[i],
				func = MacroForm_StanceDropdown_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

--
--	** COMBAT DROP DOWN **
--
function MacroForm_CombatDropdown_OnClick(self)
  local i = self:GetID()
  MacroCombatSelected = MACROFORM_COMBAT[i]
  UIDropDownMenu_SetSelectedID(MacroFormOneLineSelfPanelComboCombat, i)
end

function MacroForm_ComboCombat_OnShow()
	UIDropDownMenu_Initialize(MacroFormOneLineSelfPanelComboCombat, MacroForm_CombatDropdown_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormOneLineSelfPanelComboCombat, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_CombatDropdown_Initialize()
	local i
	if(getn(MACROFORM_COMBAT) > 0) then
		for i = 1, getn(MACROFORM_COMBAT), 1 do
			info = {
				text = MACROFORM_COMBAT[i],
				func = MacroForm_CombatDropdown_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

--
--	** STEALTH DROP DOWN **
--
function MacroForm_StealthDropdown_OnClick(self)
  local i = self:GetID()
  MacroStealthSelected = MACROFORM_STEALTH[i]
  UIDropDownMenu_SetSelectedID(MacroFormOneLineSelfPanelComboStealth, i)
end

function MacroForm_StealthDropdown_OnShow()
	UIDropDownMenu_Initialize(MacroFormOneLineSelfPanelComboStealth, MacroForm_StealthDropdown_Initialize)
	UIDropDownMenu_SetSelectedID(MacroFormOneLineSelfPanelComboStealth, 1)
	--UIDropDownMenu_SetWidth(TacticatorDropDownInstance, 100)
end

function MacroForm_StealthDropdown_Initialize()
	local i
	if(getn(MACROFORM_STEALTH) > 0) then
		for i = 1, getn(MACROFORM_STEALTH), 1 do
			info = {
				text = MACROFORM_STEALTH[i],
				func = MacroForm_StealthDropdown_OnClick
			}
			UIDropDownMenu_AddButton(info)
			i = i + 1
		end
	end
end

function MacroForm_OnSpellDragStop(self)
  local itype, spellid, booktype = GetCursorInfo()
  if(itype == "spell") then
    local spellbutton = _G["MacroFormButtonSpell"]
    local oldspell = spellname
    spellname = GetSpellName(spellid, booktype)
    local obj = _G["MacroFormMacroEditBoxEdit"]
    local newtexture = GetSpellTexture(spellid, booktype)
    if(newtexture) then
      if(oldspell == nil) then
        spellbutton:SetHeight(32)
        spellbutton:SetWidth(32)
        local point, relativeTo, relativePoint, xOfs, yOfs  = spellbutton:GetPoint(1)
        spellbutton:ClearAllPoints()
        spellbutton:SetPoint(point, relativeTo, relativePoint, xOfs+16, yOfs-16)
        spellbutton:Show()
      end
      spellbutton:SetNormalTexture(newtexture)
    else
      obj:SetText("No texture found")
    end
  end
  ClearCursor()
end

function MacroForm_OnChannelingDragStop()
  local itype, spellid, booktype = GetCursorInfo()
  if(itype == "spell") then
    local channelingbutton = _G["MacroFormOneLineSelfPanelButtonChanneling"]
    local oldspell = channelingname
    channelingname = GetSpellName(spellid, booktype)
    local obj = _G["MacroFormMacroEditBoxEdit"]
    local newtexture = GetSpellTexture(spellid, booktype)
    if(newtexture) then
      if(oldspell == nil) then
        channelingbutton:SetHeight(32)
        channelingbutton:SetWidth(32)
        local point, relativeTo, relativePoint, xOfs, yOfs  = channelingbutton:GetPoint(1)
        channelingbutton:ClearAllPoints()
        channelingbutton:SetPoint(point, relativeTo, relativePoint, xOfs+16, yOfs-16)
        channelingbutton:Show()
      end
      channelingbutton:SetNormalTexture(newtexture)
    else
      obj:SetText("No texture found")
    end
  end
  ClearCursor()
end

function MacroForm_ResetSpellTexture()
  if(spellname) then
    spellname = nil
    local spellbutton = _G["MacroFormButtonSpell"]
    spellbutton:SetHeight(64)
    spellbutton:SetWidth(64)
    local point, relativeTo, relativePoint, xOfs, yOfs = spellbutton:GetPoint(1)
    spellbutton:ClearAllPoints()
    spellbutton:SetPoint(point, relativeTo, relativePoint, xOfs-16, yOfs+16)
    spellbutton:Show()
    spellbutton:SetNormalTexture("Interface\\Buttons\\UI-Quickslot2")
  end
end

function MacroForm_ResetChannelingTexture()
  if(channelingname) then
    channelingname = nil
    local channelingbutton = _G["MacroFormOneLineSelfPanelButtonChanneling"]
    channelingbutton:SetHeight(64)
    channelingbutton:SetWidth(64)
    local point, relativeTo, relativePoint, xOfs, yOfs = channelingbutton:GetPoint(1)
    channelingbutton:ClearAllPoints()
    channelingbutton:SetPoint(point, relativeTo, relativePoint, xOfs-16, yOfs+16)
    channelingbutton:Show()
    channelingbutton:SetNormalTexture("Interface\\Buttons\\UI-Quickslot2")
  end
end
