## Interface: 40000
## Title: Multi Raid Alert
## Version: 1.07
## Notes: Little window with many buttons that can fire commands, says, scripts and more. Fully Customizable
## Author: evil.oz
## SavedVariables: MultiRaidAlertData
## SavedVariablesPerCharacter: MultiRaidAlertOptions
MultiRaidAlert.xml