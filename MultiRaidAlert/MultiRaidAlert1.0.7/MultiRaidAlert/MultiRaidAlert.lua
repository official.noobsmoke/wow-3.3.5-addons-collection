--[[

*************************************************************************
* Multi Raid Alert 1.07
* by evil.oz (http://www.curse-gaming.com/en/profile-26630.html)
*
*************************************************************************

1.07
----
**updated:** toc
**fixes:** this/self behaviour
**fixes:** 4.01 fixes here and there

1.06
----
**updated:** toc

1.05
----
**fixes:** xml binding declarations cleanup
**fixes:** proper toc syntax

1.04
----
**updated:** toc
**updated:** font issues with 3.02
**changed:** default setup is g15-alike
**changed:** different button font, with zoom hover
**fixes:** some button changes due to API differences

1.03
-----
**bugfix:** keyboard binds now working properly
**added:** max num of sets is now 18 (for logitech g15 usage mainly)
**added:** button width is customizable

1.02
-----
**updated:** toc

1.01
-----
**added:** added "LOAD SET" type, you can swaps sets directly with a button now

1.00
-----
**removed:** cannot shift-click on items in bags, 2.0 restriction (must work on that)

0.99
-----
**updated:** toc
**fixes:** lua 5.1 adjustements

0.98
-----
**added:** frames are now clamped to screen (cannot drag em away)
**added:** pressing esc in edit window closes window (experimental)

0.97
-----
**added:** can shift-click on link/items in text fields, from both chat and bag
**bugfix:** tryed to fix disappearing set buttons

0.96 
----- 
**added:** slash commands are no more case sensitive (parameters are still) 
**bugfix:** deleting a set with an alt does no longer make mra crash on another alt 
**bugfix:** no more trailing and ending spaces when no prefix/suffix is given 

0.94 
----- 
**removed:** frame position is no more saved by addon, reverting back on wow-auto saving 
**changed:** options (not sets) are now per-character setting, this wil finally prevent frame to wander around the screen (i hope) 

0.92 
----- 
**added:** by popular request, -SCRIPT- type and -SLASH CMD- type buttons 
**changed:** increased max frame width 
**added:** sets can be put one after one now (even a single line with more sets) 
**bugfix:** dragging with RMB in quick selection set was causing an error, ok now 


0.91 
----- 
**added:** label width can be changed 
**added:** menu selection set now also popup rightclicking on buttons, not only on label (needed when label is not visible) 

0.90 
----- 
**added:** frame position is now saved, should no more wander around the screen after logging 
**bugfix:** frame border now alway hides after mouseout (hope) 
**bugfix:** opening options no more makes scaling slightly adjusted 
**added:** "/mra reset" reset also position now ( "/mra resetpos" is still there in case this fails ^^) 
**bugfix:** load error 

0.87 
---- 
WARNING: YOU MUST DO A mra /reset if you are upgrading from previous version!!!!!!!!!!! 
major update 
added: two sets can be active at same time 
added: quick set selection and ordering 
added: options frame 
added: button and frame resizing 
removed: some commandline commands 
removed: cannot shift-mousewheel in edit window no more, use topmost "..." button instead 
bugfix: internal memory optimizations 

0.51 
---- 
bugfix: adding party in 0.4 i removed channel support. now everything is fine /slap me 

0.5 
---- 
added scaling support (please report anomalies) 

0.4 
---- 
added party support (was ment to be in already, think to this as a bugfix) 

0.3 
---- 
initial public release

]]--


BINDING_HEADER_MultiRaidAlertCFG = "Multi Raid Alert";
BINDING_NAME_MultiRaidAlertTopButton1 = "Top Button n. 1";
BINDING_NAME_MultiRaidAlertTopButton2 = "Top Button n. 2";
BINDING_NAME_MultiRaidAlertTopButton3 = "Top Button n. 3";
BINDING_NAME_MultiRaidAlertTopButton4 = "Top Button n. 4";
BINDING_NAME_MultiRaidAlertTopButton5 = "Top Button n. 5";
BINDING_NAME_MultiRaidAlertTopButton6 = "Top Button n. 6";
BINDING_NAME_MultiRaidAlertTopButton7 = "Top Button n. 7";
BINDING_NAME_MultiRaidAlertTopButton8 = "Top Button n. 8";
BINDING_NAME_MultiRaidAlertTopButton9 = "Top Button n. 9";
BINDING_NAME_MultiRaidAlertTopButton10 = "Top Button n. 10";
BINDING_NAME_MultiRaidAlertTopButton11 = "Top Button n. 11";
BINDING_NAME_MultiRaidAlertTopButton12 = "Top Button n. 12";
BINDING_NAME_MultiRaidAlertTopButton13 = "Top Button n. 14";
BINDING_NAME_MultiRaidAlertTopButton14 = "Top Button n. 14";
BINDING_NAME_MultiRaidAlertTopButton15 = "Top Button n. 15";
BINDING_NAME_MultiRaidAlertTopButton16 = "Top Button n. 16";
BINDING_NAME_MultiRaidAlertTopButton17 = "Top Button n. 17";
BINDING_NAME_MultiRaidAlertTopButton18 = "Top Button n. 18";
BINDING_NAME_MultiRaidAlertButton1 = "Bottom Button n. 1";
BINDING_NAME_MultiRaidAlertButton2 = "Bottom Button n. 2";
BINDING_NAME_MultiRaidAlertButton3 = "Bottom Button n. 3";
BINDING_NAME_MultiRaidAlertButton4 = "Bottom Button n. 4";
BINDING_NAME_MultiRaidAlertButton5 = "Bottom Button n. 5";
BINDING_NAME_MultiRaidAlertButton6 = "Bottom Button n. 6";
BINDING_NAME_MultiRaidAlertButton7 = "Bottom Button n. 7";
BINDING_NAME_MultiRaidAlertButton8 = "Bottom Button n. 8";
BINDING_NAME_MultiRaidAlertButton9 = "Bottom Button n. 9";
BINDING_NAME_MultiRaidAlertButton10 = "Bottom Button n. 10";
BINDING_NAME_MultiRaidAlertButton11 = "Bottom Button n. 11";
BINDING_NAME_MultiRaidAlertButton12 = "Bottom Button n. 12";
BINDING_NAME_MultiRaidAlertButton13 = "Bottom Button n. 13";
BINDING_NAME_MultiRaidAlertButton14 = "Bottom Button n. 14";
BINDING_NAME_MultiRaidAlertButton15 = "Bottom Button n. 15";
BINDING_NAME_MultiRaidAlertButton16 = "Bottom Button n. 16";
BINDING_NAME_MultiRaidAlertButton17 = "Bottom Button n. 17";
BINDING_NAME_MultiRaidAlertButton18 = "Bottom Button n. 18";
BINDING_NAME_MultiRaidAlertToggle = "Show / Hide Multi Raid Alert";
MRA_ButtonGroupSpacer=25
MRA_EditButtonSpacing=18
MRA_MaxButtons=18
MRACountdown=0
MRA_FadeTime=60
MultiRaidAlertEditedSet=1

MRA_CHANNELCHOICE={
	"SAY",
	"GUILD",
	"PARTY",
	"RAID",
	"RAID WARNING",
	"CT_RAIDASSIST",
	"BATTLEGROUND",
	"CHANNEL1",
	"CHANNEL2",
	"CHANNEL3",
	"CHANNEL4",
	"CHANNEL5",
	"CHANNEL6",
	"CHANNEL7",
	"CHANNEL8",
	"CHANNEL9",
	"CHANNEL10",
	"SLASH CMD",
	"SCRIPT",
	"LOAD SET"
}


-- *************************************************************************
-- INITIALIZATION
-- *************************************************************************
function MultiRaidAlert_OnLoad(this) 

	this:RegisterEvent("VARIABLES_LOADED");
	this:RegisterForDrag("LeftButton");

	-- add the slash commands
	SlashCmdList["MRACMD"] = function(msg)
		MultiRaidAlert_cmd(msg);
	end
	SLASH_MRACMD1 = "/mra";
	SLASH_MRACMD2 = "/multiraidalert";

end

function MultiRaidAlertEdit_OnLoad(this) 
        this:RegisterForDrag("LeftButton");
end

function MultiRaidAlertOptions_OnLoad(this) 
        this:RegisterForDrag("LeftButton");
end


-- *************************************************************************
-- timed auto-hiding buttons code
-- *************************************************************************
function MultiRaidAlert_OnEnter()
    

	MRAOptionsButton:Show()

	if MultiRaidAlertData[MultiRaidAlertOptions.LastSet]["NOTOPSET"]~=1 then 
		MRAEditButton2:Show()
	end
	MRAEditButton1:Show()

	--make border visible if it is hidden
	if MultiRaidAlertOptions.CurrentBorderAlpha<.4 then
		local r,g,b,a=MultiRaidAlertFrame:GetBackdropBorderColor()	
		MultiRaidAlertFrame:SetBackdropBorderColor(r,g,b,1)
	end
	
end

function MultiRaidAlert_OnLeave()

	--start countdown
	MRAResetCountDown()

end

function MultiRaidAlert_OnUpdate()
	
	if MRACountdown>0 then
		MRACountdown=MRACountdown-1


		if MRACountdown == 0 then
			--stop countdown disablin OnEnter event
			--MultiRaidAlertFrame:SetScript("OnUpdate", nil)

			MRAOptionsButton:Hide()
			MRAEditButton1:Hide()	
			MRAEditButton2:Hide()

			--reset frame alpha		
			local r,g,b,a=MultiRaidAlertFrame:GetBackdropBorderColor()	
			MultiRaidAlertFrame:SetBackdropBorderColor(r,g,b,MultiRaidAlertOptions.CurrentBorderAlpha)
		end
	end
end

function MRAResetCountDown()
	--called by some buttons in xml to give user some more time
	MRACountdown=MRA_FadeTime
	MultiRaidAlertFrame:SetScript("OnUpdate", function() MultiRaidAlert_OnUpdate() end)

end

-- *************************************************************************
-- EVENT HANDLER (init after var loads for now)
-- *************************************************************************
function MultiRaidAlert_OnEvent(event)

	if (event == "VARIABLES_LOADED") then
	        MRAPrintout("MultiRaidAlert by evil.oz", 1.0, 1.0, 0.0);
		InitVars()

		MRAHookFunctions()

		if MultiRaidAlertOptions.Visible then
			MultiRaidAlertFrame:Show()
		else
			MultiRaidAlertFrame:Hide()	
			MRAPrintout("panel is hidden, type /mra toggle to enable it", 1.0, 1.0, 0.0);
		end

		MultiRaidAlertFrame:SetScale(MultiRaidAlertOptions.CurrentScale)

		UpdateGrid()
	end
end


-- *************************************************************************
-- HOOK ITEM LINKING FUNCTIONS
-- *************************************************************************
function MRAHookFunctions()

	MRA_OriginalSetItemRef = SetItemRef;
	SetItemRef = MRA_SetItemRef

	--disabled for 2.0 compatibility :(

	--MRA_OriginalContainerFrameItemButton_OnClick = ContainerFrameItemButton_OnClick;
	--ContainerFrameItemButton_OnClick = MRA_ContainerFrameItemButton_OnClick

end

-- WHEN SHIFT-CLICKED IN CHAT
function MRA_SetItemRef(link, text, button)

	if MRACurrentFocus and IsShiftKeyDown() then
		MRACurrentFocus:Insert(text)
	else
		MRA_OriginalSetItemRef(link, text, button)
	end
end

-- WHEN SHIFT-CLICKED IN BAG
function MRA_ContainerFrameItemButton_OnClick(hookParams, returnValue, button, ignoreShift) 

	if MRACurrentFocus and IsShiftKeyDown() then
		MRACurrentFocus:Insert(GetContainerItemLink(this:GetParent():GetID(), this:GetID()))
	else
		MRA_OriginalContainerFrameItemButton_OnClick(hookParams, returnValue, button, ignoreShift) 
	end
end

-- *************************************************************************
-- WHEN BUTTON PRESSED
-- *************************************************************************
function MultiRaidAlert_Button(arg, type, button, control)

	if button=="LeftButton" then
		MultiRaidAlert_DoMessage(arg,type)
	else
		if type==0 then
			MRA_MenuMode=1
		else	
			MRA_MenuMode=2
		end
		MRAResetCountDown()
		MRAInitMenu(control)
	end

end

function MultiRaidAlert_DoMessage(arg,type)

	local text, mode, set, n, spacepos, fff, parm

	if type==0 then
		set=MultiRaidAlertOptions.LastTopSet
	else	
		set=MultiRaidAlertOptions.LastSet
	end

	text=MultiRaidAlertData[set]["MSG"][arg][2]

	if MultiRaidAlertData[set]["LEFTBRACKETS"] ~= "" then
		text=MultiRaidAlertData[set]["LEFTBRACKETS"].." "..text
	end
	if MultiRaidAlertData[set]["RIGHTBRACKETS"] ~= "" then
		text=text.." "..MultiRaidAlertData[set]["RIGHTBRACKETS"]
	end

	--convert %t in target name for ct_Raid
	for n=1, string.len(text) do
		if string.sub(text,n,n)=="%" then
			if string.sub(text,n+1,n+1)=="t" then
				if UnitName("target") then
					text=string.sub(text,1,n-1).."<"..UnitName("target")..">"..string.sub(text,n+2)
				else
					text=string.sub(text,1,n-1).."<target>"..string.sub(text,n+2)
				end
				do break end
			end
		end
	end

	if MultiRaidAlertData[set]["MSG"][arg][4] then
		mode=MultiRaidAlertData[set]["MSG"][arg][4]
	else
		mode="RAID_WARNING"
	end

	if mode=="RAID WARNING" then
		SendChatMessage(text,"RAID_WARNING");
	end

	if string.sub(mode, 1,7) =="CHANNEL" then
		SendChatMessage(text,"CHANNEL", nil,tonumber(string.sub(mode, 8)));
	end

	if mode=="GUILD" then
		SendChatMessage(text,"GUILD");
	end
	if mode=="RAID" then
		SendChatMessage(text,"RAID");
	end
	if mode=="PARTY" then
		SendChatMessage(text,"PARTY");
	end
	if mode=="SAY" then
		SendChatMessage(text,"SAY");
	end
	if mode=="BATTLEGROUND" then
		SendChatMessage(text,"BATTLEGROUND");
	end
	if mode=="CT_RAIDASSIST" then
	
		if ( GetNumRaidMembers() ~= 0 ) then 
			msg= "MS "..text
			SendAddonMessage("CTRA", msg, "RAID");
		end
		SendChatMessage(text,"RAID");
	end
	if mode=="SLASH CMD" then
		
		text=MultiRaidAlertData[set]["MSG"][arg][2]
		
		--separate body from parameters here
		spacepos=string.find(text, " ")
		if spacepos then
			parm=string.sub(text,spacepos+1)
			text=string.sub(text,1,spacepos-1)
		end

		--resolve function name		
		fff=MRA_ReverseSolveSlashCommand(text)
		if fff then
			local a,b=pcall(SlashCmdList[fff], parm)
			if not a then
				MRAPrintout("Running slashcommand: <"..text.."> failed ("..b..")")
			end
		else
			MRAPrintout("Slashcommand: <"..text.."> not found")
		end
	end	
	if mode=="SCRIPT" then

		text=MultiRaidAlertData[set]["MSG"][arg][2]

		--MRAPrintout("Running : <"..text..">")
		RunScript(text)
	end

	if mode=="LOAD SET" then
		local found=false
	
		--"text" already contains set name, but with traling "==="
		text=MultiRaidAlertData[set]["MSG"][arg][2]


		for n,m in pairs(MultiRaidAlertData) do
			if (m.NAME==text) then
				if type==0 then
					MultiRaidAlertOptions.LastTopSet=n
				else	
					MultiRaidAlertOptions.LastSet=n
				end
				UpdateGrid()
				found=true
			end
		end

		if not found then
			MRAPrintout("Cannot load set <"..text..">")
		end
	end


end

-- *************************************************************************
-- REDRAW OPTIONS FRAME
-- *************************************************************************
function UpdateOptionsGrid()

		MRAScaleSliderHigh:SetText("max");
		MRAScaleSliderLow:SetText("min");
		MRAScaleSlider:SetMinMaxValues(0.5,2);
		MRAScaleSlider:SetValueStep(0.05);
		MRAScaleSlider:SetValue(MultiRaidAlertOptions.CurrentScale)
		MRAScaleSliderText:SetText("scale");
		MultiRaidAlertChangeScale()

		MRAWidthSliderHigh:SetText("max");
		MRAWidthSliderLow:SetText("min");
		MRAWidthSlider:SetMinMaxValues(100,1500);
		MRAWidthSlider:SetValueStep(20);
		MRAWidthSlider:SetValue(MultiRaidAlertOptions.CurrentWidth)
		MRAWidthSliderText:SetText("frame width");
		MultiRaidAlertChangeWidth()

		MRAMarginSliderHigh:SetText("max");
		MRAMarginSliderLow:SetText("min");
		MRAMarginSlider:SetMinMaxValues(2,30);
		MRAMarginSlider:SetValueStep(1);
		MRAMarginSlider:SetValue(MultiRaidAlertOptions.CurrentMargin)
		MRAMarginSliderText:SetText("frame margin");

		MRAAlphaSliderHigh:SetText("max");
		MRAAlphaSliderLow:SetText("min");
		MRAAlphaSlider:SetMinMaxValues(0,1);
		MRAAlphaSlider:SetValueStep(.1);
		MRAAlphaSlider:SetValue(MultiRaidAlertOptions.CurrentAlpha)
		MRAAlphaSliderText:SetText("bg alpha");

		MRABorderAlphaSliderHigh:SetText("max");
		MRABorderAlphaSliderLow:SetText("min");
		MRABorderAlphaSlider:SetMinMaxValues(0,1);
		MRABorderAlphaSlider:SetValueStep(.1);
		MRABorderAlphaSlider:SetValue(MultiRaidAlertOptions.CurrentAlpha)
		MRABorderAlphaSliderText:SetText("border alpha");

		MRAButtonWidthSliderHigh:SetText("max");
		MRAButtonWidthSliderLow:SetText("min");
		MRAButtonWidthSlider:SetMinMaxValues(40,300);
		MRAButtonWidthSlider:SetValueStep(20);
		MRAButtonWidthSlider:SetValue(MultiRaidAlertOptions.ButtonWidth)
		MRAButtonWidthSliderText:SetText("button width");

		MRAButtonHeightSliderHigh:SetText("max");
		MRAButtonHeightSliderLow:SetText("min");
		MRAButtonHeightSlider:SetMinMaxValues(6,36);
		MRAButtonHeightSlider:SetValueStep(2);
		MRAButtonHeightSlider:SetValue(MultiRaidAlertOptions.ButtonHeight)
		MRAButtonHeightSliderText:SetText("button height");


		MRAButtonSpaceSliderHigh:SetText("max");
		MRAButtonSpaceSliderLow:SetText("min");
		MRAButtonSpaceSlider:SetMinMaxValues(2,40);
		MRAButtonSpaceSlider:SetValueStep(1);
		MRAButtonSpaceSlider:SetValue(MultiRaidAlertOptions.ButtonSpace)
		MRAButtonSpaceSliderText:SetText("Button Spacing");


		MRAGroupSpaceSliderHigh:SetText("max");
		MRAGroupSpaceSliderLow:SetText("min");
		MRAGroupSpaceSlider:SetMinMaxValues(2,40);
		MRAGroupSpaceSlider:SetValueStep(1);
		MRAGroupSpaceSlider:SetValue(MultiRaidAlertOptions.GroupSpace)
		MRAGroupSpaceSliderText:SetText("Group Spacing");
end


-- *************************************************************************
-- REDRAW EDIT FRAME
-- *************************************************************************
function UpdateEditGrid()

	local buttonwidth, n, button

	local Set=MultiRaidAlertEditedSet

	if not(MultiRaidAlertData[Set]) then
		for n=1,table.getn(MultiRaidAlertData) do
			if MultiRaidAlertData[n] then
				Set=n
				do break end
			end
		end
	end

	local buttons=table.getn(MultiRaidAlertData[Set]["MSG"])

	getglobal("MRASetName"):SetText(MultiRaidAlertData[Set]["NAME"])
	getglobal("MRALeftBrackets"):SetText(MultiRaidAlertData[Set]["LEFTBRACKETS"])
	getglobal("MRARightBrackets"):SetText(MultiRaidAlertData[Set]["RIGHTBRACKETS"])

	--size button label
	buttonwidth="FULL"
	MRAButtonLabelSize:Show()
	if MultiRaidAlertData[Set]["LabelSize"] == 2 then
		buttonwidth="HALF"		
	end
	if MultiRaidAlertData[Set]["LabelSize"] == 3 then
		buttonwidth="1/3"		
	end
	if MultiRaidAlertData[Set]["LabelSize"] == 4 then
		buttonwidth="HIDE"		
	end
	MRAButtonLabelSize:SetText(buttonwidth)

	-- prevbutton= countdown from 3rd to 1st button-in-a-row
	local prevbutton=1
	
	for button=1, buttons do
		getglobal("MRAButtonCaption"..button):Show()
		getglobal("MRAButtonString"..button):Show()
		getglobal("MRAButtonChannel"..button):Show()

		--position of widgets
		off=6+button*MRA_EditButtonSpacing
		getglobal("MRAButtonCaption"..button):ClearAllPoints()
		getglobal("MRAButtonCaption"..button):SetPoint("TOPLEFT" , "MultiRaidAlertLabel1" , "TOPLEFT" , 0, -off) 
		getglobal("MRAButtonString"..button):ClearAllPoints()
		getglobal("MRAButtonString"..button):SetPoint("TOPLEFT" , "MultiRaidAlertLabel2" , "TOPLEFT" , 0, -off) 
		getglobal("MRAButtonChannel"..button):ClearAllPoints()
		getglobal("MRAButtonChannel"..button):SetPoint("TOPLEFT" , "MultiRaidAlertLabel3" , "TOPLEFT" , 0, -off+4) 
		getglobal("MRAButtonSize"..button):ClearAllPoints()
		getglobal("MRAButtonSize"..button):SetPoint("TOPLEFT" , "MultiRaidAlertLabel4" , "TOPLEFT" , 0, -off+5)
		getglobal("MRAButtonCheckSpacing"..button):ClearAllPoints()
		getglobal("MRAButtonCheckSpacing"..button):SetPoint("TOPLEFT" , "MultiRaidAlertLabel5" , "TOPLEFT" , 0, -off-2) 
 		
		--label on channel button
		if MultiRaidAlertData[Set]["MSG"][button][4] then
			getglobal("MRAButtonChannel"..button):SetText(MultiRaidAlertData[Set]["MSG"][button][4])
		end


		--setsize button label
		buttonwidth="FULL"
		getglobal("MRAButtonSize"..button):Show()
		if MultiRaidAlertData[Set]["MSG"][button][5] == 2 then
			buttonwidth="HALF"		
		end
		if MultiRaidAlertData[Set]["MSG"][button][5] == 3 then
			buttonwidth="1/3"		
		end
		getglobal("MRAButtonSize"..button):SetText(buttonwidth)

		getglobal("MRAButtonCheckSpacing"..button):Show() 

		getglobal("MRAButtonCaption"..button):SetText(MultiRaidAlertData[Set]["MSG"][button][1])
		getglobal("MRAButtonString"..button):SetText(MultiRaidAlertData[Set]["MSG"][button][2])
		
		--set value of check-button
		getglobal("MRAButtonCheckSpacing"..button):SetChecked(MultiRaidAlertData[Set]["MSG"][button][3])

		
		if (buttons==1) then
			getglobal("MRAEditMinus"..button):Hide()
		else
			getglobal("MRAEditMinus"..button):Show()
		end
		
		if (button ~= MRA_MaxButtons) then 
			if (buttons ~= MRA_MaxButtons) then
				getglobal("MRAEditPlus"..button):Show()
			else
				getglobal("MRAEditPlus"..button):Hide()	
			end
		end
	end

	-- if not all buttons visible, show firstmost "+" button
	if (buttons ~= MRA_MaxButtons) then
		getglobal("MRAEditPlus0"):Show()
	else
		getglobal("MRAEditPlus0"):Hide()	
	end

	--and hide others
	for button=buttons+1,MRA_MaxButtons do
		getglobal("MRAButtonCaption"..button):Hide()
		getglobal("MRAButtonString"..button):Hide()
		getglobal("MRAButtonChannel"..button):Hide()
		getglobal("MRAButtonCheckSpacing"..button):Hide()	
		getglobal("MRAEditMinus"..button):Hide()
		getglobal("MRAButtonSize"..button):Hide()
		if button ~= MRA_MaxButtons then
			getglobal("MRAEditPlus"..button):Hide()	
		end
	end

	-- hide last check (if more than 1 button)
	if buttons > 0 then
		getglobal("MRAButtonCheckSpacing"..table.getn(MultiRaidAlertData[Set]["MSG"])):Hide()		
	end

	-- load checbutton status
	MRAButtonCheckTopSet:SetChecked(MultiRaidAlertData[Set]["NOTOPSET"])

	-- set Edit frame height
	MultiRaidAlertEditFrame:SetHeight(buttons*20+120)

	if table.getn(MultiRaidAlertData) == 1 then
		MRARemoveSetButton:Disable()
		MRABrowseSetButton:Disable()
	else
		MRARemoveSetButton:Enable()
		MRABrowseSetButton:Enable()
	end

	if table.getn(MultiRaidAlertData) == 15 then
		MRAAddSetButton:Disable()
	else
		MRAAddSetButton:Enable()
	end		
end

-- *************************************************************************
-- SLIDER EVENTS
-- *************************************************************************



function MultiRaidAlertChangeButtonSpace()
	MultiRaidAlertOptions.ButtonSpace=MRAButtonSpaceSlider:GetValue()
	UpdateGrid()
end

function MultiRaidAlertChangeGroupSpace()
	MultiRaidAlertOptions.GroupSpace=MRAGroupSpaceSlider:GetValue()
	UpdateGrid()
end

function MultiRaidAlertChangeButtonWidth()
	MultiRaidAlertOptions.ButtonWidth=MRAButtonWidthSlider:GetValue()
	UpdateGrid()
end

function MultiRaidAlertChangeButtonHeight()
	MultiRaidAlertOptions.ButtonHeight=MRAButtonHeightSlider:GetValue()
	UpdateGrid()
end

function MultiRaidAlertChangeScale()
	MultiRaidAlertOptions.CurrentScale=MRAScaleSlider:GetValue()
	MRA_RescaleFrame()
	UpdateGrid()
end

function MultiRaidAlertChangeWidth()
	MultiRaidAlertOptions.CurrentWidth = MRAWidthSlider:GetValue()
	UpdateGrid()
end

function MultiRaidAlertChangeMargin()
	MultiRaidAlertOptions.CurrentMargin = MRAMarginSlider:GetValue()
	UpdateGrid()
end

function MultiRaidAlertChangeAlpha()
	MultiRaidAlertOptions.CurrentAlpha = MRAAlphaSlider:GetValue()
	UpdateGrid()
end

function MultiRaidAlertChangeBorderAlpha()
	MultiRaidAlertOptions.CurrentBorderAlpha = MRABorderAlphaSlider:GetValue()
	UpdateGrid()
end

function MRAButtonCheckLinkedSets()
	MultiRaidAlertOptions.LinkedSets = MRAButtonLinkedSets:GetChecked()
	UpdateGrid()
end

function MRA_RescaleFrame()

	local scale=MultiRaidAlertOptions.CurrentScale
	local oldscale = MultiRaidAlertFrame:GetScale()
	local framex = (MultiRaidAlertFrame:GetLeft())* oldscale
	local framey = (MultiRaidAlertFrame:GetTop())* oldscale

	MultiRaidAlertFrame:SetScale(MultiRaidAlertOptions.CurrentScale)
	MultiRaidAlertFrame:ClearAllPoints()
	MultiRaidAlertFrame:SetPoint("TOPLEFT","UIParent","BOTTOMLEFT",framex/scale,framey/scale)

end
-- *************************************************************************
-- pressed "-"
-- *************************************************************************
function MRAEditRemoveButton(arg)

	--confirmation needed

	if table.getn(MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"])>1 then

		table.remove(MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"],arg)
		UpdateEditGrid()
		UpdateGrid()
	end
end


-- *************************************************************************
-- pressed "+"
-- *************************************************************************
function MRAEditAddButton(arg)

	if table.getn(MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"])<MRA_MaxButtons then
		new={"LABEL","TEXT", false , "RAID", 1}	
		table.insert(MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"], arg+1, new)

		UpdateEditGrid()
		UpdateGrid()
	end
end


-- *************************************************************************
-- "REMOVE SET" PRESSED
-- *************************************************************************
function MRARemoveSet()

	if table.getn(MultiRaidAlertData)>1 then
		table.remove(MultiRaidAlertData,MultiRaidAlertEditedSet)

		if not MultiRaidAlertData[MultiRaidAlertEditedSet] then
			MultiRaidAlertEditedSet=MultiRaidAlertEditedSet-1
		end

		if not MultiRaidAlertData[MultiRaidAlertOptions.LastTopSet] then
			MultiRaidAlertOptions.LastTopSet=MultiRaidAlertOptions.LastTopSet-1
		end

		if not MultiRaidAlertData[MultiRaidAlertOptions.LastSet] then
			MultiRaidAlertOptions.LastSet=MultiRaidAlertOptions.LastSet-1
		end

		UpdateEditGrid()
		UpdateGrid()
	end		
end

-- *************************************************************************
-- "NEW SET" PRESSED
-- *************************************************************************
function MRAAddSet()

	if table.getn(MultiRaidAlertData)<15 then
	
		local new

		new={	["NAME"]="SET n."..random(1000,9999),
	                ["MSG"] = {	{"B1","text1", false, "RAID", 1},
					{"B2","text2", false, "RAID", 1}
				},
			["LEFTBRACKETS"]="===",["RIGHTBRACKETS"]="==="
		    }
						
		table.insert(MultiRaidAlertData, MultiRaidAlertEditedSet, new)

		new={}		
	
		UpdateEditGrid()
		UpdateGrid()
	end	
end

-- *************************************************************************
-- SET HAS BEN RENAMED
-- *************************************************************************
function MRASetNameChanged()
	
	if MultiRaidAlertData[MultiRaidAlertEditedSet] then
		MultiRaidAlertData[MultiRaidAlertEditedSet]["NAME"]=getglobal("MRASetName"):GetText()
		UpdateGrid()
	end
end

-- *************************************************************************
-- BRACKETS HAS BEEN UPDATED
-- *************************************************************************
function MRASetBracketsChanged()

	MultiRaidAlertData[MultiRaidAlertEditedSet]["LEFTBRACKETS"]=getglobal("MRALeftBrackets"):GetText()
	MultiRaidAlertData[MultiRaidAlertEditedSet]["RIGHTBRACKETS"]=getglobal("MRARightBrackets"):GetText()
end

-- *************************************************************************
-- update button row
-- *************************************************************************
function MRAButtonCaptionUpdate(button)

	if MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button] then

		MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button][1]=getglobal("MRAButtonCaption"..button):GetText()
		MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button][2]=getglobal("MRAButtonString"..button):GetText()
		MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button][4]=getglobal("MRAButtonChannel"..button):GetText()
		MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button][3]=getglobal("MRAButtonCheckSpacing"..button):GetChecked()
	
		UpdateGrid()
	end
end

-- *************************************************************************
-- check pressed
-- *************************************************************************
function MRAButtonCheckTopSetUpdate(this)

	MultiRaidAlertData[MultiRaidAlertEditedSet]["NOTOPSET"] = this:GetChecked()
	UpdateGrid()
	
end

-- *************************************************************************
-- pressed Size button
-- *************************************************************************
function MRAButtonSizeButton(button)

	local mode

	if MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button][5] then
		mode=MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button][5]
	else
		mode=1
	end

	mode=mode+1
	if mode==4 then 
		mode=1 
	end

	MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button][5]=mode

	UpdateEditGrid()
	UpdateGrid()	
end	

-- *************************************************************************
-- pressed Label Size button
-- *************************************************************************
function MRAButtonLabelSizeButton()

	local mode

	if MultiRaidAlertData[MultiRaidAlertEditedSet]["LabelSize"] then
		mode=MultiRaidAlertData[MultiRaidAlertEditedSet]["LabelSize"]
	else
		mode=1
	end

	mode=mode+1
	if mode==5 then 
		mode=1 
	end

	MultiRaidAlertData[MultiRaidAlertEditedSet]["LabelSize"]=mode

	UpdateEditGrid()
	UpdateGrid()	
end	


-- *************************************************************************
-- pressed channel button
-- *************************************************************************
function MRAButtonChannelButton(button,arg1)

	local k,x,y,text
	local label=getglobal("MRAButtonChannel"..button):GetText()
	local found=false
	local shift=IsShiftKeyDown()
	
	for n=1,table.getn(MRA_CHANNELCHOICE) do
		if (MRA_CHANNELCHOICE[n]==label) then
			found=true

			if arg1=="LeftButton" then
				if n==table.getn(MRA_CHANNELCHOICE) then
					k=1
				else
					k=n+1
				end
			end
			if arg1=="RightButton" then
				if n==1 then
					k=table.getn(MRA_CHANNELCHOICE)
				else
					k=n-1
				end
			end

			text=MRA_CHANNELCHOICE[k]

			if shift then
				x=1
				y=table.getn(MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"])
			else
				x=button
				y=button
			end

			for but=x,y do
				getglobal("MRAButtonChannel"..but):SetText(text)
				MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][but][4]=text
			end
			
		end
	end

	if not found then
		text="SAY"
		getglobal("MRAButtonChannel"..button):SetText(text)
		MultiRaidAlertData[MultiRaidAlertEditedSet]["MSG"][button][4]=text

	end
end



-- *************************************************************************
-- clicked on label
-- *************************************************************************
function MRALabelClick(label,arg, control)

	if arg== "RightButton" then
		MRAResetCountDown()
		MRA_MenuMode=label
		MRAInitMenu(control)
	end
end

-- *************************************************************************
-- set-selection-menu-buttons actions
-- *************************************************************************
function MRAMenuClick(button)

	MultiRaidAlertMenuFrame:Hide()

	if MRA_MenuMode==1 then
		MultiRaidAlertOptions.LastTopSet=button	
		UpdateGrid()
	end

	if MRA_MenuMode==2 then
		MultiRaidAlertOptions.LastSet=button			
		UpdateGrid()
	end

	if MRA_MenuMode==3 then
		MultiRaidAlertEditedSet=button			
		UpdateEditGrid()
	end
end


-- *************************************************************************
-- initialize set-selection menu
-- *************************************************************************
function MRAInitMenu(control)

	local button, set, align,alignframe
	local MRAMenuMargin=20

	--MRA_MenuMode = 1   > called from top label
	--MRA_MenuMode = 2   > called from bottom label
	--MRA_MenuMode = 3   > called from "..." button in edit frame

	if MRA_MenuMode==1 then
		set=MultiRaidAlertOptions.LastTopSet
	end

	if MRA_MenuMode==2 then
		set=MultiRaidAlertOptions.LastSet	
	end

	if MRA_MenuMode==3 then
		set=MultiRaidAlertEditedSet		
	end

	for n=1,15 do
		button=getglobal("MultiRaidAlertMenu"..n)
		if n<= getn(MultiRaidAlertData) then

			button:ClearAllPoints()
			button:SetPoint("TOPLEFT" , "MultiRaidAlertMenuFrame" , "TOPLEFT" , MRAMenuMargin, -((20*(n-1))+MRAMenuMargin)) 
			button:SetText(MultiRaidAlertData[n]["NAME"])
			button:Show()
			if set==n then
				button:SetButtonState("PUSHED") 
				button:Disable()
			else
				button:SetButtonState("NORMAL") 
				button:Enable()
			end
		else
			button:Hide()			

		end
	end


	if control then
		--move frame according to screen postion
		--only if called with parameter, else refresh buttons only
		if control:GetTop() > 400 then
			align="TOP"
		else
			align="BOTTOM"
		end
	
		if control:GetLeft() > 500 then
			align=align.."RIGHT"
		else
			align=align.."LEFT"
		end
	
		MultiRaidAlertMenuFrame:ClearAllPoints()
		MultiRaidAlertMenuFrame:SetPoint(align , control , "TOPLEFT" , control:GetWidth()/2, -10) 
	end

	MultiRaidAlertMenuFrame:SetHeight(20*(getn(MultiRaidAlertData))+2*MRAMenuMargin)
	MultiRaidAlertMenuFrame:SetWidth(MultiRaidAlertMenu1:GetWidth()+2*MRAMenuMargin)


	MultiRaidAlertMenuFrame:Show()
end

-- *************************************************************************
-- click on side buttons
-- *************************************************************************
function MultiRaidAlert_OptionsPressed()

	MultiRaidAlertOptionsFrame:Show()
	UpdateOptionsGrid()
end

function MultiRaidAlert_EditPressed(frame)

	if frame==1 then
		MultiRaidAlertEditedSet=MultiRaidAlertOptions.LastSet
	end

	if frame==2 then
		MultiRaidAlertEditedSet=MultiRaidAlertOptions.LastTopSet
	end

	MultiRaidAlertEditFrame:Show()
	UpdateEditGrid()
end

-- *************************************************************************
-- REDRAW BUTTON ARRAY
-- *************************************************************************
function UpdateGrid()


	--resize
	MultiRaidAlertFrame:SetWidth(MultiRaidAlertOptions.CurrentWidth + 2*MultiRaidAlertOptions.CurrentMargin)
	--alpha
	local r,g,b,a=MultiRaidAlertFrame:GetBackdropBorderColor()
	MultiRaidAlertFrame:SetBackdropBorderColor(r,g,b,MultiRaidAlertOptions.CurrentBorderAlpha)
	MultiRaidAlertFrameColor:SetTexture(0,0,0,MultiRaidAlertOptions.CurrentAlpha)


	
	local Height, w, pos, buttons, button, newpos, lastbuttonspacer
	local LastSet=MultiRaidAlertOptions.LastSet
	local LastTopSet=MultiRaidAlertOptions.LastTopSet
	local notopset=false
	local MaxWidth=MultiRaidAlertOptions.CurrentWidth
	
	if MultiRaidAlertData[LastSet]["NOTOPSET"]==1 then 
		notopset=true
	end
		
	MultiRaidAlertTitle1:Hide()
	Height=MultiRaidAlertOptions.CurrentMargin
	pos=0	
	lastbuttonspacer=false

	--top first

	if MultiRaidAlertData[LastTopSet] and not (notopset) then

		w=MultiRaidAlertData[LastTopSet]["LabelSize"]
		if not w then w=1 end
		if w==4 then 
			--hide button
			MultiRaidAlertTitle1:Hide()

		else
			w=math.floor(MultiRaidAlertOptions.ButtonWidth/w)
		
			MultiRaidAlertTitle1:SetText(MultiRaidAlertData[LastTopSet]["NAME"])
			MultiRaidAlertTitle1:ClearAllPoints()
			MultiRaidAlertTitle1:SetPoint("TOPLEFT" , "MultiRaidAlertFrame" , "TOPLEFT" ,MultiRaidAlertOptions.CurrentMargin, -Height) 
			MultiRaidAlertTitle1:Show()
			MultiRaidAlertTitle1:SetWidth(w)
			MultiRaidAlertTitle1:SetHeight(MultiRaidAlertOptions.ButtonHeight)
	
			pos=pos+MultiRaidAlertTitle1:GetWidth()
	
			if pos >= MaxWidth then
				pos=0
				Height=Height+MultiRaidAlertOptions.ButtonSpace
			end
		end

		buttons=table.getn(MultiRaidAlertData[LastTopSet]["MSG"])

		for button=1, table.getn(MultiRaidAlertData[LastTopSet]["MSG"]) do
			getglobal("MRAButTop"..button):Show()
			getglobal("MRAButTop"..button):SetText(MultiRaidAlertData[LastTopSet]["MSG"][button][1])
			getglobal("MRAButTop"..button):SetButtonState("NORMAL")


			--set button width
			w=MultiRaidAlertData[LastTopSet]["MSG"][button][5]
			if not w then w=1 end
			w=math.floor(MultiRaidAlertOptions.ButtonWidth/w)

			getglobal("MRAButTop"..button):SetWidth(w)
			getglobal("MRAButTop"..button):SetHeight(MultiRaidAlertOptions.ButtonHeight)

			--new pos= where out button will be placed
			newpos=pos+w			

			if (newpos-5) >= MaxWidth then
				--new row, we got over margin
				pos=0
				Height=Height+MultiRaidAlertOptions.ButtonSpace
				if lastbuttonspacer then
					Height=Height+MultiRaidAlertOptions.GroupSpace
				end
			else
				--continuing on same row
				if lastbuttonspacer then
					pos=pos+MultiRaidAlertOptions.ButtonSpace
				end
			end
			
			--position button
			getglobal("MRAButTop"..button):ClearAllPoints()
			getglobal("MRAButTop"..button):SetPoint("TOPLEFT" , "MultiRaidAlertFrame" , "TOPLEFT" ,MultiRaidAlertOptions.CurrentMargin+pos, -Height) 
			pos=pos+w

			if  MultiRaidAlertData[LastTopSet]["MSG"][button][3]==1 then
				lastbuttonspacer=true			
			else	
				lastbuttonspacer=false
			end
		end
		--and hide others
		for button=buttons+1,MRA_MaxButtons do
			getglobal("MRAButTop"..button):Hide()	
		end

		if not MultiRaidAlertOptions.LinkedSets then
			Height=Height+MRA_ButtonGroupSpacer
		end
	else
		-- hide all buttons
		for button=1,MRA_MaxButtons do
			getglobal("MRAButTop"..button):Hide()	
		end
	end	


	--then normal

	lastbuttonspacer=false

	if MultiRaidAlertOptions.LinkedSets then

	else
		pos=0
	end



	if MultiRaidAlertData[LastSet] then


		w=MultiRaidAlertData[LastSet]["LabelSize"]
		if not w then w=1 end
		if w==4 then 
			--hide button
			MultiRaidAlertTitle2:Hide()

		else
			w=math.floor(MultiRaidAlertOptions.ButtonWidth/w)

			newpos=pos+w
			if (newpos-5) >= MaxWidth then
				--new row, we got over margin
				pos=0
				Height=Height+MultiRaidAlertOptions.ButtonSpace
			end
		
			MultiRaidAlertTitle2:SetText(MultiRaidAlertData[LastSet]["NAME"])
			MultiRaidAlertTitle2:ClearAllPoints()
			MultiRaidAlertTitle2:SetPoint("TOPLEFT" , "MultiRaidAlertFrame" , "TOPLEFT" ,MultiRaidAlertOptions.CurrentMargin+pos, -Height) 
			MultiRaidAlertTitle2:Show()
			MultiRaidAlertTitle2:SetWidth(w)
			MultiRaidAlertTitle2:SetHeight(MultiRaidAlertOptions.ButtonHeight)
	
			pos=pos+MultiRaidAlertTitle2:GetWidth()
	
			if pos >= MaxWidth then
				pos=0
				Height=Height+MultiRaidAlertOptions.ButtonSpace
			end
		end

		buttons=table.getn(MultiRaidAlertData[LastSet]["MSG"])

		for button=1, table.getn(MultiRaidAlertData[LastSet]["MSG"]) do
			getglobal("MRABut"..button):Show()
			getglobal("MRABut"..button):SetText(MultiRaidAlertData[LastSet]["MSG"][button][1])
			getglobal("MRABut"..button):SetButtonState("NORMAL")
			
			--set button width
			w=MultiRaidAlertData[LastSet]["MSG"][button][5]
			if not w then w=1 end
			w=math.floor(MultiRaidAlertOptions.ButtonWidth/w)

			getglobal("MRABut"..button):SetWidth(w)
			getglobal("MRABut"..button):SetHeight(MultiRaidAlertOptions.ButtonHeight)

			--new pos= where out button will be placed
			newpos=pos+w			

			if (newpos-5) >= MaxWidth then
				--new row, we got over margin
				pos=0
				Height=Height+MultiRaidAlertOptions.ButtonSpace
				if lastbuttonspacer then
					Height=Height+MultiRaidAlertOptions.GroupSpace
				end
			else
				--continuing on same row
				if lastbuttonspacer then
					pos=pos+MultiRaidAlertOptions.ButtonSpace
				end
			end
			
			--position button
			getglobal("MRABut"..button):ClearAllPoints()
			getglobal("MRABut"..button):SetPoint("TOPLEFT" , "MultiRaidAlertFrame" , "TOPLEFT" ,MultiRaidAlertOptions.CurrentMargin+pos, -Height) 
			pos=pos+w

			if  MultiRaidAlertData[LastSet]["MSG"][button][3]==1 then
				lastbuttonspacer=true			
			else	
				lastbuttonspacer=false
			end
	
		end
		--and hide others
		for button=buttons+1,MRA_MaxButtons do
			getglobal("MRABut"..button):Hide()	
		end

		if pos>MultiRaidAlertOptions.CurrentMargin then 
			Height=Height+MultiRaidAlertOptions.ButtonSpace 
		end
	else
		-- hide all buttons
		for button=1,MRA_MaxButtons do
			getglobal("MRABut"..button):Hide()	
		end
	end	


	Height=Height+MultiRaidAlertOptions.CurrentMargin+5
	MultiRaidAlertFrame:SetHeight(Height)
end

-- *************************************************************************
-- MOUSEWHEEL EVENT (switch sets)
-- ************************************************************************
function MultiRaidAlert_MouseWheel(arg1)

	if IsShiftKeyDown() then 
		MultiRaidAlertChangeBottomSet(arg1)
	end

	if IsAltKeyDown() then 
		MultiRaidAlertChangeTopSet(arg1)
	end
end

function MultiRaidAlertChangeBottomSet(arg1)

	local LastSet=MultiRaidAlertOptions.LastSet

	if (arg1==-1) then
		LastSet=LastSet+1
		if not(MultiRaidAlertData[LastSet]) then
			LastSet=1
		end
	end

	if (arg1==1) then
		LastSet=LastSet-1
		if (LastSet==0) then 
			LastSet=table.getn(MultiRaidAlertData)
		end
	end
	
	MultiRaidAlertOptions.LastSet=LastSet

	UpdateGrid()
		
	if MultiRaidAlertFrame:IsVisible() then
		UpdateEditGrid()
	end
end

function MultiRaidAlertChangeTopSet(arg1)

	local LastTopSet=MultiRaidAlertOptions.LastTopSet

	if (arg1==-1) then
		LastTopSet=LastTopSet+1
		if not(MultiRaidAlertData[LastTopSet]) then
			LastTopSet=1
		end
	end

	if (arg1==1) then
		LastTopSet=LastTopSet-1
		if (LastTopSet==0) then 
			LastTopSet=table.getn(MultiRaidAlertData)
		end
	end

	MultiRaidAlertOptions.LastTopSet=LastTopSet

	UpdateGrid()
		
	if MultiRaidAlertFrame:IsVisible() then
		UpdateEditGrid()
	end
end

-- *************************************************************************
-- COMMAND LINE PARAMETER HANDLER
-- *************************************************************************
function MultiRaidAlert_cmd(msg)

	if msg=="" then
		MRAPrintout(" usage:")
		MRAPrintout("  /MRA toggle    toggle main frame visible/hidden")
		MRAPrintout("  /MRA reset     reset everything to defaults (that will erase your sets)")
		MRAPrintout("  /MRA resetpos  reset main frame position (use if frame is missing)")
	end

	if msg=="toggle" then
		if MultiRaidAlertFrame:IsVisible() then
			MultiRaidAlertFrame:Hide()
			MultiRaidAlertOptions.Visible=false
		else
			MultiRaidAlertFrame:Show()
			MultiRaidAlertOptions.Visible=true
		end
	end

	if msg=="reset" then
		ResetVars()
	end

	if msg=="resetpos" then
		MultiRaidAlert_ResetPos()
	end

end

-- *************************************************************************
-- VARIABLE FIRST-TIME INIT
-- *************************************************************************
function InitVars()

	if not(MultiRaidAlertData) then
		MRAPrintout("Data not found - reverting to default")
		MultiRaidAlertResetData()
	end

	if not(MultiRaidAlertOptions) then
		MRAPrintout("Options not found - reverting to default (per-character)")
		MultiRaidAlertResetOptions()
	end

	--check for vars presence
	if not (MultiRaidAlertOptions.CurrentScale) then
		MultiRaidAlertOptions.CurrentScale=1
	end

	if not (MultiRaidAlertOptions.ButtonWidth) then
		MultiRaidAlertOptions.ButtonWidth=100
	end

	if not (MultiRaidAlertOptions.ButtonHeight) then
		MultiRaidAlertOptions.ButtonHeight=18
	end

	if not (MultiRaidAlertOptions.ButtonSpace) then
		MultiRaidAlertOptions.ButtonSpace=18
	end

	if not (MultiRaidAlertOptions.GroupSpace) then
		MultiRaidAlertOptions.GroupSpace=25
	end


	if MultiRaidAlertOptions.LastSet > table.getn(MultiRaidAlertData) then
		MultiRaidAlertOptions.LastSet=1
	end
	if MultiRaidAlertOptions.LastTopSet > table.getn(MultiRaidAlertData) then
		MultiRaidAlertOptions.LastTopSet=1
	end

end


-- *************************************************************************
-- misc stuff
-- ************************************************************************

function MRAPrintout(msg)
	if not msg then
		msg="NIL"
	end
	DEFAULT_CHAT_FRAME:AddMessage("[MRA] "..msg, 1.0, 1.0, 0.0);
end

function MultiRaidAlert_OnDragStart()
	MultiRaidAlertFrame:StartMoving();
end

function MultiRaidAlert_ResetPos()
	--re-set width prior to centering frame
	MultiRaidAlertFrame:SetWidth(MultiRaidAlertOptions.CurrentWidth + 2*MultiRaidAlertOptions.CurrentMargin)

	MultiRaidAlertFrame:ClearAllPoints()
	MultiRaidAlertFrame:SetPoint("CENTER","UIParent","CENTER",0,0)
end

function MultiRaidAlert_OnDragStop()
	MultiRaidAlertFrame:StopMovingOrSizing()
end

function MultiRaidAlertEdit_OnDragStart()
	MultiRaidAlertEditFrame:StartMoving();
end

function MultiRaidAlertEdit_OnDragStop()
	MultiRaidAlertEditFrame:StopMovingOrSizing()
end

function MultiRaidAlertOptions_OnDragStart()
	MultiRaidAlertOptionsFrame:StartMoving();
end

function MultiRaidAlertOptions_OnDragStop()
	MultiRaidAlertOptionsFrame:StopMovingOrSizing()
end

function MultiRaidAlertMenu_OnDragStart(this)
	MRACountdown=0	
	this:StartMoving();
end

function MultiRaidAlertMenu_OnDragStop(this)

	--when button is released, check his y then reorder MultiRaidalertData[] according to button's y coords
	
	MRAResetCountDown()
	this:StopMovingOrSizing()
	
	current=tonumber(string.sub(this:GetName(), 19))

	newpos=table.getn(MultiRaidAlertData)

	for n=table.getn(MultiRaidAlertData),1,-1 do
		if n~=current then

			top=getglobal("MultiRaidAlertMenu"..n):GetTop()

			if this:GetTop() > top  then 
				newpos=n-1
			end
		end
	end


	if current>newpos then
		newpos=newpos+1
	end

	--current = dragged button
	--newpos = position it have to be moved


	--set=current selected
	if MRA_MenuMode==1 then
		set=MultiRaidAlertOptions.LastTopSet
	end
	if MRA_MenuMode==2 then
		set=MultiRaidAlertOptions.LastSet
	end
	if MRA_MenuMode==3 then
		set=MultiRaidAlertEditedSet
	end

	
	--scroll all items down/up
	if current~=newpos then

		if current<newpos then
			for n=current, newpos-1 do
				if n+1==set then set=n end
				MultiRaidAlertData[n],MultiRaidAlertData[n+1]=MultiRaidAlertData[n+1],MultiRaidAlertData[n]
			end
		else
			for n=current, newpos+1 ,-1 do
				if n-1==set then set=n end
				MultiRaidAlertData[n],MultiRaidAlertData[n-1]=MultiRaidAlertData[n-1],MultiRaidAlertData[n]
			end
		end
	end
			

	--restore set
	if MRA_MenuMode==1 then
		MultiRaidAlertOptions.LastTopSet=set
	end
	if MRA_MenuMode==2 then
		MultiRaidAlertOptions.LastSet=set
	end
	if MRA_MenuMode==3 then
		MultiRaidAlertEditedSet=set
	end

	MRAInitMenu()

end

function MRA_ReverseSolveSlashCommand(slashcommand)

	local t,i,i2,b=getfenv(0),0,0,0
	local result=nil
	local inff=string.upper(slashcommand)

	for i,i2 in pairs(t) do
		if string.find(i,"^SLASH_") then 
			b=string.upper(getglobal(i))
			--DEFAULT_CHAT_FRAME:AddMessage(i..": "..b) 
			if b==inff then
				--DEFAULT_CHAT_FRAME:AddMessage(i..": "..b,1,0,1) 
				result=string.sub(i, 7,-2)
				do break end
			end
		end
	end
	return result
end


function ResetVars()

	MRAPrintout("Reverting to dummy default set")

	MultiRaidAlertResetData()
	MultiRaidAlertResetOptions()


	UpdateGrid()
	UpdateEditGrid()
	UpdateOptionsGrid()

end

function MultiRaidAlertResetData()

MultiRaidAlertData = {
	[1] = {
		["NAME"] = "Guild",
		["MSG"] = {
			[1] = {
				[1] = "hi",
				[2] = "hi guild",
				[4] = "GUILD",
				[5] = 2,
			},
			[2] = {
				[1] = "nn",
				[2] = "nn",
				[4] = "GUILD",
				[5] = 2,
			},
			[3] = {
				[1] = "logging",
				[2] = "logging, nn",
				[4] = "GUILD",
				[5] = 1,
			},
			[4] = {
				[1] = "Web site",
				[2] = "www.encore-shadowsong.dk",
				[4] = "GUILD",
				[5] = 1,
			},
		},
		["LEFTBRACKETS"] = "",
		["NOTOPSET"] = 1,
		["RIGHTBRACKETS"] = "",
	},
	[2] = {
		["NAME"] = "Molten Core",
		["MSG"] = {
			[1] = {
				[1] = "SKIN DOG",
				[2] = "Loot dog pls",
				[3] = 1,
				[4] = "CTRAID",
			},
			[2] = {
				[1] = "SPAWN",
				[2] = "Kill lavaspawn!",
				[4] = "CTRAID",
			},
			[3] = {
				[1] = "BANISH",
				[2] = "Banish!!",
				[4] = "CTRAID",
			},
		},
		["LEFTBRACKETS"] = "===",
		["RIGHTBRACKETS"] = "===",
	},
	[3] = {
		["NAME"] = "ZG Bosses ",
		["MSG"] = {
			[1] = {
				[1] = "KILL ADD",
				[2] = "Kill add fast!",
				[4] = "RAID",
				[5] = 1,
			},
			[2] = {
				[1] = "POSITIONS",
				[2] = "Back to positions!",
				[3] = 1,
				[4] = "RAID",
				[5] = 1,
			},
			[3] = {
				[1] = "NO MELEE",
				[2] = "No melee!",
				[4] = "RAID",
				[5] = 1,
			},
			[4] = {
				[1] = "MELEE",
				[2] = "Melee attack now, watch for poison",
				[3] = 1,
				[4] = "RAID",
				[5] = 1,
			},
			[5] = {
				[1] = "SON",
				[2] = "Pull son now!",
				[4] = "RAID",
				[5] = 1,
			},
			[6] = {
				[1] = "POISON",
				[2] = "Get poisoned now!!",
				[4] = "RAID",
				[5] = 1,
			},
		},
		["LEFTBRACKETS"] = "===",
		["RIGHTBRACKETS"] = "===",
	},
	[4] = {
		["NAME"] = "Zul'Gurub",
		["MSG"] = {
			[1] = {
				[1] = "herbs",
				[2] = "Herbalists roll",
				[4] = "CT_RAIDASSIST",
				[5] = 2,
			},
			[2] = {
				[1] = "miners",
				[2] = "Miners roll",
				[3] = 1,
				[4] = "CT_RAIDASSIST",
				[5] = 2,
			},
			[3] = {
				[1] = "SHEEP PULL",
				[2] = "Sheep-pull when ready",
				[4] = "CT_RAIDASSIST",
				[5] = 1,
			},
			[4] = {
				[1] = "PULL ZERKER",
				[2] = "Pull Berserker when ready",
				[3] = 1,
				[4] = "CT_RAIDASSIST",
				[5] = 1,
			},
			[5] = {
				[1] = "IMPS",
				[2] = "Imps: Kill one at a time! Tank/Banish, kill voids first",
				[4] = "CT_RAIDASSIST",
				[5] = 1,
			},
			[6] = {
				[1] = "BD",
				[2] = "Blood Drinker - Max range, no melee",
				[4] = "CT_RAIDASSIST",
				[5] = 2,
			},
			[7] = {
				[1] = "BP",
				[2] = "Blood Priest - Stunlock",
				[3] = 1,
				[4] = "CT_RAIDASSIST",
				[5] = 2,
			},
		},
		["LEFTBRACKETS"] = "===",
		["RIGHTBRACKETS"] = "===",
	},
	[5] = {
		["NAME"] = "RAID",
		["MSG"] = {
			[1] = {
				[1] = "PULL",
				[2] = "Pull then ready",
				[4] = "RAID",
				[5] = 1,
			},
			[2] = {
				[1] = "START",
				[2] = "Start attack",
				[4] = "RAID",
				[5] = 2,
			},
			[3] = {
				[1] = "STOP",
				[2] = "STOP ATTACK",
				[4] = "RAID",
				[5] = 2,
			},
			[4] = {
				[1] = "MT1",
				[2] = "Assist MT1",
				[4] = "RAID",
				[5] = 3,
			},
			[5] = {
				[1] = "MT2",
				[2] = "Assist MT2",
				[4] = "RAID",
				[5] = 3,
			},
			[6] = {
				[1] = "MT3",
				[2] = "Assist MT3",
				[3] = 1,
				[4] = "RAID",
				[5] = 3,
			},
			[7] = {
				[1] = "ADD",
				[2] = "Incoming %t, be ready",
				[4] = "RAID",
				[5] = 1,
			},
		},
		["LEFTBRACKETS"] = "===",
		["RIGHTBRACKETS"] = "===",
	},
	[6] = {
		["NAME"] = "Onyxia",
		["MSG"] = {
			[1] = {
				[1] = "DOTS",
				[2] = "Max DOTS - Max DPS",
				[4] = "CT_RAIDASSIST",
			},
			[2] = {
				[1] = "NODOTS",
				[2] = "Stop DOTS",
				[4] = "CT_RAIDASSIST",
			},
			[3] = {
				[1] = "WHELPS",
				[2] = "Kill whelps!",
				[4] = "CT_RAIDASSIST",
			},
		},
		["LEFTBRACKETS"] = "===",
		["RIGHTBRACKETS"] = "===",
	},
}

end

function MultiRaidAlertResetOptions()

MultiRaidAlertOptions = {
	["Visible"] = false,
	["LastSet"] = 1,
	["LastTopSet"] = 5,
	["CurrentScale"] = 1,
	["CurrentWidth"] = 100,
	["ButtonWidth"]=100,
	["ButtonHeight"]=16,
	["ButtonSpace"]=14,
	["GroupSpace"]=7,
	["CurrentMargin"] = 2,
	["CurrentBorderAlpha"]=0.7,
	["CurrentAlpha"]=0.7,
}


end

