## Interface: 40000
## Title: SayGMOTD
## Version: 2010-10-30
## Notes: Displays a window with the GMOTD on login, and whenever it changes.
## Author: Bitbyte of Icecrown
## X-eMail: saysapped at orgplay dot com
## X-Category: Guild, Chat/Communication
## SavedVariablesPerCharacter: GMOTD

SayGMOTD.lua
