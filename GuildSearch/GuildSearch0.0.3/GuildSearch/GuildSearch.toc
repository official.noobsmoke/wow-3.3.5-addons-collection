## Interface: 30300
## Title: Guild Search
## Notes: A UI for searching the guild notes and officer notes.
## Author: Talryn
## Version: v0.3
## OptionalDeps: Ace3, lib-st, LibDataBroker-1.1, LibDBIcon-1.0
## X-Embeds: Ace3
## X-Category: Interface Enhancements 
## SavedVariables: GuildSearchDB
## X-Curse-Packaged-Version: v0.3
## X-Curse-Project-Name: Guild Search
## X-Curse-Project-ID: guild-search
## X-Curse-Repository-ID: wow/guild-search/mainline

embeds.xml

locale.xml

GuildSearch.lua
