## Interface: 30300
## Title: OneClickBuyOut
#@debug@
# ## Title: |cffff0088EBG|r OneClickBuyOut
#@end-debug@
## LoadOnDemand: 1
## LoadWith: Blizzard_AuctionUI
## SavedVariables: OCBO_AucAdvance_Notified, OCBO_NoBuyout
## OptionalDeps: BaudAuction
## Version: v201008060217
## Author: egingell
## X-Curse-Packaged-Version: v201008060217
## X-Curse-Project-Name: OneClickBuyOut
## X-Curse-Project-ID: ocbo
## X-Curse-Repository-ID: wow/ocbo/mainline

BrowseButton.lua
OneClickBuyOut.lua