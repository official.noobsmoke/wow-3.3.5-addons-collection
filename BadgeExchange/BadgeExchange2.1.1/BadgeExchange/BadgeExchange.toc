## Interface: 30300
## Title: BadgeExchange
## Notes: Helps with exchanging various emblems down to lower quality ones
## Version: 2.1.1
## SavedVariables: BadgeExchangeDefaultEmblemAmount, BadgeExchangeDefaultWintergraspCommendationAmount, BadgeExchangeNPCs
BadgeExchange.lua
BadgeExchange.xml