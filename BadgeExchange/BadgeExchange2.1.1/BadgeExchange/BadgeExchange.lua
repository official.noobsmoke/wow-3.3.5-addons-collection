--  ---------------  --
--  Saved Variables  --
--  ---------------  --


BadgeExchangeDefaultEmblemAmount = "10"
BadgeExchangeDefaultWintergraspCommendationAmount = "5"
BadgeExchangeNPCs = {AllianceFrost={name=nil, ID=37942},
                     AllianceTriumph={name=nil, ID=35494}, AllianceTriumphToC={name=nil, ID=35573},
                     AllianceConquest={name=nil, ID=33964},
                     AllianceValor={name=nil, ID=31579},
                     AllianceStoneKeepersShard={name=nil, ID=32294},
                     HordeFrost={name=nil, ID=37941},
                     HordeTriumph={name=nil, ID=35495}, HordeTriumphToC={name=nil, ID=35574},
                     HordeConquest={name=nil, ID=33963},
                     HordeValor={name=nil, ID=31581},
                     HordeStoneKeepersShard={name=nil, ID=32296},
                     UsuriBrightcoin={name=nil, ID=35790},
                     GoodmanTheCloser={name=nil, ID=38858}}


--  ---------------  --
--  Local Variables  --
--  ---------------  --


local VERSION = "2.1.1"
local TYPE = {emblem="EMBLEM", stoneKeepersShard="STONEKEEPERSSHARD"}
local CURRENCY_IDS = {FROST=49426, TRIUMPH=47241, CONQUEST=45624, VALOR=40753, HEROISM=40752,
                      STONE_KEEPERS_SHARD=43228, WINTERGRASP_COMMENDATION=44115 }

local emblemEditBoxes = {}
local hiddenWindow


--  ----------------  --
--  Helper Functions  --
--  ----------------  --


local __scanningTooltip
local function NPCName(id)
    local tooltip = __scanningTooltip
    if tooltip == nil then
        tooltip = CreateFrame("GameTooltip", "MyScannerTT", UIParent, "GameTooltipTemplate")
        local tooltipMethods = getmetatable(tooltip).__index
        tooltip = setmetatable( tooltip,
        {__index = function(self, id)
                local method = tooltipMethods[id] -- See if this key is a tooltip method
                if method then return method end -- If it is, return the method now

                if type(id) == "number" then
                    -- Otherwise look up a unit
                    self:SetOwner(UIParent, "ANCHOR_NONE")
                    self:SetHyperlink(string.format("unit:0xF13%07X000000", id))
                    return _G[self:GetName() .. "TextLeft1"]:GetText()
                end
            end})
        __scanningTooltip = tooltip
    end
    return tooltip[id]
end

local function setDefaultEmblemAmount(default)
    if default == BadgeExchangeDefaultEmblemAmount then
        print("The default emblem amount is already " .. default .. "!")
    else
        print("Setting default emblem amount to: " .. default)
        BadgeExchangeDefaultEmblemAmount = default
    end
end

local function setDefaultWintergraspCommendationAmount(default)
    if default == BadgeExchangeDefaultWintergraspCommendationAmount then
        print("The default Wintergrasp Commendation amount is already " .. default .. "!")
    else
        print("Setting default Wintergrasp Commendation amount to: " .. default)
        BadgeExchangeDefaultWintergraspCommendationAmount = default
    end
end

local function updateNPCs(key)
    local npc = BadgeExchangeNPCs[key]

    if npc and npc.name == nil then
        npc.name = NPCName(npc.ID)
    else
        for npc_key, npc in pairs(BadgeExchangeNPCs) do
            if npc.name == nil then
                npc.name = NPCName(npc.ID)
            end
        end
    end
end

local function isMerchantSlot(slot, itemID)
    local itemLink = GetMerchantItemLink(slot)
    local _, expectedItemLink, _, _, _, _, _, _, _, _, _ = GetItemInfo(itemID)
    return itemLink == expectedItemLink
end

local function itemMismatch(slot, expectedItemID)
    local _, expectedItemLink, _, _, _, _, _, _, _, _, _ = GetItemInfo(expectedItemID)
    local gotItemLink = GetMerchantItemLink(slot) or "Nothing"

    print("Internal error!!!")
    print("Item name mismatch:")
    print("Expected: " .. expectedItemLink)
    print("Got: " .. gotItemLink)
    print("Please send an IM or mail to Datachanger with this information.")
end

local function determineFrostSlot()
    local _, class = UnitClassBase("player")

    if class == "DEATHKNIGHT" or class == "HUNTER" or class == "MAGE" or class == "ROGUE" or class == "WARLOCK" or class == "WARRIOR" then
        return 6
    elseif class == "PALADIN" or class == "PRIEST" then
        return 11
    elseif class == "DRUID" or class == "SHAMAN" then
        return 16
    end
end

local function determineTriumphSlot()
    local _, class = UnitClassBase("player")

    if class == "DEATHKNIGHT" or class == "HUNTER" or class == "MAGE" or class == "ROGUE" or class == "WARLOCK" or class == "WARRIOR" then
        return 10
    elseif class == "PALADIN" or class == "PRIEST" then
        return 15
    elseif class == "DRUID" or class == "SHAMAN" then
        return 20
    end
end


--  ------------------  --
--  Exchange Functions
--  ------------------  --


local function exchangeValor(quantity, usuri, full)
    local slot

    if usuri then
        slot = 1
    else
        slot = 39
    end

    if isMerchantSlot(slot, CURRENCY_IDS["HEROISM"]) then
        BuyMerchantItem(slot, quantity)
    else
        itemMismatch(slot, CURRENCY_IDS["HEROISM"])
    end
end

local function exchangeConquest(quantity, usuri, full)
    local slot

    if usuri then
        slot = 2
    else
        slot = 1
    end

    if isMerchantSlot(slot, CURRENCY_IDS["VALOR"]) then
        BuyMerchantItem(slot, quantity)
    else
        itemMismatch(slot, CURRENCY_IDS["VALOR"])
    end

    if full then
        exchangeValor(quantity, usuri, full)
    end
end

local function exchangeTriumph(quantity, usuri, full)
    local slot

    if usuri then
        slot = 3
    else
        slot = determineTriumphSlot()
    end

    if isMerchantSlot(slot, CURRENCY_IDS["CONQUEST"]) then
        BuyMerchantItem(slot, quantity)
    else
        itemMismatch(slot, CURRENCY_IDS["CONQUEST"])
    end

    if full then
        exchangeConquest(quantity, usuri, full)
    end
end

local function exchangeFrost(quantity, usuri, full)
    local slot

    if usuri then
        slot = 4
    else
        slot = determineFrostSlot()
    end

    if isMerchantSlot(slot, CURRENCY_IDS["TRIUMPH"]) then
        BuyMerchantItem(slot, quantity)
    else
        itemMismatch(slot, CURRENCY_IDS["TRIUMPH"])
    end

    if full then
        exchangeTriumph(quantity, usuri, full)
    end
end

local function exchangeStoneKeepersShard(quantity)
    if isMerchantSlot(44, CURRENCY_IDS["WINTERGRASP_COMMENDATION"]) then
        BuyMerchantItem(44, quantity)
    else
        itemMismatch(44, CURRENCY_IDS["WINTERGRASP_COMMENDATION"])
    end
end

local function exchange_call(t, self)
    local quantity = self.editBox:GetNumber()

    if quantity == 0 then
        _G[self:GetName() .. "CongratulationsFontString"]:Show()
        _G[self:GetName() .. "YouExchangedNothingFontString"]:Show()
        PlaySoundFile("Sound\\Creature\\BarnesTheStageManager\\BarnesAudienceApplause.wav")
        return
    end

    if self.TYPE == TYPE["emblem"] then
        if quantity > tonumber(_G[self:GetName() .. self.START_CURRENCY .. "FontString"]:GetText()) then
            _G[self:GetName() .. "InsufficientCurrencyFontString"]:Show()
        else
            return t[self.START_CURRENCY](quantity, self.USURI, self.FULL)
        end
    elseif self.TYPE == TYPE["stoneKeepersShard"] then
        if quantity * 30 > tonumber(_G[self:GetName() .. self.START_CURRENCY .. "FontString"]:GetText()) then
            _G[self:GetName() .. "InsufficientCurrencyFontString"]:Show()
        else
            return t[self.START_CURRENCY](quantity)
        end
    end
end

local exchange = {Frost=exchangeFrost, Triumph=exchangeTriumph, Conquest=exchangeConquest, Valor=exchangeValor, StoneKeepersShard=exchangeStoneKeepersShard}
setmetatable(exchange, {__call = exchange_call})


--  -----------------------  --
--  Slash Command Functions  --
--  -----------------------  --


local function BadgeExchangeHelp()
    updateNPCs("UsuriBrightcoin")
    local usuriBrightcoin = BadgeExchangeNPCs["UsuriBrightcoin"]["name"] or "Usuri Brightcoin"

    print("BadgeExchange helps with exchanging various emblems down to lower quality ones.")
    print("All emblems are converted at a 1 to 1 ratio.")
    print("Stone Keeper's Shards can be converted to Wintergrasp Commendations (for 2000 honor) at a 30 to 1 ratio.")
    print("To open the BadgeExchange window, interact with a respective quartermaster or " .. usuriBrightcoin .. " in the Underbelly of Dalaran.")
    print("Once the BadgeExchange window is open, type in a number in the \"Amount\" box, and click the button.")
    print("To change the default emblem amount upon login, type:")
    print("/be setdefaultemblem ##")
    print("To change the default Wintergrasp Commendation amount upon login, type:")
    print("/be setdefaultwintergrasp ##")
    print("Note: Changing default amounts does not automatically update the current amounts in the BadgeExchange window.")
    print("For credits, type: /be credits")
end

local function BadgeExchangeCredits()
    print("Thank you you using BadgeExchange version " .. VERSION .. "!")
    print("Thank you for caring enough to look at the credits.")
    print("BadgeExchange is developed and conceptualized by Datachanger.")
    print("Extra thanks to Starkwolf for his Non-English client testing.")
end

local function BadgeExchangeInvalidCommand()
    print("Invalid command. Type \"/be help\" or \"/badgeexchange help\" for more information.")
end

local function BadgeExchange(msg, editbox)
    local quantity

    msg = msg:lower()

    quantity = msg:match("(%d+)%s*$")
    msg = msg:gsub("%s*%d+%s*$", "")

    if msg == "setdefaultemblem" then
        if quantity then
            setDefaultEmblemAmount(quantity)
        else
            print("Please enter a number after \"setdefaultemblem\"")
        end
    elseif msg == "setdefaultwintergrasp" then
        if quantity then
            setDefaultWintergraspCommendationAmount(quantity)
        else
            print("Please enter a number after \"setdefaultwintergrasp\"")
        end
    elseif msg == "help" then
        BadgeExchangeHelp()
    elseif msg == "credits" then
        BadgeExchangeCredits()
    else
        BadgeExchangeInvalidCommand()
    end
end


--  -------------  --
--  XML Functions  --
--  -------------  --


function BadgeExchangeOnLoad()
    SlashCmdList["BADGEEXCHANGE"] = BadgeExchange
    SLASH_BADGEEXCHANGE1, SLASH_BADGEEXCHANGE2 = '/be', '/badgeexchange'

    print("BadgeExchange version " .. VERSION .. " loaded.")
    print("Type \"/be help\" or \"/badgeexchange help\" for more information.")
end

function BadgeExchangeOnAddonLoaded(self)
    local emblemEditBox = _G[self:GetName() .. "EmblemEditBox"]
    local wintergraspCommendationEditBox = _G[self:GetName() .. "WintergraspCommendationEditBox"]

    if emblemEditBox then
        table.insert(emblemEditBoxes, emblemEditBox)
        emblemEditBox:SetNumber(BadgeExchangeDefaultEmblemAmount)
        self.TYPE = TYPE["emblem"]
        self.editBox = emblemEditBox
    end

    if wintergraspCommendationEditBox then
        wintergraspCommendationEditBox:SetNumber(BadgeExchangeDefaultWintergraspCommendationAmount)
        self.TYPE = TYPE["stoneKeepersShard"]
        self.editBox = wintergraspCommendationEditBox
    end

    updateNPCs()
    BadgeExchangeRegisterFrameExchangeType(self)
    BadgeExchangeProperTarget(self)
end

function BadgeExchangeRegisterFrameExchangeType(self)
    local startCurrency, finishCurrency
    local validNPCs = {}

    if self:GetName():find("Usuri") then
        table.insert(validNPCs, "UsuriBrightcoin")
        self.BUTTONS = {}

        for _, button in pairs({self:GetChildren()}) do
            startCurrency, finishCurrency = button:GetName():match("BadgeExchangeUsuri(.+)To(.+)Button")

            if startCurrency and finishCurrency then
                button.START_CURRENCY = startCurrency
                button.FINISH_CURRENCY = finishCurrency
                _G[button:GetName() .. "ExchangeFontString"]:SetText(startCurrency .. " to " .. finishCurrency)
                table.insert(self.BUTTONS, button)
            end
        end

        self.USURI = true
    else
        local faction, _ = UnitFactionGroup("player")
        startCurrency, finishCurrency = self:GetName():match("BadgeExchange(.+)To(.+)")

        _G[self:GetName() .. "ButtonExchangeFontString"]:SetText(startCurrency .. " to " .. finishCurrency)
        table.insert(validNPCs, faction .. startCurrency)

        if startCurrency == "Frost" then
            table.insert(validNPCs, "GoodmanTheCloser")
        elseif startCurrency == "Triumph" then
            table.insert(validNPCs, faction .. startCurrency .. "ToC")
        end

        self.START_CURRENCY = startCurrency
    end

    self.validNPCs = validNPCs

end

function BadgeExchangeProperTarget(self)
    local target = GetUnitName("target", false)

    for _, npc in pairs(self.validNPCs) do
        if BadgeExchangeNPCs[npc]["name"] == nil then
            updateNPCs(npc)
        end

        if target == BadgeExchangeNPCs[npc]["name"] then
            return true
        end
    end

    return false
end

function BadgeExchangeSetCurrencies(self)
    local frostFontString = _G[self:GetName() .. "FrostFontString"]
    local triumphFontString = _G[self:GetName() .. "TriumphFontString"]
    local conquestFontString = _G[self:GetName() .. "ConquestFontString"]
    local valorFontString = _G[self:GetName() .. "ValorFontString"]
    local heroismFontString = _G[self:GetName() .. "HeroismFontString"]
    local stoneKeepersShardFontString = _G[self:GetName() .. "StoneKeepersShardFontString"]

    if self.TYPE == TYPE["emblem"] then
        for i=1,GetCurrencyListSize(),1 do
            local _, _, _, _, _, currencyCount, _, _, currencyID = GetCurrencyListInfo(i)


            if currencyID == CURRENCY_IDS["FROST"] then
                frostFontString:SetText(currencyCount)
            elseif currencyID == CURRENCY_IDS["TRIUMPH"] then
                triumphFontString:SetText(currencyCount)
            elseif currencyID == CURRENCY_IDS["CONQUEST"] then
                conquestFontString:SetText(currencyCount)
            elseif currencyID == CURRENCY_IDS["VALOR"] then
                valorFontString:SetText(currencyCount)
            elseif currencyID == CURRENCY_IDS["HEROISM"] then
                heroismFontString:SetText(currencyCount)
            end
        end

        if frostFontString:GetText() == nil then
            frostFontString:SetText("0")
        end

        if triumphFontString:GetText() == nil then
            triumphFontString:SetText("0")
        end

        if conquestFontString:GetText() == nil then
            conquestFontString:SetText("0")
        end

        if valorFontString:GetText() == nil then
            valorFontString:SetText("0")
        end

        if heroismFontString:GetText() == nil then
            heroismFontString:SetText("0")
        end
    elseif self.TYPE == TYPE["stoneKeepersShard"] then
        for i=1,GetCurrencyListSize(),1 do
            local _, _, _, _, _, currencyCount, _, _, currencyID = GetCurrencyListInfo(i)

            if currencyID == CURRENCY_IDS["STONE_KEEPERS_SHARD"] then
                stoneKeepersShardFontString:SetText(currencyCount)
            end
        end

        if stoneKeepersShardFontString:GetText() == nil then
            stoneKeepersShardFontString:SetText("0")
        end
    end
end

function BadgeExchangeEmblemEditBoxTextChange(self)
    for _, emblemEditBox in pairs(emblemEditBoxes) do
        emblemEditBox:SetText(self:GetText())
    end

    if self:GetParent().USURI then
        for _, button in pairs(self:GetParent().BUTTONS) do
            _G[button:GetName() .. "FromFontString"]:SetText("x" .. tostring(self:GetNumber()))
            _G[button:GetName() .. "ToFontString"]:SetText("x" .. tostring(self:GetNumber()))
        end
    else
        _G[self:GetParent():GetName() .. "ButtonFromFontString"]:SetText("x" .. tostring(self:GetNumber()))
        _G[self:GetParent():GetName() .. "ButtonToFontString"]:SetText("x" .. tostring(self:GetNumber()))
    end
end

function BadgeExchangeStoneKeepersShardEditBoxTextChange(self)
    _G[self:GetParent():GetName() .. "ButtonFromFontString"]:SetText("x" .. tostring(self:GetNumber() * 30))
    _G[self:GetParent():GetName() .. "ButtonToFontString"]:SetText("x" .. tostring(self:GetNumber()))
end

function BadgeExchangeExchange(self)  --Unlike other "self" varibales, this "self" is a button
    if self:GetParent().USURI then
        self:GetParent().START_CURRENCY = self.START_CURRENCY

        if self.FINISH_CURRENCY == "Heroism" then
            self:GetParent().FULL = true
        else
            self:GetParent().FULL = false
        end
    end

    exchange(self:GetParent())
end

function BadgeExchangePostCloseWindow(self)
    hiddenWindow = self
    BadgeExchangeClosed:Show()
end

function BadgeExchangeReopenWindow()
    hiddenWindow:Show()
    hiddenWindow = nil
end