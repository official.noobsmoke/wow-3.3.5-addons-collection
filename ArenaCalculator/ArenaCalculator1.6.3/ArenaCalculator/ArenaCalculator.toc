## Interface: 20300
## Title: Arena Calculator
## Notes: Tool to calculate several things related to the arena. Also enhances the PVP frame to show estimated arena points earned.
## Dependencies:
## SavedVariables: ACalc_SavedOpt
## Author: Toppe
## Version: 1.6.3
ArenaCalculator.xml