------------------------
----Arena Calculator----
----    by Toppe    ----
------------------------



local ACalc_RoundOff=0.5;



function ACalc_ChatCommand(cmd)
   argv = {};
   for arg in string.gmatch(string.lower(cmd), '[%a%d%-%.]+') do table.insert(argv, arg) end
   if (argv[1]==nil) then
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD100Arena Calculator|r |cAAFFD100v1.6.3|r command list")
      DEFAULT_CHAT_FRAME:AddMessage(" show |cAACCFF99- show the Arena Calculator frame|r")
      DEFAULT_CHAT_FRAME:AddMessage(" scale # |cAACCFF99- set the scale of the Arena Calculator frame (0.3 - 2.0) |r")
      DEFAULT_CHAT_FRAME:AddMessage(" teams |cAACCFF99- print arena teams information in the chat|r")
      DEFAULT_CHAT_FRAME:AddMessage(" rating # |cAACCFF99- calculate rating-to-points in the chat|r")
      DEFAULT_CHAT_FRAME:AddMessage(" points # |cAACCFF99- calculate points-to-rating in the chat|r")
   elseif (argv[1]=="show") then
      ACalc_ShowHide();
   elseif (argv[1]=="teams") then
      ACalc_ChatPrint();
   elseif (argv[1]=="scale") then
      local newarg=tonumber(argv[2]);
      if (newarg>=0.3 and newarg<=2.0) then
         ACalcFrame:SetScale(newarg);
         ACalcFrame:ClearAllPoints();
         ACalcFrame:SetPoint("CENTER", UIParent, "CENTER", 0, 0);
         ACalc_SavedOpt.ACalcFrameScale=newarg;
         ACalcFrameOptionsSlider1:SetValue(newarg);
      else
         DEFAULT_CHAT_FRAME:AddMessage("|cAACCFF99Scale must be between 0.3 and 2.0|r");
      end
   elseif (argv[1]=="rating") then
      local newarg=tonumber(argv[2]);
      ACalc_FrameCalc1(2, newarg);
   elseif (argv[1]=="points") then
      local newarg=tonumber(argv[2]);
      if type(newarg)=="number" then
         ACalc_FrameCalc2(2, newarg);
      else
         DEFAULT_CHAT_FRAME:AddMessage("|cAACCFF99Value must be a number.|r");
      end
   end
end


SlashCmdList["ACALC"] = ACalc_ChatCommand;
SLASH_ACALC1 = "/acalc";
SLASH_ACALC2 = "/arenacalc";
SLASH_ACALC3 = "/arenacalculator";



BINDING_HEADER_ACALC = "Arena Calculator Bindings";
BINDING_NAME_TOGGLEACALC = "Toggle ACalc Window";




local ArenaSortLoaded=0;


function ACalc_ShowHide()
   if ACalcFrame:IsShown() then
      if ACalcFrameOptions:IsShown() then
         ACalcFrameOptions:Hide();
      end
      ACalcFrame:Hide();
   else
      UIFrameFadeIn(ACalcFrame, 0.2, 0, 1);
      ACalcFrame:Show();
   end
end

function ACalcOpt_ShowHide()
   if ACalcFrameOptions:IsShown() then
      ACalcFrameOptions:Hide();
   else
      ACalcFrameOptions:Show();
      if ArenaSortLoaded==1 then
         if ACalcFrameSort:IsShown() then
            ACalcFrameSort:Hide();
         end
      end
   end
end



function ACalcOpt_Default()
   ACalc_SavedOpt.ShowTotal=1;
   ACalcFrameOptionsCheckButton1:SetChecked(true);

   ACalc_SavedOpt.ShowTeams=1;
   ACalcFrameOptionsCheckButton2:SetChecked(true);
   
   ACalc_SavedOpt.Recolor=0;
   ACalcFrameOptionsCheckButton4:SetChecked(false);

   ACalc_SavedOpt.ShowTotalHonor=0;
   ACalcFrameOptionsCheckButton5:SetChecked(false);

   ACalc_SavedOpt.ShowTotalHonorCond=0;
   ACalcFrameOptionsCheckButton6:SetChecked(false);

   ACalc_Main();

   ACalc_SavedOpt.ShowButton=1;
   ACalcFrameOptionsCheckButton3:SetChecked(true);
   ACalc_ShowFrame:Show();

   ACalc_SavedOpt.ACalcFrameScale=1.0;
   ACalcFrameOptionsSlider1:SetValue(1.0);
   ACalcFrame:SetScale(1.0);
   ACalcFrame:SetPoint("CENTER", UIParent, "CENTER", 0, 0);
end


-------------------------------------------
------- RATING TO POINTS CALCULATOR -------
-------------------------------------------


function ACalc_FrameCalc1(n, arg)
   local ACalc_CalcNumber1=0;
   if n==1 then
      ACalc_CalcNumber1=ACalcFrameBox1EditBox:GetNumber();
   elseif n==2 then
      ACalc_CalcNumber1=arg;
   end
   local ACalc_FrameCalcAnswer1=0;
   local ACalc_FrameCalcAnswer2=0;
   local ACalc_FrameCalcAnswer3=0;

   if ACalc_CalcNumber1>1500 then
      ACalc_FrameCalcAnswer1=(1511.26)/(1+1639.28*(2.71828^((-0.00412)*ACalc_CalcNumber1)));
      ACalc_FrameCalcAnswer2=math.floor((ACalc_FrameCalcAnswer1)*(1-0.12));
      ACalc_FrameCalcAnswer3=math.floor((ACalc_FrameCalcAnswer1)*(1-0.24));
      ACalc_FrameCalcAnswer1=math.floor(ACalc_FrameCalcAnswer1);
   elseif ACalc_CalcNumber1<1501 then
      ACalc_FrameCalcAnswer1=0.22*ACalc_CalcNumber1+14;
      ACalc_FrameCalcAnswer2=math.floor((0.88*ACalc_FrameCalcAnswer1)+ACalc_RoundOff);
      ACalc_FrameCalcAnswer3=math.floor((0.76*ACalc_FrameCalcAnswer1)+ACalc_RoundOff);
      ACalc_FrameCalcAnswer1=math.floor(ACalc_FrameCalcAnswer1+ACalc_RoundOff);
   end

   if ACalc_FrameCalcAnswer1<0 then
      ACalc_FrameCalcAnswer1=0;
   end
   if ACalc_FrameCalcAnswer2<0 then
      ACalc_FrameCalcAnswer2=0;
   end
   if ACalc_FrameCalcAnswer3<0 then
      ACalc_FrameCalcAnswer3=0;
   end

   if n==1 then
      ACalcFrameTextAnswer1:SetText("|cAAFFD1002v2:|r "..ACalc_FrameCalcAnswer3.."|n".."|cAAFFD1003v3:|r "..ACalc_FrameCalcAnswer2.."|n".."|cAAFFD1005v5:|r "..ACalc_FrameCalcAnswer1);
   elseif n==2 then
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1002v2:|r  |cAACCFF99Points:|r "..math.floor(ACalc_FrameCalcAnswer3+ACalc_RoundOff).."|cAACCFF99.|r");
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1003v3:|r  |cAACCFF99Points:|r "..math.floor(ACalc_FrameCalcAnswer2+ACalc_RoundOff).."|cAACCFF99.|r");
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1005v5:|r  |cAACCFF99Points:|r "..math.floor(ACalc_FrameCalcAnswer1+ACalc_RoundOff).."|cAACCFF99.|r");
   end
end



-------------------------------------------
------- POINTS TO RATING CALCULATOR -------
-------------------------------------------


function ACalc_FrameCalc2(n, arg)
   local ACalc_CalcNumber2=0;
   if n==1 then
      ACalc_CalcNumber2=ACalcFrameBox2EditBox2:GetNumber();
   elseif n==2 then
      ACalc_CalcNumber2=arg;
   end
   local ACalc_Answer1=ACalc_CalcNumber2/0.76;
   local ACalc_Answer2=ACalc_CalcNumber2/0.88;
   local ACalc_Answer3=ACalc_CalcNumber2;

   if ACalc_CalcNumber2>261 and ACalc_CalcNumber2<1511 then
      ACalc_Answer1=math.ceil((math.log((1511.26/(ACalc_CalcNumber2 / 0.76) - 1) / 1639.28))/(math.log(2.71828))/-0.00412);
   elseif ACalc_CalcNumber2<262 then
      ACalc_Answer1=math.floor(((ACalc_CalcNumber2 / 0.76 - 14) / 0.22)) - 2;
   else
      ACalc_Answer1="N/A";
   end

   if ACalc_CalcNumber2>303 and ACalc_CalcNumber2<1511 then
      ACalc_Answer2=math.ceil((math.log((1511.26/(ACalc_CalcNumber2 / 0.88) - 1) / 1639.28))/(math.log(2.71828))/-0.00412);
   elseif ACalc_CalcNumber2<304 then
      ACalc_Answer2=math.floor(((ACalc_CalcNumber2 / 0.88 - 14) / 0.22)) - 2;
   else
      ACalc_Answer2="N/A";
   end

   if ACalc_CalcNumber2>344 and ACalc_CalcNumber2<1511 then
      ACalc_Answer3=math.ceil((math.log((1511.26/ACalc_CalcNumber2 - 1) / 1639.28))/(math.log(2.71828))/-0.00412);
   elseif ACalc_CalcNumber2<345 then
      ACalc_Answer3=math.floor(((ACalc_CalcNumber2 - 14) / 0.22)) - 2;
   else
      ACalc_Answer3="N/A";
   end

   if n==1 then
      ACalcFrameTextAnswer2:SetText("|cAAFFD1002v2:|r "..ACalc_Answer1.."|n".."|cAAFFD1003v3:|r "..ACalc_Answer2.."|n".."|cAAFFD1005v5:|r "..ACalc_Answer3);
   elseif n==2 then
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1002v2:|r  |cAACCFF99Rating:|r "..ACalc_Answer1.."|cAACCFF99.|r");
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1003v3:|r  |cAACCFF99Rating:|r "..ACalc_Answer2.."|cAACCFF99.|r");
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1005v5:|r  |cAACCFF99Rating:|r "..ACalc_Answer3.."|cAACCFF99.|r");
   end
end



------------------------------------
-------- START CHAT FUNCTION -------
------------------------------------


function ACalc_ChatPrint()

   local ACalc_ArenaTeam1Points, ACalc_ArenaTeam2Points, ACalc_ArenaTeam3Points, ACalc_ArenaTeam1Rating, ACalc_ArenaTeam2Rating, ACalc_ArenaTeam3Rating, ACalc_max, ACalc_maxrecolor, ACalc_TeamRecolor = unpack(ACalc_MainCalc());


   ACalc_max=ACalc_max+GetArenaCurrency();

   if ACalc_ArenaTeam1Points>0 then
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1002v2:|r  |cAACCFF99Rating:|r "..ACalc_ArenaTeam1Rating.."|cAACCFF99. Points:|r "..ACalc_ArenaTeam1Points.."|cAACCFF99.|r");
   end
   if ACalc_ArenaTeam2Points>0 then
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1003v3:|r  |cAACCFF99Rating:|r "..ACalc_ArenaTeam2Rating.."|cAACCFF99. Points:|r "..ACalc_ArenaTeam2Points.."|cAACCFF99.|r");
   end
   if ACalc_ArenaTeam3Points>0 then
      DEFAULT_CHAT_FRAME:AddMessage("|cAAFFD1005v5:|r  |cAACCFF99Rating:|r "..ACalc_ArenaTeam3Rating.."|cAACCFF99. Points:|r "..ACalc_ArenaTeam3Points.."|cAACCFF99.|r");
   end
   if ACalc_max>GetArenaCurrency() then
      if ACalc_maxrecolor==1 then
         DEFAULT_CHAT_FRAME:AddMessage("|cAACCFF99Current points:|r "..GetArenaCurrency().."|cAACCFF99. Total points:|r |cAACC0000"..ACalc_max.."|r|cAACCFF99.|r |cAACC0000(Not eligible)|r");
      else
         DEFAULT_CHAT_FRAME:AddMessage("|cAACCFF99Current points:|r "..GetArenaCurrency().."|cAACCFF99. Total points:|r "..ACalc_max.."|cAACCFF99.|r");
      end
   else
      DEFAULT_CHAT_FRAME:AddMessage("|cAACCFF99You don't have any arena teams.|r");
   end
end



-------------------------------------
-------- START HONOR FUNCTION -------
-------------------------------------

function ACalc_Honor()

   local hk, honor = GetPVPSessionStats();
   local ACalc_honor=(GetHonorCurrency().." ("..honor+GetHonorCurrency()..")");


   PVPFrameHonorPoints:SetWidth(120);
   PVPFrameHonorLabel:SetPoint("LEFT", PVPFrameHonor, "LEFT", 20, 0);
   PVPFrameHonorIcon:SetPoint("LEFT", PVPFrameHonorLabel, "LEFT", -22, -6);
   PVPFrameHonorPoints:SetPoint("LEFT", PVPFrameHonorLabel, "RIGHT", 8, 0);
   PVPFrameHonorPoints:SetJustifyH("LEFT");


   if ACalc_SavedOpt.ShowTotalHonor==1 and ACalc_SavedOpt.ShowTotalHonorCond==1 then
      if honor>0 then
         PVPFrameHonorPoints:SetText(ACalc_honor);
      else
         PVPFrameHonorPoints:SetText(GetHonorCurrency());
      end
   elseif ACalc_SavedOpt.ShowTotalHonor==1 then
      PVPFrameHonorPoints:SetText(ACalc_honor);
   elseif ACalc_SavedOpt.ShowTotalHonor==0 then
      PVPFrameHonorPoints:SetText(GetHonorCurrency());
   end
end





-----------------------------------------
-------- START MAIN CALC FUNCTION -------
-----------------------------------------

function ACalc_MainCalc()

   local ACalc_ArenaTeam1Points=0;
   local ACalc_ArenaTeam2Points=0;
   local ACalc_ArenaTeam3Points=0;
   
   local ACalc_ArenaTeam1Played=0;
   local ACalc_ArenaTeam2Played=0;
   local ACalc_ArenaTeam3Played=0;

   local ACalc_ArenaTeam1playerPlayed=0;
   local ACalc_ArenaTeam2playerPlayed=0;
   local ACalc_ArenaTeam3playerPlayed=0;
   
   local ACalc_ArenaTeam1Name;
   local ACalc_ArenaTeam2Name;
   local ACalc_ArenaTeam3Name;

   -- Get all teamratings.
   local ACalc_ArenaTeam1Rating=ACalc_Rating(1);
   local ACalc_ArenaTeam2Rating=ACalc_Rating(2);
   local ACalc_ArenaTeam3Rating=ACalc_Rating(3);


   -- Learn which team is in what bracket, calculate their points, and get teamplayed and playerplayed data.
   if ACalc_Size(1)==2 then
      ACalc_ArenaTeam1Points=ACalc_Calculate(1)
      ACalc_ArenaTeam1Rating=ACalc_Rating(1);
      ACalc_ArenaTeam1Played=ACalc_Played(1);
      ACalc_ArenaTeam1playerPlayed=ACalc_playerPlayed(1);
      ACalc_ArenaTeam1Name=ACalc_teamName(1);
   elseif ACalc_Size(1)==3 then
      ACalc_ArenaTeam2Points=ACalc_Calculate(1);
      ACalc_ArenaTeam2Rating=ACalc_Rating(1);
      ACalc_ArenaTeam2Played=ACalc_Played(1);
      ACalc_ArenaTeam2playerPlayed=ACalc_playerPlayed(1);
      ACalc_ArenaTeam2Name=ACalc_teamName(1);
   elseif ACalc_Size(1)==5 then
      ACalc_ArenaTeam3Points=ACalc_Calculate(1);
      ACalc_ArenaTeam3Rating=ACalc_Rating(1);
      ACalc_ArenaTeam3Played=ACalc_Played(1);
      ACalc_ArenaTeam3playerPlayed=ACalc_playerPlayed(1);
      ACalc_ArenaTeam3Name=ACalc_teamName(1);
   end

   if ACalc_Size(2)==2 then
      ACalc_ArenaTeam1Points=ACalc_Calculate(2);
      ACalc_ArenaTeam1Rating=ACalc_Rating(2);
      ACalc_ArenaTeam1Played=ACalc_Played(2);
      ACalc_ArenaTeam1playerPlayed=ACalc_playerPlayed(2);
      ACalc_ArenaTeam1Name=ACalc_teamName(2);
   elseif ACalc_Size(2)==3 then
      ACalc_ArenaTeam2Points=ACalc_Calculate(2);
      ACalc_ArenaTeam2Rating=ACalc_Rating(2);
      ACalc_ArenaTeam2Played=ACalc_Played(2);
      ACalc_ArenaTeam2playerPlayed=ACalc_playerPlayed(2);
      ACalc_ArenaTeam2Name=ACalc_teamName(2);
   elseif ACalc_Size(2)==5 then
      ACalc_ArenaTeam3Points=ACalc_Calculate(2);
      ACalc_ArenaTeam3Rating=ACalc_Rating(2);
      ACalc_ArenaTeam3Played=ACalc_Played(2);
      ACalc_ArenaTeam3playerPlayed=ACalc_playerPlayed(2);
      ACalc_ArenaTeam3Name=ACalc_teamName(2);
   end

   if ACalc_Size(3)==2 then
      ACalc_ArenaTeam1Points=ACalc_Calculate(3);
      ACalc_ArenaTeam1Rating=ACalc_Rating(3);
      ACalc_ArenaTeam1Played=ACalc_Played(3);
      ACalc_ArenaTeam1playerPlayed=ACalc_playerPlayed(3);
      ACalc_ArenaTeam1Name=ACalc_teamName(3);
   elseif ACalc_Size(3)==3 then
      ACalc_ArenaTeam2Points=ACalc_Calculate(3);
      ACalc_ArenaTeam2Rating=ACalc_Rating(3);
      ACalc_ArenaTeam2Played=ACalc_Played(3);
      ACalc_ArenaTeam2playerPlayed=ACalc_playerPlayed(3);
      ACalc_ArenaTeam2Name=ACalc_teamName(3);
   elseif ACalc_Size(3)==5 then
      ACalc_ArenaTeam3Points=ACalc_Calculate(3);
      ACalc_ArenaTeam3Rating=ACalc_Rating(3);
      ACalc_ArenaTeam3Played=ACalc_Played(3);
      ACalc_ArenaTeam3playerPlayed=ACalc_playerPlayed(3);
      ACalc_ArenaTeam3Name=ACalc_teamName(3);
   end
   

   -- Get percent of player played against team played, if team has played more than 10 matches.
   local ACalc_ArenaTeam1playerPlayedPct=0;
   if ACalc_ArenaTeam1Played>=10 then
      ACalc_ArenaTeam1playerPlayedPct=floor((ACalc_ArenaTeam1playerPlayed/ACalc_ArenaTeam1Played)*100);
   else
      ACalc_ArenaTeam1playerPlayedPct=0;
   end
   
   local ACalc_ArenaTeam2playerPlayedPct=0;
   if ACalc_ArenaTeam2Played>=10 then
      ACalc_ArenaTeam2playerPlayedPct=floor((ACalc_ArenaTeam2playerPlayed/ACalc_ArenaTeam2Played)*100);
   else
      ACalc_ArenaTeam2playerPlayedPct=0;
   end

   local ACalc_ArenaTeam3playerPlayedPct=0;
   if ACalc_ArenaTeam3Played>=10 then
      ACalc_ArenaTeam3playerPlayedPct=floor((ACalc_ArenaTeam3playerPlayed/ACalc_ArenaTeam3Played)*100);
   else
      ACalc_ArenaTeam3playerPlayedPct=0;
   end



   local ACalc_LeadingTeam=0;
   local ACalc_SecondTeam=0;
   local ACalc_ThirdTeam=0;
   local ACalc_TeamRecolor=0;

   -- Create a table (array) with all teams points and sort it most->least
   local ACalc_TeamsTable={ACalc_ArenaTeam1Points, ACalc_ArenaTeam2Points, ACalc_ArenaTeam3Points};
   table.sort(ACalc_TeamsTable, function(a,b) return a>b end);

   -- See which team is actually leading (and second / third) by checking most amount of points to all teams points, and so on.
   if ACalc_TeamsTable[1]==ACalc_ArenaTeam1Points then
      ACalc_LeadingTeam=1;
   elseif ACalc_TeamsTable[1]==ACalc_ArenaTeam2Points then
      ACalc_LeadingTeam=2;
   elseif ACalc_TeamsTable[1]==ACalc_ArenaTeam3Points then
      ACalc_LeadingTeam=3;
   end
   if ACalc_TeamsTable[2]==ACalc_ArenaTeam1Points then
      ACalc_SecondTeam=1;
   elseif ACalc_TeamsTable[2]==ACalc_ArenaTeam2Points then
      ACalc_SecondTeam=2;
   elseif ACalc_TeamsTable[2]==ACalc_ArenaTeam3Points then
      ACalc_SecondTeam=3;
   end
   if ACalc_TeamsTable[3]==ACalc_ArenaTeam1Points then
      ACalc_ThirdTeam=1;
   elseif ACalc_TeamsTable[3]==ACalc_ArenaTeam2Points then
      ACalc_ThirdTeam=2;
   elseif ACalc_TeamsTable[3]==ACalc_ArenaTeam3Points then
      ACalc_ThirdTeam=3;
   end

   -- Check which team is to be recolored.
   if ACalc_LeadingTeam==1 and ACalc_ArenaTeam1playerPlayedPct>=30 then
      ACalc_TeamRecolor=1
   elseif ACalc_LeadingTeam==2 and ACalc_ArenaTeam2playerPlayedPct>=30 then
      ACalc_TeamRecolor=2
   elseif ACalc_LeadingTeam==3 and ACalc_ArenaTeam3playerPlayedPct>=30 then
      ACalc_TeamRecolor=3
   elseif ACalc_SecondTeam==1 and ACalc_ArenaTeam1playerPlayedPct>=30 then
      ACalc_TeamRecolor=1
   elseif ACalc_SecondTeam==2 and ACalc_ArenaTeam2playerPlayedPct>=30 then
      ACalc_TeamRecolor=2
   elseif ACalc_SecondTeam==3 and ACalc_ArenaTeam3playerPlayedPct>=30 then
      ACalc_TeamRecolor=3
   elseif ACalc_ThirdTeam==1 and ACalc_ArenaTeam1playerPlayedPct>=30 then
      ACalc_TeamRecolor=1
   elseif ACalc_ThirdTeam==2 and ACalc_ArenaTeam2playerPlayedPct>=30 then
      ACalc_TeamRecolor=2
   elseif ACalc_ThirdTeam==3 and ACalc_ArenaTeam3playerPlayedPct>=30 then
      ACalc_TeamRecolor=3
   end

   
   -- Get max points, and see if there is a need to recolor total arena points.
   local ACalc_max=0;
   local ACalc_maxrecolor=0;
   if ACalc_TeamRecolor==1 then
      ACalc_max=ACalc_ArenaTeam1Points;
   elseif ACalc_TeamRecolor==2 then
      ACalc_max=ACalc_ArenaTeam2Points;
   elseif ACalc_TeamRecolor==3 then
      ACalc_max=ACalc_ArenaTeam3Points;
   else
      ACalc_max=math.max(ACalc_ArenaTeam1Points, ACalc_ArenaTeam2Points, ACalc_ArenaTeam3Points);
      ACalc_maxrecolor=1;
   end

   --Learn which team is displayed in which slot, by comparing names.
   local ACalc_ArenaTeam1Slot=0;
   local ACalc_ArenaTeam2Slot=0;
   local ACalc_ArenaTeam3Slot=0;

   if ACalc_ArenaTeam1Name == PVPTeam1DataName:GetText() and PVPTeam1DataName:GetText() then
      ACalc_ArenaTeam1Slot=1;
   elseif ACalc_ArenaTeam1Name == PVPTeam2DataName:GetText() and PVPTeam2DataName:GetText() then
      ACalc_ArenaTeam1Slot=2;
   elseif ACalc_ArenaTeam1Name == PVPTeam3DataName:GetText() and PVPTeam3DataName:GetText() then
      ACalc_ArenaTeam1Slot=3;
   end
   if ACalc_ArenaTeam2Name == PVPTeam1DataName:GetText() and PVPTeam1DataName:GetText() then
      ACalc_ArenaTeam2Slot=1;
   elseif ACalc_ArenaTeam2Name == PVPTeam2DataName:GetText() and PVPTeam2DataName:GetText() then
      ACalc_ArenaTeam2Slot=2;
   elseif ACalc_ArenaTeam2Name == PVPTeam3DataName:GetText() and PVPTeam3DataName:GetText() then
      ACalc_ArenaTeam2Slot=3;
   end
   if ACalc_ArenaTeam3Name == PVPTeam1DataName:GetText() and PVPTeam1DataName:GetText() then
      ACalc_ArenaTeam3Slot=1;
   elseif ACalc_ArenaTeam3Name == PVPTeam2DataName:GetText() and PVPTeam2DataName:GetText() then
      ACalc_ArenaTeam3Slot=2;
   elseif ACalc_ArenaTeam3Name == PVPTeam3DataName:GetText() and PVPTeam3DataName:GetText() then
      ACalc_ArenaTeam3Slot=3;
   end

   local ACalc_MainCalcArray={ACalc_ArenaTeam1Points, ACalc_ArenaTeam2Points, ACalc_ArenaTeam3Points, ACalc_ArenaTeam1Rating, ACalc_ArenaTeam2Rating, ACalc_ArenaTeam3Rating, ACalc_max, ACalc_maxrecolor, ACalc_TeamRecolor, ACalc_ArenaTeam1Slot, ACalc_ArenaTeam2Slot, ACalc_ArenaTeam3Slot}
   return ACalc_MainCalcArray;
end



------------------------------------
-------- START MAIN FUNCTION -------
------------------------------------


function ACalc_Main()

   ACalc_Honor();

   local ACalc_ArenaTeam1Points, ACalc_ArenaTeam2Points, ACalc_ArenaTeam3Points, ACalc_ArenaTeam1Rating, ACalc_ArenaTeam2Rating, ACalc_ArenaTeam3Rating, ACalc_max, ACalc_maxrecolor, ACalc_TeamRecolor, ACalc_ArenaTeam1Slot, ACalc_ArenaTeam2Slot, ACalc_ArenaTeam3Slot = unpack(ACalc_MainCalc());


   -- If option to show teams points in PVP frame is on, show them. Recolor if setting to do so is on.
   if ACalc_SavedOpt.ShowTeams==1 then
      -- If team exists, print ACalc info in respective slot.
      if ACalc_ArenaTeam1Slot>0 then
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRatingLabel"):ClearAllPoints();
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRatingLabel"):SetPoint("RIGHT",getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataName"),"RIGHT",25,0);
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRatingLabel"):SetText("Rating");
         if ACalc_ArenaTeam1Points>0 then
            getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRating"):SetText(ACalc_ArenaTeam1Rating.." ("..ACalc_ArenaTeam1Points..")");
         end
         if ACalc_SavedOpt.Recolor==1 then
            if ACalc_TeamRecolor==1 then
               getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRating"):SetText(ACalc_ArenaTeam1Rating.." |cAA99FF00("..ACalc_ArenaTeam1Points..")|r");
            end
         end
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRating"):SetJustifyH("CENTER");
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRating"):SetWidth(70);
      end

      if ACalc_ArenaTeam2Slot>0 then
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRatingLabel"):ClearAllPoints();
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRatingLabel"):SetPoint("RIGHT",getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataName"),"RIGHT",25,0);
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRatingLabel"):SetText("Rating");
         if ACalc_ArenaTeam2Points>0 then
            getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRating"):SetText(ACalc_ArenaTeam2Rating.." ("..ACalc_ArenaTeam2Points..")");
         end
         if ACalc_SavedOpt.Recolor==1 then
            if ACalc_TeamRecolor==2 then
               getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRating"):SetText(ACalc_ArenaTeam2Rating.." |cAA99FF00("..ACalc_ArenaTeam2Points..")|r");
            end
         end
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRating"):SetJustifyH("CENTER");
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRating"):SetWidth(70);
      end

      if ACalc_ArenaTeam3Slot>0 then
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRatingLabel"):ClearAllPoints();
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRatingLabel"):SetPoint("RIGHT",getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataName"),"RIGHT",25,0);
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRatingLabel"):SetText("Rating");
         if ACalc_ArenaTeam3Points>0 then
            getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRating"):SetText(ACalc_ArenaTeam3Rating.." ("..ACalc_ArenaTeam3Points..")");
         end
         if ACalc_SavedOpt.Recolor==1 then
            if ACalc_TeamRecolor==3 then
               getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRating"):SetText(ACalc_ArenaTeam3Rating.." |cAA99FF00("..ACalc_ArenaTeam3Points..")|r");
            end
         end
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRating"):SetJustifyH("CENTER");
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRating"):SetWidth(70);
      end
   else
      if ACalc_ArenaTeam1Slot>0 then
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRatingLabel"):ClearAllPoints();
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRatingLabel"):SetPoint("LEFT", getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataName"), "RIGHT", 0, 0);
         if ACalc_ArenaTeam1Points>0 then
            getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRating"):SetText(ACalc_ArenaTeam1Rating);
         end
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRating"):SetJustifyH("LEFT");
         getglobal("PVPTeam"..ACalc_ArenaTeam1Slot.."DataRating"):SetWidth(60);
      end

      if ACalc_ArenaTeam2Slot>0 then
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRatingLabel"):ClearAllPoints();
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRatingLabel"):SetPoint("LEFT", getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataName"), "RIGHT", 0, 0);
         if ACalc_ArenaTeam2Points>0 then
            getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRating"):SetText(ACalc_ArenaTeam2Rating);
         end
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRating"):SetJustifyH("LEFT");
         getglobal("PVPTeam"..ACalc_ArenaTeam2Slot.."DataRating"):SetWidth(60);
      end

      if ACalc_ArenaTeam3Slot>0 then
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRatingLabel"):ClearAllPoints();
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRatingLabel"):SetPoint("LEFT", getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataName"), "RIGHT", 0, 0);
         if ACalc_ArenaTeam3Points>0 then
            getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRating"):SetText(ACalc_ArenaTeam3Rating);
         end
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRating"):SetJustifyH("LEFT");
         getglobal("PVPTeam"..ACalc_ArenaTeam3Slot.."DataRating"):SetWidth(60);
      end
   end
   
   
   -- Update personal arena rating in the PVP frame.
   local personalRatingMax = math.max(ACalc_personalRating(1), ACalc_personalRating(2), ACalc_personalRating(3));
   ACalc_PersonalRatingRating:SetText(personalRatingMax);


   -- Call the sort arena teams function after all text is set above.
   if ArenaSortLoaded==1 then
      ACalc_SortTeams(ACalc_ArenaTeam1Slot, ACalc_ArenaTeam2Slot, ACalc_ArenaTeam3Slot);
   end


   -- Set up variable with current arena points and total arena points after next update. Also recolor if necessary.
   ACalc_max=ACalc_max+GetArenaCurrency();
   if ACalc_SavedOpt.Recolor==1 and ACalc_maxrecolor==1 then
      ACalc_max=(GetArenaCurrency().." |cAACC3300("..ACalc_max..")|r");
   else
      ACalc_max=(GetArenaCurrency().." ("..ACalc_max..")");
   end


   PVPFrameArenaPoints:SetWidth(120);
   PVPFrameArenaLabel:SetPoint("LEFT", PVPFrameArena, "LEFT", 20, 0);
   PVPFrameArenaIcon:SetPoint("LEFT", PVPFrameArenaLabel, "LEFT", -22, 0);
   PVPFrameArenaPoints:SetPoint("LEFT", PVPFrameArenaLabel, "RIGHT", 8, 0);
   PVPFrameArenaPoints:SetJustifyH("LEFT");



   -- If option to show total arena points is on, show it.
   if ACalc_SavedOpt.ShowTotal==1 then
      PVPFrameArenaPoints:SetText(ACalc_max);
   else
      PVPFrameArenaPoints:SetText(GetArenaCurrency());
   end
end


--------------------------------------------------------
------- ONLOAD, ONEVENT, LOADVARIABLES AND HOOKS -------
--------------------------------------------------------



function ACalc_OnLoad()
   this:RegisterEvent("ADDON_LOADED");
end

function ACalc_OnEvent()
   if (event=="ADDON_LOADED" and arg1=="ArenaCalculator") then
      ACalc_LoadVariables();
   end
   if (event=="ADDON_LOADED" and arg1=="ArenaSort") then
      ArenaSortLoaded=1;
   end
end

-- Load SavedVariables, or set them if there are no saved ones. Call FuncHooks() after.
function ACalc_LoadVariables()
   if not ACalc_SavedOpt then
      ACalc_SavedOpt={};
      ACalc_SavedOpt.ShowTotal=1
      ACalc_SavedOpt.ShowTeams=1
      ACalc_SavedOpt.ShowButton=1
      ACalc_SavedOpt.Recolor=0
      ACalc_SavedOpt.ShowTotalHonor=0
      ACalc_SavedOpt.ShowTotalHonorCond=0
      ACalc_SavedOpt.ACalcFrameScale=1.0
   end
   if ACalc_SavedOpt.ShowTotal==0 then
      ACalcFrameOptionsCheckButton1:SetChecked(false);
   end
   if ACalc_SavedOpt.ShowTeams==0 then
      ACalcFrameOptionsCheckButton2:SetChecked(false);
   end
   if ACalc_SavedOpt.ShowButton==0 then
      ACalcFrameOptionsCheckButton3:SetChecked(false);
      ACalc_ShowFrame:Hide();
   else
      ACalcFrameOptionsCheckButton3:SetChecked(true);
      ACalc_ShowFrame:Show();
   end
   if ACalc_SavedOpt.Recolor==1 then
      ACalcFrameOptionsCheckButton4:SetChecked(true);
   end
   if ACalc_SavedOpt.ShowTotalHonor==1 then
      ACalcFrameOptionsCheckButton5:SetChecked(true);
   end
   if ACalc_SavedOpt.ShowTotalHonorCond==1 then
      ACalcFrameOptionsCheckButton6:SetChecked(true);
   end
   if not ACalc_SavedOpt.ACalcFrameScale then
      ACalc_SavedOpt.ACalcFrameScale=1.0;
   end
   ACalcFrameOptionsSlider1:SetValue(ACalc_SavedOpt.ACalcFrameScale);
   ACalc_FuncHooks();
end

-- Hook the functions that updates arena related info, therefore updating ACalc also.
function ACalc_FuncHooks()
   local oldPVPFrame_Update=PVPFrame_Update
   function newPVPFrame_Update()
      oldPVPFrame_Update()
      ACalc_Main()
   end
   PVPFrame_Update=newPVPFrame_Update
   
   local oldPVPHonor_Update=PVPHonor_Update
   function newPVPHonor_Update()
      oldPVPHonor_Update()
      ACalc_Main()
   end
   PVPHonor_Update=newPVPHonor_Update

   local oldPVPFrameToggleButton_OnClick=PVPFrameToggleButton_OnClick
   function newPVPFrameToggleButton_OnClick() 
      oldPVPFrameToggleButton_OnClick()
      ACalc_Main()
   end
   PVPFrameToggleButton_OnClick=newPVPFrameToggleButton_OnClick
end



------------------------------------
------- CHECKBUTTONS ONCLICK -------
------------------------------------


function ACalcFrameOptionsCheckButton1_OnClick()
   if ACalcFrameOptionsCheckButton1:GetChecked() then
      ACalc_SavedOpt.ShowTotal=1;
      ACalc_Main();
   else
      ACalc_SavedOpt.ShowTotal=0;
      ACalc_Main();
   end
end


function ACalcFrameOptionsCheckButton2_OnClick()
   if ACalcFrameOptionsCheckButton2:GetChecked() then
      ACalc_SavedOpt.ShowTeams=1;
      ACalc_Main();
   else
      ACalc_SavedOpt.ShowTeams=0;
      ACalc_Main();
   end
end


function ACalcFrameOptionsCheckButton3_OnClick()
   if ACalcFrameOptionsCheckButton3:GetChecked() then
      ACalc_SavedOpt.ShowButton=1;
      ACalc_ShowFrame:Show();
   else
      ACalc_SavedOpt.ShowButton=0;
      ACalc_ShowFrame:Hide();
      DEFAULT_CHAT_FRAME:AddMessage("|cAACCFF99You have disabled the Arena Calculator button. If you wish to open the Arena Calculator frame again, use |r/acalc show");
   end
end


function ACalcFrameOptionsCheckButton4_OnClick()
   if ACalcFrameOptionsCheckButton4:GetChecked() then
      ACalc_SavedOpt.Recolor=1;
      ACalc_Main();
   else
      ACalc_SavedOpt.Recolor=0;
      ACalc_Main();
   end
end


function ACalcFrameOptionsCheckButton5_OnClick()
   if ACalcFrameOptionsCheckButton5:GetChecked() then
      ACalc_SavedOpt.ShowTotalHonor=1;
      ACalc_Main();
   else
      ACalc_SavedOpt.ShowTotalHonor=0;
      ACalc_Main();
   end
end


function ACalcFrameOptionsCheckButton6_OnClick()
   if ACalcFrameOptionsCheckButton6:GetChecked() then
      ACalc_SavedOpt.ShowTotalHonorCond=1;
      ACalc_Main();
   else
      ACalc_SavedOpt.ShowTotalHonorCond=0;
      ACalc_Main();
   end
end


function ACalcFrameOptionsSlider1_OnClick()
      ACalc_SavedOpt.ACalcFrameScale=ACalcFrameOptionsSlider1:GetValue();
      ACalcFrame:SetScale(ACalcFrameOptionsSlider1:GetValue());
      ACalcFrame:SetPoint("CENTER", UIParent, "CENTER", 0, 0);
end




----------------------------------------
------- ARENA POINTS CALCULATION -------
----------------------------------------

function ACalc_Calculate(n)
   local points=0;
   local teamName, teamSize, teamRating, teamPlayed, teamWins, playerPlayed, teamRank  = GetArenaTeam(n);
   if teamRating>1500 then
      points=(1511.26)/(1+1639.28*(2.71828^((-0.00412)*teamRating)));
      if teamSize==2 then
         points=math.floor((points)*(1-0.24));
      elseif teamSize==3 then
         points=math.floor((points)*(1-0.12));
      else
         points=math.floor(points);
      end
   elseif teamRating<1501 then
      points=0.22*teamRating+14;
      if points<0 then 
         points=0;
      end
      if teamSize==2 then
         points=math.floor((0.76*points)+ACalc_RoundOff);
      elseif teamSize==3 then
         points=math.floor((0.88*points)+ACalc_RoundOff);
      else
         points=math.floor(points+ACalc_RoundOff)
      end
   end
   return points;
end





---------------------------------------------
------- GET RATINGS, SIZES AND PLAYED -------
---------------------------------------------

function ACalc_Rating(n)
	local teamName, teamSize, teamRating, teamPlayed, teamWins, playerPlayed, teamRank  = GetArenaTeam(n);
	return teamRating;
end


function ACalc_Size(n)
	local teamName, teamSize, teamRating, teamPlayed, teamWins, playerPlayed, teamRank  = GetArenaTeam(n);
	return teamSize;
end


function ACalc_Played(n)
	local teamName, teamSize, teamRating, teamPlayed, teamWins, playerPlayed, teamRank  = GetArenaTeam(n);
	return teamPlayed;
end


function ACalc_playerPlayed(n)
	local teamName, teamSize, teamRating, teamPlayed, teamWins, bleh, blah, playerPlayed  = GetArenaTeam(n);
	return playerPlayed;
end


function ACalc_teamName(n)
	local teamName, teamSize, teamRating, teamPlayed, teamWins, playerPlayed, teamRank  = GetArenaTeam(n);
	return teamName;
end


function ACalc_personalRating(n)
	local teamName, teamSize, teamRating, teamPlayed, teamWins, playerPlayed, teamRank, blah, bleh, bloh, personalRating  = GetArenaTeam(n);
	return personalRating;
end