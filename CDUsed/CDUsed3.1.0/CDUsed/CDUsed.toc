## Interface: 30300
## Title: Cooldown Used
## Author: Kiki
## Version: 3.1
## Notes: Inform the raid when someone uses a cooldown
## OptionalDeps: GroupAnalyse, sct, Prat-3.0_Libraries, MikScrollingBattleText
## SavedVariablesPerCharacter: CU_Config, CU_Spells
## SavedVariables: CU_SpellsTemplates

## PERSONNAL EMBEDDED LIBS
# GroupAnalyse
lib\GroupAnalyse\GroupAnalyse.xml

# Localization
localization.lua

# Main code
CU_defaults.lua
CDUsed.xml

# Plugins
plugins\kick.xml
plugins\feasts.lua
