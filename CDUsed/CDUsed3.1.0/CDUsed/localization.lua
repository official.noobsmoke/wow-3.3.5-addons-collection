﻿--[[
  Cooldown Used by Kiki/Espêrance - European Conseil des Ombres
   Localization file
]]

------------- French
if(GetLocale() == "frFR")
then
  CU_ZONE_WINTERGRASP = "Joug-d'hiver";
  CU_MENU_SPELLS = "Sorts suivis";
  CU_MENU_ANNOUNCES = "Annonces";
  CU_ALPHA_VALUE = "Transparence";
  CU_MENU_ENABLES = "Activation";
  CU_MENU_MODULES = "Modules";
  CU_MENU_COMBAT_TEXT = "Texte de combat";
  CU_MENU_HIDE_MAIN_WINDOW = "Cacher la fenêtre";
  CU_MENU_CT_BLIZZARD = "Blizzard Combat Text";
  CU_MENU_CT_SCT = "SCT";
  CU_MENU_CT_MSBT = "MikSBT";
  CU_MENU_ADD_SPELL = "Ajouter un nouveau sort";
  CU_MENU_SPELLS_TEMPLATES = "Modèles";
  CU_MENU_SPELLS_TEMPLATES_LOAD_DEFAULT = "Charger modèle par défaut";
  CU_MENU_SPELLS_TEMPLATES_DEFAULT_ALL = "Tout";
  CU_MENU_SPELLS_TEMPLATES_DEFAULT_TANK = "Tank";
  CU_MENU_SPELLS_TEMPLATES_DEFAULT_HEAL = "Soigneur";
  CU_MENU_SPELLS_TEMPLATES_DEFAULT_KICKER = "Interrupteur";
  CU_MENU_SPELLS_TEMPLATES_LOAD_CUSTOM = "Charger modèle perso";
  CU_MENU_SPELLS_TEMPLATES_SAVE_CUSTOM = "Sauver modèle perso";

------------- English
else
  CU_ZONE_WINTERGRASP = "Wintergrasp";
  CU_MENU_SPELLS = "Monitored Spells";
  CU_MENU_ANNOUNCES = "Announces";
  CU_ALPHA_VALUE = "Transparency";
  CU_MENU_ENABLES = "Enables";
  CU_MENU_MODULES = "Modules";
  CU_MENU_COMBAT_TEXT = "Combat text";
  CU_MENU_HIDE_MAIN_WINDOW = "Hide Window";
  CU_MENU_CT_BLIZZARD = "Blizzard Combat Text";
  CU_MENU_CT_SCT = "SCT";
  CU_MENU_CT_MSBT = "MikSBT";
  CU_MENU_ADD_SPELL = "Add new spell";
  CU_MENU_SPELLS_TEMPLATES = "Templates";
  CU_MENU_SPELLS_TEMPLATES_LOAD_DEFAULT = "Load default template";
  CU_MENU_SPELLS_TEMPLATES_DEFAULT_ALL = "Everything";
  CU_MENU_SPELLS_TEMPLATES_DEFAULT_TANK = "Tank";
  CU_MENU_SPELLS_TEMPLATES_DEFAULT_HEAL = "Heal";
  CU_MENU_SPELLS_TEMPLATES_DEFAULT_KICKER = "Kicker";
  CU_MENU_SPELLS_TEMPLATES_LOAD_CUSTOM = "Load custom template";
  CU_MENU_SPELLS_TEMPLATES_SAVE_CUSTOM = "Save custom template";

end
