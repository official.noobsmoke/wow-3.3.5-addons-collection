local _,TriviaBot_Questions = ...
TriviaBot_Questions[1] =
	{
		["Categories"] = {
			"Dragon soul", -- [1]
		},
		["Description"] = "4.3.3",
		["Author"] = "Marchello - Aszune / zopa34",
		["Title"] = "Dragon Soul quiz",
		["Category"] = {
			1, -- [1]
			1, -- [2]
			1, -- [3]
			1, -- [4]
			1, -- [5]
			1, -- [6]
			1, -- [7]
			1, -- [8]
			1, -- [9]
			1, -- [10]
			1, -- [11]
			1, -- [12]
			1, -- [13]
			1, -- [14]
			1, -- [15]
			1, -- [16]
			1, -- [17]
			1, -- [18]
			1, -- [19]
			1, -- [20]
			1, -- [21]
			1, -- [22]
			1, -- [23]
			1, -- [24]
			1, -- [25]
			1, -- [26]
			1, -- [27]
			1, -- [28]
			1, -- [29]
			1, -- [30]
			1, -- [31]
			1, -- [32]
			1, -- [33]
			1, -- [34]
			1, -- [35]
			1, -- [36]
			1, -- [37]
			1, -- [38]
			1, -- [39]
			1, -- [40]
			1, -- [41]
			1, -- [42]
			1, -- [43]
			1, -- [44]
			1, -- [45]
			1, -- [46]
			1, -- [47]
			1, -- [48]
			1, -- [49]
			1, -- [50]
			1, -- [51]
			1, -- [52]
			1, -- [53]
			1, -- [54]
			1, -- [55]
			1, -- [56]
			1, -- [57]
			1, -- [58]
			1, -- [59]
			1, -- [60]
			1, -- [61]
			1, -- [62]
			1, -- [63]
			1, -- [64]
			1, -- [65]
			1, -- [66]
			1, -- [67]
			1, -- [68]
			1, -- [69]
			1, -- [70]
			1, -- [71]
			1, -- [72]
			1, -- [73]
			1, -- [74]
			1, -- [75]
			1, -- [76]
			1, -- [77]
			1, -- [78]
			1, -- [79]
			1, -- [80]
			1, -- [81]
			1, -- [82]
			1, -- [83]
			1, -- [84]
			1, -- [85]
			1, -- [86]
			1, -- [87]
			1, -- [88]
			1, -- [89]
			1, -- [90]
			1, -- [91]
			1, -- [92]
			1, -- [93]
			1, -- [94]
			1, -- [95]
			1, -- [96]
			1, -- [97]
			1, -- [98]
			1, -- [99]
			1, -- [100]
			1, -- [101]
			1, -- [102]
			1, -- [103]
		},
		["Answers"] = {
			{
				"Dragon soul", -- [1]
			}, -- [1]
			{
				"Morchok", -- [1]
			}, -- [2]
			{
				"Stomp", -- [1]
			}, -- [3]
			{
				"Ozruk", -- [1]
			}, -- [4]
			{
				"Cavern of times", -- [1]
				"Cavern of time", -- [2]
				"Caverns of the time", -- [3]
				"Caverns of time", -- [4]
			}, -- [5]
			{
				"Ultraxion", -- [1]
			}, -- [6]
			{
				"Kohcrom", -- [1]
			}, -- [7]
			{
				"Deathwing", -- [1]
				"Madness of Deathwing", -- [2]
			}, -- [8]
			{
				"Warmaster Blackhorn", -- [1]
				"Blackhorn", -- [2]
				"Warmaste", -- [3]
			}, -- [9]
			{
				"Globule", -- [1]
				"globules", -- [2]
			}, -- [10]
			{
				"4", -- [1]
				"Four", -- [2]
				"four", -- [3]
			}, -- [11]
			{
				"Dark", -- [1]
				"Black", -- [2]
			}, -- [12]
			{
				"Cobalt", -- [1]
				"blue", -- [2]
			}, -- [13]
			{
				"Crimson", -- [1]
				"Red", -- [2]
			}, -- [14]
			{
				"Ultraxion", -- [1]
			}, -- [15]
			{
				"15", -- [1]
				"15 minutes", -- [2]
				"alot", -- [3]
			}, -- [16]
			{
				"Roll", -- [1]
				"Barrel roll", -- [2]
			}, -- [17]
			{
				"Tendon", -- [1]
				"Burning tendon", -- [2]
			}, -- [18]
			{
				"Corruption", -- [1]
			}, -- [19]
			{
				"10", -- [1]
				"ten", -- [2]
			}, -- [20]
			{
				"Warlord Zon'ozz", -- [1]
				"zon", -- [2]
				"ozz", -- [3]
			}, -- [21]
			{
				"Psychic Drain", -- [1]
			}, -- [22]
			{
				"Wyrmrest tample", -- [1]
				"Wyrmrest", -- [2]
			}, -- [23]
			{
				"Dragonflights", -- [1]
				"Dragon aspects", -- [2]
			}, -- [24]
			{
				"Dragon Soul", -- [1]
				"Dragons Soul", -- [2]
			}, -- [25]
			{
				"Binding Crystal", -- [1]
			}, -- [26]
			{
				"Focused Assault", -- [1]
			}, -- [27]
			{
				"Ice Lance", -- [1]
			}, -- [28]
			{
				"Heart of the Aspects", -- [1]
			}, -- [29]
			{
				"Ice wall", -- [1]
				"Ice Wave", -- [2]
			}, -- [30]
			{
				"Crystal Conductor", -- [1]
				"Conductor", -- [2]
			}, -- [31]
			{
				"Ice Tomb", -- [1]
			}, -- [32]
			{
				"Tauren", -- [1]
				"Moo", -- [2]
				"Cow", -- [3]
			}, -- [33]
			{
				"Furious", -- [1]
			}, -- [34]
			{
				"20", -- [1]
				"20%", -- [2]
				"20 percent", -- [3]
				"twenty", -- [4]
			}, -- [35]
			{
				"Rogue", -- [1]
				"Rogues", -- [2]
			}, -- [36]
			{
				"397", -- [1]
			}, -- [37]
			{
				"403", -- [1]
			}, -- [38]
			{
				"416", -- [1]
			}, -- [39]
			{
				"Maw of the Dragonlord", -- [1]
				"Maw of Dragonlord", -- [2]
				"Dragonlord maw", -- [3]
			}, -- [40]
			{
				"Kiril", -- [1]
				"Kiril, Fury of beasts", -- [2]
			}, -- [41]
			{
				"Essence of Destruction", -- [1]
				"Destruction Essence", -- [2]
			}, -- [42]
			{
				"384", -- [1]
			}, -- [43]
			{
				"410", -- [1]
			}, -- [44]
			{
				"Eruption", -- [1]
				"Twilight Eruption", -- [2]
			}, -- [45]
			{
				"Heroic Will", -- [1]
				"Button", -- [2]
			}, -- [46]
			{
				"Fading Light", -- [1]
			}, -- [47]
			{
				"Alexstrasza", -- [1]
			}, -- [48]
			{
				"Gift of Life", -- [1]
			}, -- [49]
			{
				"Ysera", -- [1]
			}, -- [50]
			{
				"Kalecgos", -- [1]
			}, -- [51]
			{
				"Timeloop", -- [1]
			}, -- [52]
			{
				"4", -- [1]
				"four", -- [2]
			}, -- [53]
			{
				"Yes", -- [1]
				"Moo", -- [2]
			}, -- [54]
			{
				"Dragonkin", -- [1]
			}, -- [55]
			{
				"Gariona", -- [1]
			}, -- [56]
			{
				"Twilight Sapper", -- [1]
				"Sapper", -- [2]
			}, -- [57]
			{
				"Shockwave", -- [1]
			}, -- [58]
			{
				"Frostflake", -- [1]
			}, -- [59]
			{
				"Icicle", -- [1]
			}, -- [60]
			{
				"300k", -- [1]
				"300000", -- [2]
				"300'000", -- [3]
			}, -- [61]
			{
				"2", -- [1]
				"two", -- [2]
			}, -- [62]
			{
				"1", -- [1]
				"One", -- [2]
			}, -- [63]
			{
				"Wipe", -- [1]
				"die", -- [2]
				"rage", -- [3]
				"wtf", -- [4]
			}, -- [64]
			{
				"Eye of eternity", -- [1]
			}, -- [65]
			{
				"Orc", -- [1]
			}, -- [66]
			{
				"FIRE", -- [1]
			}, -- [67]
			{
				"Trinket", -- [1]
				"Ring", -- [2]
			}, -- [68]
			{
				"six", -- [1]
				"6", -- [2]
			}, -- [69]
			{
				"six", -- [1]
				"6", -- [2]
			}, -- [70]
			{
				"four", -- [1]
				"4", -- [2]
			}, -- [71]
			{
				"Mage", -- [1]
			}, -- [72]
			{
				"five", -- [1]
				"5", -- [2]
			}, -- [73]
			{
				"Mutated Corruption", -- [1]
			}, -- [74]
			{
				"Thrall", -- [1]
			}, -- [75]
			{
				"Hemorrhage", -- [1]
			}, -- [76]
			{
				"Elementium Bolt", -- [1]
				"Elementium", -- [2]
			}, -- [77]
			{
				"Dream", -- [1]
			}, -- [78]
			{
				"Shadow", -- [1]
				"Purple", -- [2]
			}, -- [79]
			{
				"Yellow", -- [1]
				"Glowing", -- [2]
			}, -- [80]
			{
				"Crush Armor", -- [1]
			}, -- [81]
			{
				"Resonating Crystal", -- [1]
			}, -- [82]
			{
				"Earth", -- [1]
				"Earths", -- [2]
			}, -- [83]
			{
				"Disrupting Shadows", -- [1]
			}, -- [84]
			{
				"Void", -- [1]
				"Void bolt", -- [2]
			}, -- [85]
			{
				"Impale", -- [1]
			}, -- [86]
			{
				"Druid Death knight", -- [1]
				"Druid DK", -- [2]
				"Druid and death knight", -- [3]
				"druid and dk", -- [4]
				"druid and deathknight", -- [5]
				"druid deathknight", -- [6]
			}, -- [87]
			{
				"Corrupted Protector", -- [1]
				"Protector", -- [2]
			}, -- [88]
			{
				"Lightning rod", -- [1]
			}, -- [89]
			{
				"Warmaster Blackhorn", -- [1]
				"Blackhorn", -- [2]
			}, -- [90]
			{
				"Ultraxion", -- [1]
			}, -- [91]
			{
				"Vial of shadows", -- [1]
			}, -- [92]
			{
				"25", -- [1]
			}, -- [93]
			{
				"Hour of twilight", -- [1]
			}, -- [94]
			{
				"Queen Azshara", -- [1]
				"Azshara", -- [2]
			}, -- [95]
			{
				"Alizabal", -- [1]
			}, -- [96]
			{
				"Alizabal", -- [1]
			}, -- [97]
			{
				"Thrall", -- [1]
			}, -- [98]
			{
				"Hagara", -- [1]
			}, -- [99]
			{
				"Zon'ozz", -- [1]
			}, -- [100]
			{
				"Ultraxion", -- [1]
			}, -- [101]
			{
				"Yor'sahj", -- [1]
			}, -- [102]
			{
				"Blackhorn", -- [1]
			}, -- [103]
		},
		["Points"] = {
			1, -- [1]
			1, -- [2]
			1, -- [3]
			1, -- [4]
			1, -- [5]
			1, -- [6]
			1, -- [7]
			1, -- [8]
			1, -- [9]
			1, -- [10]
			1, -- [11]
			1, -- [12]
			1, -- [13]
			1, -- [14]
			1, -- [15]
			1, -- [16]
			1, -- [17]
			1, -- [18]
			1, -- [19]
			1, -- [20]
			1, -- [21]
			1, -- [22]
			1, -- [23]
			1, -- [24]
			1, -- [25]
			1, -- [26]
			1, -- [27]
			1, -- [28]
			1, -- [29]
			1, -- [30]
			1, -- [31]
			1, -- [32]
			1, -- [33]
			1, -- [34]
			1, -- [35]
			1, -- [36]
			1, -- [37]
			1, -- [38]
			1, -- [39]
			1, -- [40]
			1, -- [41]
			1, -- [42]
			1, -- [43]
			1, -- [44]
			1, -- [45]
			1, -- [46]
			1, -- [47]
			1, -- [48]
			1, -- [49]
			1, -- [50]
			1, -- [51]
			1, -- [52]
			1, -- [53]
			1, -- [54]
			1, -- [55]
			1, -- [56]
			1, -- [57]
			1, -- [58]
			1, -- [59]
			1, -- [60]
			1, -- [61]
			1, -- [62]
			1, -- [63]
			1, -- [64]
			1, -- [65]
			1, -- [66]
			1, -- [67]
			1, -- [68]
			1, -- [69]
			1, -- [70]
			1, -- [71]
			1, -- [72]
			1, -- [73]
			1, -- [74]
			1, -- [75]
			1, -- [76]
			1, -- [77]
			1, -- [78]
			1, -- [79]
			1, -- [80]
			1, -- [81]
			1, -- [82]
			1, -- [83]
			1, -- [84]
			1, -- [85]
			1, -- [86]
			1, -- [87]
			1, -- [88]
			1, -- [89]
			1, -- [90]
			1, -- [91]
			1, -- [92]
			1, -- [93]
			1, -- [94]
			1, -- [95]
			1, -- [96]
			1, -- [97]
			1, -- [98]
			1, -- [99]
			1, -- [100]
			1, -- [101]
			1, -- [102]
			1, -- [103]
		},
		["Hints"] = {
			{
				"DS", -- [1]
			}, -- [1]
			{
			}, -- [2]
			{
			}, -- [3]
			{
			}, -- [4]
			{
				"It's located in tanaris", -- [1]
			}, -- [5]
			{
			}, -- [6]
			{
			}, -- [7]
			{
			}, -- [8]
			{
			}, -- [9]
			{
			}, -- [10]
			{
			}, -- [11]
			{
			}, -- [12]
			{
			}, -- [13]
			{
			}, -- [14]
			{
			}, -- [15]
			{
			}, -- [16]
			{
			}, -- [17]
			{
			}, -- [18]
			{
			}, -- [19]
			{
			}, -- [20]
			{
			}, -- [21]
			{
			}, -- [22]
			{
			}, -- [23]
			{
			}, -- [24]
			{
			}, -- [25]
			{
			}, -- [26]
			{
			}, -- [27]
			{
			}, -- [28]
			{
			}, -- [29]
			{
			}, -- [30]
			{
			}, -- [31]
			{
			}, -- [32]
			{
			}, -- [33]
			{
			}, -- [34]
			{
			}, -- [35]
			{
			}, -- [36]
			{
			}, -- [37]
			{
			}, -- [38]
			{
			}, -- [39]
			{
			}, -- [40]
			{
			}, -- [41]
			{
			}, -- [42]
			{
			}, -- [43]
			{
			}, -- [44]
			{
			}, -- [45]
			{
			}, -- [46]
			{
			}, -- [47]
			{
			}, -- [48]
			{
			}, -- [49]
			{
			}, -- [50]
			{
			}, -- [51]
			{
			}, -- [52]
			{
			}, -- [53]
			{
			}, -- [54]
			{
			}, -- [55]
			{
			}, -- [56]
			{
			}, -- [57]
			{
			}, -- [58]
			{
			}, -- [59]
			{
			}, -- [60]
			{
			}, -- [61]
			{
			}, -- [62]
			{
			}, -- [63]
			{
			}, -- [64]
			{
			}, -- [65]
			{
			}, -- [66]
			{
			}, -- [67]
			{
			}, -- [68]
			{
			}, -- [69]
			{
			}, -- [70]
			{
			}, -- [71]
			{
			}, -- [72]
			{
			}, -- [73]
			{
			}, -- [74]
			{
			}, -- [75]
			{
			}, -- [76]
			{
			}, -- [77]
			{
			}, -- [78]
			{
			}, -- [79]
			{
			}, -- [80]
			{
			}, -- [81]
			{
			}, -- [82]
			{
			}, -- [83]
			{
			}, -- [84]
			{
			}, -- [85]
			{
			}, -- [86]
			{
			}, -- [87]
			{
			}, -- [88]
			{
			}, -- [89]
			{
			}, -- [90]
			{
			}, -- [91]
			{
			}, -- [92]
			{
			}, -- [93]
			{
			}, -- [94]
			{
			}, -- [95]
			{
			}, -- [96]
			{
			}, -- [97]
			{
			}, -- [98]
			{
			}, -- [99]
			{
			}, -- [100]
			{
			}, -- [101]
			{
			}, -- [102]
			{
			}, -- [103]
		},
		["Question"] = {
			"What is 4.3 introduced raid is called?", -- [1]
			"First boss of DS is?", -- [2]
			"What Morchok's ability does damage to all raid members within 25 yards?", -- [3]
			"Morchok looks alot like _____ from The Stonecore.", -- [4]
			"Where is entrance to Dragon Soul?", -- [5]
			"Gear/dps check boss?", -- [6]
			"What's name of Morchok's evil brother?(HC encounter)", -- [7]
			"Last boss of expansion?", -- [8]
			"What boss is sometimes called \"steak boss\"?", -- [9]
			"What's name of oozes in Yor'sahj the Unsleeping encounter?", -- [10]
			"How many oozes are comming to Yor'sahj in heroic mode?", -- [11]
			"What Globule is spawning adds in Yor'sahj encounter?", -- [12]
			"What Globule is draining all mana and making orb in Yor'sahj encounter?", -- [13]
			"What Globule does fire damage to raid in Yor'sahj encounter?", -- [14]
			"Before which boss is most hated trash in Dragon Soul?", -- [15]
			"How long is enrage timer for Madness of Deathwing?", -- [16]
			"What Deathwing does when every on is standin on one side on Spine of Deathwing?", -- [17]
			"What do you need to burst on Spine of Deathwing?", -- [18]
			"What's name of tentacle on Deathwings back?", -- [19]
			"How many stacks Void of Unmaking must do for achievement \"Ping Pong Champion\"?", -- [20]
			"Which boss sometimes is called \"Ping pong boss\"?", -- [21]
			"How is Warlods Zon'ozz's cone aoe attack is called?", -- [22]
			"What's name of tower/tample in middle of DS?", -- [23]
			"Who helps Thrall to kill Deathwing?(How they are called all together)", -- [24]
			"What does Thrall uses to kill Deathwing?", -- [25]
			"What's name of Frozen crystal in Hagara's encounter?", -- [26]
			"What Hagara's ability/attack does heavy damage on tank?", -- [27]
			"What Hagara's ability deals damage and slows attack speed?", -- [28]
			"What's name of mount that was released with 4.3.3 patch?", -- [29]
			"What usally kills noobs in Hagaras encounter?", -- [30]
			"What do you need to connect with electicity in Hagaras encounter?", -- [31]
			"What Hagara's ability puts player into ice block?", -- [32]
			"What race is Warmaster Blackhorn?", -- [33]
			"What's name of Morchok's enrage?", -- [34]
			"At what health amount does Morchok enrages?", -- [35]
			"What class got legendary weapon in 4.3?", -- [36]
			"What ilvl items first six boses drop in normal mode?", -- [37]
			"What ilvl items deathwing drop in normal mode?", -- [38]
			"What ilvl are legendary daggers?", -- [39]
			"What is name of best healer's weapon in Dragon soul that drops from Deathwing?", -- [40]
			"What's name of agility staff that increases your size and drops from Deathwing?", -- [41]
			"How is called crafting material that drops from DS?", -- [42]
			"What ilvl drops from first 6 boses on LFR difficulty?", -- [43]
			"What ilvl drops from first 6 boses on Heroic mode?", -- [44]
			"What Ultraxion casts when there are not enought people out side when he casts Hour of Twilight?", -- [45]
			"What's name of button in Ultraxions encounter?", -- [46]
			"What is name of ability that's randomly put on player in Ultraxion encounter?", -- [47]
			"What Dragonflight gives red crystal?", -- [48]
			"What's name of red crystal in Ultraxion's encounter?", -- [49]
			"What Dragonflight gives green crystal?", -- [50]
			"What Dragonflight gives blue crystal in Ultraxion's encounter?", -- [51]
			"What Nozdormu's ability doesn't let you die one time in Ultraxion's encounter?", -- [52]
			"How many platforms are at Maelstorm?(Madness of Deathwing fight)", -- [53]
			"Thrall is awsome?", -- [54]
			"What kind of creature is Ultraxion?", -- [55]
			"What's name of drake that Warmaster Blackhorn flies on?", -- [56]
			"What's name of small goblin in Blackhorn's encounter?", -- [57]
			"What Warmasters Blackhorns ability does cone aoe damage?", -- [58]
			"What's name of debuff that slows you down in heroic Hagaras encounter?", -- [59]
			"What's name of thing that falls down from sky in frozen phase at Hagara in normal nad heroic?", -- [60]
			"How much damage does Ultraxion's Hour of Twilight?", -- [61]
			"How many people must stay out side and absorb damage when Ultraxion casts Hour of Twilight in 10man heroic?", -- [62]
			"How many people must stay out side and absorb damage when Ultraxion casts Hour of Twilight in 10man normal?", -- [63]
			"What happens after twilight eruption?", -- [64]
			"Where is Hagara located?(Where does portal leads)", -- [65]
			"What race is Hagara?", -- [66]
			"What you must avoid at Ultraxion trash?", -- [67]
			"What usally doesn't drop from DS boses and is world drop?", -- [68]
			"How many groups of three globules are there before Yor'sahj?", -- [69]
			"How many types of Globules are in encounter of Yor'sahj?", -- [70]
			"How many roll you need to do to make Deathwing dizzy(for achievement)?", -- [71]
			"What class can troll before opening of portal to Eye of Eternity?", -- [72]
			"How many Eyes of Go'rath you must kill Heroic Warlord Zon'ozz encounter?", -- [73]
			"What's name of big tentacle in Madness of Deathwing encounter?", -- [74]
			"Who kills Deathwing?", -- [75]
			"What ability spawns oozes in Madness of Deathwing?", -- [76]
			"What bolt you must stop in Madness of Deathwing?", -- [77]
			"What's name of buff that decrease damage taken by 50% in Madness of Deathwing?", -- [78]
			"What Globule makes people explode from healing in Yor'sahj encounter?", -- [79]
			"What Globule makes Yor'sahj do abilities twice as often?", -- [80]
			"What's name of stacking debuff on tanks  in Morchok's encounter?", -- [81]
			"What's name of crystal you must stack in Morchok's encounter?", -- [82]
			"What blood you must avoid in Morchok's encounter?(Who's blood is it?)", -- [83]
			"What's name of debuff that healers must dispell in Zon'ozz encounter?", -- [84]
			"What bolt Yor'sahj is casting?", -- [85]
			"What's name of ability that Mutated Corruption is casting on tank?(Madness of Deathwing)", -- [86]
			"What are two best tanks in Dragon Soul patch?", -- [87]
			"What's name of hunter/warrior/shaman mark?", -- [88]
			"What's name of Int staff that Hagara drops?", -- [89]
			"What is only boss that drops shields?", -- [90]
			"Who drops Imperfect Spiecimens 27 and 28?", -- [91]
			"What world drop trinket has agility on it?", -- [92]
			"How many people are in LFR group?", -- [93]
			"What's name of 3 heroic dungeons introduced in 4.3?(All together)", -- [94]
			"What boss in Well of Eternity has 6 adds and must be interrupted?", -- [95]
			"What boss does \"Bone Storm\" in 4.3?", -- [96]
			"What boss was added in Baradin hold in 4.3?", -- [97]
			"Who must you defend in Hour of Twilight dungeon?", -- [98]
			"What boss drops Shoulder token?(Normal and HC mode)", -- [99]
			"What boss drops Glove token?(Normal and HC mode)", -- [100]
			"What boss drops Chest token?(Normal and HC mode)", -- [101]
			"What boss drops Pants token?(Normal and HC mode)", -- [102]
			"What boss drops Head token?(Normal and HC mode)", -- [103]
		},
	}

