## Interface: 40300
## Title: Dragon Soul quiz
## Notes: Questions about 4.3 patch and DS
## Author: Marchello@Aszune / zopa34

# add the name of your question file below this line \/
TriviaQuestions.lua

# ---------------------------------------------------
# DO NOT EDIT OR REMOVE ANYTHING BETWEEN THESE LINES!
# ---------------------------------------------------
## X-TriviaBot-Questions: 1
## LoadOnDemand: 1
## Dependencies: TriviaBot
core.lua
# ---------------------------------------------------
# DO NOT EDIT OR REMOVE ANYTHING BETWEEN THESE LINES!
# ---------------------------------------------------