--[[
  TriviaBot Version 2.5.4 for World of Warcraft 3.3
  Originally written by Guri of Trollbane
  Based on work created by Psy of Frostwolf
  Code blocks written by ReZeftY and StrangeWill modified
  Rewritten by KeeZ (Araina of EU-Agamaggan)
--]]

-- Local vars declared with TB_
-- TriviaBot version
local TB_VERSION = "2.5.4";

-- Declared colour codes for console messages
local TB_RED     = "|cffff0000";
local TB_MAGENTA = "|cffff00ff";
local TB_WHITE   = "|cffffffff";

-- Used for checking if the player is in a battleground
local TB_AB = "Arathi Basin";
local TB_WSG = "Warsong Gulch";
local TB_AV = "Alterac Valley";
local TB_EOTS = "Eye of the Storm";
local TB_SOTA = "Strand of the Ancients";

-- Control flags
local TB_Running = false; -- To check if TriviaBot is started
local TB_Accept_Answers = false; -- Whether or not answers are accepted
local TB_Loaded = false; -- Set to true when TriviaBot is fully loaded
local TB_Player_Entered = false;
local TB_Initialized = false; -- Flag to initialize the GUI

-- Control counters
local TB_Active_Question = 0; -- The currently active question
local TB_Report_Counter = 0; -- Count for how many questions have been asked
local TB_Round_Counter = 0; -- Count for which question we're on in a round
local TB_Hint_Counter = 0; -- What hint was sent before
local TB_Question_Starttime = 0; -- When the question was asked
local TB_Question_Hinttime = 0; -- When hints can be sent

-- Control arrays
local TB_Question_Order = {}; -- The order in which the questions will be asked
local TB_Question_List = {}; -- Current list of questions to use
local TB_Game_Scores = {}; -- Round scores
      TB_Game_Scores['Best_Win_Streak'] = {};
      TB_Game_Scores['Temp_Win_Streak'] = {};
      TB_Game_Scores['Speed'] = {};
      TB_Game_Scores['Player_Scores'] = {};
local TB_Genders = {"its", "his", "her"}; -- Gender selection
local TB_Question_Sets = {}; -- Faster listing of question-sets and categories
local TB_Questions = {}; -- Pointer to the active question set
local TB_Schedule = {}; -- The array used for scheduling events
local TB_Chat_Restricted = {"general", "trade", "lookingforgroup", "guildrecruitment", "localdefense", "worlddefense"}; -- Restricted channels for use of TriviaBot
local TB_Caps_Restricted = {"a", "and", "as", "at", "in", "of", "on", "the", "to", "vs"}; -- Words that won't be capitalized

-- Channel usage
local TB_NewChannel; -- Used for changing custom channel
local TB_Zone = GetRealZoneText(); -- Stores the current zone
local TB_ChatFrame_MessageEventHandler = ChatFrame_MessageEventHandler; -- Hook the MessageEventHandler
local TB_Message_Prefix = "!tb"; -- Incoming whispers prefix
local TB_Short_Prefix = "[TB]"; -- Short tag prefix
local TB_Long_Prefix = "[TriviaBot]"; -- Long tag prefix
local TB_Channel_Prefix = TB_Long_Prefix; -- Channel/Console prefix

-- Global vars declared with TriviaBot_
TriviaBot_Config = {}; -- Configuration array
TriviaBot_Questions = {}; -- All question-sets
TriviaBot_Scores = {}; -- Player scores
TriviaBot_Scores['Win_Streak'] = {};
TriviaBot_Scores['Speed'] = {};
TriviaBot_Scores['Player_Scores'] = {};

----------------------------------------------------------------------------
-- ChatFrame_MessageEventHandler Hook
----------------------------------------------------------------------------
function ChatFrame_MessageEventHandler(self, event, ...)
	local arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12 = ...;
   if (not TriviaBot_Config['Show_Whispers']) then
      if (event == "CHAT_MSG_WHISPER") then
         if TriviaBot_Starts(string.lower(arg1), string.lower(TB_Message_Prefix)) then
            return;
         end
      elseif (event == "CHAT_MSG_WHISPER_INFORM") then
         if TriviaBot_Starts(string.lower(arg1), string.lower(TB_Short_Prefix)) then
            return;
         end
      end
   end
	return TB_ChatFrame_MessageEventHandler(self, event, ...);
end

----------------------------------------------------------------------------
-- Core functions
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Abort current question
----------------------------------------------------------------------------
function TriviaBot_AbortQuestion()
   if (TB_Running) then
      TriviaBot_UnSchedule("all");
      TB_Accept_Answers = false;
   else
      TriviaBot_PrintError("No game running!");
   end
end

----------------------------------------------------------------------------
-- Ask a question
----------------------------------------------------------------------------
function TriviaBot_AskQuestion()
   TB_Active_Question = TB_Active_Question + 1;

   -- Check if there is questions left
   if (TB_Active_Question == #TB_Questions['Question'] + 1) then
      -- Reshuffle the order
      TriviaBot_Randomize();
      TB_Active_Question = 1;
      TriviaBot_Send("Out of questions... Reshuffled and restarted.");
      TriviaBot_Print("Out of questions - Restarting");
   end
   
   local questionNumber = 0;
   if (TriviaBot_Config['Round_Size'] ~= 0) then
      questionNumber = TB_Round_Counter + 1;
   end
   
   local setid = TriviaBot_Config['Question_Set'];
   local qid = TB_Question_List[TB_Question_Order[TB_Active_Question]]; -- Current Question id
   if (questionNumber ~= 0) then
      TriviaBot_Send("C: " .. TriviaBot_Capitalize(TB_Question_Sets[setid]['Title']) .. " - " .. TB_Question_Sets[setid]['Categories'][TB_Questions['Category'][qid]]);
      TriviaBot_Send("Q" .. questionNumber .. ": " .. TB_Questions['Question'][qid]);
   else
      TriviaBot_Send("C: " .. TriviaBot_Capitalize(TB_Question_Sets[setid]['Title']) .. " - " .. TB_Question_Sets[setid]['Categories'][TB_Questions['Category'][qid]]);
      TriviaBot_Send("Q: " .. TB_Questions['Question'][qid]);
   end

   TB_Question_Starttime = GetTime();
   TB_Question_Hinttime = TB_Question_Starttime + TriviaBot_Config['Question_Timeout']/2;
   TB_Hint_Counter = 0;
   TB_Accept_Answers = true;
   TriviaBotGUI_SkipButton:Enable();
   TriviaBot_Schedule("QUESTION_TIMEOUT", TriviaBot_Config['Question_Timeout']);
   TriviaBot_Schedule("TIMEOUT_WARNING", TriviaBot_Config['Question_Timeout'] - TriviaBot_Config['Timeout_Warning']);
end

----------------------------------------------------------------------------
-- Capitalize string excluding restricted
----------------------------------------------------------------------------
function TriviaBot_Capitalize(str)
   --str = str:lower(); -- Lowercase the string
   str = str:gsub("^%l", string.upper); -- Capitalize fist letter
   local function tchelper(first, rest)
      if (TriviaBot_RestrictionCheck(first..rest, TB_Caps_Restricted)) then
         return first:upper()..rest:lower();
      else
         return first..rest;
      end
   end
   str = str:gsub("(%a)([%w_']*)", tchelper);
   return str;
end

----------------------------------------------------------------------------
-- Change custom channel
----------------------------------------------------------------------------
function TriviaBot_ChangeCustomChannel()
   -- Check if the old channel is still joined
   if (GetChannelName(TriviaBot_Config['Channel']) > 0) then
      -- It still exists, try to leave it and re-try this method.
      LeaveChannelByName(TriviaBot_Config['Channel']);
      TriviaBot_Schedule("RETRY_CHANNEL_CHANGE", 1);
   else
      -- Set and join the new channel
      JoinChannelByName(TB_NewChannel);
      ChatFrame_AddChannel(DEFAULT_CHAT_FRAME, TB_NewChannel);
   
      -- Check if the new channel is joined
      if (GetChannelName(TB_NewChannel) > 0) then
         -- Finalize the change
         TriviaBot_Config['Channel'] = TB_NewChannel;
         TriviaBot_Config['Chat_Type'] = "channel";
         TriviaBotGUI_Channel:SetText(TriviaBot_Config['Channel']);

         -- Announce the action
         TriviaBot_Print("Channel changed to: " .. TB_NewChannel);
      else
         -- If it doesn't exist yet, re-try this method again
         TriviaBot_Schedule("RETRY_CHANNEL_CHANGE", 1);
      end
   end
end

----------------------------------------------------------------------------
-- Change chat type
----------------------------------------------------------------------------
function TriviaBot_ChatSelect(type, channel)
   -- Unregister old chat type
   TriviaBot_UnregEvent(TriviaBot_Config['Chat_Type']);
   if (type ~= "channel") then
      -- Leave the custom channel if another chat type is selected
      if (GetChannelName(TriviaBot_Config['Channel']) > 0) then
         LeaveChannelByName(TriviaBot_Config['Channel']);
      end
      TriviaBot_Config['Chat_Type'] = type;
      TriviaBot_Print("Channel changed to: " .. TriviaBot_Capitalize(type));
      -- Disable custom channel stuff
      TriviaBotGUI_Channel:EnableMouse(false);
      TriviaBotGUI_Channel:ClearFocus();
      TriviaBotGUI_Channel:SetTextColor(1,0,0);
      TriviaBotGUI_ChannelButton:Disable();
   else
      -- Enable custom channel stuff
      if (type ~= TriviaBot_Config['Chat_Type']) then
         TriviaBotGUI_Channel:EnableMouse(true);
         TriviaBotGUI_Channel:SetTextColor(1,1,1);
         TriviaBotGUI_ChannelButton:Enable();
      end
      TB_NewChannel = channel;
      TriviaBot_ChangeCustomChannel();
   end
   -- Register new chat type
   TriviaBot_RegEvent(type);
end

----------------------------------------------------------------------------
-- Compares user's answer to question
----------------------------------------------------------------------------
function TriviaBot_CheckAnswer(player, msg)
   -- Remove invalid chars from the message
   msg = TriviaBot_StringCorrection(msg);
   -- Current Question id
   local qid = TB_Question_List[TB_Question_Order[TB_Active_Question]];
   -- For every answer in the list of answers
   for i = 1, #TB_Questions['Answers'][qid], 1 do
      -- Check if answer is correct
      if (string.lower(msg) == string.lower(TB_Questions['Answers'][qid][i])) then
         -- Unschedule warnings and timeout
         TriviaBot_UnSchedule("all");

         -- Time the answer
         local timeTaken = GetTime() - TB_Question_Starttime;
         timeTaken = math.floor(timeTaken * 100 + 0.5) / 100;
         
         -- Tell player they don't suck as badly as they think they do.
         TriviaBot_Send("'".. msg .. "' is the correct answer, " .. player .. " in " .. timeTaken .. " seconds.");

         -- Generate personal arrays
         if (not TB_Game_Scores['Player_Scores'][player]) then
            TB_Game_Scores['Player_Scores'][player] = {['Win_Streak'] = 0, ['Speed'] = 120, ['Points'] = 0, ['Score'] = 0};
         end
         if (not TriviaBot_Scores['Player_Scores'][player]) then
            TriviaBot_Scores['Player_Scores'][player] = {['Win_Streak'] = 0, ['Speed'] = 120, ['Points'] = 0, ['Score'] = 0};
         end

         -- New game speed record
         if (not TB_Game_Scores['Speed']['Holder'] or timeTaken < TB_Game_Scores['Speed']['Time']) then
            TB_Game_Scores['Speed']['Holder'] = player;
            TB_Game_Scores['Speed']['Time'] = timeTaken;
            TriviaBot_Send("--New Game Speed Record--");
         end
         
         -- New all-time speed record
         if (not TriviaBot_Scores['Speed']['Holder'] or timeTaken < TriviaBot_Scores['Speed']['Time']) then
            TriviaBot_Scores['Speed']['Holder'] = player;
            TriviaBot_Scores['Speed']['Time'] = timeTaken;
            TriviaBot_Send("--New All-Time Speed Record--");
         end
         
         -- Check the temporary win streak
         if (TB_Game_Scores['Temp_Win_Streak']['Holder'] == player) then
            TB_Game_Scores['Temp_Win_Streak']['Count'] = TB_Game_Scores['Temp_Win_Streak']['Count'] + 1;
            -- Check personal game win streak
            if (TB_Game_Scores['Player_Scores'][player]['Win_Streak'] < TB_Game_Scores['Temp_Win_Streak']['Count']) then
               TB_Game_Scores['Player_Scores'][player]['Win_Streak'] = TB_Game_Scores['Temp_Win_Streak']['Count'];
            end
            -- Check personal all-time win streak
            if (TriviaBot_Scores['Player_Scores'][player]['Win_Streak'] < TB_Game_Scores['Temp_Win_Streak']['Count']) then
               TriviaBot_Scores['Player_Scores'][player]['Win_Streak'] = TB_Game_Scores['Temp_Win_Streak']['Count'];
               -- Announce the record if checked
               if (TriviaBot_Config['Report_Personal']) then
                  TriviaBot_Send(player .. " beat " .. TB_Genders[UnitSex(player)] .. " own win-streak by " .. TB_Game_Scores['Temp_Win_Streak']['Count'] .. " in a row.");
               end
            end
         else
            TB_Game_Scores['Temp_Win_Streak']['Holder'] = player;
            TB_Game_Scores['Temp_Win_Streak']['Count'] = 1;
         end
         
         -- New game win streak record
         if (not TB_Game_Scores['Best_Win_Streak']['Holder'] or TB_Game_Scores['Temp_Win_Streak']['Count'] > TB_Game_Scores['Best_Win_Streak']['Count']) then
            TB_Game_Scores['Best_Win_Streak']['Holder'] = player;
            TB_Game_Scores['Best_Win_Streak']['Count'] = TB_Game_Scores['Temp_Win_Streak']['Count'];
            if (TriviaBot_Config['Report_Win_Streak'] and TB_Game_Scores['Best_Win_Streak']['Count']%5 == 0) then
               TriviaBot_Send(player .. " has a win-streak of " .. TB_Game_Scores['Best_Win_Streak']['Count'] .. " in a row.");
            end
         end
         
         -- New all-time win streak record
         if (not TriviaBot_Scores['Win_Streak']['Holder'] or TB_Game_Scores['Temp_Win_Streak']['Count'] > TriviaBot_Scores['Win_Streak']['Count']) then
            TriviaBot_Scores['Win_Streak']['Holder'] = player;
            TriviaBot_Scores['Win_Streak']['Count'] = TB_Game_Scores['Temp_Win_Streak']['Count'];
         end

         -- Check personal speed records
         if (timeTaken < TB_Game_Scores['Player_Scores'][player]['Speed']) then
            TB_Game_Scores['Player_Scores'][player]['Speed'] = timeTaken;
         end
         if (timeTaken < TriviaBot_Scores['Player_Scores'][player]['Speed']) then
            TriviaBot_Scores['Player_Scores'][player]['Speed'] = timeTaken;
            -- Announce the record if checked
            if (TriviaBot_Config['Report_Personal']) then
               TriviaBot_Send(player .. " beat " .. TB_Genders[UnitSex(player)] .. " own speed record with " .. timeTaken .. " seconds.");
            end
         end

         -- Add points if point mode is enabled
         if (TriviaBot_Config['Point_Mode']) then
            TriviaBot_Scores['Player_Scores'][player]['Points'] = TriviaBot_Scores['Player_Scores'][player]['Points'] + TB_Questions['Points'][qid];
            TB_Game_Scores['Player_Scores'][player]['Points'] = TB_Game_Scores['Player_Scores'][player]['Points'] + TB_Questions['Points'][qid];
         end

         -- Update the score
         TriviaBot_Scores['Player_Scores'][player]['Score'] = TriviaBot_Scores['Player_Scores'][player]['Score'] + 1;
         TB_Game_Scores['Player_Scores'][player]['Score'] = TB_Game_Scores['Player_Scores'][player]['Score'] + 1;

         TriviaBot_EndQuestion(false);
      end
   end
end

----------------------------------------------------------------------------
-- Check selected channel is avaiable
----------------------------------------------------------------------------
function TriviaBot_CheckChannel(event)
   if (not TB_Loaded or not TB_Player_Entered) then
      -- Addon isn't loaded there's no reason to check the events
      return;
   end

   local chat = TriviaBot_Config['Chat_Type'];
   if (chat == "guild" and not IsInGuild()) or
      (chat == "officer" and not TriviaBot_IsOfficer()) or
      (chat == "party" and GetNumPartyMembers() == 0) or
      (chat == "raid" and GetNumRaidMembers() == 0) or
      (chat == "general" and GetChannelName("General - " .. TB_Zone) == 0) or
      (chat == "battleground" and TB_Zone ~= TB_AB and TB_Zone ~= TB_WSG and TB_Zone ~= TB_AV and TB_Zone ~= TB_EOTS and TB_Zone ~= TB_SOTA) then
      if (TB_Running) then
         TriviaBot_Stop(); -- We can't broadcast so TriviaBot is stopped
      end
      TriviaBot_ChatSelect("channel", TriviaBot_Config['Channel']);
      TriviaBotGUI_Update();
   end
end

----------------------------------------------------------------------------
-- Checks if there're any hints to send
----------------------------------------------------------------------------
function TriviaBot_CheckHints()
   if (TB_Question_Hinttime <= GetTime()) then
      local qid = TB_Question_List[TB_Question_Order[TB_Active_Question]];
      if (TB_Hint_Counter < #TB_Questions['Hints'][qid]) then
         TriviaBot_Send("Hint: " .. TB_Questions['Hints'][qid][TB_Hint_Counter + 1]);
         TB_Hint_Counter = TB_Hint_Counter + 1;
      end
      TB_Question_Hinttime = GetTime() + 10;
   end
end

----------------------------------------------------------------------------
-- Command handler
----------------------------------------------------------------------------
function TriviaBot_Command(cmd)
    -- Create variables
   local msgArgs = {};

   -- Convert to lower case
   cmd = string.lower(cmd);

   -- Seperate our args
   for value in string.gmatch(cmd, "[^ ]+") do
      table.insert(msgArgs, value);
   end

   if (#msgArgs == 0) then
      -- Toggle the GUI
      if (TriviaBotGUI:IsVisible()) then
         TriviaBotGUI:Hide();
      else
         TriviaBotGUI_Update();
         TriviaBotGUI:Show();
      end
   elseif (msgArgs[1] == "clear") then
      TB_Game_Scores = {};
      TB_Game_Scores['Best_Win_Streak'] = {};
      TB_Game_Scores['Temp_Win_Streak'] = {};
      TB_Game_Scores['Speed'] = {};
      TB_Game_Scores['Player_Scores'] = {};
      TriviaBot_Print("Game scores cleared.");
   elseif (msgArgs[1] == "clearall") then
      TriviaBot_Scores = {};
      TriviaBot_Scores['Win_Streak'] = {};
      TriviaBot_Scores['Speed'] = {};
      TriviaBot_Scores['Player_Scores'] = {};
      TriviaBot_Print("All-Time scores cleared.");
   elseif (msgArgs[1] == "help") then
      TriviaBot_Print("---TriviaBot Help---");
      TriviaBot_Print("/trivia clear - Clear current game data");
      TriviaBot_Print("/trivia clearall - Clear current all-time score data");
      TriviaBot_Print("/trivia help - Help on TriviaBot commands");
      TriviaBot_Print("/trivia reset - Reset configuration to defaults");
   elseif (msgArgs[1] == "reset") then
      TriviaBot_NewConfig(true);
      TriviaBot_ChatSelect(TriviaBot_Config['Chat_Type'], TriviaBot_Config['Channel']);
      TriviaBotGUI_Update();
   end
end

----------------------------------------------------------------------------
-- Do scheduled events
----------------------------------------------------------------------------
function TriviaBot_DoSchedule(self)
   if (TB_Schedule) then
      for id, events in pairs(TB_Schedule) do
         -- Get the time of each event
         -- If it should be run (i.e. equal or less than current time)
         if (events['time'] <= GetTime()) then
            TriviaBot_OnEvent(self, events['name']);
            TriviaBot_UnSchedule(id);
         end
      end
   end
end

----------------------------------------------------------------------------
-- Called when a question is finished
----------------------------------------------------------------------------
function TriviaBot_EndQuestion(showAnswer)
   -- Prevent further answers
   TB_Accept_Answers = false;
   TriviaBotGUI_SkipButton:Disable();

   -- Increment the counters
   TB_Round_Counter = TB_Round_Counter + 1;

   local wait = 0;
   if (showAnswer) then
      wait = wait + 4;
      TriviaBot_Schedule("SHOW_ANSWER", wait);
   end
   
   -- See if we've reached the end of the round
   if (TB_Round_Counter == TriviaBot_Config['Round_Size']) then
      TriviaBot_Schedule("END_REPORT", wait + 4);
      TriviaBot_Schedule("STOP_GAME", wait + 8);
   else
      -- Count how long it's been since a question report
      if (TriviaBot_Config['Show_Reports']) then
         TB_Report_Counter = TB_Report_Counter + 1;
         if (TB_Report_Counter == TriviaBot_Config['Top_Score_Interval']) then
            wait = wait + 4;
            TriviaBot_Schedule("REPORT_SCORES", wait);
            TB_Report_Counter = 0;
         end
      end
      TriviaBot_Schedule("NEXT_QUESTION", TriviaBot_Config['Question_Interval'] + wait);
   end
end

----------------------------------------------------------------------------
-- Index trivia questions
----------------------------------------------------------------------------
function TriviaBot_IndexTrivia(setid)
   if (TriviaBot_Questions[setid]) then
      if (not TB_Question_Sets[setid]['CatIdx']) then
         TB_Question_Sets[setid]['CatIdx'] = {[0] = {}};
         for id,_ in pairs(TB_Question_Sets[setid]['Categories']) do
            TB_Question_Sets[setid]['CatIdx'][id] = {};
         end
         for id,_ in pairs(TriviaBot_Questions[setid]['Question']) do
            table.insert(TB_Question_Sets[setid]['CatIdx'][0], id);
            local catid = TriviaBot_Questions[setid]['Category'][id];
            if (TB_Question_Sets[setid]['CatIdx'][catid]) then
               table.insert(TB_Question_Sets[setid]['CatIdx'][catid], id);
            end
         end
      end
   end
end

----------------------------------------------------------------------------
-- Checks if the player can listen to and speak in the officer chat
----------------------------------------------------------------------------
function TriviaBot_IsOfficer()
   local _,_,playerrank = GetGuildInfo("player");
   GuildControlSetRank(playerrank + 1);
   local _,_,officerchat_listen,officerchat_speak = GuildControlGetRankFlags();
   if (officerchat_listen == 1 and officerchat_speak == 1) then
      return true;
   else
      return false;
   end
end

----------------------------------------------------------------------------
-- Load question-sets info and categories
----------------------------------------------------------------------------
function TriviaBot_LoadQuestionSets()
   for id,_ in pairs(TriviaBot_Questions) do
      TB_Question_Sets[id] = {};
      TB_Question_Sets[id]['Title'] = TriviaBot_Questions[id]['Title'];
      TB_Question_Sets[id]['Description'] = TriviaBot_Questions[id]['Description'];
      TB_Question_Sets[id]['Author'] = TriviaBot_Questions[id]['Author'];
      TB_Question_Sets[id]['Categories'] = TriviaBot_Questions[id]['Categories'];
      TriviaBot_IndexTrivia(id);
   end
end

----------------------------------------------------------------------------
-- Load trivia into trivia memory
----------------------------------------------------------------------------
function TriviaBot_LoadTrivia(setid, catid)
   -- If we're running we need to skip the current question before hopping databases.
   if (TB_Running) then
      TriviaBot_AbortQuestion();
   end
   
   -- See if question-set and category exists
   if (TB_Question_Sets[setid]) then
      TB_Questions = TriviaBot_Questions[setid];
      if (TB_Question_Sets[setid]['Categories'][catid] or catid == 0) then
         TB_Question_List = TB_Question_Sets[setid]['CatIdx'][catid];
         local category = "All";
         if (catid ~= 0) then -- Single categories
            category = TB_Question_Sets[setid]['Categories'][catid];
         end
         if (#TB_Questions['Question'] > 0) then
            TriviaBot_Print("Questions loaded successfully. Question count: " .. #TB_Question_Sets[setid]['CatIdx'][catid]);--#TB_Questions['Question']);
            if (TriviaBot_Config['Question_Set'] ~= setid) then
               TriviaBot_Print("Database Name: " .. TriviaBot_Capitalize(TB_Question_Sets[setid]['Title']) .. " - Description: " .. TB_Question_Sets[setid]['Description'].. " - Author(s): " .. TB_Question_Sets[setid]['Author'] .. ".");
            else
               TriviaBot_Print("Database Name: " .. TriviaBot_Capitalize(TB_Question_Sets[setid]['Title']) .. " - Category: " .. category .. " loaded.");
            end
         else
            TriviaBot_Print("Database Name: " .. TriviaBot_Capitalize(TB_Question_Sets[setid]['Title']) .. " - Category: " .. category .. " had no questions to load.");
         end
   
         -- Always randomize the question order
         TriviaBot_Randomize();
         if (TB_Running)then
            --If we're switching databases mid-game, we should alert our players that the questions has changed
            TriviaBot_Send("Switching trivia database to: " .. TriviaBot_Capitalize(TB_Question_Sets[setid]['Title']) .. ". Description: " .. TB_Question_Sets[setid]['Description'].. ". Author: " .. TB_Question_Sets[setid]['Author'] .. ".");
            TriviaBot_Schedule("NEXT_QUESTION", 6);
         end
         collectgarbage("collect") -- Do a cleanup
     else
        TriviaBot_Print("Category ID: "..catid.." does not exist");
        TriviaBot_Print("Available categories of trivia:");
        TriviaBot_Print("ID: 0 - all");
        for id, cat in pairs(TB_Question_Sets) do
           TriviaBot_Print("ID: " .. id .. " - " .. cat['Categories']);
        end
     end
   else
      TriviaBot_Print("Question-Set ID: "..setid.." does not exist");
      TriviaBot_Print("Available Libraries of trivia:");
      for id, set in pairs(TB_Question_Sets) do
         TriviaBot_Print("ID: " .. id .. " - " .. set['Title']);
      end
   end
end

----------------------------------------------------------------------------
-- Create new configuation
----------------------------------------------------------------------------
function TriviaBot_NewConfig(reset)
   if (reset) then
      TriviaBot_Config = {};
      TriviaBot_Print("Resetting configuration to default values");
   end
   
   -- Default amount of answers shown (0 = all)
   if (not TriviaBot_Config['Answers_Shown']) then TriviaBot_Config['Answers_Shown'] = 0; end

   -- Default private channel (Trivia)
   if (not TriviaBot_Config['Channel']) then TriviaBot_Config['Channel'] = "Trivia"; end

   -- Default chat type (channel)
   if (not TriviaBot_Config['Chat_Type']) then TriviaBot_Config['Chat_Type'] = "channel"; end

   -- Using point-mode (true)
   if (not TriviaBot_Config['Point_Mode']) then TriviaBot_Config['Point_Mode'] = true; end
   
   -- Default question category (0 = all)
   if (not TriviaBot_Config['Question_Category']) then TriviaBot_Config['Question_Category'] = 0; end

   -- Default question interval (10 seconds)
   if (not TriviaBot_Config['Question_Interval']) then TriviaBot_Config['Question_Interval'] = 10; end

   -- Default question-set (1)
   if (not TriviaBot_Config['Question_Set']) then TriviaBot_Config['Question_Set'] = 1; end

   -- Default question timeout (45 seconds)
   if (not TriviaBot_Config['Question_Timeout']) then TriviaBot_Config['Question_Timeout'] = 45; end

   -- Report personal records (true)
   if (not TriviaBot_Config['Report_Personal']) then TriviaBot_Config['Report_Personal'] = true; end

   -- Report win streak updates (true)
   if (not TriviaBot_Config['Report_Win_Streak']) then TriviaBot_Config['Report_Win_Streak'] = true; end

   -- Defaults questions per round (0 = unlimited)
   if (not TriviaBot_Config['Round_Size']) then TriviaBot_Config['Round_Size'] = 0; end
   
   -- Use Short Channel Tag (false)
   if (not TriviaBot_Config['Short_Tag']) then TriviaBot_Config['Short_Tag'] = false; end

   -- Show answers (true)
   if (not TriviaBot_Config['Show_Answers']) then TriviaBot_Config['Show_Answers'] = true; end
   
   -- Show hints (true)
   if (not TriviaBot_Config['Show_Hints']) then TriviaBot_Config['Show_Hints'] = true; end

   -- Show reports (true)
   if (not TriviaBot_Config['Show_Reports']) then TriviaBot_Config['Show_Reports'] = true; end

   -- Show whispers (false)
   if (not TriviaBot_Config['Show_Whispers']) then TriviaBot_Config['Show_Whispers'] = false; end

   -- Default timeout warning (20 seconds)
   if (not TriviaBot_Config['Timeout_Warning']) then TriviaBot_Config['Timeout_Warning'] = 20; end

   -- Top score count (5)
   if (not TriviaBot_Config['Top_Score_Count']) then TriviaBot_Config['Top_Score_Count'] = 5; end

   -- Default top score interval (5 answers)
   if (not TriviaBot_Config['Top_Score_Interval']) then TriviaBot_Config['Top_Score_Interval'] = 5; end

   -- Default Update interval. Tweaking may increase performance.
   if (not TriviaBot_Config['Update_Interval']) then TriviaBot_Config['Update_Interval'] = 0.5; end

   -- Store the version
   TriviaBot_Config.Version = TB_VERSION;
end

----------------------------------------------------------------------------
-- OnEvent handler
----------------------------------------------------------------------------
function TriviaBot_OnEvent(self, event, ...)
   local arg1 = ...;
   if (event == "ADDON_LOADED" and arg1 == "TriviaBot") then
      if (not TB_Loaded) then
         -- Load the saved variables
         if (not TriviaBot_Config.Version) then
            TriviaBot_Print("Creating new configuation.");
            TriviaBot_NewConfig();
         end
         if (TriviaBot_Config.Version ~= TB_VERSION) then
            TriviaBot_Print("Old version detected, upgrading to new version.");
            TriviaBot_Print("Old: " .. TriviaBot_Config.Version .. " New: " .. TB_VERSION);
            TriviaBot_NewConfig();
         end
         
         --Start in the 'off' state
         TB_Accept_Answers = false;

         -- Send a message
         TriviaBot_Print("Version " .. TB_VERSION .. " loaded.");
         
         -- Load question-sets and categories
         TriviaBot_LoadQuestionSets();

         -- Load the questions
         TriviaBot_LoadTrivia(TriviaBot_Config['Question_Set'], TriviaBot_Config['Question_Category']);
         
         -- Initialize the GUI
         TriviaBotGUI_Initialize();

         -- Check to see if general was the last selected chat and change it
         if (TriviaBot_Config['Chat_Type'] == "general") then
            TriviaBot_Config['Chat_Type'] = "channel";
         end

         -- Make sure the channel is joined
         TriviaBot_Schedule("CHANNEL_STARTUP", 5);

         -- Set loaded state
         TB_Loaded = true;
      end
   elseif (event == "CHAT_MSG_CHANNEL" and TB_Accept_Answers) then
      local msg = arg1;
      local player = arg2;
      local channel = string.lower(arg9);
      
      if (msg and player and channel) then
         if (channel == string.lower(TriviaBot_Config['Channel']) or channel == string.lower("General - " .. TB_Zone)) then
            if (string.lower(msg) ~= "!hint") then
               TriviaBot_CheckAnswer(player, msg);
            else
               TriviaBot_CheckHints();
            end
         end
      end
   elseif (event == "CHAT_MSG_WHISPER") then
      local msg = string.lower(arg1);
      local player = arg2;

      if (msg and player) then
         TriviaBot_WhisperControl(player, msg);
      end
   elseif ((event == "CHAT_MSG_SAY" or
            event == "CHAT_MSG_GUILD" or
            event == "CHAT_MSG_OFFICER" or
            event == "CHAT_MSG_YELL" or
            event == "CHAT_MSG_RAID" or
            event == "CHAT_MSG_RAID_LEADER" or
            event == "CHAT_MSG_PARTY" or
            event == "CHAT_MSG_BATTLEGROUND" or
            event == "CHAT_MSG_BATTLEGROUND_LEADER") and TB_Accept_Answers) then
      -- Something was said, and the bot is on
      local msg = arg1;
      local player = arg2;
      
      if (msg and player) then
         if (string.lower(msg) ~= "!hint") then
            TriviaBot_CheckAnswer(player, msg);
         else
            TriviaBot_CheckHints();
         end
      end
   elseif (event == "CHAT_MSG_SYSTEM" and arg1 == ERR_TOO_MANY_CHAT_CHANNELS) then
      TriviaBot_UnSchedule("all");
      TriviaBot_Print("Leave another channel before setting a new custom channel");
   elseif (event == "NEXT_QUESTION") then TriviaBot_AskQuestion();
   elseif (event == "QUESTION_TIMEOUT") then TriviaBot_QuestionTimeout();
   elseif (event == "TIMEOUT_WARNING") then TriviaBot_Send(TriviaBot_Config['Timeout_Warning'] .. " seconds left!");
   elseif (event == "REPORT_SCORES") then TriviaBot_Report("midreport");
   elseif (event == "END_REPORT") then TriviaBot_Report("endreport");
   elseif (event == "STOP_GAME") then TriviaBot_Stop();
   elseif (event == "SHOW_ANSWER") then TriviaBot_PrintAnswers();
   elseif (event == "RETRY_CHANNEL_CHANGE") then TriviaBot_ChangeCustomChannel();
   elseif (event == "CHANNEL_STARTUP") then TriviaBot_ChatSelect(TriviaBot_Config['Chat_Type'], TriviaBot_Config['Channel']);
   elseif (event == "ZONE_CHANGED_NEW_AREA" or event == "PLAYER_ENTERING_WORLD" or event == "GUILD_ROSTER_UPDATE" or event == "PARTY_MEMBERS_CHANGED") then
      if (event == "PLAYER_ENTERING_WORLD" and not TB_Player_Entered) then
         TB_Player_Entered = true;
      end
      TB_Zone = GetRealZoneText();
      TriviaBot_CheckChannel(event);
   end
end

----------------------------------------------------------------------------
-- OnLoad Event
----------------------------------------------------------------------------
function TriviaBot_OnLoad(self)
   --Register Slash Command
   SLASH_TRIVIABOT1 = "/trivia";
   SLASH_TRIVIABOT2 = "/triviabot";
   SlashCmdList['TRIVIABOT'] = TriviaBot_Command;

   -- Register Events
   self:RegisterEvent("CHAT_MSG_SYSTEM"); -- Should the server report something important
   self:RegisterEvent("CHAT_MSG_WHISPER"); -- Enables whisper commands
   self:RegisterEvent("ADDON_LOADED"); -- Let the addon know when it's loaded
   self:RegisterEvent("ZONE_CHANGED_NEW_AREA"); -- Battleground check
   self:RegisterEvent("PLAYER_ENTERING_WORLD"); -- Battleground check
   self:RegisterEvent("GUILD_ROSTER_UPDATE"); -- Guild check
   self:RegisterEvent("PARTY_MEMBERS_CHANGED"); -- Party/Raid check
end

----------------------------------------------------------------------------
-- OnUpdate Event is run in cycles
----------------------------------------------------------------------------
function TriviaBot_OnUpdate(self, elapsed)
   self.TimeSinceLastUpdate = self.TimeSinceLastUpdate + elapsed;
   if (self.TimeSinceLastUpdate > TriviaBot_Config['Update_Interval']) then
      TriviaBot_DoSchedule(self);
      self.TimeSinceLastUpdate = 0;
   end
end

----------------------------------------------------------------------------
-- Print message in console
----------------------------------------------------------------------------
function TriviaBot_Print(msg)
   -- Check if the default frame exists
   if (DEFAULT_CHAT_FRAME) then
      -- Format the message
      msg = TB_MAGENTA .. TB_Channel_Prefix .. ": " .. TB_WHITE .. msg;
      DEFAULT_CHAT_FRAME:AddMessage(msg);
   end
end

----------------------------------------------------------------------------
-- Print answers to the channel
----------------------------------------------------------------------------
function TriviaBot_PrintAnswers()
   if (TriviaBot_Config['Answers_Shown'] == 1 or #TB_Questions['Answers'][TB_Question_List[TB_Question_Order[TB_Active_Question]]] == 1) then
      TriviaBot_Send("The correct answer was: " .. TB_Questions['Answers'][TB_Question_List[TB_Question_Order[TB_Active_Question]]][1]);
   else
      TriviaBot_Send("The correct answers were:");
      if (TriviaBot_Config['Answers_Shown'] == 0) then
         for _,answer in pairs(TB_Questions['Answers'][TB_Question_List[TB_Question_Order[TB_Active_Question]]]) do
            TriviaBot_Send(answer);
         end
      else
         local count = TriviaBot_Config['Answers_Shown'];
         if (#TB_Questions['Answers'][TB_Question_List[TB_Question_Order[TB_Active_Question]]] < count) then
            count = #TB_Questions['Answers'][TB_Question_List[TB_Question_Order[TB_Active_Question]]];
         end
         for i = 1, count, 1 do
            TriviaBot_Send(TB_Questions['Answers'][TB_Question_List[TB_Question_Order[TB_Active_Question]]][i]);
         end
      end
   end
end

----------------------------------------------------------------------------
-- Print Category List to the channel
----------------------------------------------------------------------------
function TriviaBot_PrintCategoryList()
   local setid = TriviaBot_Config['Question_Set'];
   TriviaBot_Send("Title: " .. TriviaBot_Capitalize(TB_Question_Sets[setid]['Title']));
   TriviaBot_Send("Description: " .. TB_Question_Sets[setid]['Description']);
   TriviaBot_Send("Author: " .. TB_Question_Sets[setid]['Author']);
   TriviaBot_Send("Categories (question count):");
   TriviaBot_Send("#0: All (" .. #TriviaBot_Questions[setid]['Question'] .. ")");
   for id,_ in pairs(TB_Question_Sets[setid]['Categories']) do
      TriviaBot_Send("#" .. id .. ": " .. TB_Question_Sets[setid]['Categories'][id] .. " (" .. #TB_Question_Sets[setid]['CatIdx'][id] .. ")");
   end
end

----------------------------------------------------------------------------
-- Print error message in console
----------------------------------------------------------------------------
function TriviaBot_PrintError(msg)
   -- Check if the default frame exists
   if (DEFAULT_CHAT_FRAME) then
      -- Format the message
      msg = TB_RED .. "[ERROR]" .. TB_Channel_Prefix .. ": " .. TB_WHITE .. msg;
      DEFAULT_CHAT_FRAME:AddMessage(msg);
   end
end

----------------------------------------------------------------------------
-- Print Question List to the channel
----------------------------------------------------------------------------
function TriviaBot_PrintQuestionList()
   TriviaBot_Send("Question-sets (question count):");
   for id,_ in pairs(TB_Question_Sets) do
      TriviaBot_Send("Title: " .. TriviaBot_Capitalize(TB_Question_Sets[id]['Title']) .. " (" .. #TriviaBot_Questions[id]['Question'] .. ")");
   end
end

----------------------------------------------------------------------------
-- Answers question and prepares next one
----------------------------------------------------------------------------
function TriviaBot_QuestionTimeout()
   TriviaBot_Send("Time is up! No correct answers were given.");
   TriviaBot_EndQuestion(TriviaBot_Config['Show_Answers']);
end

----------------------------------------------------------------------------
-- Randomize the trivia questions
----------------------------------------------------------------------------
function TriviaBot_Randomize()
   -- Initialise the table
   TB_Question_Order = {};
   
   -- Number of questions
   local noq = #TB_Question_List;

   -- Fill the order array
   for i = 1, noq, 1 do
      TB_Question_Order[i] = i;
   end

   local temp, rand; -- Temporary value holders
   for j = 1, 5, 1 do -- Do the switch 5 times
      -- Swap each element with a random element
      for k = 1, noq, 1 do
         rand = math.random(noq);
         temp = TB_Question_Order[k];
         TB_Question_Order[k] = TB_Question_Order[rand]
         TB_Question_Order[rand] = temp;
      end
   end
end

----------------------------------------------------------------------------
-- Register Chat Event
----------------------------------------------------------------------------
function TriviaBot_RegEvent(chat_type)
   if (chat_type == "channel") then
      TriviaBot:RegisterEvent("CHAT_MSG_CHANNEL");
   elseif (chat_type == "say") then
      TriviaBot:RegisterEvent("CHAT_MSG_SAY");
   elseif (chat_type == "yell") then
      TriviaBot:RegisterEvent("CHAT_MSG_YELL");
   elseif (chat_type == "general") then
      TriviaBot:RegisterEvent("CHAT_MSG_CHANNEL");
   elseif (chat_type == "guild") then
      TriviaBot:RegisterEvent("CHAT_MSG_GUILD");
   elseif (chat_type == "officer") then
      TriviaBot:RegisterEvent("CHAT_MSG_OFFICER");
   elseif (chat_type == "party") then
      TriviaBot:RegisterEvent("CHAT_MSG_PARTY");
   elseif (chat_type == "raid") then
      TriviaBot:RegisterEvent("CHAT_MSG_RAID");
      TriviaBot:RegisterEvent("CHAT_MSG_RAID_LEADER");
   elseif (chat_type == "battleground") then
      TriviaBot:RegisterEvent("CHAT_MSG_BATTLEGROUND");
      TriviaBot:RegisterEvent("CHAT_MSG_BATTLEGROUND_LEADER");
   else
      TriviaBot_Print("No chat events registered");
   end
end

----------------------------------------------------------------------------
-- Print report to the channel
----------------------------------------------------------------------------
function TriviaBot_Report(type)
   local Sorted_Scores = {};
   local exists;
   local limit = TriviaBot_Config['Top_Score_Count']
   if (type == "alltimereport") then
      for player, scores in pairs(TriviaBot_Scores['Player_Scores']) do
         exists = true;
         table.insert(Sorted_Scores, {['Player'] = player, ['Points'] = scores['Points'], ['Score'] = scores['Score']});
      end

      if (exists) then
         if (TriviaBot_Config['Point_Mode']) then
            table.sort(Sorted_Scores, function(v1, v2)
               if (v1['Points'] == v2['Points']) then
                   return v1['Score'] > v2['Score'];
               else
                   return v1['Points'] > v2['Points'];
               end
            end);
         else
            table.sort(Sorted_Scores, function(v1, v2)
               if (v1['Score'] == v2['Score']) then
                   return v1['Points'] > v2['Points'];
               else
                   return v1['Score'] > v2['Score'];
               end
            end)
         end
         if (limit > #Sorted_Scores) then
            limit = #Sorted_Scores;
         end
         TriviaBot_Send("All-Time standings:");
         for i = 1, limit, 1 do
            local pess = "s";
            local sess = "s";
            if (Sorted_Scores[i]['Points'] == 1) then
               pess = "";
            end
            if (Sorted_Scores[i]['Score'] == 1) then
               sess = "";
            end
            TriviaBot_Send("#" .. i .. ": " .. Sorted_Scores[i]['Player'] .. " with " .. Sorted_Scores[i]['Points'] .. " point" .. pess .. " and " .. Sorted_Scores[i]['Score'] .. " answer" .. sess .. ".");
         end
         if (TriviaBot_Scores['Speed']['Holder']) then
            TriviaBot_Send("Speed record: " .. TriviaBot_Scores['Speed']['Holder'] .. " in " .. TriviaBot_Scores['Speed']['Time'] .. " seconds.");
         end
         if (TriviaBot_Scores['Win_Streak']['Holder']) then
            TriviaBot_Send("Win-streak record: " .. TriviaBot_Scores['Win_Streak']['Holder'] .. " with " .. TriviaBot_Scores['Win_Streak']['Count'] .. " in a row.");
         end
      else
         TriviaBot_Send("No All-Time scores found.");
      end
   else
      for player, scores in pairs(TB_Game_Scores['Player_Scores']) do
         exists = true;
         table.insert(Sorted_Scores, {['Player'] = player, ['Points'] = scores['Points'], ['Score'] = scores['Score']});
      end

      if (exists) then
         if (TriviaBot_Config['Point_Mode']) then
            table.sort(Sorted_Scores, function(v1, v2)
               if (v1['Points'] == v2['Points']) then
                   return v1['Score'] > v2['Score'];
               else
                   return v1['Points'] > v2['Points'];
               end
            end);
         else
            table.sort(Sorted_Scores, function(v1, v2)
               if (v1['Score'] == v2['Score']) then
                   return v1['Points'] > v2['Points'];
               else
                   return v1['Score'] > v2['Score'];
               end
            end)
         end
         if (type == "gamereport") then
            TriviaBot_Send("Game standings:");
         elseif (type == "midreport") then
            TriviaBot_Send("Standings so far:");
            limit = 3;
         elseif (type == "endreport") then
            TriviaBot_Send("GAME OVER! Final standings:");
         end
         if (limit > #Sorted_Scores) then
            limit = #Sorted_Scores;
         end
         for i = 1, limit, 1 do
            local pess = "s";
            local sess = "s";
            if (Sorted_Scores[i]['Points'] == 1) then
               pess = "";
            end
            if (Sorted_Scores[i]['Score'] == 1) then
               sess = "";
            end
            if (TriviaBot_Config['Point_Mode']) then
               TriviaBot_Send("#" .. i .. ": " .. Sorted_Scores[i]['Player'] .. " with " .. Sorted_Scores[i]['Points'] .. " (" .. Sorted_Scores[i]['Score'] .. ") point" .. pess .. ".");
            else
               TriviaBot_Send("#" .. i .. ": " .. Sorted_Scores[i]['Player'] .. " with " .. Sorted_Scores[i]['Score'] .. " point" .. sess .. ".");
            end
         end
         if (TB_Game_Scores['Speed']['Holder']) then
            TriviaBot_Send("Speed record: " .. TB_Game_Scores['Speed']['Holder'] .. " in " .. TB_Game_Scores['Speed']['Time'] .. " seconds.");
         end
         if (TB_Game_Scores['Best_Win_Streak']['Holder']) then
            TriviaBot_Send("Win-streak record: " .. TB_Game_Scores['Best_Win_Streak']['Holder'] .. " with " .. TB_Game_Scores['Best_Win_Streak']['Count'] .. " in a row.");
         end
      else
         if (type == "gamereport") then
            TriviaBot_Send("No Game scores found.");
         elseif (type == "midreport") then
            TriviaBot_Send("No points earned so far.");
         elseif (type == "endreport") then
            TriviaBot_Send("GAME OVER! Nobody scored!");
         end
      end
   end
end

----------------------------------------------------------------------------
-- Check if string is restricted
----------------------------------------------------------------------------
function TriviaBot_RestrictionCheck(str, list)
   for _, word in ipairs(list) do
      if (str == word) then
         return false;
      end
   end
   return true;
end

----------------------------------------------------------------------------
-- Schedule an event
----------------------------------------------------------------------------
function TriviaBot_Schedule(name, time)
   local thisEvent = {['name'] = name, ['time'] = GetTime() + time};
   table.insert(TB_Schedule, thisEvent);
end

----------------------------------------------------------------------------
-- Send a TriviaBot message to the channel
----------------------------------------------------------------------------
function TriviaBot_Send(msg)
   -- Send a message to the trivia channel
   msg = TB_Channel_Prefix .. ": " .. msg; -- Add the trivia tag to each message
   local cid = GetChannelName(TriviaBot_Config['Channel']); -- Custom channel id
   local gid = GetChannelName("General - " .. TB_Zone); -- General channel id
   if (TriviaBot_Config['Chat_Type'] ~= "channel" and TriviaBot_Config['Chat_Type'] ~= "general") then
      SendChatMessage(msg, string.upper(TriviaBot_Config['Chat_Type']));
   elseif (TriviaBot_Config['Chat_Type'] == "channel" and cid > 0) then
      SendChatMessage(msg, "CHANNEL", nil, cid);
   elseif (TriviaBot_Config['Chat_Type'] == "general" and gid > 0) then
      SendChatMessage(msg, "CHANNEL", nil, gid);
   else
      -- Print error if no valid channels were found
      TriviaBot_PrintError("No valid channel set!");
   end
end

----------------------------------------------------------------------------
-- Send a whisper message back to the player
----------------------------------------------------------------------------
function TriviaBot_SendWhisper(player, msg)
   msg = TB_Short_Prefix .. ": " .. msg; -- Add a more diskrete trivia tag to the message
   SendChatMessage(msg, "WHISPER", nil, player);
end

----------------------------------------------------------------------------
-- Skip current question
----------------------------------------------------------------------------
function TriviaBot_SkipQuestion()
   TriviaBotGUI_SkipButton:Disable();
   if (TB_Running) then
      TriviaBot_Send("Question was skipped.");
      TriviaBot_UnSchedule("all");
      TB_Accept_Answers = false;
         
      -- Show the answer anyway (for those that wanted to know)
      if (TriviaBot_Config['Show_Answers']) then
         TriviaBot_Schedule("SHOW_ANSWER", 2);
      end
         
      -- Schedule the next question
      TriviaBot_Schedule("NEXT_QUESTION", 6);
      TriviaBot_Print("Question skipped");
   else
      TriviaBot_PrintError("No game running!");
   end
end

----------------------------------------------------------------------------
-- Start trivia session
----------------------------------------------------------------------------
function TriviaBot_Start()
   -- Set Running
   TB_Running = true;
   
   -- Check if the channel is present
   TriviaBot_CheckChannel();

   -- Announce the start
   TriviaBot_Send("--World of Warcraft Trivia powered by TriviaBot--");
   local id = TriviaBot_Config['Question_Set'];
   local category;
   if (TriviaBot_Config['Question_Category'] ~= 0) then
      category = TB_Question_Sets[id]['Categories'][TriviaBot_Config['Question_Category']];
   else
      category = "All";
   end
   TriviaBot_Send("Using Trivia from database: " .. TB_Question_Sets[id]['Title']);
   TriviaBot_Send("Description: " .. TB_Question_Sets[id]['Description']);
   TriviaBot_Send("Author: " .. TB_Question_Sets[id]['Author']);
   TriviaBot_Send("Category selected: " .. category);
   if (TriviaBot_Config['Round_Size'] ~= 0) then
      TriviaBot_Send("Starting a round of " .. TriviaBot_Config['Round_Size'] .. " questions.");
   end
   TriviaBot_Print("First question coming up!");
   
   -- Schedule start
   TriviaBot_Schedule("NEXT_QUESTION", 5);
   
   -- Clear game scores
   TB_Game_Scores = {};
   TB_Game_Scores['Best_Win_Streak'] = {};
   TB_Game_Scores['Temp_Win_Streak'] = {};
   TB_Game_Scores['Speed'] = {};
   TB_Game_Scores['Player_Scores'] = {};

   if (not TriviaBot_Scores) then
      TriviaBot_Scores = {};
   end
   if (not TriviaBot_Scores['Win_Streak']) then
      TriviaBot_Scores['Win_Streak'] = {};
   end
   if (not TriviaBot_Scores['Speed']) then
      TriviaBot_Scores['Speed'] = {};
   end
   if (not TriviaBot_Scores['Player_Scores']) then
      TriviaBot_Scores['Player_Scores'] = {};
   end

   -- Reset Round and Report Counters
   TB_Report_Counter = 0;
   TB_Round_Counter = 0;
   
   -- GUI Update
   TriviaBotGUI_StartStopToggle();
end

----------------------------------------------------------------------------
-- Check if str starts with start
----------------------------------------------------------------------------
function TriviaBot_Starts(str, start)
   return string.sub(str,1,string.len(start)) == start;
end

----------------------------------------------------------------------------
-- Start/Stop Toggle
----------------------------------------------------------------------------
function TriviaBot_StartStopToggle()
   if(TB_Running) then
      TriviaBot_Stop(true);
   else
      TriviaBot_Start();
   end
end

----------------------------------------------------------------------------
-- Stop trivia session
----------------------------------------------------------------------------
function TriviaBot_Stop(announce)
   -- Clear all scheduled events
   TriviaBot_UnSchedule("all");
   TB_Accept_Answers = false;
   TB_Running = false;
   
   if (announce) then
      TriviaBot_Send("Trivia stopped.");
   end
   TriviaBot_Print("Trivia stopped.")

   -- GUI Update
   TriviaBotGUI_StartStopToggle();
end

----------------------------------------------------------------------------
-- Removes leading/trailing spaces and punctuation chars from the string
----------------------------------------------------------------------------
function TriviaBot_StringCorrection(str)
   -- Remove whitespaces
   str = str:gsub("^%s*(.-)%s*$", "%1");
   -- Remove punctuation
   str = str:gsub("^%p*(.-)%p*$", "%1");
   return str;
end

----------------------------------------------------------------------------
-- Unregister Chat Event
----------------------------------------------------------------------------
function TriviaBot_UnregEvent(chat_type)
   if (chat_type == "channel") then
      TriviaBot:UnregisterEvent("CHAT_MSG_CHANNEL");
   elseif (chat_type == "say") then
      TriviaBot:UnregisterEvent("CHAT_MSG_SAY");
   elseif (chat_type == "yell") then
      TriviaBot:UnregisterEvent("CHAT_MSG_YELL");
   elseif (chat_type == "general") then
      TriviaBot:UnregisterEvent("CHAT_MSG_CHANNEL");
   elseif (chat_type == "guild") then
      TriviaBot:UnregisterEvent("CHAT_MSG_GUILD");
   elseif (chat_type == "officer") then
      TriviaBot:UnregisterEvent("CHAT_MSG_OFFICER");
   elseif (chat_type == "party") then
      TriviaBot:UnregisterEvent("CHAT_MSG_PARTY");
   elseif (chat_type == "raid") then
      TriviaBot:UnregisterEvent("CHAT_MSG_RAID");
      TriviaBot:UnregisterEvent("CHAT_MSG_RAID_LEADER");
   elseif (chat_type == "battleground") then
      TriviaBot:UnregisterEvent("CHAT_MSG_BATTLEGROUND");
      TriviaBot:UnregisterEvent("CHAT_MSG_BATTLEGROUND_LEADER");
   else
      TriviaBot_Print("No chat events unregistered");
   end
end

----------------------------------------------------------------------------
-- Removes an event from the schedule
----------------------------------------------------------------------------
function TriviaBot_UnSchedule(id)
   -- Unschedule an event
   if (id == "all") then
      TB_Schedule = {};
   else
      table.remove(TB_Schedule, id);
   end
end

----------------------------------------------------------------------------
-- Whisper command handler
----------------------------------------------------------------------------
function TriviaBot_WhisperControl(player, msg)
   -- Create variables
   local msgArgs = {};

   -- Seperate our args
   for value in string.gmatch(msg, "[^ ]+") do
      table.insert(msgArgs, value);
   end

   if TriviaBot_Starts(msg, string.lower(TB_Message_Prefix)) then
      if (msgArgs[2] == "help") then
         TriviaBot_SendWhisper(player, "Help Menu:");
         TriviaBot_SendWhisper(player, "!tb help - For this help menu.");
         TriviaBot_SendWhisper(player, "!tb info - Info about the current game.");
         TriviaBot_SendWhisper(player, "!tb score - Score help menu.");
      elseif (msgArgs[2] == "info") then
         TriviaBot_SendWhisper(player, "Title: " .. TB_Question_Sets[TriviaBot_Config['Question_Set']]['Title']);
         if (TriviaBot_Config['Question_Category'] == 0) then
            TriviaBot_SendWhisper(player, "Category: All");
         else
            TriviaBot_SendWhisper(player, "Category: " .. TB_Question_Sets[TriviaBot_Config['Question_Set']]['Categories'][TriviaBot_Config['Question_Category']]);
         end
         if (TB_Running) then
            if (TriviaBot_Config['Round_Size'] ~= 0) then
               TriviaBot_SendWhisper(player, "Round Size: " .. TriviaBot_Config['Round_Size']);
               TriviaBot_SendWhisper(player, "Current Round: " .. TB_Round_Counter + 1);
            else
               TriviaBot_SendWhisper(player, "Round Size: Unlimited");
            end
         else
            TriviaBot_SendWhisper(player, "No games currently running");
         end
      elseif (msgArgs[2] == "score") then
         if (msgArgs[3] == "game") then
            if (TB_Game_Scores['Player_Scores'][player]) then
               TriviaBot_SendWhisper(player, "Speed Record: " .. TB_Game_Scores['Player_Scores'][player]['Speed']);
               TriviaBot_SendWhisper(player, "Win Streak Record: " .. TB_Game_Scores['Player_Scores'][player]['Win_Streak']);
               TriviaBot_SendWhisper(player, "Points: " .. TB_Game_Scores['Player_Scores'][player]['Points']);
               TriviaBot_SendWhisper(player, "Score: " .. TB_Game_Scores['Player_Scores'][player]['Score']);
            else
               TriviaBot_SendWhisper(player, "No current game scores found.");
            end
         elseif (msgArgs[3] == "alltime") then
            if (TriviaBot_Scores['Player_Scores'][player]) then
               TriviaBot_SendWhisper(player, "Speed Record: " .. TriviaBot_Scores['Player_Scores'][player]['Speed']);
               TriviaBot_SendWhisper(player, "Win Streak Record: " .. TriviaBot_Scores['Player_Scores'][player]['Win_Streak']);
               TriviaBot_SendWhisper(player, "Points: " .. TriviaBot_Scores['Player_Scores'][player]['Points']);
               TriviaBot_SendWhisper(player, "Score: " .. TriviaBot_Scores['Player_Scores'][player]['Score']);
            else
               TriviaBot_SendWhisper(player, "No all-time scores found.");
            end
         else
            TriviaBot_SendWhisper(player, "Score Help Menu:");
            TriviaBot_SendWhisper(player, "!tb score game - For your current (or previous) game scores:");
            TriviaBot_SendWhisper(player, "!tb score alltime - For your all-time scores");
         end
      else
         TriviaBot_SendWhisper(player, "Help Menu:");
         TriviaBot_SendWhisper(player, "!tb help - For this help menu.");
         TriviaBot_SendWhisper(player, "!tb info - Info about the current game.");
         TriviaBot_SendWhisper(player, "!tb score - Score help menu.");
      end
   end
end

----------------------------------------------------------------------------
-- GUI functions
----------------------------------------------------------------------------

function TriviaBotGUI_OnMouseDown(self, button)
	if (button == "LeftButton") then
     self:StartMoving();
  end	
end

function TriviaBotGUI_OnMouseUp(self, button)
	if (button == "LeftButton") then
     self:StopMovingOrSizing();
  end
end

----------------------------------------------------------------------------
-- Answers Shown OnEditFocusLost Event
----------------------------------------------------------------------------
function TriviaBotGUI_AnswersShown_OnEditFocusLost()
   local value = tonumber(this:GetText());
   if (not value or value < 0) then
      TriviaBot_PrintError("Answers Shown must be higher or equal to 0.");
      this:SetText(TriviaBot_Config['Answers_Shown']);
   end
end

----------------------------------------------------------------------------
-- Answers Shown OnEscapePressed Event
----------------------------------------------------------------------------
function TriviaBotGUI_AnswersShown_OnEscapePressed()
   this:SetText(TriviaBot_Config['Answers_Shown']);
   this:ClearFocus();
end

----------------------------------------------------------------------------
-- Answers Shown Update
----------------------------------------------------------------------------
function TriviaBotGUI_AnswersShown_Update()
   if(this:GetText() == "") then
      -- Field is blanked out so we do nothing
      return;
   end
   local num = this:GetNumber();
   if (num >= 0) then
      TriviaBot_Config['Answers_Shown'] = num;
   end
end

----------------------------------------------------------------------------
-- Initialize the CategoryList drop-down menu
----------------------------------------------------------------------------
function TriviaBotGUI_CategoryList_Initialize()
   local info;
   local set = TriviaBot_Config['Question_Set'];

   if (#TB_Question_Sets > 0 and TB_Question_Sets[set]) then
      -- Add 'All' option
      info = UIDropDownMenu_CreateInfo();
      info.value = 0;
      info.text = "All";
      info.func = function() TriviaBotGUI_CategoryList_OnClick() end;
      UIDropDownMenu_AddButton(info);

      for id,_ in pairs(TB_Question_Sets[set]['Categories']) do
         info = UIDropDownMenu_CreateInfo();
         info.value = id;
         info.text = TB_Question_Sets[set]['Categories'][id];
         info.func = function() TriviaBotGUI_CategoryList_OnClick() end;
         UIDropDownMenu_AddButton(info);
      end

      UIDropDownMenu_SetSelectedValue(TriviaBotGUI_CategoryList, TriviaBot_Config['Question_Category']);
   end
end

----------------------------------------------------------------------------
-- CategoryList OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_CategoryList_OnClick()
   local cat = this.value;
   local set = TriviaBot_Config['Question_Set'];
   
   if ((TB_Question_Sets[set]['Categories'][cat] or cat == 0) and cat ~= TriviaBot_Config['Question_Category']) then
      TriviaBot_LoadTrivia(set, cat);
      TriviaBot_Config['Question_Category'] = cat;
   end

   UIDropDownMenu_SetSelectedValue(TriviaBotGUI_CategoryList, TriviaBot_Config['Question_Category']);
end

----------------------------------------------------------------------------
-- Channel OnEditFocusLost Event
----------------------------------------------------------------------------
function TriviaBotGUI_Channel_OnEditFocusLost()
   if (this:GetText():len() == 0) then
      this:SetText(TriviaBot_Config['Channel']);
   end
end

----------------------------------------------------------------------------
-- Channel OnEscapePressed Event
----------------------------------------------------------------------------
function TriviaBotGUI_Channel_OnEscapePressed()
   this:SetText(TriviaBot_Config['Channel']);
   this:ClearFocus();
end

----------------------------------------------------------------------------
-- Channel Update
----------------------------------------------------------------------------
function TriviaBotGUI_Channel_Update()
   local channel = TriviaBotGUI_Channel:GetText();
   if(channel == "") then
      -- Someone forgot to put text here so we simply put it back
      TriviaBotGUI_Channel:SetText(TriviaBot_Config['Channel']);
      return;
   end
   
   if (GetChannelName(channel) > 0) then
      if (TriviaBot_Config['Channel'] == channel) then
         TriviaBot_Print("You've already joined the chosen channel.");
      else
         TriviaBot_Config['Channel'] = channel;
         TriviaBot_Print("Channel changed to: " .. channel);
      end
      return;
   end

   -- Let's check if the channel is valid
   if (TriviaBot_RestrictionCheck(channel, TB_Chat_Restricted)) then
      TriviaBotGUI_Channel:SetText("Changing...");
      TriviaBot_ChatSelect("channel", channel);
   else
      TriviaBot_PrintError("Invalid channel! Please choose another.");
      TriviaBotGUI_Channel:SetText(TriviaBot_Config['Channel']);
   end
   TriviaBotGUI_Channel:ClearFocus();
end

----------------------------------------------------------------------------
-- Initialize the ChatType drop-down menu
----------------------------------------------------------------------------
function TriviaBotGUI_ChatType_Initialize()
   local info;
   
   -- Say channel
   info = UIDropDownMenu_CreateInfo();
   info.value = "say";
   info.text = "Say";
   info.func = function() TriviaBotGUI_ChatType_OnClick() end;
   UIDropDownMenu_AddButton(info);
   
   -- Yell channel
   info = UIDropDownMenu_CreateInfo();
   info.value = "yell";
   info.text = "Yell";
   info.func = function() TriviaBotGUI_ChatType_OnClick() end;
   UIDropDownMenu_AddButton(info);
   
   -- General channel
   if (GetChannelName("General - " .. TB_Zone) > 0) then
      info = UIDropDownMenu_CreateInfo();
      info.value = "general";
      info.text = "General";
      info.func = function() TriviaBotGUI_ChatType_OnClick() end;
      UIDropDownMenu_AddButton(info);
   end

   -- Guild channel
   if (IsInGuild()) then
      info = UIDropDownMenu_CreateInfo();
      info.value = "guild";
      info.text = "Guild";
      info.func = function() TriviaBotGUI_ChatType_OnClick() end;
      UIDropDownMenu_AddButton(info);
   end
   
   -- Officer channel
   if (TriviaBot_IsOfficer()) then
      info = UIDropDownMenu_CreateInfo();
      info.value = "officer";
      info.text = "Officer";
      info.func = function() TriviaBotGUI_ChatType_OnClick() end;
      UIDropDownMenu_AddButton(info);
   end

   -- Party channel
   if (GetNumPartyMembers() > 0) then
      info = UIDropDownMenu_CreateInfo();
      info.value = "party";
      info.text = "Party";
      info.func = function() TriviaBotGUI_ChatType_OnClick() end;
      UIDropDownMenu_AddButton(info);
   end

   -- Raid channel
   if (GetNumRaidMembers() > 0) then
      info = UIDropDownMenu_CreateInfo();
      info.value = "raid";
      info.text = "Raid";
      info.func = function() TriviaBotGUI_ChatType_OnClick() end;
      UIDropDownMenu_AddButton(info);
   end

   -- Battleground channel
   if (GetRealZoneText() == TB_AB or GetRealZoneText() == TB_WSG or GetRealZoneText() == TB_AV or GetRealZoneText() == TB_EOTS) then
      info = UIDropDownMenu_CreateInfo();
      info.value = "battleground";
      info.text = "Battleground";
      info.func = function() TriviaBotGUI_ChatType_OnClick() end;
      UIDropDownMenu_AddButton(info);
   end

   -- Custom channel
   info = UIDropDownMenu_CreateInfo();
   info.value = "channel";
   info.text = "Custom Channel";
   info.func = function() TriviaBotGUI_ChatType_OnClick() end;
   UIDropDownMenu_AddButton(info);
   
   UIDropDownMenu_SetSelectedValue(TriviaBotGUI_ChatType, TriviaBot_Config['Chat_Type']);
end

----------------------------------------------------------------------------
-- ChatType OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_ChatType_OnClick()
   -- Get the chat type value
   local type = this.value;

   -- No reason to change if the selected chat is the same as previous
   if (type == TriviaBot_Config['Chat_Type']) then
      return;
   end

   if (type == "channel") then
      TriviaBot_ChatSelect(type, TriviaBot_Config['Channel']);
   else
      TriviaBot_ChatSelect(type);
   end

   -- Warn for public channels
   if (type == "say" or type == "yell" or type == "battleground" or type == "general") then
      TriviaBot_Print(TB_RED .. "WARNING: " .. TB_WHITE .. "Public channel Selected");
      TriviaBot_Print("Outputting questions to public channels can be very annoying in busy areas.");
      TriviaBot_Print("If people report you, your account may be suspended for spamming!");
      TriviaBot_Print("Using TriviaBot in public channels is your own responsibility.");
   end

   UIDropDownMenu_SetSelectedValue(TriviaBotGUI_ChatType, TriviaBot_Config['Chat_Type']);
end

----------------------------------------------------------------------------
-- GUI OnMouseDown ClearFocus
----------------------------------------------------------------------------
function TriviaBotGUI_ClearFocus()
   TriviaBotGUI_Channel:ClearFocus();
   TriviaBotGUI_RoundSize:ClearFocus();
   TriviaBotGUI_QuestionInterval:ClearFocus();
   TriviaBotGUI_QuestionTimeout:ClearFocus();
   TriviaBotGUI_TimeoutWarning:ClearFocus();
   TriviaBotGUI_TopScoreCount:ClearFocus();
   TriviaBotGUI_TopScoreInterval:ClearFocus();
   TriviaBotGUI_AnswersShown:ClearFocus();
end

----------------------------------------------------------------------------
-- EditBox remove highlighting
----------------------------------------------------------------------------
function TriviaBotGUI_EditBox_Highlight()
   this:HighlightText(0,0); -- We don't want any highlighting
end

----------------------------------------------------------------------------
-- Initialize all GUI objects
----------------------------------------------------------------------------
function TriviaBotGUI_Initialize()
   local editboxspace = 8;
   local checkboxspace = 5;

   -- Set the main GUI screen
   TriviaBotGUI:ClearAllPoints();
   TriviaBotGUI:SetWidth(350);
   TriviaBotGUI:SetHeight(445);
   TriviaBotGUI:SetBackdrop(
      {
         bgFile = "Interface/Tooltips/UI-Tooltip-Background",
         edgeFile = "Interface/DialogFrame/UI-DialogBox-Border",
         tile = true, tileSize = 32, edgeSize = 16,
         insets = {left = 5, right = 5, top = 5, bottom = 5}
      });
   TriviaBotGUI:SetBackdropColor(0,0,0,1);
   TriviaBotGUI:SetFrameStrata("BACKGROUND");
   TriviaBotGUI:SetMovable(true);
   TriviaBotGUI:SetPoint("CENTER");
   -- Header Texture
   TriviaBotGUI:CreateTexture("TriviaBotGUI_HeaderTexture", "ARTWORK");
   TriviaBotGUI_HeaderTexture:ClearAllPoints();
   TriviaBotGUI_HeaderTexture:SetWidth(256);
   TriviaBotGUI_HeaderTexture:SetHeight(64);
   TriviaBotGUI_HeaderTexture:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header");
   TriviaBotGUI_HeaderTexture:SetPoint("TOP", TriviaBotGUI, "TOP", 0, 12);
   -- Header Label
   TriviaBotGUI:CreateFontString("TriviaBotGUI_HeaderLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_HeaderLabel:ClearAllPoints();
   TriviaBotGUI_HeaderLabel:SetPoint("TOP", TriviaBotGUI_HeaderTexture, "TOP", 0, -14);
   
   -- Close Button
   CreateFrame("Button", "TriviaBotGUI_CloseButton", TriviaBotGUI, "UIPanelCloseButton");
   TriviaBotGUI_CloseButton:ClearAllPoints();
   TriviaBotGUI_CloseButton:SetPoint("TOPRIGHT", TriviaBotGUI, "TOPRIGHT", 0, 0);
   TriviaBotGUI_CloseButton:SetScript("OnClick", function() TriviaBotGUI:Hide(); end);
   
   -- Question List
   CreateFrame("Frame", "TriviaBotGUI_QuestionList", TriviaBotGUI, "UIDropDownMenuTemplate");
   TriviaBotGUI_QuestionList:ClearAllPoints();
   UIDropDownMenu_SetWidth(TriviaBotGUI_QuestionList, 150);
   UIDropDownMenu_SetButtonWidth(TriviaBotGUI_QuestionList, 20);
   TriviaBotGUI_QuestionList:SetPoint("TOPLEFT", TriviaBotGUI_HeaderTexture, "BOTTOM", -35, 20);
   UIDropDownMenu_Initialize(TriviaBotGUI_QuestionList, TriviaBotGUI_QuestionList_Initialize);
   -- Question List Label
   TriviaBotGUI_QuestionList:CreateFontString("TriviaBotGUI_QuestionListLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_QuestionListLabel:ClearAllPoints();
   TriviaBotGUI_QuestionListLabel:SetText("Question List:");
   TriviaBotGUI_QuestionListLabel:SetPoint("RIGHT", TriviaBotGUI_QuestionList, "LEFT", 10, 2);
   -- Question List Print Button
   CreateFrame("Button", "TriviaBotGUI_QuestionListPrintButton", TriviaBotGUI, "OptionsButtonTemplate");
   TriviaBotGUI_QuestionListPrintButton:ClearAllPoints();
   TriviaBotGUI_QuestionListPrintButton:SetWidth(45);
   TriviaBotGUI_QuestionListPrintButton:SetText("Print");
   TriviaBotGUI_QuestionListPrintButton:SetPoint("RIGHT", TriviaBotGUI_QuestionListLabel, "LEFT", -5, 0);
   TriviaBotGUI_QuestionListPrintButton:SetScript("OnClick", TriviaBot_PrintQuestionList);

   -- Category List
   CreateFrame("Frame", "TriviaBotGUI_CategoryList", TriviaBotGUI, "UIDropDownMenuTemplate");
   TriviaBotGUI_CategoryList:ClearAllPoints();
   UIDropDownMenu_SetWidth(TriviaBotGUI_CategoryList, 150);
   UIDropDownMenu_SetButtonWidth(TriviaBotGUI_CategoryList, 20);
   TriviaBotGUI_CategoryList:SetPoint("TOPLEFT", TriviaBotGUI_QuestionList, "BOTTOMLEFT", 0, 0);
   UIDropDownMenu_Initialize(TriviaBotGUI_CategoryList, TriviaBotGUI_CategoryList_Initialize);
   -- Category List Label
   TriviaBotGUI_CategoryList:CreateFontString("TriviaBotGUI_CategoryListLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_CategoryListLabel:ClearAllPoints();
   TriviaBotGUI_CategoryListLabel:SetText("Category List:");
   TriviaBotGUI_CategoryListLabel:SetPoint("RIGHT", TriviaBotGUI_CategoryList, "LEFT", 10, 2);
   -- Category List Print Button
   CreateFrame("Button", "TriviaBotGUI_CategoryListPrintButton", TriviaBotGUI, "OptionsButtonTemplate");
   TriviaBotGUI_CategoryListPrintButton:ClearAllPoints();
   TriviaBotGUI_CategoryListPrintButton:SetWidth(45);
   TriviaBotGUI_CategoryListPrintButton:SetText("Print");
   TriviaBotGUI_CategoryListPrintButton:SetPoint("RIGHT", TriviaBotGUI_CategoryListLabel, "LEFT", -5, 0);
   TriviaBotGUI_CategoryListPrintButton:SetScript("OnClick", TriviaBot_PrintCategoryList);

   -- Chat Type List
   CreateFrame("Frame", "TriviaBotGUI_ChatType", TriviaBotGUI, "UIDropDownMenuTemplate");
   TriviaBotGUI_ChatType:ClearAllPoints();
   UIDropDownMenu_SetWidth(TriviaBotGUI_ChatType, 150);
   UIDropDownMenu_SetButtonWidth(TriviaBotGUI_ChatType, 20);
   TriviaBotGUI_ChatType:SetPoint("TOPLEFT", TriviaBotGUI_CategoryList, "BOTTOMLEFT", 0, 0);
   UIDropDownMenu_Initialize(TriviaBotGUI_ChatType, TriviaBotGUI_ChatType_Initialize);
   -- Chat Type List Label
   TriviaBotGUI_ChatType:CreateFontString("TriviaBotGUI_ChatTypeLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ChatTypeLabel:ClearAllPoints();
   TriviaBotGUI_ChatTypeLabel:SetText("Chat Type:");
   TriviaBotGUI_ChatTypeLabel:SetPoint("RIGHT", TriviaBotGUI_ChatType, "LEFT", 10, 2);

   -- Channel Update Button
   CreateFrame("Button", "TriviaBotGUI_ChannelButton", TriviaBotGUI, "OptionsButtonTemplate");
   TriviaBotGUI_ChannelButton:ClearAllPoints();
   TriviaBotGUI_ChannelButton:SetWidth(70);
   TriviaBotGUI_ChannelButton:SetText("Update");
   TriviaBotGUI_ChannelButton:SetPoint("TOPLEFT", TriviaBotGUI_ChatType, "BOTTOM", 15, -5);
   TriviaBotGUI_ChannelButton:SetScript("OnClick", TriviaBotGUI_Channel_Update);
   
   -- Channel TextBox
   CreateFrame("EditBox", "TriviaBotGUI_Channel", TriviaBotGUI, "InputBoxTemplate");
   TriviaBotGUI_Channel:ClearAllPoints();
   TriviaBotGUI_Channel:SetWidth(100);
   TriviaBotGUI_Channel:SetHeight(36);
   TriviaBotGUI_Channel:SetMaxLetters(20);
   TriviaBotGUI_Channel:SetAutoFocus(false);
   TriviaBotGUI_Channel:SetPoint("RIGHT", TriviaBotGUI_ChannelButton, "LEFT", -10, 1);
   TriviaBotGUI_Channel:SetScript("OnEnterPressed", TriviaBotGUI_Channel_Update);
   TriviaBotGUI_Channel:SetScript("OnEscapePressed", TriviaBotGUI_Channel_OnEscapePressed);
   TriviaBotGUI_Channel:SetScript("OnEditFocusLost", TriviaBotGUI_Channel_OnEditFocusLost);
   TriviaBotGUI_Channel:SetScript("OnEditFocusGained", TriviaBotGUI_EditBox_Highlight);
   -- Channel TextBox Label
   TriviaBotGUI_Channel:CreateFontString("TriviaBotGUI_ChannelLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ChannelLabel:ClearAllPoints();
   TriviaBotGUI_ChannelLabel:SetText("Custom Channel:");
   TriviaBotGUI_ChannelLabel:SetPoint("RIGHT", TriviaBotGUI_Channel, "LEFT", -15, 0);

   -- Round Size TextBox
   CreateFrame("EditBox", "TriviaBotGUI_RoundSize", TriviaBotGUI, "InputBoxTemplate");
   TriviaBotGUI_RoundSize:ClearAllPoints();
   TriviaBotGUI_RoundSize:SetWidth(40);
   TriviaBotGUI_RoundSize:SetHeight(36);
   TriviaBotGUI_RoundSize:SetMaxLetters(3);
   TriviaBotGUI_RoundSize:SetAutoFocus(false);
   TriviaBotGUI_RoundSize:SetNumeric(true);
   TriviaBotGUI_RoundSize:SetJustifyH("CENTER");
   TriviaBotGUI_RoundSize:SetTextInsets(0,6,0,0);
   TriviaBotGUI_RoundSize:SetPoint("TOPLEFT", TriviaBotGUI_Channel, "BOTTOMLEFT", 0, editboxspace);
   TriviaBotGUI_RoundSize:SetScript("OnEnterPressed", function() this:ClearFocus(); end);
   TriviaBotGUI_RoundSize:SetScript("OnTextChanged", TriviaBotGUI_RoundSize_Update);
   TriviaBotGUI_RoundSize:SetScript("OnEscapePressed", TriviaBotGUI_RoundSize_OnEscapePressed);
   TriviaBotGUI_RoundSize:SetScript("OnEditFocusLost", TriviaBotGUI_RoundSize_OnEditFocusLost);
   TriviaBotGUI_RoundSize:SetScript("OnEditFocusGained", TriviaBotGUI_EditBox_Highlight);
   -- Round Size TextBox Label
   TriviaBotGUI_RoundSize:CreateFontString("TriviaBotGUI_RoundSizeLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_RoundSizeLabel:ClearAllPoints();
   TriviaBotGUI_RoundSizeLabel:SetText("Round Size:");
   TriviaBotGUI_RoundSizeLabel:SetPoint("RIGHT", TriviaBotGUI_RoundSize, "LEFT", -15, 0);
   
   -- Question Interval TextBox
   CreateFrame("EditBox", "TriviaBotGUI_QuestionInterval", TriviaBotGUI, "InputBoxTemplate");
   TriviaBotGUI_QuestionInterval:ClearAllPoints();
   TriviaBotGUI_QuestionInterval:SetWidth(40);
   TriviaBotGUI_QuestionInterval:SetHeight(36);
   TriviaBotGUI_QuestionInterval:SetMaxLetters(3);
   TriviaBotGUI_QuestionInterval:SetAutoFocus(false);
   TriviaBotGUI_QuestionInterval:SetNumeric(true);
   TriviaBotGUI_QuestionInterval:SetJustifyH("CENTER");
   TriviaBotGUI_QuestionInterval:SetTextInsets(0,6,0,0);
   TriviaBotGUI_QuestionInterval:SetPoint("TOPLEFT", TriviaBotGUI_RoundSize, "BOTTOMLEFT", 0, editboxspace);
   TriviaBotGUI_QuestionInterval:SetScript("OnEnterPressed", function() this:ClearFocus(); end);
   TriviaBotGUI_QuestionInterval:SetScript("OnTextChanged", TriviaBotGUI_QuestionInterval_Update);
   TriviaBotGUI_QuestionInterval:SetScript("OnEscapePressed", TriviaBotGUI_QuestionInterval_OnEscapePressed);
   TriviaBotGUI_QuestionInterval:SetScript("OnEditFocusLost", TriviaBotGUI_QuestionInterval_OnEditFocusLost);
   TriviaBotGUI_QuestionInterval:SetScript("OnEditFocusGained", TriviaBotGUI_EditBox_Highlight);
   -- Question Interval TextBox Label
   TriviaBotGUI_QuestionInterval:CreateFontString("TriviaBotGUI_QuestionIntervalLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_QuestionIntervalLabel:ClearAllPoints();
   TriviaBotGUI_QuestionIntervalLabel:SetText("Question Interval:");
   TriviaBotGUI_QuestionIntervalLabel:SetPoint("RIGHT", TriviaBotGUI_QuestionInterval, "LEFT", -15, 0);

   -- Question Timeout TextBox
   CreateFrame("EditBox", "TriviaBotGUI_QuestionTimeout", TriviaBotGUI, "InputBoxTemplate");
   TriviaBotGUI_QuestionTimeout:ClearAllPoints();
   TriviaBotGUI_QuestionTimeout:SetWidth(40);
   TriviaBotGUI_QuestionTimeout:SetHeight(36);
   TriviaBotGUI_QuestionTimeout:SetMaxLetters(3);
   TriviaBotGUI_QuestionTimeout:SetAutoFocus(false);
   TriviaBotGUI_QuestionTimeout:SetNumeric(true);
   TriviaBotGUI_QuestionTimeout:SetJustifyH("CENTER");
   TriviaBotGUI_QuestionTimeout:SetTextInsets(0,6,0,0);
   TriviaBotGUI_QuestionTimeout:SetPoint("TOPLEFT", TriviaBotGUI_QuestionInterval, "BOTTOMLEFT", 0, editboxspace);
   TriviaBotGUI_QuestionTimeout:SetScript("OnEnterPressed", function() this:ClearFocus(); end);
   TriviaBotGUI_QuestionTimeout:SetScript("OnTextChanged", TriviaBotGUI_QuestionTimeout_Update);
   TriviaBotGUI_QuestionTimeout:SetScript("OnEscapePressed", TriviaBotGUI_QuestionTimeout_OnEscapePressed);
   TriviaBotGUI_QuestionTimeout:SetScript("OnEditFocusLost", TriviaBotGUI_QuestionTimeout_OnEditFocusLost);
   TriviaBotGUI_QuestionTimeout:SetScript("OnEditFocusGained", TriviaBotGUI_EditBox_Highlight);
   -- Question Timeout TextBox Label
   TriviaBotGUI_QuestionTimeout:CreateFontString("TriviaBotGUI_QuestionTimeoutLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_QuestionTimeoutLabel:ClearAllPoints();
   TriviaBotGUI_QuestionTimeoutLabel:SetText("Question Timeout:");
   TriviaBotGUI_QuestionTimeoutLabel:SetPoint("RIGHT", TriviaBotGUI_QuestionTimeout, "LEFT", -15, 0);
   
   -- Timeout Warning TextBox
   CreateFrame("EditBox", "TriviaBotGUI_TimeoutWarning", TriviaBotGUI, "InputBoxTemplate");
   TriviaBotGUI_TimeoutWarning:ClearAllPoints();
   TriviaBotGUI_TimeoutWarning:SetWidth(40);
   TriviaBotGUI_TimeoutWarning:SetHeight(36);
   TriviaBotGUI_TimeoutWarning:SetMaxLetters(3);
   TriviaBotGUI_TimeoutWarning:SetAutoFocus(false);
   TriviaBotGUI_TimeoutWarning:SetNumeric(true);
   TriviaBotGUI_TimeoutWarning:SetJustifyH("CENTER");
   TriviaBotGUI_TimeoutWarning:SetTextInsets(0,6,0,0);
   TriviaBotGUI_TimeoutWarning:SetPoint("TOPLEFT", TriviaBotGUI_QuestionTimeout, "BOTTOMLEFT", 0, editboxspace);
   TriviaBotGUI_TimeoutWarning:SetScript("OnEnterPressed", function() this:ClearFocus(); end);
   TriviaBotGUI_TimeoutWarning:SetScript("OnTextChanged", TriviaBotGUI_TimeoutWarning_Update);
   TriviaBotGUI_TimeoutWarning:SetScript("OnEscapePressed", TriviaBotGUI_TimeoutWarning_OnEscapePressed);
   TriviaBotGUI_TimeoutWarning:SetScript("OnEditFocusLost", TriviaBotGUI_TimeoutWarning_OnEditFocusLost);
   TriviaBotGUI_TimeoutWarning:SetScript("OnEditFocusGained", TriviaBotGUI_EditBox_Highlight);
   -- Timeout Warning TextBox Label
   TriviaBotGUI_TimeoutWarning:CreateFontString("TriviaBotGUI_TimeoutWarningLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_TimeoutWarningLabel:ClearAllPoints();
   TriviaBotGUI_TimeoutWarningLabel:SetText("Timeout Warning:");
   TriviaBotGUI_TimeoutWarningLabel:SetPoint("RIGHT", TriviaBotGUI_TimeoutWarning, "LEFT", -15, 0);
   
   -- Top Score Count TextBox
   CreateFrame("EditBox", "TriviaBotGUI_TopScoreCount", TriviaBotGUI, "InputBoxTemplate");
   TriviaBotGUI_TopScoreCount:ClearAllPoints();
   TriviaBotGUI_TopScoreCount:SetWidth(40);
   TriviaBotGUI_TopScoreCount:SetHeight(36);
   TriviaBotGUI_TopScoreCount:SetMaxLetters(3);
   TriviaBotGUI_TopScoreCount:SetAutoFocus(false);
   TriviaBotGUI_TopScoreCount:SetNumeric(true);
   TriviaBotGUI_TopScoreCount:SetJustifyH("CENTER");
   TriviaBotGUI_TopScoreCount:SetTextInsets(0,6,0,0);
   TriviaBotGUI_TopScoreCount:SetPoint("TOPLEFT", TriviaBotGUI_TimeoutWarning, "BOTTOMLEFT", 0, editboxspace);
   TriviaBotGUI_TopScoreCount:SetScript("OnEnterPressed", function() this:ClearFocus(); end);
   TriviaBotGUI_TopScoreCount:SetScript("OnTextChanged", TriviaBotGUI_TopScoreCount_Update);
   TriviaBotGUI_TopScoreCount:SetScript("OnEscapePressed", TriviaBotGUI_TopScoreCount_OnEscapePressed);
   TriviaBotGUI_TopScoreCount:SetScript("OnEditFocusLost", TriviaBotGUI_TopScoreCount_OnEditFocusLost);
   TriviaBotGUI_TopScoreCount:SetScript("OnEditFocusGained", TriviaBotGUI_EditBox_Highlight);
   -- Top Score Count TextBox Label
   TriviaBotGUI_TopScoreCount:CreateFontString("TriviaBotGUI_TopScoreCountLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_TopScoreCountLabel:ClearAllPoints();
   TriviaBotGUI_TopScoreCountLabel:SetText("Top Score Count:");
   TriviaBotGUI_TopScoreCountLabel:SetPoint("RIGHT", TriviaBotGUI_TopScoreCount, "LEFT", -15, 0);
   
   -- Top Score Interval TextBox
   CreateFrame("EditBox", "TriviaBotGUI_TopScoreInterval", TriviaBotGUI, "InputBoxTemplate");
   TriviaBotGUI_TopScoreInterval:ClearAllPoints();
   TriviaBotGUI_TopScoreInterval:SetWidth(40);
   TriviaBotGUI_TopScoreInterval:SetHeight(36);
   TriviaBotGUI_TopScoreInterval:SetMaxLetters(3);
   TriviaBotGUI_TopScoreInterval:SetAutoFocus(false);
   TriviaBotGUI_TopScoreInterval:SetNumeric(true);
   TriviaBotGUI_TopScoreInterval:SetJustifyH("CENTER");
   TriviaBotGUI_TopScoreInterval:SetTextInsets(0,6,0,0);
   TriviaBotGUI_TopScoreInterval:SetPoint("TOPLEFT", TriviaBotGUI_TopScoreCount, "BOTTOMLEFT", 0, editboxspace);
   TriviaBotGUI_TopScoreInterval:SetScript("OnEnterPressed", function() this:ClearFocus(); end);
   TriviaBotGUI_TopScoreInterval:SetScript("OnTextChanged", TriviaBotGUI_TopScoreInterval_Update);
   TriviaBotGUI_TopScoreInterval:SetScript("OnEscapePressed", TriviaBotGUI_TopScoreInterval_OnEscapePressed);
   TriviaBotGUI_TopScoreInterval:SetScript("OnEditFocusLost", TriviaBotGUI_TopScoreInterval_OnEditFocusLost);
   TriviaBotGUI_TopScoreInterval:SetScript("OnEditFocusGained", TriviaBotGUI_EditBox_Highlight);
   -- Top Score Interval TextBox Label
   TriviaBotGUI_TopScoreInterval:CreateFontString("TriviaBotGUI_TopScoreIntervalLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_TopScoreIntervalLabel:ClearAllPoints();
   TriviaBotGUI_TopScoreIntervalLabel:SetText("Top Score Interval:");
   TriviaBotGUI_TopScoreIntervalLabel:SetPoint("RIGHT", TriviaBotGUI_TopScoreInterval, "LEFT", -15, 0);

   -- Answers Shown TextBox
   CreateFrame("EditBox", "TriviaBotGUI_AnswersShown", TriviaBotGUI, "InputBoxTemplate");
   TriviaBotGUI_AnswersShown:ClearAllPoints();
   TriviaBotGUI_AnswersShown:SetWidth(40);
   TriviaBotGUI_AnswersShown:SetHeight(36);
   TriviaBotGUI_AnswersShown:SetMaxLetters(3);
   TriviaBotGUI_AnswersShown:SetAutoFocus(false);
   TriviaBotGUI_AnswersShown:SetNumeric(true);
   TriviaBotGUI_AnswersShown:SetJustifyH("CENTER");
   TriviaBotGUI_AnswersShown:SetTextInsets(0,6,0,0);
   TriviaBotGUI_AnswersShown:SetPoint("TOPLEFT", TriviaBotGUI_TopScoreInterval, "BOTTOMLEFT", 0, editboxspace);
   TriviaBotGUI_AnswersShown:SetScript("OnEnterPressed", function() this:ClearFocus(); end);
   TriviaBotGUI_AnswersShown:SetScript("OnTextChanged", TriviaBotGUI_AnswersShown_Update);
   TriviaBotGUI_AnswersShown:SetScript("OnEscapePressed", TriviaBotGUI_AnswersShown_OnEscapePressed);
   TriviaBotGUI_AnswersShown:SetScript("OnEditFocusLost", TriviaBotGUI_AnswersShown_OnEditFocusLost);
   TriviaBotGUI_AnswersShown:SetScript("OnEditFocusGained", TriviaBotGUI_EditBox_Highlight);
   -- Answers Shown TextBox Label
   TriviaBotGUI_AnswersShown:CreateFontString("TriviaBotGUI_AnswersShownLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_AnswersShownLabel:ClearAllPoints();
   TriviaBotGUI_AnswersShownLabel:SetText("Answers Shown:");
   TriviaBotGUI_AnswersShownLabel:SetPoint("RIGHT", TriviaBotGUI_AnswersShown, "LEFT", -15, 0);

   -- Show Answers Checkbox
   CreateFrame("CheckButton", "TriviaBotGUI_ShowAnswersCheckBox", TriviaBotGUI, "OptionsCheckButtonTemplate");
   TriviaBotGUI_ShowAnswersCheckBox:ClearAllPoints();
   TriviaBotGUI_ShowAnswersCheckBox:SetPoint("LEFT", TriviaBotGUI_RoundSize, "RIGHT", 5, 0);
   TriviaBotGUI_ShowAnswersCheckBox:SetScript("OnClick", TriviaBotGUI_ShowAnswersCheckBox_OnClick);
   -- Show Answers Checkbox Label
   TriviaBotGUI_ShowAnswersCheckBox:CreateFontString("TriviaBotGUI_ShowAnswersCheckBoxLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ShowAnswersCheckBoxLabel:ClearAllPoints();
   TriviaBotGUI_ShowAnswersCheckBoxLabel:SetText("Show Answers");
   TriviaBotGUI_ShowAnswersCheckBoxLabel:SetPoint("LEFT", TriviaBotGUI_ShowAnswersCheckBox, "RIGHT", 5, 0);
   
   -- Show Reports Checkbox
   CreateFrame("CheckButton", "TriviaBotGUI_ShowReportsCheckBox", TriviaBotGUI, "OptionsCheckButtonTemplate");
   TriviaBotGUI_ShowReportsCheckBox:ClearAllPoints();
   TriviaBotGUI_ShowReportsCheckBox:SetPoint("TOPLEFT", TriviaBotGUI_ShowAnswersCheckBox, "BOTTOMLEFT", 0, checkboxspace);
   TriviaBotGUI_ShowReportsCheckBox:SetScript("OnClick", TriviaBotGUI_ShowReportsCheckBox_OnClick);
   -- Show Reports Checkbox Label
   TriviaBotGUI_ShowReportsCheckBox:CreateFontString("TriviaBotGUI_ShowReportsCheckBoxLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ShowReportsCheckBoxLabel:ClearAllPoints();
   TriviaBotGUI_ShowReportsCheckBoxLabel:SetText("Show Reports");
   TriviaBotGUI_ShowReportsCheckBoxLabel:SetPoint("LEFT", TriviaBotGUI_ShowReportsCheckBox, "RIGHT", 5, 0);

   -- Show Hints Checkbox
   CreateFrame("CheckButton", "TriviaBotGUI_ShowHintsCheckBox", TriviaBotGUI, "OptionsCheckButtonTemplate");
   TriviaBotGUI_ShowHintsCheckBox:ClearAllPoints();
   TriviaBotGUI_ShowHintsCheckBox:SetPoint("TOPLEFT", TriviaBotGUI_ShowReportsCheckBox, "BOTTOMLEFT", 0, checkboxspace);
   TriviaBotGUI_ShowHintsCheckBox:SetScript("OnClick", TriviaBotGUI_ShowHintsCheckBox_OnClick);
   -- Show Hints Checkbox Label
   TriviaBotGUI_ShowHintsCheckBox:CreateFontString("TriviaBotGUI_ShowHintsCheckBoxLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ShowHintsCheckBoxLabel:ClearAllPoints();
   TriviaBotGUI_ShowHintsCheckBoxLabel:SetText("Show Hints");
   TriviaBotGUI_ShowHintsCheckBoxLabel:SetPoint("LEFT", TriviaBotGUI_ShowHintsCheckBox, "RIGHT", 5, 0);
   
   -- Show Whispers Checkbox
   CreateFrame("CheckButton", "TriviaBotGUI_ShowWhispersCheckBox", TriviaBotGUI, "OptionsCheckButtonTemplate");
   TriviaBotGUI_ShowWhispersCheckBox:ClearAllPoints();
   TriviaBotGUI_ShowWhispersCheckBox:SetPoint("TOPLEFT", TriviaBotGUI_ShowHintsCheckBox, "BOTTOMLEFT", 0, checkboxspace);
   TriviaBotGUI_ShowWhispersCheckBox:SetScript("OnClick", TriviaBotGUI_ShowWhispersCheckBox_OnClick);
   -- Show Whispers Checkbox Label
   TriviaBotGUI_ShowWhispersCheckBox:CreateFontString("TriviaBotGUI_ShowWhispersCheckBoxLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ShowWhispersCheckBoxLabel:ClearAllPoints();
   TriviaBotGUI_ShowWhispersCheckBoxLabel:SetText("Show Whispers");
   TriviaBotGUI_ShowWhispersCheckBoxLabel:SetPoint("LEFT", TriviaBotGUI_ShowWhispersCheckBox, "RIGHT", 5, 0);
   
   -- Report Win Streak Checkbox
   CreateFrame("CheckButton", "TriviaBotGUI_ReportWinStreakCheckBox", TriviaBotGUI, "OptionsCheckButtonTemplate");
   TriviaBotGUI_ReportWinStreakCheckBox:ClearAllPoints();
   TriviaBotGUI_ReportWinStreakCheckBox:SetPoint("TOPLEFT", TriviaBotGUI_ShowWhispersCheckBox, "BOTTOMLEFT", 0, checkboxspace);
   TriviaBotGUI_ReportWinStreakCheckBox:SetScript("OnClick", TriviaBotGUI_ReportWinStreakCheckBox_OnClick);
   -- Report Win Streak Checkbox Label
   TriviaBotGUI_ReportWinStreakCheckBox:CreateFontString("TriviaBotGUI_ReportWinStreakCheckBoxLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ReportWinStreakCheckBoxLabel:ClearAllPoints();
   TriviaBotGUI_ReportWinStreakCheckBoxLabel:SetText("Report Win Streak");
   TriviaBotGUI_ReportWinStreakCheckBoxLabel:SetPoint("LEFT", TriviaBotGUI_ReportWinStreakCheckBox, "RIGHT", 5, 0);
   
   -- Report Personal Checkbox
   CreateFrame("CheckButton", "TriviaBotGUI_ReportPersonalCheckBox", TriviaBotGUI, "OptionsCheckButtonTemplate");
   TriviaBotGUI_ReportPersonalCheckBox:ClearAllPoints();
   TriviaBotGUI_ReportPersonalCheckBox:SetPoint("TOPLEFT", TriviaBotGUI_ReportWinStreakCheckBox, "BOTTOMLEFT", 0, checkboxspace);
   TriviaBotGUI_ReportPersonalCheckBox:SetScript("OnClick", TriviaBotGUI_ReportPersonalCheckBox_OnClick);
   -- Report Personal Checkbox Label
   TriviaBotGUI_ReportPersonalCheckBox:CreateFontString("TriviaBotGUI_ReportPersonalCheckBoxLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ReportPersonalCheckBoxLabel:ClearAllPoints();
   TriviaBotGUI_ReportPersonalCheckBoxLabel:SetText("Report Personal");
   TriviaBotGUI_ReportPersonalCheckBoxLabel:SetPoint("LEFT", TriviaBotGUI_ReportPersonalCheckBox, "RIGHT", 5, 0);
   
   -- Point Mode Checkbox
   CreateFrame("CheckButton", "TriviaBotGUI_PointModeCheckBox", TriviaBotGUI, "OptionsCheckButtonTemplate");
   TriviaBotGUI_PointModeCheckBox:ClearAllPoints();
   TriviaBotGUI_PointModeCheckBox:SetPoint("TOPLEFT", TriviaBotGUI_ReportPersonalCheckBox, "BOTTOMLEFT", 0, checkboxspace);
   TriviaBotGUI_PointModeCheckBox:SetScript("OnClick", TriviaBotGUI_PointModeCheckBox_OnClick);
   -- Point Mode Checkbox Label
   TriviaBotGUI_PointModeCheckBox:CreateFontString("TriviaBotGUI_PointModeCheckBoxLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_PointModeCheckBoxLabel:ClearAllPoints();
   TriviaBotGUI_PointModeCheckBoxLabel:SetText("Point Mode");
   TriviaBotGUI_PointModeCheckBoxLabel:SetPoint("LEFT", TriviaBotGUI_PointModeCheckBox, "RIGHT", 5, 0);
   
   -- Short Channel Tag Checkbox
   CreateFrame("CheckButton", "TriviaBotGUI_ShortChannelTagCheckBox", TriviaBotGUI, "OptionsCheckButtonTemplate");
   TriviaBotGUI_ShortChannelTagCheckBox:ClearAllPoints();
   TriviaBotGUI_ShortChannelTagCheckBox:SetPoint("TOPLEFT", TriviaBotGUI_PointModeCheckBox, "BOTTOMLEFT", 0, checkboxspace);
   TriviaBotGUI_ShortChannelTagCheckBox:SetScript("OnClick", TriviaBotGUI_ShortChannelTagCheckBox_OnClick);
   -- Short Channel Tag Checkbox Label
   TriviaBotGUI_ShortChannelTagCheckBox:CreateFontString("TriviaBotGUI_ShortChannelTagCheckBoxLabel", "ARTWORK", "GameFontNormal");
   TriviaBotGUI_ShortChannelTagCheckBoxLabel:ClearAllPoints();
   TriviaBotGUI_ShortChannelTagCheckBoxLabel:SetText("Short Channel Tag");
   TriviaBotGUI_ShortChannelTagCheckBoxLabel:SetPoint("LEFT", TriviaBotGUI_ShortChannelTagCheckBox, "RIGHT", 5, 0);
   
   -- Start/Stop Button
   CreateFrame("Button", "TriviaBotGUI_StartStopButton", TriviaBotGUI, "OptionsButtonTemplate");
   TriviaBotGUI_StartStopButton:ClearAllPoints();
   TriviaBotGUI_StartStopButton:SetWidth(140);
   TriviaBotGUI_StartStopButton:SetText("Start TriviaBot");
   TriviaBotGUI_StartStopButton:SetPoint("BOTTOMRIGHT", TriviaBotGUI, "BOTTOM", -5, 45);
   TriviaBotGUI_StartStopButton:SetScript("OnClick", TriviaBot_StartStopToggle);

   -- Skip Button
   CreateFrame("Button", "TriviaBotGUI_SkipButton", TriviaBotGUI, "OptionsButtonTemplate");
   TriviaBotGUI_SkipButton:ClearAllPoints();
   TriviaBotGUI_SkipButton:SetWidth(140);
   TriviaBotGUI_SkipButton:SetText("Skip Question");
   TriviaBotGUI_SkipButton:SetPoint("TOPRIGHT", TriviaBotGUI_StartStopButton, "BOTTOMRIGHT", 0, -5);
   TriviaBotGUI_SkipButton:SetScript("OnClick", TriviaBot_SkipQuestion);
   TriviaBotGUI_SkipButton:Disable();
   
   -- Game Score Button
   CreateFrame("Button", "TriviaBotGUI_GameScoresButton", TriviaBotGUI, "OptionsButtonTemplate");
   TriviaBotGUI_GameScoresButton:ClearAllPoints();
   TriviaBotGUI_GameScoresButton:SetWidth(140);
   TriviaBotGUI_GameScoresButton:SetText("Game Scores");
   TriviaBotGUI_GameScoresButton:SetPoint("BOTTOMLEFT", TriviaBotGUI, "BOTTOM", 5, 45);
   TriviaBotGUI_GameScoresButton:SetScript("OnClick", function() TriviaBot_Report("gamereport"); end);

   -- All-Time Score Button
   CreateFrame("Button", "TriviaBotGUI_AllTimeScoresButton", TriviaBotGUI, "OptionsButtonTemplate");
   TriviaBotGUI_AllTimeScoresButton:ClearAllPoints();
   TriviaBotGUI_AllTimeScoresButton:SetWidth(140);
   TriviaBotGUI_AllTimeScoresButton:SetText("All-Time Scores");
   TriviaBotGUI_AllTimeScoresButton:SetPoint("TOPLEFT", TriviaBotGUI_GameScoresButton, "BOTTOMLEFT", 0, -5);
   TriviaBotGUI_AllTimeScoresButton:SetScript("OnClick", function() TriviaBot_Report("alltimereport"); end);
end

----------------------------------------------------------------------------
-- Point Mode CheckBox OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_PointModeCheckBox_OnClick()
   TriviaBot_Config['Point_Mode'] = this:GetChecked();
end

----------------------------------------------------------------------------
-- Question Interval OnEditFocusLost Event
----------------------------------------------------------------------------
function TriviaBotGUI_QuestionInterval_OnEditFocusLost()
   local value = tonumber(this:GetText());
   if (not value or value < 2 or value > 600) then
      TriviaBot_PrintError("Question Interval must be between 2 and 600.");
      this:SetText(TriviaBot_Config['Question_Interval']);
   end
end

----------------------------------------------------------------------------
-- Question Interval OnEscapePressed Event
----------------------------------------------------------------------------
function TriviaBotGUI_QuestionInterval_OnEscapePressed()
   this:SetText(TriviaBot_Config['Question_Interval']);
   this:ClearFocus();
end

----------------------------------------------------------------------------
-- Question Interval Update
----------------------------------------------------------------------------
function TriviaBotGUI_QuestionInterval_Update()
   if(this:GetText() == "") then
      -- Field is blanked out so we do nothing
      return;
   end
   local num = this:GetNumber();
   if (num >= 2 and num <= 600) then
      TriviaBot_Config['Question_Interval'] = num;
   end
end

----------------------------------------------------------------------------
-- Initialize the QuestionList drop-down menu
----------------------------------------------------------------------------
function TriviaBotGUI_QuestionList_Initialize()
   local info;

   for id,_ in pairs(TB_Question_Sets) do
      info = UIDropDownMenu_CreateInfo();
      info.value = id;
      info.text = TriviaBot_Capitalize(TB_Question_Sets[id]['Title']);
      info.func = function() TriviaBotGUI_QuestionList_OnClick() end;
      UIDropDownMenu_AddButton(info);
   end
    
   UIDropDownMenu_SetSelectedValue(TriviaBotGUI_QuestionList, TriviaBot_Config['Question_Set']);
end

----------------------------------------------------------------------------
-- QuestionList OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_QuestionList_OnClick()
   local set = this.value;
   
   if (TB_Question_Sets[set] and set ~= TriviaBot_Config['Question_Set']) then
      TriviaBot_LoadTrivia(set, 0); -- 'All' category
      TriviaBot_Config['Question_Set'] = set;
      TriviaBot_Config['Question_Category'] = 0;
   end
   
   UIDropDownMenu_SetSelectedValue(TriviaBotGUI_QuestionList, TriviaBot_Config['Question_Set']);
   UIDropDownMenu_SetSelectedValue(TriviaBotGUI_CategoryList, 0);
end

----------------------------------------------------------------------------
-- Question Timeout OnEditFocusLost Event
----------------------------------------------------------------------------
function TriviaBotGUI_QuestionTimeout_OnEditFocusLost()
   local value = tonumber(this:GetText());
   if (not value or value < 10 or value > 120) then
      TriviaBot_PrintError("Question Timeout must be between 10 and 120.");
      this:SetText(TriviaBot_Config['Question_Timeout']);
   end
end

----------------------------------------------------------------------------
-- Question Timeout OnEscapePressed Event
----------------------------------------------------------------------------
function TriviaBotGUI_QuestionTimeout_OnEscapePressed()
   this:SetText(TriviaBot_Config['Question_Timeout']);
   this:ClearFocus();
end

----------------------------------------------------------------------------
-- Question Timeout Update
----------------------------------------------------------------------------
function TriviaBotGUI_QuestionTimeout_Update()
   if(this:GetText() == "") then
      -- Field is blanked out so we do nothing
      return;
   end
   local num = this:GetNumber();
   if (num >= 10 and num <= 120) then
      TriviaBot_Config['Question_Timeout'] = num;
      if (TriviaBot_Config['Timeout_Warning']*2 > num) then
         TriviaBot_Config['Timeout_Warning'] = math.floor(num/2);
         TriviaBotGUI_TimeoutWarning:SetText(TriviaBot_Config['Timeout_Warning']);
      end
   end
end

----------------------------------------------------------------------------
-- Report Personal OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_ReportPersonalCheckBox_OnClick()
   TriviaBot_Config['Report_Personal'] = this:GetChecked();
end

----------------------------------------------------------------------------
-- Report Win Streak OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_ReportWinStreakCheckBox_OnClick()
   TriviaBot_Config['Report_Win_Streak'] = this:GetChecked();
end

----------------------------------------------------------------------------
-- Round Size OnEditFocusLost Event
----------------------------------------------------------------------------
function TriviaBotGUI_RoundSize_OnEditFocusLost()
   local value = tonumber(this:GetText());
   if (not value or (value ~= 0 and value < 5) or value > 100) then
      TriviaBot_PrintError("Round Size must be between 5 and 100 or 0 as unlimited.");
      this:SetText(TriviaBot_Config['Round_Size']);
   end
end

----------------------------------------------------------------------------
-- Round Size OnEscapePressed Event
----------------------------------------------------------------------------
function TriviaBotGUI_RoundSize_OnEscapePressed()
   this:SetText(TriviaBot_Config['Round_Size']);
   this:ClearFocus();
end

----------------------------------------------------------------------------
-- Round Size Update
----------------------------------------------------------------------------
function TriviaBotGUI_RoundSize_Update()
   if(this:GetText() == "") then
      -- Field is blanked out so we do nothing
      return;
   end
   local num = this:GetNumber();
   if (num == 0 or (num >= 5 and num <= 100)) then
      TriviaBot_Config['Round_Size'] = num;
   end
end

----------------------------------------------------------------------------
-- Short Channel Tag OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_ShortChannelTagCheckBox_OnClick()
   TriviaBot_Config['Short_Tag'] = this:GetChecked();
   if (this:GetChecked()) then
      TB_Channel_Prefix = TB_Short_Prefix;
   else
      TB_Channel_Prefix = TB_Long_Prefix;
   end
end

----------------------------------------------------------------------------
-- Show Answers OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_ShowAnswersCheckBox_OnClick()
   TriviaBot_Config['Show_Answers'] = this:GetChecked();
end

----------------------------------------------------------------------------
-- Show Hints OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_ShowHintsCheckBox_OnClick()
   TriviaBot_Config['Show_Hints'] = this:GetChecked();
end

----------------------------------------------------------------------------
-- Show Reports OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_ShowReportsCheckBox_OnClick()
   TriviaBot_Config['Show_Reports'] = this:GetChecked();
end

----------------------------------------------------------------------------
-- Show Whispers OnClick Event
----------------------------------------------------------------------------
function TriviaBotGUI_ShowWhispersCheckBox_OnClick()
   TriviaBot_Config['Show_Whispers'] = this:GetChecked();
end

----------------------------------------------------------------------------
-- Start/Stop GUI Toggle
----------------------------------------------------------------------------
function TriviaBotGUI_StartStopToggle()
   if (TB_Running) then
      TriviaBotGUI_StartStopButton:SetText("Stop TriviaBot");
      UIDropDownMenu_DisableDropDown(TriviaBotGUI_ChatType);
      TriviaBotGUI_RoundSize:EnableMouse(false);
      TriviaBotGUI_RoundSize:ClearFocus();
      TriviaBotGUI_RoundSize:SetTextColor(1,0,0);
      TriviaBotGUI_PointModeCheckBox:Disable();
      if (TriviaBot_Config['Chat_Type'] == "channel") then
         TriviaBotGUI_Channel:EnableMouse(false);
         TriviaBotGUI_Channel:ClearFocus();
         TriviaBotGUI_Channel:SetTextColor(1,0,0);
         TriviaBotGUI_ChannelButton:Disable();
      end
   else
      TriviaBotGUI_StartStopButton:SetText("Start TriviaBot");
      TriviaBotGUI_SkipButton:Disable();
      UIDropDownMenu_EnableDropDown(TriviaBotGUI_ChatType);
      TriviaBotGUI_RoundSize:EnableMouse(true);
      TriviaBotGUI_RoundSize:SetTextColor(1,1,1);
      TriviaBotGUI_PointModeCheckBox:Enable();
      if (TriviaBot_Config['Chat_Type'] == "channel") then
         TriviaBotGUI_Channel:EnableMouse(true);
         TriviaBotGUI_Channel:SetTextColor(1,1,1);
         TriviaBotGUI_ChannelButton:Enable();
      end
   end
end

----------------------------------------------------------------------------
-- Timeout Warning OnEditFocusLost Event
----------------------------------------------------------------------------
function TriviaBotGUI_TimeoutWarning_OnEditFocusLost()
   local value = tonumber(this:GetText());
   if (not value or value < 5 or value > 60 or value*2 > TriviaBot_Config['Question_Timeout']) then
      TriviaBot_PrintError("Timeout Warning must be between 5 and 60 and maximum half of Question Timeout.");
      this:SetText(TriviaBot_Config['Timeout_Warning']);
   end
end

----------------------------------------------------------------------------
-- Timeout Warning OnEscapePressed Event
----------------------------------------------------------------------------
function TriviaBotGUI_TimeoutWarning_OnEscapePressed()
   this:SetText(TriviaBot_Config['Timeout_Warning']);
   this:ClearFocus();
end

----------------------------------------------------------------------------
-- Timeout Warning Update
----------------------------------------------------------------------------
function TriviaBotGUI_TimeoutWarning_Update()
   if(this:GetText() == "") then
      -- Field is blanked out so we do nothing
      return;
   end
   local num = this:GetNumber();
   if (num >= 5 and num <= 60) then
      if (TriviaBot_Config['Question_Timeout'] < num*2) then
         TriviaBot_Config['Timeout_Warning'] = math.floor(TriviaBot_Config['Question_Timeout']/2);
         TriviaBotGUI_TimeoutWarning:SetText(TriviaBot_Config['Timeout_Warning']);
      else
         TriviaBot_Config['Timeout_Warning'] = num;
      end
   end
end

----------------------------------------------------------------------------
-- Top Score Count OnEditFocusLost Event
----------------------------------------------------------------------------
function TriviaBotGUI_TopScoreCount_OnEditFocusLost()
   local value = tonumber(this:GetText());
   if (not value or value < 3 or value > 10) then
      TriviaBot_PrintError("Question Interval must be between 3 and 10.");
      this:SetText(TriviaBot_Config['Top_Score_Count']);
   end
end

----------------------------------------------------------------------------
-- Top Score Count OnEscapePressed Event
----------------------------------------------------------------------------
function TriviaBotGUI_TopScoreCount_OnEscapePressed()
   this:SetText(TriviaBot_Config['Top_Score_Count']);
   this:ClearFocus();
end

----------------------------------------------------------------------------
-- Top Score Count Update
----------------------------------------------------------------------------
function TriviaBotGUI_TopScoreCount_Update()
   if(this:GetText() == "") then
      -- Field is blanked out so we do nothing
      return;
   end
   local num = this:GetNumber();
   if (num >= 3 and num <= 10) then
      TriviaBot_Config['Top_Score_Count'] = num;
   end
end

----------------------------------------------------------------------------
-- Top Score Interval OnEditFocusLost Event
----------------------------------------------------------------------------
function TriviaBotGUI_TopScoreInterval_OnEditFocusLost()
   local value = tonumber(this:GetText());
   if (not value or value < 5 or value > 50) then
      TriviaBot_PrintError("Question Interval must be between 5 and 50.");
      this:SetText(TriviaBot_Config['Top_Score_Interval']);
   end
end

----------------------------------------------------------------------------
-- Top Score Interval OnEscapePressed Event
----------------------------------------------------------------------------
function TriviaBotGUI_TopScoreInterval_OnEscapePressed()
   this:SetText(TriviaBot_Config['Top_Score_Interval']);
   this:ClearFocus();
end

----------------------------------------------------------------------------
-- Top Score Interval Update
----------------------------------------------------------------------------
function TriviaBotGUI_TopScoreInterval_Update()
   if(this:GetText() == "") then
      -- Field is blanked out so we do nothing
      return;
   end
   local num = this:GetNumber();
   if (num >= 5 and num <= 50) then
      TriviaBot_Config['Top_Score_Interval'] = num;
   end
end

----------------------------------------------------------------------------
-- Update GUI
----------------------------------------------------------------------------
function TriviaBotGUI_Update()
   -- Display Version
   TriviaBotGUI_HeaderLabel:SetText("TriviaBot " .. TB_VERSION);

   -- Load Channel
   TriviaBotGUI_Channel:SetText(TriviaBot_Config['Channel']);
   
   -- Load Round Size
   TriviaBotGUI_RoundSize:SetText(TriviaBot_Config['Round_Size']);
   
   -- Load Question Interval
   TriviaBotGUI_QuestionInterval:SetText(TriviaBot_Config['Question_Interval']);
   
   -- Load Question Timeout
   TriviaBotGUI_QuestionTimeout:SetText(TriviaBot_Config['Question_Timeout']);
   
   -- Load Timeout Warning
   TriviaBotGUI_TimeoutWarning:SetText(TriviaBot_Config['Timeout_Warning']);
   
   -- Load Top Score Count
   TriviaBotGUI_TopScoreCount:SetText(TriviaBot_Config['Top_Score_Count']);
   
   -- Load Top Score Interval
   TriviaBotGUI_TopScoreInterval:SetText(TriviaBot_Config['Top_Score_Interval']);
   
   -- Load Answers Shown
   TriviaBotGUI_AnswersShown:SetText(TriviaBot_Config['Answers_Shown']);

   -- Set Show Answers state
   TriviaBotGUI_ShowAnswersCheckBox:SetChecked(TriviaBot_Config['Show_Answers']);
   
   -- Set Show Reports state
   TriviaBotGUI_ShowReportsCheckBox:SetChecked(TriviaBot_Config['Show_Reports']);
   
   -- Set Show Hints state
   TriviaBotGUI_ShowHintsCheckBox:SetChecked(TriviaBot_Config['Show_Hints']);
   
   -- Set Show Whispers state
   TriviaBotGUI_ShowWhispersCheckBox:SetChecked(TriviaBot_Config['Show_Whispers']);
   
   -- Set Report Win Streak state
   TriviaBotGUI_ReportWinStreakCheckBox:SetChecked(TriviaBot_Config['Report_Win_Streak']);
   
   -- Set Report Personal state
   TriviaBotGUI_ReportPersonalCheckBox:SetChecked(TriviaBot_Config['Report_Personal']);
   
   -- Set Point Mode state
   TriviaBotGUI_PointModeCheckBox:SetChecked(TriviaBot_Config['Point_Mode']);
   
   -- Short Channel Tag state
   TriviaBotGUI_ShortChannelTagCheckBox:SetChecked(TriviaBot_Config['Short_Tag']);
   if (TriviaBot_Config['Short_Tag']) then
      TB_Channel_Prefix = TB_Short_Prefix;
   end
   
   -- Make sure QuestionList drop-down menu is initalized
   TriviaBotGUI_QuestionList_Initialize();
   
   -- Make sure CategoryList drop-down menu is initalized
   TriviaBotGUI_CategoryList_Initialize();
   
   -- Make sure ChatType drop-down menu is initalized
   TriviaBotGUI_ChatType_Initialize();

   -- Disable custom channel stuff
   if (TriviaBot_Config['Chat_Type'] ~= "channel") then
      TriviaBotGUI_Channel:EnableMouse(false);
      TriviaBotGUI_Channel:SetTextColor(1,0,0);
      TriviaBotGUI_ChannelButton:Disable();
   else
      TriviaBotGUI_Channel:EnableMouse(true);
      TriviaBotGUI_Channel:SetTextColor(1,1,1);
      TriviaBotGUI_ChannelButton:Enable();
   end
end