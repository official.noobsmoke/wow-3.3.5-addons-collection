## Interface: 30300
## Title: TriviaBot
## Author: TriviaBot Team
## Version: 2.5.4 for WoW 3.3
## Notes: If you ever find yourself being bored while questing, grinding, raiding or just standing around in a city, you can use this addon to host a fun quiz others can join to play. Remember not to disturb other players!. 
## eMail: zizanzu@gmail.com
## URL: http://triviabot.zizanzu.org
## X-Category: Chat/Communication
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: TriviaBot_Config,TriviaBot_Scores
TriviaBot.xml
