## Interface: 30300
## Title: Slam Alert
## Author: Malivil , Gup
## Version: 0.10
## Notes: Plays a sound and displays a message when you gain the Slam! buff.
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: SlamAlert
SlamAlert.xml
SlamAlertOptions.xml
