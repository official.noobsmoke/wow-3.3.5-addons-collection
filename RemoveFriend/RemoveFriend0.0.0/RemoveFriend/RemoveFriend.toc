## Interface: 30300
## Title: RemoveFriend
## Notes: Restores the "Remove Friend" button to the social frame.
## Author: Furyrage (Kargath)
RemoveFriend.xml