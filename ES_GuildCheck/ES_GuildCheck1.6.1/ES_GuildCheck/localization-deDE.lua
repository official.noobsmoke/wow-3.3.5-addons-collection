if ( GetLocale() == "deDE" ) then
	ES_GUILDCHECK_GUILDLEFT = "hat die Gilde verlassen";
	ES_GUILDCHECK_GUILDJOIN = "hat sich der Gilde angeschlossen";
	ES_GUILDCHECK_RANK = "Rang";
	ES_GUILDCHECK_LEVEL = "Stufe";
	ES_GUILDCHECK_CLASS = "Klasse";
	ES_GUILDCHECK_NOTE = "Notiz";
	ES_GUILDCHECK_ONOTE = "Offiziersnotiz";
	ES_GUILDCHECK_TOTAL = "Insgesamt";
	ES_GUILDCHECK_CHANGES = "Änderungen";
	ES_GUILDCHECK_ONECHANGE = "Insgesamt 1 Änderung";
	ES_GUILDCHECK_NOCHANGE = "Keine Änderungen gefunden";
	ES_GUILDCHECK_HELP1 = "Gib \"/egc <|cffffffffcmd|r>\" ein, wobei <|cffffffffcmd|r> folgendes sein kann:";
	ES_GUILDCHECK_HELP2 = "  |cffffffffoffline|r: Zeigt die Änderungen seit der letzten Sitzung.";
	ES_GUILDCHECK_HELP3 = "  |cffffffffonline|r: Zeigt die Änderungen in dieser Sitzung.";
	ES_GUILDCHECK_HELP4 = "  |cffffffffshow|r: Zeigt ein Fenster mit den Änderungen, die auch kopiert werden können.";
	ES_GUILDCHECK_HELP5 = "  |cfffffffflogin|r <\"|cffffff00gui|r\" or \"|cffffff00chat|r\">: Bestimmt, ob die Änderungen im Chat oder in der GUI beim Einloggen angezeigt werden (aktuell: |cffffff00%s|r).";
	ES_GUILDCHECK_HELP6 = "  |cffffffffwait|r <|cffffff00number|r>: Gibt an, wie viele Sekunden nach dem Einloggen gewartet wird, bis die Änderungen ausgegeben werden (aktuell: |cffffff00%s|r).";
	ES_GUILDCHECK_LOADED = "%s geladen. Ausgabe der Änderungen wird in ca. |cffffffff%i|r Sekunden ausgegeben.";
end