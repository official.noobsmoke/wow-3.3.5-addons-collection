﻿## Interface: 20100
## Title: ES_GuildCheck (1.61)
## Credits: Thanks to Drizzd for the GUI.
## Notes: Shows changes in your guild since your last time online.
## Notes-deDE: Zeigt Veränderungen in der Gilde seit dem letzten Mal online sein.
## Author: Tunhadil
## Dependencies:
## OptionalDeps: 
## SavedVariables: ES_GuildCheck_Data
## SavedVariablesPerCharacter: ES_GuildCheck_Options
localization-enGB.lua
localization-deDE.lua
ES_GuildCheck.xml