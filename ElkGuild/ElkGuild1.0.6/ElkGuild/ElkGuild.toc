﻿## Interface: 40000
## X-Curse-Packaged-Version: r6
## X-Curse-Project-Name: ElkGuild
## X-Curse-Project-ID: elkguild
## X-Curse-Repository-ID: wow/elkguild/mainline

## Title: ElkGuild
## Notes: Displays online guild mates.
## Notes-esES: Muestra tus companyeros de hermandad conectados.
## Notes-koKR: 접속중인 길드원의 정보를 표시합니다.
## Notes-ruRU: Показывает кто из игроков гильдии в онлайне
## Notes-zhTW: 持續追蹤線上公會名單。
## Author: Elkano
## Version: 1.0.0-6
## X-Website: http://www.wowace.com/projects/elkguild/
## X-Category: Interface Enhancements

## X-Revision: 6
## X-Date: 2010-11-02T17:02:15Z

## OptionalDeps: Ace3, CallbackHandler-1.0, LibDataBroker-1.1, LibQTip-1.0, LibStub

## SavedVariables: ElkGuildDB

#@no-lib-strip@
libs\LibStub\Libstub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
libs\AceAddon-3.0\AceAddon-3.0.xml
libs\AceEvent-3.0\AceEvent-3.0.xml
libs\AceHook-3.0\AceHook-3.0.xml
libs\AceDB-3.0\AceDB-3.0.xml
libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
libs\AceConsole-3.0\AceConsole-3.0.xml
libs\AceGUI-3.0\AceGUI-3.0.xml
libs\AceLocale-3.0\AceLocale-3.0.xml
libs\AceConfig-3.0\AceConfig-3.0.xml
libs\LibDBIcon-1.0\LibDBIcon-1.0.lua
libs\LibQTip-1.0\LibQTip-1.0.lua
#@end-no-lib-strip@

ElkGuild.lua
