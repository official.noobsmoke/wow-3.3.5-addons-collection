local _G				= getfenv(0)

local table_insert		= _G.table.insert
local table_remove		= _G.table.remove
local table_sort		= _G.table.sort

local ElkGuild = LibStub("AceAddon-3.0"):NewAddon("ElkGuild", "AceBucket-3.0", "AceConsole-3.0", "AceEvent-3.0")

local LDB = LibStub("LibDataBroker-1.1")
local LDBIcon = LibStub("LibDBIcon-1.0")
local LibQTip = LibStub("LibQTip-1.0")

local DBdefaults = {
	profile = {
		minimap = {
			hide = false,
		},
	},
}


local L = setmetatable({
	TEXT_NOGUILD = "No Guild",
	TEXT_UPDATING = "Updating...",
	TOOLTIP_COLUMN_HEADER_NAME = "Name",
	TOOLTIP_COLUMN_HEADER_LEVEL = "Level",
	TOOLTIP_COLUMN_HEADER_ZONE = "Zone",
	TOOLTIP_COLUMN_HEADER_NOTES = "Notes",
	TOOLTIP_COLUMN_HEADER_RANK = "Rank",
	TOOLTIP_TEXT_MOTD = "MOTD:",
	TOOLTIP_TEXT_NOGUILD = "You aren't in a guild.",
}, {__index = function(self, key) return key end })

-- -----
-- Roster Updater
-- -----
local updateTime = 0
local ROSTER_UPDATE_THROTTLE = 15
local updateFrame = CreateFrame("frame")
updateFrame:Hide()
updateFrame:SetScript("OnUpdate", function(self, elapsed)
	local value_GetTime = GetTime()
	if value_GetTime > updateTime then
		updateTime = value_GetTime + ROSTER_UPDATE_THROTTLE
		GuildRoster()
	end
end)

-- -----
-- player table cache
-- -----
local tablecache = {}
local function recycleplayertable(t)
	while #t > 0 do
		table_insert(tablecache, table_remove(t))
	end
	return t
end
local function getplayertable(...)
	local t
	if #tablecache > 0 then
		t = table_remove(tablecache)
	else
		t = {}
	end
	for i = 1, select('#', ...) do
		t[i] = select(i, ...)
	end
	return t
end

-- -----
local function do_OnEnter(frame)
	local tooltip = LibQTip:Acquire("ElkGuildTip")
	tooltip:SmartAnchorTo(frame)
	tooltip:SetAutoHideDelay(0.1, frame)
	tooltip:EnableMouse(true)
	if IsInGuild() then
		ElkGuild:UpdateTooltip()
		tooltip:Show()
		updateFrame:Show()
	else
		ElkGuild:UpdateTooltip_noGuild()
		tooltip:Show()
	end
end

local function do_OnLeave()
	-- empty dummy
end

local function do_OnClick(button)
	ToggleGuildFrame() 
end

function ElkGuild:OnInitialize()
	self.players = {}

	self.dbo = LibStub("LibDataBroker-1.1"):NewDataObject("ElkGuild", {
			type = "data source",
			text = L["TEXT_UPDATING"],
			icon = [[Interface\Addons\ElkGuild\ElkGuild]],
			OnEnter = do_OnEnter,
			OnLeave = do_OnLeave,
			OnClick = do_OnClick,
		})

	self.db = LibStub("AceDB-3.0"):New("ElkGuildDB", DBdefaults)
	self.db.RegisterCallback(self, "OnProfileChanged", "RefreshConfig")
	self.db.RegisterCallback(self, "OnProfileCopied", "RefreshConfig")
	self.db.RegisterCallback(self, "OnProfileReset", "RefreshConfig")

	LDBIcon:Register("ElkGuild", self.dbo, self.db.profile.minimap)
end

function ElkGuild:OnEnable()
	self:RegisterEvent("PLAYER_GUILD_UPDATE")
	self:PLAYER_GUILD_UPDATE()
end

function ElkGuild:RefreshConfig()
	-- minimap icon
	LDBIcon:Refresh("ElkGuild", self.db.profile.minimap)
	
	-- LDB text
	self:UpdateText()
end

function ElkGuild:PLAYER_GUILD_UPDATE(unit)
	if unit and unit ~= "player" then return end
--	self:print("Debug: PLAYER_GUILD_UPDATE fired...")
	if IsInGuild() then
		if not self.bucket_GUILD_ROSTER_UPDATE then
			self.bucket_GUILD_ROSTER_UPDATE = self:RegisterBucketEvent("GUILD_ROSTER_UPDATE", .5)
			self:RegisterEvent("GUILD_MOTD")
		end
		GuildRoster()
	else
		if self.bucket_GUILD_ROSTER_UPDATE then
			self:UnregisterBucket(self.bucket_GUILD_ROSTER_UPDATE)
			self:UnregisterEvent("GUILD_MOTD")
		end
		self:UpdateText()
	end
end

function ElkGuild:GUILD_ROSTER_UPDATE()
--~ 	self:Print("GUILD_ROSTER_UPDATE fired...")

	updateTime = GetTime() + ROSTER_UPDATE_THROTTLE

	self:UpdateData()

	-- update the LDB text
	self:UpdateText()
	
	-- update the tooltip
	self:UpdateTooltip()
end

function ElkGuild:GUILD_MOTD()
	self:UpdateTooltip()
end

local sorts ={
	NAME =	function(a,b)
				return a[1]<b[1]
			end,
	CLASS =	function(a,b)
				if a[3]<b[3] then
					return true
				elseif a[3]>b[3] then
					return false
				else -- fallback to LEVEL
					if a[2]<b[2] then
						return true
					elseif a[2]>b[2] then
						return false
					else -- fallback to NAME
						return a[1]<b[1]
					end
				end
			end,
	LEVEL =	function(a,b)
				if a[2]<b[2] then
					return true
				elseif a[2]>b[2] then
					return false
				else -- fallback to CLASS
					if a[3]<b[3] then
						return true
					elseif a[3]>b[3] then
						return false
					else -- fallback to NAME
						return a[1]<b[1]
					end
				end
			end,
	ZONE =	function(a,b)
				if a[5]<b[5] then
					return true
				elseif a[5]>b[5] then
					return false
				else -- fallback to CLASS
					if a[3]<b[3] then
						return true
					elseif a[3]>b[3] then
						return false
					else -- fallback to LEVEL
						if a[2]<b[2] then
							return true
						elseif a[2]>b[2] then
							return false
						else -- fallback to NAME
							return a[1]<b[1]
						end
					end
				end
			end,
	RANK = function(a,b)
				if a[10]<b[10] then
					return true
				elseif a[10]>b[10] then
					return false
				else -- fallback to CLASS
					if a[3]<b[3] then
						return true
					elseif a[3]>b[3] then
						return false
					else -- fallback to LEVEL
						if a[2]<b[2] then
							return true
						elseif a[2]>b[2] then
							return false
						else -- fallback to NAME
							return a[1]<b[1]
						end
					end
				end
			end,
}

function ElkGuild:UpdateData()
	if IsInGuild() then
		local players = recycleplayertable(self.players)
		local playersShown = 0
		local playersOnline = 0
		local numGuildMembers = GetNumGuildMembers()
		local name, rank, rankIndex, level, class, zone, note, officernote, online, status, classconst
		for i = 1, numGuildMembers, 1 do
			name, rank, rankIndex, level, class, zone, note, officernote, online, status, classconst = GetGuildRosterInfo(i)
			if note == "" then note = nil end
			if officernote == "" then officernote = nil end
			if online then
				playersOnline = playersOnline + 1
--~ 				if self:checkFilter(classconst, level, zone) then
					playersShown = playersShown + 1
					table_insert(players, getplayertable(name or UNKNOWN, level or -1, class or UNKNOWN, classconst, zone or UNKNOWN, status, note, officernote, rank or UNKNOWN, rankIndex or -1 ))
--~ 				end
			end
		end
--~ 		table_sort(players, sorts[self.db.profile.tooltip.sort])
 		table_sort(players, sorts["ZONE"])
		
		self.players = players
		self.playersShown = playersShown
		self.playersOnline = playersOnline
		self.playersTotal = GetNumGuildMembers(true)
	else
		self.players = recycleplayertable(self.players)
		self.playersShown = 0
		self.playersOnline = 0
		self.playersTotal = 0
	end
end

function ElkGuild:UpdateText()
	if IsInGuild() then
		self.dbo.text = self.playersOnline.."/"..self.playersTotal
	else
		self.dbo.text = L["TEXT_NOGUILD"]
	end
end

local function OnNameClick(frame, name, button)
	if not name then return end
	if IsAltKeyDown() then
		InviteUnit(name)
	else
		SetItemRef("player:"..name, "|Hplayer:"..name.."|h["..name.."|h", "LeftButton")
	end
end

local RAID_CLASS_COLORS_hex = {}
for k, v in pairs(RAID_CLASS_COLORS) do
	RAID_CLASS_COLORS_hex[k] = ("|cff%02x%02x%02x"):format(v.r * 255, v.g * 255, v.b * 255)
end

local function RGB2HEX(red, gree, blue)
	retrun ("|cff%02x%02x%02x"):format(red * 255, green * 255, blue * 255)
end

function ElkGuild:UpdateTooltip_noGuild()
	if not LibQTip:IsAcquired("ElkGuildTip") then
		return
	end
	local tooltip = LibQTip:Acquire("ElkGuildTip")
	tooltip:Clear()
	tooltip:SetColumnLayout(1, "CENTER")
	tooltip:AddHeader("|cfffed100Elk|cffffffffGuild")
	tooltip:AddLine(L["TOOLTIP_TEXT_NOGUILD"])
end

function ElkGuild:UpdateTooltip()
	if not LibQTip:IsAcquired("ElkGuildTip") then
		updateFrame:Hide()
		return
	end
	local tooltip = LibQTip:Acquire("ElkGuildTip")
	tooltip:Clear()

	local lineNum, colNum
	tooltip:SetColumnLayout(6, "CENTER", "LEFT", "CENTER", "CENTER", "CENTER", "RIGHT")

	lineNum = tooltip:AddHeader()
	tooltip:SetCell(lineNum, 1, "|cfffed100Elk|cffffffffGuild", tooltip:GetHeaderFont(), "CENTER", 6)

	lineNum = tooltip:AddHeader()
	tooltip:SetCell(lineNum, 1, "|cffffd200"..L["TOOLTIP_TEXT_MOTD"], nil, "LEFT", 6)
	lineNum = tooltip:AddLine()
	tooltip:SetCell(lineNum, 1, (GetGuildRosterMOTD()), nil, "LEFT", 6)
	lineNum = tooltip:AddLine(" ")

	lineNum = tooltip:AddLine()
	colNum = 2
	lineNum, colNum = tooltip:SetCell(lineNum, colNum, "|cffffd200"..L["TOOLTIP_COLUMN_HEADER_NAME"], nil, "CENTER")
	lineNum, colNum = tooltip:SetCell(lineNum, colNum, "|cffffd200"..L["TOOLTIP_COLUMN_HEADER_LEVEL"], nil, "CENTER")
	lineNum, colNum = tooltip:SetCell(lineNum, colNum, "|cffffd200"..L["TOOLTIP_COLUMN_HEADER_ZONE"], nil, "CENTER")
	lineNum, colNum = tooltip:SetCell(lineNum, colNum, "|cffffd200"..L["TOOLTIP_COLUMN_HEADER_NOTES"], nil, "CENTER")
	lineNum, colNum = tooltip:SetCell(lineNum, colNum, "|cffffd200"..L["TOOLTIP_COLUMN_HEADER_RANK"], nil, "CENTER")
	lineNum = tooltip:AddSeparator(2)
	
	for _, player in ipairs(self.players) do
		local sameGroup = (UnitInParty(player[1]) or UnitInRaid(player[1])) and true
		local levelcolor = GetQuestDifficultyColor(player[2])
		local tipnote = player[7] and "["..player[7].."]" or "-"
		if CanViewOfficerNote() then
			tipnote = tipnote .. (player[8] and " ["..player[8].."]" or " -")
		end
		lineNum = tooltip:AddLine()
		colNum = 1
		lineNum, colNum = tooltip:SetCell(lineNum, colNum, sameGroup and "|TInterface\\Buttons\\UI-CheckBox-Check:18|t" or "")		-- same group?
		lineNum, colNum = tooltip:SetCell(lineNum, colNum, player[6]..RAID_CLASS_COLORS_hex[player[4]]..player[1])					-- status / name
		lineNum, colNum = tooltip:SetCell(lineNum, colNum, player[2])																-- level
		lineNum, colNum = tooltip:SetCell(lineNum, colNum, player[5])																-- zone
		lineNum, colNum = tooltip:SetCell(lineNum, colNum, tipnote)																	-- notes
		lineNum, colNum = tooltip:SetCell(lineNum, colNum, player[9])																-- rank
		tooltip:SetLineScript(lineNum, "OnMouseUp", OnNameClick, player[1])
	end
	tooltip:UpdateScrolling()
end