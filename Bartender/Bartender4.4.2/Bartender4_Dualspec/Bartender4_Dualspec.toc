﻿## Interface: 30300
## Title: Bartender4 - Dualspec
## Notes: Automatically changes profiles when changing spec
## Author: profalbert
## X-Category: Action Bars
## X-License: All rights reserved.
## X-Curse-Packaged-Version: v1.2
## X-Curse-Project-Name: Bartender4_Dualspec
## X-Curse-Project-ID: bartender4_dualspec
## X-Curse-Repository-ID: wow/bartender4_dualspec/mainline

## SavedVariables: Bartender4DualspecDB
## Dependencies: Bartender4

libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml

Core.lua


