local AB_Version = "1.0.0";
local AB_Name = "Auto Busy";
local AB_BLUE = "|c000099ff";
local AB_YELLOW = "|cffffff55";
local AB_END_COLOR = "|r";
local AB_Title = AB_BLUE .. AB_Name .. ":" .. AB_END_COLOR .. " ";

local EVENTS = {};

-- Event ADDON_LOADED
EVENTS.ADDON_LOADED = "ADDON_LOADED";
EVENTS.PLAYER_FLAGS_CHANGED = "PLAYER_FLAGS_CHANGED";

local options = {
	debug = {message = "debug"},
	toggle = {message = "addon"},
}

function AB_OnLoad(self)
	for _,v in pairs(EVENTS) do
		self:RegisterEvent(strupper(v));
	end
	
	AB_RegisterSlashCommands();
	AB_LocalMessage(AB_BLUE .. AB_Name .. " v" .. AB_Version .. " by Tiago Costa." .. AB_END_COLOR);
	AB_LocalMessage("Type /ab or /autobusy for options.");
end

function AB_RegisterSlashCommands()
	SlashCmdList["AB4832_"] = AB_ProcessSlashCommand;
	SLASH_AB4832_1 = "/autobusy";
	SLASH_AB4832_2 = "/ab";
end

function AB_ProcessSlashCommand(option)
	option = strlower(option);
	
	if option == "toggle" or option == "debug" then
		AB_Toggle(option);
	elseif option == "status" then
		AB_ShowStatus("toggle");
	else
		AB_ShowHelp();
	end
end

function AB_Toggle(option)
	AB_Options[option] = not AB_Options[option];
	AB_ShowStatus(option);
end

function AB_ShowStatus(option)
	local message = AB_Title;
	message = message .. options[option].message .. " is ";
	message = message .. (AB_Options[option] and ((options[option].status and options[option].status[1]) or "enabled") or ((options[option].status and options[option].status[2]) or "disabled")) .. ".";
		
	AB_LocalMessage(message);
	AB_HUDMsg(message);
end

function AB_ShowHelp()
	AB_LocalMessage(AB_BLUE .. AB_Name .." v" .. AB_Version .. AB_END_COLOR);
	AB_LocalMessage(AB_YELLOW .. "Usage:");
	AB_LocalMessage(AB_YELLOW .. "    /ab toggle" .. AB_END_COLOR .. " - Enables / Disables the addon.");
	AB_LocalMessage(AB_YELLOW .. "    /ab status" .. AB_END_COLOR .. " - Shows your current settings.");
end

function AB_OnEvent(self, event, ...)
	if event == EVENTS.ADDON_LOADED and ... == "AutoBusy" then
		if not AB_Options then
			AB_Options = {
				toggle = false, 
				debug = false, 
			};
		end
		
		SendChatMessage("", AB_Options["toggle"] and "DND");
		
	elseif event == EVENTS.PLAYER_FLAGS_CHANGED then
		if not UnitIsAFK("player") and not UnitIsDND("player") then
			SendChatMessage("", AB_Options["toggle"] and "DND");
		end
	end
end

function AB_HUDMsg(message)
   UIErrorsFrame:AddMessage(message, 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
end

function AB_LocalMessage(message)
	DEFAULT_CHAT_FRAME:AddMessage(tostring(message));
end

function AB_Debug(message)
	if AB_Options.debug then
		AB_LocalMessage(AB_BLUE .. "[" .. AB_Name .. "]" .. AB_END_COLOR .. " Debug: " .. message);
	end
end