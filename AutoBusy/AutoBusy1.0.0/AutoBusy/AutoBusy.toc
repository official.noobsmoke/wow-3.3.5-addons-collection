## Interface: 40200
## Title: |cff0000ffAuto Busy|r
## Version: 1.0.0
## Notes: |nAs the addon name so wisely states this only does one thing, it puts you as Busy right after you logon. I made this just to get rid of annoying people. This way I'm always busy :)|n|n
## Author: Tiago Costa
## SavedVariablesPerCharacter: AB_Options
AutoBusy.lua
AutoBusy.xml