﻿-- (c) 2010, all rights reserved.

local function myLootButton_OnClick( self )
	-- manual looting
	if lootbop and LootSlotIsItem( self.slot ) then
		--print( string.format( "manual loot/confirm slot %s", self.slot ) )
		ConfirmLootSlot( self.slot )
	end
end

local function eventHandler( self, event, ... )
	
	if GetNumRaidMembers( ) > 0 or GetNumPartyMembers( ) > 0 then
		
		if lootbop then
			UIParent:RegisterEvent( "LOOT_BIND_CONFIRM" )
		end
		lootbop = false
	else
		if not lootbop then
			UIParent:UnregisterEvent( "LOOT_BIND_CONFIRM" )
		end
		lootbop = true
	end
	
	if event == "LOOT_OPENED" then
		
		local lootauto = ...
		
		if lootauto == 1 then
			-- auto looting
			for slot = 1, GetNumLootItems( ) do
				LootSlot( slot )
				if LootSlotIsItem( slot ) then
					--print( string.format( "auto loot/confirm slot %s", slot ) )
					ConfirmLootSlot( slot )
				end
			end
		end
		
	end
	
end

UIParent:UnregisterEvent( "LOOT_BIND_CONFIRM" )
local lootbop = true

local frame = CreateFrame( "FRAME", "ArkAutoLootBOPFrame" )
frame:RegisterEvent( "LOOT_OPENED" )
frame:RegisterEvent( "PARTY_MEMBERS_CHANGED" )
frame:SetScript( "OnEvent", eventHandler )

hooksecurefunc( "LootButton_OnClick", myLootButton_OnClick )
