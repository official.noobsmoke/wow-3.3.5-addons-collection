﻿## Interface: 30300
## X-Curse-Packaged-Version: 1.00
## X-Curse-Project-Name: ArkAutoLootBoP
## X-Curse-Project-ID: arkautolootbop
## X-Curse-Repository-ID: wow/arkautolootbop/mainline

## Title: ArkAutoLootBoP
## Notes: Allows you to loot BoP items without prompting when not in a group or raid
## Author: Arkayenro
## Version: 1.00
## DefaultState: Enabled
## LoadOnDemand: 0

## LoadManagers: AddonLoader
## X-LoadOn-Always:delayed

ArkAutoLootBoP.lua
