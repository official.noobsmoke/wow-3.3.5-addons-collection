Readme for Useful Extras, a World of Warcraft Mod.
----
Version 0.2.6
Useful Extras by EVmaker

Mounting features are now translated to also work for ruRU localizations, thanks to Chiffa TheFox. 

Useful Extras is a small little mod to give the user a number of helpful options availible to them, it takes almost no memory
or processor use.  The exact list of features it adds are listed below, but to summarize; There is a basic calculator, color
picker that is mainly helpful to developers or anyone messing with WoW text colors in-game, and a couple auto options similar
to what TwoBox used to do but with more options.

GUI Menu: Command is /ue menu
Displays all the different slash command options (except the calculator) in an easier to manage GUI menu.

Minimap Button: Left click opens the Options menu, Right click reloads the UI, left-click and drag to move it around.

Rightclick Choosing: Command is /ue rightclick reloadui/updatemounts/colorpicker
Lets you choose what you want right clicking the minimap button (or LDB display) to do from the availible options, which
are currently: reloadui (self explanatory), updatemounts (update your mount list with your currently availible mounts), or
colorpicker (opens the color picker).

Calculator: Command is /ue calc expression
The calculator is a basic one, and can handle most basic arithemetic via slash command, such as /ue calc (956-82)/6.

Exp Report: Command is /ue exp, whisper command is /whisper name exp
Prints out a basic experience report to get a quick idea of your experience status, or if received in a whisper by a friend,
guildmate or leader list person, sends the same basic printout to them.

ColorPicker: Commands are /ue pickcolor, /ue getcolor, /ue getwowcolor, /ue setcolor r,g,b and /ue test chat
pickcolor opens the in-game color picker and lets you choose a color you like. getcolor returns the colors normal RGB values.
getwowcolor returns WoW's rgb values for that color (what you'd need to set in code for that color such as for a chat message).
setcolor lets you set a normal RGB value that you can use /ue test chat to see what it would look like in WoW such as
/ue setcolor 255,145,37.  And finally, /ue test chat will display the color picked with pickcolor, and any value set by
setcolor in chat so you can tell what they will look like (the color you thought you picked isn't always what you get in WoW).

Using the recently added change to the mounting up function, you can make a global macro with /script UE_pickMount("smart")
and it will automatically mount your best availible mount depending on what you have, your location & if you have cold weather
flight for northrend.  You can then put the global macro button on the actionbar of any character that has UE enabled and click
it to smartly mount the best mount for the location regardless of what mounts each char may or may not have.

Preferred Epic Ground/Flying mount feature (thanks to Chiffa TheFox):  Commands are /ue setflyer, /ue setground, /ue clearpreferred
You can use /ue list mounts to get the exact name of all your mounts, and then use /ue setflyer, or /ue setground with the name
of one of your epic flyers or ground mounts respectively, to set that as your "preferred" mount which will be mounted when you
use the mounting whispers (or macros, such as /script UE_pickMount("ground") or /script UE_pickMount("smart") etc..).  This
lets you always mount your "Black War Mammoth" or your "*insert-color-here* Drake" etc.. instead of whatever is the first on the
list.

New Mounting Macros: Commands are /ue createmacros local, or /ue createmacros global
Will create three mounting macros either in the global, or character-specific sections for the three mounting functions,
such as one for best ground mount, one for best air, and one for "smart mounting" which can then easily be put on the actionbar.
In the case of making them global, you could then have them availible to any character on the account to use the same macro
and mount up regardless of what mounts they may or may not have.

Auto Options: See the in-game slash command for the specifics
There are a number of auto options availible each with toggles for turning them on and off, Auto Follow, Auto Mount,
Auto-accepting Invites, Summons or Resses, turning UE on or off, Allowing Friends and/or Guild, Auto-accepting Quests, Sticky
Following, Whisper Relaying and Low Health/Mana warnings, and pass on loot rolls.

Basically, you can have someone on your Leader list (see below) or your friends list (if allowing friends) to use whisper you,
or you whisper them if they use the mod, to follow you, or mount, and if you/they are on the respective Leader, or friends
list (if enabled) will do so.  Can specify ground, air (or land/flying) and the mod will automatically select the first epic
of that type availible and if none are then the regular version.  This is useful for if you or others have to go afk and you
can still have them coming along, or if you are using multiple accounts (twoboxing as it were).  Recently added is 'mountup'
where you will automatically mount the best mount availible for the location (and depending whether you have cold weather
flight or not)

Also toggleable is whether you wish to auto-accept group invites, summons, or resses from people on the leader or your friends
list, as well as if you wish people on your friends list to be able to use the whisper commands (/ue toggle usefriends).
* There is now a toggle for allowing guildmates to do the same as the above as well (off by default)

Leader List: Commands are /ue add name, /ue remove name, /ue list leaders, and /ue leader name
Allows you to set a person or number of people as 'leaders' that can use any of the whisper commands, remove them, or list who
is set.  /ue leader sets the current leader, which is used for 'whisper' messages (whisper relaying, health/mana warnings, etc..)

Functioning:
*	Toggles for: Autoaccept Invite, Summon, Ress, Autofollow, Automount, UE on or off, Allow friends or Guild lists to use
	commands, Auto-accept Quests, Auto-complete Quests (see disclaimer), Sticky Follow (follow after exiting combat)
	Whisper Relaying, Low Health/Mana Warning, and Return/Delete Icons for the Mailbox.
*	New toggles for Auto-declining Duel requests, or Guild Invites/Guild Charter requests, as well as Auto-releasing on death
	in a battleground.
*	The before said features the toggles are for (that always work for those manually added to the Leader list, and work for
	those on the friends list if the use friends toggle is on).
*	Basic GUI, can be accessed with /ue menu
*	Minimap button to open the menu, or reload the UI
*	LDB(Broker) Support, minimap button functionality but on Titan Panel, Fubar, or other LDB compatible displayer.
*	Exp Report with /ue exp, or whisper command with /whisper name exp
*	In-Game Basic Calculator (/ue calc (9*9)/3)
*	ID command, to get a moused over items itemLink, item id, and stack size
*	Ability to use the In-Game Color picker to get RGB values of any color, or the 'real' value used by WoW for its chat colors,
	Useful for developers to get the wow color values for using in chat messages.  Can use /ue test chat to see what the current
	selected color will look like in a chat message, or /ue setcolor to set a standard R,G,B value (/ue setcolor 255,124,56) for
	seeing what it'd look like in a chat message with /ue test chat.  If using /ue getwowcolor, only the first two digits after
	the period are really necessary (0.04, 0.48), or simply get the RGB values and put (rgbvalue/255) in your code.
	*Note: Also useful for a number of mods that may allow you to enter RGB values for things (such as the next version of XPGain
	which will let you set what color you want for the different experience percentages)

*
	Disclaimer for Auto-complete Quests:
	Currently, it will auto-complete quests that you select (that are completable, hence are done and ready to be turned in), since
	Blizzard apparently either protected or changed the QUEST_GREETING event, it appears to no longer be possible to do checks once
	you open dialog with a quest giver, so the next event that works only works when you select a quest yourself.  I know this
	really only saves two button clicks (clicking on continue for quests that have it, and/or the complete button itself) but it's
	there for those that want it.
	
	Also, there are questions on how to handle reward choices, so the above feature currently only works on quests with only money
	rewards, if you'd like to give your opinion on how to handle reward choices, then please post your thoughts on the feature
	request for this on the portal at http://www.wowinterface.com/portal.php?id=421.
*	

Whisper Commands:
--
(All the following are automatically accepted for people you've added to your leader list, otherwise depending on whether you
have allow friends, or allow guild to use whisper commands enabled)
--
followme (follows the whisperer)
mountground, mountair, mountland, mountflying, mountflyer, or mountup (basically just different ways to sound mount your best
ground or flying mount.  mountup smartly mounts the best availible mount based on what you have, location & if you have cold
weather flying in northrend)
leavyparty (leaves current party)
dismount (dismounts, if not flying at the time)
exp (sends your experience information, such as: Level 20 Priest, 29.4% of the way to 21, with 14684XP to go.)
	
Also Functioning: Working Slash command system
----
Todos:

In-Line Todos:
	* Improve the Basic GUI

Future Todo:
	* Buff timeout warning toggle

Eternal Todos:
	* Eliminate any bugs (if any)
----------------------------------------------------------------------
Changelog:
0.2.6:
Added option for setting the right-click of the minimap/ldb button to mount the best useable mount (smart mounting)
via /ue rightclick mount.  Useful for a quick mounting without using macros, without switching actionbars, or on
small computers (like the netbook I use).

0.2.5:
Added the Celestial Steed and X-53 Touring Rocket to the mount list, again if you don't care about those mounts then
no need to update.

0.2.4:
Just adding the Frosty Flying Carpet to the mount list, if you don't care about that mount then no need to update.

0.2.3:
3.3 Compatibility and TOC Update.  Also updated to include all (believe anyway) currently availible mounts (including
invincible and big love rocket, etc..).

0.2.2:
Added "WhisperLoot" toggle (off by default) that those on the leader list (and leader list only) may whisper
passloot, greedloot or needloot, and if WhisperLoot is enabled then all the currently being rolled on equipment
will be passed, greeded or needed; most helpful for multi-boxers or those who's friends have gone afk and such
and need to get them to greed or need.  Also setground/setflyer now can be used to set a regular ground or flyer
as the preferred mount instead of just an epic, and it will say whether the mount set as preferred is an epic or
not (such as Swift Brewfest Ram is now the preferred epic ground mount); can also enter the spell ID if known.

0.2.1:
3.2 Compatibility & TOC update, Added most (believe all, not positive) of the mounts added in 3.1 and 3.2 to the
mount list.  Thanks to the much improved flyable area API, and a bit of code changing; smart mounting is now a
whole lot leaner, does not need to be localized for any client, and no longer needs the ZonesLib (yay for
optimization).  Optimized other parts of the code as well.

0.2:
Added an option to display return/delete icons in the mailbox (a letter for will be returned, or X for will be
deleted under the time left for each mail, so you can tell at a glance what will be returned or deleted).  Also
updated the setting updating function to 'merge' the data rather then the old method, which essentially means that
it will be able to update previous users settings with the new settings without needing to clear their settings, so
no more needing to re-check settings after a new version.  Also added the new "zone" north of the Argent Tournament
in icecream to the zone list, so UE is now 3.2 compatible as well as still being 3.1 compatible.

0.1.9:
Added an auto-complete quests slash toggle (running out of room in the GUI, it will be added there in the GUI rewrite
I promise) which will auto-turnin a quest that you select from a quest giver (assuming it's completed and able to be
turned in of course) see the disclaimer in the readme for more information.  Also added additional functionality
and/or choices for what right-clicking the minimap button (or LDB) will do; with /ue rightclick (again will have a
menu option later) you can decide whether you want right-clicking the minimap button (or ldb display on Titan etc..)
to reload the ui (default), update your mount list, or open the color picker.  If anyone has any suggestions for
further options to be chooseable for it, feel free to leave a comment here or post a feature request on the portal.
Lastly, updated the mountlist to include the new and old versions of the Argent mounts and the hippogryph; however,
the old orc wolf, and the old gnome mechanostrider were not on WoWhead and had to be gotten off thottbot so I am not
positive they are correct, if anyone has either one, please do a /ue list mounts, and /ue list mountnumbers and send
the results to me in a comment on the mod, private message, or email at the email given when you click on the email
link on my site (which can be gotten from clicking on homepage on my WoWI Profile) thank you.

0.1.8q2:
And with the update to the .lua to update the version number, I forgot that it also uploaded my changes to UE where I
removed the colorpicker features to integrate them into EMLib so they can be used by all my mods, and hence UE's EMLib
needed to be updated as well.  This should be the last 'update' for 0.1.8 of UE, really.

0.1.8q:
Quickfix for ZonesLib, apparently somehow the mispelling of sholazar got re-uploaded into the current version, but yet
was still fixed in my copy that I put onto the svn, I'm really confused on how it happened but the mispelling should
most definately be fixed now, my apologies for all this may have effected and thank you xtoq for pointing it out.

0.1.8:
Added pass on loot rolls slash and toggle.  Changed the way auto-accept group invite works, as the method in the prior
version had issues for a user (although I couldn't reproduce it myself), this way shouldn't have any problems.  Also
added a couple ()'s around some ifchecks (particularly auto-accept group invite) so that it doesn't accept the invite
if you have allow friends/guild enabled but auto-accept group invite itself off (darn those parenthesizes).

0.1.7:
Implemented subzone checking, so that you can now smart-mount on Krasus landing in Dalaran, if there are any other
places where this is the case (a certain subzone of a zone that can be flying mounted in) just let me know and it can
easily be added in now.

0.1.6q:
Cosmetic fix, seeing as how 3.1 added a number of new macro icons, anyone using the /ue createmacros command would be
seeing drastically different icons then intended for the UE ground, flying or smart mounting (not that the user is free
to put the icons and names of the macros to whatever they wish, but I am a little bit of a neat freak and like the
icons to start as what they were intended to be, so they are back to what they were).

0.1.6:
3.1 Compatability, which consequently fixes the auto-accept group invite bug.  Added a hide quest whispers slash and
toggle.  Sholazar smart-mount problem due to a mispell also fixed.

0.1.5:
Now has a ruRU translation for the mounting features thanks to Chiffa TheFox, also implemented a "Preferred" epic ground
or flyer mount feature for the mounting features (/ue setflyer, /ue setground), idea and original code thanks to Chiffa.
Added a new slash command to create three macros for the mounting features (/ue createmacros global, or /ue createmacros
local).  Fixed a bug with Sticky Follow introduced in the last version with the 'mute' whispers option.

0.1.4:
Added 'hide follow whispers' toggle in GUI and slash commands, for those times when all the follow whispers are getting
too spammy but you still want to keep the follow options on.  Also thanks to some ideas figured out while working on
XPGain, very improved the variable resets (player won't really notice a difference but for me it is much improved).

0.1.3:
Integrated LDB support, can now be put onto Titan Panel, Fubar, or other LDB compatible mods (DockingStation).

0.1.2:
Renamed 'Auto-Decline Guild Invites' to 'Auto-Decline Guild Requests', as it now will automatically decline Guild Charter
requests while enabled.  Added a simple chat message when a guild charter request is denied such as:
<UsefulExtras> Ichiyoru tried to offer you a guild petition.
So you know that someone tried, in case you do wish to accept and can simply turn Auto-Decline guild requests off to do it,
might also add a 'silent' option (so you don't even see that much) if people think it'd be useful.
Lastly UE has finally been updated to use EMLib (see portal FAQ for more info there).

0.1.1q:
(q same as Looter being for quickfix), I apologize for this, there was an error popping up about 'populateBuffs' which is
a function from when I was working on buff timeout warnings, but had taken out to get the last version released, but missed a
call to it which caused the error and also led to the menu not updating the options on login.  Both issues have been fixed now
and everything should be working fine again.  Consequently, if theres a way to get swatter to always show errors when they
happen instead of hiding them over reloads, please let me know (I never knew of the error because swatter would always hide it).

0.1.1b:
Added a minimap button, left clicking it will open the options menu, while right clicking it will reload the ui, left-click and
drag to move it around the minimap.  Also added an exp report feature, do /ue exp to get a basic printout of your experience
status, or receive a whisper with exp (/whisper name exp) by a friend, guildmate or leader list to send the same printout to
them.  I apologize on the buff timeout warning, its taking a bit more effort then I'd thought it would so it'll be in the next
version, which will be released after a basic working bookmod (what is to be a continually updated Librarian/Bookwormish mod).

0.1b:
Jumped the version a bit because of the fairly important feature implemented, that being a basic GUI, where you can more easily
configure all of UE's various options without needing to use the slash commands (/ue menu).  You'll still need to use the slash
command for the calculator and ID features however.  Buff timeout warnings, and possibly a minimap button, will hopefully be in
the next release.

0.08b:
Added Sticky Follow (re-follow after getting out of combat), Whisper Relaying (relay whispers received to the current leader),
Auto-accept Quests (shared, escort quests, and quests from a quest giver), and Low Health/Mana warnings.  Also along with those
added the /ue leader name; slash command to set the current leader which is used for a few options (such as the above whisper relay
and low health/mana warnings).  /ue lowhealth # and /ue lowmana #; are used to set the percent of health or mana to warn at.

0.07b:
Rewrote the mounting function and variables, will now be able to choose the first epic flyer/ground if availible and if not regular
without any problems (now goes by the mounts ID's versus matching names).

0.06b:
Updated the mounting whisper command to take into account the Deathknights ground and flying mounts (as I don't have a level 70
Deathknight as yet, I am fairly sure the latter will work, but if it doesn't please leave a bug report on the forum so I can fix it).
Also added a check for if the person is in a guild before trying to update the guildmembers list (didn't harm anything, but printed
"not in a guild" to chat when receiving a whisper if you were not in a guild).

0.05b:
Added toggles for Auto-declining Duel requests or Guild invites.  Also added a toggle for Auto-releasing on dying in a battleground.

0.04b:
Added allow guild toggle (off by default) that if toggled on will let guildmates use the whisper commands and be considered for
the auto-accepts.  Added dismount and leaveparty whisper commands.  Added the reload/reloadui and show tracking slash commands
to the help list (they were already there, but forgot to add them to it), such as /ue reload (or reloadui) and /ue show tracking
which simply reloads the ui, or shows the minimap tracking button (fiance had problems with it not showing up before due to other
mods she was using and this helped until she found the problem, have encountered the problem with other mods as well).

0.03b:
Added most all of the 'special' flyers and ground mounts (winterspring frostsaber, drakes, headless horseman steed, etc..) as well
as alliance mounts to the 'detect first availible' mount for mountup whisper.  Also added the "id" slash command, which will print
the item link, item id, and stack size of the currently moused over item.

0.02b:
Added the /ue add and /ue remove commands to the slash command list printout, they were already implemented but I forgot to add
them there.  Also added checks to the whispers when beginning to follow someone or if the follow is broken for if the autofollow
toggle is on, and to not whisper if it isn't.

0.01b:
Initial release, included the auto-accept invite, summon, and ress toggles, autofollow and automount, as well as allow friends.
Also included the basic in-game calculator and color picker features.