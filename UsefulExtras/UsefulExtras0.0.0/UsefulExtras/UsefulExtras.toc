## Interface: 30300
## Title: Useful Extras
## Notes: Adds a variety of useful things, such as auto-accepting group invites, auto-following or mounting, etc..
## Author: EVmaker
## Dependencies: EMLib
## SavedVariablesPerCharacter: UE_LeaderList, UE_Settings, UEMiniMap_Settings
## X-Embeds: CallBackHandler, LibStub, LibDataBroker
Libs\LibStub.lua
Libs\CallbackHandler-1.0.lua
Libs\LibDataBroker-1.1.lua
UsefulExtrasLDB.xml
UsefulExtras.xml
UEMiniMap.xml
MountsLib.lua