-- LibDataBroker Support for Useful Extras
assert(LibStub, "UELDB requires LibStub")
assert(LibStub:GetLibrary("LibDataBroker-1.1"), "UELDB requires LibDataBroker-1.1")

local brokertext  = "UsefulExtras";
local ldb = LibStub:GetLibrary("LibDataBroker-1.1")
local uetext = "Left click to open the menu. |nRight click to reload ui, update the mount list, or open the color picker.";

function UELDB_OnLoad()
	UEbroker = ldb:NewDataObject(brokertext, {
		type = "launcher",
		label = brokertext,
		icon = "Interface\\Icons\\Inv_Misc_Enggizmos_Swissarmy",
		OnClick = function(self, button)
			if button == "LeftButton" then
				EMLFrame_Toggle("UE1")
			elseif button == "RightButton" then
				UE_rightClick()
			end
		end,
		OnEnter = function(self)
			GameTooltip:SetOwner(self, "ANCHOR_CURSOR")
			GameTooltip:ClearLines()
			GameTooltip:AddLine(brokertext,0,1,0)
			GameTooltip:AddLine("------------------------------------",0.8,0.8,1)
			GameTooltip:AddLine(uetext,1,1,1)
			GameTooltip:Show()
		end,
		OnLeave = function()
			GameTooltip:Hide()
		end,
	})
end
-- End of code