--[[
Lua for the minimap button of Useful Extras
]]--

-- Declare minimap saved variable
UEMiniMap_Settings = {
	["MinimapPos"] = -20
}

-- Declare minimap defaults
local UEMiniMap_defaultSettings = {
	["MinimapPos"] = -20
}

function UE_MinimapButton_Reposition()
	UE_MinimapButton:SetPoint("TOPLEFT","Minimap","TOPLEFT",52-(80*cos(UEMiniMap_Settings["MinimapPos"])),(80*sin(UEMiniMap_Settings["MinimapPos"]))-52)
end


function UE_MinimapButton_DraggingFrame_OnUpdate()

	local xpos,ypos = GetCursorPosition()
	local xmin,ymin = Minimap:GetLeft(), Minimap:GetBottom()

	xpos = xmin-xpos/UIParent:GetScale()+70 -- get coordinates as differences from the center of the minimap
	ypos = ypos/UIParent:GetScale()-ymin-70

	UEMiniMap_Settings["MinimapPos"] = math.deg(math.atan2(ypos,xpos)) -- save the degrees we are relative to the minimap center
	UE_MinimapButton_Reposition() -- move the button
end


 function UEMiniMap_OnLoad()
	this:RegisterEvent("VARIABLES_LOADED"); 
 end
 

 function UEMiniMap_OnEvent()
	local text;
	-- VARIABLES_LOADED event
	if ( event == "VARIABLES_LOADED" ) then
		-- execute event code in this function
		UE_MinimapButton_Reposition()
	end
 end
 
UE_Minimap_Button_OnEnter = function()
	if ( GameTooltip.lmmbfinished ) then
		return;
	end
	local text;
	GameTooltip.lmmbfinished = 1;
	GameTooltip:SetOwner(UE_MinimapButton, "ANCHOR_TOP");
	if (UE_Settings["RightClick"] == "reload") then
		text = "UsefulExtras|n______ |n|nLeft click to open the menu. |nRight click to reload ui. |nClick and drag to move Minimap Button.";
	elseif (UE_Settings["RightClick"] == "mounts") then
		text = "UsefulExtras|n______ |n|nLeft click to open the menu. |nRight click to update your mount list. |nClick and drag to move the Minimap Button.";
	elseif (UE_Settings["RightClick"] == "colorpicker") then
		text = "UsefulExtras|n______ |n|nLeft click to open the menu. |nRight click to open the color picker. |nClick and drag to move the Minimap Button.";
	else
		text = "UsefulExtras|n______ |n|nLeft click to open the menu. |nClick and drag to move the Minimap Button.";
	end
	GameTooltip:AddLine(text,.8,.8,.8,1);
	GameTooltip:Show();
end

UE_Minimap_Button_OnLeave = function()
   GameTooltip:Hide();
   GameTooltip.lmmbfinished = nil;
end


function UE_MinimapButton_OnClick(button)
	if ( button == "RightButton" ) then
		UE_rightClick()
	else
		EMLFrame_Toggle("UE1")
	end
end
