--[[
Version 0.2.6
Useful Extras by EVmaker
Functioning:
*	Toggles for: Autoaccept Invite, Summon, Ress, Autofollow, Automount, Whisper Relaying, UE on or off, Allow friends
	or Guild lists to use commands, Auto-accept Quests, Auto-complete Quests, Sticky Follow (follow after exiting combat),
	Whisper Relaying, Low Health/Mana Warning, and Return/Delete icons for the Mailbox.
*	The before said features the toggles are for (that always work for those manually added to the Leader list, and work for
	those on the friends list if the use friends toggle is on).
*	Basic GUI, can be accessed with /ue menu
*	Minimap button to open the menu, or reload the UI
*	Exp Report with /ue exp, or whisper command with /whisper name exp
*	In-Game Basic Calculator (/ue calc 1,+,1 ; 9,*,5 ; 8,/,4 ; 12,-,3)
*	Ability to use the In-Game Color picker to get RGB values of any color, or the 'real' value used by WoW for its chat colors
	via /ue pickcolor, getcolor, or getwowcolor.  Useful for developers to get the wow color values for using in chat messages.
	Can also use /ue test chat to see what the current selected color will look like in a chat message, or /ue setcolor to set a
	standard R,G,B value (/ue setcolor 255,124,56) for seeing what it'd look like in a chat message with /ue test chat.
	(I would heavily recommend using a chat mod which allows copying from the chat window if you are using /ue getwowcolor
	as the values returned are rather long) or simply get the RGB values and put (rgbvalue/255) in your code.

Also Functioning: Working Slash command system, and basic GUI

Complete by end of Beta: eliminate all bugs (if any)

Future Todo: (most everything will be toggeable to be enabled or disabled as you prefer)
* Improve the Basic GUI
* Titan/Fubar Compatibility
]]--

-- Declare the global settings
UE_timerTask = "loot";
UE_LeaderList = {
	[1] = "StartDontRemove"
}
UE_Settings = {
["Version"] = "0.2.6",
["OnOff"] = true,
["UseMMB"] = true,
["RightClick"] = "reload",
["NowLeader"] = false,
["AutoInvite"] = true,
["AutoRess"] = true,
["AutoSummon"] = true,
["AutoRelease"] = true,
["AutoFollow"] = true,
["AutoMount"] = true,
["AutoQuest"] = false,
["AutoComplete"] = false,
["FlyerPref"] = {false,false},
["GroundPref"] = {false,false},
["BuffWarning"] = false,
["PassLoot"] = false,
["LowWarning"] = false,
["LowHealth"] = 0,
["LowMana"] = 0,
["RessTime"] = 0,
["MailIcons"] = true,
["StickyFollow"] = false,
["WhisperRelay"] = false,
["HideWhispers"] = false,
["HideQWhispers"] = false,
["WhisperLoot"] = true,
["UseFriends"] = false,
["UseGuild"] = false,
["DeclineDuel"] = false,
["DeclineGuild"] = false
}

-- Declare the local settings
-- (Moved the actual mount lists to another file)
local UE_MountList = {
	[1] = "StartDontRemove"
}
local UE_NumMountList = {
	[1] = "StartDontRemove"
}
local UE_FriendsList = {
	[1] = "StartDontRemove"
}
local UE_GuildList = {
	[1] = "StartDontRemove"
}
local UE_PartyList = {
	[1] = "StartDontRemove"
}
local UE_ColorValues = {6,135,88}
local UE_updateInterval = 1.0;

-- Declare the local defaults
local UE_EML = "UsefulExtras";
local debugMode = false;
local lastFollow = nil;
local isFollowing = false;
local isBroken = false;
local lowHealth = 0;
local lowMana = 0;
local atLowHP = false;
local atLowMana = false;
local acceptedInvite = false;
local currentRolls = {};
local currentTime = 0;
local highestTime = 0;
--local buffCount = 0;
--local buffTime = {};
--local buffList = {};
local UE_defaultSettings = {
["Version"] = "0.2.6",
["OnOff"] = true,
["UseMMB"] = true,
["RightClick"] = "reload",
["NowLeader"] = false,
["AutoInvite"] = true,
["AutoRess"] = true,
["AutoSummon"] = true,
["AutoRelease"] = true,
["AutoFollow"] = true,
["AutoMount"] = true,
["AutoQuest"] = false,
["AutoComplete"] = false,
["FlyerPref"] = {false,false},
["GroundPref"] = {false,false},
["BuffWarning"] = false,
["PassLoot"] = false,
["LowWarning"] = false,
["LowHealth"] = 0,
["LowMana"] = 0,
["RessTime"] = 0,
["MailIcons"] = true,
["StickyFollow"] = false,
["WhisperRelay"] = false,
["HideWhispers"] = false,
["HideQWhispers"] = false,
["WhisperLoot"] = true,
["UseFriends"] = false,
["UseGuild"] = false,
["DeclineDuel"] = false,
["DeclineGuild"] = false
}
local UE_defaultColors = {6,135,88}
local UE_defaultFlyer = {
	["name"] = nil,
	["id"] = nil,
}
local UE_defaultGround = {
	["name"] = nil,
	["id"] = nil,
}

-- UE slash handling
function UE_SlashHandler(theMsg)
	msg = strtrim(strlower(theMsg or ""))
	if msg == "version" then
		EMLChat("This version of Useful Extras is: "..UE_defaultSettings["Version"],"chat",UE_EML)
	elseif strmatch(msg, "rightclick") then
		local theChoice = strmatch(msg, "rightclick (.*)");
		if (theChoice == "reloadui") then
			UE_Settings["RightClick"] = "reload";
			EMLChat("Right clicking the minimap button will now reload the UI.","chat",UE_EML)
		elseif (theChoice == "updatemounts") then
			UE_Settings["RightClick"] = "mounts";
			EMLChat("Right clicking the minimap button will now update your mount list.","chat",UE_EML)
		elseif (theChoice == "colorpicker") then
			UE_Settings["RightClick"] = "colorpicker";
			EMLChat("Right clicking the minimap button will now open the color picker.","chat",UE_EML)
		elseif (theChoice == "mount") then
			UE_Settings["RightClick"] = "mount";
			EMLChat("Right clicking the minimap button will now mount the best useable mount.","chat",UE_EML)
		else
			EMLChat("Current options for right-clicking the minimap button are: reloadui, mount, updatemounts, and colorpicker","chat",UE_EML)
			EMLChat("which reload the ui, mount best useable mount, update your mount list, or opens the color picker respectively.","chat",UE_EML)
		end
	elseif msg == "menu" then
		EMLFrame_Toggle("UE1")
	elseif msg == "exp" then
		UE_checkExp("report")
	elseif msg == "pickcolor" then
		EML_colorPicker("open")
	elseif msg == "getcolor" then
		EML_colorPicker("get",UE_EML)
	elseif msg == "getwowcolor" then
		EML_colorPicker("getreal",UE_EML)
	elseif msg == "resetcolor" then
		EML_colorPicker("reset")
		local defaultColors = EML_copyTable(UE_defaultColors);
		EML_clearTable(UE_ColorValues)
		UE_ColorValues = defaultColors;
	elseif msg == "clearchat" then
		UE_clearChat()
	elseif msg == "clearpreferred" then
		local localSettings = EML_copyTable(UE_defaultSettings);
		UE_Settings["FlyerPref"] = localSettings["FlyerPref"];
		UE_Settings["GroundPref"] = localSettings["GroundPref"];
	elseif strmatch(msg, "setcolor") then
		local theColor = strmatch(msg, "setcolor (.*)")
		if (theColor ~= nil) then
			local tempR,tempG,tempB = strmatch(msg, "setcolor (.*),(.*),(.*)")
			local theR = tonumber(tempR);
			local theG = tonumber(tempG);
			local theB = tonumber(tempB);
			UE_ColorValues = {theR,theG,theB};
			if (theR ~= nil and theG ~= nil and theB ~= nil) then
				EMLChat("Set colors as R:"..theR..", G:"..theG..", B:"..theB,"chat",UE_EML,UE_ColorValues)
			end
		else
			EMLChat("Must enter RGB value, such as 121,13,68 for Red, Green, Blue values","chat",UE_EML)
		end
	elseif strmatch(msg, "lowhealth") then
		local theHealth = strmatch(msg, "lowhealth (.*)")
		if (theHealth ~= nil) then
			local numHealth = tonumber(theHealth);
			UE_Settings["LowHealth"] = numHealth
			EMLChat("Low health warning set to: "..theHealth.."%","chat",UE_EML)
			UE_setCheckBoxes()
		else
			EMLChat("Must give a number percentage to warn at","chat",UE_EML)
		end
	elseif strmatch(msg, "lowmana") then
		local theMana = strmatch(msg, "lowmana (.*)")
		if (theMana ~= nil) then
			local numMana = tonumber(theMana);
			UE_Settings["LowMana"] = numMana
			EMLChat("Low mana warning set to: "..theMana.."%","chat",UE_EML)
			UE_setCheckBoxes()
		else
			EMLChat("Must give a number percentage to warn at","chat",UE_EML)
		end
	elseif msg == "debug" then
		if (debugMode) then
			debugMode = false;
			EMLChat("Debug mode toggled off.","chat",UE_EML)
		else
			debugMode = true;
			EMLChat("Debug mode toggled on.","chat",UE_EML)
		end
	elseif strmatch(msg, "createmacros") then
		local whereTo = strmatch(msg, "createmacros (.*)");
		if (whereTo == "local") then
			whereTo = 1;
			EMLChat("Creating local mounting macros","chat",UE_EML,"teal")
		elseif (whereTo == "global") then
			whereTo = 0;
			EMLChat("Creating global mounting macros","chat",UE_EML,"teal")
		else
			whereTo = 1;
			EMLChat("Didn't specify to make the mounting macros local or global, so making them local.","chat",UE_EML)
		end
		CreateMacro("MountGroundUE",257,'/script UE_pickMount("ground")',whereTo,1)
		CreateMacro("MountFlyerUE",249,'/script UE_pickMount("flyer")',whereTo,1)
		CreateMacro("SmartMountUE",239,'/script UE_pickMount("smart")',whereTo,1)
	elseif strmatch(msg, "test1") then
		local theOption = strmatch(theMsg, "test1 (.*)");
		UE_testFunction(theOption)
	elseif strmatch(msg, "test2") then
		local theOption = strmatch(theMsg, "test2 (.*)");
		UE_testFunction2(theOption)
	elseif msg == "test chat" then
		local currentColor = EML_colorPicker();
		EMLChat("Test with current color picker color","chat",UE_EML)
		EMLChat("Hello There.","chat",UE_EML,currentColor)
		EMLChat("Test with current set color values","chat",UE_EML)
		EMLChat("Hello There.","chat",UE_EML,UE_ColorValues)
	elseif msg == "reload" then
		ReloadUI();
	elseif msg == "reloadui" then
		ReloadUI();
	elseif msg == "reset" then
		UE_Reset()
	elseif msg == "show tracking" then
		MiniMapTracking:Show()
	elseif strmatch(msg, "calc") then
		local calcStuff = strmatch(msg, "calc (.*)");
		if (calcStuff ~= nil) then
			UE_calcFunction(calcStuff)
		else
			EMLChat("Must enter data to calculate.","chat",UE_EML)
		end
	elseif strmatch(msg, "setflyer") then
		local theFlyer = strmatch(theMsg, "setflyer (.*)");
		UE_preferredMount(theFlyer,"flyer")
	elseif strmatch(msg, "setground") then
		local theGround = strmatch(theMsg, "setground (.*)");
		UE_preferredMount(theGround,"ground")
	elseif strmatch(msg, "id") then
		local whatID = strmatch(theMsg, "id (.*)");
		if (whatID == nil) then
			EML_toolInfo("info",UE_EML)
		else
			EMLChat("Arg: "..tostring(whatID),"chat",UE_EML)
			local itemArray = EML_getInfo(whatID);
			if (itemArray) then
				EMLChat("Item Link: "..itemArray.Link,"chat",UE_EML)
			end
		end
	elseif strmatch(theMsg, "add") then
		local theName = strmatch(theMsg, "add (.*)");
		local addName = string.lower(tostring(theName));
		if (theName ~= nil) then
			EML_addTable(addName,UE_LeaderList)
			EMLChat("Adding "..addName.." to the Leader list.","chat",UE_EML)
		else
			EMLChat("Must give name of person to add to the Leader list.","chat",UE_EML)
		end
	elseif strmatch(theMsg, "remove") then
		local theName = strmatch(theMsg, "remove (.*)");
		local remName = string.lower(tostring(theName));
		if (theName ~= nil) then
			EML_remTable(remName,UE_LeaderList)
			EMLChat("Removing "..remName.." from the Leader list.","chat",UE_EML)
		else
			EMLChat("Must give name of person to remove from the Leader list.","chat",UE_EML)
		end
	elseif strmatch(msg, "list") then
		local whatList = tostring(strmatch(msg, "list (.*)"))
		if (whatList == "friends") then
			UE_populateFriends()
			EML_Display(UE_FriendsList,UE_EML)
		elseif (whatList == ("leaders" or "leader")) then
			EML_Display(UE_LeaderList,UE_EML)
		elseif (whatList == "mounts") then
			UE_populateMounts()
			local mountCount = GetNumCompanions("MOUNT");
			EML_Display(UE_MountList,UE_EML)
			EMLChat("You have "..tostring(mountCount).." mounts.","chat",UE_EML)
		elseif (whatList == "mountnumbers") then
			EML_Display(UE_NumMountList,UE_EML)
		elseif (whatList == "guild") then
			UE_populateGuild()
			EML_Display(UE_GuildList,UE_EML)
		elseif (whatList == "party") then
			UE_populateParty()
			EML_Display(UE_PartyList,UE_EML)
		elseif (whatList == "preferred") then
			EMLChat("Flyer Preference:","chat",UE_EML,"teal")
			EML_Display(UE_Settings["FlyerPref"],UE_EML)
			EMLChat("Ground Preference:","chat",UE_EML,"teal")
			EML_Display(UE_Settings["GroundPref"],UE_EML)
			EMLChat("(1 is name, 2 is ID, if false then no preferred mount set)","chat",UE_EML,"teal")
		else
			EMLChat("List: friends, leaders, guild, party, preferred, or mounts?","chat",UE_EML)
		end
	elseif strmatch(theMsg, "leader") then
		local theName = strmatch(theMsg, "leader (.*)");
		local ldrName = string.lower(tostring(theName));
		if (theName ~= nil) then
			UE_Settings["NowLeader"] = ldrName;
			EMLChat("Current leader set to: "..theName,"chat",UE_EML)
			UE_setCheckBoxes()
		else
			EMLChat("Must give name of person to make the current leader.","chat",UE_EML)
		end
	elseif msg == "authors" then
		EMLChat("evmaker, the Lua Coder.","chat",UE_EML)
	elseif msg == "evmaker" then
		EMLChat("EVmaker codes the Lua (only) part of the mod.","chat",UE_EML)
	elseif strmatch(msg, "toggle") then
		local theChoice = strmatch(msg, "toggle (.*)");
		if (theChoice ~= nil and theChoice ~= "" and theChoice ~= " ") then
			UE_Toggle(theChoice)
		else
			UE_Toggle("empty")
		end
	else
		EMLChat("Valid Options:","chat",UE_EML)
		EMLChat("Toggles:","chat",UE_EML)
		EMLChat("Most defaults are off, but check /ue toggle status","chat",UE_EML)
		EMLChat("-----------------","chat",UE_EML)
		EMLChat("toggle status, (check what toggles are on or off)","chat",UE_EML)
		EMLChat("toggle ue, (toggle Useful Extras on or off)","chat",UE_EML)
		EMLChat("toggle minimap, (toggles the minimap button on or off)","chat",UE_EML)
		EMLChat("toggle autofollow, (toggle autofollowing on)","chat",UE_EML)
		EMLChat("command: /whisper name followme","chat",UE_EML)
		EMLChat("toggle autoinvite, (toggle auto-accepting invites)","chat",UE_EML)
		EMLChat("toggle autoress, (toggle auto-accepting resses)","chat",UE_EML)
		EMLChat("toggle autosummon, (toggle auto-accepting summons)","chat",UE_EML)
		EMLChat("toggle autorelease, (toggle auto-release on death in battleground)","chat",UE_EML)
		EMLChat("toggle automount, (toggle automounting)","chat",UE_EML)
		EMLChat("command: /whisper name mountground or mountair","chat",UE_EML)
		EMLChat("command: also mountland, mountflying, or mountup (smart deciding)","chat",UE_EML)
		EMLChat("toggle autoquest, (toggle auto-accepting quests)","chat",UE_EML)
		EMLChat("toggle autocomplete, (toggle auto-completing quests)","chat",UE_EML)
		EMLChat("toggle passloot, (toggle passing on loot rolls)","chat",UE_EML)
--		EMLChat("toggle buffwarning, (toggle buff timeout warnings)","chat",UE_EML)
		EMLChat("toggle lowwarning, (toggle low health/mana warnings)","chat",UE_EML)
		EMLChat("command: /ue lowhealth percent# or lowmana percent#","chat",UE_EML)
		EMLChat("toggle mailicons (toggle showing mail return/delete icons)","chat",UE_EML)
		EMLChat("toggle stickyfollow, (toggle re-following after combat)","chat",UE_EML)
		EMLChat("toggle whisperrelay, (toggles relaying of whispers to leader)","chat",UE_EML)
		EMLChat("toggle hidewhispers, (toggles follow whispers on or off)","chat",UE_EML)
		EMLChat("toggle usefriends, (toggle whispers to allow friends list)","chat",UE_EML)
		EMLChat("toggle useguild, (toggle whispers to allow guildmates)","chat",UE_EML)
		EMLChat("toggle declineduels, (toggle auto-declining duel requests)","chat",UE_EML)
		EMLChat("toggle declineguilds, (toggle auto-declining guild invites)","chat",UE_EML)
		EMLChat("-----------------","chat",UE_EML)
		EMLChat("Other Commands:","chat",UE_EML)
		EMLChat("menu, (opens the GUI configuration menu)","chat",UE_EML)
		EMLChat("rightclick, (lets you choose what happens on right clicking the minimap button)","chat",UE_EML)
		EMLChat("exp, (basic experience printout)","chat",UE_EML)
		EMLChat("setflyer, setground (sets preferred flyer or ground mount)","chat",UE_EML)
		EMLChat("createmacros local or global","chat",UE_EML)
		EMLChat("(creates macros for ground, flyer and smart mounting that'll work regardless of character)","chat",UE_EML)
		EMLChat("Leave Party, Dismount, Exp whispers","chat",UE_EML)
		EMLChat("command: /whisper name leaveparty, dismount, or exp","chat",UE_EML)
		EMLChat("reload or reloadui, (simply reloads the ui)","chat",UE_EML)
		EMLChat("add, (add someone to your leader list)","chat",UE_EML)
		EMLChat("remove, (remove someone from your leader list)","chat",UE_EML)
		EMLChat("leader, (sets the current leader for whispers)","chat",UE_EML)
		EMLChat("list, (can list your leader, friend or mount lists)","chat",UE_EML)
		EMLChat("pickcolor, (opens the in-game color picker to select a color)","chat",UE_EML)
		EMLChat("getcolor, (gets the RGB value of the picked color)","chat",UE_EML)
		EMLChat("getwowcolor, (gets the value of the color in wow format)","chat",UE_EML)
		EMLChat("setcolor, (sets the color settings to use a specific rgb value)","chat",UE_EML)
		EMLChat("test chat, (shows what the picked color, and color settings value look like in chat)","chat",UE_EML)
		EMLChat("calc, (basic in-game calculator)","chat",UE_EML)
		EMLChat("id, (prints the item link, id and stack size of the currently moused over item)","chat",UE_EML)
		EMLChat("version, (check your Useful Extras version)","chat",UE_EML)
		EMLChat("reset, (Reset everything back to default)","chat",UE_EML)
		EMLChat("show tracking, (Shows the tracking minimap button if it isn't showing up)","chat",UE_EML)
		EMLChat("-----------------","chat",UE_EML)
		EMLChat("Authors, slashes for them as well.","chat",UE_EML)
	end
end

-- Setup
function UsefulExtras_OnLoad()
	ORIG_InboxFrame_Update = InboxFrame_Update;
	InboxFrame_Update = UE_InboxFrame_Update;
	this:RegisterEvent("CHAT_MSG_WHISPER")
	this:RegisterEvent("AUTOFOLLOW_BEGIN")
	this:RegisterEvent("AUTOFOLLOW_END")
	this:RegisterEvent("PARTY_INVITE_REQUEST")
	this:RegisterEvent("RESURRECT_REQUEST")
	this:RegisterEvent("PLAYER_ENTERING_WORLD")
	this:RegisterEvent("CONFIRM_SUMMON")
	this:RegisterEvent("PLAYER_DEAD")
	this:RegisterEvent("GUILD_INVITE_REQUEST")
	this:RegisterEvent("PETITION_SHOW")
	this:RegisterEvent("DUEL_REQUESTED")
	this:RegisterEvent("PLAYER_REGEN_ENABLED")
	this:RegisterEvent("QUEST_ACCEPT_CONFIRM")
	this:RegisterEvent("QUEST_DETAIL")
	this:RegisterEvent("QUEST_COMPLETE")
--	this:RegisterEvent("GOSSIP_SHOW")
	this:RegisterEvent("UNIT_HEALTH")
	this:RegisterEvent("UNIT_MANA")
	this:RegisterEvent("PARTY_MEMBERS_CHANGED")
	this:RegisterEvent("CHAT_MSG_ADDON")
	this:RegisterEvent("START_LOOT_ROLL")
--	this:RegisterEvent("UNIT_AURA")
	SLASH_UE1 = "/usefulextras"
	SLASH_UE2 = "/ue"
	SlashCmdList["UE"] = function(msg)
		UE_SlashHandler(msg);
	end
	EMLChat("Useful Extras "..UE_defaultSettings["Version"].." loaded","plainchat",UE_EML,"teal")
end

-- Handles events
function UsefulExtras_OnEvent(event)
	if (event == "PLAYER_ENTERING_WORLD") then
		-- Setting stuff up
		UE_populateFriends()
		UE_populateMounts()
		UE_populateGuild()
		UE_populateParty()
--		UE_populateBuffs()
		UE_setCheckBoxes()
		if (UE_Settings["Version"] ~= UE_defaultSettings["Version"]) then
			if (debugMode) then EMLChat("Version is different.","chat",UE_EML) end
			UE_Reset("version")
		else
			if (debugMode) then EMLChat("Version is the same.","chat",UE_EML) end
		end
	end
	if (UE_Settings["OnOff"]) then
		if (event == "CHAT_MSG_WHISPER") then
			if (debugMode) then
				EMLChat("Whisper received.","chat",UE_EML)
				EMLChat("Sender: "..arg2..", Message: "..arg1,"chat",UE_EML)
			end
			UE_processWhisper(arg2,arg1)
		end
		if (event == "PARTY_INVITE_REQUEST") then
			if (debugMode) then EMLChat("Party invite request from: "..arg1,"chat",UE_EML) end
			UE_processDialog(arg1,"invite")
		end
		if (event == "RESURRECT_REQUEST") then
			if (debugMode) then EMLChat("Ressurect request from: "..arg1,"chat",UE_EML) end
			UE_processDialog(arg1,"ress")
		end
		if (event == "CONFIRM_SUMMON") then
			if (debugMode) then EMLChat("Summon request from: "..arg1,"chat",UE_EML) end
			UE_processDialog(arg1,"summon")
		end
		if (event == "AUTOFOLLOW_BEGIN") then
			isFollowing=true
			lastFollow=arg1
			if (UE_Settings["AutoFollow"]) and (not UE_Settings["StickyFollow"]) and (not UE_Settings["HideWhispers"]) then SendChatMessage("Following you", "WHISPER", nil, arg1) end
			if (UE_Settings["AutoFollow"]) and (UE_Settings["StickyFollow"]) and (not UE_Settings["HideWhispers"]) then SendChatMessage("Sticky Following you", "WHISPER", nil, arg1) end
			if (arg1 ~= nil and debugMode and not UE_Settings["StickyFollow"]) then EMLChat("Following: "..arg1,"chat",UE_EML) end
			if (arg1 ~= nil and debugMode and UE_Settings["StickyFollow"]) then EMLChat("Sticky Following: "..arg1,"chat",UE_EML) end
		end
		if (event == "AUTOFOLLOW_END") then
			isBroken = true
			if (isFollowing) then
				if (UE_Settings["AutoFollow"]) and (not UE_Settings["HideWhispers"]) then SendChatMessage("Follow is broken", "WHISPER", nil, lastFollow) end
				isFollowing = false
			end
		end
		if (event == "PLAYER_DEAD") and (UE_Settings["AutoRelease"]) then
			local inBG = UE_bgStatus();
			if (inBG) then
				RepopMe()
			end
		end
		if (event == "DUEL_REQUESTED") and (UE_Settings["DeclineDuel"]) then
			CancelDuel()
		end
		if (event == "GUILD_INVITE_REQUEST") and (UE_Settings["DeclineGuild"]) then
			DeclineGuild()
			StaticPopup_Hide("GUILD_INVITE")
		end
		if (event == "PETITION_SHOW") and (UE_Settings["DeclineGuild"]) then
			local petitionInfo = UE_petitionInfo();
			if (petitionInfo.petitionType == "guild") then
				ClosePetition()
				EMLChat(petitionInfo.originator.." tried to offer you a "..petitionInfo.petitionType.." petition.","chat",UE_EML,"teal")
			end
		end
		if (event == "PLAYER_REGEN_ENABLED") and (UE_Settings["StickyFollow"]) then
			if (lastFollow ~= nil) and (isBroken) and UE_Settings["AutoFollow"] then
				FollowUnit(lastFollow);
				if (not UE_Settings["HideWhispers"]) then SendChatMessage("Re-Following you", "WHISPER", nil, lastFollow) end
				isBroken = false;
			end
		end
		if (event == "QUEST_DETAIL") then
			if (UE_Settings["AutoQuest"]) then
				if (UE_Settings["NowLeader"] ~= false) then
					if (not UE_Settings["HideQWhispers"]) then SendChatMessage("Accepting Quest", "WHISPER", nil, UE_Settings["NowLeader"]) end
				end
				AcceptQuest()
			end
			if (UE_Settings["AutoComplete"]) then
				CompleteQuest()
			end
		end
		if (event == "QUEST_COMPLETE" and UE_Settings["AutoComplete"]) then
			local questChoices = GetNumQuestChoices();
			if (questChoices and questChoices < 1) then
				GetQuestReward()
				if (debugMode) then EMLChat("Quest Completed.","chat",UE_EML,"green") end
			else
				if (debugMode) then EMLChat("Quest Rejected.","chat",UE_EML,"red") end
			end
		end
--[[
		if (event == "GOSSIP_SHOW") then
			if (debugMode) then EMLChat("Gossip open","chat",UE_EML,"teal") end
			local testArray = {};
			for i=0, 25 do
				SelectGossipActiveQuest(i)
				local isCompleteable = IsQuestCompletable();
				if (isCompleteable) then
					if (debugMode) then EMLChat("Turninable.","chat",UE_EML,"green") end
				else
					if (debugMode) then EMLChat("Not Turninable.","chat",UE_EML,"red") end
				end
			end
		end
]]--
		if (event == "QUEST_ACCEPT_CONFIRM") and (UE_Settings["AutoQuest"]) then
			if (UE_Settings["NowLeader"] ~= false) then
				if (not UE_Settings["HideQWhispers"]) then SendChatMessage("Accepting Escort Quest", "WHISPER", nil, UE_Settings["NowLeader"]) end
			end
			ConfirmAcceptQuest()
		end
		if (event == "UNIT_HEALTH")  and (UE_Settings["LowWarning"]) then
			if (UE_Settings["LowHealth"] ~= 0) and (arg1 == "player") then
				if (atLowHP) then
					if ((UnitHealth("player")/UnitHealthMax("player")*100) > UE_Settings["LowHealth"]) then
						atLowHP = false;
					end
				else
					if ((UnitHealth("player")/UnitHealthMax("player")*100) < UE_Settings["LowHealth"]) then
						atLowHP = true;
						if (UE_Settings["NowLeader"] ~= false) then SendChatMessage("Need Healing!", "WHISPER", nil, UE_Settings["NowLeader"]) end
					end
				end
			end
		end
		if (event == "UNIT_MANA") and (UE_Settings["LowWarning"]) then
			if (UE_Settings["LowMana"] ~= 0) and (arg1 == "player") then
				if (atLowMana) then
					if ((UnitMana("player")/UnitManaMax("player")*100) > UE_Settings["LowMana"]) then
						atLowMana = false;
					end
				else
					if ((UnitMana("player")/UnitManaMax("player")*100) < UE_Settings["LowMana"]) then
						atLowMana = true;
						if (UE_Settings["NowLeader"] ~= false) then SendChatMessage("Low on Mana!", "WHISPER", nil, UE_Settings["NowLeader"]) end
					end
				end
			end
		end
		if (event == "START_LOOT_ROLL") and (UE_Settings["PassLoot"]) then
			RollOnLoot(arg1,0)
			EMLChat("Passing On.","chat",UE_EML)
		elseif (event == "START_LOOT_ROLL") and (not UE_Settings["PassLoot"]) and (UE_Settings["WhisperLoot"]) then
			local tempVar = (arg2/1000);
			local tempArray = {arg1,tempVar};
			EML_addTable(tempArray,currentRolls)
			currentTime = 0;
			for index,value in pairs(currentRolls) do
				local _, rollTime = EML_splitTwo(value);
				if (rollTime > highestTime) then
					highestTime = rollTime;
				end
			end
			UE_timerTask = "loot";
			UE_Timer:Show()
			if (debugMode) then EML_Display(currentRolls,UE_EML) end
		end
--[[
		if (event == "UNIT_AURA") and (arg1 == "player") and (UE_Settings["BuffWarning"]) then
			UE_populateBuffs()
		end
]]--
		if (event == "PARTY_MEMBERS_CHANGED") and (acceptedInvite) then
			acceptedInvite = false;
			StaticPopup_Hide("PARTY_INVITE")
			UE_populateParty()
		elseif (event == "PARTY_MEMBERS_CHANGED") and (not acceptedInvite) then
			UE_populateParty()
		end
	else
		-- Do nothing
	end
end

-- Resets Useful Extras
function UE_Reset(arg)
	local localSettings = EML_copyTable(UE_defaultSettings);
	local localColors = EML_copyTable(UE_defaultColors);
	UE_Settings = EML_mergeTable(UE_Settings,localSettings);
	UE_populateFriends()
	UE_populateGuild()
	UE_populateMounts()
	if (arg == "version") then
		UE_Settings["Version"]=localSettings["Version"];
		EMLChat("Version is changed, updating settings.","chat",UE_EML)
	elseif (arg == "ress") then
		UE_Settings["RessTime"] = 0;
		UE_timerTask = false;
	else
		EMLChat("Resetting settings","chat",UE_EML)
		EML_clearTable(UE_Settings)
		UE_Settings=localSettings;
		EML_clearTable(UE_ColorValues)
		UE_ColorValues=localColors;
		EML_clearTable(UE_LeaderList)
	end
	UE_setCheckBoxes()
end

-- Gets the time
function UE_getTime()
	local theTime=tonumber(floor(GetTime()))
	return theTime;
end

-- Performs calculator functions
function UE_calcFunction(arg)
	local theResult = nil;
	local tempVar = "UEExp";
	setglobal(tempVar, nil);
	RunScript(tempVar.."=("..arg..")");
	theResult = getglobal(tempVar);
	if (theResult ~= nil) then
		EMLChat("The result is: "..theResult,"chat",UE_EML)
	end
	return theResult;
end

function UE_clearChat()
	DEFAULT_CHAT_FRAME:Clear()
end

-- Gets & Handles experience information
function UE_checkExp(where,who)
	local expClass = UnitClass("player");
	local expCurrent = UnitXP("player");
	local expMax = UnitXPMax("player");
	local expPercent = ((floor((expCurrent/expMax)*10000))/100);
	local expLeft = (expMax - expCurrent);
	local expPercentLeft = strsub(((expLeft/expMax)*100),1,5);
	local currentLevel = UnitLevel("player");
	local nextLevel = currentLevel + 1;
	local UE_Msg = "";
	-- Current Level
	UE_Msg = UE_Msg.."Level "..currentLevel.." "..expClass..", "
	-- Percentage of this level completed.
	UE_Msg = UE_Msg..expPercent.."% of the way to "..nextLevel..", with "
	-- Amount of XP till next level.
	UE_Msg = UE_Msg..expLeft.."XP to go. "
	if (where == "report") then
		EMLChat(UE_Msg,"chat",UE_EML)
	elseif (where == "send") and (who ~= nil) then
		SendChatMessage(UE_Msg, "WHISPER", nil, who)
	else
		return expClass,currentLevel,nextLevel,expCurrent,expMax,expLeft,expPercent,expPercent,expPercentLeft;
	end
end

-- Generates random numbers
function UE_randGen(min,max)
	local randNum;
	minNum=tonumber(min)
	maxNum=tonumber(max)
	if (min ~= nil) and (min ~= "") then
		randNum=random(min,max)
	else
		randNum=random(1,100)
	end
	return randNum
end

-- Test whatever the coder is working on
function UE_testFunction(arg)
--[[
	UE_populateMounts()
	UE_populateFriends()
	UE_populateGuild()
]]--
	EML_Display(currentRolls,UE_EML)
end

-- Another function for testing what the coder is working on
function UE_testFunction2(arg)
--	UE_handleEvents(arg)
	EMLChat("Mount Lists:","chat",UE_EML,"teal")
	EML_Display(UE_MountList,UE_EML)
	EML_Display(UE_NumMountList,UE_EML)
	EMLChat("Guild List:","chat",UE_EML,"teal")
	EML_Display(UE_GuildList,UE_EML)
	EMLChat("Friend List:","chat",UE_EML,"teal")
	EML_Display(UE_FriendsList,UE_EML)
	EMLChat("Party List:","chat",UE_EML,"teal")
	EML_Display(UE_PartyList,UE_EML)
end

-- Color picker function has been moved into EMLib

-- Fills the friends list table with the users friends list
function UE_populateFriends()
	EML_clearTable(UE_FriendsList)
	ShowFriends()
	local numFriends = GetNumFriends();
	for i=1, numFriends do
		local friendName, friendLevel, friendClass, friendArea, friendOnline, friendStatus, friendNote = GetFriendInfo(i);
		if (friendName) then
			EML_addTable(string.lower(tostring(friendName)),UE_FriendsList)
		end
	end
end

-- Fills the mounts list table with the users mounts
function UE_populateMounts()
	EML_clearTable(UE_MountList)
	EML_clearTable(UE_NumMountList)
	for i=1, GetNumCompanions("mount") do
		local mountID, mountName, mountSpellID, _, _ = GetCompanionInfo("mount", i);
		if (mountName ~= nil and mountSpellID ~= nil) then
			if (debugMode) then EMLChat("Mount name: "..mountName..", Spell ID: "..mountSpellID,"chat",UE_EML) end
			EML_addTable(mountName,UE_MountList)
			EML_addTable(mountSpellID,UE_NumMountList)
		end
	end
end

-- Fills the guild list with players guildmates
function UE_populateGuild()
	EML_clearTable(UE_GuildList)
	if (IsInGuild()) then
		GuildRoster()
		local numGuild = GetNumGuildMembers(true);
		for i=0, numGuild do
			local memberName, _, _, _, _, _, _, _, _, _ = GetGuildRosterInfo(i);
			if (memberName) then
				EML_addTable(string.lower(tostring(memberName)),UE_GuildList)
			end
		end
		if (debugMode) and (numGuild) then
			EMLChat("Number of guild members: "..tostring(numGuild),"chat",UE_EML)
		end
	else
		if (debugMode) then EMLChat("Not in a guild, so not updating the guild list","chat",UE_EML) end
	end
end

-- Fills the party list with party members
function UE_populateParty()
	EML_clearTable(UE_PartyList)
	for Count = 1,GetNumPartyMembers() do
		local theName = UnitName("party"..Count);
		if (theName) then
			EML_addTable(string.lower(tostring(theName)),UE_PartyList)
		else
			if (debugMode) then EMLChat("Not a name.","chat",UE_EML,"red") end
		end
	end
end

-- Checks whether someone is a friend or a leader
function UE_checkWho(who)
	UE_populateFriends()
	UE_populateGuild()
	UE_populateParty()
	local isFriend = false;
	local isGuild = false;
	local whoIs = string.lower(tostring(who));
	local whoStatus = {
		["allowed"] = false,
		["friend"] = EML_findTable(whoIs,UE_FriendsList),
		["guild"] = EML_findTable(whoIs,UE_GuildList),
		["leader"] = EML_findTable(whoIs,UE_LeaderList),
		["party"] = EML_findTable(whoIs,UE_PartyList),
		["any"] = false,
	}
	if (UE_Settings["UseFriends"]) then
		isFriend = whoStatus.friend;
	end
	if (UE_Settings["UseGuild"]) then
		isGuild = whoStatus.guild;
	end
	if (isFriend or isGuild or whoStatus.leader) then
		whoStatus["allowed"] = true;
	end
	if (whoStatus.friend or whoStatus.guild or whoStatus.leader or whoStatus.party) then
		whoStatus["any"] = true;
	end
	return whoStatus;
end

-- Handles whisper commands
function UE_processWhisper(who,what)
	local whoStatus = UE_checkWho(who);
	if (debugMode) then
		EMLChat("Leader: "..tostring(whoStatus.leader)..", Friend: "..tostring(whoStatus.friend)..", Guild: "..tostring(whoStatus.guild)..", Party: "..tostring(whoStatus.party)..", Allowed: "..tostring(whoStatus.allowed)..", Any: "..tostring(whoStatus.any),"chat",UE_EML)
	end
	if (what == "followme") and (whoStatus.allowed) then
		FollowUnit(who)
	elseif (what == "shoo") and (whoStatus.allowed) then
		FollowUnit("player")
	elseif (what == "leaveparty") and (whoStatus.allowed) then
		LeaveParty()
	elseif (what == "mountland" or what == "mountground") and (whoStatus.allowed) then
		UE_pickMount("land")
	elseif (what == "mountair" or what == "mountflying") and (whoStatus.allowed) then
		UE_pickMount("air")
	elseif (what == "mountup") and (whoStatus.allowed) then
		UE_pickMount("smart")
	elseif (what == "dismount") and (whoStatus.allowed) then
		if (IsMounted()) and (not IsFlying()) then
			Dismount()
		else
			SendChatMessage("I don't want to die!  Not while flying!", "WHISPER", nil, who)
		end
	elseif (what == "exp") and (whoStatus.any) then
		UE_checkExp("send",who)
	elseif (what == "passloot") and (whoStatus.leader) then
		for index,value in pairs(currentRolls) do
			local rollID, _ = EML_splitTwo(value);
			RollOnLoot(rollID,0)
			EML_remTable(index,currentRolls)
		end
		currentTime = 200;
	elseif (what == "greedloot") and (whoStatus.leader) then
		for index,value in pairs(currentRolls) do
			local rollID, _ = EML_splitTwo(value);
			RollOnLoot(rollID,2)
			ConfirmLootRoll(rollID,2)
			EML_remTable(index,currentRolls)
		end
		currentTime = 200;
	elseif (what == "needloot") and (whoStatus.leader) then
		for index,value in pairs(currentRolls) do
			local rollID, _ = EML_splitTwo(value);
			RollOnLoot(rollID,1)
			ConfirmLootRoll(rollID,1)
			EML_remTable(index,currentRolls)
		end
		currentTime = 200;
	else
		if (UE_Settings["WhisperRelay"]) and (UE_Settings["NowLeader"] ~= false) then
			SendChatMessage(who.." whispered: "..what, "WHISPER", nil, UE_Settings["NowLeader"])
		else
			if (UE_Settings["WhisperRelay"]) then
				EMLChat("Must set a current leader with /ue leader for relaying to work.","chat",UE_EML)
			end
		end
	end
end

function UE_preferredMount(what,mountType)
	local theIndex,mountID,mountName;
	local flyerName,flyerID = EML_splitTwo(UE_Settings["FlyerPref"]);
	local groundName,groundID = EML_splitTwo(UE_Settings["GroundPref"]);
	local isID = tonumber(what);
	if (EML_findTable(what,UE_MountList)) then
		theIndex = EML_getTable(what,UE_MountList);
	elseif (EML_findTable(isID,UE_NumMountList)) then
		theIndex = EML_getTable(isID,UE_NumMountList);
	else
		EMLChat("You don't have that mount.","chat",UE_EML)
		return false;
	end
	if (mountType == "flyer") then
		if (flyerName and flyerID) then
			EMLChat("old preferred flyer: "..flyerName,"chat",UE_EML)
		else
			EMLChat("no preferred flyer","chat",UE_EML)
		end
	elseif (mountType == "ground") then
		if (groundName and groundID) then
			EMLChat("old prefered ground: "..groundName,"chat",UE_EML)
		else
			EMLChat("no preferred ground","chat",UE_EML)
		end
	end
	if (theIndex) then
		mountID = EML_getTable(theIndex,UE_NumMountList);
		mountName = EML_getTable(theIndex,UE_MountList);
	end
	if (mountID and mountType == "flyer") then
		if (EML_epicFlyer[mountID]) then
			UE_Settings["FlyerPref"] = {mountName,mountID};
			EMLChat("new preferred epic flyer: "..mountName,"chat",UE_EML)
			return true;
		elseif (EML_regularFlyer[mountID]) then
			UE_Settings["FlyerPref"] = {mountName,mountID};
			EMLChat("new preferred regular flyer: "..mountName,"chat",UE_EML)
			return true;
		else
			EMLChat("Mount given is not a regular or epic flyer that you have!","chat",UE_EML)
		end
	elseif (mountID and mountType == "ground") then
		if (EML_epicGround[mountID]) then
			UE_Settings["GroundPref"] = {mountName,mountID};
			EMLChat("new preferred epic ground: "..mountName,"chat",UE_EML)
			return true;
		elseif (EML_regularGround[mountID]) then
			UE_Settings["GroundPref"] = {mountName,mountID};
			EMLChat("new preferred regular ground: "..mountName,"chat",UE_EML)
			return true;
		else
			EMLChat("Mount given is not a regular or epic ground mount that you have!","chat",UE_EML)
		end
	else
		EMLChat("Error: Notify EV that he hasn't specified ground or flyer for setting preferred mounts somewhere.","chat",UE_EML)
		return nil;
	end
end

-- Picks first availible epic land, or flyer, or if none availible then regular mount
function UE_pickMount(what)
	local isSwimming = IsSwimming();
	local flyerName,flyerID = EML_splitTwo(UE_Settings["FlyerPref"]);
	local groundName,groundID = EML_splitTwo(UE_Settings["GroundPref"]);
	UE_populateMounts()
	if (debugMode and what ~= nil) then
		EMLChat("Preferred Ground:","chat",UE_EML,"teal")
		EML_Display(UE_Settings["GroundPref"],UE_EML)
		EMLChat("Preferred Flyer:","chat",UE_EML,"teal")
		EML_Display(UE_Settings["FlyerPref"],UE_EML)
	end
	if (what == "land" or what == "ground") then
		if (isSwimming) then
			for index,value in pairs(UE_NumMountList) do
				if (EML_waterMounts[value]) then
					if (debugMode) then EMLChat("Water Mount Match","chat",UE_EML) end
					CallCompanion("mount",index)
					return true;
				else
					if (debugMode) then EMLChat("No Water Mount Match","chat",UE_EML) end
				end
			end
		end
		if (groundName) then
			local mountIndex = EML_getTable(groundID,UE_NumMountList);
			if (debugMode) then EMLChat("Preferred Ground Match","chat",UE_EML) end
			CallCompanion("mount",mountIndex)
			return true;
		else
			if (debugMode) then EMLChat("No Preferred Ground","chat",UE_EML) end
		end
		for index,value in pairs(UE_NumMountList) do
			if (EML_epicGround[value]) then
				if (debugMode) then EMLChat("Epic Ground Match","chat",UE_EML) end
				CallCompanion("mount",index)
				return true;
			else
				if (debugMode) then EMLChat("No Epic Ground Match","chat",UE_EML) end
			end 
		end
		for index,value in pairs(UE_NumMountList) do
			if (EML_regularGround[value]) then
				if (debugMode) then EMLChat("Regular Ground Match","chat",UE_EML) end
				CallCompanion("mount",index)
				return true;
			else
				if (debugMode) then EMLChat("No Regular Ground Match","chat",UE_EML) end
			end
		end
		return false;
	end
	if (what == "air" or what == "flying" or what == "flyer") then
		if (flyerName) then
			local mountIndex = EML_getTable(flyerID,UE_NumMountList);
			if (debugMode) then EMLChat("Preferred Epic Flyer Match","chat",UE_EML) end
			CallCompanion("mount",mountIndex)
			return true;
		else
			if (debugMode) then EMLChat("No Preferred Flyer","chat",UE_EML) end
		end
		for index,value in pairs(UE_NumMountList) do
			if (EML_epicFlyer[value]) then
				if (debugMode) then EMLChat("Epic Flyer Match","chat",UE_EML) end
				CallCompanion("mount",index)
				return true;
			else
				if (debugMode) then EMLChat("No Epic Flyer Match","chat",UE_EML) end
			end
		end
		for index,value in pairs(UE_NumMountList) do
			if (EML_regularFlyer[value]) then
				if (debugMode) then EMLChat("Regular Flyer Match","chat",UE_EML) end
				CallCompanion("mount",index)
				return true;
			else
				if (debugMode) then EMLChat("No Regular Flyer Match","chat",UE_EML) end
			end
		end
		return false;
	end
	if (what == "smart") then
		if (isSwimming) then
			UE_pickMount("ground")
			return nil;
		end
		local canFly = UE_canFly();
		if (canFly) then
			local hasFlyer = UE_pickMount("flyer");
			if (not hasFlyer) then UE_pickMount("ground") end
			if (debugMode) then EMLChat("Can Fly section tried.","chat",UE_EML) end
		else
			UE_pickMount("ground")
			if (debugMode) then EMLChat("Can't Fly section tried.","chat",UE_EML) end
		end
	end
end

function UE_canFly()
	local canFly = false;
	SetMapToCurrentZone()
	local flyableArea = IsFlyableArea();
	local playerContinent = GetCurrentMapContinent();
	local spellName = GetSpellInfo(54197);
	local hasColdWeather = IsUsableSpell(spellName);
	if (flyableArea) then
		if (playerContinent == 4 and hasColdWeather) or (playerContinent == 3) then
			canFly = true;
		end
	end
	return canFly;
end

-- Handles 'dialog' windows (summons, invites and resses)
function UE_processDialog(who,what)
	local whoStatus = UE_checkWho(who);
	if (what == "invite" and UE_Settings["AutoInvite"]) and (whoStatus.allowed) then
		acceptedInvite = true;
		AcceptGroup()
	end
	if (what == "ress" and UE_Settings["AutoRess"]) and (whoStatus.any) then
		if (not InCombatLockdown()) and (GetCorpseRecoveryDelay() < 1) then 
			AcceptResurrect()
			EMLChat("Trying to resurrect.","chat",UE_EML)
		elseif (not InCombatLockdown()) and (GetCorpseRecoveryDelay() > 0) then
			local ressTime = GetCorpseRecoveryDelay();
			UE_timerTask = "ress";
			UE_Settings["RessTime"] = GetCorpseRecoveryDelay();
			UE_Timer:Show();
		end
	end
	if (what == "summon" and UE_Settings["AutoSummon"]) and (whoStatus.allowed or whoStatus.party) then
		ConfirmSummon()
	end
end

-- Gets BG Information, currently used to tell if the player is in a battleground.
function UE_bgStatus()
    local bgStatus, mapName, instanceID, minLevel, maxLevel, i;
	local zoneName = tostring(GetZoneText());
    local isInBG = false;
	if (zoneName == "Wintergrasp") then
		isInBG = true;
	end
    for i=1, MAX_BATTLEFIELD_QUEUES do
        bgStatus, mapName, instanceID, minLevel, maxLevel, _ = GetBattlefieldStatus(i);    
        if (bgStatus) then
            if (debugMode) then EMLChat("Status: "..bgStatus,"chat",UE_EML) end
            if (bgStatus == "active") then
                isInBG = true;
            end
        end
        if (mapName) then
            if (debugMode) then EMLChat("Map: "..mapName,"chat",UE_EML) end
        end
        if (instanceID) then
            if (debugMode) then EMLChat("Instance ID: "..instanceID,"chat",UE_EML) end
        end
        if (minLevel) then
            if (debugMode) then EMLChat("Min Level: "..minLevel,"chat",UE_EML) end
        end
        if (maxLevel) then
            if (debugMode) then EMLChat("Max Level: "..maxLevel,"chat",UE_EML) end
        end
    end
    return isInBG;
end

-- Gets the information on an offered petition (guild/arena charters).
function UE_petitionInfo()
	local petitionType, title, bodyText, maxSigs, originator, isOriginator, minSigs = GetPetitionInfo()
	local petitionInfo = {};
	petitionInfo["petitionType"]=petitionType;
	petitionInfo["title"]=title;
	petitionInfo["bodyText"]=bodyText;
	petitionInfo["minSigs"]=minSigs;
	petitionInfo["maxSigs"]=maxSigs;
	petitionInfo["originator"]=originator;
	petitionInfo["isOriginator"]=isOriginator;
	for index,value in pairs(petitionInfo) do
		if (value == nil or value == "" or value == " ") then
			index = nil;
		end
	end
	if (debugMode) then EML_Display(petitionInfo,UE_EML) end
	return petitionInfo;
end
--[[
	Start of the GUI functions
]]--
-- Displays the leader list via GUI
function UE_GUIDisplay(what)
	EML_Display(what,UE_EML)
end

-- Adds or removes someone from the leader list via the GUI
function UE_GUILeader(what)
	local userInput=string.lower(tostring(UE1_InputBox:GetText()));
	if (what == "add") then
		if userInput ~= (nil or "" or " ") then
			EMLChat("Adding "..userInput.." to the Leader List.","chat",UE_EML)
			EML_addTable(userInput,UE_LeaderList)
			UE1_InputBox:SetText("")
		else
			EMLChat("Must enter a name to add to the leader list","chat",UE_EML)
		end
	elseif (what == "remove") then
		if userInput ~= (nil or "" or " ") then
			EMLChat("Removing "..userInput.." from the Leader List.","chat",UE_EML)
			EML_remTable(userInput,UE_LeaderList)
			UE1_InputBox:SetText("")
		else
			EMLChat("Must enter a name to remove from the leader list","chat",UE_EML)
		end
	elseif (what == "clear") then
		EMLChat("Clearing leader list.","chat",UE_EML)
		EML_clearTable(UE_LeaderList)
	end
end

-- Makes the Color Picker GUI buttons work
function UE_GUIColorPicker(what)
	if (what == "open") then
		EML_colorPicker("open",UE_EML)
	elseif (what == "getcolor") then
		EML_colorPicker("get",UE_EML)
	elseif (what == "getwowcolor") then
		EML_colorPicker("getreal",UE_EML)
	else
		if (debugMode) then EMLChat("Something isn't right..","chat",UE_EML) end
	end
end

-- Determine the status of the checkboxes and set things accordingly
function UE_checkCheckBoxes()
	local userInput=string.lower(tostring(UE1_Leader:GetText()));
	local healthInput=tonumber(UE1_LowHealth:GetText());
	local manaInput=tonumber(UE1_LowMana:GetText());
	if (userInput ~= nil) and (userInput ~= "") and (userInput ~= " ") then
		UE_Settings["NowLeader"]=userInput;
		UE1_Leader:SetText(userInput)
	else
		UE_Settings["NowLeader"]=false;
	end
	if (healthInput ~= nil) and (healthInput ~= "") and (healthInput ~= " ") then
		UE_Settings["LowHealth"]=healthInput;
	else
		UE_Settings["LowHealth"]=0;
	end
	if (manaInput ~= nil) and (manaInput ~= "") and (manaInput ~= " ") then
		UE_Settings["LowMana"]=manaInput;
	else
		UE_Settings["LowMana"]=0;
	end
	if (UE1_AutoFollow:GetChecked()) then
		UE_Settings["AutoFollow"]=true;
	else
		UE_Settings["AutoFollow"]=false;
	end
	if (UE1_AutoInvite:GetChecked()) then
		UE_Settings["AutoInvite"]=true;
	else
		UE_Settings["AutoInvite"]=false;
	end
	if (UE1_AutoRess:GetChecked()) then
		UE_Settings["AutoRess"]=true;
	else
		UE_Settings["AutoRess"]=false;
	end
	if (UE1_AutoSummon:GetChecked()) then
		UE_Settings["AutoSummon"]=true;
	else
		UE_Settings["AutoSummon"]=false;
	end
	if (UE1_AutoRelease:GetChecked()) then
		UE_Settings["AutoRelease"]=true;
	else
		UE_Settings["AutoRelease"]=false;
	end
	if (UE1_AutoMounting:GetChecked()) then
		UE_Settings["AutoMount"]=true;
	else
		UE_Settings["AutoMount"]=false;
	end
	if (UE1_AutoQuest:GetChecked()) then
		UE_Settings["AutoQuest"]=true;
	else
		UE_Settings["AutoQuest"]=false;
	end
	if (UE1_StickyFollow:GetChecked()) then
		UE_Settings["StickyFollow"]=true;
	else
		UE_Settings["StickyFollow"]=false;
	end
	if (UE1_WhisperRelay:GetChecked()) then
		UE_Settings["WhisperRelay"]=true;
	else
		UE_Settings["WhisperRelay"]=false;
	end
	if (UE1_HideWhispers:GetChecked()) then
		UE_Settings["HideWhispers"]=true;
	else
		UE_Settings["HideWhispers"]=false;
	end
	if (UE1_HideQWhispers:GetChecked()) then
		UE_Settings["HideQWhispers"]=true;
	else
		UE_Settings["HideQWhispers"]=false;
	end
	if (UE1_PassLoot:GetChecked()) then
		UE_Settings["PassLoot"]=true;
	else
		UE_Settings["PassLoot"]=false;
	end
--[[
	if (UE1_BuffWarning:GetChecked()) then
		UE_Settings["BuffWarning"]=true;
	else
		UE_Settings["BuffWarning"]=false;
	end
]]--
	if (UE1_LowWarning:GetChecked()) then
		UE_Settings["LowWarning"]=true;
	else
		UE_Settings["LowWarning"]=false;
	end
	if (UE1_MailIcons:GetChecked()) then
		UE_Settings["MailIcons"]=true;
	else
		UE_Settings["MailIcons"]=false;
	end
	if (UE1_UseFriends:GetChecked()) then
		UE_Settings["UseFriends"]=true;
	else
		UE_Settings["UseFriends"]=false;
	end
	if (UE1_UseGuild:GetChecked()) then
		UE_Settings["UseGuild"]=true;
	else
		UE_Settings["UseGuild"]=false;
	end
	if (UE1_DeclineDuels:GetChecked()) then
		UE_Settings["DeclineDuel"]=true;
	else
		UE_Settings["DeclineDuel"]=false;
	end
	if (UE1_DeclineGuilds:GetChecked()) then
		UE_Settings["DeclineGuild"]=true;
	else
		UE_Settings["DeclineGuild"]=false;
	end
	if (UE1_OnOff:GetChecked()) then
		UE_Settings["OnOff"]=true;
	else
		UE_Settings["OnOff"]=false;
	end
	if (UE1_HideMMB:GetChecked()) then
		UE_Settings["UseMMB"]=true;
		UE_MinimapButton:Show()
	else
		UE_Settings["UseMMB"]=false;
		UE_MinimapButton:Hide()
	end
end

-- Set the checkboxes on the menu based on variable/toggle status
function UE_setCheckBoxes()
	if (UE_Settings["NowLeader"] ~= false and UE_Settings["NowLeader"] ~= nil) then
		UE1_Leader:SetText(UE_Settings["NowLeader"])
	else
		UE1_Leader:SetText("")
		if (UE_Settings["NowLeader"] == nil) then UE_Reset() end
	end
	if (UE_Settings["LowHealth"] ~= false) then
		UE1_LowHealth:SetText(UE_Settings["LowHealth"])
	else
		UE1_LowHealth:SetText(0)
	end
	if (UE_Settings["LowMana"] ~= false) then
		UE1_LowMana:SetText(UE_Settings["LowMana"])
	else
		UE1_LowMana:SetText(0)
	end
	if (UE_Settings["AutoFollow"]) then
		UE1_AutoFollow:SetChecked(true)
	else
		UE1_AutoFollow:SetChecked(nil)
	end
	if (UE_Settings["AutoInvite"]) then
		UE1_AutoInvite:SetChecked(true)
	else
		UE1_AutoInvite:SetChecked(nil)
	end
	if (UE_Settings["AutoRess"]) then
		UE1_AutoRess:SetChecked(true)
	else
		UE1_AutoRess:SetChecked(nil)
	end
	if (UE_Settings["AutoSummon"]) then
		UE1_AutoSummon:SetChecked(true)
	else
		UE1_AutoSummon:SetChecked(nil)
	end
	if (UE_Settings["AutoRelease"]) then
		UE1_AutoRelease:SetChecked(true)
	else
		UE1_AutoRelease:SetChecked(nil)
	end
	if (UE_Settings["AutoMount"]) then
		UE1_AutoMounting:SetChecked(true)
	else
		UE1_AutoMounting:SetChecked(nil)
	end
	if (UE_Settings["AutoQuest"]) then
		UE1_AutoQuest:SetChecked(true)
	else
		UE1_AutoQuest:SetChecked(nil)
	end
	if (UE_Settings["StickyFollow"]) then
		UE1_StickyFollow:SetChecked(true)
	else
		UE1_StickyFollow:SetChecked(nil)
	end
	if (UE_Settings["WhisperRelay"]) then
		UE1_WhisperRelay:SetChecked(true)
	else
		UE1_WhisperRelay:SetChecked(nil)
	end
	if (UE_Settings["HideWhispers"]) then
		UE1_HideWhispers:SetChecked(true)
	else
		UE1_HideWhispers:SetChecked(nil)
	end
	if (UE_Settings["HideQWhispers"]) then
		UE1_HideQWhispers:SetChecked(true)
	else
		UE1_HideQWhispers:SetChecked(nil)
	end
	if (UE_Settings["PassLoot"]) then
		UE1_PassLoot:SetChecked(true)
	else
		UE1_PassLoot:SetChecked(nil)
	end
--[[
	if (UE_Settings["BuffWarning"]) then
		UE1_BuffWarning:SetChecked(true)
	else
		UE1_BuffWarning:SetChecked(nil)
	end
]]--
	if (UE_Settings["LowWarning"]) then
		UE1_LowWarning:SetChecked(true)
	else
		UE1_LowWarning:SetChecked(nil)
	end
	if (UE_Settings["MailIcons"]) then
		UE1_MailIcons:SetChecked(true)
	else
		UE1_MailIcons:SetChecked(nil)
	end
	if (UE_Settings["UseFriends"]) then
		UE1_UseFriends:SetChecked(true)
	else
		UE1_UseFriends:SetChecked(nil)
	end
	if (UE_Settings["UseGuild"]) then
		UE1_UseGuild:SetChecked(true)
	else
		UE1_UseGuild:SetChecked(nil)
	end
	if (UE_Settings["DeclineDuel"]) then
		UE1_DeclineDuels:SetChecked(true)
	else
		UE1_DeclineDuels:SetChecked(nil)
	end
	if (UE_Settings["DeclineGuild"]) then
		UE1_DeclineGuilds:SetChecked(true)
	else
		UE1_DeclineGuilds:SetChecked(nil)
	end
	if (UE_Settings["OnOff"]) then
		UE1_OnOff:SetChecked(true)
	else
		UE1_OnOff:SetChecked(nil)
	end
	if (UE_Settings["UseMMB"]) then
		UE1_HideMMB:SetChecked(true)
		UE_MinimapButton:Show()
	else
		UE1_HideMMB:SetChecked(nil)
		UE_MinimapButton:Hide()
	end
--	UE_handleEvents()
end

-- Handles the minimap button right-click functionality
function UE_rightClick()
	if (UE_Settings["RightClick"] == "reload") then
		ReloadUI()
	elseif (UE_Settings["RightClick"] == "mounts") then
		UE_populateMounts()
	elseif (UE_Settings["RightClick"] == "colorpicker") then
		EML_colorPicker("open")
	elseif (UE_Settings["RightClick"] == "mount") then
		UE_pickMount("smart")
	else
		if (debugMode) then EMLChat("No right-click functionality defined.","chat",UE_EML,"red") end
	end
end

function UE_rollClear()
	if (currentTime >= highestTime) then
		UE_Timer:Hide()
		currentTime = 0;
		highestTime = 0;
		EML_clearTable(currentRolls)
		if (debugMode) then EMLChat("Rolls cleared.","chat",UE_EML) end
		UE_timerTask = false;
	else
		currentTime = (currentTime+1);
		if (debugMode) then EMLChat("Time in seconds: "..currentTime,"chat",UE_EML) end
	end
end

-- Timer, currently for clearing loot roll info
function UE_waitTimer(self,elapsed,whatDo)
	local localSettings = EML_copyTable(UE_Settings);
	self.TimeSinceLastUpdate = self.TimeSinceLastUpdate + elapsed;
	if (whatDo == "loot") then
		if (self.TimeSinceLastUpdate > UE_updateInterval) then
			self.TimeSinceLastUpdate = 0;
			UE_rollClear()
		end
	elseif (whatDo == "ress") then
		if (self.TimeSinceLastUpdate > localSettings["RessTime"]) then
			self.TimeSinceLastUpdate = 0;
			UE_Reset("ress")
			if (not InCombatLockdown()) then 
				AcceptResurrect()
			end
			UE_Timer:Hide()
		end
	elseif (whatDo == "Unused") then
		-- Unused currently
	end
	if (self.TimeSinceLastUpdate > UE_updateInterval and whatDo ~= "loot" and whatDo ~= "ress") then
		self.TimeSinceLastUpdate = 0;
		UE_Timer:Hide()
	end
end

-- Adds return or expire icons to the mailbox
function UE_InboxFrame_Update()
	ORIG_InboxFrame_Update();
	if (UE_Settings["MailIcons"]) then
		local numItems = GetInboxNumItems();
		local index = ((InboxFrame.pageNum - 1) * INBOXITEMS_TO_DISPLAY) + 1;
		for i=1, INBOXITEMS_TO_DISPLAY do
			if (index <= numItems) then
				-- show icon
				getglobal("Mail"..i.."ExpireTexture"):Show();

				-- set texture
				local expireTexture;
				if (InboxItemCanDelete(index)) then
					expireTexture = "Interface\\AddOns\\UsefulExtras\\IconMailDeleted";
				else 
					expireTexture = "Interface\\AddOns\\UsefulExtras\\IconMailReturned";
				end
				getglobal("Mail"..i.."ExpireTextureIcon"):SetTexture(expireTexture);
			else
				-- hide icon
				getglobal("Mail"..i.."ExpireTexture"):Hide();
			end
			index = index + 1;
		end
	end
end

function UE_Toggle(arg)
	if (arg == "ue") then
		if UE_Settings["OnOff"]==false then
			UE_Settings["OnOff"]=true
			EMLChat("Useful Extras turned on","chat",UE_EML)
		else
			UE_Settings["OnOff"]=false
			EMLChat("Useful Extras turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "minimap") then
		if UE_Settings["UseMMB"]==false then
			UE_Settings["UseMMB"]=true
			EMLChat("Minimap button toggled","chat",UE_EML)
		else
			UE_Settings["UseMMB"]=false
			EMLChat("Minimap button toggled","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "autofollow") then
		if UE_Settings["AutoFollow"]==false then
			UE_Settings["AutoFollow"]=true
			EMLChat("Autofollow turned on","chat",UE_EML)
		else
			UE_Settings["AutoFollow"]=false
			EMLChat("Autofollow turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "autoinvite") then
		if UE_Settings["AutoInvite"]==false then
			UE_Settings["AutoInvite"]=true
			EMLChat("Auto-accepting Invite turned on","chat",UE_EML)
		else
			UE_Settings["AutoInvite"]=false
			EMLChat("Auto-accepting Invite turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "autoress") then
		if UE_Settings["AutoRess"]==false then
			UE_Settings["AutoRess"]=true
			EMLChat("Auto-accepting Ress requests on","chat",UE_EML)
		else
			UE_Settings["AutoRess"]=false
			EMLChat("Auto-accepting Ress requests off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "autosummon") then
		if UE_Settings["AutoSummon"]==false then
			UE_Settings["AutoSummon"]=true
			EMLChat("Auto-accepting summon requests on","chat",UE_EML)
		else
			UE_Settings["AutoSummon"]=false
			EMLChat("Auto-accepting summon requests off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "autorelease") then
		if UE_Settings["AutoRelease"]==false then
			UE_Settings["AutoRelease"]=true
			EMLChat("You will automatically release on death in a battleground","chat",UE_EML)
		else
			UE_Settings["AutoRelease"]=false
			EMLChat("You won't autorelease on death in a battleground","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "automount") then
		if UE_Settings["AutoMount"]==false then
			UE_Settings["AutoMount"]=true
			EMLChat("Automounting turned on","chat",UE_EML)
		else
			UE_Settings["AutoMount"]=false
			EMLChat("Automounting turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "autoquest") then
		if UE_Settings["AutoQuest"]==false then
			UE_Settings["AutoQuest"]=true
			EMLChat("Auto-accepting Quests turned on","chat",UE_EML)
		else
			UE_Settings["AutoQuest"]=false
			EMLChat("Auto-accepting Quests turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "autocomplete") then
		if UE_Settings["AutoComplete"]==false then
			UE_Settings["AutoComplete"]=true
			EMLChat("Auto-completing Quests turned on","chat",UE_EML)
		else
			UE_Settings["AutoComplete"]=false
			EMLChat("Auto-completing Quests turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "stickyfollow") then
		if UE_Settings["StickyFollow"]==false then
			UE_Settings["StickyFollow"]=true
			EMLChat("Sticky Following turned on","chat",UE_EML)
		else
			UE_Settings["StickyFollow"]=false
			EMLChat("Sticky Following turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "whisperrelay") then
		if UE_Settings["WhisperRelay"]==false then
			UE_Settings["WhisperRelay"]=true
			EMLChat("Whisper relaying turned on","chat",UE_EML)
		else
			UE_Settings["WhisperRelay"]=false
			EMLChat("Whisper relaying turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "hidewhispers") then
		if UE_Settings["HideWhispers"]==false then
			UE_Settings["HideWhispers"]=true
			EMLChat("Hide follow whispers turned on","chat",UE_EML)
		else
			UE_Settings["HideWhispers"]=false
			EMLChat("Hide follow whispers turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "hideqwhispers") then
		if UE_Settings["HideQWhispers"]==false then
			UE_Settings["HideQWhispers"]=true
			EMLChat("Hide quest accept whispers turned on","chat",UE_EML)
		else
			UE_Settings["HideQWhispers"]=false
			EMLChat("Hide quest accept whispers turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "passloot") then
		if UE_Settings["PassLoot"]==false then
			UE_Settings["PassLoot"]=true
			EMLChat("Passing on loot rolls turned on","chat",UE_EML)
		else
			UE_Settings["PassLoot"]=false
			EMLChat("Passing on loot rolls turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "lowwarning") then
		if UE_Settings["LowWarning"]==false then
			UE_Settings["LowWarning"]=true
			EMLChat("Low Health/Mana warnings turned on","chat",UE_EML)
		else
			UE_Settings["LowWarning"]=false
			EMLChat("Low Health/Mana warnings turned off","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "mailicons") then
		if UE_Settings["MailIcons"]==false then
			UE_Settings["MailIcons"]=true
			EMLChat("Mail return or delete icons will display in the mailbox","chat",UE_EML)
		else
			UE_Settings["MailIcons"]=false
			EMLChat("Mail return or delete icons won't be shown","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "whisperloot") then
		if UE_Settings["WhisperLoot"]==false then
			UE_Settings["WhisperLoot"]=true
			EMLChat("Will now pass/greed or need when whispered by leader list","chat",UE_EML)
		else
			UE_Settings["WhisperLoot"]=false
			EMLChat("Will not pass/greed or need when whispered by leader list","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "usefriends") then
		if UE_Settings["UseFriends"]==false then
			UE_Settings["UseFriends"]=true
			EMLChat("Friends can now use whisper commands","chat",UE_EML)
		else
			UE_Settings["UseFriends"]=false
			EMLChat("Friends can no longer use whisper commands","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "useguild") then
		if UE_Settings["UseGuild"]==false then
			UE_Settings["UseGuild"]=true
			EMLChat("Guild can now use whisper commands","chat",UE_EML)
		else
			UE_Settings["UseGuild"]=false
			EMLChat("Guild can no longer use whisper commands","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "declineduels") then
		if UE_Settings["DeclineDuel"]==false then
			UE_Settings["DeclineDuel"]=true
			EMLChat("You will automatically decline duel requests","chat",UE_EML)
		else
			UE_Settings["DeclineDuel"]=false
			EMLChat("You will choose whether to accept duel requests","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "declineguilds") then
		if UE_Settings["DeclineGuild"]==false then
			UE_Settings["DeclineGuild"]=true
			EMLChat("You will automatically decline guild invites and charter requests","chat",UE_EML)
		else
			UE_Settings["DeclineGuild"]=false
			EMLChat("You will choose whether to accept guild invites and charter requests","chat",UE_EML)
		end
		UE_setCheckBoxes()
	elseif (arg == "status") then
		UE_toggleStatus()
	elseif (arg == "empty") then
		EMLChat("Valid toggles are: ue, minimap, autofollow, autoinvite, autoress, autosummon, autorelease, automount, autoquest, autocomplete, stickyfollow, whisperrelay, hidewhispers, hideqwhispers, passloot, lowwarning, mailicons, whisperloot, usefriends, useguild, declineguilds, and declineduels.","chat",UE_EML)
	else
		EMLChat("Valid toggles are: ue, minimap, autofollow, autoinvite, autoress, autosummon, autorelease, automount, autoquest, autocomplete, stickyfollow, whisperrelay, hidewhispers, hideqwhispers, passloot, lowwarning, mailicons, whisperloot, usefriends, useguild, declineguilds, and declineduels.","chat",UE_EML)
	end
end

function UE_toggleStatus()
	EMLChat("Status of UE is:","chat",UE_EML)
	if UE_Settings["OnOff"] then
		EMLChat("Useful Extras is on","chat",UE_EML)
	else
		EMLChat("Useful Extras is off","chat",UE_EML)
	end
	if UE_Settings["UseMMB"] then
		EMLChat("Minimap button is shown","chat",UE_EML)
	else
		EMLChat("Minimap button is hidden","chat",UE_EML)
	end
	if (UE_Settings["RightClick"] == "reload") then
		EMLChat("Right-clicking the minimap button will reload the UI.","chat",UE_EML)
	elseif (UE_Settings["RightClick"] == "mounts") then
		EMLChat("Right-clicking the minimap button will update your mount list.","chat",UE_EML)
	elseif (UE_Settings["RightClick"] == "colorpicker") then
		EMLChat("Right-clicking the minimap button will open the color picker.","chat",UE_EML)
	end
	if (UE_Settings["NowLeader"] ~= false) then
		EMLChat("Current leader is: "..UE_Settings["NowLeader"],"chat",UE_EML)
	else
		EMLChat("No current leader set","chat",UE_EML)
	end
	if UE_Settings["AutoFollow"] then
		EMLChat("Autofollowing is on","chat",UE_EML)
	else
		EMLChat("Autofollowing is off","chat",UE_EML)
	end
	if UE_Settings["AutoInvite"] then
		EMLChat("Auto-accept Invite is on","chat",UE_EML)
	else
		EMLChat("Auto-accept Invite is off","chat",UE_EML)
	end
	if UE_Settings["AutoRess"] then
		EMLChat("Auto-accept Ress is on","chat",UE_EML)
	else
		EMLChat("Auto-accept Ress is off","chat",UE_EML)
	end		if UE_Settings["AutoSummon"] then
		EMLChat("Auto-accept Summon is on","chat",UE_EML)
	else
		EMLChat("Auto-accept Summon is off","chat",UE_EML)
	end
	if UE_Settings["AutoRelease"] then
		EMLChat("Auto-releasing on death in a battleground is on","chat",UE_EML)
	else
		EMLChat("Auto-releasing on death in a battleground is off","chat",UE_EML)
	end
	if UE_Settings["AutoMount"] then
		EMLChat("Automounting is on","chat",UE_EML)
	else
		EMLChat("Automounting is off","chat",UE_EML)
	end
	if UE_Settings["AutoQuest"] then
		EMLChat("Auto-accepting Quests is on","chat",UE_EML)
	else
		EMLChat("Auto-accepting Quests is off","chat",UE_EML)
	end
	if UE_Settings["AutoComplete"] then
		EMLChat("Auto-completing Quests is on","chat",UE_EML)
	else
		EMLChat("Auto-completing Quests is off","chat",UE_EML)
	end
	if UE_Settings["StickyFollow"] then
		EMLChat("Sticky Following is on","chat",UE_EML)
	else
		EMLChat("Sticky Following is off","chat",UE_EML)
	end
	if UE_Settings["WhisperRelay"] then
		EMLChat("Relaying whispers is on","chat",UE_EML)
	else
		EMLChat("Relaying whispers is off","chat",UE_EML)
	end
	if UE_Settings["HideWhispers"] then
		EMLChat("Hide follow whispers is on","chat",UE_EML)
	else
		EMLChat("Hide follow whispers is off","chat",UE_EML)
	end
	if UE_Settings["PassLoot"] then
		EMLChat("Passing on loot rolls is on","chat",UE_EML)
	else
		EMLChat("Passing on loot rolls is off","chat",UE_EML)
	end
	if UE_Settings["WhisperLoot"] then
		EMLChat("Whisper looting is on","chat",UE_EML)
	else
		EMLChat("Whisper looting is off","chat",UE_EML)
	end
--[[
	if UE_Settings["BuffWarning"] then
		EMLChat("Buff timeout warnings is on","chat",UE_EML)
	else
		EMLChat("Buff timeout warnings is off","chat",UE_EML)
	end
]]--
	if UE_Settings["LowWarning"] then
		EMLChat("Low Health/Mana warnings is on","chat",UE_EML)
		EMLChat("Health: "..UE_Settings["LowHealth"].."%, Mana: "..UE_Settings["LowMana"].."%","chat",UE_EML)
	else
		EMLChat("Low Health/Mana warnings is off","chat",UE_EML)
	end
	if UE_Settings["MailIcons"] then
		EMLChat("Mail return/delete icons are being shown","chat",UE_EML)
	else
		EMLChat("Mail return/delete icons are not being shown","chat",UE_EML)
	end
	if UE_Settings["UseFriends"] then
		EMLChat("Friends can use whisper commands","chat",UE_EML)
	else
		EMLChat("Friends cannot use whisper commands","chat",UE_EML)
	end
	if UE_Settings["UseGuild"] then
		EMLChat("Guild can use whisper commands","chat",UE_EML)
	else
		EMLChat("Guild cannot use whisper commands","chat",UE_EML)
	end
	if UE_Settings["DeclineDuel"] then
		EMLChat("Auto-declining of Duel requests is on","chat",UE_EML)
	else
		EMLChat("Auto-declining of Duel requests is off","chat",UE_EML)
	end
	if UE_Settings["DeclineGuild"] then
		EMLChat("Auto-declining of Guild invites and charter requests is on","chat",UE_EML)
	else
		EMLChat("Auto-declining of Guild invites and charter requests is off","chat",UE_EML)
	end
end

function UE_sendMsg(what,where)
	if (what and where) then
		SendAddonMessage("UE","PLAYER_XP_UPDATE"..","..UnitXP("player")..","..UnitXPMax("player"),"PARTY")
	end
end
--End of code