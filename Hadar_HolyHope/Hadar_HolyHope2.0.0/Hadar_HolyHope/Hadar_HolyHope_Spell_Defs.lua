--[[  Definition table for each spell circle

Edit this table to change spell button behavior and locations.  

***** Edit at your own risk! *****
***** Edit at your own risk! *****

	Format for the table entries:
	
	CIRCLENAME = {
		[BUTTON NUMBER] = { 
			angle = ANGLE NUMBER, 
			radius = RADIUS NUMBER,
			states = {
				[STATE NUMBER] = { "ATTRIBUTE", "ATTRIBUTE", "ATTRIBUTE" },
				[STATE NUMBER] = { "ATTRIBUTE", "ATTRIBUTE", "ATTRIBUTE" }
			}
		}
	}


	What this means:
	* CIRCLE NAME:		The different circles types (blessing, spec, etc).  
				This should not be changed.

	* BUTTON NUMBER:	Each circle has a number of buttons around it starting from 1 and counting up.  
				Adding buttons means you'd make your button the next number.  

	* ANGLE NUMBER:	The angle from the center where the button will be placed.  
				An angle of 90 will place the button directly at the top of the center circle.  
				Starts at 0 and rotates counterclockwise around the center button.

	* RADIUS NUMBER:	How far from the center that the button will be placed.  
				Default is 46.

	* STATE NUMBER:	This is a number corresponding to the state number the center button is in.
				When the center button is in this state number, these attributes will apply to the spell button.  
				A number of 0 means it is the default state for this button and will always apply and be visible.
				If a spell button does not have a state assignment for a particular state, then the button is hidden.

	* ATTRIBUTE:		These are the secure button attributes that will be applied to a button.  
				These are strings and are comma delimited between attributes.
				Each attribute is a pair in the form of "Name:Value".
				See the SecureTemplates.lua file from the Blizzard FrameXML for further details on Secure Button attributes.
]] -----------

HHH_CIRCLE_SPELL_BUTTONS = { 
	blessings = { 
		[1] = {	angle = 22.5, 
			radius = 46,
			states = {
				[1] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["kings"] },
				[2] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["gkings"] }
			}
		},
		[2] = { angle = 67.5, radius = 46,
			states = {
				[1] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["might"] },
				[2] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["gmight"] }
			}
		},
		[3] = { angle = 112.5, radius = 46,
			states = { 
				[1] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["wisdom"] },
				[2] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["gwisdom"] }
			}
		},

		[4] = { angle = 157.5, radius = 46,
			states = {
				[1] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["sanctuary"] },
				[2] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["gsanctuary"] }
			}
		},
		[5] = { angle = 202.5, radius = 46,
			states = { 
				[0] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["salvation"] }
			}
		},
		[6] = { angle = 247.5, radius = 46,
			states = { [0] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["freedom"] } }
		},
		[7] = { angle = 292.5, radius = 46,
			states = { [0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["sacrifice"] } }
		},
		[8] = { angle = 337.5, radius = 46,
			states = { [0] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["protection"] } }
		}
	},

	seal = {
		[1] = { angle = 0, radius = 46,
			states = { [0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["somartyr"] } }
		},
		[2] = { angle = 0, radius = 46,
			states = { [0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["soblood"] } }
		},
		[3] = { angle = 45, radius = 46,
			states = { [0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["sowisdom"] } }
		},
		[4] = { angle = 90, radius = 46,
			states = { [0] = {"*type*:spell", "*spell*:"..HHH_SPELL_TABLE["socommand"] } }
		},
		[5] = { angle = 135, radius = 46,
			states = { [0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["solight"] } }
		},
		[6] = {angle = 180, radius = 46,
			states = { [0] =  { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["sovengeance"] } }
		},
		[7] = {angle = 180, radius = 46,
			states = { [0] =  { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["socorruption"] } }
		},
		[8] = { angle = 225, radius = 46,
			states = { [0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["sojustice"] } }
		},
		[9] = { angle = 315, radius = 46,
			states = { [0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["sorighteousness"] } }
		}
	},

	------------- Spec States: 1 = Ret, 2 = Holy, 3 = Prot ---------------
	spec = {
		[2] = { angle = 22.5, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["avengingwrath"] }
			}
		},
		
		[1] = { angle = 67.5, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["hammerofjustice"] }
			}
		},
		[5] = { angle = 112.5, radius = 46,
			states = {
				[1] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["repentance"] }, 
				[2] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["holyshock"] }, 
				[3] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["holyshield"] }
			}
		},
		[6] = { angle = 157.5, radius = 46,
			states = {
				[1] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["crusaderstrike"] },
				[2] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["divineillumination"] },
				[3] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["avengersshield"] }
			}
		},
		[7] = { angle = 202.5, radius = 46,
			states = {
				[1] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["divinestorm"] },
				[2] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["beaconoflight"] },
				[3] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["shieldofrighteousness"] }
			}
		},
		[8] = { angle = 247.5, radius = 46,
			states = {
				[0] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["cleanse"] },
				[3] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["righteousdefense"] }
			}
		},
		[4] = { angle = 292.5, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["consecration"] }
			}
		},
		[3] = { angle = 337.5, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["divineshield"] }
			}
		},
		[9] = { angle = 180, radius = 74,
			states = { 
				[1] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["sacredshield"] },
				[2] = { "unit1:", "unit2:player", "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["sacredshield"] },
				[3] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["hammeroftherighteous"] }
			}
		},
		[10] = { angle = 0, radius = 74,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["divineplea"] }
			}
		}
	},
	
	dps = {
		[1] = { angle = 0, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["exorcism"] }
			}
		},
		[2] = { angle = 45, radius = 46,
			states = {
				[0] =  { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["hammerofwrath"] }
			}
		},
		[3] = { angle = 90, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["socommand"] }
			}
		},
		[4] = { angle = 135, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["divinestorm"] }
			}
		},
		[5] = { angle = 180, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["crusaderstrike"] }
			}
		},
		[6] = { angle = 225, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["consecration"] }
			}
		},
		[7] = { angle = 315, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["holywrath"] }
			}
		},
		[8] = { angle = 270, radius = 46,
			states = {
				[0] = { "*type*:spell", "*spell*:"..HHH_SPELL_TABLE["avengingwrath"] }
			}
		}
	}
	
};
