------------------------
-- Hadar's Holy Hope
-- German localization
------------------------

if ( GetLocale() == "deDE" ) then

HHH_OPTIONS_TITLE = "Hadar's Holy Hope ("..GetAddOnMetadata("Hadar_HolyHope", "Version")..")";
HHH_OPTIONS_SUBTEXT = "These options control the properties of Hadar's Holy Hope Add On.";

HHH_SPELL_TABLE = {

	might = "Segen der Macht",
	wisdom = "Segen der Weisheit",
	kings = "Segen der K\195\182nige",
	sanctuary = "Segen des Refugiums",
	
	salvation = "Hand der Erl195\182sung",
	freedom = "Hand der Freiheit",
	sacrifice = "Hand der Aufopferung",
	protection = "Hand des Schutzes",
	
	gmight = "Gro\195\159er Segen der Macht",
	gwisdom = "Gro\195\159er Segen der Weisheit",
	gkings = "Gro\195\159er Segen der K\195\182nige",
	gsanctuary = "Gro\195\159er Segen des Refugiums",
	
	sojustice = "Siegel der Gerechtigkeit",
	solight = "Siegel des Lichts",
	sowisdom = "Siegel der Weisheit",
	sorighteousness = "Siegel der Rechtschaffenheit",
	somartyr = "Siegel des M\195\164rtyrer",
	soblood = "Siegel des Blutes",
	sovengeance = "Siegel der Vergeltung",
	socorruption = "Siegel des Verderbnis",
	socommand = "Siegel des Befehls",

	hammerofjustice = "Hammer der Gerechtigkeit",
	avengingwrath = "Zornige Vergeltung",
	divineshield = "Gottesschild",
	righteousdefense = "Rechtschaffene Verteidigung",
	cleanse = "Reinigung des Glaubens",
	holywrath = "Heiliger Zorn",
	exorcism = "Exorzismus",

	divinestorm = "G\195\182ttlicher Sturm",
	crusaderstrike = "Kreuzfahrersto\195\159",
	repentance = "Bu\195\159e",
	
	holyshock = "Heiliger Schock",
	divinefavor = "G\195\182ttliche Gunst",
	
	holyshield = "Heiliger Schild",
	avengersshield = "Schild des R\195\164chers",

	divineillumination = "G\195\182ttliche Eingebung",

	hammerofwrath = "Hammer des Zorns",
	redemption = "Erl\195\182sung",
	divineintervention = "G\195\182ttliches Eingreifen",

	consecration = "Weihe",
	divinestorm = "G\195\182ttlicher Sturm",

	jolight = "Richturteil des Lichts",
	jowisdom = "Richturteil der Weisheit",
	jojustice = "Richturteil der Gerechtigkeit",

	sacredshield = "Heiliger Schild",
	divineplea = "G\195\182ttliche Bitte",
	hammeroftherighteous" = "Hammer der Rechtschaffenen",
	shieldofrighteousness" = "Schild der Rechtschaffenheit",
	divineprotection = "G\195\182ttlicher Schutz",
	beaconoflight = "Flamme des Glaubens"
};

HHH_MACRO_TABLE = {
	righteousdefense = "/cast [help] Rechtschaffene Verteidigung; [target=targettarget, help] Rechtschaffene Verteidigung; Rechtschaffene Verteidigung;",
};

HHH_ITEM = {
	["Kings"] = "Symbol der K\195\182nige",
	["Hearthstone"] = "Ruhestein",
};

HHH_MENU = {
	["Length"] = "L�nge",
	["Active"] = "Activ",
	["Lock"] = "Frame Lock",
	["UnlockText"] = "Frames sind jetzt entsperrt",
	["LockText"] = "Frames sind jetzt gesperrt",
	["Tooltips"] = "Tooltips",
	["Blessing"] = "Segen",
	["Seal"] = "Siegel",
	["Mount"] = "Reittier",
	["Off"] = "Aus",
	["Partial"] = "Partial",
	["Total"] = "Full",
	["Spec"] = "Spec",
	["Redemption"] = "Erl\195\182sung",
	["Show"] = "Show",
	["DPS"] = "DPS"
};

end