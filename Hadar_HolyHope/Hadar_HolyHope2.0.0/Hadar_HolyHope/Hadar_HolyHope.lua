------------------------------------------------------------------------------------
--
-- Hadar's Holy Hope
-- Note: This is based on the original "Holy Hope" mod.  I've recreated most of the 
--		 functionality of the original and improved on a few things.  I did this out 
--		 of respect for the original author and his work.  
--
--		 All code is my own, so blame me if something doesn't work. :)
--
-- Author: Hadar <Sunder>, Khaz Modan
-- Original Concept: Battochon <The Horde>, Ysondre (EU)
--
------------------------------------------------------------------------------------

HHH_SPELLS1 = {};

HHH_DefaultConfig = {
	["BlessState"] = 1,
	["SpecState"] = 1,
	["SealState"] = 1,
	["DPSState"] = 1,

	["BlessingScale"] = 100,
	["SealScale"] = 100,
	["SpecScale"] = 100,
	["RedemptionScale"] = 100,
	["DPSScale"] = 100,

	["BlessingToggle"] = 1,
	["SealToggle"] = 1,
	["SpecToggle"] = 1,
	["RedemptionToggle"] = 1,
	["DPSToggle"] = 1,

	["Lock"] = 0,
	["Tooltip"] = 2
};

if (not HHH) then
	HHH = {};
end

if (not HHH.Globals) then
	HHH.Globals = {};
end

HHH.Globals.ICON_DIR = "Interface\\AddOns\\Hadar_HolyHope\\Icons";
HHH.Globals.SPEC_LABEL = {"RET", "HOLY", "PROT"};
HHH.Globals.firstLoad = true;

HHH.spellButtons = {};

-------------------
--Helper Functions
-------------------

-- Prints a message to the default chat window
function HHH.Print(msg)
    if (msg) then
		DEFAULT_CHAT_FRAME:AddMessage(tostring(msg));
    end
end

function HHH.IsPlayerPaladin()
	local localizedClass, englishClass;
	localizedClass, englishClass = UnitClass("player");
	
	if (englishClass == "PALADIN") then
		return true;
	else
		return false;
	end	
end


-- Counts the number of "Symbol of Kings" a player has in his bags
function HHH.CountSymbols()
	local symbols = 0;
	local bag, slot = 0;
	
	for bag = 0, NUM_BAG_FRAMES do
		for slot = 1, GetContainerNumSlots(bag) do
			local itemName = GetContainerItemLink(bag, slot);
			if (itemName) then
				if (strfind(itemName, "%["..HHH_ITEM.Kings.."%]")) then
					local texture, count = GetContainerItemInfo(bag, slot);
					symbols = symbols + count;
				end
			end
		end
	end
	return symbols;
end

-- Sets the text for the symbol counter
function HHH.UpdateSymbolCounter()
	local symbolCount = HHH.CountSymbols();
	HHH_BlessCenterButtonText:SetText(symbolCount);
end

function HHH.UpdateSymbolCounter()
	local symbolCount = HHH.CountSymbols();
	HHH_BlessCenterButtonText:SetText(symbolCount);
end

-- Scales the frame and the children.
function HHH.ScaleButton(frm, scale)
	if (frm and scale) then
		local s = scale / 100;
		frm:SetScale(s);
	end
end

function HHH.SpellButton_UpdateCooldown(button, elapsed)
	local spellName = button.spellName;
	local buttonName = button:GetName();
	local start, duration, enable = GetSpellCooldown(spellName);
	local usable, noMana = IsUsableSpell(spellName);
	
	if (start and duration) then
		local cooldown = "";
		if ( start > 0 and duration > 0) then
			cooldown = tostring(ceil(duration - ( GetTime() - start)));
		end
		button:SetText(cooldown);
	end
	
	if (usable and start == 0) then
		getglobal(buttonName.."Icon"):SetVertexColor(1,1,1,1);
	else
		getglobal(buttonName.."Icon"):SetVertexColor(0.3,0.3,0.3,1);
	end
	
end

function HHH.Center_OnDragStart(self)
	if (HHH_Config.Lock == 0) then
		self:StartMoving();
	end
end

function HHH.Center_OnDragStop(self)
	--local point, relativeTo, relativePoint, xOfs, yOfs;
	--point, relativeTo, relativePoint, xOfs, yOfs = self:GetPoint();
	--HHH_CIRCLE_LOC[self:GetName()] = {point, "UIParent", relativePoint, xOfs, yOfs};
	self:StopMovingOrSizing();
end

function HHH.PopSpells()
	local spellid = 0;
	local spellname = "";

	for i = 2, MAX_SKILLLINE_TABS do
		local name, texture, offset, numSpells = GetSpellTabInfo(i);
   		
   		if not name then
			break;
		end
   		
		for s = offset + 1, offset + numSpells do
			local spell, rank = GetSpellName(s, BOOKTYPE_SPELL);
	      		if rank then
				spell = spell.." "..rank;
			end
			HHH.Print(name..": "..spell..": "..s..": "..offset);
		end
	end
end

function HHH.HasSpell(spellName)
	local name;
	name = GetSpellInfo(spellName);
	
	if (name ~= nil) then
		return true;
	else
		return false;
	end	
end

function HHH.GetSpellID(spellName)
	local spellID = 0;
	local i = 1;
	while true do
		local spell, rank = GetSpellName(i, BOOKTYPE_SPELL);
		if (not spell) then
			break;
		else
			if (strlower(spell) == strlower(spellName)) then
				spellID = i;
			end
		end
		i = i + 1;
	end
	return spellID;
end

function HHH.CreateSpellButtonTooltip(self)

	-- HHH_Config.Tooltip values
	--	2 = Full
	--	1 = Partial
	--	0 = None

	local spellID = HHH.GetSpellID(self.spellName);
	
	if (spellID == 0 or spellID == nil) then
		return;
	end

	if (HHH_Config.Tooltip == 2) then
		--Full
		GameTooltip_SetDefaultAnchor(GameTooltip, UIParent);
		GameTooltip:SetHyperlink(GetSpellLink(spellID, BOOKTYPE_SPELL))
		GameTooltip:Show();

	elseif (HHH_Config.Tooltip == 1) then
		--Partial
		local mana = nil;
		local manaStr;
		
		GameTooltip_SetDefaultAnchor(GameTooltip, UIParent);
		--GameTooltip:SetSpell(spellID, BOOKTYPE_SPELL);
		GameTooltip:SetHyperlink(GetSpellLink(spellID, BOOKTYPE_SPELL))
		
		spellName = GameTooltipTextLeft1:GetText();
		manaStr = GameTooltipTextLeft2:GetText();
		mana = strfind(manaStr, "%d* Mana");
		
		GameTooltip:ClearLines();
		GameTooltip:AddLine(spellName,1,1,1);
		
		if (mana) then
			GameTooltip:AddLine(manaStr);
		else
			GameTooltip:AddLine("0 Mana");
		end
		
		GameTooltip:Show();		
	elseif (HHH_Config.Tooltip == 0) then
		--Off
	end

end

----------------------------------
-- Function to create the center 
-- circle buttons
----------------------------------
function HHH.CreateCenters()
	----------------------------------------------
	-- Blessings circle center button
	----------------------------------------------
	HHH.blessCenter = CreateFrame("Button", "HHH_BlessCenterButton", UIParent, "HHH_CenterButtonTemplate");
	HHH.blessCenter:RegisterForDrag("LeftButton");
	HHH.blessCenter:RegisterForClicks("LeftButtonUp", "RightButtonUp");

	--------------------------------------
	-- Seal circle center button
	--------------------------------------
	HHH.sealCenter = CreateFrame("Button", "HHH_SealCenterButton", UIParent, "HHH_CenterButtonTemplate");
	HHH.sealCenter:RegisterForDrag("LeftButton");
	HHH.sealCenter:RegisterForClicks("LeftButtonUp", "RightButtonUp");
	
	---------------------------------------
	-- Spec circle center button
	---------------------------------------
	HHH.specCenter = CreateFrame("Button", "HHH_SpecCenterButton", UIParent, "HHH_CenterButtonTemplate");
	HHH.specCenter:RegisterForDrag("LeftButton");
	HHH.specCenter:RegisterForClicks("LeftButtonUp", "RightButtonUp");
	
	--------------------------------------
	-- DPS circle center button
	--------------------------------------
	HHH.dpsCenter = CreateFrame("Button", "HHH_DPSCenterButton", UIParent, "HHH_CenterButtonTemplate");
	HHH.dpsCenter:RegisterForDrag("LeftButton");
	HHH.dpsCenter:RegisterForClicks("LeftButtonUp", "RightButtonUp");

	-------------------------------
	-- Redemption Button
	-------------------------------
	HHH.redemptionBtn = CreateFrame("Button", "HHH_RedemptionButton", UIParent, "HHH_RedemptionButtonTemplate");
	HHH.redemptionBtn:RegisterForDrag("LeftButton");
	HHH.redemptionBtn:RegisterForClicks("LeftButtonUp", "RightButtonUp");

end

function HHH.ConfigureCenters()
	
	----------------------------------------------
	-- Blessings circle center button
	----------------------------------------------
	HHH.blessCenter.spellName = HHH_ITEM["Hearthstone"];
	HHH_BlessCenterButtonIcon:SetTexture(HHH.Globals.ICON_DIR.."\\symbol32");

	HHH.blessCenter:SetAttribute("type2", "attribute");
	HHH.blessCenter:SetAttribute("attribute-name2", "nextstate");
	HHH.blessCenter:SetAttribute("attribute-value2", 1);

	--HHH.blessCenter:SetAttribute("type2", "item");	
	--HHH.blessCenter:SetAttribute("item2", HHH.blessCenter.spellName);
	
	HHH.blessCenter:Execute(
	[[
		currentState = 1;
		minState = 1;
		maxState = 2;
	]] );
	
	HHH.blessCenter.UpdateText = function (self, newText)
		self:SetText(newText);
	end
	
	HHH.blessCenter.UpdateIcon = function (self, spellName, round)
		local texture = GetSpellTexture(HHH_SPELL_TABLE[spellName]);
		if (round) then
			SetPortraitToTexture(getglobal(self:GetName().."Icon"), texture);
		else	
			getglobal(self:GetName().."Icon"):SetTexture(texture);
		end	
	end

	HHH.blessCenter.UpdateConfig = function (self, value)
		HHH_Config["BlessState"] = value;
	end

	HHH.blessCenter:SetAttribute("_onattributechanged",  
	[[
		if (name == "nextstate") then
			currentState = self:GetAttribute("state") + value;

			if (currentState > maxState) then currentState = minState; end
			if (currentState < minState) then currentState = maxState; end

			self:SetAttribute("state", currentState);
		end
	
		if (name == "state") then
			control:CallMethod("UpdateConfig", value);
			control:ChildUpdate("state", value);
			if (stateLabel) then
				control:CallMethod("UpdateText", stateLabel[value]);
			end
		end
	]]);

	--------------------------------------
	-- Seal circle center button
	--------------------------------------
	HHH.sealCenter.spellName = HHH_SPELL_TABLE["jowisdom"];
	HHH.sealCenter:SetScript("OnUpdate", function(self, elapsed) HHH.SpellButton_UpdateCooldown(self, elapsed); end);

	HHH.sealCenter.UpdateText = function (self, newText)
		self:SetText(newText);
	end
	
	HHH.sealCenter.UpdateIcon = function (self, spellName, round)
		local texture = GetSpellTexture(HHH_SPELL_TABLE[spellName]);
		if (round) then
			SetPortraitToTexture(getglobal(self:GetName().."Icon"), texture);
		else	
			getglobal(self:GetName().."Icon"):SetTexture(texture);
		end	
	end
	
	HHH.sealCenter.UpdateConfig = function (self, value)
		HHH_Config["SealState"] = value;
	end

	HHH.sealCenter:SetAttribute("*type1", "spell");
	HHH.sealCenter:SetAttribute("*spell1", HHH_SPELL_TABLE["jowisdom"]);
	
	HHH.sealCenter:SetAttribute("type2", "attribute");
	HHH.sealCenter:SetAttribute("attribute-name2", "nextstate");
	HHH.sealCenter:SetAttribute("attribute-value2", 1);
	
	HHH.sealCenter:SetAttribute("jolight", HHH_SPELL_TABLE["jolight"]);
	HHH.sealCenter:SetAttribute("jowisdom", HHH_SPELL_TABLE["jowisdom"]);
	HHH.sealCenter:SetAttribute("jojustice", HHH_SPELL_TABLE["jojustice"]);
	
	HHH.sealCenter:Execute(
	[[
		currentState = 1;
		minState = 1;
		maxState = 3;
		
		stateJudgement = newtable();
		stateJudgement[1] = "jowisdom"
		stateJudgement[2] = "jolight";
		stateJudgement[3] = "jojustice";
	]] );

	HHH.sealCenter:SetAttribute("_onattributechanged", 
	[[
		if (name == "nextstate") then
			currentState = self:GetAttribute("state") + value;

			if (currentState > maxState) then currentState = minState; end
			if (currentState < minState) then currentState = maxState; end

			self:SetAttribute("state", currentState);
		end

		if (name == "state") then
			self:SetAttribute("*spell1", self:GetAttribute(stateJudgement[value]));
			control:CallMethod("UpdateIcon",  stateJudgement[value], true);
			control:CallMethod("UpdateConfig", value);
			control:ChildUpdate("state", value);
			
		end
	]] );
	
	---------------------------------------
	-- Spec circle center button
	---------------------------------------
	HHH.specCenter.spellName = HHH_SPELL_TABLE["hammerofwrath"];
	SetPortraitToTexture(HHH_SpecCenterButtonIcon, GetSpellTexture(HHH.specCenter.spellName));
		
	HHH.specCenter:SetScript("OnUpdate", function(self, elapsed) HHH.SpellButton_UpdateCooldown(self, elapsed); end);

	HHH.specCenter.Text2 = HHH.specCenter:CreateFontString(nil, "OVERLAY");
	HHH.specCenter.Text2:SetFontObject("GameFontNormal");
	HHH.specCenter.Text2:SetPoint("CENTER", HHH.specCenter, "CENTER", 0, 10);
	HHH.specCenter.Text2:SetText("Spec");
	
	HHH.specCenter.UpdateText = function (self, newText)
		self.Text2:SetText(newText);
	end
	
	HHH.specCenter.UpdateIcon = function (self, spellName, round)
		local texture = GetSpellTexture(HHH_SPELL_TABLE[spellName]);
		if (round) then
			SetPortraitToTexture(getglobal(self:GetName().."Icon"), texture);
		else	
			getglobal(self:GetName().."Icon"):SetTexture(texture);
		end	
	end
	
	HHH.specCenter.UpdateConfig = function (self, value)
		HHH_Config["SpecState"] = value;
	end
	
	HHH.specCenter:SetAttribute("*type1", "spell");
	HHH.specCenter:SetAttribute("*spell1", HHH.specCenter.spellName);
	
	HHH.specCenter:SetAttribute("type2", "attribute");
	HHH.specCenter:SetAttribute("attribute-name2", "nextstate");
	HHH.specCenter:SetAttribute("attribute-value2", 1);
	
	HHH.specCenter:Execute(
	[[
		currentState = 1;
		minState = 1;
		maxState = 3;

		stateLabel = newtable();
		stateLabel[1] = "Ret";
		stateLabel[2] = "Holy";
		stateLabel[3] = "Prot";
	]] );

	HHH.specCenter:SetAttribute("_onattributechanged", 
	[[
		if (name == "nextstate") then
			currentState = self:GetAttribute("state") + value;

			if (currentState > maxState) then currentState = minState; end
			if (currentState < minState) then currentState = maxState; end

			self:SetAttribute("state", currentState);
		end
	
		if (name == "state") then
			control:CallMethod("UpdateConfig", value);
			control:ChildUpdate("state", value);
			if (stateLabel) then
				control:CallMethod("UpdateText", stateLabel[value]);
			end
		end
	]]);

	--------------------------------------
	-- DPS circle center button
	--------------------------------------
	HHH.dpsCenter.spellName = HHH_SPELL_TABLE["jowisdom"];
	HHH.dpsCenter:SetScript("OnUpdate", function(self, elapsed) HHH.SpellButton_UpdateCooldown(self, elapsed); end);

	HHH.dpsCenter.UpdateText = function (self, newText)
		self:SetText(newText);
	end
	
	HHH.dpsCenter.UpdateIcon = function (self, spellName, round)
		local texture = GetSpellTexture(HHH_SPELL_TABLE[spellName]);
		if (round) then
			SetPortraitToTexture(getglobal(self:GetName().."Icon"), texture);
		else	
			getglobal(self:GetName().."Icon"):SetTexture(texture);
		end	
	end
	
	HHH.dpsCenter.UpdateConfig = function (self, value)
		HHH_Config["DPSState"] = value;
	end

	HHH.dpsCenter:SetAttribute("*type1", "spell");
	HHH.dpsCenter:SetAttribute("*spell1", HHH_SPELL_TABLE["jowisdom"]);
	
	HHH.dpsCenter:SetAttribute("type2", "attribute");
	HHH.dpsCenter:SetAttribute("attribute-name2", "nextstate");
	HHH.dpsCenter:SetAttribute("attribute-value2", 1);
	
	HHH.dpsCenter:SetAttribute("jolight", HHH_SPELL_TABLE["jolight"]);
	HHH.dpsCenter:SetAttribute("jowisdom", HHH_SPELL_TABLE["jowisdom"]);
	HHH.dpsCenter:SetAttribute("jojustice", HHH_SPELL_TABLE["jojustice"]);
	
	HHH.dpsCenter:Execute(
	[[
		currentState = 1;
		minState = 1;
		maxState = 3;
		
		stateJudgement = newtable();
		stateJudgement[1] = "jowisdom"
		stateJudgement[2] = "jolight";
		stateJudgement[3] = "jojustice";
	]] );

	HHH.dpsCenter:SetAttribute("_onattributechanged", 
	[[
		if (name == "nextstate") then
			currentState = self:GetAttribute("state") + value;

			if (currentState > maxState) then currentState = minState; end
			if (currentState < minState) then currentState = maxState; end

			self:SetAttribute("state", currentState);
		end

		if (name == "state") then
			self:SetAttribute("*spell1", self:GetAttribute(stateJudgement[value]));
			control:CallMethod("UpdateIcon",  stateJudgement[value], true);
			control:CallMethod("UpdateConfig", value);
			control:ChildUpdate("state", value);
		end
	]] );	
	HHH.dpsCenter:Show();

	-------------------------------
	-- Redemption Button
	-------------------------------
	local spellName = HHH_SPELL_TABLE["redemption"];
	--HHH.redemptionBtn:SetHighlightTexture(nil);
	HHH.redemptionBtn:SetScript("OnUpdate", function(self, elapsed) HHH.SpellButton_UpdateCooldown(self, elapsed); end);
	HHH.redemptionBtn.spellName = spellName;
	HHH.redemptionBtn:SetAttribute("*type*", "spell");
	HHH.redemptionBtn:SetAttribute("*spell*", HHH.redemptionBtn.spellName);
	
	SetPortraitToTexture(HHH_RedemptionButtonIcon, GetSpellTexture(spellName));
		
	HHH.redemptionBtn:SetAttribute("_onstate-dead",
	[[
		if (newstate == "true") then
			self:Show();
		else
			self:Hide();
		end
	]]);
			
	--RegisterStateDriver(HHH.redemptionBtn, "dead", "[dead, help, nocombat] true; false");
	HHH.redemptionBtn:Hide();
end

-----------------------------------------
-- Create spell buttons functions
-----------------------------------------
-- Note: Button creation happens after
-- character load due to spell checks.
-----------------------------------------

function HHH.SetButtonAttributes(btn, ...)
	for i=1, select("#", ...) do
		local start, finish, attrib = strfind(select(i, ...), "([^:]+)"); 
 		local val = strsub(select(i, ...), finish+2);
		btn:SetAttribute(attrib, val);
	end
end


function HHH.SpellButton_OnAttributeChanged(self, name, value)
	if (name == "state") then
		local circleName = self.circleName
		local attribs = HHH_CIRCLE_SPELL_BUTTONS[circleName][self:GetID()].states[value] or
			       HHH_CIRCLE_SPELL_BUTTONS[circleName][self:GetID()].states[0];

		if (attribs) then
			local spellname, spelltex;
			
			HHH.SetButtonAttributes(self, unpack(attribs));		
			
			spellname, _, spelltex = GetSpellInfo(self:GetAttribute("*spell*"));
			
			if (spellname) then
				SetPortraitToTexture(getglobal(self:GetName().."Icon"), spelltex);
				self.spellName = spellname;
				self:Show();
			else
				self:Hide();
			end
		else
			self:Hide();
		end
	end
end

function HHH.CreateSpellButtons(circleName, header)

	for btnNum, vals in pairs(HHH_CIRCLE_SPELL_BUTTONS[circleName]) do
		local btnName = "HHH_"..circleName.."_SpellButton_"..btnNum;
		local btn = getglobal(btnName);

		local states  = vals.states;
		local angle = vals.angle;
		local radius = vals.radius;
		
		local x = radius * cos(angle);
		local y = radius * sin(angle);
			
		local spellName;
		
		if (not btn) then
			btn = CreateFrame("Button", btnName, header, "HHH_SpellButtonTemplate");
			btn:RegisterForClicks("LeftButtonUp", "RightButtonUp");
			btn:SetPoint("CENTER", header, "CENTER", x, y);
			btn:SetScript("OnUpdate", function(self, elapsed) HHH.SpellButton_UpdateCooldown(self, elapsed); end);
			btn:SetID(btnNum);
			btn.circleName = circleName;

			btn:SetAttribute("checkselfcast", true);
			btn:SetAttribute("checkfocuscast", true);

			btn:SetAttribute("_childupdate-state",
			[[
				self:SetAttribute("state", message);
			]] );
			
			btn:Hide();			
		end
	
		
	end
end

------------------------
-- Show or Hide Anchors
------------------------
function HHH.ShowHide_Anchors()
	if (HHH_Config.BlessingToggle == 1) then
		HHH_BlessCenterButton:Show();
	else
		HHH_BlessCenterButton:Hide();
	end
	
	if (HHH_Config.SealToggle == 1) then
		HHH_SealCenterButton:Show();
	else
		HHH_SealCenterButton:Hide();
	end
	
	if (HHH_Config.SpecToggle == 1) then
		HHH_SpecCenterButton:Show();
	else
		HHH_SpecCenterButton:Hide();
	end
	
	if (HHH_Config.DPSToggle == 1) then
		HHH_DPSCenterButton:Show();
	else
		HHH_DPSCenterButton:Hide();
	end
	
	if (HHH_Config.RedemptionToggle == 1) then
		RegisterStateDriver(HHH.redemptionBtn, "dead", "[dead, help, nocombat] true; false");
	else
		UnregisterStateDriver(HHH.redemptionBtn, "dead");
		HHH_RedemptionButton:Hide();
	end
end



-- Initialization function
function HHH.Initialize()
	if (HHH.IsPlayerPaladin()) then
		HHH.ConfigureCenters();
		HHH.CreateSpellButtons("blessings", HHH.blessCenter);
		HHH.CreateSpellButtons("seal", HHH.sealCenter);
		HHH.CreateSpellButtons("spec", HHH.specCenter);	
		HHH.CreateSpellButtons("dps", HHH.dpsCenter);

		HHH.ScaleButton(HHH.blessCenter, HHH_Config.BlessingScale);
		HHH.ScaleButton(HHH.sealCenter, HHH_Config.SealScale);
		HHH.ScaleButton(HHH.specCenter, HHH_Config.SpecScale);
		HHH.ScaleButton(HHH.dpsCenter, HHH_Config.DPSScale);
		HHH.ScaleButton(HHH.redemptionBtn, HHH_Config.RedemptionScale);
		
		HHH.blessCenter:SetAttribute("state", HHH_Config.BlessState);
		HHH.sealCenter:SetAttribute("state", HHH_Config.SealState);
		HHH.specCenter:SetAttribute("state", HHH_Config.SpecState);
		HHH.dpsCenter:SetAttribute("state", HHH_Config.DPSState);

		HHH.ShowHide_Anchors();
	else
		HHH_EventFrame:UnregisterAllEvents();
		HHH_EventFrame:SetScript("OnUpdate", nil);
		HHH_EventFrame:SetScript("OnEvent", nil);
		
		HHH.blessCenter:Hide();
		HHH.sealCenter:Hide();
		HHH.specCenter:Hide();
		HHH.dpsCenter:Hide();
		HHH.redemptionBtn:Hide();
	end	
end

-------------------------------
--Event Frame functions
-------------------------------
local function HHH_EventFrame_OnEvent(event)
	if (event == "BAG_UPDATE") then
		HHH.UpdateSymbolCounter();

	elseif (event == "PLAYER_ENTERING_WORLD" and HHH.Globals.firstLoad) then
		HHH.Globals.firstLoad = false;
		HHH.Initialize();

	elseif (event == "VARIABLES_LOADED") then
		if (not HHH_Config) then
			print("No HHH_Config Found");
			HHH_Config = {};
			HHH_Config = HHH_DefaultConfig;
		else
			for index, value in pairs(HHH_DefaultConfig) do
				if (not HHH_Config[index]) then
					print("HHH_Config["..index.."] not found.");
					HHH_Config[index] = value;
				end
			end
		end
	
	elseif (event == "LEARNED_SPELL_IN_TAB") then
		HHH.blessCenter:Execute( [[ control:ChildUpdate("state", currentState); ]]);
		HHH.sealCenter:Execute( [[ control:ChildUpdate("state", currentState); ]]);
		HHH.specCenter:Execute( [[ control:ChildUpdate("state", currentState); ]]);
		HHH.dpsCenter:Execute( [[ control:ChildUpdate("state", currentState); ]]);
	end
end

-----------------------------------
-- Create Center Frames
-----------------------------------
HHH.CreateCenters();

------------------------------
-- Create event frame
------------------------------
local HHH_evFrame = CreateFrame("Frame", "HHH_EventFrame");
HHH_evFrame:SetScript("OnEvent", function(self, event) HHH_EventFrame_OnEvent(event); end);

HHH_evFrame:RegisterEvent("BAG_UPDATE");
HHH_evFrame:RegisterEvent("ADDON_LOADED");
HHH_evFrame:RegisterEvent("PLAYER_ENTERING_WORLD");
HHH_evFrame:RegisterEvent("VARIABLES_LOADED");
HHH_evFrame:RegisterEvent("LEARNED_SPELL_IN_TAB");

-------------------------
-- Slash Command Handler
-------------------------
function HHH_Slash_Handler()
	InterfaceOptionsFrame_OpenToCategory("Hadar's Holy Hope");
end

SlashCmdList["HADARHOLYHOPE"] = HHH_Slash_Handler;
SLASH_HADARHOLYHOPE1 = "/hhh";


--Key Binding Labels
BINDING_HEADER_HHH_BIND = "Hadar's Holy Hope";