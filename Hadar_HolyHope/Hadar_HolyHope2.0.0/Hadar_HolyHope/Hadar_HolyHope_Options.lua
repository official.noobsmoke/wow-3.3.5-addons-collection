
function HHH_OptionsFrame_OnLoad(panel)
	
	panel.name = "Hadar's Holy Hope";
	panel.okay = HHH_OptionsFrame_Okay;
	panel.cancel = HHH_OptionsFrame_Cancel;
	panel.default = HHH_OptionsFrame_Default;
	panel.refresh = HHH_OptionsFrame_OnShow;

	InterfaceOptions_AddCategory(panel);	
end

function HHH_OptionsFrame_OnVariableLoad()
-- Set Check Button values
	HHH_OptionsFrameBlessingCheckButton.currValue = HHH_Config.BlessingToggle;
	HHH_OptionsFrameBlessingCheckButton.value = HHH_Config.BlessingToggle;

	HHH_OptionsFrameSpecCheckButton.currValue = HHH_Config.SpecToggle;
	HHH_OptionsFrameSpecCheckButton.value = HHH_Config.SpecToggle;

	HHH_OptionsFrameSealCheckButton.currValue = HHH_Config.SealToggle;
	HHH_OptionsFrameSealCheckButton.value = HHH_Config.SealToggle;

	HHH_OptionsFrameDPSCheckButton.currValue = HHH_Config.DPSToggle;
	HHH_OptionsFrameDPSCheckButton.value = HHH_Config.DPSToggle;

	HHH_OptionsFrameRedemptionCheckButton.currValue = HHH_Config.RedemptionToggle;
	HHH_OptionsFrameRedemptionCheckButton.value = HHH_Config.RedemptionToggle;
	
	HHH_OptionsFrameLockCheckButton.currValue = HHH_Config.Lock;
	HHH_OptionsFrameLockCheckButton.value = HHH_Config.Lock;

-- Set Scale slider values
	HHH_OptionsFrameBlessingScale.currValue = HHH_Config.BlessingScale;
	HHH_OptionsFrameBlessingScale.value = HHH_Config.BlessingScale;

	HHH_OptionsFrameSpecScale.currValue = HHH_Config.SpecScale;
	HHH_OptionsFrameSpecScale.value = HHH_Config.SpecScale;

	HHH_OptionsFrameSealScale.currValue = HHH_Config.SealScale;
	HHH_OptionsFrameSealScale.value = HHH_Config.SealScale;
	
	HHH_OptionsFrameDPSScale.currValue = HHH_Config.DPSScale;
	HHH_OptionsFrameDPSScale.value = HHH_Config.DPSScale;
		
	HHH_OptionsFrameRedemptionScale.currValue = HHH_Config.RedemptionScale;
	HHH_OptionsFrameRedemptionScale.value = HHH_Config.RedemptionScale;

-- Set tooltip dropdown value
	HHH_OptionsFrameTooltipDropDown.currValue = HHH_Config.Tooltip;
	HHH_OptionsFrameTooltipDropDown.value = HHH_Config.Tooltip;

end

function HHH_OptionsFrame_OnShow()
	
	-- Set check buttons
	HHH_OptionsFrameBlessingCheckButton:SetChecked(HHH_OptionsFrameBlessingCheckButton.value);
	HHH_OptionsFrameSpecCheckButton:SetChecked(HHH_OptionsFrameSpecCheckButton.value);
	HHH_OptionsFrameSealCheckButton:SetChecked(HHH_OptionsFrameSealCheckButton.value);
	HHH_OptionsFrameDPSCheckButton:SetChecked(HHH_OptionsFrameDPSCheckButton.value);
	HHH_OptionsFrameRedemptionCheckButton:SetChecked(HHH_OptionsFrameRedemptionCheckButton.value);
	HHH_OptionsFrameLockCheckButton:SetChecked(HHH_OptionsFrameLockCheckButton.value);

	-- Set sliders
	HHH_OptionsFrameBlessingScale:SetValue(HHH_OptionsFrameBlessingScale.value);
	HHH_OptionsFrameBlessingScaleText:SetText(HHH_OptionsFrameBlessingScale.value);
	HHH_SliderEnableDisable(HHH_OptionsFrameBlessingCheckButton.value, HHH_OptionsFrameBlessingScale);

	HHH_OptionsFrameSpecScale:SetValue(HHH_OptionsFrameSpecScale.value);
	HHH_OptionsFrameSpecScaleText:SetText(HHH_OptionsFrameSpecScale.value);
	HHH_SliderEnableDisable(HHH_OptionsFrameSpecCheckButton.value, HHH_OptionsFrameSpecScale);

	HHH_OptionsFrameSealScale:SetValue(HHH_OptionsFrameSealScale.value);
	HHH_OptionsFrameSealScaleText:SetText(HHH_OptionsFrameSealScale.value);
	HHH_SliderEnableDisable(HHH_OptionsFrameSealCheckButton.value, HHH_OptionsFrameSealScale);
	
	HHH_OptionsFrameDPSScale:SetValue(HHH_OptionsFrameDPSScale.value);
	HHH_OptionsFrameDPSScaleText:SetText(HHH_OptionsFrameDPSScale.value);
	HHH_SliderEnableDisable(HHH_OptionsFrameDPSCheckButton.value, HHH_OptionsFrameDPSScale);

	HHH_OptionsFrameRedemptionScale:SetValue(HHH_OptionsFrameRedemptionScale.value);
	HHH_OptionsFrameRedemptionScaleText:SetText(HHH_OptionsFrameRedemptionScale.value);
	HHH_SliderEnableDisable(HHH_OptionsFrameRedemptionCheckButton.value, HHH_OptionsFrameRedemptionScale);

	-- Set drop down
	UIDropDownMenu_SetSelectedValue(HHH_OptionsFrameTooltipDropDown, HHH_OptionsFrameTooltipDropDown.value);

end


---------------
-- Okay button
---------------
function HHH_OptionsFrame_Okay()
-- Set Check Button values
	HHH_OptionsFrameBlessingCheckButton.currValue = HHH_OptionsFrameBlessingCheckButton.value;
	HHH_OptionsFrameSpecCheckButton.currValue = HHH_OptionsFrameSpecCheckButton.value;
	HHH_OptionsFrameSealCheckButton.currValue = HHH_OptionsFrameSealCheckButton.value;
	HHH_OptionsFrameDPSCheckButton.currValue = HHH_OptionsFrameDPSCheckButton.value;
	HHH_OptionsFrameRedemptionCheckButton.currValue = HHH_OptionsFrameRedemptionCheckButton.value;
	HHH_OptionsFrameLockCheckButton.currValue = HHH_OptionsFrameLockCheckButton.value;

-- Set Scale slider values
	HHH_Config.BlessingScale = HHH_OptionsFrameBlessingScale.value;
	HHH_OptionsFrameBlessingScale.currValue = HHH_OptionsFrameBlessingScale.value;
	
	HHH_Config.SpecScale = HHH_OptionsFrameSpecScale.value;
	HHH_OptionsFrameSpecScale.currValue = HHH_OptionsFrameSpecScale.value;
	
	HHH_Config.SealScale = HHH_OptionsFrameSealScale.value;
	HHH_OptionsFrameSealScale.currValue = HHH_OptionsFrameSealScale.value;

	HHH_Config.DPSScale = HHH_OptionsFrameDPSScale.value;
	HHH_OptionsFrameDPSScale.currValue = HHH_OptionsFrameDPSScale.value;
	
	HHH_Config.RedemptionScale = HHH_OptionsFrameRedemptionScale.value;
	HHH_OptionsFrameRedemptionScale.currValue = HHH_OptionsFrameRedemptionScale.value;

-- Set tooltip dropdown value
	HHH_OptionsFrameTooltipDropDown.currValue = HHH_OptionsFrameTooltipDropDown.value;

end

-----------------
-- Cancel Button
-----------------
function HHH_OptionsFrame_Cancel()

-- Set Check Button values
	HHH_OptionsFrameBlessingCheckButton_OnClick(HHH_OptionsFrameBlessingCheckButton.currValue);
	HHH_OptionsFrameSpecCheckButton_OnClick(HHH_OptionsFrameSpecCheckButton.currValue);
	HHH_OptionsFrameSealCheckButton_OnClick(HHH_OptionsFrameSealCheckButton.currValue);
	HHH_OptionsFrameDPSCheckButton_OnClick(HHH_OptionsFrameDPSCheckButton.currValue);
	HHH_OptionsFrameRedemptionCheckButton_OnClick(HHH_OptionsFrameRedemptionCheckButton.currValue);
	HHH_OptionsFrameLockCheckButton_OnClick(HHH_OptionsFrameLockCheckButton.currValue);

-- Set Scale slider values
	HHH_Config.BlessingScale = HHH_OptionsFrameBlessingScale.currValue;
	HHH_OptionsFrameBlessingScale.value = HHH_OptionsFrameBlessingScale.currValue;
	HHH.ScaleButton(HHH.blessCenter, HHH_OptionsFrameBlessingScale.currValue);
	
	HHH_Config.SpecScale = HHH_OptionsFrameSpecScale.currValue;
	HHH_OptionsFrameSpecScale.value = HHH_OptionsFrameSpecScale.currValue;
	HHH.ScaleButton(HHH.specCenter, HHH_OptionsFrameSpecScale.currValue);

	HHH_Config.SealScale = HHH_OptionsFrameSealScale.currValue;
	HHH_OptionsFrameSealScale.value = HHH_OptionsFrameSealScale.currValue;
	HHH.ScaleButton(HHH.sealCenter, HHH_OptionsFrameSealScale.currValue);
	
	HHH_Config.DPSScale = HHH_OptionsFrameDPSScale.currValue;
	HHH_OptionsFrameDPSScale.value = HHH_OptionsFrameDPSScale.currValue;
	HHH.ScaleButton(HHH.dpsCenter, HHH_OptionsFrameDPSScale.currValue);
		
	HHH_Config.RedemptionScale = HHH_OptionsFrameRedemptionScale.currValue;
	HHH_OptionsFrameRedemptionScale.value = HHH_OptionsFrameRedemptionScale.currValue;
	HHH.ScaleButton(HHH.redemptionBtn, HHH_OptionsFrameRedemptionScale.currValue);

-- Set tooltip dropdown value
	HHH_Config.Tooltip = HHH_OptionsFrameTooltipDropDown.currValue;
	HHH_OptionsFrameTooltipDropDown.value = HHH_OptionsFrameTooltipDropDown.currValue;

end

------------------
-- Default Button
------------------
function HHH_OptionsFrame_Default()
	
	--HHH_Config.BlessState = 1;
	--HHH_Config.SpecState = 1;
	HHH_Config.BlessingScale = 100;
	HHH_Config.SealScale = 100;
	HHH_Config.DPSScale = 100;
	HHH_Config.SpecScale = 100;
	HHH_Config.RedemptionScale = 50;
	HHH_Config.BlessingToggle = 1;
	HHH_Config.SealToggle = 1;
	HHH_Config.SpecToggle = 1;
	HHH_Config.DPSToggle = 1;
	HHH_Config.RedemptionToggle = 1;
	HHH_Config.Lock = 0;
	HHH_Config.Tooltip = 2;

	HHH_OptionsFrame_OnVariableLoad();
	
	HHH.blessCenter:Show();
	HHH.specCenter:Show();
	HHH.sealCenter:Show();
	HHH.dpsCenter:Show();

	HHH.redemptionBtn:Hide();

	HHH.ScaleButton(HHH.blessCenter, HHH_Config.BlessingScale);
	HHH.ScaleButton(HHH.specCenter, HHH_Config.SpecScale);	
	HHH.ScaleButton(HHH.sealCenter, HHH_Config.SealScale);
	HHH.ScaleButton(HHH.dpsCenter, HHH_Config.DPSScale);
	HHH.ScaleButton(HHH.redemptionBtn, HHH_Config.RedemptionScale);
	
end

--------------------------
-- Blessings check button
--------------------------
function HHH_OptionsFrameBlessingCheckButton_OnClick(checked)
	local check;
	if (checked == 1) then
		HHH.blessCenter:Show();
		check = 1;
	else
		HHH.blessCenter:Hide();
		check = 0;
	end
	
	HHH_Config.BlessingToggle = check;
	HHH_OptionsFrameBlessingCheckButton.value = check;
	HHH_SliderEnableDisable(check, HHH_OptionsFrameBlessingScale);
end

---------------------
-- Spec check button
---------------------
function HHH_OptionsFrameSpecCheckButton_OnClick(checked)
	local check;
	if (checked == 1) then
		HHH.specCenter:Show();
		check = 1;
	else
		HHH.specCenter:Hide();
		check = 0;
	end

	HHH_Config.SpecToggle = check;
	HHH_OptionsFrameSpecCheckButton.value = check;
	HHH_SliderEnableDisable(check, HHH_OptionsFrameSpecScale);
end

---------------------
-- Seal check button
---------------------
function HHH_OptionsFrameSealCheckButton_OnClick(checked)
	local check;
	if (checked == 1) then
		HHH.sealCenter:Show();
		check = 1;
	else
		HHH.sealCenter:Hide();
		check = 0;
	end

	HHH_Config.SealToggle = check;
	HHH_OptionsFrameSealCheckButton.value = check;

	HHH_SliderEnableDisable(check, HHH_OptionsFrameSealScale);
end

---------------------
-- DPS check button
---------------------
function HHH_OptionsFrameDPSCheckButton_OnClick(checked)
	local check;
	if (checked == 1) then
		HHH.dpsCenter:Show();
		check = 1;
	else
		HHH.dpsCenter:Hide();
		check = 0;
	end

	HHH_Config.DPSToggle = check;
	HHH_OptionsFrameDPSCheckButton.value = check;

	HHH_SliderEnableDisable(check, HHH_OptionsFrameDPSScale);
end

---------------------------
-- Redemption check button
---------------------------
function HHH_OptionsFrameRedemptionCheckButton_OnClick(checked)
	local check;
	if (checked == 1) then
		RegisterStateDriver(HHH_RedemptionButton, "dead", "[dead, help, nocombat] true; false");
		check = 1;
	else
		UnregisterStateDriver(HHH_RedemptionButton, "dead");
		HHH.redemptionBtn:Hide();
		check = 0;
	end

	HHH_Config.RedemptionToggle = check;
	HHH_OptionsFrameRedemptionCheckButton.value = check;
	HHH_SliderEnableDisable(check, HHH_OptionsFrameRedemptionScale);
end

---------------------
-- Lock Check Button
---------------------
function HHH_OptionsFrameLockCheckButton_OnClick(checked)
	local check;
	if (checked == 1) then
		check = 1;
	else
		check = 0;
	end
	HHH_Config.Lock = check;
	HHH_OptionsFrameLockCheckButton.value = check;
end

---------------------------
-- Enable / Disable slider
---------------------------
function HHH_SliderEnableDisable(checked, slider)
	
	if (checked) then
		--OptionsFrame_EnableSlider(slider);
		local name = slider:GetName();
		getglobal(name.."Thumb"):Show();
		getglobal(name.."Text"):SetVertexColor(NORMAL_FONT_COLOR.r , NORMAL_FONT_COLOR.g , NORMAL_FONT_COLOR.b);
		getglobal(name.."Low"):SetVertexColor(HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b);
		getglobal(name.."High"):SetVertexColor(HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b);
	else
		--OptionsFrame_DisableSlider(slider);
		local name = slider:GetName();
		getglobal(name.."Thumb"):Hide();
		getglobal(name.."Text"):SetVertexColor(GRAY_FONT_COLOR.r, GRAY_FONT_COLOR.g, GRAY_FONT_COLOR.b);
		getglobal(name.."Low"):SetVertexColor(GRAY_FONT_COLOR.r, GRAY_FONT_COLOR.g, GRAY_FONT_COLOR.b);
		getglobal(name.."High"):SetVertexColor(GRAY_FONT_COLOR.r, GRAY_FONT_COLOR.g, GRAY_FONT_COLOR.b);
	end
	
	slider:EnableMouse(checked);

end

-----------------
-- Slider change
-----------------
function HHH_OptionsFrameScale_OnValueChanged(self, scaleFrame)
	getglobal(self:GetName().."Text"):SetText(self:GetValue().."%");
	HHH.ScaleButton(scaleFrame, self:GetValue());

	self.value = self:GetValue();
end

----------------------
-- Tooltips Drop Down
----------------------

function HHH_OptionsFrameTooltipDropDown_OnLoad()
	UIDropDownMenu_Initialize(this, HHH_OptionsFrameTooltipDropDown_Initialize);
	UIDropDownMenu_SetSelectedValue(this, HHH_Config.Tooltip);

	HHH_OptionsFrameTooltipDropDown.tooltip = this.value;
	UIDropDownMenu_SetWidth(HHH_OptionsFrameTooltipDropDown, 90);
	this.SetValue = 
		function (self, value) 
			UIDropDownMenu_SetSelectedValue(self, value);
			HHH_Config.Tooltip = value;
			HHH_OptionsFrameTooltipDropDown.tooltip = value;
		end;
	this.GetValue =
		function (self)
			return UIDropDownMenu_GetSelectedValue(self);
		end;
end

function HHH_OptionsFrameTooltipDropDown_OnClick()
	UIDropDownMenu_SetSelectedValue(HHH_OptionsFrameTooltipDropDown, this.value);
	HHH_OptionsFrameTooltipDropDown.tooltip = this.value;
	HHH_OptionsFrameTooltipDropDown.value = this.value;
	HHH_Config.Tooltip = this.value;
end

function HHH_OptionsFrameTooltipDropDown_Initialize()
	local selectedValue = UIDropDownMenu_GetSelectedValue(HHH_OptionsFrameTooltipDropDown);
	local info = UIDropDownMenu_CreateInfo();

	-- Tooltips Off --
	info.text = HHH_MENU.Off;
	info.func = HHH_OptionsFrameTooltipDropDown_OnClick;
	info.value = 0;
	if ( info.value == selectedValue ) then
		info.checked = 1;
	else
		info.checked = nil;
	end
	info.tooltipTitle = HHH_MENU.Off;
	info.tooltipText = HHH_MENU.Off;
	UIDropDownMenu_AddButton(info);

	-- Tooltips Partial --
	info.text = HHH_MENU.Partial;
	info.func = HHH_OptionsFrameTooltipDropDown_OnClick;
	info.value = 1;
	if ( info.value == selectedValue ) then
		info.checked = 1;
	else
		info.checked = nil;
	end
	info.tooltipTitle = HHH_MENU.Partial;
	info.tooltipText = HHH_MENU.Partial;
	UIDropDownMenu_AddButton(info);

	-- Tooltips Full --
	info.text = HHH_MENU.Total;
	info.func = HHH_OptionsFrameTooltipDropDown_OnClick;
	info.value = 2;
	if ( info.value == selectedValue ) then
		info.checked = 1;
	else
		info.checked = nil;
	end
	info.tooltipTitle = HHH_MENU.Total;
	info.tooltipText = HHH_MENU.Total;
	UIDropDownMenu_AddButton(info);

end

