------------------------
-- Hadar's Holy Hope
-- English localization
------------------------

if ( GetLocale() == "enUS" ) or ( GetLocale() == "enGB" ) then

HHH_OPTIONS_TITLE = "Hadar's Holy Hope ("..GetAddOnMetadata("Hadar_HolyHope", "Version")..")";
HHH_OPTIONS_SUBTEXT = "These options control the properties of Hadar's Holy Hope Add On.";

HHH_SPELL_TABLE = {
	
	might = "Blessing of Might",
	wisdom = "Blessing of Wisdom",
	kings = "Blessing of Kings",
	sanctuary = "Blessing of Sanctuary",
	
	salvation = "Hand of Salvation",
	freedom = "Hand of Freedom",
	sacrifice = "Hand of Sacrifice",
	protection = "Hand of Protection",		

	gmight = "Greater Blessing of Might",
	gwisdom = "Greater Blessing of Wisdom",
	gkings = "Greater Blessing of Kings",
	gsanctuary = "Greater Blessing of Sanctuary",
	
	sojustice = "Seal of Justice",
	solight = "Seal of Light",
	sowisdom = "Seal of Wisdom",
	sorighteousness = "Seal of Righteousness",
	somartyr = "Seal of the Martyr",
	soblood = "Seal of Blood",
	sovengeance = "Seal of Vengeance",
	socorruption = "Seal of Corruption",
	socommand = "Seal of Command",

	hammerofwrath =	"Hammer of Wrath",
	redemption = "Redemption",
	divineintervention = "Divine Intervention",
	hammerofjustice = "Hammer of Justice",
	avengingwrath = "Avenging Wrath",
	divineprotection = "Divine Protection",
	divineshield = "Divine Shield",
	righteousdefense = "Righteous Defense",
	cleanse = "Cleanse",
	consecration = "Consecration",
	holywrath = "Holy Wrath",
	exorcism = "Exorcism",
	divineplea = "Divine Plea",
	sacredshield = "Sacred Shield",

	repentance = "Repentance",
	crusaderstrike = "Crusader Strike",
	divinestorm = "Divine Storm",

	divinefavor = "Divine Favor",
	holyshock = "Holy Shock",
	divineillumination = "Divine Illumination",
	beaconoflight = "Beacon of Light",

	holyshield = "Holy Shield",
	avengersshield = "Avenger's Shield",
	shieldofrighteousness = "Shield of Righteousness",
	hammeroftherighteous = "Hammer of the Righteous",
	
	jolight = "Judgement of Light",
	jowisdom = "Judgement of Wisdom",
	jojustice = "Judgement of Justice"	
};

HHH_MACRO_TABLE = {
	righteousdefense = "/cast [help] Righteous Defense; [target=targettarget, help] Righteous Defense; Righteous Defense;",
};

HHH_ITEM = {
	["Kings"] = "Symbol of Kings",
	["Hearthstone"] = "Hearthstone",
};

HHH_MENU = {
	["Length"] = "Length",
	["Active"] = "Active",
	["Lock"] = "Frame Lock",
	["UnlockText"] = "Frames are now Unlocked",
	["LockText"] = "Frames are now Locked",
	["Tooltips"] = "Tooltips",
	["Blessing"] = "Blessing",
	["Seal"] = "Seal",
	["Mount"] = "Mount",
	["Off"] = "Off",
	["Partial"] = "Partial",
	["Total"] = "Full",  
	["Spec"] = "Spec",
	["Redemption"] = "Redemption",
	["Show"] = "Show",
	["DPS"] = "DPS"
};

end
