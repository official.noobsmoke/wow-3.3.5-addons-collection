## Interface: 30000
## Title: Hadar's Holy Hope
## Version: 2.0 (beta 1)
## Notes: Remade version of the original "Holy Hope"
## Author: Hadar <Sunder> - Khaz Modan
## SavedVariablesPerCharacter: HHH_Config, HHH_CIRCLE_LOC

Localization-EN.lua
Localization-FR.lua
Localization-DE.lua
Localization-RU.lua

Hadar_HolyHope_Spell_Defs.lua

Hadar_HolyHope.xml
Hadar_HolyHope.lua

Hadar_HolyHope_Options.xml