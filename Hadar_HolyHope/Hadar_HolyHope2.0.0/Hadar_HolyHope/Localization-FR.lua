------------------------
-- Hadar's Holy Hope
-- Frensh localization
------------------------

if ( GetLocale() == "frFR" ) then

HHH_OPTIONS_TITLE = "Hadar's Holy Hope ("..GetAddOnMetadata("Hadar_HolyHope", "Version")..")";
HHH_OPTIONS_SUBTEXT = "Ces options controlent les propri\195\169t\195\169s de l'addon Hadar's Holy Hope.";

HHH_SPELL_TABLE = {

	might = "B\195\169n\195\169diction de puissance",
	wisdom = "B\195\169n\195\169diction de sagesse",
	kings = "B\195\169n\195\169diction des rois",
	sanctuary = "B\195\169n\195\169diction du sanctuaire",
	
	salvation = "Main de salut",
	freedom = "Main de libert\195\169",
	sacrifice = "Main de sacrifice",
	protection = "Main de protection",
	
	gmight = "B\195\169n\195\169diction de puissance sup\195\169rieure",
	gwisdom = "B\195\169n\195\169diction de sagesse sup\195\169rieure",
	gkings = "B\195\169n\195\169diction des rois sup\195\169rieure",
	gsanctuary = "B\195\169n\195\169diction du sanctuaire sup\195\169rieure",
	
	sojustice = "Sceau de justice",
	solight = "Sceau de lumi\195\168re",
	sowisdom = "Sceau de sagesse",
	sorighteousness = "Sceau de pi\195\169t\195\169",
	somartyr = "Sceau du martyr",
	soblood = "Sceau de sang",
	sovengeance = "Sceau de vengeance",
	socorruption = "Sceau de corruption",
	socommand = "Sceau d'autorit\195\169",
	
	hammerofjustice = "Marteau de la justice",
	avengingwrath = "Courroux vengeur",
	divineshield = "Bouclier divin",
	righteousdefense = "D\195\169fense vertueuse",
	cleanse = "Epuration",
	holywrath = "Fureur vertueuse",
	exorcism = "Exorcisme",

	divinestorm = "Temp\195\170te divine",
	crusaderstrike = "Inquisition",
	repentance = "Repentir",

	holyshock = "Horion sacr\195\169",
	divinefavor = "Faveur divine",

	holyshield = "Bouclier sacr\195\169",
	avengersshield = "Bouclier vengeur",

	divineillumination = "Illumination divine",

	hammerofwrath = "Marteau de courroux",
	redemption = "R\195\169demption",
	divineintervention = "Intervention divine",
	
	consecration = "Cons\195\169cration",
	divinestorm = "Temp\195\170te divine",

	jolight = "Jugement de lumi\195\168re",
	jowisdom = "Jugement de sagesse",
	jojustice = "Jugement de justice",

	sacredshield = "Bouclier sacr\195\169",
	divineplea = "Supplique divine",
	hammeroftherighteous = "Marteau du vertueux",
	shieldofrighteousness = "Bouclier de pi\195\169t\195\169",
	divineprotection = "Protection divine",
	beaconoflight = "Guide de lumi\195\168re"
};

HHH_MACRO_TABLE = {
	righteousdefense = "/cast [help] D\195\169fense vertueuse; [target=targettarget, help] D\195\169fense vertueuse; D\195\169fense vertueuse;",
};

HHH_ITEM = {
	["Kings"] = "Symbole des rois",
	["Hearthstone"] = "Pierre de foyer",
};

HHH_MENU = {
	["Length"] = "Longueur",
	["Active"] = "Actif",
	["Lock"] = "Bloquer",
	["UnlockText"] = "D\195\169bloqu\195\169s",
	["LockText"] = "Bloqu\195\169s",
	["Tooltips"] = "Informations",
	["Blessing"] = "B\195\169n\195\169dictions",
	["Seal"] = "Sceau",
	["Mount"] = "Montures",
	["Off"] = "D\195\169sactiv\195\169es",
	["Partial"] = "Partielles",
	["Total"] = "Compl\195\168tes",
	["Spec"] = "Sp\195\169",
	["Redemption"] = "R\195\169demption",
	["Show"] = "Montrer",
	["DPS"] = "DPS"
};

end 