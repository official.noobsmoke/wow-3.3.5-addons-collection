﻿------------------------
-- Hadar's Holy Hope
-- Russian localization
------------------------
-- translate by Dannte of Gordunni, Aliance
------------------------

if ( GetLocale() == "ruRU" ) then

HHH_OPTIONS_TITLE = "Hadar's Holy Hope ("..GetAddOnMetadata("Hadar_HolyHope", "Version")..")";
HHH_OPTIONS_SUBTEXT = "Настройки аддона. Перевел аддон Даннтэ , Сервер Горднни , Альянс.";

HHH_SPELL_TABLE = {

might = "благословение могущества",
wisdom = "благословение мудрости",
kings = "благословение королей",
sanctuary = "благословение неприкосновенности",

salvation = "длань спасения",
freedom = "длань свободы",
sacrifice = "длань жертвенности",
protection = "Длань Защиты",

gmight = "великое благословение могущества",
gwisdom = "великое благословение мудрости",
gkings = "великое благословение королей",
gsanctuary = "великое благословение оберега ",

sojustice = "печать справедливости",
solight = "печать света",
sowisdom = "Печать мудрости",
sorighteousness = "печать праведности",
somartyr = "Печать Мученика",
soblood = "печать крови",
sovengeance = "печать мщения",
socorruption = "Печать коррупцией",
socommand = "печать повиновения",

hammerofjustice = "молот правосудия",
avengingwrath = "гнев карателя",
divineshield = "Божественный щит",
righteousdefense = "праведная защита",
cleanse = "очищение",
holywrath = "гнев небес",
exorcism = "экзорцизм",

divinestorm = "Божественная буря",
crusaderstrike = "удар воина света",
repentance = "покаяние",

holyshock = "шок небес",
divinefavor = "божественное одобрение",

holyshield = "Щит небес",
avengersshield = "щит мстителя",

divineillumination = "божественное просветление",

hammerofwrath = "молот гнева",
redemption = "искупление",
divineintervention = "божественное вмешательство",

consecration = "освящение",
divinestorm = "божественная буря",

jolight = "правосудие света",
jowisdom = "правосудие мудрости",
jojustice = "правосудие справедливости",

sacredshield = "Sacred Shield",
divineplea = "Divine Plea",
hammeroftherighteous = "Молот праведника",
shieldofrighteousness = "Щит праведности",
divineprotection = "божественная защита",
beaconoflight = "Частица света"


};

HHH_MACRO_TABLE = {
righteousdefense = "/cast [help] праведная защита; [target=targettarget, help] праведная защита; праведная защита;",
};

HHH_ITEM = {
["Kings"] = "Знак королей",
["Hearthstone"] = "Камень возвращения",
};

HHH_MENU = {
["Length"] = "Длина",
["Active"] = "активно",
["Lock"] = "закрепить",
["UnlockText"] = "закреплено",
["LockText"] = "откреплено",
["Tooltips"] = "подсказки",
["Blessing"] = "благославления",
["Seal"] = "печати",
["Mount"] = "верховое животное",
["Off"] = "выкл",
["Partial"] = "частично",
["Total"] = "все",
["Spec"] = "спец",
["Redemption"] = "искупление",
["Show"] = "показать",
["DPS"] = "ДПС"
};

end