IRT_Lib = {}
IRT_Lib.PlayerName = UnitName("player")


CLASS_COLORS = {
    ["Hunter"] = {0.67, 0.83, 0.45, "FFABD473"},
    ["Warlock"] = {0.58, 0.51, 0.79, "FF9482C9"},
    ["Priest"] = {1.0, 1.0, 1.0, "FFFFFFFF"},
    ["Paladin"] = {0.96, 0.55, 0.73, "FFF58CBA"},
    ["Mage"] = {0.41, 0.8, 0.94, "FF69CCF0"},
    ["Rogue"] = {1.0, 0.96, 0.41, "FFFFF569"},
    ["Druid"] = {1.0, 0.49, 0.04, "FFff7D0A"},
    ["Shaman"] = {0.0, 0.44, 0.87, "FF0070DE"},
    ["Warrior"] = {0.78, 0.61, 0.43, "FFC79C6E"},
    ["Death Knight"] = {0.77, 0.12 , 0.23, "FFC41F3B"},
}

RAID_ICON = {
    [1] = "|TInterface\\TargetingFrame\\UI-RaidTargetingIcon_1:0|t", -- star
    [2] = "|TInterface\\TargetingFrame\\UI-RaidTargetingIcon_2:0|t", -- circle
    [3] = "|TInterface\\TargetingFrame\\UI-RaidTargetingIcon_3:0|t", -- diamond
    [4] = "|TInterface\\TargetingFrame\\UI-RaidTargetingIcon_4:0|t", -- triangle
    [5] = "|TInterface\\TargetingFrame\\UI-RaidTargetingIcon_5:0|t", -- moon
    [6] = "|TInterface\\TargetingFrame\\UI-RaidTargetingIcon_6:0|t", -- square
    [7] = "|TInterface\\TargetingFrame\\UI-RaidTargetingIcon_7:0|t", -- cross
    [8] = "|TInterface\\TargetingFrame\\UI-RaidTargetingIcon_8:0|t" -- skull
}

function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function contains(array, element)
    for _, value in pairs(array) do
        if value == element then
            return true
        end
    end
    return false
end

function containsElement(array, element)
    for _, value in pairs(array) do
        if value == element then
            return value
        end
    end
    return false
end
     
function playerInRaidOrParty()
    if GetNumRaidMembers() > 0 then
        return true, "raid"
    elseif GetNumPartyMembers() > 0 then
        return true, "party"
    end
end

function raidIbyName(sname)
    for i=1, GetNumRaidMembers() do
        if UnitName("raid"..i) == sname then
            return "raid"..i, i
        end  
    end
    return false
end 

function partyIbyName(sname)
    if GetNumPartyMembers() > 0 then
        for i=1, GetNumPartyMembers() do
            if UnitName("party"..i) == sname then
                return "party"..i, i
            end  
        end
    end

    if UnitName("player") == sname then
        return "player"
    end

    return false
end

function arrayLength(array)
    local length = 0
    for i in pairs(array) do
        length = length + 1
    end
    return length
end
    
function getPosition(array, element)
    for i = 1, table.getn(array) do
        if array[i] == element then
            return i
        end
    end
end

function ifContains(array)
    for _, value in pairs(array) do
        if value ~= nil then return true end
    end
    return false
end 

function setClassColor(str, class)
    cString = "|c".. CLASS_COLORS[class][4] .. str .. "|r"
    return cString
end

function setDBMPullTimer(timer)
    local pullIn = timer .. "\tPull in"
    if IsRaidOfficer() then
        SendAddonMessage("DBMv4-Pizza", pullIn, "RAID", UnitName("player"))
    end
end 
            
function getHP(sname)
    if UnitInRaid("player") ~= nil then
        for i=1, GetNumRaidMembers() do
            if UnitName("raid"..i) == sname then
                return string.format("%.2f", 100*UnitHealth("raid"..i)/UnitHealthMax("raid"..i))
            end
        end 
    elseif UnitName("party1") then
        for i=1, 5 do
            if UnitName("party"..i) == sname then
                return string.format("%.2f", 100*UnitHealth("party"..i)/UnitHealthMax("party"..i))
            end
        end 
    elseif UnitName("player") == sname then
        return string.format("%.2f", 100*UnitHealth("player")/UnitHealthMax("player"))
    end     
end     
            
function getUnitMana(sname)
    if UnitInRaid("player") then
        for i=1, GetNumRaidMembers() do
            if UnitName("raid"..i) == sname then
                return string.format("%.2f", 100*UnitPower("raid"..i, 0)/UnitPowerMax("raid"..i, 0))
            end
        end 
    elseif UnitName("party1") then
        for i=1, 5 do
            if UnitName("party"..i) == sname then
                if UnitPower("party"..i, 0) then
                    return string.format("%.2f", 100*UnitPower("party"..i, 0)/UnitPowerMax("party"..i, 0))
                else 
                    return "unknown"
                end
            end
        end 
    elseif UnitName("player") == sname then
        return string.format("%.2f", 100*UnitPower("player", 0)/UnitPowerMax("player", 0))
    end
end
            
function playerInPartyRaid()
    local result
    
    if UnitInRaid("player") then
        result = "raid"
    elseif UnitName("party1") then
        result = "party"
    else
        result = "solo"
    end
    
    return result
end
            
function setClassColor(str, class)
    cString = "|c".. CLASS_COLORS[class][4] .. str .. "|r"
    return cString
end

function elePosInArray(array, element)
    if array and element then
        local position = 1
        for i in pairs(array) do
            if i == element then
                return position
            end
            position = position + 1
        end
    end
end

--------------------------------------
-- [Global] Hand of Sacrifice Order --
--------------------------------------

function isAvailable(array, nickName)
    if array then
        if array[nickName] then
            if GetTime() > array[nickName][2] then
                return true
            end
        end
    end
end