local addonName, addonTable = ...
local IRT_UT
local ifRaidLeader
local PlayerName = UnitName("player")

-- IRT_PullTracker v1.0 --
-- Author: Merfin @ Warmane --
-- Creation: 12/18/2019 --
-- To see more custom AddOns or WeakAurases go to https://discord.gg/8VypAzV --

local SUB_EVENTS = {
    "SPELL_DAMAGE", 
    "SPELL_MISSED", 
    "SPELL_DISPEL", 
    "SPELL_DISPEL_FAILED", 
    "SPELL_STOLEN", 
    "SPELL_AURA_APPLIED_DOSE", 
    "SPELL_AURA_REFRESH", 
    "RANGE_DAMAGE", 
    "RANGE_MISSED", 
    "SWING_DAMAGE", 
    "SWING_MISS",
    "SPELL_CAST_SUCCESS",
}
        
local BLACK_LIST = {
    "Soothe Animal", 
    "Hunter's Mark", 
    "Sap", 
    "Mind Vision", 
    "Baby Spice",
}
        
local BOSSES = {
    "Anub'arak", 
    "Lord Marrowgar", 
    "Lady Deathwhisper", 
    "Festergut", 
    "Rotface", 
    "Professor Putricide", 
    "Blood-Queen Lana'thel", 
    "Halion", 
    "Saviana Ragefire", 
    "Baltharus the Warborn", 
    "General Zarithrian",
}

local HITBOX = {
    ["Lord Marrowgar"] = {18.05, 20.08, 21.75}, 
    ["Lady Deathwhisper"] = {7.405},
    ["Rotface"] = {9.499145},
    ["Festergut"] = {9.499145},
    ["Blood-Queen Lana'thel"] = {8.615},
    ["Baltharus the Warborn"] = {14.99},
    ["Saviana Ragefire"] = {7.77},
    ["General Zarithrian"] = {13.74},
    ["Halion"] = {19.495},
    ["Fjola Lightbane"] = {10.494},
    ["Fjola Lightbane"] = {10.494},
    ["Anub'arak"] = {11.496},
    ["Professor Putricide"] = {9.29}
}
        
-- Engage Bosses quotes 
local ENGAGES = {
    ["Lord Marrowgar"] = "The Scourge will wash over this world as a swarm of death and destruction!",
    ["Lady Deathwhisper"] = "What is this disturbance? You dare trespass upon this hallowed ground? This shall be your final resting place!",
    ["Deathbringer Saurfang"] = "BY THE MIGHT OF THE LICH KING!",
    ["Festergut"] = "Fun time?",
    ["Rotface"] = "WEEEEEE!",
    ["Professor Putricide"] = "Good news, everyone! I think I've perfected a plague that will destroy all life on Azeroth!",
    ["Blood-Queen Lana'thel"] = "You have made an... unwise... decision.",
    ["Saviana Ragefire"] = "You will sssuffer for this intrusion!",
    ["Baltharus the Warborn"] = "Ah, the entertainment has arrived.",
    ["General Zarithrian"] = "Alexstrasza has chosen capable allies.... A pity that I must END YOU!",
    ["Halion"] = "Your world teeters on the brink of annihilation. You will ALL bear witness to the coming of a new age of DESTRUCTION!",
    ["Anub'arak"] = "This place will serve as your tomb!"
}
        
-- Bosses Coordinates plus CurrentMapSizes {x, y, width, height} 
local BOSS_COORDINATES = {
    ["Lord Marrowgar"] = {0.390020, 0.597774, 1355.47009278, 903.647033691},
    ["Lady Deathwhisper"] = {0.389835, 0.855974, 1355.47009278, 903.647033691},
    ["Festergut"] = {0.198078, 0.653300, 1148.73999024, 765.82006836},
    ["Rotface"] = {0.198034, 0.420934, 1148.73999024, 765.82006836},
    ["Professor Putricide"] = {0.088706, 0.538038, 1148.73999024, 765.82006836},
    ["Blood-Queen Lana'thel"] = {0.513687, 0.321118, 373.70996094, 249.12988281},
    ["Saviana Ragefire"] = {0.352662, 0.553248, 752.083, 502.09},
    ["Baltharus the Warborn"] = {0.681570, 0.549922, 752.083, 502.09},
    ["General Zarithrian"] = {0.497230, 0.755784, 752.083, 502.09},
    ["Halion"] = {0.489670, 0.544066, 752.083, 502.09},
    ["Fjola Lightbane"] = {0.429019, 0.435903, 369.9861869814, 246.657989502},
    ["Eydis Darkbane"] = {0.429019, 0.598071, 369.9861869814, 246.657989502},
    ["Anub'arak"] = {0.534214, 0.355, 739.996017456, 493.33001709},
}

local function bossAlive(bossName)
    for i=1, GetNumRaidMembers() do
        local unitID = "raid"..i.."target"
        if UnitName(unitID) == bossName then
            if UnitAffectingCombat(unitID) then return true end
            if UnitExists(unitID) then
                if UnitHealth(unitID)*100/UnitHealthMax(unitID) > 0 then
                    return true
                end
            end
        end  
    end
    return false
end

local function isUnitRelevant(i)
    local _, _, _, _, _, _, zone, online = GetRaidRosterInfo(i);
    local ZoneName = GetRealZoneText();
    if zone == ZoneName and online == 1 and not UnitIsDead("raid"..i) then
        return true
    else
        return false
    end
end

local function isPet(pname) 
    for i=1, GetNumRaidMembers() do
        if isUnitRelevant(i) then
            if UnitName("raidpet"..i) == pname then
                return true, UnitName("raid"..i)
            end 
        end 
    end
    return false
end

local function distanceInYards(bname, unitX, unitY)
    local x2, y2, width, height = BOSS_COORDINATES[bname][1], BOSS_COORDINATES[bname][2], BOSS_COORDINATES[bname][3], BOSS_COORDINATES[bname][4]
    local posX, posY = unitX, unitY
    
    dX = (x2 - posX) * width
    dY = (y2 - posY) * height
    return math.sqrt(dX * dX + dY * dY)
end

local function calculateAngle(bname, unitID)
    local posX, posY = GetPlayerMapPosition(unitID)
    local orientationX, orientationY = BOSS_COORDINATES[bname][1] - posX, BOSS_COORDINATES[bname][2] - posY
    local a, b = math.abs(orientationX), math.abs(orientationY) 
    local angle = math.deg(math.atan(a/b))
    return orientationX, orientationY, angle
end

local function Distance_w_HitBox(bname, distanceYards, angle, orientationX, orientationY)
    if bname == "Lord Marrowgar" then
        if (distanceYards < 23.10) then
            return distanceYards - HITBOX[bname][1]
        elseif (distanceYards < 29.8) then
            return distanceYards - HITBOX[bname][2]
        else
            return distanceYards - HITBOX[bname][3]
        end
    elseif bname == "Saviana Ragefire" then
        hitbox = 0
        if (orientationX < 0 and orientationY > 0)  then
            if (angle < 15) then
                hitbox = 7.799
            elseif (angle < 35) then
                hitbox = 7.81
            elseif (angle > 35 and angle < 60) then
                hitbox = 7.78
            elseif (angle < 65) then
                hitbox = 7.63
            else
                hitbox = 7.65
            end
        elseif (orientationX > 0 and orientationY > 0) then
            if (angle < 25 ) then
                hitbox = 7.799
            elseif (angle < 35) then
                hitbox = 7.81
            elseif (angle < 35) then
                hitbox = 7.71
            elseif (angle < 65) then
                hitbox = 7.63
            else
                hitbox = 7.612
            end
        elseif (orientationX < 0 and orientationY < 0)  then
            if (angle < 25) then
                hitbox = 7.80
            elseif (angle < 50) then
                hitbox = 7.76
            elseif (angle < 65) then
                hitbox = 7.72
            else
                hitbox = 7.65
            end
        elseif (orientationX > 0 and orientationY < 0) then
            if (angle < 18) then
                hitbox = 7.79
            elseif (angle < 25) then
                hitbox = 7.71
            else
                hitbox = 7.54
            end
        end
        return distanceYards - hitbox
    else
        return distanceYards - HITBOX[bname][1]
    end
    -- don't forget to combine them!
end

local function Unit_Proximity(bname, tableCoord)

    local unitName, minDistance = "", 0
    if bname ~= "Lady Deathwhisper" then
        for i, value in pairs(tableCoord) do
            unitID = i
            local distance, class = distanceInYards(bname, tableCoord[i][1], tableCoord[i][2]), UnitClass(unitID)
            local orX, orY, angle = calculateAngle(bname, unitID)
            distance = math.abs(Distance_w_HitBox(bname, distance, angle, orX, orY))
            -- Agro radius is decreased on Rogues with Stealth, so i just ignore it
            if class == "Rogue" and UnitBuff(unitID, "Stealth") then
                if minDistance == 0 then
                    minDistance = 100
                    distance = minDistance + 1
                else
                    distance = minDistance + 1
                end
            end
                
            if minDistance == 0 then
                minDistance = distance
                unitName = UnitName(unitID)
            elseif distance < minDistance then
                minDistance = distance
                unitName = UnitName(unitID)
            end
        end
    -- LDW room has few pillars and you can hide behind those even if range is less than 10 yards
    -- that's why we have separate statement for that
    elseif bname == "Lady Deathwhisper" then
        for i, value in pairs(tableCoord) do
            unitID = i
            local orientationX, orientationY, angle = calculateAngle(bname, unitID)
            if ((orientationY > 0) and ((orientationX < 0 and (angle < 25 or angle > 42)) or (orientationX > 0 and (angle < 26 or angle > 42.8)))) or orientationY < 0 then
                local distance, class = distanceInYards(bname, tableCoord[i][1], tableCoord[i][2]), UnitClass(unitID)
                local orX, orY, angle = calculateAngle(bname, unitID)
                distance = math.abs(Distance_w_HitBox(bname, distance, angle, orX, orY))
                if class == "Rogue" and UnitBuff(unitID, "Stealth") then
                    if minDistance == 0 then
                        minDistance = 100
                        distance = minDistance + 1
                    else
                        distance = minDistance + 1
                    end
                end
                    
                if minDistance == 0 then
                    minDistance = distance
                    unitName = UnitName(unitID)
                elseif distance < minDistance then
                    inDistance = distance
                    unitName = UnitName(unitID)
                end
            end
        end
    end
    return unitName, minDistance
end

-- returns string with info about pulling time
local function timeOfPulling(DBMPullTime, pullTime)
    if DBMPullTime and DBMPullTime ~= 0 then
        local ctime = tonumber(pullTime)
        local ptime  = tonumber(DBMPullTime)
        differenceInTime = string.format("%.2f", tonumber(ptime - ctime)) 
        if ctime < ptime then
            return " "..differenceInTime.."s to DBM pull timer" 
        else
            return " "..math.abs(differenceInTime).."s after DBM pull timer"
        end
    else 
        return ""
    end
end

-- returns table of Relevant Units Coordinates --
local function getRaidUnitsCoord()
    SetMapToCurrentZone()
    local table = {}
    for i=1, GetNumRaidMembers() do
        if isUnitRelevant(i) then
            local unitID = "raid"..i
            table[unitID] = {}
            table[unitID][1], table[unitID][2] = GetPlayerMapPosition(unitID)
        end
    end
    return table
end

local function Track_Start(frame)
    frame:RegisterEvent("CHAT_MSG_ADDON")
    frame:RegisterEvent("CHAT_MSG_MONSTER_YELL");
    frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
    frame:UnregisterEvent("PLAYER_REGEN_ENABLED");
    frame:UnregisterEvent("PLAYER_ALIVE");
end

local function Track_Stop(frame)
    frame:UnregisterEvent("CHAT_MSG_ADDON")
    frame:UnregisterEvent("CHAT_MSG_MONSTER_YELL");
    frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
    frame:RegisterEvent("PLAYER_REGEN_ENABLED");
    frame:RegisterEvent("PLAYER_ALIVE");
end

local function Set_Mode(frame)
    if IsInInstance() and (GetInstanceInfo() == "Icecrown Citadel" 
        or GetInstanceInfo() == "The Ruby Sanctum" or GetInstanceInfo() == "Trial of the Crusader") then
        Track_Start(frame)
    else
        Track_Stop(frame)
    end
end

local function OnRaidRosterUpdate(frame, src)
    local rofficer = IRT_PT.settings.raidofficer 
    local rleader = IRT_PT.settings.raidleader
    
    if ( rofficer and not IsRaidOfficer() ) or ( rleader and not IsRaidLeader() ) then 
        frame:UnregisterEvent("CHAT_MSG_ADDON")
        frame:UnregisterEvent("CHAT_MSG_MONSTER_YELL");
        frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
        frame:UnregisterEvent("PLAYER_REGEN_ENABLED");
        frame:UnregisterEvent("PLAYER_ALIVE");
        return false 
    end

    frame:RegisterEvent("PLAYER_ENTERING_WORLD")

    if src == "from_event" then
        if PRE_time and PRE_time > 5 then
            Set_Mode(frame)
        end
    else
        Set_Mode(frame)
    end

    return true
end

local function displayMSG(message, hideTime)
    frame.text:SetText(message)
    frame:Show()

    if hideTime then
        frame.elapsed = hideTime
        frame:SetScript("OnUpdate", function(self, elapsed)
            frame.elapsed = frame.elapsed - elapsed
            if frame.elapsed > 0 then return end
            frame.text:SetText("")
            frame:Hide()
        end)
    end
end

local function Send_Message(finalMessage)
    local channel = IRT_PT.settings.channel
    SendChatMessage(finalMessage, channel, GetDefaultLanguage("player"))
end

local function Track_Enable(frame)
    local DBMPullTime, pullTime, lastBoss = 0, 0, ""
    local PRE_time = PRE_time or 0

    frame:SetScript("OnEvent", function(self, event, ...)
        -- Catches DBM pull timer --
        if event == "CHAT_MSG_ADDON" then
            local prefix, text = ...
            if prefix == "DBMv4-Pizza" then
                if string.find(text, "Pull") == nil and string.find(text, "Атака") == nil then
                    return false
                end
                local number = tonumber(string.match(text, "%d+"))
                DBMPullTime = number + GetTime()
            end
        -- Body Pull --    
        elseif event == "CHAT_MSG_MONSTER_YELL" then
            local text, srcName = ... 
            if contains(BOSSES, srcName) then
                if ENGAGES[srcName] == text then
                    PRE_time = PRE_time or 0
                    Track_Stop(self)
                    -- Saving RaidUnits Coordinates as quick as possible after detecting pull 
                    local raidCoord = getRaidUnitsCoord()
                    pullTime, lastBoss = GetTime(), srcName
                    local name, distance = Unit_Proximity(srcName, raidCoord)
                    local finalMessage = "The closest Unit (" .. string.format("%.2f", distance) .. " yards) to the " .. srcName .. " was " .. name .. " " .. timeOfPulling(DBMPullTime, pullTime) .. " (can be pulled by Pet or Totems)."
                    Send_Message(finalMessage)
                end    
            end
        -- Pull by Ability --    
        elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
            if contains(BOSSES, select(7, ...)) then
                local _, subEvent, _, srcName, _, _, destName, _, spellID, spellName = ...
                if contains(SUB_EVENTS, subEvent) and not contains(BLACK_LIST, spellName) then
                    Track_Stop(self)
                    pullTime, lastBoss = GetTime(), srcName
                    local checkPet, oname = isPet(srcName)
                    if not checkPet then
                        local unitID, i = raidIbyName(srcName)
                        if unitID then
                            SetMapToCurrentZone()
                            local x, y = GetPlayerMapPosition(unitID)
                            local distance = distanceInYards(destName, x, y)
                            local orX, orY, angle = calculateAngle(destName, unitID)
                            distance = math.abs(Distance_w_HitBox(destName, distance, angle, orX, orY))
                            local finalMessage = srcName .. " pulled the boss " .. destName .. timeOfPulling(DBMPullTime, pullTime) .. " from " .. string.format("%.2f", distance) .. " yards by " .. GetSpellLink(spellID)
                            Send_Message(finalMessage)
                        end  
                    elseif checkPet then
                        if srcName and oname and destName and timeOfPulling(DBMPullTime, pullTime) then
                            local finalMessage = srcName .. " pet (" .. oname .. " is owner) pulled the boss " .. destName .. timeOfPulling(DBMPullTime, pullTime) .. " by " .. GetSpellLink(spellID)
                            Send_Message(finalMessage)
                        end 
                    end  
                end 
            end 
        elseif event == "PLAYER_REGEN_ENABLED" or event == "PLAYER_ALIVE" then
            PRE_time = GetTime()
            if pullTime then
                if not bossAlive(lastBoss) then
                    self.elapsed = 5
                    self:SetScript("OnUpdate", function(self, elapsed)
                        self.elapsed = self.elapsed - elapsed
                        if self.elapsed <= 0 then
                            DBMPullTime, pullTime = 0, 0
                            Set_Mode(self)
                            self:Hide()
                        end
                    end)
                end
            end
        elseif event == "PLAYER_ENTERING_WORLD" then
            Set_Mode(self)
        elseif event == "RAID_ROSTER_UPDATE" then
            OnRaidRosterUpdate(self, "from_event")
        end
    end)
end

local function IRT_PT_CheckAdd(frame)
    if OnRaidRosterUpdate(frame, "from_click") then
        return true
    end
end

local function IRT_PT_EnableMode()
    local Core = _G["IRT_PT_Core"] or CreateFrame("Frame", "IRT_PT_Core")
    Core:RegisterEvent("RAID_ROSTER_UPDATE")
    Track_Enable(Core)

    if IRT_PT.settings.mode then
        if not IRT_PT_CheckAdd(Core) then 
            Core:UnregisterEvent("CHAT_MSG_ADDON")
            Core:UnregisterEvent("CHAT_MSG_MONSTER_YELL");
            Core:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");

            Core:UnregisterEvent("PLAYER_DEAD");
            Core:UnregisterEvent("PLAYER_REGEN_ENABLED");
            Core:UnregisterEvent("PLAYER_ALIVE"); 
        end
    else
        Core:UnregisterAllEvents()
    end
end

local function IRT_PT_OnInitialize()
    IRT_PT_EnableMode()
end




------------------------
--- ROLE PLAY TIMERS ---
------------------------

    local replicas = {
        ["Highlord Tirion Fordring"] = {"Hailing from the deepest", "Only by working together", 17, 35}, -- North Beasts
        ["Wilfred Fizzlebang"] = {"Prepare for oblivion!", 41}, -- Jarraxxus
        ["Blood-Queen Lana'thel"] = {"Foolish mortals. You thought us defeated so easily?", 28}, -- BQL
        ["The Lich King"] = {"So the Light's vaunted justice has finally arrived?", 55}, -- TLK
        ["Deathbringer Saurfang"] = {"Stubborn and old. What chance do you have?", 61}, -- DBS Horde
        ["Muradin Bronzebeard"] = {"Let's get a move on then!", 50}, -- DBS Ally
    }

    local function PSFtime(time)
        -- params: 2, 3, 4, 5
        PlaySoundFile("Interface\\AddOns\\InvisusRaidTools\\Media\\Sounds\\"..time..".mp3")
    end

    local function Start_DBMtimer(hideTime)
        local toggle = false 
        local channel = IRT_UT.RolePlay.channel
        local T = 1
        local decimals = hideTime - math.floor(hideTime)
        local C = hideTime - decimals

        if IsRaidOfficer() then
            local fHideTime = hideTime.."\tPull in"
            SendAddonMessage("DBMv4-Pizza", fHideTime, "RAID", PlayerName)
        end

        local soundPlayed = true

        if IRT_UT.RolePlay.PlaySound then
            soundPlayed = false
        end

        local f =_G["CnD"] or CreateFrame("Frame","CnD")
        f:SetScript("OnUpdate", function(s,e) 
            T = T + e
            if T > 1 + decimals then 
                decimals = 0
                T = 0 

                if (not ifRaidLeader and IsRaidOfficer()) or (ifRaidLeader and IsRaidLeader()) then
                    if math.floor(C/15) == C/15 or C < 6 or C == 10 or not toggle then 
                        SendChatMessage(C > 0 and "Pull in ".. C .." sec" or ">> Pull Now <<", channel) 
                        toggle = true 
                    end
                end 
                C = C - 1 

                if not soundPlayed and C <= 4 then
                    PSFtime(5)
                    soundPlayed = true
                end

                if C < 0 then 
                    toggle = false 
                    s:Hide() 
                end 
            end 
        end)
        f:Show()
    end

    local function UIErrosFrame_Do()
        if IRT_UT.UIError then
            UIErrorsFrame:Hide()
        else
            UIErrorsFrame:Show()
        end
    end

    local function setEvent(frame)
        frame:SetScript("OnEvent", function(self, event, ...)
            local msg, srcName = ...
            if event == "CHAT_MSG_MONSTER_YELL" then
                if replicas[srcName] then
                    if srcName ~= "Highlord Tirion Fordring" then
                        if string.find(msg, replicas[srcName][1]) then
                            local time = replicas[srcName][2]
                            Start_DBMtimer(time)
                        end
                    elseif srcName == "Highlord Tirion Fordring" then
                        if string.find(msg, replicas[srcName][1]) then
                            local time = replicas[srcName][3]
                            Start_DBMtimer(time)
                        elseif string.find(msg, replicas[srcName][2]) then
                            local time = replicas[srcName][4]
                            Start_DBMtimer(time)
                        end
                    end
                end
            elseif event == "PLAYER_ENTERING_WORLD" then
                if IsInInstance() then
                    frame:RegisterEvent("CHAT_MSG_MONSTER_YELL")
                else
                    frame:UnregisterEvent("CHAT_MSG_MONSTER_YELL")
                end
            end
        end)
    end

    local function RolePlays_Enable()
        if IRT_UT.RolePlay.Enable then
            local frame = _G["RolePlayCore"] or CreateFrame("Frame", "RolePlayCore")
            frame:RegisterEvent("PLAYER_ENTERING_WORLD")
            if IsInInstance() then
                frame:RegisterEvent("CHAT_MSG_MONSTER_YELL")
            end
            setEvent(frame)
        else
            if _G["RolePlayCore"] then
                _G["RolePlayCore"]:UnregisterAllEvents()
            end
        end
    end

    local function IRT_UT_OnInitialize()
        UIErrosFrame_Do()

        ifRaidLeader = IRT_UT.RolePlay.RL
        RolePlays_Enable()
    end







--------------------------
--- Spell Announcements --
--------------------------
    local IRT_Announcements
    local brew = 0

    local spellIDs = {49016, 1044, 10278, 1038, 75490, 75490, 29166, 48477, 58659, 698, 58887, 57934, 34477, 64901, 64843, 16190, 47883, 64205, 31821, 50763, 47883, 6940, 49277, 48788}

    local resSpells = {
        "Revive", 
        "Rebirth", 
        "Resurrection", 
        "Redemption", 
        "Ancestral Spirit", 
        "Soulstone Resurrection"
    }

    local frame = CreateFrame("Frame", "IRT_Announce");

    local function Enable_Announcements()
        local state = IRT_Announcements.Enable
        if state == true then
            if IsInInstance() then
                frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
                frame:RegisterEvent("UNIT_SPELLCAST_SENT");
            else
                frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
                frame:UnregisterEvent("UNIT_SPELLCAST_SENT");
            end
        else
            frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
            frame:UnregisterEvent("UNIT_SPELLCAST_SENT");
        end
    end

    frame:SetScript("OnEvent", function(self, event, ...)
            
        local timeStomp, subEvent, _, srcName, _, _, destName, _, spellID, spellName = ...

        if event == "COMBAT_LOG_EVENT_UNFILTERED" then
            if srcName == PlayerName and (subEvent == "SPELL_CAST_SUCCESS" or subEvent == "SPELL_RESURRECT") then 
                if contains(spellIDs, spellID) then
                    local channel = "SAY"
                    
                    if GetNumRaidMembers() > 0 then
                        channel = "RAID"
                    elseif GetNumPartyMembers() > 0 then
                        channel = "PARTY"
                    end

                    -- Eyes of Twilight (Healers Trink) --
                    if spellID == 75490 then
                        local item = select(2, GetItemInfo(54573))
                        SendChatMessage("Used " .. item, "SAY", GetDefaultLanguage("player"), whisper);
                        -- Ritual of Summoning / Ritual of Souls / Ritual of Refreshment
                    elseif spellID == 698 or spellID == 58887 or spellID == 58659 then
                        SendChatMessage("Creating a " .. GetSpellLink(spellID) .. ". Please, assist.", "SAY", GetDefaultLanguage("player"), whisper);
                        -- Hymn of Hope / Divine Hymn / Mana Tide Totem / Divine Sacrifice / Aura Mastery
                    elseif spellID == 64901 or spellID == 64843 or spellID == 16190 or spellID == 64205 or spellID == 31821 then
                        SendChatMessage("Casting " .. GetSpellLink(spellID), "SAY", GetDefaultLanguage("player"), "channel");
                        -- Innervate --
                    else
                        SendChatMessage("Casted " .. GetSpellLink(spellID) .. " on " .. destName, channel, GetDefaultLanguage("player"), "channel");
                    end
                end
            elseif arg10 == "Using Direbrew's Remote" then
                local channel = "SAY"
                
                if GetNumRaidMembers() > 0 then
                    channel = "RAID"
                elseif GetNumPartyMembers() > 0 then
                    channel = "PARTY"
                end
                
                if arg2 == "SPELL_CREATE" then
                    SendChatMessage(srcName.." created "..GetSpellLink(spellID)..". Do not click on it!", "YELL", GetDefaultLanguage("player"), whisper);
                end
            end
        elseif contains(resSpells, arg2) and event == "UNIT_SPELLCAST_SENT" and arg1 == "player" then
            local spellID = arg2
            if arg2 == "Soulstone Resurrection" then
                spellID = 47883
            end
            SendChatMessage("Started to cast " .. GetSpellLink(spellID) .. " on " .. arg4, channel, GetDefaultLanguage("player"), "channel");
        elseif event == "PLAYER_ENTERING_WORLD" then
            Enable_Announcements()
        end            
    end)




--------------------------
--- Frost Beacon Widget --
--------------------------
    local media = addonTable.media 
    local mediaSorted = addonTable.mediaSorted
    local test = false

    local IRT_RW, RL

    local function displayMSG(self, message, hideTime)  
        self.text:SetText(message)
        self:Show()

        if hideTime then
            self.elapsed = hideTime
            self:SetScript("OnUpdate", function(self, elapsed)
                self.elapsed = self.elapsed - elapsed
                if self.elapsed > 0 then return end
                self:Hide()
            end)
        end
    end

    local affected = {}

    local function FBdoDisplay()
        local _, _, _, _, maxp, diff = GetInstanceInfo()

        if maxp == 10 then 
            if table.getn(affected) == 2 then return true end
        elseif maxp == 25 and diff == 0 then
            if table.getn(affected) == 5 then return true end
        elseif maxp == 25 and diff == 1 then
            if table.getn(affected) == 6 then return true end
        end
    end

    local function SCMside(side, name)
        local side = "<"..side..">"
        SendChatMessage("Your side is "..side..". You recieved the message, because you're affected by "..GetSpellLink(70126), "WHISPER", GetDefaultLanguage("player"), name)
    end

    local function PSFside(side)
        -- params: "left", "center", "right"
        PlaySoundFile("Interface\\AddOns\\InvisusRaidTools\\Media\\Sounds\\move"..side..".mp3")
    end

    local function FBSendMessage(index)
        local _, _, _, _, maxp, diff = GetInstanceInfo()
        local name = affected[index]
        if name == PlayerName then return end
        if maxp == 10 then 
            if index == 1 then SCMside("LEFT", name) end
            if index == 2 then SCMside("RIGHT", name) end
        elseif maxp == 25 and diff == 0 then
            if index == 1 then SCMside("LEFT", name) end
            if index == 2 then SCMside("LEFT", name) end
            if index == 3 then SCMside("CENTER", name) end
            if index == 4 then SCMside("RIGHT", name) end
            if index == 5 then SCMside("RIGHT", name) end
        elseif maxp == 25 and diff == 1 then
            if index == 1 then SCMside("LEFT", name) end
            if index == 2 then SCMside("LEFT", name) end
            if index == 3 then SCMside("CENTER", name) end
            if index == 4 then SCMside("CENTER", name) end
            if index == 5 then SCMside("RIGHT", name) end
            if index == 6 then SCMside("RIGHT", name) end
        end
    end

    local function FBPlaySoundFile(index)
        local _, _, _, _, maxp, diff = GetInstanceInfo()
        if maxp == 10 then 
            if index == 1 then PSFside("LEFT") end
            if index == 2 then PSFside("RIGHT") end
        elseif maxp == 25 and diff == 0 then
            if index == 1 then PSFside("LEFT") end
            if index == 2 then PSFside("LEFT") end
            if index == 3 then PSFside("CENTER") end
            if index == 4 then PSFside("RIGHT") end
            if index == 5 then PSFside("RIGHT") end
        elseif maxp == 25 and diff == 1 then
            if index == 1 then PSFside("LEFT") end
            if index == 2 then PSFside("LEFT") end
            if index == 3 then PSFside("CENTER") end
            if index == 4 then PSFside("CENTER") end
            if index == 5 then PSFside("RIGHT") end
            if index == 6 then PSFside("RIGHT") end
        end
    end

    local function FBisPlayer(index)  
        if PlayerName == affected[index] then
            FBPlaySoundFile(index)
            return "<!!"..affected[index].."!!>"
        else
            if IRT_RW.FBW.Message then
                if not IRT_RW.FBW.RL and IsRaidOfficer() then
                    FBSendMessage(index)
                elseif IRT_RW.FBW.RL and IsRaidLeader() then
                    FBSendMessage(index)
                end
            end
            return affected[index]
        end
    end

    local function FBgetString(affectedTable)
        local s = ""
        if table.getn(affectedTable) == 2 then
            local leftA = setClassColor(FBisPlayer(1), UnitClass(raidIbyName(affectedTable[1])))
            local leftB = setClassColor(FBisPlayer(2), UnitClass(raidIbyName(affectedTable[2])))
            s = string.format("|cFF00FF00Left:|r %s\n|cFF00FF00Right:|r %s", leftA, leftB)
        elseif table.getn(affectedTable) == 5 then
            local leftA = setClassColor(FBisPlayer(1), UnitClass(raidIbyName(affectedTable[1])))
            local leftB = setClassColor(FBisPlayer(2), UnitClass(raidIbyName(affectedTable[2])))
            local leftC = setClassColor(FBisPlayer(3), UnitClass(raidIbyName(affectedTable[3])))
            local leftD = setClassColor(FBisPlayer(4), UnitClass(raidIbyName(affectedTable[4])))
            local leftE = setClassColor(FBisPlayer(5), UnitClass(raidIbyName(affectedTable[5])))
            s = string.format("|cFF00FF00Left:|r %s, %s\n|cFF00FF00Center:|r %s\n|cFF00FF00Right:|r %s, %s", leftA, leftB, leftC, leftD, leftE)
        elseif table.getn(affectedTable) == 6 then
            local leftA = setClassColor(FBisPlayer(1), UnitClass(raidIbyName(affectedTable[1])))
            local leftB = setClassColor(FBisPlayer(2), UnitClass(raidIbyName(affectedTable[2])))
            local leftC = setClassColor(FBisPlayer(3), UnitClass(raidIbyName(affectedTable[3])))
            local leftD = setClassColor(FBisPlayer(4), UnitClass(raidIbyName(affectedTable[4])))
            local leftE = setClassColor(FBisPlayer(5), UnitClass(raidIbyName(affectedTable[5])))
            local leftF = setClassColor(FBisPlayer(6), UnitClass(raidIbyName(affectedTable[6])))
            s = string.format("|cFF00FF00Left:|r %s, %s\n|cFF00FF00Center:|r %s, %s\n|cFF00FF00Right:|r %s, %s", leftA, leftB, leftC, leftD, leftE, leftF)
        end
        return s
    end 

    local function FB_SetPoint(frame)
        local x, y = IRT_RW.FBW.XOffset, IRT_RW.FBW.YOffset
        frame:SetPoint("CENTER", x, y)
    end

    local function FB_SetFont(frame)
        local font = IRT_RW.FBW.Font
        local size = IRT_RW.FBW.Size
        local shadow = IRT_RW.FBW.Shadow

        frame:SetFont(media.Font[font], size, shadow)
    end

    local function FB_SetTestText(frame)
        if test then
            local a = "|cFF00FF00Left:|r |cFFF58CBAKyznyx|r, |cFF40C7EBRubinart|r"
            local b = "|cFF00FF00Center:|r |cFFFF7D0AMerfin|r, |cFFC41F3BRixx|r"
            local c = "|cFF00FF00Right:|r |cFFC79C6ERezzy|r, |cFFC41F3BTgood|r"
            local str = a..'\n'..b..'\n'..c

            frame.text:SetText(str)
            frame:Show()
        else
            frame:Hide()
        end
    end

    local FB = CreateFrame("Frame", "IRT_FBW", UIParent)

    FB:SetScript("OnEvent", function(self, event, ...)
        if event == "PLAYER_ENTERING_WORLD" then
            if IsInInstance() and GetInstanceInfo() == "Icecrown Citadel" then
                FB:RegisterEvent("CHAT_MSG_MONSTER_YELL")
            else
                FB:UnregisterEvent("CHAT_MSG_MONSTER_YELL")
                FB:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            end
        elseif event == "CHAT_MSG_MONSTER_YELL" then
            local text, srcName = ...
            if srcName == "Sindragosa" and string.find(text, "Your incursion ends here") then
                FB:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            end
        elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
            local _, subEvent, _, _, _, _, destName, _, _, spellName = ...
            
            if subEvent == "SPELL_CAST_START" and spellName == "Ice Tomb" then
                affected = {}
                
            elseif subEvent == "SPELL_AURA_APPLIED" and spellName == "Frost Beacon" then
                affected = affected or {}
                table.insert(affected, destName)
                if FBdoDisplay() then 
                    local result = FBgetString(affected)
                    displayMSG(self, result, 7)
                    FB:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
                end
            end
        end
    end)

    local function FBW_Enable_Mode()
        local enable = IRT_RW.FBW.Enable
        if enable then
            FB:RegisterEvent("PLAYER_ENTERING_WORLD");
            if IsInInstance() and GetInstanceInfo() == "Icecrown Citadel" then
                FB:RegisterEvent("CHAT_MSG_MONSTER_YELL")
            else
                FB:UnregisterEvent("CHAT_MSG_MONSTER_YELL")
                FB:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            end
        else
            FB:UnregisterAllEvents();
        end
    end

    local function IRT_FBW_Enable()
        FB:SetWidth(1) 
        FB:SetHeight(1) 
        FB:SetAlpha(.95);
        FB_SetPoint(FB)
        FB.text = FB:CreateFontString(nil, "ARTWORK") 
        FB_SetFont(FB.text)
        FB_SetPoint(FB.text)
        FB:Hide()

        FBW_Enable_Mode()
    end

    local function IRT_RW_OnInitialize()
        IRT_FBW_Enable()
    end


------------------------------------
--- Boss Mechanics Announcements ---
------------------------------------
local BMA = CreateFrame("Frame", "IRT_BMA", UIParent)
local DefileF, NetherPowerF, UnchainedMagicF, HarvestSoulsF

local function bma_timings(file)
    if file == "Defile" then
        return 5.55
    elseif file == "UnchainedMagic" then
        return 6.44
    elseif file == "NetherPower" then
        return 6.44
    elseif file == "HarvestSouls" then
        return 6.34
    end
end

local function BMA_Start_Timer(file, timer, frame)
    frame:Hide()
    frame:Show()
    frame.elapsed = timer - bma_timings(file)
    frame:SetScript("OnUpdate", function(self, elapsed)
        self.elapsed = self.elapsed - elapsed
        if self.elapsed > 0 then return end
        if IRT_UT.BMA[file] then
            PlaySoundFile("Interface\\AddOns\\InvisusRaidTools\\Media\\Sounds\\"..file..".mp3")
        end
        self:Hide()
    end)
end

BMA:SetScript("OnEvent", function(self, event, ...)
    if event == "PLAYER_ENTERING_WORLD" then
        local instance = GetInstanceInfo()
        if IsInInstance() and (instance == "Icecrown Citadel" or instance == "The Ruby Sanctum" or instance == "Trial of the Crusader") then
            BMA:RegisterEvent("CHAT_MSG_MONSTER_YELL")
        else
            if DefileF then DefileF:Hide() end
            BMA:UnregisterEvent("CHAT_MSG_MONSTER_YELL")
            BMA:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
        end
    elseif event == "CHAT_MSG_MONSTER_YELL" then
        local text, srcName = ...
        if srcName == "The Lich King" then
            DefileF = DefileF or CreateFrame("Frame", "IRT_BMA_Defile")
            -- Defile after 1st transition
            if string.find(text, "keep you alive to") then
                BMA:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            elseif string.find(text, "Watch as the world around you collapses!") then
                local _, _, _, _, maxp, diff = GetInstanceInfo()
                -- If 10hc or 25hc and it's 1st phase
                if diff == 1 and 100*UnitHealth("boss1")/UnitHealthMax("boss1") > 40 then
                    BMA_Start_Timer("Defile", 38, DefileF)
                elseif diff == 0 then
                    BMA_Start_Timer("Defile", 38, DefileF)
                end
            elseif string.find(text, "Frostmourne hungers") then
                BMA_Start_Timer("Defile", 50, DefileF)
            elseif string.find(text, "I will freeze you from within") then
                DefileF:Hide()
            end
        elseif srcName == "Lord Jaraxxus" then
            if string.find(text, "You face") then
                NetherPowerF = NetherPowerF or CreateFrame("Frame", "IRT_BMA_NetherPower")
                BMA_Start_Timer("NetherPower", 15, NetherPowerF)
                BMA:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            elseif string.find(text, "Another will take") then
                NetherPowerF:Hide()
                BMA:UnegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            end
        elseif srcName == "Sindragosa" then
            if string.find(text, "At last") then
                UnchainedMagicF:Hide()
                BMA:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            elseif string.find(text, "limitless power and") then
                BMA:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            end
        end
    elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
        local _, subEvent, _, srcName, _, _, destName, _, _, spellName = ...
        if subEvent == "SPELL_CAST_START" then 
            if spellName == "Defile" then
                DefileF = DefileF or CreateFrame("Frame", "IRT_BMA_Defile")
                BMA_Start_Timer("Defile", 32.5, DefileF)
            elseif spellName == "Fury of Frostmourne" then
                DefileF:Hide()
                HarvestSoulsF:Hide()
                BMA:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
            end
        elseif subEvent == "SPELL_AURA_APPLIED" then 
            if spellName == "Nether Power" and destName == "Lord Jaraxxus" then
                NetherPowerF = NetherPowerF or CreateFrame("Frame", "IRT_BMA_NetherPower")
                BMA_Start_Timer("NetherPower", 45, NetherPowerF)
            elseif spellName == "Unchained Magic" and destName == PlayerName then
                UnchainedMagicF = UnchainedMagicF or CreateFrame("Frame", "IRT_BMA_UnchainedMagic")
                BMA_Start_Timer("UnchainedMagic", 30, UnchainedMagicF)
            end
        elseif subEvent == "SPELL_AURA_REMOVED" then
            if spellName == "Unchained Magic" and destName == PlayerName  then
                UnchainedMagicF:Hide()
            end
        elseif subEvent == "SPELL_CAST_SUCCESS" then
            if spellName == "Harvest Souls" then
                HarvestSoulsF = HarvestSoulsF or CreateFrame("Frame", "IRT_BMA_HarvestSouls")
                BMA_Start_Timer("HarvestSouls", 105, HarvestSoulsF)
            end
        end
    end
end)


local function IRT_BMA_Enable()
    if IRT_UT.BMA.Enable then
        BMA:RegisterEvent("PLAYER_ENTERING_WORLD")
        if IsInInstance() and (GetInstanceInfo() == "Icecrown Citadel" or GetInstanceInfo() == "The Ruby Sanctum") then
            BMA:RegisterEvent("CHAT_MSG_MONSTER_YELL")
        else
            BMA:UnregisterEvent("CHAT_MSG_MONSTER_YELL")
        end
    else
        BMA:UnregisterAllEvents()
    end
end

local function IRT_BMA_OnInitialize()
    IRT_BMA_Enable()
end

local function UsefulTools_GUI(container, AceGUI)
    local AceGUI = AceGUI
    local GUI = addonTable.GUI

    local spellOrders_settings = {
        {value = 1, text = "Pull Tracker"},
        {value = 2, text = "Role Play Timers"},
        {value = 3, text = "Spell Announcements"},
        {value = 4, text = "Frost Beacon Widget"},
        {value = 5, text = "Boss Mechanics Announcement"},
    }

    local channels = {
        ["SAY"] = "SAY",
        ["YELL"] = "YELL",
        ["PARTY"] = "PARTY",
        ["RAID"] = "RAID",
        ["RAID_WARNING"] = "RAID_WARNING",
        ["GUILD"] = "GUILD",
        ["GUILD"] = "OFFICER",
    }

    local shadows = {
        ["OUTLINE"] = "OUTLINE",
        ["THICKOUTLINE"] = "THICKOUTLINE",
        ["MONOCHROME"] = "MONOCHROME",
        ["None"] = "None",
    }

    local fonts = {}
    for i in pairs(media.Font) do
        fonts[i] = i 
    end

    local function Set_PullTrackerSection(container)
        GUI.PT = GUI.PT or {}

        local function Mode_CallBack(container, widget, value)
            IRT_PT.settings.mode = value
            IRT_PT_EnableMode()

            local bool = not value
            GUI.PT.RaidOfficer:SetDisabled(bool)
            GUI.PT.RaidLeader:SetDisabled(bool)
            GUI.PT.Channel:SetDisabled(bool)
        end

        local function RaidOfficer_CallBack(container, widget, value)
            IRT_PT.settings.raidofficer = value
            IRT_PT_EnableMode()
        end

        local function RaidLeader_CallBack(container, widget, value)
            IRT_PT.settings.raidleader = value
            IRT_PT_EnableMode()
        end

        local function Channel_CallBack(container, widget, value)
            IRT_PT.settings.channel = value
        end

        local title = AceGUI:Create("Heading")
            title:SetText("General Options")
            title:SetFullWidth(true)
            container:AddChild(title)
    
        local Enable = AceGUI:Create("CheckBox")
            Enable:SetLabel("Enable Mode")
            Enable:SetValue(IRT_PT.settings.mode)
            Enable:SetCallback("OnValueChanged", Mode_CallBack)
            container:AddChild(Enable)
            GUI.PT.Enable = Enable

        local RaidOfficer = AceGUI:Create("CheckBox")
            RaidOfficer:SetLabel("Only if Raid Officer")
            RaidOfficer:SetValue(IRT_PT.settings.raidofficer)
            RaidOfficer:SetCallback("OnValueChanged", RaidOfficer_CallBack)
            container:AddChild(RaidOfficer)
            GUI.PT.RaidOfficer = RaidOfficer

        local RaidLeader = AceGUI:Create("CheckBox")
            RaidLeader:SetLabel("Only if Raid Leader")
            RaidLeader:SetValue(IRT_PT.settings.raidleader)
            RaidLeader:SetCallback("OnValueChanged", RaidLeader_CallBack)
            container:AddChild(RaidLeader)
            GUI.PT.RaidLeader = RaidLeader

        local Channel = AceGUI:Create("Dropdown")
            Channel:SetLabel("Announce Channel")
            Channel:SetList(channels)
            Channel:SetValue(IRT_PT.settings.channel)
            Channel:SetCallback("OnValueChanged", Channel_CallBack)
            Channel:SetWidth(150)
            container:AddChild(Channel)
            GUI.PT.Channel = Channel

        if not Enable:GetValue() then
            RaidOfficer:SetDisabled(true)
            RaidLeader:SetDisabled(true)
            Channel:SetDisabled(true)
        end
    end

    local function Set_RolePlaySection(container)
        GUI.RolePlay = GUI.RolePlay or {}

        local function Enable_All_Settings(value)
            for i in pairs(GUI.RolePlay) do
                GUI.RolePlay[i]:SetDisabled(not value)
            end
        end

        local function Enable_CallBack(container, widget, value)
            IRT_UT.RolePlay.Enable = value
            RolePlays_Enable()
            Enable_All_Settings(value)
        end

        local function PSF_CallBack(container, widget, value)
            IRT_UT.RolePlay.PlaySound = value
        end

        local function RL_CallBack(container, widget, value)
            IRT_UT.RolePlay.RL = value
            ifRaidLeader = value
        end
    
        local title = AceGUI:Create("Heading")
            title:SetText("Role Play Timers")
            title:SetFullWidth(true)
            container:AddChild(title)
    
        local Enable = AceGUI:Create("CheckBox")
            Enable:SetLabel("Enable Module")
            Enable:SetValue(IRT_UT.RolePlay.Enable)
            Enable:SetCallback("OnValueChanged", Enable_CallBack)
            container:AddChild(Enable)

        local PSF = AceGUI:Create("CheckBox")
            PSF:SetLabel("Play Sound Counter")
            PSF:SetValue(IRT_UT.RolePlay.PlaySound)
            PSF:SetCallback("OnValueChanged", PSF_CallBack)
            container:AddChild(PSF)
            GUI.RolePlay.PSF = PSF

        local Test = AceGUI:Create("Button")
            Test:SetText("Sound Example")
            Test:SetWidth(150)
            Test:SetCallback("OnClick", 
                function() 
                    PlaySoundFile("Interface\\AddOns\\InvisusRaidTools\\Media\\Sounds\\5.mp3") 
                end)

            container:AddChild(Test)
            GUI.RolePlay.Test = Test

        local RL = AceGUI:Create("CheckBox")
            RL:SetLabel("Only if Raid Leader")
            RL:SetDescription("Make chat pull countdown only if Raid Leader")
            RL:SetValue(IRT_UT.RolePlay.RL)
            RL:SetCallback("OnValueChanged", RL_CallBack)
            container:AddChild(RL)
            GUI.RolePlay.RL = RL

        Enable_All_Settings(IRT_UT.RolePlay.Enable)
    end

    local function Set_SpellAnnouncementSection(container)
        GUI.SA = GUI.SA or {}

        local function Enable_CallBack(container, widget, value)
            IRT_Announcements.Enable = value
            Enable_Announcements()
        end
    
        local title = AceGUI:Create("Heading")
            title:SetText("Chat Announcements settings")
            title:SetFullWidth(true)
            container:AddChild(title)
    
        local Enable = AceGUI:Create("CheckBox")
            Enable:SetLabel("Enable Chat Announcements")
            Enable:SetValue(IRT_Announcements.Enable)
            Enable:SetCallback("OnValueChanged", Enable_CallBack)
            container:AddChild(Enable)
            GUI.SA.Enable = Enable
    end

    local function Set_FrostBeaconSection(container)
        GUI.RW = GUI.RW or {}
        GUI.RW.FBW = GUI.RW.FBW or {}

        local function Enable_All_Settings(value)
            for i in pairs(GUI.RW.FBW) do
                GUI.RW.FBW[i]:SetDisabled(not value)
            end
        end

        local function Enable_CallBack(container, widget, value)
            IRT_RW.FBW.Enable = value
            FBW_Enable_Mode()
            Enable_All_Settings(value)
        end

        local function Message_CallBack(container, widget, value)
            IRT_RW.FBW.Message = value
            GUI.RW.RL:SetDisabled(not value)
        end

        local function RL_CallBack(container, widget, value)
            IRT_RW.FBW.RL = value
            RL = value
        end

        local function fxOfs_CallBack(container, widget, value)
            IRT_RW.FBW.XOffset = value
            FB_SetPoint(FB.text)
        end

        local function fyOfs_CallBack(container, widget, value)
            IRT_RW.FBW.YOffset = value
            FB_SetPoint(FB.text)
        end

        local function Size_CallBack(container, widget, value)
            IRT_RW.FBW.Size = value
            FB_SetFont(FB.text)
        end

        local function Font_CallBack(container, widget, value)
            IRT_RW.FBW.Font = value
            FB_SetFont(FB.text)
        end
    
        local function Shadow_CallBack(container, widget, value)
            IRT_RW.FBW.Shadow = value
            FB_SetFont(FB.text)
        end

        local function TestMode_CallBack(container, widget, value)
            test = value
            FB_SetTestText(FB)
        end

        local title = AceGUI:Create("Heading")
            title:SetText("Frost Beacon Widget")
            title:SetFullWidth(true)
            container:AddChild(title)
    
        local Enable = AceGUI:Create("CheckBox")
            Enable:SetLabel("Enable Module")
            Enable:SetValue(IRT_RW.FBW.Enable)
            Enable:SetCallback("OnValueChanged", Enable_CallBack)
            container:AddChild(Enable)

        local TestMode = AceGUI:Create("CheckBox")
            TestMode:SetLabel("Test Mode")
            TestMode:SetValue(test)
            TestMode:SetCallback("OnValueChanged", TestMode_CallBack)
            container:AddChild(TestMode)
            GUI.RW.FBW.TestMode = TestMode
        
        local Message = AceGUI:Create("CheckBox")
            Message:SetLabel("Enable Messages Send")
            Message:SetValue(IRT_RW.FBW.Message)
            Message:SetCallback("OnValueChanged", Message_CallBack)
            container:AddChild(Message)
            GUI.RW.FBW.Message = Message

        local RL = AceGUI:Create("CheckBox")
            RL:SetLabel("Message Only if Raid Leader")
            RL:SetValue(IRT_RW.FBW.RL)
            RL:SetCallback("OnValueChanged", RL_CallBack)
            container:AddChild(RL)
            GUI.RW.FBW.RL = RL

        local xOfs = AceGUI:Create("Slider")
            xOfs:SetLabel("X Offset")
            xOfs:SetWidth(150)
            xOfs:SetSliderValues(-2000, 2000, 1)
            xOfs:SetValue(IRT_RW.FBW.XOffset)
            xOfs:SetCallback("OnValueChanged", fxOfs_CallBack)
            container:AddChild(xOfs)
            GUI.RW.FBW.xOfs = xOfs

        local yOfs = AceGUI:Create("Slider")
            yOfs:SetLabel("Y Offset")
            yOfs:SetWidth(150)
            yOfs:SetSliderValues(-2000, 2000, 1)
            yOfs:SetValue(IRT_RW.FBW.YOffset)
            yOfs:SetCallback("OnValueChanged", fyOfs_CallBack)
            container:AddChild(yOfs)
            GUI.RW.FBW.yOfs = yOfs

        local Size = AceGUI:Create("Slider")
            Size:SetLabel("Size")
            Size:SetWidth(150)
            Size:SetSliderValues(0, 40, 1)
            Size:SetValue(IRT_RW.FBW.Size)
            Size:SetCallback("OnValueChanged", Size_CallBack)
            container:AddChild(Size)
            GUI.RW.FBW.Size = Size

        local Font = AceGUI:Create("Dropdown")
            Font:SetLabel("Font")
            Font:SetList(fonts)
            Font:SetValue(IRT_RW.FBW.Font)
            Font:SetCallback("OnValueChanged", Font_CallBack)
            Font:SetWidth(150)
            container:AddChild(Font)
            GUI.RW.FBW.Font = Font
    
        local Shadow = AceGUI:Create("Dropdown")
            Shadow:SetLabel("Shadow")
            Shadow:SetList(shadows)
            Shadow:SetValue(IRT_RW.FBW.Shadow)
            Shadow:SetCallback("OnValueChanged", Shadow_CallBack)
            Shadow:SetWidth(150)
            container:AddChild(Shadow)
            GUI.RW.FBW.Shadow = Shadow

        if not GUI.RW.FBW.Message then RL:SetDisabled(true) end
        Enable_All_Settings(IRT_RW.FBW.Enable)
    end

    local function Set_BossMechanicsAnnouncement(container)
        GUI.RW = GUI.RW or {}
        GUI.RW.BMA = GUI.RW.BMA or {}

        local function Enable_All_Settings(value)
            for i in pairs(GUI.RW.BMA) do
                GUI.RW.BMA[i]:SetDisabled(not value)
            end
        end

        local function Enable_CallBack(container, widget, value)
            IRT_UT.BMA.Enable = value
            IRT_BMA_Enable()
            Enable_All_Settings(value)
        end

        local function NetherPower_CallBack(container, widget, value)
            IRT_UT.BMA.NetherPower = value
        end

        local function UnchainedMagic_CallBack(container, widget, value)
            IRT_UT.BMA.UnchainedMagic = value
        end

        local function HarvestSouls_CallBack(container, widget, value)
            IRT_UT.BMA.HarvestSouls = value
        end

        local function Defile_CallBack(container, widget, value)
            IRT_UT.BMA.Defile = value
        end

        local Enable = AceGUI:Create("CheckBox")
            Enable:SetLabel("Enable Module")
            Enable:SetValue(IRT_UT.BMA.Enable)
            Enable:SetCallback("OnValueChanged", Enable_CallBack)
            container:AddChild(Enable)



        local NetherPowerTitle = AceGUI:Create("Heading")
            NetherPowerTitle:SetText("Nether Power CD Announcement")
            NetherPowerTitle:SetFullWidth(true)
            container:AddChild(NetherPowerTitle)

        local NetherPower = AceGUI:Create("CheckBox")
            NetherPower:SetLabel("Enable Announcement")
            NetherPower:SetValue(IRT_UT.BMA.NetherPower)
            NetherPower:SetCallback("OnValueChanged", NetherPower_CallBack)
            container:AddChild(NetherPower)
            GUI.RW.BMA.NetherPower = NetherPower

        local NetherPowerTest = AceGUI:Create("Button")
            NetherPowerTest:SetText("Sound Example")
            NetherPowerTest:SetWidth(150)
            NetherPowerTest:SetCallback("OnClick", 
                function() 
                    print("123")
                    PlaySoundFile("Interface\\AddOns\\InvisusRaidTools\\Media\\Sounds\\NetherPower.mp3") 
                end)

            container:AddChild(NetherPowerTest)
            GUI.RW.BMA.NetherPowerTest = NetherPowerTest

        local UnchainedMagicTitle = AceGUI:Create("Heading")
            UnchainedMagicTitle:SetText("Unchained Magic 2nd phase Warning Announcement")
            UnchainedMagicTitle:SetFullWidth(true)
            container:AddChild(UnchainedMagicTitle)

        local UnchainedMagic = AceGUI:Create("CheckBox")
            UnchainedMagic:SetLabel("Enable Announcement")
            UnchainedMagic:SetValue(IRT_UT.BMA.UnchainedMagic)
            UnchainedMagic:SetCallback("OnValueChanged", UnchainedMagic_CallBack)
            container:AddChild(UnchainedMagic)
            GUI.RW.BMA.UnchainedMagic = UnchainedMagic

        local UnchainedMagicTest = AceGUI:Create("Button")
            UnchainedMagicTest:SetText("Sound Example")
            UnchainedMagicTest:SetWidth(150)
            UnchainedMagicTest:SetCallback("OnClick", 
                function() 
                    PlaySoundFile("Interface\\AddOns\\InvisusRaidTools\\Media\\Sounds\\UnchainedMagic.mp3") 
                end)

            container:AddChild(UnchainedMagicTest)
            GUI.RW.BMA.UnchainedMagicTest = UnchainedMagicTest

        local DefileTitle = AceGUI:Create("Heading")
            DefileTitle:SetText("Defile CD Announcement")
            DefileTitle:SetFullWidth(true)
            container:AddChild(DefileTitle)

        local Defile = AceGUI:Create("CheckBox")
            Defile:SetLabel("Enable Announcement")
            Defile:SetValue(IRT_UT.BMA.Defile)
            Defile:SetCallback("OnValueChanged", Defile_CallBack)
            container:AddChild(Defile)
            GUI.RW.BMA.Defile = Defile

        local DefileTest = AceGUI:Create("Button")
            DefileTest:SetText("Sound Example")
            DefileTest:SetWidth(150)
            DefileTest:SetCallback("OnClick", 
                function() 
                    PlaySoundFile("Interface\\AddOns\\InvisusRaidTools\\Media\\Sounds\\Defile.mp3") 
                end)

            container:AddChild(DefileTest)
            GUI.RW.BMA.DefileTest = DefileTest
            
        local HarvestSoulsTitle = AceGUI:Create("Heading")
            HarvestSoulsTitle:SetText("Harvest Souls CD Announcement")
            HarvestSoulsTitle:SetFullWidth(true)
            container:AddChild(HarvestSoulsTitle)

        local HarvestSouls = AceGUI:Create("CheckBox")
            HarvestSouls:SetLabel("Enable Announcement")
            HarvestSouls:SetValue(IRT_UT.BMA.HarvestSouls)
            HarvestSouls:SetCallback("OnValueChanged", HarvestSouls_CallBack)
            container:AddChild(HarvestSouls)
            GUI.RW.BMA.HarvestSouls = HarvestSouls

        local HarvestSoulsTitleTest = AceGUI:Create("Button")
            HarvestSoulsTitleTest:SetText("Sound Example")
            HarvestSoulsTitleTest:SetWidth(150)
            HarvestSoulsTitleTest:SetCallback("OnClick", 
                function() 
                    PlaySoundFile("Interface\\AddOns\\InvisusRaidTools\\Media\\Sounds\\HarvestSouls.mp3") 
                end)

            container:AddChild(HarvestSoulsTitleTest)
            GUI.RW.BMA.HarvestSoulsTitleTest = HarvestSoulsTitleTest
        
        Enable_All_Settings(IRT_UT.BMA.Enable)
    end

    local function UT_tree_CallBack(container, widget, group)
        container:ReleaseChildren() 
        container:SetFullHeight(true)
        container:SetLayout("Fill")

        local scroll = AceGUI:Create("ScrollFrame")
        scroll:SetLayout("Flow")
        container:AddChild(scroll)

        if group == 1 then
            Set_PullTrackerSection(scroll)
        elseif group == 2 then
            Set_RolePlaySection(scroll)
        elseif group == 3 then
            Set_SpellAnnouncementSection(scroll)
        elseif group == 4 then
            Set_FrostBeaconSection(scroll)
        elseif group == 5 then
            Set_BossMechanicsAnnouncement(scroll)
        end
    end

    container:SetFullHeight(true)
    container:SetLayout("Fill")

    local frame = AceGUI:Create("TreeGroup")
    frame:SetTree(spellOrders_settings)
    frame:SetFullHeight(true)
    frame:SetCallback("OnGroupSelected", UT_tree_CallBack)
    container:AddChild(frame)
    frame:SelectByValue(1)
end

function InvisusRT:UsefulTools_GUI(container, AceGUI)
    UsefulTools_GUI(container, AceGUI)
end

local UT_DB = CreateFrame("Frame", "loadsDB")
UT_DB:RegisterEvent("ADDON_LOADED")
UT_DB:RegisterEvent("PLAYER_LOGIN")
UT_DB:RegisterEvent("PLAYER_LOGOUT") 
function UT_DB:OnEvent(event, arg1)
    if event == "ADDON_LOADED" then
        if arg1 == "InvisusRaidTools" then
            IRT_UT = addonTable.db.profile.IRT_UT
            IRT_UT_OnInitialize()

            IRT_PT = addonTable.db.profile.IRT_PT
            IRT_PT_OnInitialize()

            IRT_Announcements = addonTable.db.profile.IRT_Announcements
            frame:RegisterEvent("PLAYER_ENTERING_WORLD");

            IRT_RW = addonTable.db.profile.IRT_RW
            IRT_RW_OnInitialize()

            IRT_BMA_OnInitialize()

            self:UnregisterEvent("ADDON_LOADED")
        end
    elseif event == "PLAYER_LOGIN" then
        IRT_UT_OnInitialize()
        IRT_RW_OnInitialize()
        self:UnregisterEvent("PLAYER_LOGIN")
    elseif event == "PLAYER_LOGOUT" then
        InvisusRaidTools_DB["IRT_PT"] = IRT_PT
        InvisusRaidTools_DB["IR_PT_OPTIONS"] = IR_PT_OPTIONS
    end
end

UT_DB:SetScript("OnEvent", UT_DB.OnEvent)
