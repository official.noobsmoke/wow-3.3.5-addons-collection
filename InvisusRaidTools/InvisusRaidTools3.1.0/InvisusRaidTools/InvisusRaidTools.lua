-- Invisus Raid Tools
-- Useful Raiding Tools for WotLK
-- Author: Merfin @ https://invisus.shivtr.com/

local addonName, addonTable = ...
local addonVersion = tonumber(GetAddOnMetadata("InvisusRaidTools", "Version")) 
local RAID_CLASS_COLORS = RAID_CLASS_COLORS

InvisusRT = LibStub("AceAddon-3.0"):NewAddon("InvisusRT", "AceConsole-3.0")
addonTable.GUI = addonTable.GUI or {}

local defaults = { 
    profile = { 
        minimap = { 
            hide = false, 
        }, 
        settings = true,
        IRT_Orders = {
            ["Settings"] = {
                ["Buttons"] = {
                    ["Ignore Class Filter"] = false,
                    ["Free Frames Alignment"] = false,
                    ["Lock Frames"] = true,
                    ["Send Data"] = false,
                    ["Send Data CheckButtonState"] = false,
                    ["Hide Minimap Icon"] = false,
                },
            },
            ["Frame"] = {
                ["Layout"] = {
                    ["Anchor"] = "BOTTOMLEFT", -- first frame Anchor
                    ["Transparency"] = 1, -- (from 0.0 to 1)
                    ["Scale"] = 1,
                    ["xOfs"] = -800,
                    ["yOfs"] = 0,
                    ["HSpace"] = 30,
                    ["VSpace"] = 27,
                    ["Rows"] = 4,
                },

                ["Title"] = {
                    ["AutoSize"] = true,
                    ["Texture"] = "normTex",
                    ["ClassColor"] = false,
                    ["Color"] = {0, 0, 0, 0.6},
                    ["Transparency"] = 1,
                    ["Width"] = 170,
                    ["Height"] = 20,
                },

                ["SpellIcon"] = {
                    ["Show"] = true,
                    ["AutoSize"] = false,
                    ["Anchor"] = "BOTTOMLEFT",
                    ["RelativePoint"] = "BOTTOMLEFT",
                    ["Transparency"] = 1,
                    ["Size"] = 28,
                    ["xOfs"] = 0,
                    ["yOfs"] = 0,
                },

                ["Text"] = {
                    ["Title"] = {
                        ["Show"] = true,
                        ["Font"] = "Accidental Presidency",
                        ["ClassColor"] = true,
                        ["Color"] = {0, 0.2, 0, 1},
                        ["Shadow"] = "None",
                        ["Size"] = 15,
                        ["Length"] = 30,
                        ["Anchor"] = "RIGHT",
                        ["RelativePoint"] = "RIGHT",
                        ["xOfs"] = -5,
                        ["yOfs"] = 0,
                    },
                },
            },
            ["StatusBar"] = {
                ["Layout"] = {
                    ["Transparency"] = 1,
                    ["Width"]    = 175,
                    ["Height"] = 20,
                    ["Space"] = 3,
                    ["Bar CD Update Rate"] = 0.01,
                },

                ["Texture"] = {
                    ["Cooldown"] = {
                        ["Texture"] = "normTex",
                        ["ClassColor"] = true,
                        ["Color"] = {1, 0, 0, 1},
                        ["Transparency"] = 0.6,
                    },

                    ["Background"] = {
                        ["Texture"] = "normTex",
                        ["Color"] = {0, 0, 0, 0.5},
                        ["Transparency"] = 1,
                    },
                },

                ["Text"] = {
                    ["SrcName"] = {
                        --["Show"] = true,
                        ["Font"] = "Accidental Presidency",
                        --["Color"] = {1, 1, 1, 1},
                        ["Shadow"] = "None",
                        ["Size"] = 15,
                        ["Length"] = 10,
                        ["Anchor"] = "LEFT",
                        ["RelativePoint"] = "LEFT",
                        ["xOfs"] = 2,
                        ["yOfs"] = 0,
                    },

                    ["DestName"] = {
                        ["Show"] = true,
                        ["Font"] = "Accidental Presidency",
                        ["Color"] = {1, 1, 1, 1},
                        ["Shadow"] = "None",
                        ["Size"] = 15,
                        ["Length"] = 5,
                        ["Anchor"] = "CENTER",
                        ["RelativePoint"] = "CENTER",
                        ["xOfs"] = 0,
                        ["yOfs"] = 0,
                    },

                    ["Time"] = {
                        ["Show"] = true,
                        ["Font"] = "Accidental Presidency",
                        ["Color"] = {1, 1, 1, 1},
                        ["Shadow"] = "None",
                        ["Size"] = 15,
                        ["Anchor"] = "RIGHT",
                        ["RelativePoint"] = "RIGHT",
                        ["xOfs"] = -5,
                        ["yOfs"] = 0,
                    },

                    ["Ready"] = {
                        ["Color"] = {0, 1, 0.3, 1},
                    },

                    ["Not Ready"] = {
                        ["Color"] = {1, 1, 1, 0.35},
                    },
                },

                ["State"] = {
                    ["Show"] = true,
                    ["AutoSize"] = true,
                    ["Size"] = 20,
                    ["Transparency"] = 1,
                    ["Anchor"] = "LEFT",
                    ["RelativePoint"] = "RIGHT",
                    ["xOfs"] = 5,
                    ["yOfs"] = 0,
                    ["Path"] = {
                        ["Death"] = "Interface\\Icons\\Ability_creature_cursed_02",
                        ["Valkyr"] = "Interface\\Icons\\Achievement_Boss_SvalaSorrowgrave",
                        ["Offline"] = "Interface\\Icons\\Ability_druid_cower",
                        ["OutOfRaid"] = "Interface\\Icons\\ability_hunter_displacement",
                    },
                },
            },
        
            ["options"] = {
                ["Login"] = true,
                ["positions"] = {
                    ["row"] = 0, 
                    ["column"] = 0,
                },
            },
        },

        IRT_PT = {
            ["settings"] = {
                ["channel"] = "RAID",
                ["mode"] = true,
                ["font"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\Accidental Presidency.ttf",
                ["raidofficer"] = true,
                ["raidleader"] = true,
            },

            ["options"] = {
                ["raidofficer"] = true,
            }
        },

        IRT_UT = {
            ["UIError"] = false,
            ["RolePlay"] = {
                ["Enable"] = true,
                ["channel"] = "RAID_WARNING",
                ["PlaySound"] = true,
                ["RL"] = true,
            },
            ["BMA"] = {
                ["Enable"] = true,
                ["NetherPower"] = true,
                ["UnchainedMagic"] = true,
                ["Defile"] = true,
                ["HarvestSouls"] = true,
            },
        },

        IRT_Announcements = {
            ["Enable"] = false,
        },

        IRT_RW = {
            ["FBW"] = {
                ["Enable"] = true,
                ["RL"] = true,
                ["Message"] = true,
                --["TestMode"] = false,
                ["Font"] = "Accidental Presidency",
                ["Size"] = 18,
                ["Shadow"] = "OUTLINE",
                ["XOffset"] = 0,
                ["YOffset"] = 55,
            },
        },
    }, 
}

local options = {
    name = "InvisusRT",
    handler = InvisusRT,
    hidden = false,
    type = 'group',
    args = {
        hide = {
            type = 'toggle',
            name = 'Hide Minimap Icon',
            desc = 'Hide the minimap icon',
            set = 'SetMinimap',
            get = 'GetMinimap',
        },
    },
}

local function InvisusRT_GUI()
    local AceGUI = LibStub("AceGUI-3.0")
    local frame = AceGUI:Create("Frame")
    addonTable.optionsWindow = frame

    frame:SetTitle("|cFF00CDFFInvisusRT|r Options")
    frame:SetStatusText("InvisusRT @ invisus.shivtr.com (Icecrown, Warmane)")
    frame:SetWidth(700)
    frame:SetHeight(500)
    frame:SetLayout("Fill")
    frame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end)

    local function Draw_SpellOrder(container)
        InvisusRT:SpellOrders_GUI(container, AceGUI)
    end

    local function Draw_PullTracker(container)
        InvisusRT:PT_GUI(container, AceGUI)
    end

    local function Draw_UsefulTools(container)
        InvisusRT:UsefulTools_GUI(container, AceGUI)
    end

    local function Draw_Announcements(container)
        InvisusRT:Announcements_GUI(container, AceGUI)
    end

    local function Draw_RaidWidgets(container)
        InvisusRT:RaidWidgets_GUI(container, AceGUI)
    end

    local function SelectGroup(container, event, group)
        container:ReleaseChildren()
        if group == "tab1" then
            Draw_SpellOrder(container)
        elseif group == "tab2" then
            Draw_UsefulTools(container)
        end
    end

    local tabs = {
        {
            text= "Spell Orders", 
            value= "tab1"
        }, 
        

        {
            text= "Useful Tools",
            value= "tab2"
        },

    }

    local tab = AceGUI:Create("TabGroup")
          tab:SetLayout("Flow")
          tab:SetTabs(tabs)
          tab:SetCallback("OnGroupSelected", SelectGroup)
          tab:SelectTab("tab1")

    frame:AddChild(tab)
end

local function Open_GUI()
    if addonTable.optionsWindow then
        if addonTable.optionsWindow:IsShown() then
            addonTable.optionsWindow:Hide()
        else
            InvisusRT_GUI()
            addonTable.optionsWindow:Show()
        end
    else
        InvisusRT_GUI()
    end
end

function InvisusRT_openGUI()
    Open_GUI()
end

local MiniMapButton_OnClick = function(self, button)
    local options, config = _G["InvisusRaidTools_FrameOptions"],
                            _G["IRT_OrdersConfig"]

    if IsAltKeyDown() then
        Open_GUI()
    else
        if config:IsVisible() then
            
            config:Hide()
        else
            config:Show()
        end
    end
end

local IRT_LDB = LibStub("LibDataBroker-1.1"):NewDataObject("InvisusRT!", {
    type = "data source",
    text = "InvisusRT!",
    icon = "Interface\\Icons\\Achievement_Boss_SvalaSorrowgrave",
    OnClick = function() IRT_OrdersConfig:Show() end,
    OnEnter =  function(self)   IRT_Tooltip_GUI:SetOwner(self, "ANCHOR_BOTTOMLEFT")
                                IRT_Tooltip_GUI:SetText("|cFF00CDFFInvisusRT|r v"..addonVersion)
                                IRT_Tooltip_GUI:AddLine(" ")
                                IRT_Tooltip_GUI:AddLine("|cFF00FFAALeft Click:|r Open Order Builder")
                                IRT_Tooltip_GUI:AddLine("|cFF00FFAALeft Click + Alt:|r Open IRT Options")
                                IRT_Tooltip_GUI:Show() end,

    OnLeave = function() IRT_Tooltip_GUI:Hide() end,
    OnClick = function(self, button) MiniMapButton_OnClick(self, button) end,
})

InvisusRT.icon = LibStub("LibDBIcon-1.0")

function InvisusRT:OnInitialize()
    self.db = LibStub("AceDB-3.0"):New("InvisusRT_DB", defaults)
    addonTable.db = self.db

    self.db.RegisterCallback(self, "OnProfileChanged", "RefreshConfig")
    self.db.RegisterCallback(self, "OnProfileCopied", "RefreshConfig")
    self.db.RegisterCallback(self, "OnProfileReset", "RefreshConfig")

    options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db);

    local config = LibStub("AceConfig-3.0");
    self.configdialog = LibStub("AceConfigDialog-3.0");
    config:RegisterOptionsTable("InvisusRT", options, {"invisusconf"});
    self.configdialog:SetDefaultSize("InvisusRT", 1000, 600);

    config:RegisterOptionsTable( "InvisusRT", options )
    self.configdialog:AddToBlizOptions( "InvisusRT", "InvisusRT")

    InvisusRT.icon:Register("InvisusRT!", IRT_LDB, self.db.profile.minimap) 
    self:RegisterChatCommand("invisus", SetMinimap) 
end

function InvisusRT:SetMinimap(info, input)
    self.db.profile.hide = input
    if input then
        InvisusRT.icon:Hide("InvisusRT!")
    else
        InvisusRT.icon:Show("InvisusRT!")
    end
end

function InvisusRT:GetMinimap(info)
    return self.db.profile.hide
end

function InvisusRT:RefreshConfig()

    -- hiding exisiting frames 
    if IRT_Orders.frames then
        for frameName in pairs(IRT_Orders.frames) do
            IRT_Orders.framesData[frameName]:Hide()
        end
    end

    if  _G["IRT_Orders_GlobalFrame"] then
        _G["IRT_Orders_GlobalFrame"]:UnregisterAllEvents()
    end

    IRT_Orders = addonTable.db.profile.IRT_Orders
    addonTable.RefreshFrames()
end

---------------------------------------
---------------Commands----------------
---------------------------------------
local function MPTcommands(msg, editbox)
    local _, _, cmd, args = string.find(msg, "%s?(%w+)%s?(.*)")

    if cmd then
        if cmd =="o" then
            if not InvisusRaidTools_SetupSpellOrder:IsShown() then
                InvisusRaidTools_SetupSpellOrder:Show()
                
            else
                InvisusRaidTools_SetupSpellOrder:Hide()
            end
        end
    else
        Open_GUI()
    end
end

SLASH_INVISUS_ORDERS1 = '/irt'
SlashCmdList["INVISUS_ORDERS"] = MPTcommands
