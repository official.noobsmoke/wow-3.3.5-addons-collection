-- Invisus Orders (part of Invisus Raid Tools)
-- Creates custom Orders on certain spells 
-- Author: Merfin @ https://invisus.shivtr.com/

--InvisusRT = LibStub("AceAddon-3.0"):NewAddon("InvisusRT_SpellOrders", "AceConsole-3.0")

local addonName, addonTable = ...

local _G = _G
local pairs = pairs
local ipairs = ipairs
local unpack = unpack
local GetSpellInfo = GetSpellInfo
local RAID_CLASS_COLORS = RAID_CLASS_COLORS

local spell_info = addonTable.orders_spnested
local spell_info_sorted = addonTable.orders_sorted
local spellproc_info = addonTable.IRT_Orders_Spell_Proc_Info
local classes = addonTable.classes


local media = addonTable.media 
local mediaSorted = addonTable.mediaSorted

--local function IRT_Orders_OnInit()
    --IRT_Orders.frames = IRT_Orders.frames or {}
--end

local counterSN = counterSN or 0
local deathsT = deathsT or {}
local UHlastupdate = UHlastupdate or 0

local function strShorten(str, len)
    local result = ""
    if str and str ~= "" then
        result = string.sub(str, 1, len)
    end
    return result
end

-- Adds Unit in Order
local function insertName(name, array, unitID, query)
    if UnitName("player") == name then unitID = "player" end
    array[name] = array[name] or {}
    table.insert(array[name], true)
    table.insert(array[name], 0)
    table.insert(array[name], unitID)
    table.insert(array[name], name)
    query[#query + 1] = name
end

local function insert_framesQuery(arr, ele)
    if contains(arr, ele) then return end
    table.insert(arr, ele)
end

local function SpellMatchesClass(unitID, spellName)
    if not unitID then return end 
    if UnitClass(unitID) == spell_info[spellName][3] then return true end
end


local function Unit_SetAttributes(spellName, unitID, unitName, frame)
    local unitRelevant = false
    local dbIcon = IRT_Orders.StatusBar.State

    if unitID == "gone" then
        frame.icon:SetTexture(dbIcon.Path.OutOfRaid)
    elseif not UnitIsConnected(unitID) then
        frame.icon:SetTexture(dbIcon.Path.Offline)
    elseif UnitIsDeadOrGhost(unitID) then
        frame.icon:SetTexture(dbIcon.Path.Death)
    elseif UnitInVehicle(unitID) then
        if UnitName("boss1") == "The Lich King" then
            if ( UnitHealth("boss1")*100/UnitHealthMax("boss1") ) > 42 then
                frame.icon:SetTexture(dbIcon.Path.Valkyr)
                unitRelevant = false
            end
        end
    else
        unitRelevant = true
    end

    if unitRelevant then
        frame.icon:Hide()
    else
        frame.icon:Show()
    end

    return unitRelevant
end

local function Frame_SetOrder(spellName)
    local IRT_toggle = false
    IRT_Orders.frames[spellName] = IRT_Orders.frames[spellName] or {}

    for _, i in pairs(IRT_Orders.frames[spellName].query) do
        local frame = IRT_Orders.frames[spellName].order[i][5]
        local unitID = IRT_Orders.unitIDs[i][1]
        local r, g, b

        local unitRelevant = Unit_SetAttributes(spellName, unitID, i, frame)

        if not IRT_toggle and IRT_Orders.frames[spellName].order[i][1] and unitRelevant then
            frame.unitName:SetTextColor(unpack(IRT_Orders.StatusBar.Text.Ready.Color))
            IRT_toggle = true
        else
            frame.unitName:SetTextColor(unpack(IRT_Orders.StatusBar.Text["Not Ready"].Color))
        end
    end
end

local function StatusBar_SetBuffTimer(spellName, unitName)
    local expirationTime = spell_info[spellName][1]
    local statusBar = IRT_Orders.frames[spellName].order[unitName][5]
    local update = IRT_Orders.StatusBar.Layout["Bar CD Update Rate"]

    statusBar:SetValue(expirationTime)
    statusBar.value:SetFormattedText("%d:%02d", floor(expirationTime/60), expirationTime%60)

    local frameName = "BuffOnUpdate"..spellName..unitName
    local f = _G[frameName] or CreateFrame("Frame", frameName)
    f:Show()
    f.elapsed, f.updateV, f.updateT = expirationTime, update, 0.1
    f:SetScript("OnUpdate", function(self, elapsed)
        if not statusBar:IsShown() then self:Hide() end
        self.elapsed = self.elapsed - elapsed
        self.updateV = self.updateV - elapsed
        self.updateT = self.updateT - elapsed

        if self.updateV <= 0 then
            -- elapsed bugs after log in
            if elapsed > 2 then
                f.elapsed = f.elapsed + elapsed
            end

            statusBar:SetValue(self.elapsed)

            if self.updateT <= 0 then
                statusBar.value:SetFormattedText("Buff: %d:%02d", floor(self.elapsed/60), self.elapsed%60)
                self.updateT = 0.1
            end

            self.updateV = update
        end

        if self.elapsed > 0 then return end
        statusBar.value:SetText("")
        statusBar:SetValue(0)
        self:Hide()
    end)
end

local function StatusBar_SetUpdateTimer(spellName, unitName)
    local expirationTime = spell_info[spellName][1]
    local statusBar = IRT_Orders.frames[spellName].order[unitName][5]
    local hideTime = IRT_Orders.frames[spellName].order[unitName][2] - GetTime()
    local update = IRT_Orders.StatusBar.Layout["Bar CD Update Rate"]

    statusBar:SetValue(expirationTime)
    statusBar.value:SetFormattedText("%d:%02d", floor(hideTime/60), hideTime%60)

    if hideTime < 0 then return end
    Frame_SetOrder(spellName)
    local frameName = "CDOnUpdate"..unitName..spellName

    local f = _G[frameName] or CreateFrame("Frame", frameName)
    f:Show()
    f.elapsed, f.updateV, f.updateT = hideTime, update, 0.1
    f:SetScript("OnUpdate", function(self, elapsed)
        if not statusBar:IsShown() then self:Hide() end

        self.elapsed = self.elapsed - elapsed
        self.updateV = self.updateV - elapsed
        self.updateT = self.updateT - elapsed

        if self.updateV <= 0 then
            -- elapsed bugs after log in
            if elapsed > 2 then
                f.elapsed = f.elapsed + elapsed
            end

            statusBar:SetValue(self.elapsed)

            if self.updateT <= 0 then
                statusBar.value:SetFormattedText("%d:%02d", floor(self.elapsed/60), self.elapsed%60)
                self.updateT = 0.1
            end

            self.updateV = update
        end

        if self.elapsed > 0 then return end
        IRT_Orders.frames[spellName].order[unitName][1] = true
        statusBar.destName:SetText("")
        statusBar.value:SetText("")
        statusBar:SetValue(0)
        Frame_SetOrder(spellName)
        self:Hide()
    end)
end

local function For_All_Frames(func)
    if IRT_Orders.frames then
        for frameName in pairs(IRT_Orders.frames) do
            local frame = IRT_Orders.framesData[frameName]
            func(frame)
        end 
    end
end

local function Frame_GetOffsets(frame)
    local justifyX, justifyY
    local anchor = IRT_Orders.Frame.Layout.Anchor
    if anchor == "TOPLEFT" then
        justifyX, justifyY = 1, -1
    elseif anchor == "TOPRIGHT" then
        justifyX, justifyY = -1, -1
    elseif anchor == "BOTTOMLEFT" then
        justifyX, justifyY = 1, 1
    elseif anchor == "BOTTOMRIGHT" then
        justifyX, justifyY = -1, 1
    end

    local StartX = IRT_Orders.Frame.Layout.xOfs
    local StartY = IRT_Orders.Frame.Layout.yOfs
    local x = StartX
    local y = StartY
    local column = IRT_Orders.options.positions.column
    local row = IRT_Orders.options.positions.row
    local maxColumn = IRT_Orders.Frame.Layout.Rows
    local VSpace = IRT_Orders.Frame.Layout.VSpace
    local HSpace = IRT_Orders.Frame.Layout.HSpace
    local frameHeight = _G[frame]:GetHeight()
    local frameWidth = _G[frame]:GetWidth()
    local spaceX, spaceY = frameWidth + HSpace,
                            frameHeight + VSpace

    x = x + math.floor(row / maxColumn) * spaceX
    y = y + column * spaceY

    -- if it's not new line and we have top anchor position of last frame
    if IRT_Orders.options.positions.prevY then
        y = IRT_Orders.options.positions.prevY + VSpace
    end

    -- if new line
    if math.floor(column / maxColumn) == column / maxColumn and column > 0 then
        y = StartY
        column = 0
        IRT_Orders.options.positions.prevY = nil
    end

    IRT_Orders.options.positions.row = row + 1
    IRT_Orders.options.positions.column = column + 1
    IRT_Orders.options.positions.prevY = y + frameHeight

    if IRT_Orders.options.positions.prevY then
        x, y = x*justifyX, y*justifyY
    end
    return x, y
end

local function Frame_GetPoint(frameName)
    local point = IRT_Orders.FramesOffsets[frameName].point
    local x = IRT_Orders.FramesOffsets[frameName].x
    local y = IRT_Orders.FramesOffsets[frameName].y
    return point, _, _, x, y
end

local function Frame_SetPoint(frame)
    frame:ClearAllPoints()

    if IRT_Orders.Settings.Buttons["Free Frames Alignment"] then
        if IRT_Orders.FramesOffsets then
            local frameName = frame:GetName()
            if IRT_Orders.FramesOffsets[frameName] then
                local point, _, _, x, y = Frame_GetPoint(frameName)
                frame:SetPoint(point, x, y)
                return
            end
        end
    end 

    local x, y = Frame_GetOffsets(frame:GetName())
    frame:SetPoint(IRT_Orders.Frame.Layout.Anchor, UIParent, "CENTER", x, y)   
end

local function Frame_SetHeight(frame)
    local sbAmount = #IRT_Orders.frames[frame:GetName()].query
    local barSpacing = IRT_Orders.StatusBar.Layout.Space
    local barsHeight = IRT_Orders.StatusBar.Layout.Height*sbAmount
    local titleHeight = frame.TitleTexture:GetHeight()
    local h = barsHeight + titleHeight + sbAmount*barSpacing
    frame:SetHeight(h)
end

local function Frame_SetWidth(frame)
    local w = IRT_Orders.StatusBar.Layout.Width
    frame:SetWidth(w)
end

local function Frame_SetSize(frame)
    Frame_SetWidth(frame)
    Frame_SetHeight(frame)
end

local function Frames_SetHeight()
    For_All_Frames(Frame_SetHeight)
end

local function Frames_SetWidth()
    For_All_Frames(Frame_SetWidth)
end

local function Frames_SetSize()
    Frames_SetWidth()
    Frames_SetHeight()
end

---------------------------------------
---------- Frame Title Texture --------
---------------------------------------
    local function Frame_SetTitleTransparency(frame)
        local t = IRT_Orders.Frame.Title.Transparency
        frame.TitleTexture:SetAlpha(t)
    end

    local function Frame_SetTitleWidth(frame)
        local autosize = IRT_Orders.Frame.Title.AutoSize
        local w
        if autosize then
            w = IRT_Orders.StatusBar.Layout.Width
        else
            w = IRT_Orders.Frame.Title.Width
        end
        frame.TitleTexture:SetWidth(w)
    end

    local function Frame_SetTitleHeight(frame)
        local autosize = IRT_Orders.Frame.Title.AutoSize
        local h
        if autosize then
            h = IRT_Orders.StatusBar.Layout.Height
        else
            h = IRT_Orders.Frame.Title.Height
        end
        frame.TitleTexture:SetHeight(h)
        Frame_SetHeight(frame)
    end

    local function Frame_SetTitleSize(frame)
        Frame_SetTitleWidth(frame)
        Frame_SetTitleHeight(frame)
    end

    local function Frame_SetTitleTextureColor(frame)
        local r, g, b, a
        if IRT_Orders.Frame.Title.ClassColor then
            local class = spell_info[frame:GetName()][4]
            local c = RAID_CLASS_COLORS[class]
            r, g, b, a = c.r, c.g, c.b, 1
        else
            r, g, b, a = unpack(IRT_Orders.Frame.Title.Color)
        end
        frame.TitleTexture:SetVertexColor(r, g, b, a)
    end

    local function Frame_SetTitleTexture(frame)
        local texture = IRT_Orders.Frame.Title.Texture
        local Texture = media.Texture[texture]
        frame.TitleTexture:SetTexture(Texture)
    end
    ----> All Frames
    local function Frames_SetTitleWidth()
        For_All_Frames(Frame_SetTitleWidth)
    end

    local function Frames_SetTitleHeight()
        For_All_Frames(Frame_SetTitleHeight)
    end

    local function Frames_SetTitleTransparency()
        For_All_Frames(Frame_SetTitleTransparency)
    end

    local function Frames_SetTitleSize()
        For_All_Frames(Frame_SetTitleSize)
    end

    local function Frames_SetTitleTextureColor()
        For_All_Frames(Frame_SetTitleTextureColor)
    end

    local function Frames_SetTitleTexture()
        For_All_Frames(Frame_SetTitleTexture)
    end

---------------------------------------
----------- Frame Title Icon ----------
---------------------------------------
    local function Frame_SetTitleIconTransparency(frame)
        frame.TitleIcon:SetAlpha(IRT_Orders.Frame.SpellIcon.Transparency)
    end

    local function Frame_SetTitleIcon(frame)
        if IRT_Orders.Frame.SpellIcon.Show then 
            frame.TitleIcon:Show() 
        else
            frame.TitleIcon:Hide() 
        end
    end

    local function Frame_SetTitleIconSize(frame)
        local autosize = IRT_Orders.Frame.SpellIcon.AutoSize
        local s
        if autosize then
            s = frame.TitleTexture:GetHeight()
        else
            s = IRT_Orders.Frame.SpellIcon.Size
        end
        frame.TitleIcon:SetSize(s, s)
    end

    local function Frame_SetTitleIconPosition(frame)
        local anchor = IRT_Orders.Frame.SpellIcon.Anchor
        local relativePoint = IRT_Orders.Frame.SpellIcon.RelativePoint
        local relativeTo = frame.TitleTexture
        local xOfs, yOfs = IRT_Orders.Frame.SpellIcon.xOfs, IRT_Orders.Frame.SpellIcon.yOfs

        frame.TitleIcon:ClearAllPoints()
        frame.TitleIcon:SetPoint(anchor, relativeTo, relativePoint, xOfs, yOfs)

    end
    ----> All Frames
    local function Frames_SetTitleIconTransparency()
        For_All_Frames(Frame_SetTitleIconTransparency)
    end

    local function Frames_SetTitleIcon()
        For_All_Frames(Frame_SetTitleIcon)
    end

    local function Frames_SetTitleIconSize()
        For_All_Frames(Frame_SetTitleIconSize)
    end

    local function Frames_SetTitleIconPosition()
        For_All_Frames(Frame_SetTitleIconPosition)
    end

---------------------------------------
----------- Frame Title Text ----------
---------------------------------------

    local function Frame_TitleTextShow(frame)
        local show = IRT_Orders.Frame.Text.Title.Show
        if show then
            frame.TitleText:Show()
        else
            frame.TitleText:Hide()
        end
    end

    local function Frame_SetTitleTextLen(frame)
        local len = IRT_Orders.Frame.Text.Title.Length
        local name = frame:GetName()

        frame.TitleText:SetText(strShorten(name, len))
    end

    local function Frame_SetTitleTextColor(frame)
        local r, g, b, a
        if IRT_Orders.Frame.Text.Title.ClassColor then
            local spellName = frame:GetName()
            local class = spell_info[spellName][4]
            if class == "DEATH KNIGHT" then class = "DEATHKNIGHT" end
            local c = RAID_CLASS_COLORS[class]
            r, g, b, a = c.r, c.g, c.b, 1
        else
            local c = IRT_Orders.Frame.Text.Title.Color
            r, g, b, a = c[1], c[2], c[3], c[4]
        end
        frame.TitleText:SetVertexColor(r, g, b, a)
    end

    local function Frame_SetTitleTextPoint(frame)
        frame.TitleText:ClearAllPoints()
        local db = IRT_Orders.Frame.Text.Title
        local anchor = db.Anchor
        local relTo = frame.TitleTexture
        local relPoint = db.RelativePoint
        local xOfs = db.xOfs
        local yOfs = db.yOfs
        frame.TitleText:SetPoint(anchor, relTo, relPoint, xOfs, yOfs)
    end

    local function Frame_SetTitleFont(frame)
        local font = IRT_Orders.Frame.Text.Title.Font
        local size = IRT_Orders.Frame.Text.Title.Size
        local shadow = IRT_Orders.Frame.Text.Title.Shadow
        frame.TitleText:SetFont(media.Font[font], size, shadow)
    end

    local function Frames_TitleTextShow()
        For_All_Frames(Frame_TitleTextShow)
    end
    local function Frames_SetTitleTextLen()
        For_All_Frames(Frame_SetTitleTextLen)
    end

    local function Frames_SetTitleTextColor()
        For_All_Frames(Frame_SetTitleTextColor)
    end

    local function Frames_SetTitleTextPoint()
        For_All_Frames(Frame_SetTitleTextPoint)
    end

    local function Frames_SetTitleFont()
        For_All_Frames(Frame_SetTitleFont)
    end
---------------------------------------
------------ Frame Settings -----------
---------------------------------------
    local function Frame_SetTransparency(frame)
        frame:SetAlpha(IRT_Orders.Frame.Layout.Transparency)
    end

    local function Frames_SetTransparency()
        For_All_Frames(Frame_SetTransparency)
    end

    local function Frame_SetScale(frame)
        frame:SetScale(IRT_Orders.Frame.Layout.Scale)
    end

    local function Frames_SetScale()
        For_All_Frames(Frame_SetScale)
    end

    local function Frame_Lock(frame)
        frame:SetMovable(false)
        frame:EnableMouse(false)
    end

    local function Frame_Unlock(frame)
        if not IRT_Orders.Settings.Buttons["Lock Frames"] then
            frame:SetMovable(true)
            frame:EnableMouse(true)
            frame:RegisterForDrag("LeftButton")
            frame:SetScript("OnDragStart", frame.StartMoving)
            frame:SetScript("OnDragStop", function(self)
                frame:StopMovingOrSizing()
                if IRT_Orders.Settings.Buttons["Free Frames Alignment"] then
                    local frameName = frame:GetName()
                    local point, _, _, x, y = frame:GetPoint()
                    IRT_Orders.FramesOffsets = IRT_Orders.FramesOffsets or {}
                    IRT_Orders.FramesOffsets[frameName] = IRT_Orders.FramesOffsets[frameName] or {}
                    IRT_Orders.FramesOffsets[frameName].point = point
                    IRT_Orders.FramesOffsets[frameName].x = x
                    IRT_Orders.FramesOffsets[frameName].y = y
                end
            end)
        end
    end

    local function Frames_Unlock()
        For_All_Frames(Frame_Unlock)
    end

    local function Frames_Alignment(action)
        if IRT_Orders.framesData then
            for frameName in pairs(IRT_Orders.frames) do
                if action == "lock" then
                    Frame_Lock(_G[frameName])
                else
                    Frame_Unlock(_G[frameName])
                end
            end
        end
    end

    local function Frame_QueryElementRemove(frameName)
        for i, j in pairs(IRT_Orders.framesQuery) do
            if j == frameName then 
                table.remove(IRT_Orders.framesQuery, i)
                break
            end
        end

        if arrayLength(IRT_Orders.frames) == 0 then
            IRT_Orders.framesQuery = {}
        end
    end

    local function Frames_Reposition()
        if not IRT_Orders.Settings.Buttons["Free Frames Alignment"] then
            IRT_Orders.options.positions.row = 0
            IRT_Orders.options.positions.column = 0
            IRT_Orders.options.positions.prevY = nil

            if IRT_Orders.framesQuery then
                for _, j in pairs(IRT_Orders.framesQuery) do
                    Frame_SetPoint(_G[j])
                end
            end
        end
    end

    local function Frame_SetTitle(frame)
        local textDB = IRT_Orders.StatusBar.Text.Title
        local iconDB = IRT_Orders.StatusBar.SpellIcon

        local spellName = frame:GetName()
        local spellID = spell_info[frame:GetName()][2]
        local iconTexture = select(3, GetSpellInfo(spellID))

        frame.TitleTexture = frame.TitleTexture or frame:CreateTexture(nil, "LOW")
            frame.TitleTexture:SetPoint("TOPLEFT", frame, "TOPLEFT", 0, 0)
            Frame_SetTitleSize(frame)
            Frame_SetTitleTexture(frame)
            Frame_SetTitleTextureColor(frame)
            --Frame_SetTitleTransparency(frame)

        frame.TitleIcon = frame.TitleIcon or frame:CreateTexture(nil, "OVERLAY", "IRT_Icon")
            frame.TitleIcon:SetTexture(iconTexture)
            Frame_SetTitleIcon(frame)
            Frame_SetTitleIconSize(frame)
            Frame_SetTitleIconPosition(frame)
            Frame_SetTitleIconTransparency(frame)
            Frame_SetTitleIcon(frame)

        frame.TitleText = frame.TitleText or frame:CreateFontString(nil, "OVERLAY")
            Frame_SetTitleFont(frame)
            Frame_SetTitleTextPoint(frame)
            Frame_SetTitleTextColor(frame)
            Frame_SetTitleTextLen(frame)
            Frame_TitleTextShow(frame)
            frame.TitleText:SetJustifyH("LEFT");

        IRT_Orders.frames[spellName].positions.prv_frame = frame.TitleTexture
    end

    local function Frame_SetFrame(frame)
        local lock = IRT_Orders.Settings.Buttons["Lock Frames"]

        Frame_SetTransparency(frame)
        Frame_SetScale(frame)
        Frame_SetTitle(frame)

        if lock then Frame_Lock(frame) else Frame_Unlock(frame) end
    end

---------------------------------------
--------- Status Bar Settings ---------
---------------------------------------
    local function For_All_StatusBars(func)
        for frameName in pairs(IRT_Orders.frames) do
            local f = _G[frameName]
            local statusBars = { f.StatusBarContainer:GetChildren() }
            for _, statusBar in ipairs(statusBars) do
                func(statusBar)
            end
        end 
    end

    local function StatusBar_SetBackgroundColor(statusBar)
        local bc = IRT_Orders.StatusBar.Texture.Background.Color
        statusBar.bg:SetVertexColor(unpack(bc))
    end

    local function StatusBar_SetBackgroundTexture(statusBar)
        local btex = IRT_Orders.StatusBar.Texture.Background.Texture
        local path = media.Texture[btex]
        statusBar.bg:SetTexture(path)
    end

    local function StatusBar_SetColor(statusBar)
        local r, g, b, a

        if IRT_Orders.StatusBar.Texture.Cooldown.ClassColor then
            local container = statusBar:GetParent()
            local frame = container:GetParent()
            local spellName = frame:GetName()

            local class = spell_info[spellName][4]
            if class == "DEATH KNIGHT" then class = "DEATHKNIGHT" end
            local c = RAID_CLASS_COLORS[class]
            r, g, b, a = c.r, c.g, c.b, 1
            a = IRT_Orders.StatusBar.Texture.Cooldown.Transparency
        else
            local c = IRT_Orders.StatusBar.Texture.Cooldown.Color
            r, g, b, a = c[1], c[2], c[3], c[4]
        end
        statusBar:SetStatusBarColor(r, g, b, a)
    end

    local function StatusBar_SetColorAlpha(statusBar)
        local r, g, b, a = statusBar:GetStatusBarColor()
        a = IRT_Orders.StatusBar.Texture.Cooldown.Transparency
        statusBar:SetStatusBarColor(r, g, b, a)
    end

    local function StatusBar_SetTexture(statusBar)
        local tex = IRT_Orders.StatusBar.Texture.Cooldown.Texture
        local path = media.Texture[tex]
        statusBar:SetStatusBarTexture(path)
    end

    local function StatusBar_SetTransparency(statusBar)
        local t = IRT_Orders.StatusBar.Layout.Transparency
        statusBar:SetAlpha(t)
    end

    local function StatusBar_SetHeight(statusBar)
        local h = IRT_Orders.StatusBar.Layout.Height
        statusBar:SetHeight(h)
    end

    local function StatusBar_SetWidth(statusBar)
        local w = IRT_Orders.StatusBar.Layout.Width
        statusBar:SetWidth(w)
    end
    ----> All Status Bars
    local function StatusBars_SetBackgroundColor()
        For_All_StatusBars(StatusBar_SetBackgroundColor)
    end

    local function StatusBars_SetBackgroundTexture()
        For_All_StatusBars(StatusBar_SetBackgroundTexture)
    end

    local function StatusBars_SetColor()
        For_All_StatusBars(StatusBar_SetColor)
    end

    local function StatusBars_SetColorAlpha()
        For_All_StatusBars(StatusBar_SetColorAlpha)
    end

    local function StatusBars_SetTexture()
        For_All_StatusBars(StatusBar_SetTexture)
    end

    local function StatusBars_SetTexture()
        For_All_StatusBars(StatusBar_SetTexture)
    end

    local function StatusBars_SetTransparency()
        For_All_StatusBars(StatusBar_SetTransparency)
    end

    local function StatusBars_Reposition()
        for frameName in pairs(IRT_Orders.frames) do
            local frame = _G[frameName]
            local prv_frame = frame.TitleTexture

            local statusBars = { frame.StatusBarContainer:GetChildren() }

            for _, statusBar in ipairs(statusBars) do
                statusBar:SetPoint("TOPLEFT", prv_frame, "BOTTOMLEFT", 0, -1*IRT_Orders.StatusBar.Layout.Space)
                prv_frame = statusBar
            end
        end
    end

    local function StatusBars_SetHeight()
        For_All_StatusBars(StatusBar_SetHeight)
    end

    local function StatusBars_SetWidth()
        For_All_StatusBars(StatusBar_SetWidth)
    end

---------------------------------------
------ Status Bar Font Settings -------
---------------------------------------
    local function StatusBar_SetDestNameText(statusBar, dstName)
        if dstName and dstName ~= "" then
            str = strShorten(dstName, IRT_Orders.StatusBar.Text.DestName.Length)
            statusBar.destName:SetText("("..str..")")
        end
    end

    local function StatusBar_SetTimePoint(statusBar)
        statusBar.value:ClearAllPoints()
        local db = IRT_Orders.StatusBar.Text.Time
        local anchor = db.Anchor
        local relTo = statusBar
        local relPoint = db.RelativePoint
        local xOfs = db.xOfs
        local yOfs = db.yOfs
        statusBar.value:SetPoint(anchor, relTo, relPoint, xOfs, yOfs)
    end

    local function StatusBar_SetDestNamePoint(statusBar)
        statusBar.destName:ClearAllPoints()
        local db = IRT_Orders.StatusBar.Text.DestName
        local anchor = db.Anchor
        local relTo = statusBar
        local relPoint = db.RelativePoint
        local xOfs = db.xOfs
        local yOfs = db.yOfs
        statusBar.destName:SetPoint(anchor, relTo, relPoint, xOfs, yOfs)
    end

    local function StatusBar_SetSrcNamePoint(statusBar)
        statusBar.unitName:ClearAllPoints()
        local db = IRT_Orders.StatusBar.Text.SrcName
        local anchor = db.Anchor
        local relTo = statusBar
        local relPoint = db.RelativePoint
        local xOfs = db.xOfs
        local yOfs = db.yOfs
        statusBar.unitName:SetPoint(anchor, relTo, relPoint, xOfs, yOfs)
    end

    local function StatusBar_SetDestNameLen(statusBar, name)
        local len = IRT_Orders.StatusBar.Text.DestName.Length
        local name = name

        if not name then 
            name = statusBar.destName:GetText()
        end

        statusBar.destName:SetText(strShorten(name, len))
    end

    local function StatusBar_SetSrcNameLen(statusBar, name)
        local len = IRT_Orders.StatusBar.Text.SrcName.Length
        statusBar.unitName:SetText(strShorten(name, len))
    end

    local function StatusBar_SetTimeFontColor(statusBar)
        local c = IRT_Orders.StatusBar.Text.Time.Color
        statusBar.value:SetTextColor(unpack(c))
    end

    local function StatusBar_SetDestNameFontColor(statusBar)
        local c = IRT_Orders.StatusBar.Text.DestName.Color
        statusBar.destName:SetTextColor(unpack(c))
    end

    local function StatusBar_SetTimeFont(statusBar)
        local font = IRT_Orders.StatusBar.Text.Time.Font
        local size = IRT_Orders.StatusBar.Text.Time.Size
        local shadow = IRT_Orders.StatusBar.Text.Time.Shadow
        statusBar.value:SetFont(media.Font[font], size, shadow)
    end

    local function StatusBar_SetDestNameFont(statusBar)
        local font = IRT_Orders.StatusBar.Text.DestName.Font
        local size = IRT_Orders.StatusBar.Text.DestName.Size
        local shadow = IRT_Orders.StatusBar.Text.DestName.Shadow
        statusBar.destName:SetFont(media.Font[font], size, shadow)
    end

    local function StatusBar_ShowTimeFont(statusBar)
        local show = IRT_Orders.StatusBar.Text.Time.Show
        if show then 
            statusBar.value:Show()
        else
            statusBar.value:Hide()
        end
    end

    local function StatusBar_ShowDestNameFont(statusBar)
        local show = IRT_Orders.StatusBar.Text.DestName.Show
        if show then 
            statusBar.destName:Show()
        else
            statusBar.destName:Hide()
        end
    end

    local function StatusBar_SetSrcNameFont(statusBar)
        local font = IRT_Orders.StatusBar.Text.SrcName.Font
        local size = IRT_Orders.StatusBar.Text.SrcName.Size
        local shadow = IRT_Orders.StatusBar.Text.SrcName.Shadow
        statusBar.unitName:SetFont(media.Font[font], size, shadow)
    end

    --> For All Status Bars
    local function StatusBars_SetTimePoint()
        For_All_StatusBars(StatusBar_SetTimePoint)
    end 

    local function StatusBars_SetDestNamePoint()
        For_All_StatusBars(StatusBar_SetDestNamePoint)
    end

    local function StatusBars_SetSrcNamePoint()
        For_All_StatusBars(StatusBar_SetSrcNamePoint)
    end

    local function StatusBars_SetDestNameLen()
        For_All_StatusBars(StatusBar_SetSrcNamePoint)
    end

    local function StatusBars_SetSrcNameLen()
        for frameName in pairs(IRT_Orders.frames) do
            for unitName in pairs(IRT_Orders.frames[frameName].order) do
                local statusBar = IRT_Orders.frames[frameName].order[unitName][5]
                StatusBar_SetSrcNameLen(statusBar, unitName)
            end
        end
    end

    local function StatusBars_SetTimeFontColor()
        For_All_StatusBars(StatusBar_SetTimeFontColor)
    end

    local function StatusBars_SetDestNameFontColor()
        For_All_StatusBars(StatusBar_SetDestNameFontColor)
    end

    local function StatusBars_SetTimeFont()
        For_All_StatusBars(StatusBar_SetTimeFont)
    end

    local function StatusBars_SetDestNameFont()
        For_All_StatusBars(StatusBar_SetDestNameFont)
    end

    local function StatusBars_ShowTimeFont()
        For_All_StatusBars(StatusBar_ShowTimeFont)
    end

    local function StatusBars_ShowDestNameFont()
        For_All_StatusBars(StatusBar_ShowDestNameFont)
    end

    local function StatusBars_SetSrcNameFont()
        For_All_StatusBars(StatusBar_SetSrcNameFont)
    end

---------------------------------------
------ Status Bar Icon Settings -------
---------------------------------------
    local function StatusBar_Icon_Show(statusBar)
        local show = IRT_Orders.StatusBar.State.Show
        if show then
            statusBar.icon:Show()
        else
            statusBar.icon:Hide()
        end
    end

    local function StatusBar_Icon_Size(statusBar)
        local autosize = IRT_Orders.StatusBar.State.AutoSize
        local s

        if autosize then
            s = statusBar:GetHeight()
        else
            s = IRT_Orders.StatusBar.State.Size
        end

        statusBar.icon:SetSize(s, s)
    end

    local function StatusBar_Icon_Point(statusBar)
        statusBar.icon:ClearAllPoints()
        local db = IRT_Orders.StatusBar.State
        local anchor = db.Anchor
        local relTo = statusBar
        local relPoint = db.RelativePoint
        local xOfs = db.xOfs
        local yOfs = db.yOfs
        statusBar.icon:SetPoint(anchor, relTo, relPoint, xOfs, yOfs)
    end

    local function StatusBar_Icon_Alpha(statusBar)
        local t = IRT_Orders.StatusBar.State.Transparency
        statusBar.icon:SetAlpha(t)
    end
    
    -- For All Status Bars
    local function StatusBars_Icon_Show()
        For_All_StatusBars(StatusBar_Icon_Show)
    end

    local function StatusBars_Icon_Size()
        For_All_StatusBars(StatusBar_Icon_Size)
    end

    local function StatusBars_Icon_Point()
        For_All_StatusBars(StatusBar_Icon_Point)
    end

    local function StatusBars_Icon_Alpha()
        For_All_StatusBars(StatusBar_Icon_Alpha)
    end

local function Frame_AddStatusBar(unitName, frameName)
    local frame = _G[frameName]
    local statusBarName = "StatusBar"..frameName..unitName
    frame.StatusBarContainer = frame.StatusBarContainer or CreateFrame("Frame", nil, frame)

    -- Setting up Status Bar
    frame.StatusBarContainer.StatusBar = _G[statusBarName] or CreateFrame("StatusBar", statusBarName, frame.StatusBarContainer)
    -- Cooldown Texture
    local statusBar = frame.StatusBarContainer.StatusBar
        StatusBar_SetWidth(statusBar)
        StatusBar_SetHeight(statusBar)
        StatusBar_SetTransparency(statusBar)
        StatusBar_SetTexture(statusBar)
        StatusBar_SetColor(statusBar)
        statusBar:GetStatusBarTexture():SetHorizTile(false)
        statusBar:GetStatusBarTexture():SetVertTile(false)

    statusBar.bg = statusBar.bg or statusBar:CreateTexture(nil, "BACKGROUND")
        StatusBar_SetBackgroundTexture(statusBar)
        StatusBar_SetBackgroundColor(statusBar)
        statusBar.bg:SetAllPoints(true)

    statusBar.value = statusBar.value or statusBar:CreateFontString(nil, "OVERLAY")
        StatusBar_SetTimeFont(statusBar)
        StatusBar_SetTimeFontColor(statusBar)
        StatusBar_SetTimePoint(statusBar)
        statusBar.value:SetJustifyH("CENTER")
        statusBar.value:SetText("")
        statusBar:SetMinMaxValues(0, spell_info[frameName][1])
        statusBar:SetValue(0)
        StatusBar_ShowTimeFont(statusBar)

    statusBar.unitName = statusBar.unitName or statusBar:CreateFontString(nil, "ARTWORK")
        StatusBar_SetSrcNameFont(statusBar)
        StatusBar_SetSrcNameLen(statusBar, unitName)
        StatusBar_SetSrcNamePoint(statusBar)
        statusBar.unitName:SetJustifyH("LEFT")
        statusBar.unitName:SetJustifyV("CENTER")

    statusBar.destName = statusBar.destName or statusBar:CreateFontString(nil, "ARTWORK")
        StatusBar_SetDestNameFont(statusBar)
        StatusBar_SetDestNameFontColor(statusBar)
        StatusBar_SetDestNamePoint(statusBar)
        statusBar.destName:SetJustifyV("CENTER")
        statusBar.destName:SetText("")

    statusBar.icon = statusBar.icon or statusBar:CreateTexture(nil, "LOW", "IRT_Icon")
        StatusBar_Icon_Point(statusBar)
        StatusBar_Icon_Size(statusBar)
        StatusBar_Icon_Alpha(statusBar)
        StatusBar_Icon_Show(statusBar)

    local prv_frame = IRT_Orders.frames[frameName].positions.prv_frame
    statusBar:SetPoint("TOPLEFT", prv_frame, "BOTTOMLEFT", 0, -1*IRT_Orders.StatusBar.Layout.Space)
    IRT_Orders.frames[frameName].positions.prv_frame = _G[statusBarName]

    statusBar:Show()
    return statusBar
end

local function Manage_UnitIDs()
    if IRT_Orders.unitIDs then
        for i in pairs(IRT_Orders.unitIDs) do
            local unitID = IRT_Orders.unitIDs[i][1]

            -- Fixing UnitID if it's changed
            if unitID == "gone" or UnitName(unitID) ~= i then
                unitID = raidIbyName(i) or partyIbyName(i)
                if unitID then
                    IRT_Orders.unitIDs[i][1] = unitID
                    if IRT_Orders.unitIDs[i][2] then
                        for spellName in pairs(IRT_Orders.unitIDs[i][2]) do
                            Frame_SetOrder(spellName)
                            if _G["IRT_Orders_DeathUpdate"..i] then
                                _G["IRT_Orders_DeathUpdate"..i]:Hide()
                            end
                        end
                    end
                else
                    unitID = "gone"
                    IRT_Orders.unitIDs[i][1] = unitID
                end
                
                if unitID == "gone" then
                    for spellName in pairs(IRT_Orders.unitIDs[i][2]) do
                        local frameName = "CDOnUpdate"..i..spellName
                        if _G[frameName] then
                            if _G[frameName]:IsShown() then 
                                _G[frameName]:Hide() 

                                for unitName in pairs(IRT_Orders.frames[spellName].order) do
                                    local frame = IRT_Orders.frames[spellName].order[unitName][5]
                                    frame:SetValue(0)
                                    local statusBarName = "StatusBar"..spellName..unitName
                                    _G[statusBarName].value:SetText("")
                                end
                            end
                        end

                        Frame_SetOrder(spellName)
                    end 
                else
                    for spellName in pairs(IRT_Orders.unitIDs[i][2]) do
                        local expTime = IRT_Orders.frames[spellName].order[i][2]
                        if GetTime() - expTime < 0 then
                            local frameName = "CDOnUpdate"..i..spellName
                            if not _G[frameName]:IsShown() then
                                StatusBar_SetUpdateTimer(spellName, i)
                            end
                        end
                    end 
                end
            end
        end
    end
end

local function insUnitDeathArray(unitID)
    deathsT[unitID] = UnitIsDeadOrGhost(unitID)
end

local function Button_SendData()
    if IsRaidOfficer() or IsPartyLeader() then
        if _G["IRT_SendDataCheckButton"]:GetChecked() then
            IRT_Orders.Settings.Buttons["Send Data"] = true
            _G["IRT_SendDataCheckButtonText"]:SetTextColor(.99,.81, 0)
            return
        end
    end

    _G["IRT_SendDataCheckButtonText"]:SetTextColor(.5,.5,.5)
end


local function onEvent(frame)
    local frmCntnr = IRT_Orders.frames -- frame container
    frame:SetScript("OnEvent", function(self, event, ...)
        if event == "COMBAT_LOG_EVENT_UNFILTERED" then
            local _, subEvent, _, unitName, _, _, dstName, _, spellID, spellName = ...
            if frmCntnr[spellName] then
                if frmCntnr[spellName].order[unitName] then
                    if subEvent == "SPELL_CAST_SUCCESS" then
                        local statusBar = frmCntnr[spellName].order[unitName][5]

                        StatusBar_SetDestNameText(statusBar, dstName)
                        
                        if spellName == "Misdirection" or spellName == "Tricks of the Trade" then 
                            return 
                        end

                        local table = frmCntnr[spellName].order[unitName]

                        frmCntnr[spellName].USS = GetTime()
                        local spellCD = spell_info[spellName][1]
                        table[1] = false
                        table[2] = GetTime() + spellCD
                        StatusBar_SetUpdateTimer(spellName, unitName)
                    elseif subEvent == "SPELL_AURA_APPLIED" then
                        -- Misdirection / ToTs
                        if spellID == 57934 or spellID == 34477 then
                            StatusBar_SetBuffTimer(spellName, unitName)
                        end
                    elseif subEvent == "SPELL_AURA_REMOVED" then
                        if spellID == 57934 or spellID == 34477 then
                            local unitCntnr = frmCntnr[spellName].order[unitName]
                            local spellCD = spell_info[spellName][1]
                            unitCntnr[1] = false
                            unitCntnr[2] = GetTime() + spellCD
                            local buffOnUpdate = "BuffOnUpdate"..spellName..unitName
                            if _G[buffOnUpdate] then _G[buffOnUpdate]:Hide() end
                            StatusBar_SetUpdateTimer(spellName, unitName)
                        end
                    end
                end
            elseif spellName == "Readiness" and frmCntnr["Misdirection"] then
                if subEvent == "SPELL_CAST_SUCCESS" then
                    local updTimer = "CDOnUpdate"..unitName.."Misdirection"
                    if _G[updTimer] then
                        _G[updTimer]:Hide() 
                        local frame = IRT_Orders.frames["Misdirection"].order[unitName][5]
                        frame.destName:SetText("")
                        frame.value:SetText("")
                        frame:SetValue(0)

                        local unitCntnr = frmCntnr["Misdirection"].order[unitName]
                        unitCntnr[1] = true
                        unitCntnr[2] = 0

                        Frame_SetOrder("Misdirection")
                    end
                end
            end
        elseif event == "UNIT_HEALTH" then
            if IRT_Orders.unitIDs[UnitName(arg1)] then
                local isUnitDeadOrGhost = UnitIsDeadOrGhost(arg1)
                if isUnitDeadOrGhost ~= deathsT[arg1] then
                    for frameName in pairs(IRT_Orders.unitIDs[UnitName(arg1)][2]) do
                        Frame_SetOrder(frameName)
                    end
                    deathsT[arg1] = isUnitDeadOrGhost
                end
            end
        elseif event == "UNIT_ENTERED_VEHICLE" or event == "UNIT_EXITED_VEHICLE" then
            if UnitExists("boss1") then
                if UnitName("boss1") == "The Lich King" then
                    if ( UnitHealth("boss1")*100/UnitHealthMax("boss1") ) > 42 then
                        local name = UnitName(arg1)

                        if IRT_Orders.unitIDs[name] then
                            for spellName in pairs(IRT_Orders.unitIDs[name][2]) do
                                Frame_SetOrder(spellName)
                            end 
                        end
                    end
                end
            end
        elseif event == "RAID_ROSTER_UPDATE" or event == "PARTY_MEMBERS_CHANGED" then
            -- fixing UnitIDs
            Manage_UnitIDs()
            Button_SendData()

            for frameName in pairs(IRT_Orders.frames) do
                if IRT_Orders.frames[frameName].lock then
                    Frame_SetOrder(frameName)
                end
            end
        end
    end)
end

local function Frame_RaidOfficerCheck(frame)
    frame:RegisterEvent("RAID_ROSTER_UPDATE")
    frame:RegisterEvent("PARTY_MEMBERS_CHANGED")

    frame:SetScript("OnEvent", function(self, event, ...)
        if event == "RAID_ROSTER_UPDATE" or event == "PARTY_MEMBERS_CHANGED" then
            Button_SendData()
        end
    end)
end

local function Order_SetString(frameName)
    local text = "No players in order"
    if IRT_Orders.frames then
        if IRT_Orders.frames[frameName] then text = IRT_Orders.frames[frameName].strLine end
    end
    IRT_OrdersConfig_Menu_ListFont:SetText(text)
end

-- Sending AddOn Message based on "subMsg" and "msg" strings
local function AddonSM(subMsg, msg)
    SendAddonMessage(subMsg, msg, "RAID", UnitName("player"))
end

-- Adds "unitName" in "orderName" order
local function AddUnitToOrder(orderName, unitName, onImportCall)
    local msg, state = "", false

    -- Build Order Table if the order doesn't exist
    if not IRT_Orders.frames[orderName] then
        IRT_Orders.frames[orderName] = {}
        IRT_Orders.frames[orderName].lock = false
        IRT_Orders.frames[orderName].USS = 0
        IRT_Orders.frames[orderName].order = {} 
        IRT_Orders.frames[orderName].strLine = ""
        IRT_Orders.frames[orderName].query = {}
    end

    if not IRT_Orders.frames[orderName].lock then
        local unitID = raidIbyName(unitName) or partyIbyName(unitName)
        local unitClass = false

        if onImportCall and not unitID then 
            unitID = "gone"
            unitClass = spell_info[orderName][3]
        end


        if unitID then
            if not unitClass then unitClass = UnitClass(unitID) end
            if unitClass == spell_info[orderName][3] then
                if not IRT_Orders.frames[orderName].order[unitName] then
                    -- Addint Unit to UnitQuery
                    insertName(unitName, IRT_Orders.frames[orderName].order, unitID,
                               IRT_Orders.frames[orderName].query)

                    if IRT_Orders.frames[orderName].strLine == "" then
                        IRT_Orders.frames[orderName].strLine =
                            "|cFF0BD6FFPlayers in order:|r\n" .. unitName
                    else
                        IRT_Orders.frames[orderName].strLine =
                            IRT_Orders.frames[orderName].strLine .. ", " .. unitName
                    end

                    IRT_OrdersConfig_Menu_ListFont:SetText(
                        IRT_Orders.frames[orderName].strLine)
                    msg = "|cFF00FF00" .. unitName ..
                              "|r has successfully added to Hand of Sacrifice Order!"
                    state = true
                else
                    msg = "Error: Current name: |cFFFF0000" .. unitName ..
                              "|r is already in Hand of Sacrifice Order!"
                end
            else
                msg = "Error: Current unit: |cFFFF0000" .. unitName ..
                          "|r is not " .. spell_info[orderName][3]
            end
        else
            msg = "Error: Can't found Unit in the raid"
        end
    else
        msg = orderName ..
                  " order is locked! You can't add more names unless you decide to wipe it."
    end
    return state, msg
end

local function LockOrder(orderName)
    local msg, state = "", false
    if IRT_Orders.frames[orderName] then
        if not IRT_Orders.frames[orderName].lock then
            local arrayLen = arrayLength(IRT_Orders.frames[orderName].order)
            if arrayLen > 0 then
                IRT_Orders.framesData = IRT_Orders.framesData or {}
                IRT_Orders.framesData[orderName] = _G[orderName] or CreateFrame("Frame", orderName, UIParent)
                local frame = IRT_Orders.framesData[orderName]

                IRT_Orders.framesQuery = IRT_Orders.framesQuery or {}
                insert_framesQuery(IRT_Orders.framesQuery, orderName)

                IRT_Orders.frames[orderName].positions = {}

                Frame_SetFrame(frame)

                local gbFrame = _G["IRT_Orders_GlobalFrame"] or CreateFrame("Frame", "IRT_Orders_GlobalFrame")
                    onEvent(gbFrame)
                    gbFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
                    gbFrame:RegisterEvent("UNIT_HEAlTH")
                    gbFrame:RegisterEvent("UNIT_ENTERED_VEHICLE")
                    gbFrame:RegisterEvent("UNIT_EXITED_VEHICLE")
                    gbFrame:RegisterEvent("RAID_ROSTER_UPDATE")
                    gbFrame:RegisterEvent("PARTY_MEMBERS_CHANGED")

                -- Setting up UniIDs etc.
                IRT_Orders.unitIDs = IRT_Orders.unitIDs or {}

                for _, i in pairs(IRT_Orders.frames[orderName].query) do
                    local unitID = IRT_Orders.frames[orderName].order[i][3]
                    local name = IRT_Orders.frames[orderName].order[i][4]

                    -- Building unitIDs array, that manages UnitIDs 
                    if not IRT_Orders.unitIDs[name] then
                        IRT_Orders.unitIDs[name] = {}
                        IRT_Orders.unitIDs[name][1] = unitID
                        IRT_Orders.unitIDs[name][2] = {}
                    end

                    IRT_Orders.unitIDs[name][2][orderName] = IRT_Orders.unitIDs[name][2][orderName] or IRT_Orders.framesData[orderName]
                    
                    -- Adds Unit to Death Array if is Dead
                    insUnitDeathArray(unitID)

                    -- Setting UP Status Bar
                    IRT_Orders.frames[orderName].positions.row = IRT_Orders.frames[orderName].positions.row or 0
                    IRT_Orders.frames[orderName].order[i][5] = Frame_AddStatusBar(name, orderName)
                end

                Frame_SetSize(frame)
                Frame_SetPoint(frame)

                IRT_Orders.frames[orderName].lock = true
                msg = orderName .. " has been Locked!"
                state = true

                -- if spells were on CD
                for i in pairs(IRT_Orders.frames[orderName].order) do
                    local CDexp = IRT_Orders.frames[orderName].order[i][2]
                    local name = IRT_Orders.frames[orderName].order[i][4]

                    if GetTime() - CDexp < 0 then
                        StatusBar_SetUpdateTimer(orderName, name)
                    else
                        IRT_Orders.frames[orderName].order[i][1] = true
                    end
                end

                local result = Frame_SetOrder(orderName)
                frame:Show()

                if IRT_Orders.Settings.Buttons["Send Data"] then
                    if IsRaidOfficer() or IsPartyLeader() then
                        for _, i in pairs(IRT_Orders.frames[orderName].query) do
                            AddonSM(orderName, "IRT Add".." "..i)
                        end
                        AddonSM(orderName, "IRT Lock")
                    end
                end          
            else
                msg = orderName ..
                          " order can not be locked due lack of any unit"
            end
        else
            msg = orderName .. " order already exists!"
        end
    else
        msg = orderName .. " order can not be locked due lack of any unit"
    end
    return state, msg
end

local function Frame_ClearAllPoints(frame)
    local statusBars = { frame.StatusBarContainer:GetChildren() }

    for _, statusBar in ipairs(statusBars) do
        statusBar:ClearAllPoints()
    end

    frame.StatusBarContainer:ClearAllPoints()
    frame:ClearAllPoints()
end

local function Frame_HideAll()
end

local function WipeOrder(orderName)
    local msg, state = orderName .. " order doesn't exist", false
    if IRT_Orders.frames[orderName] then
        if IRT_Orders.frames[orderName].lock then
            local frame = IRT_Orders.framesData[orderName]
            if frame then
                for i in pairs(IRT_Orders.frames[orderName].order) do
                    local name = IRT_Orders.frames[orderName].order[i][4]

                    IRT_Orders.unitIDs[name][2][orderName] = nil
                    if arrayLength(IRT_Orders.unitIDs[name][2]) == 0 then
                        IRT_Orders.unitIDs[name] = nil
                    end

                    IRT_Orders.frames[orderName].order[i][5]:Hide()
                end

                Frame_ClearAllPoints(frame)
                frame:UnregisterAllEvents()
                frame:Hide()
                
                IRT_Orders.frames[orderName] = nil

                Order_SetString(orderName)
                Frame_QueryElementRemove(orderName)
                Frames_Reposition()

                state = true

                if arrayLength(IRT_Orders.frames) == 0 then
                    _G["IRT_Orders_GlobalFrame"]:UnregisterAllEvents()
                end

                if IRT_Orders.Settings.Buttons["Send Data"] then
                    if IsRaidOfficer() or IsPartyLeader() then
                        AddonSM(orderName, "IRT Wipe")
                    end
                end
            end
        end
    end

    return state, msg
end

-- Wipes every existed order
local function WipeAll()
    local msg, state = "All orders have been successfully wiped", false

    for i in pairs(IRT_Orders.frames) do
        WipeOrder(i)
        if _G[i.."RRUUpdate"] then 
            _G[i.."RRUUpdate"]:Hide()
        end
        IRT_Orders.frames[i] = nil
    end

    IRT_Orders.frames = {}
    IRT_Orders.options.positions.row = 0
    IRT_Orders.options.positions.column = 0
    IRT_Orders.options.positions.prevY = nil
    Order_SetString("clear")
    return state, msg
end
 
-- Sends data about every frame to the raid
local function SendData()
    if IRT_Orders.Settings.Buttons["Send Data"] then
        if IsRaidOfficer() or IsPartyLeader() then
            for j in pairs(IRT_Orders.frames) do
                for _, i in pairs(IRT_Orders.frames[j].query) do
                    AddonSM(j, "IRT Add".." "..i)
                end
                AddonSM(j, "IRT Lock")
            end
        end
    end
end

function addonTable.RefreshFrames()
    if IRT_Orders.framesQuery then
        if arrayLength(IRT_Orders.frames) > 0 then
            IRT_Orders.options.positions.row = 0
            IRT_Orders.options.positions.column = 0
            IRT_Orders.options.positions.prevY = nil
            for _, i in pairs(IRT_Orders.framesQuery) do
                if IRT_Orders.frames[i].lock then
                    IRT_Orders.frames[i].lock = false

                    local frame = CreateFrame("Frame", i, UIParent)
                    LockOrder(i)
                end
            end
        end
    end

    Manage_UnitIDs()

    local frameRaidOfficer = CreateFrame("Frame", "IRT_ROC")
    Frame_RaidOfficerCheck(frameRaidOfficer)
    Button_SendData()
end

-------------------------------------------
--------- ADDON_CHAT_MSG RECIEVER ---------
-------------------------------------------

local function ImportCall()
    IRTlastImport = IRTlastImport or 0
    if GetTime() - IRTlastImport > 1 then
        SendData()
        IRTlastImport = GetTime()
    end
end

-- registers and adding names to orders
local addonMSG = CreateFrame("Frame")
addonMSG:RegisterEvent("CHAT_MSG_ADDON")
function addonMSG:OnEvent(event, ...)
    local subMsg, msg, channel, srcName = ...
    -- if recieved message was sent by RaidOfficer (assit / raid leader)
    if string.find(msg, "IRT") and srcName ~= UnitName("player") then
        local UnitIsOfficer = UnitIsRaidOfficer(raidIbyName(srcName)) or UnitIsPartyLeader(partyIbyName(srcName))
        if UnitIsOfficer then
            -- clearing every already existed order
            local messageW = {}

            for i in string.gmatch(msg, "%S+") do
                table.insert(messageW, i)
            end

            local orderName, action, unitName, classReq = subMsg, messageW[2], messageW[3]

            -- Adds only Class Related Orders (adds every order if you're officer or choose to ignore ClassFilter)
            if orderName == "Import" and srcName ~= UnitName("player") then
                ImportCall()
            elseif spell_info[orderName] then
                if UnitClass("player") == spell_info[orderName][3] or IRT_Orders.Settings.Buttons["Ignore Class Filter"] then
                    if action == "Add" then
                        AddUnitToOrder(orderName, unitName, true)
                    elseif action == "Lock" then
                        LockOrder(orderName)
                    elseif action == "Wipe" then
                        WipeOrder(orderName)
                    end
                end
            end
        elseif subMsg == "Import" then
            ImportCall()
        elseif string.find(msg, "SendCD") then
            local messageW = {}

            for i in string.gmatch(msg, "%S+") do
                table.insert(messageW, i)
            end

            local orderName, action, CD = subMsg, messageW[2], tonumber(messageW[3])

            if IRT_Orders.frames then
                if IRT_Orders.frames[orderName] then
                    if IRT_Orders.frames[orderName][srcName] then
                        IRT_Orders.frames[orderName][srcName][2] = CD
                    end
                end
            end
        end
    end
end
addonMSG:SetScript("OnEvent", addonMSG.OnEvent)


-------------------------------------------
--------- SPELL ORDER BUILDER GUI ---------
-------------------------------------------
local selectedName
IRT_selectedValue = nil

local function Frames_SetSavedPoint()
    if IRT_Orders.FramesOffsets then
        for frame in pairs(IRT_Orders.frames) do
            if IRT_Orders.FramesOffsets[frame] then
                _G[frame]:ClearAllPoints()
                local point, _, _, x, y = Frame_GetPoint(frame)
                _G[frame]:SetPoint(point, x, y)
            end
        end
    end
end

function IRT_CreateSpellOrder(frame)
    IRT_selectedValue = IRT_selectedValue or "Choose order"

    frame:SetPoint("CENTER", _G["IRT_OrdersConfig"], "CENTER", 0, 0)
    UIDropDownMenu_SetWidth(frame, 123)
    UIDropDownMenu_SetText(frame, IRT_selectedValue)

    UIDropDownMenu_Initialize(frame, function(self, level, menuList)
        local info = UIDropDownMenu_CreateInfo()
        if (level or 1) == 1 then
            for i=1, 10 do
                local class = classes[i]
                if arrayLength(spell_info_sorted[class]) > 0 then
                    info.text = class

                    if IRT_selectedValue then
                        if spell_info_sorted[class][IRT_selectedValue] then
                            info.checked = true
                        else
                            info.checked = false
                        end
                    else
                        info.checked = false
                    end

                    info.menuList, info.hasArrow = i, true
                    UIDropDownMenu_AddButton(info)
                end
            end
        else
            local class = classes[menuList]
            if arrayLength(spell_info_sorted[class]) > 0 then
                local spellTable = {}

                for spell in pairs(spell_info_sorted[class]) do
                    table.insert(spellTable, spell)
                end

                for i=1, #spellTable do
                    spellName = spellTable[i]
                    info.text, info.arg1, info.checked = spellName, spellName, spellName == IRT_selectedValue
                    info.func = self.SetValue
                    info.icon = select(3, GetSpellInfo(spell_info[spellName][2]))
                    UIDropDownMenu_AddButton(info, level)
                end
            end
        end
    end)

    function frame:SetValue(newValue)
        IRT_selectedValue = newValue
        UIDropDownMenu_SetText(frame, newValue)
        Order_SetString(newValue)
        CloseDropDownMenus()
    end
end


-- Buttons Func
function InvisusRT_OnAddName(ebFrame, value, len)
    local len = ebFrame:GetNumLetters()
    if len == 0 then
        if UnitExists("target") then
            unitID = raidIbyName(UnitName("target")) or partyIbyName(UnitName("target"))
            if SpellMatchesClass(unitID, IRT_selectedValue) then
                value = UnitName("target")
            else
                IRT_OrdersConfig_Notification:AddMessage(
                    "|cFFFF0000Nickname is not valid!|r", 1, 1, 1, 53, 3)
                return
            end
        else
            IRT_OrdersConfig_Notification:AddMessage(
                    "|cFFFF0000Nickname is not valid!|r", 1, 1, 1, 53, 3)
            return
        end
    end
    local state, msg = AddUnitToOrder(IRT_selectedValue, value)
    if state then
        IRT_OrdersConfig_Notification:AddMessage(
            "|cFF93FF00" .. value ..
                " has been successfully added to the order list|r", 1, 1, 1, 53,
            3)
        IRT_OrdersConfig_EditBox:SetText("")
    else
        IRT_OrdersConfig_Notification:AddMessage(msg, 1, 1, 1, 53,
                                                              3)
    end
end

function InvisusRT_OnOrderLock(orderName)
    local state, msg = LockOrder(orderName)
    if state then
        -- IRT_OrdersConfig_EditBox:SetText("")
        IRT_OrdersConfig_Notification:AddMessage(
            "|cFF93FF00" .. orderName ..
                " order has been successfully LOCKED!|r", 1, 1, 1, 53, 3)
    else
        IRT_OrdersConfig_Notification:AddMessage(msg, 1, 1, 1, 53,
                                                              3)
    end
end

function InvisusRT_OnOrderWipe(orderName)
    local state, msg = WipeOrder(orderName)
    if state then
        IRT_OrdersConfig_Notification:AddMessage(
            "|cFF93FF00" .. orderName .. " order has been WIPED!|r", 1, 1, 1,
            53, 3)
    else
        IRT_OrdersConfig_Notification:AddMessage(msg, 1, 1, 1, 53,
                                                              3)
    end
end

function InvisusRT_OnWipeAll()
    local state, msg = WipeAll()
    if state then 
        IRT_OrdersConfig_Notification:AddMessage(msg, 1, 1, 1, 53, 3)
    end
end

function InvisusRT_OnSendData()
    SendData()
end

function InvisusRT_OnImportData()
    SendAddonMessage("Import", "IRT", "RAID", UnitName("player"))
    IRT_OrdersConfig_Notification:AddMessage("Request for Orders was sent.", 1, 1, 1, 53, 3)
end

function InvisusRT_OnSendDataCheckButton()
    Button_SendData()
end

---------------------------------------
----- SPELL ORDERS OPTION TAB GUI -----
---------------------------------------
local StatusTable

local function SpellOrders_GUI(container, AceGUI)
    local AceGUI = AceGUI
    local GUI = addonTable.GUI

    local frame = AceGUI:Create("TreeGroup")

    local spellOrders_settings = {
        {value = 1, text = "General"},
        {value = 2, text = "Frame", children = {
                {value = 1, text = "General Options"},
                {value = 2, text = "Title Texture"},
                {value = 3, text = "Spell Icon"},
                {value = 4, text = "Title Text"}
            }
        },
        {value = 3, text = "StatusBar", children = {
                {value = 1, text = "General Options"},
                {value = 2, text = "Bar Texture"},
                {value = 3, text = "State Icon"},
                {value = 4, text = "Text"}
            }
        },
        {value = 4, text = "About"}
    }

    local anchors = {
        ["TOP"] = "TOP",
        ["CENTER"] = "CENTER",
        ["BOTTOM"] = "BOTTOM",
        ["LEFT"] = "LEFT",
        ["RIGHT"] = "RIGHT",
        ["TOPLEFT"] = "TOPLEFT",
        ["TOPRIGHT"] = "TOPRIGHT",
        ["BOTTOMLEFT"] = "BOTTOMLEFT",
        ["BOTTOMRIGHT"] = "BOTTOMRIGHT",
    }

    local shadows = {
        ["OUTLINE"] = "OUTLINE",
        ["THICKOUTLINE"] = "THICKOUTLINE",
        ["MONOCHROME"] = "MONOCHROME",
        ["None"] = "None",
    }

    local textures = {}
    for i in pairs(media.Texture) do
        textures[i] = i
    end

    local fonts = {}
    for i in pairs(media.Font) do
        fonts[i] = i 
    end
    
    ----> Sections
    local function Set_StatusBar_Section(container)
        local title = AceGUI:Create("Heading")
            title:SetText("Status Bar Settings")
            title:SetFullWidth(true)
            container:AddChild(title)
            

        local GeneralOptions = AceGUI:Create("Button")
            GeneralOptions:SetText("General Options")
            GeneralOptions:SetWidth(150)
            GeneralOptions:SetCallback("OnClick", 
                function()
                    frame:SelectByPath(3)
                    frame:SelectByPath("3\0011")
                    frame:RefreshTree()
                end)
            container:AddChild(GeneralOptions)

        local BarTexture = AceGUI:Create("Button")
            BarTexture:SetText("Bar Texture")
            BarTexture:SetWidth(150)
            BarTexture:SetCallback("OnClick", 
                function()
                    frame:SelectByPath(3)
                    frame:SelectByPath("3\0012")
                    frame:RefreshTree()
                end)
            container:AddChild(BarTexture)

        local StateIcon = AceGUI:Create("Button")
            StateIcon:SetText("State Icon")
            StateIcon:SetWidth(150)
            StateIcon:SetCallback("OnClick", 
                function()
                    frame:SelectByPath(3)
                    frame:SelectByPath("3\0013")
                    frame:RefreshTree()
                end)
            container:AddChild(StateIcon)

        local Text = AceGUI:Create("Button")
            Text:SetText("Text")
            Text:SetWidth(150)
            Text:SetCallback("OnClick", 
                function()
                    frame:SelectByPath(3)
                    frame:SelectByPath("3\0014")
                    frame:RefreshTree()
                end)
            container:AddChild(Text)
    end

    local function Set_StatusBar_StateIcon(container)

        local function StatusBar_Icon_Size_CallBack(container, widget, value)
            IRT_Orders.StatusBar.State.Size = value
            container.editbox:ClearFocus()
            StatusBars_Icon_Size()
        end

        local function StatusBar_Icon_Transp_CallBack(container, widget, value)
            IRT_Orders.StatusBar.State.Transparency = value/100
            StatusBars_Icon_Alpha()
        end

        local function StatusBar_Icon_yOfs_CallBack(container, widget, value)
            IRT_Orders.StatusBar.State.yOfs = value
            StatusBars_Icon_Point()
        end

        local function StatusBar_Icon_xOfs_CallBack(container, widget, value)
            IRT_Orders.StatusBar.State.xOfs = value
            StatusBars_Icon_Point()
        end

        local function StatusBar_Icon_RelativePoint_CallBack(container, widget, value)
            IRT_Orders.StatusBar.State.RelativePoint = value
            StatusBars_Icon_Point()
        end

        local function StatusBar_Icon_Anchor_CallBack(container, widget, value)
            IRT_Orders.StatusBar.State.Anchor = value
            StatusBars_Icon_Point()
        end

        local function StatusBar_Icon_AutoSize_CallBack(container, widget, value)
            IRT_Orders.StatusBar.State.AutoSize = value
            StatusBars_Icon_Size()

            GUI.StatusBar.StateIcon.Size:SetDisabled(value)
        end

        local function StatusBar_Icon_Show_CallBack(container, widget, value)
            IRT_Orders.StatusBar.State.Show = value
            StatusBars_Icon_Show()

            local stateIcon = GUI.StatusBar.StateIcon
            for widget in pairs(stateIcon) do
                if widget ~= "Show" then
                    if not stateIcon.Show:GetValue() then
                        stateIcon[widget]:SetDisabled(true)
                    else
                        stateIcon[widget]:SetDisabled(false)
                    end
                end
            end
        end

        GUI.StatusBar = GUI.StatusBar or {}
        GUI.StatusBar.StateIcon = GUI.StatusBar.StateIcon or {}

        local title = AceGUI:Create("Heading")
            title:SetText("Status Bar State Icon Settings")
            title:SetFullWidth(true)
            container:AddChild(title)

        local Show = AceGUI:Create("CheckBox")
            Show:SetLabel("Enable Icon")
            Show:SetValue(IRT_Orders.StatusBar.State.Show)
            Show:SetCallback("OnValueChanged", StatusBar_Icon_Show_CallBack)
            container:AddChild(Show)
            GUI.StatusBar.StateIcon.Show = Show

        local AutoSize = AceGUI:Create("CheckBox")
            AutoSize:SetLabel("Autosize")
            AutoSize:SetValue(IRT_Orders.StatusBar.State.AutoSize)
            AutoSize:SetCallback("OnValueChanged", StatusBar_Icon_AutoSize_CallBack)
            container:AddChild(AutoSize)
            GUI.StatusBar.StateIcon.AutoSize = AutoSize

        local Anchor = AceGUI:Create("Dropdown")
            Anchor:SetLabel("Anchor")
            Anchor:SetList(anchors)
            Anchor:SetValue(IRT_Orders.StatusBar.State.Anchor)
            Anchor:SetCallback("OnValueChanged", StatusBar_Icon_Anchor_CallBack)
            Anchor:SetWidth(150)
            container:AddChild(Anchor)
            GUI.StatusBar.StateIcon.Anchor = Anchor

        local RelativePoint = AceGUI:Create("Dropdown")
            RelativePoint:SetLabel("Relative Point")
            RelativePoint:SetList(anchors)
            RelativePoint:SetValue(IRT_Orders.StatusBar.State.RelativePoint)
            RelativePoint:SetCallback("OnValueChanged", StatusBar_Icon_RelativePoint_CallBack)
            RelativePoint:SetWidth(150)
            container:AddChild(RelativePoint)
            GUI.StatusBar.StateIcon.RelativePoint = RelativePoint

        local Transparency = AceGUI:Create("Slider")
            Transparency:SetLabel("Transparency")
            Transparency:SetWidth(150)
            Transparency:SetSliderValues(1, 100, 1)
            Transparency:SetValue(IRT_Orders.StatusBar.State.Transparency*100)
            Transparency:SetCallback("OnValueChanged", StatusBar_Icon_Transp_CallBack)
            container:AddChild(Transparency)
            GUI.StatusBar.StateIcon.Transparency = Transparency

        local Size = AceGUI:Create("Slider")
            Size:SetLabel("Size")
            Size:SetWidth(150)
            Size:SetSliderValues(1, 200, 1)
            Size:SetValue(IRT_Orders.StatusBar.State.Size)
            Size:SetCallback("OnValueChanged", StatusBar_Icon_Size_CallBack)
            container:AddChild(Size)
            GUI.StatusBar.StateIcon.Size = Size

        local xOfs = AceGUI:Create("Slider")
            xOfs:SetLabel("X Offset")
            xOfs:SetWidth(150)
            xOfs:SetSliderValues(-300, 300, 1)
            xOfs:SetValue(IRT_Orders.StatusBar.State.xOfs)
            xOfs:SetCallback("OnValueChanged", StatusBar_Icon_xOfs_CallBack)
            container:AddChild(xOfs)
            GUI.StatusBar.StateIcon.xOfs = xOfs

        local yOfs = AceGUI:Create("Slider")
            yOfs:SetLabel("Y Ofsset")
            yOfs:SetWidth(150)
            yOfs:SetSliderValues(-300, 300, 1)
            yOfs:SetValue(IRT_Orders.StatusBar.State.yOfs)
            yOfs:SetCallback("OnValueChanged", StatusBar_Icon_yOfs_CallBack)
            container:AddChild(yOfs)
            GUI.StatusBar.StateIcon.yOfs = yOfs

        local stateIcon = GUI.StatusBar.StateIcon
        for widget in pairs(stateIcon) do
            if widget ~= "Show" then
                if not stateIcon.Show:GetValue() then
                    stateIcon[widget]:SetDisabled(true)
                else
                    stateIcon[widget]:SetDisabled(false)
                end
            end
        end

        local autosize = AutoSize:GetValue()
        GUI.StatusBar.StateIcon.Size:SetDisabled(autosize)
    end

    local function Set_StatusBar_Text_Section(container)

        local function Time_yOfs_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.Time.yOfs = value
            StatusBars_SetTimePoint()
        end
        
        local function Time_xOfs_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.Time.xOfs = value
            StatusBars_SetTimePoint()
        end
        
        local function Time_Size_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.Time.Size = value
            StatusBars_SetTimeFont()
        end
        
        local function Time_RelativePoint_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.Time.RelativePoint = value
            StatusBars_SetTimePoint()
        end
        
        local function Time_Anchor_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.Time.Anchor = value
            StatusBars_SetTimePoint()
        end
        
        local function Time_Color_CallBack(self)
            local c = IRT_Orders.StatusBar.Text.Time.Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a
            StatusBars_SetTimeFontColor()
        end
        
        local function Time_Shadow_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.Time.Shadow = value
            StatusBars_SetTimeFont()
        end
        
        local function TimeFont_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.Time.Font = value
            StatusBars_SetTimeFont()
        end
        
        local function Time_Show_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.Time.Show = value
            StatusBars_ShowTimeFont()

            local time = GUI.StatusBar.Text.Time
            for widget in pairs(time) do
                if widget ~= "Show" then
                    if not time.Show:GetValue() then
                        time[widget]:SetDisabled(true)
                    else
                        time[widget]:SetDisabled(false)
                    end
                end
            end
        end

        -----
        local function DestName_Color_CallBack(self)
            local c = IRT_Orders.StatusBar.Text.DestName.Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a

            StatusBars_SetDestNameFontColor()
        end

        local function DestName_yOfs_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.yOfs = value
            StatusBars_SetDestNamePoint()
        end
        
        local function DestName_xOfs_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.xOfs = value
            StatusBars_SetDestNamePoint()
        end
        
        local function DestName_Length_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.Length = value
            StatusBars_SetDestNameLen()
        end
        
        local function DestName_Size_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.Size = value
            StatusBars_SetDestNameFont()
        end
        
        local function DestName_RelativePoint_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.RelativePoint = value
            StatusBars_SetDestNamePoint()
        end
        
        local function DestName_Anchor_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.Anchor = value
            StatusBars_SetDestNamePoint()
        end
        
        local function DestName_Shadow_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.Shadow = value
            StatusBars_SetDestNameFont()
        end
        
        local function DestName_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.Font = value
            StatusBars_SetDestNameFont()
        end

        local function DestName_Show_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.DestName.Show = value
            StatusBars_ShowDestNameFont()

            local dest = GUI.StatusBar.Text.DestName
            for widget in pairs(dest) do
                if widget ~= "Show" then
                    if not dest.Show:GetValue() then
                        dest[widget]:SetDisabled(true)
                    else
                        dest[widget]:SetDisabled(false)
                    end
                end
            end 
        end
    
        -------
    
        local function SrcName_yOfs_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.SrcName.yOfs = value
            StatusBars_SetSrcNamePoint()
        end
        
        local function SrcName_xOfs_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.SrcName.xOfs = value
            StatusBars_SetSrcNamePoint()
        end
        
        local function SrcName_Length_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.SrcName.Length = value
            StatusBars_SetSrcNameLen()
        end
        
        local function SrcName_Size_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.SrcName.Size = value
            StatusBars_SetSrcNameFont()
        end
        
        local function SrcName_RelativePoint_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.SrcName.RelativePoint = value
            StatusBars_SetSrcNamePoint()
        end
        
        local function SrcName_Anchor_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.SrcName.Anchor = value
            StatusBars_SetSrcNamePoint()
        end
        
        local function SrcName_Shadow_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.SrcName.Shadow = value
            StatusBars_SetSrcNameFont()
        end
        
        local function SrcName_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Text.SrcName.Font = value
            StatusBars_SetSrcNameFont()
        end

        local function ReadyColor_CallBack(self)
            local c = IRT_Orders.StatusBar.Text.Ready.Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a

            if IRT_Orders.frames then
                for frameName in pairs(IRT_Orders.frames) do
                    Frame_SetOrder(frameName)
                end 
            end
        end

        local function NotReadyColor_CallBack(self)
            local c = IRT_Orders.StatusBar.Text["Not Ready"].Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a

            if IRT_Orders.frames then
                for frameName in pairs(IRT_Orders.frames) do
                    Frame_SetOrder(frameName)
                end 
            end
        end

        GUI.StatusBar = GUI.StatusBar or {}
        GUI.StatusBar.Text = GUI.StatusBar.Text or {}
        GUI.StatusBar.Text.SrcName = GUI.StatusBar.Text.SrcName or {}
        GUI.StatusBar.Text.DestName = GUI.StatusBar.Text.DestName or {}
        GUI.StatusBar.Text.Time = GUI.StatusBar.Text.Time or {}
    
        local title = AceGUI:Create("Heading")
            title:SetText("Source Name Text Settings")
            title:SetFullWidth(true)
            container:AddChild(title)
    
        local Font = AceGUI:Create("Dropdown")
            Font:SetLabel("Font")
            Font:SetList(fonts)
            Font:SetValue(IRT_Orders.StatusBar.Text.SrcName.Font)
            Font:SetCallback("OnValueChanged", SrcName_CallBack)
            Font:SetWidth(150)
            container:AddChild(Font)
            GUI.StatusBar.Text.SrcName.Font = Font
    
        local Shadow = AceGUI:Create("Dropdown")
            Shadow:SetLabel("Shadow")
            Shadow:SetList(shadows)
            Shadow:SetValue(IRT_Orders.StatusBar.Text.SrcName.Shadow)
            Shadow:SetCallback("OnValueChanged", SrcName_Shadow_CallBack)
            Shadow:SetWidth(150)
            container:AddChild(Shadow)
            GUI.StatusBar.Text.SrcName.Shadow = Shadow
    
        local Anchor = AceGUI:Create("Dropdown")
            Anchor:SetLabel("Anchor")
            Anchor:SetList(anchors)
            Anchor:SetValue(IRT_Orders.StatusBar.Text.SrcName.Anchor)
            Anchor:SetCallback("OnValueChanged", SrcName_Anchor_CallBack)
            Anchor:SetWidth(150)
            container:AddChild(Anchor)
            GUI.StatusBar.Text.SrcName.Anchor = Anchor
    
        local RelativePoint = AceGUI:Create("Dropdown")
            RelativePoint:SetLabel("Relative Point")
            RelativePoint:SetList(anchors)
            RelativePoint:SetValue(IRT_Orders.StatusBar.Text.SrcName.RelativePoint)
            RelativePoint:SetCallback("OnValueChanged", SrcName_RelativePoint_CallBack)
            RelativePoint:SetWidth(150)
            container:AddChild(RelativePoint)
            GUI.StatusBar.Text.SrcName.RelativePoint = RelativePoint
    
        local Size = AceGUI:Create("Slider")
            Size:SetLabel("Text Size")
            Size:SetWidth(150)
            Size:SetSliderValues(1, 50, 1)
            Size:SetValue(IRT_Orders.StatusBar.Text.SrcName.Size)
            Size:SetCallback("OnEnterPressed", SrcName_Size_CallBack)
            Size:SetCallback("OnValueChanged", SrcName_Size_CallBack)
            container:AddChild(Size)
            GUI.StatusBar.Text.SrcName.Size = Size
    
        local Length = AceGUI:Create("Slider")
            Length:SetLabel("Text Length")
            Length:SetWidth(150)
            Length:SetSliderValues(1, 30, 1)
            Length:SetValue(IRT_Orders.StatusBar.Text.SrcName.Length)
            Length:SetCallback("OnValueChanged", SrcName_Length_CallBack)
            container:AddChild(Length)
            GUI.StatusBar.Text.SrcName.Length = Length
    
        local xOfs = AceGUI:Create("Slider")
            xOfs:SetLabel("X Offset")
            xOfs:SetWidth(150)
            xOfs:SetSliderValues(-300, 300, 1)
            xOfs:SetValue(IRT_Orders.StatusBar.Text.SrcName.xOfs)
            xOfs:SetCallback("OnValueChanged", SrcName_xOfs_CallBack)
            container:AddChild(xOfs)
            GUI.StatusBar.Text.SrcName.xOfs = xOfs
        
        local yOfs = AceGUI:Create("Slider")
            yOfs:SetLabel("Y Offset")
            yOfs:SetWidth(150)
            yOfs:SetSliderValues(-300, 300, 1)
            yOfs:SetValue(IRT_Orders.StatusBar.Text.SrcName.yOfs)
            yOfs:SetCallback("OnValueChanged", SrcName_yOfs_CallBack)
            container:AddChild(yOfs)
            GUI.StatusBar.Text.SrcName.yOfs = yOfs

        local ReadyColor = AceGUI:Create("ColorPicker")
            ReadyColor:SetLabel("Next in Order Color")
            ReadyColor:SetHasAlpha(true)
            ReadyColor:SetColor(unpack(IRT_Orders.StatusBar.Text.Ready.Color))
            ReadyColor:SetCallback("OnValueChanged", ReadyColor_CallBack)
            ReadyColor:SetCallback("OnValueConfirmed", ReadyColor_CallBack)
            container:AddChild(ReadyColor)
            GUI.StatusBar.Text.SrcName.ReadyColor = ReadyColor

        local NotReadyColor = AceGUI:Create("ColorPicker")
            NotReadyColor:SetLabel("Not Ready Color")
            NotReadyColor:SetHasAlpha(true)
            NotReadyColor:SetColor(unpack(IRT_Orders.StatusBar.Text["Not Ready"].Color))
            NotReadyColor:SetCallback("OnValueChanged", NotReadyColor_CallBack)
            NotReadyColor:SetCallback("OnValueConfirmed", NotReadyColor_CallBack)
            container:AddChild(NotReadyColor)
            GUI.StatusBar.Text.SrcName.NotReadyColor = NotReadyColor
    
        local title = AceGUI:Create("Heading")
            title:SetText("Destination Name Text Settings")
            title:SetFullWidth(true)
            container:AddChild(title)
    
        local Show = AceGUI:Create("CheckBox")
                Show:SetLabel("Enable Title Text")
                Show:SetValue(IRT_Orders.Frame.Text.Title.Show)
                Show:SetCallback("OnValueChanged", DestName_Show_CallBack)
                container:AddChild(Show)
                GUI.StatusBar.Text.DestName.Show = Show
    
        local Font = AceGUI:Create("Dropdown")
            Font:SetLabel("Font")
            Font:SetList(fonts)
            Font:SetValue(IRT_Orders.StatusBar.Text.DestName.Font)
            Font:SetCallback("OnValueChanged", DestName_CallBack)
            Font:SetWidth(150)
            container:AddChild(Font)
            GUI.StatusBar.Text.DestName.Font = Font
    
        local Shadow = AceGUI:Create("Dropdown")
            Shadow:SetLabel("Shadow")
            Shadow:SetList(shadows)
            Shadow:SetValue(IRT_Orders.StatusBar.Text.DestName.Shadow)
            Shadow:SetCallback("OnValueChanged", DestName_Shadow_CallBack)
            Shadow:SetWidth(150)
            container:AddChild(Shadow)
            GUI.StatusBar.Text.DestName.Shadow = Shadow

        local DestColor = AceGUI:Create("ColorPicker")
            DestColor:SetLabel("Destination Name Color")
            DestColor:SetHasAlpha(true)
            DestColor:SetColor(unpack(IRT_Orders.StatusBar.Text.DestName.Color))
            DestColor:SetCallback("OnValueChanged", DestName_Color_CallBack)
            DestColor:SetCallback("OnValueConfirmed", DestName_Color_CallBack)
            container:AddChild(DestColor)
            GUI.StatusBar.Text.DestName.Color = DestColor
    
        local Anchor = AceGUI:Create("Dropdown")
            Anchor:SetLabel("Anchor")
            Anchor:SetList(anchors)
            Anchor:SetValue(IRT_Orders.StatusBar.Text.DestName.Anchor)
            Anchor:SetCallback("OnValueChanged", DestName_Anchor_CallBack)
            Anchor:SetWidth(150)
            container:AddChild(Anchor)
            GUI.StatusBar.Text.DestName.Anchor = Anchor
    
        local RelativePoint = AceGUI:Create("Dropdown")
            RelativePoint:SetLabel("Relative Point")
            RelativePoint:SetList(anchors)
            RelativePoint:SetValue(IRT_Orders.StatusBar.Text.DestName.RelativePoint)
            RelativePoint:SetCallback("OnValueChanged", DestName_RelativePoint_CallBack)
            RelativePoint:SetWidth(150)
            container:AddChild(RelativePoint)
            GUI.StatusBar.Text.DestName.RelativePoint = RelativePoint
    
        local Size = AceGUI:Create("Slider")
            Size:SetLabel("Text Size")
            Size:SetWidth(150)
            Size:SetSliderValues(1, 50, 1)
            Size:SetValue(IRT_Orders.StatusBar.Text.DestName.Size)
            Size:SetCallback("OnEnterPressed", DestName_Size_CallBack)
            Size:SetCallback("OnValueChanged", DestName_Size_CallBack)
            container:AddChild(Size)
            GUI.StatusBar.Text.DestName.Size = Size
    
        local Length = AceGUI:Create("Slider")
            Length:SetLabel("Text Length")
            Length:SetWidth(150)
            Length:SetSliderValues(1, 30, 1)
            Length:SetValue(IRT_Orders.StatusBar.Text.DestName.Length)
            Length:SetCallback("OnValueChanged", DestName_Length_CallBack)
            container:AddChild(Length)
            GUI.StatusBar.Text.DestName.Length = Length
    
        local xOfs = AceGUI:Create("Slider")
            xOfs:SetLabel("X Offset")
            xOfs:SetWidth(150)
            xOfs:SetSliderValues(-300, 300, 1)
            xOfs:SetValue(IRT_Orders.StatusBar.Text.DestName.xOfs)
            xOfs:SetCallback("OnValueChanged", DestName_xOfs_CallBack)
            container:AddChild(xOfs)
            GUI.StatusBar.Text.DestName.xOfs = xOfs
        
        local yOfs = AceGUI:Create("Slider")
            yOfs:SetLabel("Y Offset")
            yOfs:SetWidth(150)
            yOfs:SetSliderValues(-300, 300, 1)
            yOfs:SetValue(IRT_Orders.StatusBar.Text.DestName.yOfs)
            yOfs:SetCallback("OnValueChanged", DestName_yOfs_CallBack)
            container:AddChild(yOfs)
            GUI.StatusBar.Text.DestName.yOfs = yOfs
    
        local dest = GUI.StatusBar.Text.DestName

        for widget in pairs(dest) do
            if widget ~= "Show" then
                if not dest.Show:GetValue() then
                    dest[widget]:SetDisabled(true)
                else
                    dest[widget]:SetDisabled(false)
                end
            end
        end

        local title = AceGUI:Create("Heading")
            title:SetText("Time Text Settings")
            title:SetFullWidth(true)
            container:AddChild(title)

        local Show = AceGUI:Create("CheckBox")
            Show:SetLabel("Enable Time Text")
            Show:SetValue(IRT_Orders.StatusBar.Text.Time.Show)
            Show:SetCallback("OnValueChanged", Time_Show_CallBack)
            container:AddChild(Show)
            GUI.StatusBar.Text.Time.Show = Show

        local Font = AceGUI:Create("Dropdown")
            Font:SetLabel("Font")
            Font:SetList(fonts)
            Font:SetValue(IRT_Orders.StatusBar.Text.Time.Font)
            Font:SetCallback("OnValueChanged", TimeFont_CallBack)
            Font:SetWidth(150)
            container:AddChild(Font)
            GUI.StatusBar.Text.Time.Font = Font

        local Shadow = AceGUI:Create("Dropdown")
            Shadow:SetLabel("Shadow")
            Shadow:SetList(shadows)
            Shadow:SetValue(IRT_Orders.StatusBar.Text.Time.Shadow)
            Shadow:SetCallback("OnValueChanged", Time_Shadow_CallBack)
            Shadow:SetWidth(150)
            container:AddChild(Shadow)
            GUI.StatusBar.Text.Time.Shadow = Shadow

        local TimeColor = AceGUI:Create("ColorPicker")
            TimeColor:SetLabel("Not Ready Color")
            TimeColor:SetHasAlpha(true)
            TimeColor:SetColor(unpack(IRT_Orders.StatusBar.Text.Time.Color))
            TimeColor:SetCallback("OnValueChanged", Time_Color_CallBack)
            TimeColor:SetCallback("OnValueConfirmed", Time_Color_CallBack)
            container:AddChild(TimeColor)
            GUI.StatusBar.Text.Time.Color = TimeColor

        local Anchor = AceGUI:Create("Dropdown")
            Anchor:SetLabel("Anchor")
            Anchor:SetList(anchors)
            Anchor:SetValue(IRT_Orders.StatusBar.Text.Time.Anchor)
            Anchor:SetCallback("OnValueChanged", Time_Anchor_CallBack)
            Anchor:SetWidth(150)
            container:AddChild(Anchor)
            GUI.StatusBar.Text.Time.Anchor = Anchor

        local RelativePoint = AceGUI:Create("Dropdown")
            RelativePoint:SetLabel("Relative Point")
            RelativePoint:SetList(anchors)
            RelativePoint:SetValue(IRT_Orders.StatusBar.Text.Time.RelativePoint)
            RelativePoint:SetCallback("OnValueChanged", Time_RelativePoint_CallBack)
            RelativePoint:SetWidth(150)
            container:AddChild(RelativePoint)
            GUI.StatusBar.Text.Time.RelativePoint = RelativePoint

        local Size = AceGUI:Create("Slider")
            Size:SetLabel("Text Size")
            Size:SetWidth(150)
            Size:SetSliderValues(1, 50, 1)
            Size:SetValue(IRT_Orders.StatusBar.Text.Time.Size)
            Size:SetCallback("OnValueChanged", Time_Size_CallBack)
            container:AddChild(Size)
            GUI.StatusBar.Text.Time.Size = Size

        local xOfs = AceGUI:Create("Slider")
            xOfs:SetLabel("X Offset")
            xOfs:SetWidth(150)
            xOfs:SetSliderValues(-300, 300, 1)
            xOfs:SetValue(IRT_Orders.StatusBar.Text.Time.xOfs)
            xOfs:SetCallback("OnValueChanged", Time_xOfs_CallBack)
            container:AddChild(xOfs)
            GUI.StatusBar.Text.Time.xOfs = xOfs

        local yOfs = AceGUI:Create("Slider")
            yOfs:SetLabel("Y Offset")
            yOfs:SetWidth(150)
            yOfs:SetSliderValues(-300, 300, 1)
            yOfs:SetValue(IRT_Orders.StatusBar.Text.Time.yOfs)
            yOfs:SetCallback("OnValueChanged", Time_yOfs_CallBack)
            container:AddChild(yOfs)
            GUI.StatusBar.Text.Time.yOfs = yOfs

        local time = GUI.StatusBar.Text.Time
        for widget in pairs(time) do
            if widget ~= "Show" then
                if not time.Show:GetValue() then
                    time[widget]:SetDisabled(true)
                else
                    time[widget]:SetDisabled(false)
                end
            end
        end
    end

    local function Set_StatusBar_Texture_Section(container)

        local function SB_BuffTextureColor_CallBack(self)
            local c = IRT_Orders.StatusBar.Texture.Buff.Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a
        end
    
        local function SB_BuffTexture_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Texture.Buff.Texture = value
        end
    
        local function SB_BackgroundTextureColor_CallBack(self)
            local c = IRT_Orders.StatusBar.Texture.Background.Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a
            StatusBars_SetBackgroundColor()
        end
    
        local function SB_BackgroundTexture_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Texture.Background.Texture = value
            StatusBars_SetBackgroundTexture()
        end
    
        local function SB_TextureColor_CallBack(self)
            local c = IRT_Orders.StatusBar.Texture.Cooldown.Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a
            StatusBars_SetColor()
        end
    
        local function SB_TextureColor_Transparency_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Texture.Cooldown.Transparency = value/100
            StatusBars_SetColorAlpha()
        end

        local function SB_TextureClassColor_Back(container, widget, value)
            IRT_Orders.StatusBar.Texture.Cooldown.ClassColor = value
            StatusBars_SetColor()
        end

        local function SB_Texture_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Texture.Cooldown.Texture = value
            StatusBars_SetTexture()
        end

        GUI.StatusBarTexture = GUI.StatusBarTexture or {}
        GUI.StatusBarBackgroundTexture = GUI.StatusBarBackgroundTexture or {}
        GUI.StatusBarBuffTexture = GUI.StatusBarBuffTexture or {}

        local title = AceGUI:Create("Heading")
            title:SetText("Cooldown Bar Texture")
            title:SetFullWidth(true)
            container:AddChild(title)

        local Texture = AceGUI:Create("Dropdown")
            Texture:SetLabel("Texture")
            Texture:SetList(textures)
            Texture:SetValue(IRT_Orders.StatusBar.Texture.Cooldown.Texture)
            Texture:SetCallback("OnValueChanged", SB_Texture_CallBack)
            Texture:SetWidth(150)
            container:AddChild(Texture)
            GUI.StatusBarTexture.Texture = Texture

        local Transparency = AceGUI:Create("Slider")
            Transparency:SetLabel("Color Transparency")
            Transparency:SetWidth(150)
            Transparency:SetSliderValues(1, 100, 1)
            Transparency:SetValue(IRT_Orders.StatusBar.Texture.Cooldown.Transparency*100)
            Transparency:SetCallback("OnValueChanged", SB_TextureColor_Transparency_CallBack)
            container:AddChild(Transparency)
            GUI.StatusBarTexture.Transparency = Transparency

        local ClassColor = AceGUI:Create("CheckBox")
            ClassColor:SetLabel("CD / Buff Class Color")
            ClassColor:SetValue(IRT_Orders.StatusBar.Texture.Cooldown.ClassColor)
            ClassColor:SetCallback("OnValueChanged", SB_TextureClassColor_Back)
            container:AddChild(ClassColor)
            GUI.StatusBarTexture.ClassColor = ClassColor

        local TexColor = AceGUI:Create("ColorPicker")
            TexColor:SetLabel("CD / Buff Texture Color")
            TexColor:SetHasAlpha(true)
            TexColor:SetColor(unpack(IRT_Orders.StatusBar.Texture.Cooldown.Color))
            TexColor:SetCallback("OnValueChanged", SB_TextureColor_CallBack)
            TexColor:SetCallback("OnValueConfirmed", SB_TextureColor_CallBack)
            container:AddChild(TexColor)
            GUI.StatusBarTexture.TexColor = TexColor

        local title = AceGUI:Create("Heading")
            title:SetText("Background Texture")
            title:SetFullWidth(true)
            container:AddChild(title)

        local Texture = AceGUI:Create("Dropdown")
            Texture:SetLabel("Texture")
            Texture:SetList(textures)
            Texture:SetValue(IRT_Orders.StatusBar.Texture.Background.Texture)
            Texture:SetCallback("OnValueChanged", SB_BackgroundTexture_CallBack)
            Texture:SetWidth(150)
            container:AddChild(Texture)
            GUI.StatusBarBackgroundTexture.Texture = Texture


        local BackgroundColor = AceGUI:Create("ColorPicker")
            BackgroundColor:SetLabel("CD Texture Color")
            BackgroundColor:SetHasAlpha(true)
            BackgroundColor:SetColor(unpack(IRT_Orders.StatusBar.Texture.Background.Color))
            BackgroundColor:SetCallback("OnValueChanged", SB_BackgroundTextureColor_CallBack)
            BackgroundColor:SetCallback("OnValueConfirmed", SB_BackgroundTextureColor_CallBack)
            container:AddChild(BackgroundColor)
            GUI.StatusBarBackgroundTexture.BackgroundColor = BackgroundColor
    end

    local function Set_StatusBar_General_Section(container)

        local function SB_Transparency_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Layout.Transparency = value/100
    
            StatusBars_SetTransparency()
        end
    
        local function SB_CDUpdateRate_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Layout["Bar CD Update Rate"] = value
        end
    
        local function SB_Space_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Layout.Space = value
            StatusBars_Reposition()
            Frames_SetHeight()
            Frames_Reposition()
        end
    
        local function SB_Height_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Layout.Height = value
            Frames_SetTitleHeight()
            Frames_SetHeight()
            StatusBars_SetHeight()
            Frames_SetTitleIconSize()
            Frames_Reposition()
        end
        
        local function SB_Width_CallBack(container, widget, value)
            IRT_Orders.StatusBar.Layout.Width = value
            Frames_SetTitleWidth()
            Frames_SetWidth()
            StatusBars_SetWidth()
            Frames_Reposition()
        end

        GUI.StatusBar = GUI.StatusBar or {}

        local title = AceGUI:Create("Heading")
            title:SetText("Status Bar General Settings")
            title:SetFullWidth(true)
            container:AddChild(title)

        local Transparency = AceGUI:Create("Slider")
            Transparency:SetLabel("Bar Transparency")
            Transparency:SetWidth(150)
            Transparency:SetSliderValues(1, 100, 1)
            Transparency:SetValue(IRT_Orders.StatusBar.Layout.Transparency*100)
            Transparency:SetCallback("OnValueChanged", SB_Transparency_CallBack)
            container:AddChild(Transparency)
            GUI.StatusBar.Transparency = Transparency

        local Width = AceGUI:Create("Slider")
            Width:SetLabel("Bar Width")
            Width:SetWidth(150)
            Width:SetSliderValues(1, 500, 1)
            Width:SetValue(IRT_Orders.StatusBar.Layout.Width)
            Width:SetCallback("OnValueChanged", SB_Width_CallBack)
            container:AddChild(Width)
            GUI.StatusBar.Width = Width

        local Height = AceGUI:Create("Slider")
            Height:SetLabel("Bar Height")
            Height:SetWidth(150)
            Height:SetSliderValues(1, 500, 1)
            Height:SetValue(IRT_Orders.StatusBar.Layout.Height)
            Height:SetCallback("OnValueChanged", SB_Height_CallBack)
            container:AddChild(Height)
            GUI.StatusBar.Height = Height

        local Space = AceGUI:Create("Slider")
            Space:SetLabel("Bars Spacing")
            Space:SetWidth(150)
            Space:SetSliderValues(0, 100, 1)
            Space:SetValue(IRT_Orders.StatusBar.Layout.Space)
            Space:SetCallback("OnValueChanged", SB_Space_CallBack)
            container:AddChild(Space)
            GUI.StatusBar.Space = Space

        local CDUpdateRate = AceGUI:Create("Slider")
            CDUpdateRate:SetLabel("Bar CD-Update-Rate")
            CDUpdateRate:SetWidth(150)
            CDUpdateRate:SetSliderValues(0.01, 1, 0.01)
            CDUpdateRate:SetValue(IRT_Orders.StatusBar.Layout["Bar CD Update Rate"])
            CDUpdateRate:SetCallback("OnValueChanged", SB_CDUpdateRate_CallBack)
            container:AddChild(CDUpdateRate)
            GUI.StatusBar.CDUpdateRate = CDUpdateRate
    end

    local function Set_Frame_Section(container)
        local title = AceGUI:Create("Heading")
            title:SetText("Frame Settings")
            title:SetFullWidth(true)
            container:AddChild(title)
            

        local GeneralOptions = AceGUI:Create("Button")
            GeneralOptions:SetText("General Options")
            GeneralOptions:SetWidth(150)
            GeneralOptions:SetCallback("OnClick", 
                function()
                    frame:SelectByPath(2)
                    frame:SelectByPath("2\0011")
                    frame:RefreshTree()
                end)
            container:AddChild(GeneralOptions)

        local TitleTexture = AceGUI:Create("Button")
            TitleTexture:SetText("Title Texture")
            TitleTexture:SetWidth(150)
            TitleTexture:SetCallback("OnClick", 
                function()
                    frame:SelectByPath(2)
                    frame:SelectByPath("2\0012")
                    frame:RefreshTree()
                end)
            container:AddChild(TitleTexture)

        local SpellIcon = AceGUI:Create("Button")
            SpellIcon:SetText("Spell Icon")
            SpellIcon:SetWidth(150)
            SpellIcon:SetCallback("OnClick", 
                function()
                    frame:SelectByPath(2)
                    frame:SelectByPath("2\0013")
                    frame:RefreshTree()
                end)
            container:AddChild(SpellIcon)

        local TitleText = AceGUI:Create("Button")
            TitleText:SetText("Title Text")
            TitleText:SetWidth(150)
            TitleText:SetCallback("OnClick", 
                function()
                    frame:SelectByPath(2)
                    frame:SelectByPath("2\0014")
                    frame:RefreshTree()
                end)
            container:AddChild(TitleText)
    end

    local function Set_Frame_Title_Text_Section(container)

        local function fTitleText_yOfs_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.yOfs = value
            Frames_SetTitleTextPoint()
        end
        
        local function fTitleText_xOfs_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.xOfs = value
            Frames_SetTitleTextPoint()
        end
        
        local function fTitleText_length_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.Length = value
            Frames_SetTitleTextLen()
        end
        
        local function fTitleText_size_CallBack(container, widget, value)
            container.editbox:ClearFocus()
            IRT_Orders.Frame.Text.Title.Size = value
            Frames_SetTitleFont(frame)
        end
        
        local function fTitleText_ClassColor_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.ClassColor = value
            Frames_SetTitleTextColor()
        end

        local function fTitleText_color_CallBack(self)
            local c = IRT_Orders.Frame.Text.Title.Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a
            Frames_SetTitleTextColor()
        end
        
        local function fTitleText_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.Show = value
            Frames_TitleTextShow()

            for widget in pairs(GUI.FrameTitleText) do
                if widget ~= "Show" then
                    if not GUI.FrameTitleText.Show:GetValue() then
                        GUI.FrameTitleText[widget]:SetDisabled(true)
                    else
                        GUI.FrameTitleText[widget]:SetDisabled(false)
                    end
                end
            end 
        end
        
        local function fTitleText_relativePoint_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.RelativePoint = value
            Frames_SetTitleTextPoint()
        end
        
        local function fTitleText_anchor_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.Anchor = value
            Frames_SetTitleTextPoint()
        end
        
        local function fTitleText_shadow_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.Shadow = value
            Frames_SetTitleFont()
        end
        
        local function fTitleText_font_CallBack(container, widget, value)
            IRT_Orders.Frame.Text.Title.Font = value
            Frames_SetTitleFont()
        end
    
        GUI.FrameTitleText = GUI.FrameTitleText or {}
    
        local title = AceGUI:Create("Heading")
            title:SetText("Frame Title Text Settings")
            title:SetFullWidth(true)
            container:AddChild(title)
    
        local Show = AceGUI:Create("CheckBox")
            Show:SetLabel("Enable Title Text")
            Show:SetValue(IRT_Orders.Frame.Text.Title.Show)
            Show:SetCallback("OnValueChanged", fTitleText_CallBack)
            container:AddChild(Show)
            GUI.FrameTitleText.Show = Show
        
        local Font = AceGUI:Create("Dropdown")
            Font:SetLabel("Font")
            Font:SetList(fonts)
            Font:SetValue(IRT_Orders.Frame.Text.Title.Font)
            Font:SetCallback("OnValueChanged", fTitleText_font_CallBack)
            Font:SetWidth(150)
            container:AddChild(Font)
            GUI.FrameTitleText.Font = Font
    
        local Shadow = AceGUI:Create("Dropdown")
            Shadow:SetLabel("Shadow")
            Shadow:SetList(shadows)
            Shadow:SetValue(IRT_Orders.Frame.Text.Title.Shadow)
            Shadow:SetCallback("OnValueChanged", fTitleText_shadow_CallBack)
            Shadow:SetWidth(150)
            container:AddChild(Shadow)
            GUI.FrameTitleText.Shadow = Shadow
    
        local Anchor = AceGUI:Create("Dropdown")
            Anchor:SetLabel("Anchor")
            Anchor:SetList(anchors)
            Anchor:SetValue(IRT_Orders.Frame.Text.Title.Anchor)
            Anchor:SetCallback("OnValueChanged", fTitleText_anchor_CallBack)
            Anchor:SetWidth(150)
            container:AddChild(Anchor)
            GUI.FrameTitleText.Anchor = Anchor
    
        local RelativePoint = AceGUI:Create("Dropdown")
            RelativePoint:SetLabel("Relative Point")
            RelativePoint:SetList(anchors)
            RelativePoint:SetValue(IRT_Orders.Frame.Text.Title.RelativePoint)
            RelativePoint:SetCallback("OnValueChanged", fTitleText_relativePoint_CallBack)
            RelativePoint:SetWidth(150)
            container:AddChild(RelativePoint)
            GUI.FrameTitleText.RelativePoint = RelativePoint
    
        local ClassColor = AceGUI:Create("CheckBox")
            ClassColor:SetLabel("Text Class Color")
            ClassColor:SetValue(IRT_Orders.Frame.Text.Title.ClassColor)
            ClassColor:SetCallback("OnValueChanged", fTitleText_ClassColor_CallBack)
            container:AddChild(ClassColor)
            GUI.FrameTitleText.ClassColor = ClassColor

        local Color = AceGUI:Create("ColorPicker")
            Color:SetLabel("Text Color")
            Color:SetHasAlpha(true)
            Color:SetColor(unpack(IRT_Orders.Frame.Text.Title.Color))
            Color:SetCallback("OnValueChanged", fTitleText_color_CallBack)
            Color:SetCallback("OnValueConfirmed", fTitleText_color_CallBack)
            container:AddChild(Color)
            GUI.FrameTitleText.Color = Color 
    
        local Size = AceGUI:Create("Slider")
            Size:SetLabel("Text Size")
            Size:SetWidth(150)
            Size:SetSliderValues(1, 50, 1)
            Size:SetValue(IRT_Orders.Frame.Text.Title.Size)
            Size:SetCallback("OnEnterPressed", fTitleText_size_CallBack)
            Size:SetCallback("OnValueChanged", fTitleText_size_CallBack)
            container:AddChild(Size)
            GUI.FrameTitleText.Size = Size
    
        local Length = AceGUI:Create("Slider")
            Length:SetLabel("Text Length")
            Length:SetWidth(150)
            Length:SetSliderValues(1, 30, 1)
            Length:SetValue(IRT_Orders.Frame.Text.Title.Length)
            Length:SetCallback("OnValueChanged", fTitleText_length_CallBack)
            container:AddChild(Length)
            GUI.FrameTitleText.Length = Length
    
        local xOfs = AceGUI:Create("Slider")
            xOfs:SetLabel("X Offset")
            xOfs:SetWidth(150)
            xOfs:SetSliderValues(-300, 300, 1)
            xOfs:SetValue(IRT_Orders.Frame.Text.Title.xOfs)
            xOfs:SetCallback("OnValueChanged", fTitleText_xOfs_CallBack)
            container:AddChild(xOfs)
            GUI.FrameTitleText.xOfs = xOfs
        
        local yOfs = AceGUI:Create("Slider")
            yOfs:SetLabel("Y Offset")
            yOfs:SetWidth(150)
            yOfs:SetSliderValues(-300, 300, 1)
            yOfs:SetValue(IRT_Orders.Frame.Text.Title.yOfs)
            yOfs:SetCallback("OnValueChanged", fTitleText_yOfs_CallBack)
            container:AddChild(yOfs)
            GUI.FrameTitleText.yOfs = yOfs
    
        for widget in pairs(GUI.FrameTitleText) do
            if widget ~= "Show" then
                if not GUI.FrameTitleText.Show:GetValue() then
                    GUI.FrameTitleText[widget]:SetDisabled(true)
                else
                    GUI.FrameTitleText[widget]:SetDisabled(false)
                end
            end
        end 
    end

    local function Set_Frame_Title_Texture_Section(container)

        local function ST_Height_CallBack(container, widget, value)
            IRT_Orders.Frame.Title.Height = value
            Frames_SetTitleHeight()
            Frames_SetTitleIconSize()
            Frames_Reposition()
        end
    
        local function ST_Width_CallBack(container, widget, value)
            IRT_Orders.Frame.Title.Width = value
            Frames_SetTitleWidth()
        end
    
        local function ST_Transp_CallBack(container, widget, value)
            IRT_Orders.Frame.Title.Transparency = value/100
            Frames_SetTitleTransparency()
        end
    
        local function ST_Color_CallBack(self)
            local c = IRT_Orders.Frame.Title.Color
            c[1], c[2], c[3] , c[4] = self.r, self.g, self.b, self.a
            Frames_SetTitleTextureColor()
        end
     
        local function ST_ClassColor_CallBack(container, widget, value)
            IRT_Orders.Frame.Title.ClassColor = value
            Frames_SetTitleTextureColor()
    
            GUI.FrameTitle.Color:SetDisabled(value)
        end
    
        local function ST_Texture_CallBack(container, widget, value)
            IRT_Orders.Frame.Title.Texture = value
            Frames_SetTitleTexture()
        end
    
        local function ST_AutoSize_CallBack(container, widget, value)
            IRT_Orders.Frame.Title.AutoSize = value
            Frames_SetTitleSize()
            Frames_SetTitleIconSize()
            Frames_Reposition()
            
            GUI.FrameTitle.Width:SetDisabled(value)
            GUI.FrameTitle.Height:SetDisabled(value)
        end

        GUI.FrameTitle = GUI.FrameTitle or {}

        local title = AceGUI:Create("Heading")
        title:SetText("Frame Title Texture")
        title:SetFullWidth(true)
        container:AddChild(title)

        -- Frame Title Texture Texture
        local Texture = AceGUI:Create("Dropdown")
            Texture:SetLabel("Title Texture")
            Texture:SetList(textures)
            Texture:SetValue(IRT_Orders.Frame.Title.Texture)
            Texture:SetCallback("OnValueChanged", ST_Texture_CallBack)
            Texture:SetWidth(150)
            container:AddChild(Texture)
            GUI.FrameTitle.Texture = Texture

        local AutoSize = AceGUI:Create("CheckBox")
            AutoSize:SetLabel("Texture AutoSize")
            AutoSize:SetValue(IRT_Orders.Frame.Title.AutoSize)
            AutoSize:SetCallback("OnValueChanged", ST_AutoSize_CallBack)
            container:AddChild(AutoSize)
            GUI.FrameTitle.AutoSize = AutoSize

        local ClassColor = AceGUI:Create("CheckBox")
            ClassColor:SetLabel("Texture ClassColor")
            ClassColor:SetValue(IRT_Orders.Frame.Title.ClassColor)
            ClassColor:SetCallback("OnValueChanged", ST_ClassColor_CallBack)
            container:AddChild(ClassColor)
            GUI.FrameTitle.ClassColor = ClassColor

        local Color = AceGUI:Create("ColorPicker")
            Color:SetLabel("Texture Color")
            Color:SetHasAlpha(true)
            Color:SetColor(unpack(IRT_Orders.Frame.Title.Color))
            Color:SetCallback("OnValueChanged", ST_Color_CallBack)
            Color:SetCallback("OnValueConfirmed", ST_Color_CallBack)
            container:AddChild(Color)
            GUI.FrameTitle.Color = Color

        --[[Frame Title Texture Transparency Slider
        local Transparency = AceGUI:Create("Slider")
        Transparency:SetLabel("Frame Title Texture Transparency")
        Transparency:SetWidth(150)
        Transparency:SetSliderValues(1, 100, 1)
        Transparency:SetValue(IRT_Orders.Frame.Title.Transparency*100)
        Transparency:SetCallback("OnValueChanged", ST_Transp_CallBack)
        section:AddChild(Transparency)
        GUI.FrameTitle.Transparency = Transparency --]]

        local Width = AceGUI:Create("Slider")
            Width:SetLabel("Width")
            Width:SetWidth(150)
            Width:SetSliderValues(1, 500, 1)
            Width:SetValue(IRT_Orders.Frame.Title.Width)
            Width:SetCallback("OnValueChanged", ST_Width_CallBack)
            container:AddChild(Width)
            GUI.FrameTitle.Width = Width

        local Height = AceGUI:Create("Slider")
            Height:SetLabel("Height")
            Height:SetWidth(150)
            Height:SetSliderValues(1, 500, 1)
            Height:SetValue(IRT_Orders.Frame.Title.Height)
            Height:SetCallback("OnValueChanged", ST_Height_CallBack)
            container:AddChild(Height)
            GUI.FrameTitle.Height = Height

        if AutoSize:GetValue() then
            Width:SetDisabled(true)
            Height:SetDisabled(true)
        end

        if ClassColor:GetValue() then
            Color:SetDisabled(true)
        end
    end

    local function Set_Frame_Title_Icon_Section(container)
        GUI.SpellIcon = GUI.SpellIcon or {}

        local function SI_Transp_CallBack(container, widget, value)
            IRT_Orders.Frame.SpellIcon.Transparency = value/100
            Frames_SetTitleIconTransparency()
        end
    
        local function SI_yOfs_CallBack(container, widget, value)
            IRT_Orders.Frame.SpellIcon.yOfs = value
            Frames_SetTitleIconPosition()
        end
    
        local function SI_xOfs_CallBack(container, widget, value)
            IRT_Orders.Frame.SpellIcon.xOfs = value
            Frames_SetTitleIconPosition()
        end
    
        local function SI_RelativePoint_CallBack(container, widget, value)
            IRT_Orders.Frame.SpellIcon.RelativePoint = value
            Frames_SetTitleIconPosition()
        end
    
        local function SI_Anchor_CallBack(container, widget, value)
            IRT_Orders.Frame.SpellIcon.Anchor = value
            Frames_SetTitleIconPosition()
        end
    
        local function SI_Size_CallBack(container, widget, value)
            IRT_Orders.Frame.SpellIcon["Size"] = value
            Frames_SetTitleIconSize()
            Frames_Reposition()
        end
    
        local function SI_Show_CallBack(container, widget, value)
            IRT_Orders.Frame.SpellIcon.Show = value
            Frames_SetTitleIcon()
    
            for i in pairs(GUI.SpellIcon) do
                if i ~= "Show" then 
                    GUI.SpellIcon[i]:SetDisabled(not value)
                end
            end
        end
    
        local function SI_AutoSize_CallBack(container, widget, value)
            IRT_Orders.Frame.SpellIcon.AutoSize = value
            Frames_SetTitleIconSize()
            
            local bool = value
            GUI.SpellIcon.Size:SetDisabled(bool)
        end

        local title = AceGUI:Create("Heading")
            title:SetText("Title Icon")
            title:SetFullWidth(true)
            container:AddChild(title)

        local Show = AceGUI:Create("CheckBox")
            Show:SetLabel("Enable Title Icon")
            Show:SetValue(IRT_Orders.Frame.SpellIcon.Show)
            Show:SetCallback("OnValueChanged", SI_Show_CallBack)
            container:AddChild(Show)
            GUI.SpellIcon.Show = Show

        local AutoSize = AceGUI:Create("CheckBox")
            AutoSize:SetLabel("Autosize")
            AutoSize:SetValue(IRT_Orders.Frame.SpellIcon.AutoSize)
            AutoSize:SetCallback("OnValueChanged", SI_AutoSize_CallBack)
            container:AddChild(AutoSize)
            GUI.SpellIcon.AutoSize = AutoSize

        local Anchor = AceGUI:Create("Dropdown")
            Anchor:SetLabel("Anchor")
            Anchor:SetList(anchors)
            Anchor:SetValue(IRT_Orders.Frame.SpellIcon.Anchor)
            Anchor:SetCallback("OnValueChanged", SI_Anchor_CallBack)
            Anchor:SetWidth(150)
            container:AddChild(Anchor)
            GUI.SpellIcon.Anchor = Anchor

        local RelativePoint = AceGUI:Create("Dropdown")
            RelativePoint:SetLabel("Relative Point")
            RelativePoint:SetList(anchors)
            RelativePoint:SetValue(IRT_Orders.Frame.SpellIcon.RelativePoint)
            RelativePoint:SetCallback("OnValueChanged", SI_RelativePoint_CallBack)
            RelativePoint:SetWidth(150)
            container:AddChild(RelativePoint)
            GUI.SpellIcon.RelativePoint = RelativePoint

        local Transparency = AceGUI:Create("Slider")
            Transparency:SetLabel("Transparency")
            Transparency:SetWidth(150)
            Transparency:SetSliderValues(1, 100, 1)
            Transparency:SetValue(IRT_Orders.Frame.SpellIcon.Transparency*100)
            Transparency:SetCallback("OnValueChanged", SI_Transp_CallBack)
            container:AddChild(Transparency)
            GUI.SpellIcon.Transparency = Transparency

        local Size = AceGUI:Create("Slider")
            Size:SetLabel("Size")
            Size:SetWidth(150)
            Size:SetSliderValues(1, 200, 1)
            Size:SetValue(IRT_Orders.Frame.SpellIcon.Size)
            Size:SetCallback("OnValueChanged", SI_Size_CallBack)
            container:AddChild(Size)
            GUI.SpellIcon.Size = Size

        local xOfs = AceGUI:Create("Slider")
            xOfs:SetLabel("X Offset")
            xOfs:SetWidth(150)
            xOfs:SetSliderValues(-300, 300, 1)
            xOfs:SetValue(IRT_Orders.Frame.SpellIcon.xOfs)
            xOfs:SetCallback("OnValueChanged", SI_xOfs_CallBack)
            container:AddChild(xOfs)
            GUI.SpellIcon.xOfs = xOfs

        local yOfs = AceGUI:Create("Slider")
            yOfs:SetLabel("Y Ofsset")
            yOfs:SetWidth(150)
            yOfs:SetSliderValues(-300, 300, 1)
            yOfs:SetValue(IRT_Orders.Frame.SpellIcon.yOfs)
            yOfs:SetCallback("OnValueChanged", SI_yOfs_CallBack)
            container:AddChild(yOfs)
            GUI.SpellIcon.yOfs = yOfs


        if not Show then
            AutoSize:SetDisabled(true)
            Size:SetDisabled(true)
        else
            Size:SetDisabled(AutoSize:GetValue())
        end
    end

    local function Set_Frame_General_Section(container)

        local function fRows_CallBack(container, widget, value)
            container.editbox:ClearFocus()
            IRT_Orders.Frame.Layout.Rows = value
            Frames_Reposition()
        end

        local function fHSpace_CallBack(container, widget, value)
            container.editbox:ClearFocus()
            IRT_Orders.Frame.Layout.HSpace = value
            Frames_SetWidth()
            Frames_Reposition()
        end
    
        local function fSpace_CallBack(container, widget, value)
            container.editbox:ClearFocus()
            IRT_Orders.Frame.Layout.VSpace = value
            Frames_Reposition()
        end
    
        local function fyOfs_CallBack(container, widget, value)
            container.editbox:ClearFocus()
            IRT_Orders.Frame.Layout.yOfs = value
            Frames_Reposition()
        end
    
        local function fxOfs_CallBack(container, widget, value)
            container.editbox:ClearFocus()
            IRT_Orders.Frame.Layout.xOfs = value
            Frames_Reposition()
        end
    
        local function fScale_CallBack(container, widget, value)
            container.editbox:ClearFocus()
            IRT_Orders.Frame.Layout.Scale = value
            Frames_SetScale()
        end
    
        local function fTransp_CallBack(container, widget, value)
            container.editbox:ClearFocus()
            IRT_Orders.Frame.Layout.Transparency = value / 100
            Frames_SetTransparency()
        end
    
        local function fAnchor_CallBack(container, widget, value)
            IRT_Orders.Frame.Layout.Anchor = value
            Frames_Reposition()
        end

        local function LF_CallBack(container, widget, value)
            IRT_Orders.Settings.Buttons["Lock Frames"] = value
            if value then
                Frames_Alignment("lock")
            else
                Frames_Alignment("unlock")
            end
        end
    
        local function FFA_CallBack(container, widget, value)
            IRT_Orders.Settings.Buttons["Free Frames Alignment"] = value
            if value then
                GUI.Frame.LF:SetDisabled(false)
                Frames_Unlock()
                Frames_SetSavedPoint()
            else
                GUI.Frame.LF:SetDisabled(true)
                Frames_Alignment("lock")
                Frames_Reposition()
            end
    
            local bool = value
            GUI.Frame.xOfs:SetDisabled(bool)
            GUI.Frame.yOfs:SetDisabled(bool)
            GUI.Frame.Anchor:SetDisabled(bool)
            GUI.Frame.HSpace:SetDisabled(bool)
            GUI.Frame.Space:SetDisabled(bool)
            GUI.Frame.Rows:SetDisabled(bool)
        end

        GUI.Frame = GUI.Frame or {}

        local title = AceGUI:Create("Heading")
            title:SetText("Frame")
            title:SetFullWidth(true)
            container:AddChild(title)

        local FFA = AceGUI:Create("CheckBox")
            FFA:SetLabel("Free Frames Alignment")
            FFA:SetDescription("Allows Frames to take your positions")
            FFA:SetValue(IRT_Orders.Settings.Buttons["Free Frames Alignment"])
            FFA:SetCallback("OnValueChanged", FFA_CallBack)
            container:AddChild(FFA)
            GUI.Frame.FFA = FFA

        local LF = AceGUI:Create("CheckBox")
            LF:SetLabel("Lock Frames")
            LF:SetDescription("Locks Frames positions (becoming unclickable)")
            LF:SetValue(IRT_Orders.Settings.Buttons["Lock Frames"])
            LF:SetCallback("OnValueChanged", LF_CallBack)
            container:AddChild(LF)
            GUI.Frame.LF = LF

        local anchors = {
            ["TOPLEFT"] = "Down and then Right",
            ["TOPRIGHT"] = "Down and then Left",
            ["BOTTOMLEFT"] = "Up and then Right",
            ["BOTTOMRIGHT"] = "Up and then Left",
        }

        local Anchor = AceGUI:Create("Dropdown")
            Anchor:SetLabel("Grow Orientation")
            Anchor:SetWidth(150)
            Anchor:SetList(anchors)
            Anchor:SetValue(IRT_Orders.Frame.Layout.Anchor)
            Anchor:SetCallback("OnValueChanged", fAnchor_CallBack)
            container:AddChild(Anchor)
            GUI.Frame.Anchor = Anchor

        
        local Transp = AceGUI:Create("Slider")
            Transp:SetLabel("Transparency")
            Transp:SetWidth(150)
            Transp:SetSliderValues(0, 100, 1)
            Transp:SetValue(IRT_Orders.Frame.Layout.Transparency*100)
            Transp:SetCallback("OnValueChanged", fTransp_CallBack)
            container:AddChild(Transp)
            GUI.Frame.Transp = Transp

        local Scale = AceGUI:Create("Slider")
            Scale:SetLabel("Scale")
            Scale:SetWidth(150)
            Scale:SetSliderValues(0.1, 10, 0.01)
            Scale:SetValue(IRT_Orders.Frame.Layout.Scale)
            Scale:SetCallback("OnValueChanged", fScale_CallBack)
            container:AddChild(Scale)
            GUI.Frame.Scale = Scale

        local xOfs = AceGUI:Create("Slider")
            xOfs:SetLabel("First Frame X Offset")
            xOfs:SetWidth(150)
            xOfs:SetSliderValues(-2000, 2000, 1)
            xOfs:SetValue(IRT_Orders.Frame.Layout.xOfs)
            xOfs:SetCallback("OnValueChanged", fxOfs_CallBack)
            container:AddChild(xOfs)
            GUI.Frame.xOfs = xOfs

        local yOfs = AceGUI:Create("Slider")
            yOfs:SetLabel("First Frame Y Offset")
            yOfs:SetWidth(150)
            yOfs:SetSliderValues(-2000, 2000, 1)
            yOfs:SetValue(IRT_Orders.Frame.Layout.yOfs)
            yOfs:SetCallback("OnValueChanged", fyOfs_CallBack)
            container:AddChild(yOfs)
            GUI.Frame.yOfs = yOfs

        local Space = AceGUI:Create("Slider")
            Space:SetLabel("Frames Vertical Spacing")
            Space:SetWidth(150)
            Space:SetSliderValues(0, 500, 1)
            Space:SetValue(IRT_Orders.Frame.Layout.VSpace)
            Space:SetCallback("OnValueChanged", fSpace_CallBack)
            container:AddChild(Space)
            GUI.Frame.Space = Space

        local HSpace = AceGUI:Create("Slider")
            HSpace:SetLabel("Frames Horisontal Spacing")
            HSpace:SetWidth(150)
            HSpace:SetSliderValues(0, 500, 1)
            HSpace:SetValue(IRT_Orders.Frame.Layout.HSpace)
            HSpace:SetCallback("OnValueChanged", fHSpace_CallBack)
            container:AddChild(HSpace)
            GUI.Frame.HSpace = HSpace

        local Rows = AceGUI:Create("Slider")
            Rows:SetLabel("Frames Rows")
            Rows:SetWidth(150)
            Rows:SetSliderValues(1, 100, 1)
            Rows:SetValue(IRT_Orders.Frame.Layout.Rows)
            Rows:SetCallback("OnValueChanged", fRows_CallBack)
            container:AddChild(Rows)
            GUI.Frame.Rows = Rows
        
        if FFA:GetValue() then
            GUI.Frame.xOfs:SetDisabled(true)
            GUI.Frame.yOfs:SetDisabled(true)
            GUI.Frame.Anchor:SetDisabled(true)
            GUI.Frame.HSpace:SetDisabled(true)
            GUI.Frame.Space:SetDisabled(true)
            GUI.Frame.Rows:SetDisabled(true)
        elseif not FFA:GetValue() then
            GUI.Frame.LF:SetDisabled(true)
        end
    end

    local function Set_General_Section(container)

        local function ICF_CallBack(container, widget, value)
            IRT_Orders.Settings.Buttons["Ignore Class Filter"] = value
        end
    
        local function MiniMapIcon_CallBack(container, widget, value)
            InvisusRT.db.profile.hide = value
            if value then
                InvisusRT.icon:Hide("InvisusRT!")
            else
                InvisusRT.icon:Show("InvisusRT!")
            end
        end

        local title = AceGUI:Create("Heading")
            title:SetText("General Settings")
            title:SetFullWidth(true)
            container:AddChild(title)
    
        local MiniMapIcon = AceGUI:Create("CheckBox")
            MiniMapIcon:SetLabel("Hide MiniMap Icon")
            MiniMapIcon:SetValue(IRT_Orders.Settings.Buttons["Ignore Class Filter"])
            MiniMapIcon:SetCallback("OnValueChanged", MiniMapIcon_CallBack)
            container:AddChild(MiniMapIcon)
            GUI.MiniMapIcon = MiniMapIcon

        local ICF = AceGUI:Create("CheckBox")
            ICF:SetLabel("Ignore Class Filter")
            ICF:SetDescription("Receive Orders Data not only on your class")
            ICF:SetValue(IRT_Orders.Settings.Buttons["Ignore Class Filter"])
            ICF:SetCallback("OnValueChanged", ICF_CallBack)
            container:AddChild(ICF)
            GUI.ICF = ICF
    end

    local function Set_About_Section(container)
        local title = AceGUI:Create("Heading")
            title:SetText("Spell Orders - What is this?")
            title:SetFullWidth(true)
            container:AddChild(title)
    end
    
    local function SpellOrders_tree_CallBack(container, widget, group)
        local lastPath = addonTable.GUI_LastPath
        local test = tonumber(group)
        container:ReleaseChildren() 
        container:SetFullHeight(true)
        container:SetLayout("Fill")
        
        local scroll = AceGUI:Create("ScrollFrame")
        scroll:SetLayout("Flow")
        container:AddChild(scroll)


        if group == 1 then
            Set_General_Section(scroll)
        elseif group == 2 then
            Set_Frame_Section(scroll)
        elseif group == "2\0011" or group == 21 then
            Set_Frame_General_Section(scroll)
        elseif group == "2\0012" or group == 22 then
            Set_Frame_Title_Texture_Section(scroll)
        elseif group == "2\0013" or group == 23 then
            Set_Frame_Title_Icon_Section(scroll)
        elseif group == "2\0014" or group == 24 then
            Set_Frame_Title_Text_Section(scroll)
        elseif group == 3 then
            Set_StatusBar_Section(scroll)
        elseif group == "3\0011" or group == 31 then
            Set_StatusBar_General_Section(scroll)
        elseif group == "3\0012" or group == 32 then
            Set_StatusBar_Texture_Section(scroll)
        elseif group == "3\0013" or group == 33 then
            Set_StatusBar_StateIcon(scroll)
        elseif group == "3\0014" or group == 34 then
            Set_StatusBar_Text_Section(scroll)
        elseif group == 4 then
            Set_About_Section(scroll)
        end

        addonTable.GUI.LastPath = group
    end

    container:SetFullHeight(true)
    container:SetLayout("Fill")

    frame:SetTree(spellOrders_settings)
    frame:SetFullHeight(true)
    frame:SetCallback("OnGroupSelected", SpellOrders_tree_CallBack)
    container:AddChild(frame)

    StatusTable = StatusTable or { groups = {} }
    frame:SetStatusTable( StatusTable )
    frame:RefreshTree()

    if addonTable.GUI.LastPath then
        frame:SelectByPath(addonTable.GUI.LastPath)
    else
        frame:SelectByPath(1)
    end
end

function InvisusRT:SpellOrders_GUI(container, AceGUI)
    SpellOrders_GUI(container, AceGUI)
end

---------------------------------------
---------- LOADING VARIABLES ----------
---------------------------------------
local IRTordersDB = CreateFrame("Frame", "loadsDB")
IRTordersDB:RegisterEvent("ADDON_LOADED")
IRTordersDB:RegisterEvent("PLAYER_LOGOUT")
IRTordersDB:RegisterEvent("PLAYER_LOGIN")
function IRTordersDB:OnEvent(event, arg1)
    if event == "ADDON_LOADED" then
        if arg1 == "InvisusRaidTools" then
            IRT_Orders = addonTable.db.profile.IRT_Orders
            IRT_Orders.frames = IRT_Orders.frames or {}
        end
    elseif event == "PLAYER_LOGOUT" then
        if IRT_Orders then
            IRT_Orders.framesData = nil

            if arrayLength(IRT_Orders.frames) > 0 then
                for i in pairs(IRT_Orders.frames) do
                    if not IRT_Orders.frames[i].lock then
                        IRT_Orders.frames[i] = nil
                    end
                end
            end
        end

    elseif event == "PLAYER_LOGIN" then
        addonTable.RefreshFrames(self)
        self:UnregisterEvent("PLAYER_LOGIN")
    end
end

IRTordersDB:SetScript("OnEvent", IRTordersDB.OnEvent)