local addonName, addonTable = ...

local spell_info = {
    ["Death Knight"] = {
        ["Hysteria"] = {180, 49016},
    },

    ["Druid"] = {
        ["Innervate"] = {180, 29166},
        ["Rebirth"] = {600, 48477},
    },

    ["Hunter"] = {
        ["Misdirection"] = {30, 34477},
    },

    ["Mage"] = {
    },

    ["Paladin"] = {
        ["Hand of Sacrifice"] = {120, 6940},
        ["Aura Mastery"] = {120, 31821},
        ["Hand of Freedom"] = {25, 1044},
        ["Holy Wrath"] = {30, 48817},
        ["Hand of Protection"] = {300, 5599},
        ["Divine Sacrifice"] = {120, 64205},
    },

    ["Priest"] = {
        ["Divine Hymn"] = {480, 64843},
    },

    ["Rogue"] = {
        ["Tricks of the Trade"] = {30, 57933},
    },

    ["Shaman"] = {
        ["Mana Tide Totem"] = {300, 16190},
    },

    ["Warlock"] = {
    },

    ["Warrior"] = {
        ["Intimidating Shout"] = {120, 5246},
    },
}

addonTable.IRT_Orders_Spell_Proc_Info = {
    ["Tricks of the Trade"] = 57934,
    ["Misdirection"] = 34477,
}

local classes = {
    [1] = "Death Knight",
    [2] = "Druid",
    [3] = "Hunter",
    [4] = "Mage",
    [5] = "Paladin",
    [6] = "Priest",
    [7] = "Rogue",
    [8] = "Shaman",
    [9] = "Warlock",
    [10] = "Warrior",
}

local nestedTable = {}

for class in pairs(spell_info) do
    local arr = spell_info[class]
    if arrayLength(arr) > 0 then
        for spellName in pairs(arr) do
            nestedTable[spellName] = nestedTable[spellName] or {}
            local t = spell_info[class][spellName]
            local nestA, nestB, nestC, nestD = t[1], t[2], class, string.upper(class)

            table.insert(nestedTable[spellName], nestA)
            table.insert(nestedTable[spellName], nestB)
            table.insert(nestedTable[spellName], nestC)
            table.insert(nestedTable[spellName], nestD)
        end
    end
end


addonTable.orders_spnested = nestedTable
addonTable.orders_sorted = spell_info
addonTable.classes = classes