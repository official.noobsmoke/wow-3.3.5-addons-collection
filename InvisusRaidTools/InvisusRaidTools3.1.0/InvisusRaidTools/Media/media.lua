-- Media for Invisus Raid Tools
-- Useful Raiding Tools for WotLK
-- Author: Merfin @ https://invisus.shivtr.com/


local addonName, addonTable = ...

local media = {
    ["Font"] = {
        ["FRIZQT"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\FRIZQT__.TTF",
        ["Accidental Presidency"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\Accidental Presidency.ttf",
        ["contb"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\contb.ttf",
        ["Expressaway"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\Expressway.ttf",
        ["Homespun"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\Homespun.ttf",
        ["HOOGE"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\HOOGE.TTF",
        ["Jobless"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\Jobless.ttf",
        ["Mitsuha"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\Mitsuha.ttf",
        ["PTSansNarrow"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\Fonts\\PTSansNarrow.ttf",
    },
    ["Texture"] = {
        ["Aluminium"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Aluminium.tga",
        ["Armory"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Armory.tga",
        ["BantoBar"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\BantoBar.tga",
        ["Glaze2"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Glaze2.tga",
        ["Gloss"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Gloss.tga",
        ["Graphite"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Graphite.tga",
        ["Grid"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Grid.tga",
        ["Healbot"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Healbot.tga",
        ["LiteStep"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\LiteStep.tga",
        ["Minimalist"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Minimalist.tga",
        ["normTex"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\normTex.tga", 
        ["Otravi"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Otravi.tga",
        ["Outline"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Outline.tga",
        ["Perl"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Perl.tga",
        ["Round"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Round.tga",
        ["Smooth"] = "Interface\\AddOns\\InvisusRaidTools\\Media\\StatusBar\\Smooth.tga",
    },
}

local mediaSorted = {}
mediaSorted.Font = {}
mediaSorted.Texture = {}

for i in pairs(media.Font) do
    table.insert(mediaSorted.Font, i)
end

for i in pairs(media.Texture) do
    table.insert(mediaSorted.Texture, i)
end

addonTable.media = media
addonTable.mediaSorted = mediaSorted