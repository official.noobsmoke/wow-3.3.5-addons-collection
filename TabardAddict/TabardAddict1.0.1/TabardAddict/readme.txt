Tabard Addict
Mod for World of Warcraft
author: gmz323(Greg)
current version: v1.01

Description: 
============
A simple mod that shows which tabards you have and have not equipped on your way to meeting the various tabard achievements.

Download locations:
===================
* wowinterface
* curse

Usage:
======
/ta
/tabardaddict

Features:
=========
* Shows all tabards that count towards equip count achievement including tabards not available to your faction or currently in game.
* Mouse Over tabard icon - shows in game tooltip.
* Ctrl-Click tabard icon - will launch Dressing Room with tabard equipped. See what your character looks like in the other factions tabards. Also preview TCG tabards before buying.
* Shift-Click tabard icon - will insert item link into active chat edit box.

Coming Soon:
============
* Add filter settings - filter out tabards you don't want to see in the display
* Add sort settings - optional sort settings on the tabards shown
* Add some basic info about each of the tabards so everything doesn't have to be looked up online.
* additional table optimizations

FAQ(s):
=======
(Q) Why show tabards not available to my character?
(*) Many people like to see what their horde character looks like in alliance tabards and vice versa in the Dressing Room.
(*) You can also see what your character looks like in TCG loot code tabards before buying a loot code.

(Q)I just equipped a new tabard but the addon did not update.
(*) The addon updates when the window is opened, so close and reopen the addon window.


Changelog:
==========
v1.01 (2011-03-01)
------------------
* Initial release of Tabard Addict


