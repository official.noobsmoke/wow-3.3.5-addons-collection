## Interface: 40000
## Title: Tabard Addict
## Version: 1.01
## Author: gmz323(Greg)
## Notes: Shows which tabards you have equipped on your way to meeting the tabard collection achievements.
## SavedVariablesPerCharacter: TabardAddictServerData
TabardAddictTabardData.lua
TabardAddict.lua
TabardAddict.xml