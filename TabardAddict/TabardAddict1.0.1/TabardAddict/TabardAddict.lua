--  ===============================
--	Tabard Addict - TabardAddict.lua
--	By: gmz323(Greg)
--  ===============================

-- Constants
local TABARD_ADDICT_VERSION = "1.01"
local TABARD_ADDICT_DATA_ROWS = 85



-- Locals
local TabardAddictServerData = {} -- equip data from server

local CurrentTabardEntry_line = 0;
local CurrentTabardEntries_itemID = {} 
local CurrentTabardEntries_itemLink = {}

local tempDisplayData = {} -- temp table built to show subset of data
local tempDisplayData_rowcount;
local tabardsEquipped = 0;

-- Slash commands
SLASH_TABARDADDICT1 = "/ta"
SLASH_TABARDADDICT2 = "/tabardaddict"

SlashCmdList["TABARDADDICT"] = function(msg, editbox)
	if frameTabardAddict:IsShown() then
		frameTabardAddict:Hide()
	else
		frameTabardAddict:Show()
	end
end

--*****************************************************************************
--*****************************************************************************
local function TabardAddict_PopulateDisplayTable()
	
	local maintableRow, j, b1, b2;
	
	wipe(tempDisplayData);
	
	-- This creates a temp table by joining local tabard data and server results
	for maintableRow in ipairs(taTabardData)
	do
      tempDisplayData[maintableRow] = {};     -- create a new row
	  tempDisplayData_rowcount = maintableRow;
      for j=1,4 do
        tempDisplayData[maintableRow][j] = taTabardData[maintableRow][j];
      end
	  -- init equipped bool
	  tempDisplayData[maintableRow][5] = false;
	  -- set equipped bool from server data
	  for b1,b2 in ipairs(TabardAddictServerData)
	  do
		if (b2[1] == tempDisplayData[maintableRow][1]) then
			tempDisplayData[maintableRow][5] = TabardAddictServerData[b1][2];
		end
	  end
    end
end

--*****************************************************************************
--*****************************************************************************
local function TabardAddict_SortDisplayTable()

	-- currently sorting only on tabard name
	table.sort(tempDisplayData, function(a,b) return a[2]<b[2] end)
	
end

--*****************************************************************************
--*****************************************************************************
local function TabardAddict_OnEvent(self, event, ...)
	-- not currently handling any events
end
 
--*****************************************************************************
--*****************************************************************************
function TabardAddict_GetServerData() 

	local j,m,r,a,b;
	
	wipe(TabardAddictServerData);
	
	-- id 1020 = ten tabards achievement
	a=GetAchievementCriteriaInfo 
	for b=1,TABARD_ADDICT_DATA_ROWS
	do 
		_,_,_,_,_,_,_,_,_,j = a(1021,b); 
		_,_,m,_,_,_,_,r,_,_ = a(j);
		TabardAddictServerData[b] = {}     -- create a new row
		TabardAddictServerData[b][1] = r;  -- save id
		TabardAddictServerData[b][2] = m;  -- save "criteria met" true/false
		if m==true then
			-- increment total tabards equipped
			tabardsEquipped=tabardsEquipped + 1;
		end;
	end
end

--*****************************************************************************
--*****************************************************************************
function TabardAddictScrollBar_Update()
	local line; -- 1 through 8 of our window to scroll
	local lineplusoffset; -- an index into our data calculated from the scroll offset
	
	FauxScrollFrame_Update(TabardAddictScrollBar,tempDisplayData_rowcount,8,40);
	for line=1,8 do
		lineplusoffset = line + FauxScrollFrame_GetOffset(TabardAddictScrollBar);
		if lineplusoffset <= tempDisplayData_rowcount then
			getglobal("TabardAddictEntry"..line).TabardName:SetText(tempDisplayData[lineplusoffset][2]);
			getglobal("TabardAddictEntry"..line).TabardName:Show();
			
			-- check if equippred
			if (tempDisplayData[lineplusoffset][5] == true) then
				getglobal("TabardAddictEntry"..line).TabardEquipped:SetText("Been Equipped: |cff00FF00Yes|r");
				-- show overlay on equipped tabards
				getglobal("TabardAddictEntry"..line).TabardIconOverlay:Show(); 
			else
				getglobal("TabardAddictEntry"..line).TabardEquipped:SetText("Been Equipped: |cffFF0000No|r");
				getglobal("TabardAddictEntry"..line).TabardIconOverlay:Hide();
			end
			getglobal("TabardAddictEntry"..line).TabardEquipped:Show();
			
			-- set tabard icon texture
			getglobal("TabardAddictEntry"..line).TabardIcon:SetTexture(tempDisplayData[lineplusoffset][4]);
			getglobal("TabardAddictEntry"..line).TabardIcon:Show();
			-- set alpha values
			getglobal("TabardAddictEntry"..line).TabardAddictButtonBack:SetAlpha(0.35);
			getglobal("TabardAddictEntry"..line).TabardAddictButtonHighlight:SetAlpha(0.70);
			
			-- set local vars
			CurrentTabardEntries_itemID[line] = tempDisplayData[lineplusoffset][1];
			CurrentTabardEntries_itemLink[line] = tempDisplayData[lineplusoffset][3];
			
			-- update tooltip
			if CurrentTabardEntry_line > 0 then
				TabardAddictEntryButton_OnEnter(CurrentTabardEntry_line);
			end
		else
			getglobal("TabardAddictEntry"..line).TabardName:Hide();
			getglobal("TabardAddictEntry"..line).TabardEquipped:Hide();
			getglobal("TabardAddictEntry"..line).TabardIcon:Hide();
			getglobal("TabardAddictEntry"..line).TabardIconOverlay:Hide();
		end
	end
end

--*****************************************************************************
--*****************************************************************************
function TabardAddictEntryButton_OnEnter(line)
	CurrentTabardEntry_line = line;
end

--*****************************************************************************
--*****************************************************************************
function TabardAddictEntryButton_OnLeave()
	CurrentTabardEntry_line = 0;
	ResetCursor();
	GameTooltip:Hide();
end

--*****************************************************************************
--*****************************************************************************
function TabardAddictEntryButton_OnUpdate(line)
	
	local itemID = CurrentTabardEntries_itemID[line];
	
	-- Show tooltip if over tabard icon
	if ( MouseIsOver(getglobal("TabardAddictEntry"..line).TabardIcon) ) then
		GameTooltip:SetOwner(frameTabardAddict, "ANCHOR_RIGHT", 0, -120);
		GameTooltip:SetHyperlink("item:"..itemID);
		GameTooltip:Show();
		-- if ctrl is down show inspect cursor else reset it
		if ( IsModifiedClick("DRESSUP") ) then
			ShowInspectCursor();
		else
			ResetCursor();
		end
	else
		-- not over icon - hide tooltip and reset cursor
		GameTooltip:Hide();
		ResetCursor();
	end
end

--*****************************************************************************
--*****************************************************************************
function TabardAddictEntryButton_OnModifiedClick(line)
	local itemID = CurrentTabardEntries_itemID[line];
	local itemLink = CurrentTabardEntries_itemLink[line];
	
	-- If there was a DRESSUP or CHATLINK click over a tabard icon handle it
	if ( MouseIsOver(getglobal("TabardAddictEntry"..line).TabardIcon) ) then
		if ( IsModifiedClick("DRESSUP") ) then
			DressUpItemLink(itemID);
		elseif ( IsModifiedClick("CHATLINK") ) then
			if (ChatEdit_GetActiveWindow()) then
				ChatEdit_InsertLink(itemLink);
			end;
		end;
	end;
end

--*****************************************************************************
--*****************************************************************************
function TabardAddict_OnShow(self)
	
	local tabardsNextAchievement = 0;
	
	PlaySound("igCharacterInfoOpen");
	
	-- set description text
	self.taDescText:SetText("Many of the tabards listed below are not available to "..UnitName("player")..". More display filtering options coming soon!");
	
	-- reset # of equipped tabards
	tabardsEquipped = 0;
	-- query server for data
	TabardAddict_GetServerData()

	-- populate counts
	if (tabardsEquipped >= 30) then
		tabardsNextAchievement = "none";
	elseif (tabardsEquipped >= 25) then
		tabardsNextAchievement = 30;
	elseif (tabardsEquipped >= 10) then
		tabardsNextAchievement = 25;
	elseif (tabardsEquipped >= 1) then
		tabardsNextAchievement = 10;
	else
		tabardsNextAchievement = 1;
	end;
	
	-- populate equipped count and next achievement text
	self.taTabardTotalText:SetText("Tabards Equipped: "..tabardsEquipped.."    Next Achievement: "..tabardsNextAchievement);
	
	-- populate the display table
	TabardAddict_PopulateDisplayTable();
	-- sort the display table
	TabardAddict_SortDisplayTable();
	-- initial display
	TabardAddictScrollBar_Update();
end

--*****************************************************************************
--*****************************************************************************
function TabardAddict_OnHide(self)

	PlaySound("igCharacterInfoClose");
	-- collectgarbage("collect");
end


--*****************************************************************************
--*****************************************************************************
function TabardAddict_OnLoad(self)

	-- Register the frame so it can be dragged
	self:RegisterForDrag("LeftButton")

	-- update portrait emblem
	SetPortraitToTexture(self.taTabardEmblem, "Interface\\ICONS\\INV_Chest_Cloth_30")
	
	self.taTitleText:SetText("Tabard Addict");
	self.taVersionText:SetText("v"..TABARD_ADDICT_VERSION);

	-- set alliance or horde background
	local englishFaction = UnitFactionGroup("player");
	if ( englishFaction == "Horde" ) then  -- horde
		TabardAddictBGFaction:Show();
		TabardAddictBGFaction2:Hide();
	else  -- alliance
		TabardAddictBGFaction:Hide();
		TabardAddictBGFaction2:Show();
	end;
end



