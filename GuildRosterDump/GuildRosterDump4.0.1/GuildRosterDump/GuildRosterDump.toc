## Interface: 40000
## X-Curse-Packaged-Version: v4.0.1-Release
## X-Curse-Project-Name: GuildRosterDump
## X-Curse-Project-ID: guildrosterdump
## X-Curse-Repository-ID: wow/guildrosterdump/mainline

## Title: GuildRosterDump
## Notes: Dumps guild roster info to a frame for copy/paste into Excel. Use /gdump to do the dump.
## Author: Quaiche
## Version: $project-version$
## X-Category: Misc
## X-License: Apache 2.0
## OptionalDeps: tekDebug, tekErr

libs\LibStub.lua
libs\tekKonfigAboutPanel.lua
libs\QScrollingEditBox.lua

GuildRosterDump.lua
