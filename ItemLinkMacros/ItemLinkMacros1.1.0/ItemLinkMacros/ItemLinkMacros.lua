--
-- ItemLinkMacros
-- Author: Nafe
-- Guild: Clan Blackhoof
-- Server: Cenarion Circle
--

--This addon may cause some sort of taint.  This needs testing
--UPDATE: seems fine in combat.  EditMacro seems to fail silently regardless of this addon.

local _G = getfenv(0)

--stack references for globals
local GetItemInfo = _G.GetItemInfo
local GetItemSpell = _G.GetItemSpell

local strfind = _G.strfind

----------------------------------------------------------
------- Replacing the ChatEdit_InsertLink function -------
----------------------------------------------------------
--I don't really need to store this, but whatever
local oldFunction = ChatEdit_InsertLink

ChatEdit_InsertLink = function(text)
   if ( not text ) then
      return false;
   end
   local activeWindow = ChatEdit_GetActiveWindow();
   if ( activeWindow ) then
      -- don't add a space for parsing... what was Blizzard thinking?
      -- activeWindow:Insert(" "..text);
      activeWindow:Insert(text);
      return true;
   end
   if ( BrowseName and BrowseName:IsVisible() ) then
      local item;
      if ( strfind(text, "item:", 1, true) ) then
         item = GetItemInfo(text);
      end
      if ( item ) then
         BrowseName:SetText(item);
         return true;
      end
   end
   if ( MacroFrameText and MacroFrameText:IsVisible() ) then
      local macroText = MacroFrameText:GetText();
      
      local item;
      if ( strfind(text, "item:", 1, true) ) then
         item = GetItemInfo(text);
      end
      -- my check for whether this is a spell or "other" link is kinda bad.  I could improve it with a Regex, but I doubt there will be many problems
      local spellLink = strfind(text, "|Hspell:", 1, true)
      
      local enchantLink = strfind(text, "|Henchant:", 1, true)
      local achievementLink = strfind(text, "|Hachievement:", 1, true)
      local talentLink = strfind(text, "|Htalent:", 1, true)
      local otherLink = enchantLink or achievementLink or talentLink
      
      if ( not otherLink and macroText == "" and not item ) then
         MacroFrameText:Insert(SLASH_CAST1.." "..text);
      elseif ( not otherLink and not spellLink and ( strfind(macroText, SLASH_USE1, 1, true) or strfind(macroText, SLASH_EQUIP1, 1, true) or strfind(macroText, SLASH_CAST1, 1, true) ) ) then
         MacroFrameText:Insert(item or text);
      else
         -- Why did Blizzard decide to strip item hyperlinks anyways?
         -- MacroFrameText:Insert(item or text);
         MacroFrameText:Insert(text);
      end
      return true;
   end
   return false;
end

-----------------------------------------------------------
-------------------- Slash Handlers =) --------------------
-----------------------------------------------------------
--have a slash handler to insert any text into the MacroFrameText box directly
SLASH_ADDTEXT1="/addtext"
SlashCmdList["ADDTEXT"] = function(text)
   if ( MacroFrameText and MacroFrameText:IsVisible() ) then
      MacroFrameText:Insert(text)
   end
end
