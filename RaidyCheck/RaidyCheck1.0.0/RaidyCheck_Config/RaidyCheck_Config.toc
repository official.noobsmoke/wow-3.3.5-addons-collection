## Interface: 30300
## X-Curse-Packaged-Version: 1.0
## X-Curse-Project-Name: RaidyCheck
## X-Curse-Project-ID: raidycheck
## X-Curse-Repository-ID: wow/raidycheck/mainline

## Title: RaidyCheck - configuration
## Notes: Raid boss mod and buff checking.
## Author: Adirelle
## Version: 1.0 
## Dependencies: RaidyCheck

# Use awesome AddonLoader to make the configuration LoD
## LoadManagers: AddonLoader
## X-LoadOn-InterfaceOptions: RaidyCheck
## X-LoadOn-Slash: /raidycheck /rdc

#@no-lib-strip@
..\RaidyCheck\libs\AceGUI-3.0\AceGUI-3.0.xml
..\RaidyCheck\libs\AceConfig-3.0\AceConfig-3.0.xml
#@end-no-lib-strip@

..\RaidyCheck\Config.lua
