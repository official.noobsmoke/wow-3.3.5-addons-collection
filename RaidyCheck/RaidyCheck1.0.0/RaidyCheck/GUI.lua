--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

local TOOLTIP, SIDE = GameTooltip, 'ANCHOR_TOPLEFT'
local function SetTooltip(frame, contentCallback, anchor)
	anchor = anchor or frame
	frame:SetMotionScriptsWhileDisabled(true)
	frame.ShowTooltip = function(self)
		TOOLTIP:SetOwner(anchor, SIDE)
		TOOLTIP:ClearLines()
		contentCallback(TOOLTIP)
		TOOLTIP:Show()
	end
	frame.HideTooltip = function(self)
		if TOOLTIP:GetOwner() == anchor then
			TOOLTIP:Hide()
		end
	end
	frame:SetScript('OnEnter', frame.ShowTooltip)
	frame:SetScript('OnLeave', frame.HideTooltip)
end

local function Anchor_OnDragStart(self)
	if addon.db.profile.lockAnchor or self.moving then return end
	self:HideTooltip()
	self.moving = true
	addon:HidePanel()
	return self:StartMoving()
end

local function Anchor_OnDragStop(self)
	if not self.moving then return end
	self.moving = false
	self:StopMovingOrSizing()
	local anchorDB = addon.db.profile.anchor
	anchorDB[1], anchorDB[2], anchorDB[3], anchorDB[4], anchorDB[5] = self:GetPoint()
	addon:Debug("OnDragStop", unpack(anchorDB))
	if self:IsMouseOver() then self:ShowTooltip() end
	return addon:UpdatePanel()
end

local function Anchor_OnClick(self)
	local value = not addon.db.profile.alternateDisplay
	addon.db.profile.alternateDisplay = value
	addon:OnConfigChanged("alternateDisplay", value)
end

local function ConfigButton_OnClick(self, button)
	if button == "LeftButton" then
		return InterfaceOptionsFrame_OpenToCategory('RaidyCheck')
	end
end

local function StrictButton_OnClick(self)
	addon:SetStrictMode(not addon.strictChecking)
	PlaySound(addon.strictChecking and "igMainMenuOptionCheckBoxOn" or "igMainMenuOptionCheckBoxOff")
end

local function ExpandButton_UpdateTexture(self)
	local offset = addon.db.profile.collapsed and 0 or 0.5
	self:GetNormalTexture():SetTexCoord(0, 0.5, offset, offset+0.5)
	self:GetPushedTexture():SetTexCoord(0.5, 1, offset, offset+0.5)
end

local function ExpandButton_OnClick(self)
	local collapsed = not addon.db.profile.collapsed
	addon.db.profile.collapsed = collapsed
	PlaySound(collapsed and "igMainMenuOptionCheckBoxOff" or "igMainMenuOptionCheckBoxOn")
	self:UpdateTexture()
	addon:OnConfigChanged('collapsed', collapsed)
end

local function ReadyCheckButton_OnEvent(self, event)
	if IsRealPartyLeader() or IsRealRaidLeader() or IsRaidOfficer() then
		self:Enable()
	else
		self:Disable()
	end
end

local function Anchor_LayoutButtons(self)
	local previous, to, ox, oy = self, "TOPRIGHT", -8, -5
	local num = 0
	local width = 16 + self.text:GetStringWidth()
	for i, button in ipairs(self.buttons) do
		if addon.db.profile.buttons[button.dbKey] then
			button:SetPoint("TOPRIGHT", previous, to, ox, oy)
			button:Show()
			width = width - ox + button:GetWidth()
			previous, to, ox, oy = button, "TOPLEFT", -2, 0
			num = num + 1
		else
			button:Hide()
		end
	end
	self:SetWidth(width)
end

function addon:CreateAnchor()
	local anchor = CreateFrame("Button", nil, UIParent)
	anchor:SetHeight(24)
	anchor:SetBackdrop({
		bgFile = "Interface\\Tooltips\\UI-Tooltip-Background", tile = true, tileSize = 16,
		edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 16,
		insets = {left = 5, right = 5, top = 5, bottom = 5}
	})
	anchor:SetBackdropColor(0.09, 0.09, 0.09, 0.5)
	anchor:SetBackdropBorderColor(0.5, 0.5, 0.5, 1)
	anchor:SetPoint(unpack(self.db.profile.anchor))
	anchor:SetMovable(true)
	anchor:RegisterForDrag("LeftButton")
	anchor:SetScript("OnDragStart", Anchor_OnDragStart)
	anchor:SetScript("OnDragStop", Anchor_OnDragStop)
	anchor:SetScript("OnClick", Anchor_OnClick)

	SetTooltip(anchor, function(tooltip)
		tooltip:AddLine(L["RaidyCheck anchor"])
		tooltip:AddLine(L["Click to change displayed information."])
		if not addon.db.profile.lockAnchor then
			tooltip:AddLine(L["Drag to move."], 1, 1, 1)
		end
	end)

	local text = anchor:CreateFontString(nil, nil, "GameFontNormalSmall")
	text:SetPoint("LEFT", anchor, "LEFT", 8, 0)
	text:SetText(addonName)
	anchor.text = text

	local readyCheckButton = CreateFrame("Button", nil, anchor, "UIPanelButtonTemplate")
	readyCheckButton:SetWidth(16)
	readyCheckButton:SetHeight(16)
	readyCheckButton:SetText([[|TInterface\RaidFrame\ReadyCheck-Ready:0:0:0:0:64:64:6:59:6:59|t]])
	readyCheckButton:SetScript('OnClick', DoReadyCheck)
	readyCheckButton:SetScript('OnEvent', ReadyCheckButton_OnEvent)
	SetTooltip(readyCheckButton, function(tooltip)
		tooltip:AddLine(L["ReadyCheck"])
		tooltip:AddLine(L["Click to do a ready check."], 1, 1, 1)
	end, anchor)
	readyCheckButton:RegisterEvent("PARTY_MEMBERS_CHANGED")
	readyCheckButton:RegisterEvent("RAID_ROSTER_UPDATE")
	readyCheckButton:RegisterEvent("PLAYER_REGEN_ENABLED")
	ReadyCheckButton_OnEvent(readyCheckButton, 'OnCreate')
	readyCheckButton.dbKey = "readycheck"
	anchor.readyCheckButton = readyCheckButton

	local configButton = CreateFrame("Button", nil, anchor, "UIPanelButtonTemplate")
	configButton:SetWidth(16)
	configButton:SetHeight(16)
	configButton:SetText([[|TInterface\AddOns\RaidyCheck\media\config:0:0:0:0:32:32:0:23:0:23|t]])
	configButton:SetScript('OnClick', ConfigButton_OnClick)
	SetTooltip(configButton, function(tooltip)
		tooltip:AddLine(L["Configuration"])
		tooltip:AddLine(L["Click to open the configuration window."], 1, 1, 1)
	end, anchor)
	configButton.dbKey = "config"
	anchor.configButton = configButton

	local strictButton = CreateFrame("CheckButton", nil, anchor)
	strictButton:SetWidth(16)
	strictButton:SetHeight(16)
	strictButton:SetNormalTexture([[Interface\Buttons\UI-CheckBox-Up]])
	strictButton:SetPushedTexture([[Interface\Buttons\UI-CheckBox-Down]])
	strictButton:SetHighlightTexture([[Interface\Buttons\UI-CheckBox-Highlight]])
	strictButton:SetCheckedTexture([[Interface\Buttons\UI-CheckBox-Check]])
	strictButton:SetScript('OnClick', StrictButton_OnClick)
	SetTooltip(strictButton, function(tooltip)
		tooltip:AddLine(L['Strict mode'])
		tooltip:AddLine(L['Check to enable strict mode.'], 1, 1, 1)
	end, anchor)
	strictButton.dbKey = "strict"
	anchor.strict = strictButton

	local expandButton = CreateFrame("Button", nil, anchor)
	expandButton:SetWidth(16)
	expandButton:SetHeight(16)
	expandButton:SetNormalTexture([[Interface\Buttons\UI-Panel-QuestHideButton]])
	expandButton:SetPushedTexture([[Interface\Buttons\UI-Panel-QuestHideButton]])
	expandButton:GetNormalTexture():SetTexCoord(0, 0.5, 0.5, 1)
	expandButton:GetPushedTexture():SetTexCoord(0.5, 1, 0.5, 1)
	expandButton:SetHighlightTexture([[Interface\Buttons\UI-Panel-MinimizeButton-Highlight]])
	expandButton:SetScript('OnClick', ExpandButton_OnClick)
	SetTooltip(expandButton, function(tooltip)
		if addon.db.profile.collapsed then
			tooltip:AddLine(L['Expand'])
			tooltip:AddLine(L['Click to show the panel.'], 1, 1, 1)
		else
			tooltip:AddLine(L['Collapse'])
			tooltip:AddLine(L['Click to hide the panel.'], 1, 1, 1)
		end
	end, anchor)
	expandButton.dbKey = "expand"
	expandButton.UpdateTexture = ExpandButton_UpdateTexture
	expandButton:UpdateTexture()
	anchor.expandButton = expandButton

	anchor.LayoutButtons = Anchor_LayoutButtons
	anchor.buttons = { expandButton, readyCheckButton, strictButton, configButton }

	anchor:LayoutButtons()

	anchor:Hide()
	return anchor
end

