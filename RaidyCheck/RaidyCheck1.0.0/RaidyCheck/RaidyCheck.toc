## Interface: 30300
## X-Curse-Packaged-Version: 1.0
## X-Curse-Project-Name: RaidyCheck
## X-Curse-Project-ID: raidycheck
## X-Curse-Repository-ID: wow/raidycheck/mainline

## Title: RaidyCheck
## Notes: Raid boss mod and buff checking.
## Author: Adirelle
## Version: 1.0 
## OptionalDeps: Ace3, tekDebug, LibTalentQuery-1.0, LibGroupTalents-1.0, LibQTip-1.0

## SavedVariables: RaidyCheckDB

## LoadManagers: AddonLoader
## X-LoadOn-Instance: true

#@no-lib-strip@
libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\AceAddon-3.0\AceAddon-3.0.xml
libs\AceDB-3.0\AceDB-3.0.xml
libs\AceTimer-3.0\AceTimer-3.0.xml
libs\AceEvent-3.0\AceEvent-3.0.xml
libs\AceHook-3.0\AceHook-3.0.xml
libs\AceComm-3.0\AceComm-3.0.xml
libs\AceSerializer-3.0\AceSerializer-3.0.xml
libs\LibTalentQuery-1.0\lib.xml
libs\LibGroupTalents-1.0\lib.xml
libs\LibQTip-1.0\LibQTip-1.0.lua
#@end-no-lib-strip@

Localization.lua
RaidyCheck.lua
GUI.lua

modules\BrokenItems.lua
modules\BossMod.lua
modules\ClassBuffs.lua
modules\Flask.lua
modules\Mana.lua
modules\NonCombatBuffs.lua
modules\Pet.lua
modules\SelfBuffs.lua
modules\Soulstone.lua
modules\WellFed.lua

