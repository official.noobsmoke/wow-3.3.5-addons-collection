--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

-- L['Non-Combat Buffs']
local module = addon:NewModule('Non-Combat Buffs')

local GetSpellStrings = addon.GetSpellStrings

local aotp, aotpIcon = GetSpellStrings(13159) -- Aspect of the Pack
local crusader, crusaderIcon = GetSpellStrings(32223) -- Crusader Aura

function module:Test(event, guid, name, unit, class)
	if class == 'HUNTER' then
		local caster = select(8, UnitBuff(unit, aotp))
		if caster and UnitIsUnit(caster, unit) then
			return aotp, aotpIcon
		end
	elseif class == 'PALADIN' then
		local caster = select(8, UnitBuff(unit, crusader))
		if caster and UnitIsUnit(caster, unit) then
			return crusader, crusaderIcon
		end	
	end
end
