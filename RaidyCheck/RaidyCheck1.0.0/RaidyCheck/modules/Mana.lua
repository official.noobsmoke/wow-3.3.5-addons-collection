--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

-- L['Mana']
local module = addon:NewModule('Mana')

function module:OnEnable()
	module.prototype.OnEnable(self)
	self:RegisterEvent('UNIT_MANA', 'UpdateUnit')
	self:RegisterEvent('UNIT_MAXMANA', 'UpdateUnit')
	self:RegisterEvent('UNIT_DISPLAYPOWER', 'UpdateUnit')
end

function module:Test(event, guid, name, unit, class)
	if UnitPowerType(unit) ~= SPELL_POWER_MANA then return end	
	local min, max = UnitPower(unit, SPELL_POWER_MANA), UnitPowerMax(unit, SPELL_POWER_MANA)
	if max and max > 0 and max ~= 100 then
		local frac = 100 * min / max
		local info = string.format("%d%%", frac)
		if frac < (addon.strictChecking and 95 or 40) then
			return info, ' |cff72bfff'..info..'|r '
		else
			return info
		end
	end
end

