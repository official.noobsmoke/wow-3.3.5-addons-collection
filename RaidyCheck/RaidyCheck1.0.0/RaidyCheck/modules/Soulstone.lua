--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

-- L['Soulstone']
local module = addon:NewModule('Soulstone', 'AceTimer-3.0')

local stoneName, stoneIcon = addon.GetSpellStrings(20764)
module.legend = stoneIcon..' '..L['no soulstone found']

local soulstones = {} -- target guid => warlock guid
local warlocks = {} -- warlock guid => expiration time

function module:ClearUnit(event, guid, ...)
	module.prototype.ClearUnit(self, event, guid, ...)
	soulstones[guid] = nil
	warlocks[guid] = nil
end

function module:Test(event, guid, name, unit, class)
	local expirationTime, caster = select(7, UnitBuff(unit, stoneName))
	local casterGUID = caster and UnitGUID(caster)
	if casterGUID then
		local newTime = math.max(warlocks[casterGUID] or 0, expirationTime)
		if newTime ~= warlocks[casterGUID] then
			warlocks[casterGUID] = newTime
			if casterGUID ~= guid then
				self:UpdateUnit(event, casterGUID)
			end
		end
	end
	local oldCasterGUID = soulstones[guid]
	if oldCasterGUID and oldCasterGUID ~= casterGUID and oldCasterGUID ~= guid then
		self:UpdateUnit(event, oldCasterGUID)
	end
	local haveIt = caster and (stoneIcon..UnitName(caster))
	if class == 'WARLOCK' then
		local now, expirationTime, haveIt = GetTime(), warlocks[guid] or 0, haveIt and (haveIt..' - ') or ''
		if expirationTime > now then
			return haveIt..L['Cooldown:']..' '..string.format(SecondsToTimeAbbrev(expirationTime-now))
		else
			return haveIt..L['Ready'], stoneIcon
		end
	else
		return haveIt
	end
end

