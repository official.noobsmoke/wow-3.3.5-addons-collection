--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

local IconString, GetSpellStrings = addon.IconString, addon.GetSpellStrings
local UnitBuff = UnitBuff
local UnitClass = UnitClass

-- L['Class Buffs']
local module = addon:NewModule('Class Buffs')

local LibGroupTalents = LibStub('LibGroupTalents-1.0')

local hasSanctuary
local groupClasses = {}
local tests = {}

local sanctuaryName = GetSpellInfo(20911)

function module:OnEnable()
	module.prototype.OnEnable(self)
	LibGroupTalents.RegisterCallback(self, 'LibGroupTalents_UpdateComplete')
	LibGroupTalents:CheckForMissingTalents()
end

function module:OnDisable()
	module.prototype.OnDisable(self)
	LibGroupTalents.UnregisterAllCallbacks(self)
end

--[===[@debug@
local function unpackClasses(t, index)
	local name, value = next(t, index)
	if name then
		return name..'=', value, unpackClasses(t, name)
	else
		return
	end
end
--@end-debug@]===]

--local allClasses = { "DRUID", "HUNTER", "MAGE", "WARLOCK", "PALADIN",	"DEATHKNIGHT", "SHAMAN", "ROGUE", "PRIEST", "WARRIOR" }

local newClasses = {}
function module:UpdateClassStrengths(event)
	self:Debug('UpdateClassStrengths', event)
	local newSanc = false
	for guid, name, unit, class in self.core:IterateRoster() do
		newClasses[class] = (newClasses[class] or 0) + 1
		if class == 'PALADIN' and LibGroupTalents:GUIDHasTalent(guid, sanctuaryName) then
			newSanc = true
		end
	end
	local changed = (hasSanctuary ~= newSanc)
	hasSanctuary = newSanc
	for class in pairs(tests) do -- only check tested clases
		local newCount = newClasses[class]
		if groupClasses[class] ~= newCount then
			changed = true
			groupClasses[class] = newCount
		end
	end
	wipe(newClasses)
	--[===[@debug@
	if changed then
		self:Debug('Class strengths changed, hasSanctuary=', hasSanctuary, 'classes=', unpackClasses(groupClasses))
	end
	--@end-debug@]===]
	return changed
end

function module:LibGroupTalents_UpdateComplete(event)
	self:Debug('LibGroupTalents_UpdateComplete', event)
	if self:UpdateClassStrengths(event) then
		return self:UpdateAll(event)
	end
end

function module:OnRosterChange(event)
	self:Debug('OnRosterChange', event)
	if self:UpdateClassStrengths(event) then
		return self:UpdateAll(event)
	end
end

function module:UpdateAll(event)
	for guid, name, unit, class in self.core:IterateRoster() do
		self:_UpdateUnit(event, guid, name, unit, class)
	end
end

local pass, fail
do
	local allData, allFailures = {}, {}

	function pass(result)
		tinsert(allData, result)
		return true
	end

	function fail(result, data)
		if data then tinsert(allData, data) end
		tinsert(allFailures, result)
		return false
	end

	local tconcat = table.concat
	function module:Test(event, guid, name, unit, unitClass)
		if not next(groupClasses) then return end

		local unitRole = LibGroupTalents:GetGUIDRole(guid) or "unknown"
		-- groupClasses only contains entries for classes we have test for
		for class, classCount in next, groupClasses do
			tests[class](unit, classCount, unitClass, unitRole)
		end

		-- Concatenate test results
		local dataStr, failureStr = "", nil
		if #allData > 0 then
			dataStr = tconcat(allData, "")
			wipe(allData)
		end
		if #allFailures > 0 then
			failureStr = tconcat(allFailures, "")
			wipe(allFailures)
		end
		return dataStr, failureStr
	end
end

-- Test helpers

local function GenerateBuffTest(spellId, ...)
	local spellName, spellIcon = GetSpellStrings(spellId)
	local count = select('#', ...)
	if count == 0 then
		return function(unit)
			if UnitBuff(unit, spellName) then
				return pass(spellIcon)
			else
				return fail(spellIcon)
			end
		end
	else
		local buffs = { [spellName] = spellIcon }
		for i = 1, count do
			local name, icon = GetSpellStrings((select(i, ...)))
			buffs[name] = icon
		end
		return function(unit)
			for name, icon in next, buffs do
				if UnitBuff(unit, name) then
					return pass(icon)
				end
			end
			return fail(spellIcon)
		end
	end
end

local function IsManaUser(class, role)
	return role == 'caster' or role == 'healer' or (role == 'melee' and (class == 'HUNTER' or class == 'SHAMAN' or class == 'PALADIN'))
end

-- And now the tests

do -- Druid
	local wildTest = GenerateBuffTest(1126, 21849, 69381)
	local thornsTest = GenerateBuffTest(467)
	function tests.DRUID(unit, count, class, role)
		wildTest(unit)
		if role == 'tank' and addon.strictChecking then
			thornsTest(unit)
		end
	end
end

do -- Mage
	local intellectTest = GenerateBuffTest(1459, 23028, 61316, 61024, 54424)
	function tests.MAGE(unit, count, class, role)
		if IsManaUser(class, role) then
			intellectTest(unit)
		end
	end
end

do -- Priest
	local fortitudeTest = GenerateBuffTest(1243, 21562, 69377)
	local spiritTest = GenerateBuffTest(14752, 27681, 54424)
	function tests.PRIEST(unit, count, class, role)
		fortitudeTest(unit)
		if IsManaUser(class, role) then
			spiritTest(unit)
		end
	end
end

do -- Warrior
	local battleShoutTest = GenerateBuffTest(47436, 19740, 25782) -- check for Blessing of Might as they do not stack
	local commandingShoutTest = GenerateBuffTest(469)
	function tests.WARRIOR(unit, count)
		if addon.strictChecking then
			commandingShoutTest(unit)
			if count >= 2 and (role == 'melee' or role == 'tank') then
				battleShoutTest(unit)
			end
		end
	end
end

do -- Death Knight
	local hornOrWinterTest = GenerateBuffTest(57330, 58643)
	function tests.DEATHKNIGHT(unit, count, class, role)
		if addon.strictChecking and (role == 'melee' or role == 'tank') then
			hornOrWinterTest(unit)
		end
	end
end

do -- Paladin blessings
	local function GenerateBlessingTest(single, greater)
		local singleName, singleIcon = GetSpellStrings(single)
		local greaterName, greaterIcon = GetSpellStrings(greater)
		return function(unit)
			local caster = select(8, UnitBuff(unit, singleName))
			if caster then
				return pass(singleIcon..UnitName(caster))
			end
			caster = select(8, UnitBuff(unit, greaterName))
			if caster then
				return pass(greaterIcon..UnitName(caster))
			end
			return fail(singleIcon)
		end
	end
	
	local forgottenKings, forgottenKingsIcon = GetSpellStrings(69378)
	local kings = GenerateBlessingTest(20217, 25898)
	local wisdom = GenerateBlessingTest(19742, 25894)
	local might = GenerateBlessingTest(19740, 25782)
	local sanctuary = GenerateBlessingTest(20911, 25899)

	local blessings = {
		DRUIDtankSanc = { sanctuary, kings, might },
		SHAMANmelee   = { kings, might, wisdom },
		PALADINmelee  = { kings, might, wisdom },
		HUNTER        = { kings, might, wisdom },
		tankSanc      = { sanctuary, might },
		tank          = { kings, might },
		melee         = { kings, might },
		caster        = { kings, wisdom },
		healer        = { kings, wisdom },
	}

	function tests.PALADIN(unit, count, class, role)
		local tests
		if hasSanctuary and role == 'tank' then
			tests = blessings[class..'tankSanc'] or blessings.tankSanc
		else
			tests = blessings[class..role] or blessings[class] or blessings[role]
		end
		if not tests then return end
		for i, test in ipairs(tests) do
			if test == kings and UnitBuff(unit, forgottenKings) then
				pass(forgottenKingsIcon)
			else
				test(unit, count, class, role)
				if count == 1 then return end
				count = count - 1
			end
		end
	end
end

do -- Shaman
	local debuffName, debuffIcon = GetSpellStrings((UnitFactionGroup("player") == "Alliance") and 57723 or 57724)
	function tests.SHAMAN(unit, count, class, role)
		return UnitDebuff(unit, debuffName) and fail(debuffIcon)
	end
end
