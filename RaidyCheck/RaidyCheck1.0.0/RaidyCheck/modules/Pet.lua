--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

-- L['Pet']
local module = addon:NewModule('Pet')
module.strictChecking = true

function module:OnEnable()
	module.prototype.OnEnable(self)
	self:RegisterEvent('UNIT_PET', 'UpdateUnit')
end

local failure = addon.IconString([[Interface\Icons\ABILITY_SEAL]])
local petClasses = { HUNTER = true, WARLOCK = true }

module.legend = failure..' '..L['missing pet']

function module:Test(event, guid, name, unit, class)
	if not petClasses[class] or UnitUsingVehicle(unit) or UnitOnTaxi(unit) then return end	
	local petUnit
	if UnitIsUnit(unit, 'player') then
		if IsMounted() then 
			return 
		else
			petUnit = 'pet'
		end
	else
		petUnit = (unit..'pet'):gsub('(%d+)pet', 'pet%1')
	end
	if UnitExists(petUnit) then
		return UnitName(petUnit)
	else
		return "", failure
	end
end

