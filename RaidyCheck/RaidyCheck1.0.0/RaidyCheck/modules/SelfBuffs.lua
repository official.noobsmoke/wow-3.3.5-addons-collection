--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

local IconString, GetSpellStrings = addon.IconString, addon.GetSpellStrings
local UnitBuff = UnitBuff
local LibGroupTalents = LibStub('LibGroupTalents-1.0')

-- L['Self Buffs']
local module = addon:NewModule('Self Buffs')

local tests = {}

local pass, fail
do
	local allData, allFailures = {}, {}

	function pass(result)
		tinsert(allData, result)
	end

	function fail(result, data)
		if data then tinsert(allData, data) end
		tinsert(allFailures, result)
	end

	local tconcat = table.concat
	function module:Test(event, guid, name, unit, class)
		local test = tests[class]
		if not test then return end
		test(unit)
		local data, failure = "", nil
		if #allData > 0 then
			data = tconcat(allData, "")
			wipe(allData)
		end
		if #allFailures > 0 then
			failure = tconcat(allFailures, "")
			wipe(allFailures)
		end
		self:Debug(name, 'data=', data, 'failure=', failure)
		return data, failure
	end
end

-- Buff test generator
local function GenerateBuffTest(spellId, ...)
	local spellName, spellIcon = GetSpellStrings(spellId)
	local count = select('#', ...)
	if count == 0 then
		return function(unit)
			if UnitBuff(unit, spellName) then
				return pass(spellIcon)
			else
				return fail(spellIcon)
			end
		end
	else
		local buffs = { [spellName] = spellIcon }
		for i = 1, count do
			local name, icon = GetSpellStrings((select(i, ...)))
			buffs[name] = icon
		end
		return function(unit)
			for name, icon in next, buffs do
				if UnitBuff(unit, name) then
					return pass(icon)
				end
			end
			return fail(spellIcon)
		end
	end
end

-- Test a buff provided by a talent
local function GenerateTalentedBuffTest(talent, ...)
	local talentName = GetSpellInfo(talent)
	local test = GenerateBuffTest(talent, ...)
	return function(unit)
		if addon.strictChecking and LibGroupTalents:UnitHasTalent(unit, talentName) then
			return test(unit)
		end
	end
end

-- Test a buff that only tanks should use
local function GenerateTankOnlyTest(id)
	local name, icon = GetSpellStrings(id)
	return function(unit)
		local role = LibGroupTalents:GetUnitRole(unit)
		local hasIt = UnitBuff(unit, name) and icon
		module:Debug('Tank-Only', name, 'unit=', unit, 'role=', role, 'hasIt=', hasIt and true or false)
		if role == 'tank' and hasIt then
			return pass(icon)
		elseif role ~= 'tank' and not hasIt then
			return true
		else
			return fail(icon, hasIt)
		end
	end
end

-- And now the list of tested buffs

do -- Hunter: Trueshut Aura (when available) and any useful Aspect
	local trueShotTest = GenerateTalentedBuffTest(19506, 53137, 30809)
	local aspectTest = GenerateBuffTest(61846, 13165, 20043)

	function tests.HUNTER(unit)
		aspectTest(unit)
		trueShotTest(unit)
	end
end

do -- Priest: Inner Fire, Shadow Form and Vampirc Embrace (when available)
	local innerFireTest = GenerateBuffTest(48168)
	local shadowFormTest = GenerateTalentedBuffTest(15473)
	local vampiricEmbraceTest = GenerateTalentedBuffTest(15286)

	function tests.PRIEST(unit)
		if addon.strictChecking then
			innerFireTest(unit)
		end
		shadowFormTest(unit)
		vampiricEmbraceTest(unit)
	end
end

do -- Druid: appropriate form
	local forms = {
		tank = GenerateBuffTest(5487, 9634),
		healer = GenerateTalentedBuffTest(33891),
		melee = GenerateBuffTest(768),
		caster = GenerateTalentedBuffTest(24858),
	}

	function tests.DRUID(unit)
		local test = forms[LibGroupTalents:GetUnitRole(unit) or ""]
		return test and test(unit)
	end
end

-- Paladin: Righteous Fury
tests.PALADIN = GenerateTankOnlyTest(25780)

-- Death Knight: Frost Presence
tests.DEATHKNIGHT = GenerateTankOnlyTest(48263)


