--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

-- L['BossMod']
local module = addon:NewModule('BossMod', 'AceTimer-3.0')
module.strictChecking = true

local failure = addon.IconString([[Interface\Icons\INV_Misc_Gear_01]])
module.legend = failure..' '..L['no boss mod detected']

local BROADCAST_THROTLLE = 60
local PROBE_DELAY = 10

local playerName = UnitName('player')
local versions = {}
local unknowns
local pendingProbe

function module:OnEnable()
	module.prototype.OnEnable(self)
	for _, module in self:IterateModules() do
		module:SetEnabledState(true)
	end
	if not unknowns then
		unknowns = {}
		for guid, name in self.core:IterateRoster() do
			if not versions[guid] then
				self:Debug('No data about new player:', name)
				unknowns[guid] = true
			end
		end
	end
	self:UpdateSchedule()
end

function module:OnDisable()
	pendingProbe = nil
	return module.prototype.OnDisable(self)
end

function module:UpdateSchedule()
	local hasUnknowns = next(unknowns)
	if not pendingProbe and hasUnknowns then
		self:Debug('Scheduling probe')
		pendingProbe = self:ScheduleTimer('Probe', PROBE_DELAY)
	elseif pendingProbe and not hasUnknowns then
		self:Debug('Canceling scheduled probe')
		self:CancelTimer(pendingProbe, true)
		pendingProbe = nil
	end
end

local tmp = {}
function module:Probe()
	self:Debug('Probing')
	pendingProbe = nil
	for guid, name, unit in self.core:IterateRoster() do
		if unknowns[guid] and UnitIsConnected(unit) then
			tinsert(tmp, name)
			unknowns[guid] = nil
		end
	end
	if #tmp > 0 then
		for _, module in self:IterateModules() do
			module:Probe(tmp)
		end
		wipe(tmp)
		self:UpdateSchedule()
	end
end

function module:_AddUnit(event, guid, name, ...)
	if not versions[guid] and not unknowns[guid] then
		self:Debug('No data about new player:', name)
		unknowns[guid] = true
		self:UpdateSchedule()
	end
	return module.prototype._AddUnit(self, event, guid, name, ...)
end

function module:ClearUnit(event, guid, name, ...)
	if unknowns[guid] then
		self:Debug('Forgetting about', name)
		unknowns[guid] = nil
		self:UpdateSchedule()
	end
	return module.prototype.ClearUnit(self, event, guid, name, ...)
end

function module:Test(event, guid, name, unit, class)
	if versions[guid] then
		return versions[guid]
	else
		return "", failure
	end
end

function module:HasBossMod(name, bossModName, version)
	local guid, name, unit, class = addon:UnitData(name)
	if not guid then return end
	unknowns[guid] = nil
	self:Debug("HasBossMod", name, guid, bossModName, version)
	if bossModName then
		if version then
			versions[guid] = bossModName..' '..version
		elseif not versions[guid] then
			versions[guid] = bossModName
		end
	else
		versions[guid] = nil
	end
	self:UpdateUnit('HasBossMod', name)
end

-- Per-addon module prototype

local subModProto = {
	Debug = module.Debug,
}

module:SetDefaultModuleState(false)
module:SetDefaultModulePrototype(subModProto)

function subModProto:OnInitialize()
	self:Debug('OnInitialize')
	self.lastRemoteProbeTime = 0
end

function subModProto:OnDisable()
	self:Debug('OnDisable')
end

function subModProto:HasBossMod(name, version)
	return module:HasBossMod(name, self.label, version)
end

function subModProto:Probe(names)
	self.localVersion = self:ProbeLocal() or nil
	if self.localVersion then
		self:HasBossMod(playerName, self.localVersion)
	end
	local broadcastThreshold = (GetRealNumRaidMembers() > 0 and 5 or 3)
	if #names > broadcastThreshold or not self.SingleProbe then
		if GetTime() - self.lastRemoteProbeTime > BROADCAST_THROTLLE then
			self:Debug('Broadcast probing')
			if self:BroadcastProbe() then
				self:SeenRemoteProbe()
			end
		end
	else
		self:Debug('Probing players:', unpack(names))
		for _, name in pairs(names) do
			self:SingleProbe(name)
		end
	end
end

function subModProto:SeenRemoteProbe()
	self.lastRemoteProbeTime = GetTime()
end

function subModProto:SendComm(channel, prefix, target, ...)
	if channel == 'RAID' and (GetRealNumRaidMembers() == 0 and GetRealNumPartyMembers() == 0) then return end
	self:Debug('SendComm', channel, prefix, target, ...)
	if channel == 'WHISPER' then
		self:BackendSendComm(channel, prefix, target, ...)
	else
		self:BackendSendComm(channel, prefix, nil, target, ...)
	end
	return true
end

-- AceComm-based modules

local function DeserializerHelper(self, prefix, channel, sender, success, ...)
	if success then
		self:Debug('AceComm received:', prefix, channel, sender, success, ...)
		return self[prefix](self, prefix, channel, sender, ...)	
	end
end

function module:NewAceCommModule(name, label, ...)
	local prefixes = { ... }
	local submodule = self:NewModule(name, 'AceComm-3.0', 'AceSerializer-3.0')
	submodule.label = label
	function submodule:OnEnable()
		self:Debug('OnEnable')
		for i, prefix in ipairs(prefixes) do
			self:RegisterComm(prefix)
		end
	end
	function submodule:OnCommReceived(prefix, message, channel, sender)
		if sender == playerName then return end
		return DeserializerHelper(self, prefix, channel, sender, self:Deserialize(message))
	end
	function submodule:BackendSendComm(channel, prefix, target, ...)
		self:SendCommMessage(prefix, self:Serialize(...), channel, target, "BULK")
	end
	return submodule
end

-- CHAT_MSG_ADDON-based modules

function module:NewAddonMessageModule(name, label, ...)
	local prefixes = {}
	for i = 1, select('#', ...) do
		prefixes[select(i, ...)] = true
	end
	local submodule = self:NewModule(name, 'AceEvent-3.0')
	submodule.label = label
	function submodule:OnEnable()
		self:Debug('OnEnable')
		self:RegisterEvent('CHAT_MSG_ADDON')
	end
	function submodule:CHAT_MSG_ADDON(event, prefix, message, channel, sender)
		if sender == playerName or not prefixes[prefix] then return end
		self:Debug(event, prefix, message, channel, sender)
		return self[prefix](self, prefix, channel, sender, message)
	end
	function submodule:BackendSendComm(channel, prefix, target, ...)
		SendAddonMessage(prefix, strjoin("", ...), channel, target)
	end
	return submodule
end

-- Big Wigs

local bwMod = module:NewAddonMessageModule('BigWigs', 'BigWigs', 'BWVQ3', 'BWVR3', 'BWVRA3')

function bwMod:ProbeLocal()
	return BIGWIGS_RELEASE_REVISION
end

function bwMod:BroadcastProbe()
	return self:SendComm("RAID", "BWVQ3", "-")
end

function bwMod:SingleProbe(name)
	return self:SendComm("WHISPER", "BWVQ3", name, "-")
end

function bwMod:BWVR3(prefix, channel, sender, message)
	self:HasBossMod(sender, message)
end

bwMod.BWVRA3 = bwMod.BWVR3
bwMod.BWVQ3 = bwMod.SeenRemoteProbe

-- Deadly Boss Mods

local dbmMod = module:NewAddonMessageModule('DBM', 'Deadly Boss Mods', 'DBMv4-Ver')

function dbmMod:ProbeLocal()
	return type(DBM) == "table" and DBM.Revision
end

-- Can't probe player individually

function dbmMod:BroadcastProbe()
	return self:SendComm("RAID", "DBMv4-Ver", "Hi!")
end

dbmMod['DBMv4-Ver'] = function(self, prefix, channel, sender, message)
	self:HasBossMod(sender, tonumber(strsplit("\t", message) or ""))
end

-- Deus Vox Encounters

local dxeMod = module:NewAceCommModule('DXE', 'Deus Vox Encounters', 'DXE')

function dxeMod:ProbeLocal()
	return type(DXE) == "table" and DXE.version
end

function dxeMod:BroadcastProbe()
	return self:SendComm("RAID", "DXE", "RequestAddOnVersion", "addon")
end

function dxeMod:SingleProbe(name)
	return self:SendComm("WHISPER", "DXE", name, "RequestVersions", "addon")
end

function dxeMod:DXE(prefix, channel, sender, commType, arg1, arg2)
	if commType == 'VersionBroadcast' and arg1 == 'addon' then
		self:HasBossMod(sender, arg2)
	elseif commType == 'AllVersionsBroadcast' then
		self:HasBossMod(sender, tonumber(arg1:match('addon,([^:]+):')))
	elseif commType == 'RequestVersions' and channel == 'RAID' then
		self:SeenRemoteProbe()
	end
end

-- Raid Watch 2

local rw2Mod = module:NewAceCommModule('RW2', 'Raid Watch 2', 'RW2')

function rw2Mod:ProbeLocal()
	return RWREVISION
end

function rw2Mod:BroadcastProbe()
	return self:SendComm("RAID", "RW2", "ReqVersionInfo")
end

function rw2Mod:SingleProbe(name)
	return self:SendComm("WHISPER", "RW2", name, "ReqVersionInfo")
end

function rw2Mod:RW2(prefix, channel, sender, commType, _, revision)
	if commType == 'IncGuildVersionInfo' or commType == 'IncVersionInfo' then
		self:HasBossMod(sender, revision)
	elseif commType == 'ReqVersionInfo' and channel == 'RAID' then
		self:SeenRemoteProbe()
	end
end
