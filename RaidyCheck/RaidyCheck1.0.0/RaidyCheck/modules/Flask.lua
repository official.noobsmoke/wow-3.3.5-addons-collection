--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

-- L['Flask']
local module = addon:NewModule('Flask')
module.strictChecking = true

local flasks = {
	(GetSpellInfo(53760)), -- Endless rage
	(GetSpellInfo(54212)), -- Pure Mojo
	(GetSpellInfo(53758)), -- Stoneblood
	(GetSpellInfo(53755)), -- Frost Wyrm
--[===[@debug@
	(GetSpellInfo(67019)), -- of the North
--@end-debug@]===]
}
local _, failure = addon.GetSpellStrings(53760)

module.legend = failure..' '..L['no flask']

function module:Test(event, guid, name, unit, class)
	for _, buffName in pairs(flasks) do
		if UnitBuff(unit, buffName)  then
			return buffName
		end
	end
	return "", failure
end
