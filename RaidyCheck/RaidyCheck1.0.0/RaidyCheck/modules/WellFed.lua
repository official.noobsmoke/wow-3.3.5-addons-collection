--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

-- L['Well Fed']
local module = addon:NewModule('Well Fed')
module.strictChecking = true

local buffName, icon = addon.GetSpellStrings(57294)
module.legend = icon..' '..L['not well fed']

function module:Test(event, guid, name, unit, class)
	if UnitBuff(unit, buffName) then
		return buffName
	else
		return "", icon
	end
end
