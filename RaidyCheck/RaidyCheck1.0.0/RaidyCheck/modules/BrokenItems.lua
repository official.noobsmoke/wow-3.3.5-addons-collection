--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
local L = addon.L

-- L['Broken Items']
local module = addon:NewModule('Broken Items', 'AceHook-3.0', 'AceTimer-3.0')

local failIcon = addon.IconString([[Interface\Icons\Trade_BlackSmithing]])

module.legend = failIcon..' '..L['has broken items.']

local slotsToCheck = {
	INVSLOT_HEAD, INVSLOT_SHOULDER, INVSLOT_BODY,	INVSLOT_CHEST,
	INVSLOT_WAIST, INVSLOT_LEGS, INVSLOT_FEET, INVSLOT_WRIST,
	INVSLOT_HAND, INVSLOT_MAINHAND, INVSLOT_OFFHAND, INVSLOT_RANGED
}

local INSPECT_CACHE_TIME = 10
local INSPECT_PERIOD = 2

local brokenItems = {}
local lastInspect = {}
local queue = {}
local timerHandle

function module:OnEnable()
	module.prototype.OnEnable(self)
	self:SecureHook("NotifyInspect")
	self:OnRosterChange('OnEnable')
end

function module:OnDisable()
	module.prototype.OnDisable(self)
	self:StopProcessing(true)
end

function module:OnRosterChange(event)
	wipe(queue)
	local now = GetTime()
	for guid, _, unit in self.core:IterateRoster() do
		if (lastInspect[guid] or 0) < now - INSPECT_CACHE_TIME then
			tinsert(queue, unit)
		end
	end
	self:StartProcessing()
end

function module:StartProcessing()
	if queue[1] and not timerHandle or not self:TimeLeft(timerHandle) then
		self:Debug("Start processing")
		timerHandle = self:ScheduleRepeatingTimer("ProcessQueue", INSPECT_PERIOD)
	end
end

function module:StopProcessing(force)
	if not queue[1] or force then
		self:Debug("Stop processing")
		self:CancelTimer(timerHandle, true)
		timerHandle = nil
	end
end

function module:ProcessQueue()
	if _G.InspectFrame and _G.InspectFrame:IsShown() then return end
	self:Debug("Processing queue")
	for i = 1, #queue do
		local unit = tremove(queue, 1) -- cut head
		if UnitIsUnit(unit, 'player') then
			self:Debug("Inspecting player")
			self:NotifyInspect("player")
			break
		elseif CanInspect(unit, false) then
			self:Debug("Inspecting", unit)
			NotifyInspect(unit)
			break
		else
			self:Debug("Can't inspect", unit)
			tinsert(queue, unit) -- push at end of the queue
		end
	end
	if #queue > 0 then
		self:Debug("Remaining units to process:", unpack(queue))
	else
		self:StopProcessing()
	end
end

function module:NotifyInspect(unit)
	if not CanInspect(unit, false) then return end
	local guid, name, unit = self.core:UnitData(unit)
	if not guid then return end
	local now = GetTime()
	if (lastInspect[guid] or 0) >= now - INSPECT_CACHE_TIME then return end
	lastInspect[guid] = now
	self:Debug('NotifyInspect', guid, name, unit)
	for i, queuedUnit in ipairs(queue) do
		if UnitIsUnit(queuedUnit, unit) then
			self:Debug("Removing it from queue")
			tremove(queue, i)
			self:StopProcessing()
			break
		end
	end
	local count = 0
	for i, slot in pairs(slotsToCheck) do
		if GetInventoryItemBroken(unit, slot) then
			self:Debug("Slot", slot, "broken")
			count = count + 1
		end
	end
	brokenItems[guid] = count
end

function module:Test(event, guid, name, unit, class)
	local count = brokenItems[guid]
	if not count then return end
	if count > 0 then
		return L["%d piece(s)"]:format(count), (count > 0) and failIcon
	else
		return ""
	end
end


