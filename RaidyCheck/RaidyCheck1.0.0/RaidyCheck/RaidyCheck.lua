--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName, addon = ...
_G[addonName] = LibStub('AceAddon-3.0'):NewAddon(addon, addonName, 'AceEvent-3.0', 'AceTimer-3.0', 'AceHook-3.0')
local L = addon.L

local LibGroupTalents = LibStub('LibGroupTalents-1.0')
local LibQTip = LibStub('LibQTip-1.0')

local UnitName = UnitName
local UnitClass = UnitClass
local UnitGUID = UnitGUID
local UnitIsConnected = UnitIsConnected
local UnitIsDeadOrGhost = UnitIsDeadOrGhost
local UnitIsVisible = UnitIsVisible

local DEFAULTS = {
	profile = {
		enabled = true,
		showInPartyInstance = false,
		showAnchor = true,
		lockAnchor = false,
		tooltipLegend = true,
		anchor = { 'TOPLEFT', nil, 'TOPLEFT', 100, -100 },
		buttons = { ['*'] = true },
		modules = { ['*'] = true },
		collapsed = false,
		scrollable = false,
		multiColumns = false,
		alternateDisplay = false,
	}
}
addon.DEFAULTS = DEFAULTS

-- Debugging code
if tekDebug then
	local frame = tekDebug:GetFrame(addonName)
	function addon:Debug(...) 
		frame:AddMessage('|cffff7700['..self.name..']|r '..string.join(", ",tostringall(...)):gsub("([%[%(=]), ", "%1"):gsub(', ([%]%)])','%1'):gsub(':, ', ': ')) 
	end
else
	function addon.Debug() end
end

function addon:ShouldActivate()
	if not self.db.profile.enabled or InCombatLockdown() then
		return false
	elseif self.testMode then
		return true
	end
	local _, instanceType =  IsInInstance()
	return instanceType == 'party' or instanceType == 'raid'
end

function addon:CheckActivation(event, ...)
	if not self:ShouldActivate() then
		self:Debug('Disabling RaidyCheck on:', event)
		return self:Disable()
	elseif not self:IsEnabled() then
		self:Debug('Enabling RaidyCheck on:', event)
		return self:Enable()
	end
	-- Already enabled, only update module states
	self:Debug('Checking module state on:', event)
	local needUpdate
	for name, module in self:IterateModules() do
		if module:ShouldActivate() then
			if not module:IsEnabled() then
				self:Debug('Enabling '..name)
				module:Enable()
				needUpdate = true
			end
		elseif module:IsEnabled() then
			self:Debug('Disabling '..name)
			module:Disable()
			needUpdate = true
		end
	end
	if needUpdate then
		return self:UpdateAll(event)
	end
end

function addon:OnInitialize()
	self.db = LibStub('AceDB-3.0'):New("RaidyCheckDB", DEFAULTS, true)

	self.anchor = self:CreateAnchor()

	local ae = LibStub('AceEvent-3.0')
	ae.RegisterEvent(addonName, "PLAYER_REGEN_ENABLED", addon.CheckActivation, addon)
	ae.RegisterEvent(addonName, "PLAYER_ENTERING_WORLD", addon.CheckActivation, addon)

	self:SetEnabledState(self:ShouldActivate())
end

function addon:OnEnable()
	self:Debug('OnEnable')
	if not self:IsHooked(GameTooltip, 'OnTooltipSetUnit') then
		self:HookScript(GameTooltip, 'OnTooltipSetUnit')
	end
	self:RegisterEvent('PLAYER_REGEN_DISABLED', 'Disable')
	self:RegisterEvent('PARTY_MEMBERS_CHANGED', 'UpdateRoster')
	self:RegisterEvent('RAID_ROSTER_UPDATE', 'UpdateRoster')
	self:RegisterEvent('UNIT_NAME_UPDATE')
	self:RegisterEvent('UNIT_AURA', 'UnitEvent')
	self:RegisterEvent('UNIT_HEALTH', 'UnitEvent')
	self:RegisterEvent('UNIT_FLAGS', 'UnitEvent')
	self:RegisterEvent('UNIT_DYNAMIC_FLAGS', 'UnitEvent')
	self:RegisterEvent('PLAYER_ENTERING_WORLD', 'UpdateAll')
	self:RegisterEvent('UNIT_TARGET', 'CheckStrictMode')
	self:RegisterEvent('UPDATE_MOUSEOVER_UNIT', 'CheckStrictMode')
	LibGroupTalents.RegisterCallback(self, 'LibGroupTalents_Update')

	self:ScheduleRepeatingTimer('OnUpdate', 1)
	if self.db.profile.showAnchor then
		self.anchor:Show()
	end
	for name, module in self:IterateModules() do
		local enable = module:ShouldActivate()
		self:Debug(module.name..':', enable)
		module:SetEnabledState(enable)
	end

	self:WipeRoster()
	self:ScheduleTimer('PostEnable', 0.1)
end

function addon:PostEnable()
	self:Debug('PostEnable')
	self:UpdateRoster('PostEnable')
	self:UpdatePanel()
end

function addon:OnDisable()
	self:Debug('OnDisable')
	if self.automaticStrict then
		self.automaticStrict = nil
		self:SetStrictMode(false, true)
	end
	LibGroupTalents.UnregisterAllCallbacks(self)
	self:HidePanel()
	self.anchor:Hide()
end

function addon:CheckStrictMode(event, unit)
	if self.strictChecking or self.automaticStrict then return end
	if event == 'UPDATE_MOUSEOVER_UNIT' then
		unit = 'mouseover'
	else
		unit = unit .. 'target'
	end
	if string.match(tostring(UnitClassification(unit)), 'boss') and UnitCanAttack('player', unit) and not UnitIsDeadOrGhost(unit) then
		self:Debug('Automatically enable strict mode')
		self.automaticStrict = true
		self:SetStrictMode(true)
	end
end

function addon:SetStrictMode(value, dontCheckActivation)
	if value ~= self.strictChecking then
		self:Debug('New strict mode:', value)
		self.strictChecking = value
		self.anchor.strict:SetChecked(value)
		if not dontCheckActivation then
			return self:CheckActivation('SetStrictMode')
		end
	end
end

function addon:OnConfigChanged(key, value, ...)
	if key == 'showInPartyInstance' or key == 'testMode' or key == 'enabled' then
		return self:CheckActivation('OnConfigChanged-'..key)
	elseif key == 'anchor' then
		self.anchor:ClearAllPoints()
		self.anchor:SetPoint(unpack(value))
		return self:UpdatePanel()
	end
	if not self:IsEnabled() then return end
	if key == 'showAnchor' then
		if value then
			self.anchor:Show()
		else
			self.anchor:Hide()
		end
	elseif key == 'tooltipLegend' or key == 'collapsed' or key == 'scrollable' or key == 'multiColumns' or key == 'alternateDisplay' then
		return self:UpdatePanel()
	elseif key == 'modules' then
		local name, value = value, ...
		if value then
			self:EnableModule(name)
		else
			self:DisableModule(name)
		end
		self:UpdateAll('OnConfigChanged')
	elseif key == 'buttons' then
		return self.anchor:LayoutButtons()
	end
end

function addon:IterateEnabledModules()
	local iterator, data, first = self:IterateModules()
	return function(data, name)
		local module
		repeat
			name, module = iterator(data, name)
		until not module or module:IsEnabled()
		return name, module
	end, data, first
end

function addon:ForEachModule(func, ...)
	if type(func) == 'string' then
		for name, module in self:IterateEnabledModules() do
			local method = module[func]
			if type(method) == 'function' then
				method(module, ...)
			end
		end
	else
		for name, module in self:IterateEnabledModules() do
			func(module, ...)
		end
	end
end

-- Roster handling
local names = {}
local units = {}
local classes = {}
local status = {}
do
	local function rosterIterator(list, guid)
		guid = next(list, guid)
		if guid then
			return guid, names[guid], units[guid], classes[guid]
		end
	end

	-- Querying methods
	function addon:IterateRoster(guidList)
		return rosterIterator, guidList or names
	end

	function addon:UnitData(unit)
		local guid = unit and UnitGUID(unit)
		if guid and names[guid] then
			return guid, names[guid], units[guid], classes[guid]
		end
	end

	-- Wipe roster data
	function addon:WipeRoster()
		wipe(names)
		wipe(units)
		wipe(classes)
		wipe(status)
	end

	-- Iterators
	local function raidIterator(num, index)
		if index < num then return index+1, 'raid'..index+1	end
	end
	local function partyIterator(num, index)
		if index == 0 then
			return 1, 'player'
		elseif index <= num then
			return index+1, 'party'..index
		end
	end
	local function soloIterator(num, index)
		if index == 0 then return 1, 'player'	end
	end

	-- Updating
	local added = {}
	local deleted = {}
	local changed = {}
	function addon:UpdateRoster(event, quiet)
		-- Get the iterator
		local num, iterator = GetNumRaidMembers(), raidIterator
		if num == 0 then
			num, iterator = GetNumPartyMembers(), partyIterator
			if num == 0 then
				num, iterator = 1, soloIterator
			end
		end
		-- Plan to deleted all roster
		for guid in pairs(names) do
			deleted[guid] = true
		end
		-- Scan the roster
		for i, unit in iterator, num, 0 do
			local name, guid, _, class = UnitName(unit), UnitGUID(unit), UnitClass(unit)
			if class and guid and name ~= UNKNOWN then
				local guid, _, class = UnitGUID(unit), UnitClass(unit)
				-- Do not delete this one
				deleted[guid] = nil
				if not names[guid] then
					-- New unit
					added[guid] = true
				elseif names[guid] ~= name or units[guid] ~= unit or classes[guid] ~= class then
					-- Something changed
					changed[guid] = true
				end
				-- Always update the data
				names[guid], units[guid], classes[guid] = name, unit, class
			end
		end
		-- Propagate changes
		if next(added) or next(changed) or next(deleted) then

			-- Per unit update
			for guid, name, unit, class in rosterIterator, deleted do
				self:RemoveUnit(event, guid, name, unit, class)
				names[guid], units[guid], classes[guid] = nil, nil, nil
			end

			-- Global update
			self:ForEachModule('OnRosterChange', added, deleted, changed)

			for guid, name, unit, class in rosterIterator, added do
				self:AddUnit(event, guid, name, unit, class)
			end
			for guid, name, unit, class in rosterIterator, changed do
				self:UpdateUnit(event, guid, name, unit, class)
			end

			-- Wipe our mess
			wipe(deleted)
			wipe(added)
			wipe(changed)
		end
	end

	function addon:UNIT_NAME_UPDATE(event, unit)
		if not (UnitInParty(unit) or UnitInRaid(unit)) or unit:match('pet') then return end
		local name, guid, _, class = UnitName(unit), UnitGUID(unit), UnitClass(unit)
		if class and guid and name ~= UNKNOWN then
			if name ~= names[guid] or unit ~= units[guid] or class ~= classes[guid] then
				local isNew = not names[guid]
				names[guid], units[guid], classes[guid] = name, unit, class
				if isNew then
					added[guid] = true
					self:ForEachModule('OnRosterChange', added, deleted, changed)
					wipe(added)
					self:AddUnit(event, guid, name, unit, class)
				else
					changed[guid] = true
					self:ForEachModule('OnRosterChange', added, deleted, changed)
					wipe(changed)
					self:UpdateUnit(event, guid, name, unit, class)
				end
			end
		end
	end
end

-- Unit updating
local dirtyUnits = {}

local function GetUnitStatus(unit)
	if not UnitIsConnected(unit) then
		return "DISCONNECTED"
	elseif not UnitIsVisible(unit) then
		return "OUTOFSCOPE"
	elseif UnitIsDeadOrGhost(unit) then
		return "DEAD"
	end
end

function addon:UpdateAll(event)
	self:Debug('UpdateAll', event)
	for guid in self:IterateRoster() do
		dirtyUnits[guid] = true
	end
	self:OnDataChange()
end

function addon:UnitEvent(event, unit)
	local guid = UnitGUID(unit)
	if names[guid] then
		dirtyUnits[guid] = true
	end
end

function addon:LibGroupTalents_Update(event, guid, unit)
	if names[guid] then
		self:Debug('LibGroupTalents_Update', event, unit)
		dirtyUnits[guid] = true
	end
end

local function _doUnit(self, action, event, guid, name, unit, class)
	self:Debug(action, event, guid, name, unit, class)
	local unitStatus = GetUnitStatus(unit)
	dirtyUnits[guid] = nil
	if unitStatus ~= status[guid] then
		status[guid] = unitStatus
		self:OnDataChange(event)
	elseif event == 'UNIT_HEALTH' then
		return
	end
	if not unitStatus then
		self:ForEachModule(action, event, guid, name, unit, class)
	end
end

function addon:AddUnit(event, guid, name, unit, class)
	return _doUnit(self, '_AddUnit', event, guid, name, unit, class)
end

function addon:UpdateUnit(event, guid, name, unit, class)
	return _doUnit(self, '_UpdateUnit', event, guid, name, unit, class)
end

function addon:RemoveUnit(event, guid, name, unit, class)
	dirtyUnits[guid], status[guid] = nil, nil
	self:ForEachModule('ClearUnit', event, guid, name, unit, class)
	self:OnDataChange(event)
end

-- Unit tooltip

function addon:OnTooltipSetUnit(tooltip)
	local name, unit = tooltip:GetUnit()
	local guid = UnitGUID(unit or name or "")
	if not guid or status[guid] or not names[guid] then return end
	self:Debug('OnTooltipSetUnit', names[guid])
	for moduleName, module in self:IterateEnabledModules() do
		local value = module:GetPlayerData(guid)
		if value then
			tooltip:AddDoubleLine(L[moduleName]..':', value == "" and '-' or value)
		end
	end
end

-- Spell related helpers

local ICONSIZE = 16
local function IconString(icon)
	if not icon then return end
	if icon:match([[^Interface\Icons]]) then
		return '|T'..icon..':'..ICONSIZE..':'..ICONSIZE..':0:0:64:64:5:61:5:61|t'
	else
		return '|T'..icon..':'..ICONSIZE..'|t'
	end
end
addon.ICONSIZE = ICONSIZE
addon.IconString = IconString

local function GetSpellStrings(id)
	local name, _, icon = GetSpellInfo(id)
	return name, IconString(icon)
end
addon.GetSpellStrings = GetSpellStrings

-- Panel handling

local dirtyPanel

do
	local panel

	function addon:HidePanel()
		if panel then
			LibQTip:Release(panel)
			panel = nil
		end
	end

	local TOOLTIP_BACKDROP = {
		bgFile = [[Interface\Tooltips\UI-Tooltip-Background]], tile = true, tileSize = 16,
		edgeFile = [[Interface\Tooltips\UI-Tooltip-Border]], edgeSize = 16,
		insets = { left=5, right=5, top=5, bottom=5 }
	}

	function addon:GetPanel(numColumns)
		if panel and panel:GetColumnCount() ~= numColumns then
			self:HidePanel()
		end
		if not panel then
			if numColumns > 2 then
				panel = LibQTip:Acquire(addonName)
				panel:AddColumn("LEFT")
				for i = 2, numColumns do
					panel:AddColumn("CENTER")
				end
			else
				panel = LibQTip:Acquire(addonName, 2, "LEFT", "RIGHT")
			end
			panel:SetBackdrop(TOOLTIP_BACKDROP)
			panel:SetBackdropColor(0.09, 0.09, 0.09, 0.75)
			panel:SetBackdropBorderColor(0.5, 0.5, 0.5, 1)
			panel:SetFrameStrata("MEDIUM")
		else
			panel:Clear()
		end
		return panel
	end
end

local new, del
do
	local heap = {}
	function new(...)
		local t = next(heap) or {}
		heap[t] = nil
		return t
	end
	function del(t, recurse)
		if recurse then
			for k,v in pairs(t) do
				if type(v) == "table" then
					del(v, true)
				end
			end
		end
		wipe(t)
		heap[t] = true
	end
end

local WHITE = { r = 1, g = 1, b = 1 }
local STATUS_ICON = {
	DISCONNECTED = IconString([[Interface\Icons\INV_Sigil_Thorim]])..' |cffff0000'..L['DISCONNECTED']..'|r',
	DEAD = [[|TInterface\TargetingFrame\UI-TargetingFrame-Skull:]]..ICONSIZE..':'..ICONSIZE..':0:0:32:32:4:26:4:26|t |cffff0000'..L['DEAD']..'|r',
	OUTOFSCOPE = IconString([[Interface\Icons\Spell_Frost_Stun]])..' |cffff0000'..L['OUTOFSCOPE']..'|r',
}

function addon:GetPanelData(showData)
	local method = showData and "GetPlayerData" or "GetPlayerFailure"
	local columns, names, lines = new(), new(), new()
	local numLines = 0
	local COLORS = CUSTOM_CLASS_COLORS or RAID_CLASS_COLORS
	for guid, name, unit, class in self:IterateRoster() do
		local unitStatus = status[guid]
		local color = class and COLORS[class] or WHITE
		local coloredName = ("|cff%02x%02x%02x%s|r"):format(255*color.r, 255*color.g, 255*color.b, name)
		local line
		if unitStatus then
			line = new()
			line[1], line[2] = coloredName , STATUS_ICON[unitStatus]
		else
			for moduleName, module in self:IterateEnabledModules() do
				local data = module[method](module, guid)
				if data and data ~= "" then
					if not line then
						line = new()
						line[1] = coloredName
					end
					if not columns[moduleName] then
						columns[moduleName] = true
						tinsert(columns, moduleName)
					end
					line[moduleName] = data
				end
			end
		end
		if line then
			if not names[name] then
				names[name] = unit
				tinsert(names, name)
			end
			lines[name] = line
			numLines = numLines + 1
		end
	end
	if numLines > 0 then
		table.sort(names)
		table.sort(columns)
		return lines, columns, names
	else
		del(lines, true)
		del(columns)
		del(names)
	end
end

local function OnEnterLine(line, unit)
	GameTooltip:SetOwner(line, 'ANCHOR_NONE')
	local x,y = line:GetCenter()
	local vhalf = (y > UIParent:GetHeight()/2) and "TOP" or "BOTTOM"
	local hhalf = (x < UIParent:GetWidth()/2) and "LEFT" or "RIGHT"
	GameTooltip:SetPoint(vhalf..hhalf, line, (hhalf == "LEFT" and "RIGHT" or "LEFT"), (hhalf == "LEFT" and 10 or -10), 0)
	GameTooltip:SetUnit(unit)
	GameTooltip:Show()
end

local function OnLeaveLine(line)
	if GameTooltip:GetOwner() == line then
		GameTooltip:Hide()
	end
end

local function GetTipAnchor(frame)
	local x,y = frame:GetCenter()
	if not x or not y then return "TOPLEFT", frame, "BOTTOMLEFT" end
	local hhalf = (x > UIParent:GetWidth()*2/3) and "RIGHT" or (x < UIParent:GetWidth()/3) and "LEFT" or ""
	local vhalf = (y > UIParent:GetHeight()/2) and "TOP" or "BOTTOM"
	return vhalf..hhalf, frame, (vhalf == "TOP" and "BOTTOM" or "TOP")..hhalf
end

function addon:UpdatePanel()
	if not self:IsEnabled() or self.db.profile.collapsed then
		return self:HidePanel()
	end
	self:Debug('UpdatePanel')
	dirtyPanel = nil

	local lines, columns, names = self:GetPanelData(self.db.profile.alternateDisplay)
	if not lines then
		self:Debug('UpdatePanel: empty')
		return self:HidePanel()
	end
	self:Debug('UpdatePanel: names=', strjoin(",", names), 'columns=', strjoin(",", columns))

	local isMulti = self.db.profile.multiColumns
	local numColumns = isMulti and math.max(2, 1+#columns) or 2
	local panel = self:GetPanel(numColumns)
	for i, name in ipairs(names) do
		local line = lines[name]
		local coloredName, status = line[1], line[2]
		local y = panel:AddLine(coloredName)
		panel:SetLineScript(y, "OnEnter", OnEnterLine, names[name])
		panel:SetLineScript(y, "OnLeave", OnLeaveLine, names[name])
		if status then
			panel:SetCell(y, 2, status, nil, "CENTER", numColumns-1)
		elseif isMulti then
			for j, column in ipairs(columns) do
				panel:SetCell(y, 1+j, line[column] or "-")
			end
		else
			local oneString = new()
			for j, column in ipairs(columns) do
				local str = line[column]
				if str then
					tinsert(oneString, str)
				end
			end
			panel:SetCell(y, 2, table.concat(oneString, ""))
			del(oneString)
		end
	end
	del(lines, true)
	del(columns)
	del(names)

	if self.db.profile.tooltipLegend then
		local firstLegend = true
		for _, module in self:IterateEnabledModules() do
			if module.legend then
				if firstLegend then
					panel:AddSeparator()
					panel:SetCell(panel:AddLine(), 1, L['Legend'], nil, nil, panelWidth)
					firstLegend = false
				end
				panel:SetCell(panel:AddLine(), 1, module.legend, nil, nil, panelWidth)
			end
		end
	end

	panel:SetPoint(GetTipAnchor(self.anchor))
	panel:UpdateScrolling(addon.db.profile.scrollable and (UIParent:GetHeight() / 4) or nil)
	panel:Show()
end

function addon:OnDataChange()
	dirtyPanel = true
end

-- Global update method

function addon:OnUpdate()
	for guid in pairs(dirtyUnits) do
		if names[guid] and units[guid] and classes[guid] then
			self:UpdateUnit('OnUpdate', guid, names[guid], units[guid], classes[guid])
		else
			dirtyUnits[guid] = nil
		end
	end
	if dirtyPanel then
		self:UpdatePanel()
	end
end

-- Module prototype

local moduleProto = {
	core = addon,
	Debug = addon.Debug,
}
moduleProto.prototype = moduleProto

addon:SetDefaultModuleState(false)
addon:SetDefaultModuleLibraries('AceEvent-3.0')
addon:SetDefaultModulePrototype(moduleProto)

function moduleProto:OnInitialize()
	self:Debug('OnInitialize')
	self.data = {}
	self.failures = {}
end

function moduleProto:OnEnable()
	self:Debug('OnEnable')
end

function moduleProto:OnDisable()
	self:Debug('OnDisable')
end

function moduleProto:ShouldActivate()
	return addon.db.profile.modules[self.moduleName] and (addon.strictChecking or not self.strictChecking)
end

function moduleProto:ClearUnit(event, guid, name, unit, class)
	self:Debug('ClearUnit', event, guid, name, unit, class)
	self:SetData(guid)
end

function moduleProto:GetPlayerData(guid)
	return self.data[guid]
end

function moduleProto:GetPlayerFailure(guid)
	return self.failures[guid]
end

function moduleProto:SetData(guid, data, failure)
	if guid and (data ~= self.data[guid] or failure ~= self.failures[guid]) then
		self:Debug('Data changed for:', guid, 'data=', data, 'failure=', failure)
		self.data[guid], self.failures[guid] = data, failure
		addon:OnDataChange()
	end
end

function moduleProto:_UpdateUnit(event, guid, name, unit, class)
	self:SetData(guid, self:Test(event, guid, name, unit, class))
end

moduleProto._AddUnit = moduleProto._UpdateUnit

function moduleProto:UpdateUnit(event, unitOrGUID)
	local guid = unitOrGUID and ((names[unitOrGUID] and unitOrGUID) or UnitGUID(unitOrGUID))
	if not guid or status[guid] or not names[guid] then return end
	self:Debug('UpdateUnit', event, unitOrGUID, 'data:', guid, names[guid], units[guid], classes[guid])
	self:_UpdateUnit(event, guid, names[guid], units[guid], classes[guid])
end

function moduleProto:Test(event, guid, name, unit, class)
	return self.data[guid], self.failures[guid] -- NOOP
end
