## Interface: 30100
## LoadOnDemand: 1
## Title: Lib: TalentQuery 1.0
## Notes: Library to help with querying unit talents.
## Author: Peragor
## Version: $Rev: 58 $
## X-Category: Library
## X-ReleaseDate: $Date: 2009-05-21 09:52:56 +0000 (Thu, 21 May 2009) $
## X-Website: http://wowace.com/wiki/LibTalentQuery-1.0
## X-License: LGPL v2.1
## X-Curse-Packaged-Version: WoW 3.1.2 Release
## X-Curse-Project-Name: LibTalentQuery-1.0
## X-Curse-Project-ID: libtalentquery-1-0
## X-Curse-Repository-ID: wow/libtalentquery-1-0/mainline

LibStub\LibStub.lua
CallbackHandler-1.0\CallbackHandler-1.0.lua
lib.xml
