--[=[
RaidyCheck - raid boss mod and buff checking
(c) 2010 Adirelle (adirelle@tagada-team.net)
All rights reserved.
--]=]

local addonName = 'RaidyCheck'
local addon = LibStub('AceAddon-3.0'):GetAddon(addonName)
local L = addon.L

-----------------------------------------------------------------------------
-- Optionxs
-----------------------------------------------------------------------------

function CreateOptions()
	local function ResolvePath(info)
		local db = addon.db.profile
		local path = info.arg or info[#info]
		local key = tostring(path)
		if type(path) == "table" then
			key = path[#path]
			for i = 1, #path-1 do
				db = db[path[i]]
			end
		end
		return db, key
	end
	
	local function Get(info, ...)
		local db, key = ResolvePath(info)
		if info.type == 'multiselect' then
			db, key = db[key], ...
		end
		return db[key]
	end
	
	local function Set(info, value, ...)
		local db, key = ResolvePath(info)
		if info.type == 'multiselect' then
			local subKey, value = value, ...
			db[key][subKey] = value
		else
			db[key] = value
		end
		return addon:OnConfigChanged(key, value, ...)
	end
	
	local moduleList = {}
	
	local function isAddonDisabled(info)
		return info.type ~= 'group' and not addon.db.profile.enabled
	end
	local function isAnchorHidden(info)
		return isAddonDisabled(info) or not addon.db.profile.showAnchor 
	end

	return {
		get = Get,
		set = Set,
		name = 'RaidyCheck',
		type = 'group',
		childGroups = 'tab',
		disabled = isAddonDisabled,
		args = {
			tests = {
				name = L['Tests'],
				type = 'group',
				order = 10,
				args = {
					enabled = {
						name = L['Enabled'],
						desc = L['Uncheck this to completly disable RaidyCheck'],
						type = 'toggle',
						order = 0,
						disabled = false,
					},
					testMode = {
						name = L['Test mode'],
						desc = L['Check this to enable RaidyCheck outside of instances. This settings is not saved accros game sessions.'],
						type = 'toggle',
						order = 10,
						get = function() return addon.testMode end,
						set = function(info, value) addon.testMode = value addon:OnConfigChanged('testMode', value) end,
					},
					strictMode = {
						name = L['Strict mode'],
						desc = L['Check this to enable all tests.'],
						type = 'toggle',
						order = 20,
						get = function() return addon.strictChecking end,
						set = function(info, value) addon:SetStrictMode(value) end,
					},
					showInPartyInstance = {
						name = L['Show in 5-man instances'],
						desc = L['Check this to enable RaidyCheck in 5-man instances in addition to raid instances.'],
						type = 'toggle',
						order = 30,
					},
					modules = {
						name = L['Modules'],
						desc = L['Select which checks should be done'],
						type = 'multiselect',
						values = function()
							for name, module in addon:IterateModules() do
								moduleList[name] = L[module.uiName or name]
							end
							return moduleList
						end,
						order = 40,
					},				
				},
			},
			display = {
				name = L['Display'],				
				order = 20,
				type = 'group',
				args = {
					_panelHeader = {
						name = L['Panel'],
						desc = L['Panel settings'],
						type = 'header',
						order = 100,
					},
					scrollable = {
						name = L['Limited height'],
						desc = L['Check this to restrict panel height by adding a scroll bar.'],
						type = 'toggle',
						order = 110,
					},
					multiColumns = {
						name = L['Multicolumns'],
						desc = L['Check this to have each module result displayed on its own column.'],
						type = 'toggle',
						order = 120,
					},
					tooltipLegend = {
						name = L['Legend'],
						desc = L['Check this to display the tooltip legend in addition to failed checks.'],
						type = 'toggle',
						order = 120,
					},
					_anchorHeader = {
						name = L['Anchor'],
						desc = L['Anchor settings'],
						type = 'header',
						order = 200,
					},
					showAnchor = {
						name = L['Show'],
						desc = L['Check this to show the tooltip anchor button.'],
						type = 'toggle',
						order = 210,
					},
					lockAnchor = {
						name = L['Lock'],
						desc = L['Check this to prevent the anchor to be dragged.'],
						type = 'toggle', 
						order = 215,
						disabled = isAnchorHidden,
					},
					resetAnchor = {
						name = L['Reset position'],
						desc = L['Move the anchor back to its default position.'],
						type = 'execute',
						func = function()
							for k,v in pairs(addon.DEFAULTS.profile.anchor) do
								addon.db.profile.anchor[k] = v
							end
							return addon:OnConfigChanged('anchor', addon.DEFAULTS.profile.anchor)
						end,
						order = 220,
						disabled = isAnchorHidden,
					},
					buttons = {
						name = L['Buttons'],
						desc = L['Select which buttons you would like to see on the anchor.'],
						type = 'multiselect',
						values = {
							strict = L['Strict mode'],
							expand = L['Collapse/expand'],
							config = L['Configuration'],
							readycheck = L['Readycheck'],							
						},
						order = 230,
						disabled = isAnchorHidden,
					},
				},
			},
		},
	}
end

-----------------------------------------------------------------------------
-- Setup
-----------------------------------------------------------------------------

local configAddonName = ...
local OPTION_CATEGORY = GetAddOnMetadata(configAddonName, 'X-LoadOn-InterfaceOptions')

-- AddonLoader support
if AddonLoader and AddonLoader.RemoveInterfaceOptions then
	AddonLoader:RemoveInterfaceOptions(OPTION_CATEGORY)
end

local AceConfig = LibStub("AceConfig-3.0")
local AceConfigDialog = LibStub("AceConfigDialog-3.0")

-- Main options
local options
AceConfig:RegisterOptionsTable(addonName, function() 
	if not options then 
		options = CreateOptions()
	end
	return options 
end)
AceConfigDialog:AddToBlizOptions(addonName, OPTION_CATEGORY)

-- Macro to open to the GUI
SLASH_RAIDYCHECK1 = "/raidycheck"
SLASH_RAIDYCHECK2 = "/rdc"
function SlashCmdList.RAIDYCHECK(arg)
	if arg == 'test' then
		addon.testMode = not addon.testMode
		return addon:OnConfigChanged('testMode', addon.testMode)
	end
	return InterfaceOptionsFrame_OpenToCategory('RaidyCheck')
end

