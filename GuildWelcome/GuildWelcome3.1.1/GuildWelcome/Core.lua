local L = LibStub("AceLocale-3.0"):GetLocale("GuildWelcome")
local event_frame = CreateFrame("Frame", nil, UIParent)
event_frame:SetScript("OnEvent", function(_, _, msg)
    local name = string.match(msg, L["(.+) has joined the guild"])
	if name then
		GuildWelcome:MemberAdded(name)
	end
end)
event_frame:SetScript("OnUpdate", function(self, elapsed)
    if self.timing then
        self.TimeSinceLastUpdate = self.TimeSinceLastUpdate + elapsed
        if (self.TimeSinceLastUpdate > self.timetowait) then
            GuildWelcome:Pour(self.message, 0, 1, 0)
            self.timing = false
            self.TimeSinceLastUpdate = 0
        end
    end
end)
event_frame:RegisterEvent("CHAT_MSG_SYSTEM")
event_frame.TimeSinceLastUpdate = 0

GuildWelcome = LibStub("AceAddon-3.0"):NewAddon("GuildWelcome", "LibSink-2.0", "AceConsole-3.0", "AceEvent-3.0")
local ldb = LibStub:GetLibrary("LibDataBroker-1.1")
local dataobj = ldb:NewDataObject("GuildWelcome", {
	type = "launcher",
    launcher = true,
    icon = "Interface\\Icons\\INV_Misc_Note_04",
    OnClick = function(clickedframe, button)
        InterfaceOptionsFrame_OpenToFrame(GuildWelcome.optionsframe)
    end,
    tooltiptext = "GuildWelcome",
})

local options = { 
    name = "GuildWelcome",
    handler = GuildWelcome,
    type = 'group',
    childGroups = "tab",
    args = {
		msg = {
            type = 'input',
            multiline = true,
            name = L["Message"],
            desc = L["The message text to be displayed.  Note the string \"%s\" will be replaced with the username"],
            usage = L["<Your message here>"],
            width = "full",
            get =   function() 
                        return tostring(GuildWelcome.db.profile.message)
                    end,
            set =   function(info, NewValue)
                        GuildWelcome.db.profile.message = tostring(NewValue)
                    end,
        },
		time = {
			type = 'range',
			name = L["Time"],
			desc = L["The average amount of time (in seconds) between noticing a new member and your Welcome message"],
			min = 1,
			max = 20,
			get =   function()
                        return tonumber(GuildWelcome.db.profile.between)
                    end,
			set =   function(info, NewValue)
                        GuildWelcome.db.profile.between = tonumber(NewValue)
                    end,
		},
	},
}

--CONSTANTS
GuildWelcome.version = '3.11'

local function ChatCmd(input)
	if not input or input:trim() == "" then
		InterfaceOptionsFrame_OpenToFrame(GuildWelcome.optionsframe)
	else
		LibStub("AceConfigCmd-3.0").HandleCommand(NazScrooge, "GuildWelcome", "GuildWelcome", input:trim() ~= "help" and input or "")
	end
end

function GuildWelcome:OnEnable()
    self.db = LibStub("AceDB-3.0"):New("GuildWelcomeDB", {}, "Default")
    self.db:RegisterDefaults({
        profile = {
            sinkOptions = {
                sink20OutputSink = "Channel",
                sink20ScrollArea = "Guild Chat",
            },
            message = "Welcome %s",
            between = 2,
            display = 'Guild Chat',
        },
    })
	self:SetSinkStorage(self.db.profile.sinkOptions) -- set location to save sink options
	options.args.output = self:GetSinkAce3OptionsDataTable() -- add in the libsink options table to our options table
    options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db) -- add in the profile commands to our options table
    self.optionsframe = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("GuildWelcome", "GuildWelcome") -- Add the options to Bliz's new section in interface
	LibStub("AceConfig-3.0"):RegisterOptionsTable("GuildWelcome", options) -- Register the chat commands to use our options table
    self:RegisterChatCommand(L["Slash-Command"], ChatCmd)
    self:RegisterChatCommand(L["Slash-Command-Short"], ChatCmd)
end

function GuildWelcome:MemberAdded(name)
	--new member to guild name is name
	local newmessage = string.format(GuildWelcome.db.profile.message, name)
	local newtime = math.random()
	newtime = newtime * 2 * GuildWelcome.db.profile.between
    event_frame.timing = true
    event_frame.timetowait = newtime
    event_frame.message = newmessage
end