## Title: Guild Welcome |cff7fff7f -3.11-|r
## Notes: Addon to welcome new guild members
## Author: Wintermute

## Interface: 30100
## Version: 3.11


## DefaultState: enabled
## LoadOnDemand: 0
## SavedVariables: GuildWelcomeDB
## OptionalDeps: Ace3, LibSink-2.0
## X-Embeds: Ace3, LibSink-2.0
## X-Category: Inventory
## X-License: GPL v2 or later

## LoadManagers: AddonLoader
## X-LoadOn-Guild: true
## X-LoadOn-Slash: /GuildWelcome, /gw

embeds.xml
locales\locales.xml

Core.lua