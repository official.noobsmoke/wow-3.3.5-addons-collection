## Interface: 30300
## Title: Rebirther
## Notes: Tracks Rebirth and Innervate cooldowns.
## Author: torkel104
## Version: Beta-100802
## SavedVariables: RebirtherDB
## OptionalDeps: SharedMedia
## X-Curse-Packaged-Version: Beta-100802
## X-Curse-Project-Name: Rebirther
## X-Curse-Project-ID: rebirther
## X-Curse-Repository-ID: wow/rebirther/mainline

embeds.xml
templates.xml

Locales\enUS.lua

Rebirther.lua
